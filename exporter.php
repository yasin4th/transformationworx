<html>
<head>
	<!-- <meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
    <meta charset="UTF-8">
</head>
<body>
<a class="update-btn" href="#" onclick="tableToExcel('exporttable', 'W3C Example Table','Class')">Export</a>
<a href="#" id="test" onClick="javascript:fnExcelReport();">download</a>
<script>
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name, filename) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    var downloadLink = document.createElement("a");
	downloadLink.href = uri + base64(format(template, ctx));
	var d = new Date();
	downloadLink.download = filename + '_('+d.getDate()+'-'+(d.getMonth()+1)+'-'+d.getFullYear()+')';
	document.body.appendChild(downloadLink);
	downloadLink.click();
	document.body.removeChild(downloadLink);
    
    //window.location.href = uri + base64(format(template, ctx))
   
  }
})()

 function fnExcelReport() 
 {
            var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
            tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';

            tab_text = tab_text + '<x:Name>Test Sheet</x:Name>';

            tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
            tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';

            tab_text = tab_text + "<table border='1px'>";
            tab_text = tab_text + $('#exporttable').html();
            tab_text = tab_text + '</table></body></html>';

            var data_type = 'data:application/vnd.ms-excel';

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                if (window.navigator.msSaveBlob) {
                    var blob = new Blob([tab_text], {
                        type: "application/csv;charset=utf-8;"
                    });
                    navigator.msSaveBlob(blob, 'Test file.xls');
                }
            } else {
                $('#test').attr('href', data_type + ', ' + encodeURIComponent(tab_text));
                $('#test').attr('download', 'Test file.xls');
            }

        }
</script>
<?php
$servername = "localhost";
$username  = "root";
// $password  = "GDiV74Yoez";
$password  = "";
// $dbname    = "elearning";
$dbname    = "testenginestaging";
// Create connection
$conn = new mysqli($servername, $username, $password ,$dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
// echo "Connected successfully";
$where = '';
if(isset($_GET['class']))
{
	$where = 'AND class_id = '.$_GET['class'];
}
$sql = "SELECT id,question,parent_id,type FROM questions WHERE `parent_id` = '0' AND `delete` = '0' ".$where;

$result = $conn->query($sql);

?>
<table id="exporttable">
	<thead>
		<tr>
			<th>ID</th>
			<th>Question</th>
			<th>Option 1</th>
			<th>Option 2</th>
			<th>Option 3</th>
			<th>Option 4</th>
			<th>Correct</th>
			<th>Passage</th>
		</tr>		
	</thead>
	<tbody>
		
			

<?php

while($row = $result->fetch_assoc()) 
{
	if($row['type'] == '11')
	{
		$sql = "SELECT id,question,parent_id,type FROM questions WHERE parent_id = '".$row['id']."' AND `delete` = 0 ";

		$res = $conn->query($sql);
		
		$child = 1;
		$row['question'] = breakk($row['question']);
		while($row1 = $res->fetch_assoc()) 
		{
			echo "<tr>";		
				echo "<td>{$row1['id']}</td>";
				$cq = breakk($row1['question']);
				echo "<td>{$cq}</td>";
				$sql = "SELECT * FROM options WHERE question_id = '".$row1['id']."' AND deleted = 0 ";

				$res2 = $conn->query($sql);
				$counter = 1;
				$correct = 0;
				while($row2 = $res2->fetch_assoc())
				{
					if($row2['answer'] == '1')
					{
						$correct = $counter;
					}
					$counter++;	
					$opt = breakk($row2['option']);		
					echo "<td>{$opt}</td>";
				}
				echo "<td>{$correct}</td>";

				if($child == 1)
				{
					echo "<td rowspan='{$res->num_rows}'>{$row['question']}</td>";					
				}
				else
				{
					//echo "<td></td>";
				}

			echo "</tr>";
			$child++;
		}

		
	}
	else
	{
		$row['question'] = breakk($row['question']);
		echo "<tr>";		
			echo "<td>{$row['id']}</td>";
			echo "<td>{$row['question']}</td>";

			$sql = "SELECT * FROM options WHERE question_id = '".$row['id']."' AND deleted = 0 ";

			$res2 = $conn->query($sql);
			$counter = 1;
			$correct = 0;
			while($row2 = $res2->fetch_assoc())
			{
				
				if($row2['answer'] == '1')
				{
					$correct = $counter;
				}
				$counter++;
				$opt = breakk($row2['option']); 
				echo "<td>{$opt}</td>";
			}			
			echo "<td>{$correct}</td>";
			// echo "<td></td>";
		echo "</tr>";
	}
}

function breakk($str)
{
	$str = preg_replace('/<br>/', '<_BR_>', $str);
	return $str;
}

?>
		
	</tbody>
</table>
<?php 
$conn->close();


//     print_r($row['question']);

//     $data = $row['question'];
//         // echo "<pre>";        
//         // preg_match_all('/src="([^\s=]+)"/', $data, $matches);
//         // unset($matches[0]);
//         // print_r($matches);
//         // echo "</pre>";
    
//         // foreach ($matches[1] as $key=> $value) {
                        
//         //     $target_dir = "assets/qimages/";
//         //     $target_file = date('Y-m-d-H-i-s-').substr($value,strrpos($value,"/")+1);
//         //     echo $target_file."<br>";
//         //     copy($value,$target_dir.$target_file);
//         $new_data = str_replace('size="14"', "", $data);
//         $new_data = str_replace('size="16"', "", $new_data);
//         $new_sql = "UPDATE questions SET question='$new_data' where question='$data'";
//         // $res = $conn->query($new_sql);
//             // echo $new_data;
//         // }
    
        

   
// }
        

// $conn->close();
?>

</body>
</html>