<?php
include __DIR__.'/vendor/Zend/Loader/AutoloaderFactory.php';
Zend\Loader\AutoloaderFactory::factory(array(
		'Zend\Loader\StandardAutoloader' => array(
				'autoregister_zf' => true,
				'db' => 'Zend\Db\Sql'
		)
));
		require('vendor/custom/Api.php');
		$req = new stdClass();
		$req->hash = $_GET['phrase'];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"http://ec2-52-54-81-196.compute-1.amazonaws.com:3002/getCertificate");
		// curl_setopt($ch, CURLOPT_URL,"http://192.168.1.20:3002/getCertificate");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,"hash={$req->hash}");

		// in real life you should use something like:
		// curl_setopt($ch, CURLOPT_POSTFIELDS,
		//          http_build_query(array('postvar1' => 'value1')));

		// receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec ($ch);
		// echo "<pre>";
		$so = json_decode($server_output);

		curl_close ($ch);
		$a = Api::getCertificate($req);
		//print_r($so);
		//die();

		if($a->status != 1 && $so->username != $a->data['firstname']) {die('Something went wrong.');}

		header("Location: /certificates/{$req->hash}/Certificate.pdf");
		/*require_once('vendor/custom/PDF-Library/autoload.php');

		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor($req->data['username']);
		$pdf->SetTitle('Certificate');
		$pdf->SetSubject('TCPDF Tutorial');
		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('dejavusans', '', 14, '', true);

		// Add a page
		// This method has several options, check the source code documentation for more information.
		$pdf->AddPage();

		// set text shadow effect
		$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$issued = date('d-M-Y', $a->data['created']);
$expired = date('d-M-Y', strtotime('+3 years', $a->data['created']));
// die($issued);
		// Set some content to print
$html = <<<EOD
<div style="text-align: center;max-width: 900px;margin: auto;">
<table>
<tbody>
<tr>
<td>
<div style="text-align: left;padding: 0 15px; float: left;"><h1 style="color: #f00">CIO<br><small style="color: #333; font-size: 12px;">Association of Canada</small></h1></div>
</td>
<td>
<div style="text-align: right;padding: 0 15px; float: left;"><h1 style="color: #f00;">Transformation<span style="color: #333;">Worx</span></h1></div>
</td>
</tr>
</tbody>
</table>
<p>Certify that</p>
<p><span style="border-bottom: 1px solid #333; padding: 2px 15px;">{$a->data['username']}</span></p>
<p>has satisfied the requirement of CIO Association of Canada<br>and TransformationWorx to receive this certification and practice as </p>
<h1 style="color: #777;">{$a->data['program']}</h1>


<table>
<tbody>
<tr>
<td>
<div style="text-align: left;padding: 0 15px;">
<div style="height: 40px;"></div>
<p style="color:#555; margin: 5px 0px 0px; line-height:1; padding:0px; font-size:14px;">President</p>
<p style="color:color: #777; font-size: 12px; margin: 0px 0px; line-height:1; padding:0px;">CIO Association of Canada</p>
</div>
</td>
<td>
<div style="text-align: left; padding: 0 15px;"></div>
</td>
<td>
<div style="text-align: left;padding: 0 15px;">
<div style="height: 40px;"></div>
<p style="color:#555; margin: 5px 0px 0px; line-height:1; padding:0px; font-size:14px;">President</p>
<p style="color: #777; font-size: 12px; margin: 0px 0px; line-height:1; padding:0px;">TransformationWorx</p>
</div>
</td>
</tr>
</tbody>
</table>
<div style="text-align: left; padding: 0px 15px;">
<p style="color:#555; font-size:14px;">Issued on: <span style="padding: 2px 10px;min-width: 100px; border-bottom: 1px solid #333; display: inline-block; color: #333;">$issued</span></p>
</div>
<div style="text-align: right; padding: 0px 15px;">
<p style="color:#555; font-size:14px;">This certificate is valid for 3 years until: <span style="padding: 2px 10px;min-width: 100px; border-bottom: 1px solid #333; display: inline-block; color: #333;text-align: left;">$expired</span></p>
</div>
</div>
</div>
EOD;

	// Print text using writeHTMLCell()
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

	// ---------------------------------------------------------

	// Close and output PDF document
	// This method has several options, check the source code documentation for more information.

	// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
	$pdf->setFooterData(array(0,64,0), array(0,64,128));
	mkdir("certificates/{$req->hash}", 0755, true);
	$pdf->Output("/var/www/html/dltexams/certificates/{$req->hash}/Certificate.pdf", 'F');*/
?>
