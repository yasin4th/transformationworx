<?php
  require_once 'config.inc.test.php';
?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
		<ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="active">Disclaimer Terms</li>
        </ul>
        <!-- Start Disclaimer Terms BOX -->   
        <div class="row service-box">
          <div class="col-md-12 col-sm-12">
              <h2>Disclaimer Terms</h2>
          </div>
        </div>
		<div class="row service-box margin-bottom-40">
          <div class="col-md-12 col-sm-12 p-mrg-left-40 p">
			<p>The website www.exampointer.com is the sole property of ExamPointer.com Pvt Ltd, headquartered at B-32, Basement, Shivalik, Malviya Nagar, New Delhi – 110017. The copyright to all the content provided on this website belongs to ‘ExamPointer.com Pvt Ltd.’</p>
			<p>None of the Materials may be copied, reproduced, distributed, republished, downloaded, displayed, posted or transmitted in any form or by any means, including, but not limited to, electronic, mechanical, photocopying, recording, or otherwise, without the prior written permission of ‘ExamPointer.com Pvt Ltd.’</p>
			<p>ExamPointer has the full rights to change the contents of any programs, resources or any information provided on the website at its discretion. Further ExamPointer can change the prices, eligibility, period of offer, offers expiry or the contents of the products and services being offered on the website from time to time. All these changes, if made, will be applicable to all the registered and non-registered users of the website.</p>
      <p>ExamPointer do not take any legal responsibility for any errors or misrepresentations that might have crept in. We have tried and made our best efforts to provide accurate up to date information on the website. However we do make periodic changes in the website so as to provide most updated and errorless contents and information.</p>
      <p>The contents on the website have been provided with an intention of helping the students in their studies. The test patterns, question banks & papers, articles, Study Tips and Techniques, Interviews, other resources and services provided on the website do not take the responsibility of the failure of any student in any exam or elsewhere.</p>
      <p>The data as submitted by the student while registering on the website is the sole property of ExamPointer and the company can use this information to contact the student for education information through email, telephone and SMS, promotional mails/special offers from www.ExamPointer.co.in or any of its partner websites.</p>
      <p>If you do not understand and/ or agree to be bound by all Terms, do not use this Website. Your use of this Website at any time constitutes a binding agreement by you to abide by these Terms.</p>
      <p>‘ExamPointer.com Pvt Ltd.’ controls and operates this Website from its headquarters in Delhi, India and makes no representation that the materials on the website are appropriate or available for use in other locations. If you use this Website from other locations, you are responsible for compliance with applicable local laws, including but not limited to the export and import regulations of other countries. Unless otherwise explicitly stated, all marketing or promotional materials found on this Website are solely directed to individuals, companies or other entities located in India and comply with the laws prevailing for the time being in force in India. Disputes if any shall be subject to the exclusive jurisdiction of Courts at Delhi.</p>
			<p>ExamPointer, at any time, can cancel a user’s registration without providing any reasons. The website has been designed only for educational purpose, and in no way any student can misuse the services.</p>
			
          </div>
        </div>
        <!-- End Disclaimer Terms BOX -->

      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
    <!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>