<?php
include 'DB.php';

date_default_timezone_set('Asia/Kolkata');

$_answers = ['A','B','C','D','E'];
$db = new DB();

if(!isset($_GET['id'])) {
	dd("Please enter id...");
}
$filename = 'test'.$_GET['id'].'.json';

$string = file_get_contents("kd/paper/".$filename);
$qjson=json_decode($string, true);
$no = str_replace('.json', '', substr($filename, 4));

$string = file_get_contents("kd/answers/ans{$no}.json");
$ajson=json_decode($string, true);

$test_name = $ajson['data']['test_data']['test_name'];

$time = time();

/*$tag = $db->_insert('tags', [
	'tag' => $test_name,
	'description' => $test_name,
	'user_id' => 1,
	'created' => time(),
	'delete' => 0,
]);*/

/*$class = $db->_insert('classes', [
	'name' => $test_name,
	'description' => $test_name,
	'user_id' => 1,
	'created' => time(),
	'deleted' => 0,
]);*/

$class = '22';
pp("TEST NAME = ".$test_name);
if(preg_match('/(RRB)/i', $test_name)) {
	$subject_id = 47;
} else if(preg_match('/(SSC)/i', $test_name)) {
	$subject_id = 45;
} else if(preg_match('/(SBI|BANK|IBPS)/i', $test_name)) {
	$subject_id = 46;
} else {
	dd("No relevant subject found.");
}
pp("Subject ID: " . $subject_id);
$test_paper = $db->_insert('test_paper', [
	'name' => $test_name,
	'type' => '4',
	'class' => $class,
	'total_attempts' => 1,
	'instructions' => '',
	'start_time' => 0,
	'end_time' => 0,
	'user_id' => 1,
	'status' => 1,
	'created' => time(),
	'deleted' => 0,
]);

$test_paper_subjects = $db->_insert('test_paper_subjects', [
	'test_paper_id' => $test_paper,
	'subject_id' => $subject_id
]);


$passages = [];
// dd($qjson['passages']['EN']);
foreach ($qjson['passages']['EN'] as $key => $value) {
	$p = $db->_insert('questions', [
		'class_id' => $class,
		'question' => $value,
		'user_id' => 1,
		'created' => 0,
		'type' => 11,
		'delete' => 0
	]);
	array_push($passages, ['key'=>$key,'passage'=>$value,'id'=>$p]);
}

echo "Test Paper ID : ".$test_paper."<br>";

		// pp($qjson['qs']);
		// print_r($json['passages']);
		// print_r($json['hindi_font']);
		// print_r($json['subject_names']);
		// print_r($json['qordering']);
		// print_r($json['plt_windw']);
		// print_r($qjson['sub_dist']);
		foreach ($qjson['qs'] as $k => $section) {

			$section_id = $db->_insert('section',[
				'name' => $qjson['subject_names'][$k],
				'total_question' => count($section),
				'test_paper' => $test_paper,
			]);

			echo "Section ID : ".$section_id."<br>";


			$labels = getLabels($section, $section_id, $class, $test_paper, $db);

			// $tpl = $db->_insert('test_paper_labels', [
			// 	'section_id' => $section_id,
			// 	'class_id' => $class,
			// 	'subject_id' => 0,
			// 	'question_type' => 1,
			// 	'number_of_question' => count($section),
			// 	'max_marks' => 1,
			// 	'neg_marks' => 0
			// ]);

			$count = 0;
			$ps_id = 0;
			// dd($section);
			foreach ($section as $j => $q) {

				$question = $q['content']['EN']['q'];
				$question_id = $q['content']['EN']['q_id'];
				$passage_id = $q['content']['EN']['passage_id'];

				// pp($passages);
				// pp(getPassage($passage_id,$passages));
				// echo "passage id";
				// dd($passage_id);


				$q_id = $db->_insert('questions', [
					'class_id' => $class,
					'question' => $question,
					'description' => '',
					'type' => 1,
					'user_id' => 1,
					'created' => time(),
					'parent_id' => getPassage($passage_id,$passages),
					'delete' => 0,
				]);

				echo "Question ID : ".$q_id."  ";

				if($passage_id == 0) {
					$tpq = $db->_insert('test_paper_questions', [
						'test_paper_id' => $test_paper,
						'section_id' => $section_id,
						'lable_id' => getLabel($labels, $passage_id),
						'question_id' => $q_id,
						'max_marks' => getLabelPlus($labels, $passage_id),
						'neg_marks' => getLabelMinus($labels, $passage_id),
						'created' => time()
					]);
				} else {
					$tpq = $db->_iou('test_paper_questions', [
						'test_paper_id' => $test_paper,
						'section_id' => $section_id,
						'lable_id' => getLabel($labels, $passage_id),
						'question_id' => getPassage($passage_id,$passages),
						'max_marks' => getLabelPlus($labels, $passage_id),
						'neg_marks' => getLabelMinus($labels, $passage_id),
						'created' => 0
					],[
						'test_paper_id' => $test_paper,
						'section_id' => $section_id,
						'lable_id' => getLabel($labels, $passage_id),
						'question_id' => getPassage($passage_id,$passages),
						'max_marks' => getLabelPlus($labels, $passage_id),
						'neg_marks' => getLabelMinus($labels, $passage_id),
						'created' => 0
					]);
				}

				$options = $q['content']['EN']['opt'];

				$options = preg_replace("/[\n\r\t]/","",$options);

				// $options = preg_replace('/[\s]+/', '', $options);
				// preg_match_all('/<div class="opt-data">(.*)(<\/div>)?/im', $options, $matches);
				preg_match_all('/<div class="opt-data">(((?!div).)*)(?=(<\/div>))/im', $options, $matches);

				$ans = $ajson['data']['qinfo'][$question_id]['ans'];
				// pp($options);
				// pp($matches[1]);


				if(count($matches[1])==0) {
					pp("Options data = 0");
					pp($options);
					pp($matches);
				}

				foreach ($matches[1] as $key => $opt) {
					// pp($k);
					$o_id = $db->_insert('options', [
						'option' => $opt,//strip_tags($opt, '<img>'),
						'answer' => array_search($ans, $_answers) == $key,
						'number' => 1,
						'question_id' => $q_id,
						'user_id' => 1,
						'created' => time(),
						'deleted' => 0,
					]);
				}
			} // section
		} // test paper

?>
