<?php
		require_once 'config.inc.php';
		require_once 'config.inc.test.php';


	@session_start();
	if(isset($_SESSION['role']) && $_SESSION['role'] != '4')
	{
		@session_destroy();
	}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
		<?php include HEADER; ?>
		<!-- Navigation END -->

<section style="background-image:url('blade/main/images/team.jpg');
								background-size:100% 100%;
								background-repeat:no-repeat;
								height: 350px; position: relative">
		 <div class="overlay1">
			<div class="container">
				 <div class="row">
							 <div class="col-sm-12 col-md-12 col-xs-12 center packages">
									<h3 style="margin-top: 150px; color:white;"> Meet Our
									<span style="color:orange;">Team</span></h3>
									<p style="color:white; font-size: 14px; font-weight: 300">This platform works perfectly on any device like mobile, tab, laptop, desktop,   and on any platform like i.e Windows, Mac or Linux. This revolutionary product is a must for every institution who wants to make their pupil high achievers.</p>
								</div>
				 </div>
			</div>
			</div>
</section>
<div style="padding-top: 30px;"></div>
		<div class="main">
			<div class="container">
				<!-- <ul class="breadcrumb">
						<li><a href="<?=URI1?>">Home</a></li>
						<li class="active">Team</li>
				</ul> -->
				<!-- BEGIN SIDEBAR & CONTENT -->
				<div class="row margin-bottom-40">
					<!-- BEGIN CONTENT -->
					<div class="services-block">


							<br>
							<center>
									<section class="mrg-t-30">
											<div class="container">
											<div class="row mrg-sm">
													<div class="col-sm-12 col-md-4">
														<div class="employ_pic">
															<img src="blade/main/images/rohit.jpg" class="img-responsive">
														</div>
													</div>
												<div class="col-sm-12 col-md-8">
														<div class="row description">
																<div class="col-sm-12 col-md-12  employ_descr">
																	<h4>Rohit Pratap Singh</h4>
																</div>

																<div class="col-sm-12 col-md-12  employ_descr">
																	<h6><b>LOREM IPSUM</b></h6>
																</div>

																<div class="col-sm-12 col-md-12  employ_descr">
																	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type andit to make when an unknown printer took a galley of type andit to make a type when an unknown printer took a galley of type. a type </p>
																</div>
														</div>
												</div>
											</div>
											<div class="row teamdiv mrg-sm">
														<div class="col-sm-12 col-md-4">
															<div class="employ_pic">
																<img src="blade/main/images/diapk.jpg" class="img-responsive">
															</div>
														</div>

														<div class="col-sm-12 col-md-8">
															<div class="row description">
																	<div class="col-sm-12 col-md-12  employ_descr">
																		<h4>Deepak</h4>
																	</div>
																	<div class="col-sm-12 col-md-12  employ_descr">
																		<h6><b>LOREM IPSUM</b></h6>
																	</div>

																<div class="col-sm-12 col-md-12  employ_descr">
																		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type andit to make a type when an unknown printer took a galley of type andit to make a type when an unknown printer took a galley of type. </p>
																</div>
															</div>
														</div>
											</div>
											<div class="row teamdiv2 mrg-sm">
												<div class="col-sm-12 col-md-8">
													<div class="row description">
															<div class="col-sm-12 employ_descr2">
																<h4>Deepak</h4>
															</div>
															<div class="col-sm-12 employ_descr2">
																	<h6><b>LOREM IPSUM</b></h6>
															</div>
														 <div class="col-sm-12 employ_descr2" style="float: right;">
																<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type andit to make a type when an unknown printer took a galley of type andit to make a type when an unknown printer took a galley of type. </p>
														 </div>
													</div>
												</div>
												<div class="col-sm-11 col-md-4">
													<div class="employ_pic">
														<img src="blade/main/images/diapk.jpg" class="img-responsive">
													</div>
												</div>
											</div>
											<div class="row">
													<div class="col-sm-12 col-md-4">
														<div class="employ_pic">
															<img src="blade/main/images/Ajai.jpg" class="img-responsive">
														</div>
													</div>
													<div class="col-sm-12 col-md-8">
														<div class="row description">
																<div class="col-sm-12 col-md-12  employ_descr">
																	<h4>Ajay Maurya</h4>
																</div>
																<div class="col-sm-12 col-md-12  employ_descr">
																	<h6><b>LOREM IPSUM</b></h6>
																</div>
																<div class="col-sm-12 col-md-12  employ_descr">
																		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type andit to make a type when an unknown printer took a galley of type andit to make a type when an unknown printer took a galley of type. </p>
																</div>
														</div>
													</div>
											</div>
											<div class="row teamdiv">
												<div class="col-sm-12 col-md-4">
													<div class="employ_pic">
														<img src="blade/main/images/aria.jpg" class="img-responsive">
													</div>
												</div>
												<div class="col-sm-12 col-md-8">
													<div class="row description">
														<div class="col-sm-12 col-md-12  employ_descr">
															<h4>Arya</h4>
														</div>
														<div class="col-sm-12 col-md-12  employ_descr">
															<h6><b>LOREM IPSUM</b></h6>
														</div>
														<div class="col-sm-12 col-md-12  employ_descr">
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type andit to make a type when an unknown printer took a galley of type andit to make a type when an unknown printer took a galley of type. </p>
														</div>
													</div>
												</div>
											</div>
											<div class="row teamdiv2">
												<div class="col-sm-12 col-md-8">
													<div class="row description">
															<div class="col-sm-12 employ_descr2">
																<h4>Arya</h4>
															</div>
															<div class="col-sm-12 employ_descr2">
																<h6><b>LOREM IPSUM</b></h6>
															</div>
															<div class="col-sm-12 employ_descr2" style="float: right;">
																<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type andit to make a type when an unknown printer took a galley of type andit to make a type when an unknown printer took a galley of type. </p>
															</div>
													</div>
												</div>
												<div class="col-s11 col-md-4">
													<div class="employ_pic">
														<img src="blade/main/images/aria.jpg" class="img-responsive">
													</div>
												</div>
											</div>
									</section>
							</center>
					</div>
			<!-- END CONTENT -->
				</div>
			</div>
		</div>
	</section>
		<!-- BEGIN PRE-FOOTER -->
	<?php include FOOTER; ?>
		<!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
		<?php include('html/js-files.php'); ?>
		<!-- END PAGE LEVEL JAVASCRIPTS -->

</body>
<!-- END BODY -->
</html>