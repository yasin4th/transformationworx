<?php 
    require_once 'config.inc.test.php';

	@session_start();
    if (!isset($_SESSION['username']))
    {       
        header('Location: profile');        
    }
	include 'Access-API.php';	
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?=URI1?>">Home</a></li>
            <li class="active">Report Card</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          <!-- <div class="sidebar col-md-2 col-sm-3">
            <?php //include('html/student/sidebar.php'); ?>
          </div> -->
          <!-- END SIDEBAR -->

          <!-- BEGIN CONTENT -->
          <div class="col-md-12 col-sm-12">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2>Report Card</h2>
				</div>
				<div class="col-md-6 col-sm-6"></div>
				<div class="col-md-12 col-sm-12">
					<div id="percentage-score-spline-chart-target" style="height: 400px;"></div>
				</div>
				<div class="col-md-12 col-sm-12">
					<div class="table-scrollable">
						<table id="tests-table" class="table table-striped table-bordered table-hover hidden">
							<thead>
								<tr>
									<th>
										Test Paper Name
									</th>
									<th>
										No. Of Q
									</th>
									<th>
										Qu. Attempted
									</th>
									<th>
										Correct Qu.
									</th>
									<th>
										Net Score
									</th>
									<th>
										% Marks
									</th>
									<th>
										Speed
									</th>
									<!-- <th>
										Strike Rate
									</th>
									<th>
										EAS
									</th> -->
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="row mrg-bot20">
				<div class="col-md-12 margin-bottom-40">
					<!-- <form id="create-paper-form" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-3">
								<label>Test Program</label>
								<select id="select-test-programs" class="form-control form-group">
									<option value="0">Select Program Name</option>
								</select>
							</div>
							<div class="col-md-3" >
								<label>Select Test Type</label>
								<select id="select-test-type" class="form-control form-group">
									<option value="0">Select Test Type</option>
									<option value="1">Diagnostic Test</option>
									<option value="3">Chapter Test</option>
									<option value="4">Full Test</option>
									<option value="5">Scheduled Test</option>
								</select>
							</div>
							<div class="col-md-3" id="sub" style="display: none;">
								<label>Select Subject</label>
								<select id="select-subject" class="form-control form-group">
									<option value="0">Select Subject</option>
								</select>
							</div>
							<div class="col-md-2">
								<button type="submit" class="btn green pull-right create-report">Create</button>
							</div>
						</div>
					</form> -->


					<div class="portlet create-report-page">
						<div class="portlet-title">
							<div class="caption">

							</div>
							<!-- <div class="tools">
								<a href="javascript:;" class="collapse"></a>
							</div> -->
						</div>
						<div class="portlet-body form hide">
							<!-- <div class="col-md-12 col-sm-12 mrg-top20">
								<div class="actions pull-right">
									<div class="btn-group btn-group-devided" data-toggle="buttons">
										<a data-toggle="tab" href="#speed-div">
											<label class="btn btn-transparent green-haze btn-circle btn-sm active">
											<input type="radio" name="option" class="toggle hide" id="speed">Speed</label>
										</a>
										<a data-toggle="tab" href="#strike-rate-div">
											<label class="btn btn-transparent green-haze btn-circle btn-sm">
											<input type="radio" name="option" class="toggle hide" id="strike-rate">Strike Rate</label>
										</a>
										<a data-toggle="tab" href="#eas-div">
											<label class="btn btn-transparent green-haze btn-circle btn-sm">
											<input type="radio" name="option" class="toggle hide" id="eas">EAS</label>
										</a>
										<a data-toggle="tab" href="#percentage-score-div">
											<label class="btn btn-transparent green-haze btn-circle btn-sm">
											<input type="radio" name="option" class="toggle hide" id="percentage-score">Percentage Score</label>
										</a>										
										<a data-toggle="tab" href="#toppers-gap-div">
											<label class="btn btn-transparent green-haze btn-circle btn-sm">
											<input type="radio" name="option" class="toggle hide" id="toppers-gap">Toppers Gap</label>
										</a>
									</div>
								</div>
							</div> -->
							<div class="tab-content">
								<!-- start speed div -->
								<div id="speed-div" class="tab-pane active">
									<div class="col-md-12 mrg-bot20 mrg-top20">
										<h2>Speed</h2>
										<div id="site_activities_content" class="">
											<div id="speed-spline-chart-target" style="height: 400px;"></div>
										</div>
										<!-- <h4 class="text-center mrg-top10">Graphical Analysis of Part Test</h4> -->
									</div>
								</div>
								<!-- end speed div -->
								<!-- start strike-rate div -->
								<div id="strike-rate-div" class="tab-pane">
									<h2>Strike Rate</h2>
									<div id="strike-rate-spline-chart-target" style="height: 400px;"></div>
								</div>
								<!-- end strike-rate div -->
								<!-- start eas div -->
								<div id="eas-div" class="tab-pane">
									<h2>Effective Average Speed</h2>
									<div id="eas-spline-chart-target" style="height: 400px;"></div>
								</div>
								<!-- end eas div -->
								<!-- start percentage-score div -->
								<div id="percentage-score-div" class="tab-pane">
									<h2>Score %</h2>
									<div id="percentage-score-spline-chart-target" style="height: 400px;"></div>
								</div>
								<!-- end percentage-score div -->
								<!-- start success-gap div -->
								<!-- <div id="success-gap-div" class="tab-pane">
									<h2>Success Gap</h2>
									<div id="success-gap-spline-chart-target" style="height: 400px;"></div>
								</div> -->
								<!-- end success-gap div -->
								<!-- start toppers-gap div -->
								<div id="toppers-gap-div" class="tab-pane">
									<h2>Toppers Gap</h2>
									<div id="toppers-gap-spline-chart-target" style="height: 400px;"></div>
								</div>
								<!-- end toppers-gap div -->
								<div class="col-md-12 mrg-top20">
									<div class="margin-top-10 margin-bottom-10 table-scrollable">
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>



			</div>

			<!-- END PAGE CONTENT-->
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include('new-html/footer.php'); ?>
    <!-- END FOOTER -->

	<!-- /modal -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
	<?php include('html/student/js-files.php'); ?>


	<script src="assets/global/plugins/jquery.form.js"></script>

	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="admin/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
	<script src="admin/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
	<script src="admin/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
	<script src="admin/assets/global/plugins/highcharts/highcharts.js" type="text/javascript"></script>
	<script src="admin/assets/global/plugins/highcharts/modules/exporting.js" type="text/javascript"></script>

	<!-- END PAGE LEVEL PLUGINS -->

<script src="assets/js/custom/report-card.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
   // Index.initCharts(); // init index page's custom scripts
});
</script>

    <!-- END PAGE LEVEL JAVASCRIPTS -->

</body>
<!-- END BODY -->
</html>