<?php 
	header('Location: my-program.php');
?>
<?php include 'Access-API.php'; ?>
<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>

	<?php
	date_default_timezone_set('Asia/Calcutta');
	$today = date('d-m-Y');
	?>

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li><a href="#">Student</a></li>
            <li class="active">Study Planner</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          <div class="sidebar col-md-2 col-sm-3">
            <?php include('html/student/sidebar.php'); ?>
          </div>
          <!-- END SIDEBAR -->
          <!-- BEGIN CONTENT -->
          <div class="col-md-10 col-sm-9">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2>Study Planner</h2>
				</div>
				<div class="col-md-6 col-sm-6">
					<a href="#large" data-toggle="modal" onclick="changeHeaders()" class="btn green pull-right" data-backdrop="static"><i class="fa fa-plus-circle"></i> &nbsp;Add Subject</a>
				</div>
			</div>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="portlet box green-meadow calendar">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Calendar
							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-3 col-sm-12" id="subjectPlanned">
									<!-- BEGIN DRAGGABLE EVENTS PORTLET-->
									<!--<h3 class="event-form-title">Draggable Events</h3>
									<div id="external-events">
										<form class="inline-form">
											<input type="text" value="" class="form-control" placeholder="Event Title..." id="event_title"/><br/>
											<a href="javascript:;" id="event_add" class="btn default">
											Add Event </a>
										</form>
										<hr/>
										<div id="event_box">
										</div>
										<label for="drop-remove">
										<input type="checkbox" id="drop-remove"/>remove after drop </label>
										<hr class="visible-xs"/>
									</div>-->
									<!-- END DRAGGABLE EVENTS PORTLET-->

								</div>
								<div class="col-md-9 col-sm-12">
									<div id="calendar" class="has-toolbar">
									</div>
								</div>
							</div>
							<!-- END CALENDAR PORTLET-->
						</div>
					</div>
                </div>
              </div>
			</div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
    <!-- END FOOTER -->
	<!-- modal -->
		<!--<div id="large" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">-->
	<div class="modal fade bs-modal-lg modal-scroll" id="large" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title control-label"><i class="fa fa-edit"></i> <strong>Create Plan</strong></h4>
				</div>
				<div class="modal-body">
					<!-- BEGIN PAGE CONTENT-->
					<div class="row">
						<div class="col-md-12">
							<div class="portlet" id="form_wizard_1">
								<div class="portlet-body form">
									<form action="#" class="form" id="submit_form" method="POST">
										<div class="form-wizard">
											<div class="form-body">
												<ul class="nav nav-pills nav-justified steps bor-bot-org">
													<li class="active">
														<a href="#tab1" data-toggle="tab" class="step">
														<span class="number">
														1 </span>
														<span class="desc">
														<i class="fa fa-check"></i> Plan Creation </span>
														</a>
													</li>
													<li>
														<a href="#tab2" data-toggle="tab" class="step">
														<span class="number">
														2 </span>
														<span class="desc">
														<i class="fa fa-check"></i> Time Slot Settings </span>
														</a>
													</li>
													<li>
														<a href="#tab3" data-toggle="tab" class="step active">
														<span class="number">
														3 </span>
														<span class="desc">
														<i class="fa fa-check"></i> Chapter Settings </span>
														</a>
													</li>
												</ul>
												<div class="tab-content">
													<div class="alert alert-danger display-none">
														<button class="close" data-dismiss="alert"></button>
														You have some form errors. Please check below.
													</div>
													<div class="alert alert-success display-none">
														<button class="close" data-dismiss="alert"></button>
														Your form validation is successful!
													</div>
													<div class="tab-pane active" id="tab1">
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label">Select Class</label>
																	<div class="row" id="classChanger">
																		<div class="col-md-12">

																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-6">

															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label">Select Subject</label>
																	<div class="row" id="subjectChanger">
																		<div class="col-md-12">

																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-6">

															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label">Start Date</label>
																	<div class="row">
																		<div class="col-md-12">
																			<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-viewmode="years" data-date-start-date="<?php echo $today; ?>">
																				<input type="text" class="form-control" readonly id="startDate" name="startDate">
																				<span class="input-group-btn">
																				<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
																				</span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label">Select Target Date</label>
																	<div class="row">
																		<div class="col-md-12">
																			<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-viewmode="years" data-date-start-date="<?php echo $today; ?>">
																				<span class="input-group-btn">
																				<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
																				</span>
																				<input type="text" class="form-control" readonly id="targetDate" name="targetDate">
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label">Off Day &nbsp;<i class="fa fa-question-circle popovers" data-trigger="hover" data-content="Where student will not study at all. E.g. Family Function, Exam Days, Vacation, Birthday, Festivals etc." data-original-title="Off Day"></i></label>
																	<div class="row">
																		<div class="col-md-12">
																			<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-viewmode="years" data-date-multidate="true" data-date-autoclose="false" data-date-start-date="<?php echo $today; ?>">
																				<input type="text" class="form-control" readonly id="offDay" name="offDay">
																				<span class="input-group-btn">
																				<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
																				</span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label">School Holiday &nbsp;<i class="fa fa-question-circle popovers" data-trigger="hover" data-content="This is the day where student will study for more hours than the regular school day. This can include Sunday or any other School Holiday like summer vacation, Deepawali Holidays or other holidays" data-original-title="School Holiday" style="z-index:12050;"></i></label>
																	<div class="row">
																		<div class="col-md-12">
																			<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-viewmode="years" data-date-multidate="true" data-date-autoclose="false" data-date-start-date="<?php echo $today; ?>" data-date-days-of-week-highlighted="0">
																				<span class="input-group-btn">
																				<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
																				</span>
																				<input type="text" class="form-control" readonly id="schoolHoliday" name="schoolHoliday">

																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!--<div class="row">
															<div class="col-md-12">
																<label class="control-label">Add your Family Event or functions if any &nbsp; <i class="fa fa-question-circle popovers" data-container="body" data-trigger="hover" data-placement="left" data-content="To add multiple chapter under the selected subject & unit" data-original-title="Add Multiple Chapter"></i>
																<i>( This date will skipped from Study planner)</i></label>
															</div>
															<div class="col-md-5">
																<div class="form-group">
																	<div class="row">
																		<div class="col-md-12">
																			<input type ="text" class="form-control" placehoplder="Event Name"/>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-5">
																<div class="form-group">
																	<div class="row">
																		<div class="col-md-12">
																			<div class="input-group input-medium date date-picker" data-date="today" data-date-format="dd-mm-yyyy" data-date-viewmode="years" data-date-multidate="true" data-date-autoclose="false">
																				<span class="input-group-btn">
																				<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
																				</span>
																				<input type="text" class="form-control" readonly>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-2">
																<div class="form-group">
																	<a class="btn btn-circle btn-icon-only blue" href="#" style="margin: 25px 0px 0px;">
																		<i class="fa fa-plus"></i>
																	</a>
																</div>
															</div>
														</div>-->
														<!--<div class="row">
															<div class="col-md-12 form-group">
																<label class="control-label">Do Not Include this day in calender</label>
																<div class="input-group">
																	<div class="icheck-inline">
																		<label>
																		<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey"> Sun </label>
																		<label>
																		<input type="checkbox" checked class="icheck" data-checkbox="icheckbox_square-grey"> Mon </label>
																		<label>
																		<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey"> Tue </label>
																		<label>
																		<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey"> Wed </label>
																		<label>
																		<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey"> Thue </label>
																		<label>
																		<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey"> Fri </label>
																		<label>
																		<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey"> Sat </label>
																	</div>
																</div>
															</div>
														</div>-->

														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<div class="radio-list">
																		<label class="radio-inline">
																		<input type="radio" name="optionsRadios" class="dailyOrAlternate" value="daily" id="daily"> Daily </label>
																		<label class="radio-inline">
																		<input type="radio" name="optionsRadios" class="dailyOrAlternate" value="alternate" id="alternate"> Alternate Day </label>
																	</div>
																</div>
															</div>
															<div class="col-md-6">

															</div>
														</div>

													</div>
													<div class="tab-pane" id="tab2">
														<div class="form-group">
															<div class="row light-grey">
																<div class="col-md-6">
																		<label class="control-label">Recommended average Time/Day :</label>
																</div>
																<div class="col-md-6">
																	<label class="control-label text-success" id="recommendedTime"></label>
																</div>
															</div>
														</div>
														<label class="control-label">Select time slot in school days study</label>
														<div class="row" id="schoolDaysSlots">
														<div class="row" id="schoolDaysSlotsRow_0">
															<div class="col-md-10">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label">From</label>
																	<div class="row">
																		<div class="col-md-12">
																			<div class="input-group">
																				<input type="text" class="form-control timepicker timepicker-no-seconds" data-minute-step="30" id="schoolDaysFrom_0">
																				<span class="input-group-btn">
																				<button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
																				</span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label">To</label>
																	<div class="row">
																		<div class="col-md-12">
																			<div class="input-group">
																				<input type="text" class="form-control timepicker timepicker-no-seconds" data-minute-step="30" id="schoolDaysTo_0">
																				<span class="input-group-btn">
																				<button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
																				</span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															</div>

															<div class="col-md-2">
																<div class="form-group">
																	<a class="btn btn-circle btn-icon-only blue" href="#" onclick="addSchoolDaysSlots()" style="margin: 25px 0px 0px;">
																		<i class="fa fa-plus"></i>
																	</a>
																	<a class="btn btn-circle btn-icon-only blue" href="#"  id="schoolDaysSlotsSubtract" onclick="deleteSchoolDaysSlots()" style="margin: 25px 0px 0px 10px;">
																		<i class="fa fa-minus"></i>
																	</a>
																</div>
															</div>
														</div>
														</div>
														<label class="control-label">Select time slot in school holiday days study</label>
														<div class="row" id="schoolHolidaysSlots">
														<div class="row" id="schoolHolidaysSlotsRow_0">
															<div class="col-md-10">
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="control-label">From</label>
																		<div class="row">
																			<div class="col-md-12">
																				<div class="input-group">
																					<input type="text" class="form-control timepicker timepicker-no-seconds" data-minute-step="30" id="schoolHolidaysFrom_0">
																					<span class="input-group-btn">
																					<button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
																					</span>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="control-label">To</label>
																		<div class="row">
																			<div class="col-md-12">
																				<div class="input-group">
																					<input type="text" class="form-control timepicker timepicker-no-seconds" data-minute-step="30" id="schoolHolidaysTo_0">
																					<span class="input-group-btn">
																					<button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
																					</span>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

															<div class="col-md-2">
																<div class="form-group">
																	<a class="btn btn-circle btn-icon-only blue" href="#" onclick="addSchoolHolidaysSlots()" style="margin: 25px 0px 0px;">
																		<i class="fa fa-plus"></i>
																	</a>
																	<a class="btn btn-circle btn-icon-only blue" href="#" id="schoolHolidaysSlotsSubtract" onclick="deleteSchoolHolidaysSlots()" style="margin: 25px 0px 0px 10px;">
																		<i class="fa fa-minus"></i>
																	</a>
																</div>
															</div>
														</div>
														</div>
													</div>
													<div class="tab-pane" id="tab3">
														<div class="form-group">
															<div class="row light-grey">
																<div class="col-md-6">
																		<label class="control-label">Total Available Hours :</label>
																</div>
																<div class="col-md-6">
																	<label class="control-label text-success" id="totalAvailableHours"></label>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="table-scrollable">
																	<table class="table table-striped table-bordered table-advance table-hover last-td" id="chapterRecommendedTime">
																	<thead>
																	<tr>
																		<th>
																			 Chapter
																		</th>
																		<th>
																			 Rec Time
																		</th>
																		<th>
																			 Allotted Time
																		</th>
																		<th>
																			 LC
																		</th>
																		<th>
																			 PQ
																		</th>
																		<th>
																			 TR
																		</th>
																	</tr>
																	</thead>
																	<tbody id="sortable">

																	</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div>

												</div>
											</div>
											<div class="form-actions">
												<div class="row">
													<div class="col-md-12">
														<a href="#" onclick="handleBack(0)" class="btn default button-previous pull-Left" id="myButtonBack">
														<i class="m-icon-swapleft"></i> Back </a>
														<a href="#" onclick="findingHours(0);fetchPlanChaptersTime(0);" class="btn grey-cascade button-next pull-right" id="myButtonContinue">
														Continue <i class="m-icon-swapright m-icon-white"></i>
														</a>
														<!-- <a href="#" onclick="" class="btn red button-next pull-left" id="myButtonContinue">
														Delete <i class="m-icon-trash m-icon-white"></i>
														</a> -->
														<a href="#" id="submitButton" onclick="populateCalendar(0)" class="btn green button-submit pull-right">
														Submit <i class="m-icon-swapright m-icon-white"></i>
														</a>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>

					<!-- END PAGE CONTENT-->
				</div>
			</div>
		</div>
	</div>
	<!-- modal 12 -->
	<!--<div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header has-success">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title control-label"><i class="fa fa-edit"></i> <strong>Create Plan</strong></h4>
				</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="table-scrollable">
							<table class="table table-striped table-bordered table-advance table-hover last-td">
							<thead>
							<tr>
								<th>
									 Chapter
								</th>
								<th>
									 Rec Time
								</th>
								<th>
									 Allotted Time
								</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>
									Chapter 1
								</td>
								<td>
									 10
								</td>
								<td>
									 <input type="text" class="form-control input-sm">
								</td>
							</tr>
							<tr>
								<td>
									Chapter 2
								</td>
								<td>
									 30
								</td>
								<td>
									 <input type="text" class="form-control input-sm">
								</td>
							</tr>
							<tr>
								<td>
									Chapter 3
								</td>
								<td>
									 30
								</td>
								<td>
									 <input type="text" class="form-control input-sm">
								</td>
							</tr>
							<tr>
								<td>
									Chapter 4
								</td>
								<td>
									 20
								</td>
								<td>
									 <input type="text" class="form-control input-sm">
								</td>
							</tr>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-arrow-left"></i> &nbsp; Back</button>
				<a href="#" data-toggle="modal" class="btn green"><i class="fa fa-edit"></i> &nbsp; Create plan</a>
			</div>
		</div>
	</div>-->
	<!-- /modal -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
    <?php include('html/student/js-files.php'); ?>




	<script>
	$(function() {
		$( "#sortable" ).sortable({
			revert: true
		});
	});
	</script>
	<script src="assets/js/custom/planner.js" type="text/javascript"></script>

    <!-- END PAGE LEVEL JAVASCRIPTS
</body>
<!- END BODY -->
</html>