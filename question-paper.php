<?php include 'Access-API.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Start Header -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 question-header">
				<div class="row">
					<div class="col-md-3 col-sm-3">
						<h4 class="TestName"><strong><!-- SBIPO-01 --></strong></h4>
					</div>
					<div class="col-md-9 col-sm-9">
						<h3 class="TestOrganiser"><strong><!--  Aiets-TestEngine --></strong></h3>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 padd15">
				<div class="col-md-12 border-bbb mrg15">
					<h5 style="text-align:center;"><strong>Please read the following intructions carefully</strong></h5>
					<p><strong>Total Number of Questions :</strong> <lable id="total-test-paper-question"><!-- 100  --></lable><br><strong>Total Time Available : </strong><lable id="test_paper_time"><!-- 1 Hour --></lable></p>
					<div class="margin-top-10 margin-bottom-10 table-scrollable">
						<table id="sections" class="table table-bordered table-striped table-hover">
						<thead>
						<tr>
							<th>
								<div class="padd5">
									Section #
								</div>
							</th>
							<th>
								<div class="padd5">
									 Section Name
								</div>
							</th>
							<th>
								<div class="padd5">
									 Total Number of Questions
								</div>
							</th>
							<th>
								<div class="padd5">
									Cut Off Marks
								</div>
							</th>
							<th>
								<div class="padd5">
									 Marks For Correct Answer
								</div>
							</th>
							<th>
								<div class="padd5">
									 Negative Marking For Wrong Answer
								</div>
							</th>
						</tr>
						</thead>
						<tbody>
						<!-- 
						<tr>
							<td>
								<div class="padd5">
									 3
								</div>
							</td>
							<td>
								<div class="padd5">
									 Logical Reasoning
								</div>
							</td>
							<td>
								<div class="padd5">
									 35
								</div>
							</td>
							<td>
								<div class="padd5">
									 35
								</div>
							</td>
							<td>
								<div class="padd5">
									 1
								</div>
							</td>
							<td>
								<div class="padd5">
									 -0.25
								</div>
							</td>
						</tr>
						-->
						</tbody>
						</table>
					</div>
					<div class="row">
						<div class="col-md-12 table-border0">
						<div class="margin-top-10 margin-bottom-10 table-scrollable">
							<div class="col-md-12 badge-padd">
								<div class="padd5">
									<span class="badge btn-circle badge-white"> 1 </span> You have not visited the Question.
							 	</div>
								<div class="padd5">
							 		<span class="badge btn-circle badge-danger"> 2 </span> Current Question.
							 	</div>
								<div class="padd5">
							 		<span class="badge btn-circle badge-success"> 3 </span> You have Answered the Question.
							 	</div>
								<div class="padd5">
							 		<span class="badge btn-circle btn-warning"> 4 </span> You have  Marked for Review.
							 	</div>
								<!-- <div class="padd5">
							 		<span class="badge btn-circle badge-purple"> 5 </span> Answered but Marked for Review.
							 	</div> -->
							</div>	
							<!-- <table class="table badge-padd" style="min-width:500px;">
								<tbody>
									<tr>
										<td>
											 <span class="badge btn-default"> 1 </span>
											 You have not visited the Question.
										</td>										
									</tr>
									<tr>
										<td>
											 <span class="badge red"> 2 </span>
											 You have not Answered the Question.
										</td>										
									</tr>
									<tr>
										<td>
											 <span class="badge badge-success"> 3 </span>
											 You have Answered the Question.
										</td>										
									</tr>
									<tr>
										<td>
											 <span class="badge btn-warning"> 4 </span>
											 Not Answered and Marked for Review.
										</td>										
									</tr>
									<tr>
										<td>
											<span class="badge purple"> 5 </span>
											 Answered but Marked for Review.
										</td>										
									</tr>
								</tbody>
							</table> -->

							<h4 class="question-paper-intruction-heading">
								<span>General Instructions</span><br>
								Please read the following instructions very carefully.
							</h4>
							
								<div class="well">
									<div id="test_paper_instructions"> </div>
								</div>
										
						</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<a class="btn default StartTest" id="launch-question-paper" onclick="$(this).attr('disabled','disabled');window.open( 'launch.php', 'Test Paper', 'location=no,scrollbars=yes,status=no,toolbar=yes,resizable=yes' )"> Start Test </a>
					<a class="btn default" href="test-papers.php?program_id=<?=$_GET['program_id']?>">Close Test</a>
				</div>
			</div>
			<!-- <div class="col-md-2 text-center">
				<img src="assets/frontend/layout/img/img12 copy.png" class="user-dp img-responsive padd20 Profile-mrg50per" /> -->
				<!--<i class="fa fa-user" style="margin-top:200px; font-size:200px"></i>-->
				<!-- <h5 class="mrg-top5"><strong>Your photo appears here</strong></h5>
			</div> -->
		</div>
	</div>
	

	<div id="loader" style="width: 100%; height: 100%; position: fixed; top: 0px; left: 0px; z-index: 10000; display: none; background: rgba(0, 0, 0, 0.309804);">
		<img src="assets/global/img/ajax-loader.gif" style="position: fixed;
		top: 50%;
		left: 50%;">
	</div>
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
    <script src="admin/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/custom/question-paper.js"></script>
    <script type="text/javascript">
	    /*
		* function to show ajax loader
		*/
		$(document).ajaxStart(function(){
		    $('#loader').show();
		});

		/*
		* function to hide ajax loader
		*/
		$(document).ajaxComplete(function(){
		    $('#loader').hide();
		});
	
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
	<a class="btn default StartTest" id="launch-question-paper1" onclick="window.open( 'launch.php', 'Test Paper', 'location=no,scrollbars=yes,status=no,toolbar=yes,resizable=yes' )" style="display: none;"></a>

</body>
<!-- END BODY -->
</html>
