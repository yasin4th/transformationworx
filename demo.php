<?php
    require_once 'config.inc.test.php';
    header('Location: '.URI1);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
<script language="JavaScript">
	window.onload = maxWindow;
	function maxWindow()
	{
		window.moveTo(0,0);
		top.window.resizeTo(screen.availWidth,screen.availHeight);
		if (document.all)
		{
		}
		else if (document.layers||document.getElementById)
		{
			if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth)
			{
			top.window.outerHeight = screen.availHeight;
			top.window.outerWidth = screen.availWidth;
			}
		}
	}
	document.onkeydown=function(e)
	{	
		if(e.which == 73 || e.which == 17 || e.which == 16 )
		{
			return false;
		}

	}
</script>
	
	<?php include('html/head-tag.php'); ?>
	<style type="text/css">
	ul{
		list-style-type: none;
	}
	</style>
	<script src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript"></script>
	<script type="text/javascript">
		document.onselectstart = new Function("return false");
	</script>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Start Header -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 question-header">
				<div class="row">
					<div class="col-md-3 col-sm-3">
						<h4 class="TestName"><strong class="test-paper-name"><!-- SBIPO-01 --></strong></h4>
					</div>
					<div class="col-md-9 col-sm-9">
						<h3 class="TestOrganiser"><strong> <!-- Aiets-TestEngine --></strong></h3>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9 padd15">
				<div class="col-md-12 border-bbb mrg15">
					<label><strong>Sections</strong></label><br>
					<div id="sections" style="min-height: 44px;">
						<!-- <button type="button" class="btn blue mrg-bot5">English Language</button>
						<button type="button" class="btn btn-info mrg-bot5">Quantitative Apptitude</button>
						<button type="button" class="btn btn-info mrg-bot5">Logical Reasoning</button> -->
					</div>
				</div>
				<div id="questions" class="col-md-12 border-bbb" style="min-height:400px;">
					<!-- <div class="from-group">
						<label><strong>Question No 1</strong></label><br>
					</div>
					<div class="col-md-12">
						<div class="row border-top-bbb">
							<div class="col-md-12 border-right-bbb mrg15">
								
								<h5><strong>The narrator describes the pigeon as a 'revolting bird' because</strong></h5>
								<div class="radio-list">
									<label>
									<input type="radio" name="optionsRadios" id="optionsRadios22" value="option1"> he could not fly </label>
									<label>
									<input type="radio" name="optionsRadios" id="optionsRadios22" value="option1"> he had to be carried everywhere </label>
									<label>
									<input type="radio" name="optionsRadios" id="optionsRadios22" value="option1"> he had wrinkled skin covered with yellow feathers </label>
									<label>
									<input type="radio" name="optionsRadios" id="optionsRadios22" value="option1"> he was fat </label>
								</div>
							</div>
						</div>
						
					</div> -->
				</div>

				<div class="col-md-12 save-btnBox border-bbb">
					<div class="padd7">
						
						<div class="width100per">
							<a id="btn-review" class="btn blue mrg-bot5">Mark For Review & Next</a>
						</div>
						<div class="width100per">
							<a id="clear" class="btn blue mrg-bot5">Clear Response</a>
						</div>
						<div class="width100perR">
							<a id="save" class="btn blue mrg-bot5">Save & Next</a>
						</div>
					</div>
				</div>
				
				
			</div>
			<div class="col-md-3">
				<div class="row">
					<div class="col-md-12">
						<!-- <div class="col-md-6 col-xs-6 text-center">
							<img src="assets/frontend/layout/img/img12 copy.png" class="img-responsive padd20" />
							<i class="fa fa-user profile-icon"></i>
							<p style="margin-top: 3px; font-size: 9px;"><strong>Your photo appears here</strong></p>
						</div> -->
						<div class="col-md-12 col-xs-12 padd-top20">
							<p><strong id="countdown-timer" class="countdown-timer"></strong></p>
								<!-- 							<p><a class="deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-pause"></i></a> <strong>Pause Test</strong></p>
								 -->
						</div>
					</div>
				</div>
				<div class="col-md-12 QuestionPalette-bg">
					<!-- <p>You are viewing <strong>EL</strong> section</p> -->
					<p>Question Palette:</p>
					<div class="row">
						<div class="form-group">
							<div class="scroller QuestionPalette-height" data-always-visible="1" data-rail-visible="0">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-2"></div>
										<div id="question-numbering" class="col-md-8 mrg-bot5 text-center">
											<!-- <a class="deleteOption btn btn-circle btn-icon-only red" style="margin-bottom: inherit;">1</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">2</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">3</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">4</a>

											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">17</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">18</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">19</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">20</a>

											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">21</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">22</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">23</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">24</a> -->
										</div>
										<div class="col-md-2"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="legend-div form-group">
								<p><strong>Legend</strong></p>
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<span><a class="deleteOption btn btn-circle btn-icon-only btn-default"></a> Not Visited</span>										
									</div>
									<div class="col-md-6 col-sm-6">
										<span><a class="deleteOption btn btn-circle btn-icon-only green"></a> Answered</span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6 col-sm-6">										
										<span><a class="deleteOption btn btn-circle btn-icon-only btn-warning"></a> Marked</span>
									</div>
									<div class="col-md-6 col-sm-6">
										<span><a class="deleteOption btn btn-circle btn-icon-only purple"></a> Marked and Saved</span>
									</div>
								</div>
								<!-- <div class="row">
									<div class="col-md-6 col-sm-6">
										<span><a class="deleteOption btn btn-circle btn-icon-only btn-default"></a> Not Visited</span>
									</div>
									<div class="col-md-6 col-sm-6">
										
									</div>
								</div> -->
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<div class="btn-group-justified">
												<a class="btn blue" href="#all-questions-modal" data-toggle="modal" id="show-all-questions-modal"> Question Paper </a>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<div class="btn-group-justified">
												<a class="btn blue" href="#instructions-modal" data-toggle="modal">Instructions</a>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<!-- <div class="col-md-6">
										<div class="form-group">
											<div class="btn-group-justified">
												<a class="btn btn-info">Profile</a>
											</div>
										</div>
									</div> -->
									<div class="col-md-12">
										<div class="form-group">
											<div class="btn-group-justified">
												<a data-toggle="modal" href="#submit-div" id="submit" class="btn blue">Submit</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- /.modal -->
	<div class="modal fade bs-modal-lg" id="all-questions-modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Question Paper</h4>
				</div>
				<div class="modal-body questions">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	<!-- Start Instructions modal -->
	<div class="modal fade bs-modal-lg" id="instructions-modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Instructions</h4>
				</div>
				<div class="modal-body">
					<h4 class="question-paper-intruction-heading">
						<span>General Instructions</span><br>
						Please read the following instructions very carefully.
					</h4>
					<div id="test_paper_instructions"> </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- End Instructions modal -->

	<!-- start submit modal -->
	<div class="modal fade bs-modal-lg" id="result-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header ">
					<!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button> -->
					<h4 class="modal-title">Your Score</h4>
				</div>
				<div class="modal-body">
					<h3> 
					   Please Wait while result is generating...
					</h3>
					
					<div id="result-progressbar-div" class="progress" style="display: none;">
						<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">
					    	<h4> Please Wait... </h4>
						</div>
					</div>
					
					<div id="result-marks-div" style="display: none;">
						<h2 class="text-center">You Got <span id="marks-archived"></span></h2>
						<!-- <h3  class="optmizer text-center">Do you want to optimize this test ?</h3> -->
					</div>

				</div>
				<div class="modal-footer">
					<!-- <a id="create-test-optmizer" class="optmizer btn blue" disabled="true"> Create Optmizer </a> -->
					<a id="go-to-test-history" title="Go to Result Details" href="#" class="btn default" disabled="true"> See Results </a>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- end submit modal -->
	
	<?php include('html/js-files.php'); ?>

	<!-- <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
	<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
	<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
	<script src="assets/frontend/layout/scripts/layout.js" type="text/javascript"></script>
	<script src="admin/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
	<script src="assets/js/common.js" type="text/javascript"></script> -->
	<script src="admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="admin/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>	
    <script src="assets/global/plugins/hashes.min.js" type="text/javascript"></script>	
	<script src="assets/global/plugins/countdown/jquery.countdown.min.js" type="text/javascript"></script>
	<script src="assets/js/custom/demo/demo-class.js" type="text/javascript"></script>
	<script src="assets/js/custom/demo/demo.js" type="text/javascript"></script>

</body>

</html>