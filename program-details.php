<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="active">Package Details</li>
        </ul>
        <div class="row margin-bottom-40">
          <!-- BEGIN CONTENT -->
          <div class="col-md-12">
            <div class="content-page">
              <div class="row">
                <div class="col-md-3 col-sm-3">
                  <aside class="courses-filters mrg-top40">
                    <div class="courses-filters-section well">
                      <div class="form-group">
                        <div class=""><h3>Filter Your Packages</h3></div>  
                      </div>
                      <div class="row">
                        <form role="form" id="filter">
                          <div class="col-md-12">
                            <div class="form-group">
                              <div class="input-group">
                                <input type="text" class="form-control" id="keyword" placeholder="Search Category" >
                                <span class="input-group-btn">
                                  <button type="submit" class="btn grey-cascade">
                                    <span class="fa fa-search"></span>
                                  </button>  
                                </span>
                              </div> 
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <ul class="ul-category-filter" id="category-filter">
                                <li>
                                  <div class="checkbox-list">
                                    <label><input type="checkbox"> All Categories </label>
                                  </div>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </form> 
                      </div>
                    </div>
                  </aside>
                </div>
				  
                <div class="col-md-9 col-sm-9">
                  <h1>Package Details</h1>
                  <div class="border-bbb padd15">
                    <div class="row">
                      <div class="col-md-4 form-group">
                        <img src="assets/frontend/pages/img/works/course1.jpg" alt="Amazing Project" class="img-responsive image img-circle" style="width: 248px; height: 248px;">
                      </div>
                      <div class="col-md-8 form-group">
                        <h2><strong class="program-name">Course 1</strong></h2>
                        <p class="text-justify description"></p>
                      </div>
                      <!-- <div class="row">
                        <div class="col-md-4 form-group">
                          <button class="btn btn blue"><i class="fa fa-inr"></i> 250</button>
                          <button type="button" class="btn blue"> Buy Now </button>
                        </div> -->
                      <div class="col-md-8 col-md-offset-2 form-group">
                        <h4><strong>Number of Tests</strong></h4>
                        <div class="table-scrollable" id="modal-table">
                          <table class="table table-striped table-hover table-bordered">
                            <thead>
                              <tr>
                                <th>Test Topic</th>
                                <th>No. of Tests</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Test Topic 1</td>
                                <td>5</td>
                              </tr>
                              <tr>
                                <td>Test Topic 2</td>
                                <td>10</td>
                              </tr>

                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div class="col-md-2"></div>
                      <div class="col-md-12 col-sm-12">
                        <span class="pull-left price">CAD 350</span>
                        <div class="pull-right">
                          <button type="button" class="btn btn-primary buy-program"> Buy Now </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <!-- END CONTENT -->
        </div>
      </div>
	  </div>


    <!-- BEGIN PRE-FOOTER -->
	<?php include('new-html/footer.php'); ?>
    <!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
    <!-- END PAGE LEVEL JAVASCRIPTS -->

</body>
<!-- END BODY -->
</html>