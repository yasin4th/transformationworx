<?php
  require_once 'config.inc.test.php';

  @session_start();
  if(isset($_SESSION['role']) && $_SESSION['role'] != '4')
  {
	if (!isset($_SESSION['username']))
	{
		header('Location: profile');
	}
  }
?>
<?php
  @session_start();
  if(isset($_SESSION['role']) && $_SESSION['role'] != '4')
  {
	@session_destroy();
  }
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <?php include('html/head-tag.php'); ?>
  <link rel='stylesheet' id='thim-css-style-css' href='assets/demo/custom-style.css' type='text/css' media='all' />
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
	<?php include HEADER; ?>

	<!-- Navigation END -->
	<div class="main">
	  <div class="container">
		<ul class="breadcrumb">
			<li><a href="<?=URI1?>">Home</a></li>
			<li class="active">Exams</li>
		</ul>
		<!-- BEGIN SIDEBAR & CONTENT -->
		<div class="row margin-bottom-40">
		  <!-- BEGIN CONTENT -->
		  <div class="col-md-3 col-sm-3">
			<aside class="courses-filters">
			  <div class="courses-filters-section well">
				<div class="form-group">
				  <div class=""><h3>Filter Your Packages</h3></div>
				</div>
				<div class="row">
				  <form role="form" id="filter">
					<div class="col-md-12">
					  <div class="form-group">
						<div class="input-group">
						  <input type="text" class="form-control" id="keyword" placeholder="Search Category" >
						  <span class="input-group-btn">
							<button type="submit" class="btn grey-cascade">
							  <span class="fa fa-search"></span>
							</button>
						  </span>
						</div>
					  </div>
					</div>
					<div class="col-md-12">
					  <div class="form-group">
						<ul class="ul-category-filter" id="category-filter">
						  <li>
							<div class="checkbox-list">
							  <label><input type="checkbox"> All Categories </label>
							</div>
						  </li>
						</ul>
						<!-- <select class="form-control" id="classes">
						  <option value="0"> Select Class </option>
						  <option value="1"> Class 12 </option>
						  <option value="2"> Class 10 </option>
						</select> -->
					  </div>
					</div>
				  </form>
				</div>
			  </div>
			</aside>
		  </div>
		  <div class="col-md-9 col-sm-9">
			<div id="programs-container" class="row margin-bottom-20">

			</div>
			<div class="col-md-6">
			  <ul class="question-table-pagination pagination pull-right"></ul>
			</div>
		  </div>
		  <!-- END CONTENT -->
		</div>
	  </div>
	</div>
	<!-- /.modal -->
	<div class="modal fade" id="program-details-modal" tabindex="-1" role="basic" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title">Program Details</h4>
		  </div>
		  <div class="modal-body">
			<div class="row">
			  <div class="col-md-4 form-group">
				<img src="assets/frontend/pages/img/works/course1.jpg" alt="Amazing Project" class="img-responsive image">
			  </div>
			  <div class="col-md-8 form-group">
				<h2><strong class="program-name">Course 1</strong></h2>
				<p class="text-justify description">A program in the College of Education that candidates complete to earn their Single Subject Credential in English,
				  which allows them to teach English at the middle and high school levels in the State of California.</p>
			  </div>
			<!-- <div class="row">
			  <div class="col-md-4 form-group">
				<button class="btn btn blue"><i class="fa fa-inr"></i> 250</button>
				<button type="button" class="btn blue"> Buy Now </button>
			  </div> -->
			  <div class="col-md-8 col-md-offset-2 form-group">
				<h4><strong>Number of Tests</strong></h4>
				<div class="table-scrollable" id="modal-table">
				  <table class="table table-striped table-hover table-bordered">
					<thead>
					  <tr>
						<th>Test Topic</th>
						<th>No. of Tests</th>
					  </tr>
					</thead>
					<tbody>
					  <tr>
						<td>Test Topic 1</td>
						<td>5</td>
					  </tr>
					  <tr>
						<td>Test Topic 2</td>
						<td>10</td>
					  </tr>
					  <tr>
						<td>Test Topic 3</td>
						<td>8</td>
					  </tr>
					</tbody>
			  </table>
				</div>
			  </div>
			  <div class="col-md-2"></div>
			</div>
			<!-- </div> -->
		  </div>
		  <div class="modal-footer">
			<span class="pull-left price">CAD 250</span>
			<div class="pull-right">
			  <!-- <span class="promocode">Have a Promocode?</span>
			  <div class="promocode-div pull-left mrg-right15" style="display:none;">
				<input type="text" class="form-control input-small pull-left mrg-right15" id="promo" placeholder="Enter Promocode">
				<button type="button" class="btn grey-cascade" id="btn-promo"> Apply </button>
			  </div> -->
			  <button type="button" class="btn grey-cascade buy-program"> Buy Now </button>
			  <button type="button" class="btn default" data-dismiss="modal">Close</button>
			</div>
		  </div>
		</div>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- BEGIN PRE-FOOTER -->
	<?php include FOOTER; ?>

	<!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
	<?php include('html/js-files.php'); ?>
	<script type="text/javascript" src="assets/js/custom/all-programs.js"></script>
	<!-- END PAGE LEVEL JAVASCRIPTS -->

	<script>
		$(document).ready(function(){
			$("span.promocode").click(function(){
				$("span.promocode").hide("slow");
				$("div.promocode-div").show("slow");
			});
		});
	</script>

</body>
<!-- END BODY -->
</html>
