<?php
  require_once 'config.inc.test.php';
    
  @session_start();  
  if(isset($_SESSION['role']) && $_SESSION['role'] != '4')
  {
    @session_destroy();
  }
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
  <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">        
        <!-- BEGIN CONTENT -->
        <div class="row mrg-top40">
          <div class="col-md-7">
            <div class="your-score">
            <h3><strong>Am I on track to pass?</strong><span> How do I compare to others?</span></h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged...</p>
            </div>
          </div>
          <div class="col-md-5">
            <div class="score-chart">
             <img alt="" src="assets/frontend/pages/img/works/over-all-report.png" class="img-responsive">
            </div>
          </div>
        </div>
        <div class="row mrg-tb40">
          <div class="col-md-5">
            <div class="score-chart">
              <img alt="" src="assets/frontend/pages/img/works/questions-report.png" class="img-responsive">
            </div>
          </div>
          <div class="col-md-7">
            <div class="your-score">
              <h3><strong>All the features you'd expect.</strong><span> And more.</span></h3>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged...
              </p>
            </div>
          </div>
        </div>
        <div class="row mrg-top40">
          <div class="col-md-7">
            <div class="your-score">
            <h3><strong>Am I on track to pass?</strong><span> How do I compare to others?</span></h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged...</p>
            </div>
          </div>
          <div class="col-md-5">
            <div class="score-chart">
             <img alt="" src="assets/frontend/pages/img/works/score-report.png" class="img-responsive">
            </div>
          </div>
        </div>
        <div class="row mrg-tb40">
          <div class="col-md-5">
            <div class="score-chart">
              <img alt="" src="assets/frontend/pages/img/works/topper-gap-report.png" class="img-responsive">
            </div>
          </div>
          <div class="col-md-7">
            <div class="your-score">
              <h3><strong>All the features you'd expect.</strong><span> And more.</span></h3>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged...
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- BEGIN PRE-FOOTER -->
  <?php include('new-html/footer.php'); ?>
    <!-- END FOOTER -->
  <!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
    <!-- END PAGE LEVEL JAVASCRIPTS -->

</body>
<!-- END BODY -->
</html>