<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Start Header -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 question-header">
				<div class="row">
					<div class="col-md-3 col-sm-3">
						<h4 class="TestName"><strong>SBIPO-01</strong></h4>
					</div>
					<div class="col-md-9 col-sm-9">
						<h3 class="TestOrganiser"><strong> Aiets-TestEngine</strong></h3>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9 padd15">
				<div class="col-md-12 border-bbb mrg15">
					<label><strong>Sections</strong></label><br>
					<div id="sections">
						<button type="button" class="btn blue mrg-bot5">English Language</button>
						<button type="button" class="btn btn-info mrg-bot5">Quantitative Apptitude</button>
						<button type="button" class="btn btn-info mrg-bot5">Logical Reasoning</button>
					</div>
				</div>
				<div class="col-md-12 border-bbb">
					<div class="from-group">
						<label><strong>Question No 1</strong></label><br>
					</div>
					<div class="col-md-12">
						<div class="row border-top-bbb">
							<div class="col-md-6 border-right-bbb mrg15">
								<div class="row">
									<div class="scroller QuestionDetailsHeight" data-always-visible="1" data-rail-visible="0">
										<div class="col-md-12">
										<p>
											 My next pet was a pigeon, the most revolting bird to look at, with his feathers pushing through the wrinkled scarlet skin, mixed with the horrible yellow down that covers baby pigeons and makes them look as though they have been peroxiding their hair. Because of his repulsive and obese appearance, we called him Quasimodo. 
										</p>
										<p>
											 Since he had an unorthodox upbringing, without parents to teach him, Quasimodo became convinced that he was not a bird at all, and refused to fly. He walked everywhere. He was always eager to join us in anything we did. He would even try to come for walks with us. So you had to either carry him on your shoulder, which was risking an accident to your clothes, or else you let him walk behind. If you let him walk, then you had to slow down your own pace to suit his, for should you get too far ahead you would hear the most frantic and imploring coos and turn around to find Quasimodo running desperately after you.
										</p>
										<p>Here to repeat</p>
										<p>
											 My next pet was a pigeon, the most revolting bird to look at, with his feathers pushing through the wrinkled scarlet skin, mixed with the horrible yellow down that covers baby pigeons and makes them look as though they have been peroxiding their hair. Because of his repulsive and obese appearance, we called him Quasimodo. 
										</p>
										<p>
											 Since he had an unorthodox upbringing, without parents to teach him, Quasimodo became convinced that he was not a bird at all, and refused to fly. He walked everywhere. He was always eager to join us in anything we did. He would even try to come for walks with us. So you had to either carry him on your shoulder, which was risking an accident to your clothes, or else you let him walk behind. If you let him walk, then you had to slow down your own pace to suit his, for should you get too far ahead you would hear the most frantic and imploring coos and turn around to find Quasimodo running desperately after you.
										</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<h5><strong>Q1. Read the questions given below and write the option you consider the most appropriate in your answer sheet.</strong></h5><br>
								<h5><strong>1. The narrator describes the pigeon as a 'revolting bird' because</strong></h5>
								<div class="radio-list">
									<label>
									<input type="radio" name="optionsRadios" id="optionsRadios22" value="option1"> he could not fly </label>
									<label>
									<input type="radio" name="optionsRadios" id="optionsRadios22" value="option1"> he had to be carried everywhere </label>
									<label>
									<input type="radio" name="optionsRadios" id="optionsRadios22" value="option1"> he had wrinkled skin covered with yellow feathers </label>
									<label>
									<input type="radio" name="optionsRadios" id="optionsRadios22" value="option1"> he was fat </label>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="col-md-12 save-btnBox border-bbb">
					<div class="padd7">
						<div class="width100per">
							<a href="#" class="btn btn-info mrg-bot5">Make For Review & Next</a>
						</div>
						<div class="width100per">
							<a href="#" class="btn btn-info mrg-bot5">Clear Response</a>
						</div>
						<div class="width100perR">
							<a href="#" class="btn blue mrg-bot5">Save & Next</a>
						</div>
					</div>
				</div>
				
			</div>
			<div class="col-md-3">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 col-xs-6 text-center">
							<!--<img src="assets/frontend/layout/img/img12 copy.png" class="img-responsive padd20" />-->
							<i class="fa fa-user profile-icon"></i>
							<p style="margin-top: 3px; font-size: 9px;"><strong>Your photo appears here</strong></p>
						</div>
						<div class="col-md-6 col-xs-6 padd-top20">
							<p><strong>Time Left : 45:11</strong></p>
							<i>it</i>
							<p><a class="deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-pause"></i></a> <strong>Pause Test</strong></p>
						</div>
					</div>
				</div>
				<div class="col-md-12 QuestionPalette-bg">
					<p>You are viewing <strong>EL</strong> section</p>
					<p>Question Palette:</p>
					<div class="row">
						<div class="form-group">
							<div class="scroller QuestionPalette-height" data-always-visible="1" data-rail-visible="0">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-12 mrg-bot5 text-center">
											<a class="deleteOption btn btn-circle btn-icon-only red">1</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">2</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">3</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">4</a>
										</div>
										<div class="col-md-12 mrg-bot5 text-center">
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">5</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">6</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">7</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">8</a>
										</div>
										<div class="col-md-12 mrg-bot5 text-center">
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">9</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">10</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">11</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">12</a>
										</div>
										<div class="col-md-12 mrg-bot5 text-center">
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">13</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">14</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">15</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">16</a>
										</div>
										<div class="col-md-12 mrg-bot5 text-center">
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">17</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">18</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">19</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">20</a>
										</div>
										<div class="col-md-12 mrg-bot5 text-center">
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">21</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">22</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">23</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default">24</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<p><strong>Legend</strong></p>
								<div class="row">
									<div class="col-md-6">
										<h5><a class="deleteOption btn btn-circle btn-icon-only green"></a> Answered</h5>
									</div>
									<div class="col-md-6">
										<h5><a class="deleteOption btn btn-circle btn-icon-only red"></a> Not Answered</h5>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<h5><a class="deleteOption btn btn-circle btn-icon-only blue"></a> Marked</h5>
									</div>
									<div class="col-md-6">
										<h5><a class="deleteOption btn btn-circle btn-icon-only btn-default"></a> Not Visited</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<div class="btn-group-justified">
												<a class="btn btn-info">Question Paper</a>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<div class="btn-group-justified">
												<a class="btn btn-info">Instructions</a>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<div class="btn-group-justified">
												<a class="btn btn-info">Profile</a>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<div class="btn-group-justified">
												<a class="btn btn-info">Submit</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	
<!-- END PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>