<?php include 'Access-API.php'; ?>
<?php include 'Access-Institute-API.php'; ?>
<?php require_once 'config.inc.test.php'; ?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
	<?php include('html/navigation.php'); ?>
	<!-- Navigation END -->

	<div class="main">
	  <div class="container">
		<ul class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li><a href="#">Institute</a></li>
			<li class="active">Manage Assistant</li>
		</ul>
		<!-- BEGIN SIDEBAR & CONTENT -->
		<div class="row margin-bottom-40">
		  <!-- BEGIN SIDEBAR -->
		  <div class="sidebar col-md-3 col-sm-3">
			<?php include('html/student/sidebar.php'); ?>
		  </div>
		  <!-- END SIDEBAR -->

		  <!-- BEGIN CONTENT -->
		  <div class="col-md-9 col-sm-9">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2>Manage Assistants</h2>
				</div>
				<div class="col-md-6 col-sm-6"></div>
			</div>
			<div class="row mrg-bot20">
				<div class="col-md-12 margin-bottom-40">
					<form id="add-assistant-form" role="form">
						<div class="row">
							<div class="col-md-3">
								<label>First Name<span class="required">* </span></label>
								<input type="text" id="fname" class="form-control" placeholder="Enter name">
							</div>
							<div class="col-md-3">
								<label>Last Name</label>
								<input type="text" id="lname" class="form-control" placeholder="Enter name">
							</div>
							<div class="col-md-3">
								<label>Assistant Email ID<span class="required">* </span></label>
								<input type="email" class="form-control" id="assistant_email" placeholder="Enter Email ID">
							</div>
							<div class="col-md-3">
								<label>Assistant Phone Numbar<span class="required">* </span></label>
								<input type="text" class="form-control"  id="assistant_phone" placeholder="Enter Phone Numbar">
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<button type="submit" class="btn green  mrg-top24">Add</button>
							</div>
						</div>
					</form>
				</div>

				<div class="col-md-12">
					<div class="margin-top-10 margin-bottom-10 table-scrollable">
						<table id="assistant-table" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>
										S. No.
									</th>
									<th>
										Assistant Name
									</th>
									<th>
										Email
									</th>
									<th>
										Phone No
									</th>
									<th>
										Action
									</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>

			</div>

			<!-- END PAGE CONTENT-->
		  </div>
		  <!-- END CONTENT -->
		</div>
		<!-- END SIDEBAR & CONTENT -->
	  </div>
	</div>
	<div class="modal fade" id="edit-assistant" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Edit Assistant</h4>
				</div>
				<form id="assistant-edit-form" role="form">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">First Name</label><span class="required">* </span>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="form-control fname" placeholder="First Name">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Last Name</label>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="form-control lname" placeholder="Last Name">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Email ID</label><span class="required">* </span>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="form-control email1" placeholder="Email Id">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Phone Number</label><span class="required">* </span>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="form-control phone" placeholder="Phone Numbar">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Password</label><span class="required">* </span>
									<div class="row">
										<div class="col-sm-12">
											<input type="Password" class="form-control" name="password" id="password" placeholder="Password" >
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label for="confirm-password" class="control-label">Confirm Password <span class="require">*</span></label>
									<div class="row">
										<div class="col-sm-12">
											<input type="password" class="form-control" id="confirm-password" placeholder="Confirm Password" >
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn blue">Edit</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	</div>
			<!-- /.modal -->

	<!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
	<!-- END FOOTER -->

	<!-- /modal -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
	<?php include('html/js-files.php'); ?>
	<?php include('html/student/js-files.php'); ?>

<script src="assets/js/custom/institute/manage-assistants.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
   // Index.initCharts(); // init index page's custom scripts
});
</script>
<script>
</script>


<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>