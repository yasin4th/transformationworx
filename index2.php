<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <!-- BEGIN SLIDER -->
    <div class="page-slider margin-bottom-40">
      <div class="fullwidthbanner-container revolution-slider">
        <div class="fullwidthabnner">
          <ul id="revolutionul">
            <!-- THE NEW SLIDE -->
            <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
              <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
              <img src="assets/frontend/pages/img/revolutionslider/online-test-page-banner-3.png" alt="">

              <!-- <div class="caption lft slide_title_white slide_item_left "
                data-x="50"
                data-y="60"
                data-speed="400"
                data-start="1500"
                data-easing="easeOutExpo">
                <img src="assets/frontend/pages/img/revolutionslider/slider1-1.png" alt="" style="width:80%;">
              </div>
              <div class="caption lft slide_subtitle_white slide_item_left"
                data-x="0"
                data-y="250"
                data-speed="400"
                data-start="2000"
                data-easing="easeOutExpo">
                <img src="assets/frontend/pages/img/revolutionslider/slider1-2.png" alt="" style="width:80%;">
              </div>
              <div class="caption lfr slide_subtitle_white slide_item_left"
                data-x="0"
                data-y="290"
                data-speed="400"
                data-start="2500"
                data-easing="easeOutExpo">
                <img src="assets/frontend/pages/img/revolutionslider/slider1-3.png" alt="" style="width:80%;">
              </div>
              <div class="caption lfb slide_subtitle_white slide_item_left"
                data-x="0"
                data-y="330"
                data-speed="400"
                data-start="3000"
                data-easing="easeOutExpo">
                <img src="assets/frontend/pages/img/revolutionslider/slider1-4.png" alt="" style="width:80%;">
              </div> -->
              <!-- <a class="caption lft btn dark slide_btn slide_item_left" href="#"
                data-x="187"
                data-y="315"
                data-speed="400"
                data-start="3000"
                data-easing="easeOutExpo">
                Register Now!
              </a>    -->
              <!-- <div class="caption lfb"
                data-x="640"
                data-y="0"
                data-speed="700"
                data-start="1000"
                data-easing="easeOutExpo">
                <img src="assets/frontend/pages/img/revolutionslider/slider1-3.png" alt="">
              </div> -->
              <!-- <div class="caption lfb"
                data-x="640"
                data-y="0"
                data-speed="700"
                data-start="1000"
                data-easing="easeOutExpo">
                <img src="assets/frontend/pages/img/revolutionslider/slider1-4.png" alt="">
              </div> -->
            </li>

            <!-- THE FIRST SLIDE -->
            <!-- <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg"> -->
              <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
              <!-- <img src="assets/frontend/pages/img/revolutionslider/bg7" alt="">

              <div class="caption lft slide_desc slide_item_left"
                data-x="500"
                data-y="40"
                data-speed="400"
                data-start="1500"
                data-easing="easeOutExpo">
                <img src="assets/frontend/pages/img/revolutionslider/slider2-1.png" alt="" style="width:80%;">
              </div>
              <div class="caption lft slide_desc slide_item_left"
                data-x="700"
                data-y="120"
                data-speed="400"
                data-start="2000"
                data-easing="easeOutExpo">
                <img src="assets/frontend/pages/img/revolutionslider/slider2-2.png" alt="" style="width:80%;">
              </div>
              <div class="caption lft slide_desc slide_item_left"
                data-x="450"
                data-y="220"
                data-speed="400"
                data-start="2500"
                data-easing="easeOutExpo">
                <img src="assets/frontend/pages/img/revolutionslider/slider2-3.png" alt="" style="width:80%;">
              </div>
              <div class="caption lft slide_desc slide_item_left"
                data-x="510"
                data-y="260"
                data-speed="400"
                data-start="2500"
                data-easing="easeOutExpo">
                <img src="assets/frontend/pages/img/revolutionslider/slider2-4.png" alt="" style="width:80%;">
              </div> -->
              <!-- <a class="caption lft btn green slide_btn slide_item_left" href="#"
                data-x="30"
                data-y="290"
                data-speed="400"
                data-start="3000"
                data-easing="easeOutExpo">
                Register Now!
              </a> -->
              <!-- <div class="caption lfb"
                data-x="465"
                data-y="290"
                data-speed="700"
                data-start="3000"
                data-easing="easeOutExpo">
                <img src="assets/frontend/pages/img/revolutionslider/slider2-5.png" alt="" style="width:80%;">
              </div>
            </li> -->

            <!-- THE SECOND SLIDE -->
            <!--<li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-delay="9400" data-thumb="assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
              <img src="assets/frontend/pages/img/revolutionslider/bg2.jpg" alt="">
              <div class="caption lfl slide_title slide_item_left"
                data-x="30"
                data-y="125"
                data-speed="400"
                data-start="3500"
                data-easing="easeOutExpo">
                Powerfull &amp; Clean
              </div>
              <div class="caption lfl slide_subtitle slide_item_left"
                data-x="30"
                data-y="200"
                data-speed="400"
                data-start="4000"
                data-easing="easeOutExpo">
                Responsive Admin &amp; Website Theme
              </div>
              <div class="caption lfl slide_desc slide_item_left"
                data-x="30"
                data-y="245"
                data-speed="400"
                data-start="4500"
                data-easing="easeOutExpo">
                Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
              </div>
              <div class="caption lfr slide_item_right"
                data-x="635"
                data-y="105"
                data-speed="1200"
                data-start="1500"
                data-easing="easeOutBack">
                <img src="assets/frontend/pages/img/revolutionslider/mac.png" alt="Image 1">
              </div>
              <div class="caption lfr slide_item_right"
                data-x="580"
                data-y="245"
                data-speed="1200"
                data-start="2000"
                data-easing="easeOutBack">
                <img src="assets/frontend/pages/img/revolutionslider/ipad.png" alt="Image 1">
              </div>
              <div class="caption lfr slide_item_right"
                data-x="735"
                data-y="290"
                data-speed="1200"
                data-start="2500"
                data-easing="easeOutBack">
                <img src="assets/frontend/pages/img/revolutionslider/iphone.png" alt="Image 1">
              </div>
              <div class="caption lfr slide_item_right"
                data-x="835"
                data-y="230"
                data-speed="1200"
                data-start="3000"
                data-easing="easeOutBack">
                <img src="assets/frontend/pages/img/revolutionslider/macbook.png" alt="Image 1">
              </div>
              <div class="caption lft slide_item_right"
                data-x="865"
                data-y="45"
                data-speed="500"
                data-start="5000"
                data-easing="easeOutBack">
                <img src="assets/frontend/pages/img/revolutionslider/hint1-red.png" id="rev-hint1" alt="Image 1">
              </div>
              <div class="caption lfb slide_item_right"
                data-x="355"
                data-y="355"
                data-speed="500"
                data-start="5500"
                data-easing="easeOutBack">
                <img src="assets/frontend/pages/img/revolutionslider/hint2-red.png" id="rev-hint2" alt="Image 1">
              </div>
            </li>-->

            <!-- THE THIRD SLIDE -->
            <!-- <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
              <img src="assets/frontend/pages/img/revolutionslider/slider3-bg.png" alt="">
              <div class="caption lfl slide_desc slide_item_right"
                data-x="350"
                data-y="100"
                data-speed="400"
                data-start="1500"
                data-easing="easeOutBack">
				        <img src="assets/frontend/pages/img/revolutionslider/slider3-1.png" alt="Image 1" style="width:80%;">
              </div>
              <div class="caption lfr slide_desc slide_item_right"
                data-x="320"
                data-y="200"
                data-speed="400"
                data-start="2000"
                data-easing="easeOutExpo">
                <img src="assets/frontend/pages/img/revolutionslider/slider3-2.png" alt="Image 2" style="width:80%;">
              </div>
              <div class="caption lft slide_desc slide_item_right"
                data-x="270"
                data-y="260"
                data-speed="400"
                data-start="2500"
                data-easing="easeOutExpo">
                <img src="assets/frontend/pages/img/revolutionslider/slider3-3.png" alt="Image 3" style="width:80%;">
              </div> -->
              <!-- <div class="caption lfr slide_desc"
                data-x="470"
                data-y="220"
                data-speed="400"
                data-start="3000"
                data-easing="easeOutExpo">
                <img src="assets/frontend/pages/img/revolutionslider/slider3-4.png" alt="Image 4">
              </div> -->
              <!-- <a class="caption lfr btn yellow slide_btn" href=""
                data-x="470"
                data-y="280"
                                 data-speed="400"
                                 data-start="3500"
                                 data-easing="easeOutExpo">
                                 Get Register Now!
                            </a> -->
                        <!-- </li> -->

                        <!-- THE FORTH SLIDE -->
                       <!-- <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">

                            <img src="assets/frontend/pages/img/revolutionslider/bg4.jpg" alt="">
                             <div class="caption lft slide_title"
                                 data-x="30"
                                 data-y="105"
                                 data-speed="400"
                                 data-start="1500"
                                 data-easing="easeOutExpo">
                                 What else included ?
                            </div>
                            <div class="caption lft slide_subtitle"
                                 data-x="30"
                                 data-y="180"
                                 data-speed="400"
                                 data-start="2000"
                                 data-easing="easeOutExpo">
                                 The Most Complete Admin Theme
                            </div>
                            <div class="caption lft slide_desc"
                                 data-x="30"
                                 data-y="225"
                                 data-speed="400"
                                 data-start="2500"
                                 data-easing="easeOutExpo">
                                 Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
                            </div>
                            <a class="caption lft slide_btn btn red slide_item_left" href="http://www.keenthemes.com/preview/index.php?theme=metronic_admin" target="_blank"
                                 data-x="30"
                                 data-y="300"
                                 data-speed="400"
                                 data-start="3000"
                                 data-easing="easeOutExpo">
                                 Learn More!
                            </a>
                            <div class="caption lft start"
                                 data-x="670"
                                 data-y="55"
                                 data-speed="400"
                                 data-start="2000"
                                 data-easing="easeOutBack"  >
                                 <img src="assets/frontend/pages/img/revolutionslider/iphone_left.png" alt="Image 2">
                            </div>

                            <div class="caption lft start"
                                 data-x="850"
                                 data-y="55"
                                 data-speed="400"
                                 data-start="2400"
                                 data-easing="easeOutBack"  >
                                 <img src="assets/frontend/pages/img/revolutionslider/iphone_right.png" alt="Image 3">
                            </div>
                        </li>-->
                </ul>
                <div class="tp-bannertimer tp-bottom"></div>
            </div>
        </div>
    </div>
    <!-- END SLIDER -->

    <div class="main">
      <div class="container">
        <!-- BEGIN SERVICE BOX -->
        <!-- <div class="row service-box margin-bottom-40">
          <div class="col-md-4 col-sm-4">
            <div class="service-box-heading">
              <em><i class="fa fa-tachometer red"></i></em>
              <span>Unlimited Practice</span>
            </div>
            <p>Huge repository of different categories of questions. Detailed solutions for all questions with Special focus on shortcuts & speed tips to reduce attempt time.</p>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="service-box-heading">
              <em><i class="fa fa-bar-chart green"></i></em>
              <span>Powerful Detailed Analysis</span>
            </div>
            <p>Detailed analysis that will pinpoint the areas of strengths, weakness and test taking strategy. Comparison of your score with toppers and cut-offs tell you Toppers Gap and Success Gap.</p>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="service-box-heading">
              <em><i class="fa fa-book blue"></i></em>
              <span>Study Planner</span>
            </div>
            <p>A Unique tool which helps in making a Workable Plan based on your Goal, course covered and time available.</p>
          </div>
        </div> -->
        <!-- END SERVICE BOX -->

        

        <!-- BEGIN RECENT WORKS -->
        <div class="row recent-work margin-bottom-40">
          <div class="col-md-12">
            <h2>Popular Programs</h2>
            <!-- <p>Lorem ipsum dolor sit amet, dolore eiusmod quis tempor incididunt ut et dolore Ut veniam unde voluptatem. Sed unde omnis iste natus error sit voluptatem.</p> -->
          </div>

          <div class="col-md-12">
            <div id="popular-programs" class="owl-carousel owl-carousel4">
              <div class="recent-work-item">
                <a href="#">
                  <em>
                    <img src="assets/frontend/pages/img/works/course1.jpg" alt="Amazing Project" class="img-responsive">
                    <div class="overlay"></div>
                  </em>
                  <span class="recent-work-description">
                    <strong>Course 1</strong>
                    <b>Course Description</b>
                  </span>
                </a>
              </div>
              <div class="recent-work-item">
                <a href="#">
                  <em>
                    <img src="assets/frontend/pages/img/works/course2.jpg" alt="Amazing Project" class="img-responsive">
                    <div class="overlay"></div>
                  </em>
                  <span class="recent-work-description" href="#">
                    <strong>Course 2</strong>
                    <b>Course Description</b>
                  </span>
                </a>
              </div>
              <div class="recent-work-item">
                <a href="#">
                  <em>
                    <img src="assets/frontend/pages/img/works/course3.jpg" alt="Amazing Project" class="img-responsive">
                    <div class="overlay"></div>
                  </em>
                  <span class="recent-work-description" href="#">
                    <strong>Course 3</strong>
                    <b>Course Description</b>
                  </span>
                </a>
              </div>
              <div class="recent-work-item">
                <a href="#">
                  <em>
                    <img src="assets/frontend/pages/img/works/course4.jpg" alt="Amazing Project" class="img-responsive">
                    <div class="overlay"></div>
                  </em>
                  <span class="recent-work-description" href="#">
                    <strong>Course 4</strong>
                    <b>Course Description</b>
                  </span>
                </a>
              </div>
              <div class="recent-work-item">
                <a href="#">
                  <em>
                    <img src="assets/frontend/pages/img/works/course5.jpg" alt="Amazing Project" class="img-responsive">
                    <div class="overlay"></div>
                  </em>
                  <span class="recent-work-description" href="#">
                    <strong>Course 5</strong>
                    <b>Course Description</b>
                  </span>
                </a>
              </div>
              <div class="recent-work-item">
                <a href="#">
                  <em>
                    <img src="assets/frontend/pages/img/works/course6.jpg" alt="Amazing Project" class="img-responsive">
                    <div class="overlay"></div>
                  </em>
                  <span class="recent-work-description" href="#">
                    <strong>Course 6</strong>
                    <b>Course Description</b>
                  </span>
                </a>
              </div>
              <div class="recent-work-item">
                <a href="#">
                  <em>
                    <img src="assets/frontend/pages/img/works/course7.jpg" alt="Amazing Project" class="img-responsive">
                    <div class="overlay"></div>
                  </em>
                  <span class="recent-work-description" href="#">
                    <strong>Course 7</strong>
                    <b>Course Description</b>
                  </span>
                </a>
              </div>
              <div class="recent-work-item">
                <a href="#">
                  <em>
                    <img src="assets/frontend/pages/img/works/course8.jpg" alt="Amazing Project" class="img-responsive">
                    <div class="overlay"></div>
                  </em>
                  <span class="recent-work-description" href="#">
                    <strong>Course 8</strong>
                    <b>Course Description</b>
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <!-- BEGIN BLOCKQUOTE BLOCK -->
        <div class="row quote-v1 margin-bottom-30">
          <div class="col-md-9">
            <!-- <span>Improve Your Score by 15 to 20%. <a href="assets/frontend/pages/img/MLG_Blockquote_Pop_Up.png" class="fancybox-fast-view konw-more">Click here to know more</a></span> -->
          </div>
          <div class="col-md-3 text-right">
            <a href="all-programs.php" class="btn-transparent"><i class="fa fa-bars margin-right-10"></i>All Programs</a>
          </div>
        </div>
        <!-- END BLOCKQUOTE BLOCK -->

        <!-- <div class="row recent-work margin-bottom-40">
          <div class="col-md-12">
            <h2>Latest Courses</h2>
          </div>
          <div class="col-md-12">
            <div id="latest-programs" class="owl-carousel owl-carousel4">
              <div class="recent-work-item">
                <a href="#">
                  <em>
                    <img src="assets/frontend/pages/img/works/course1.jpg" alt="Amazing Project" class="img-responsive">
                    <div class="overlay"></div>
                  </em>
                  <span class="recent-work-description">
                    <strong>Course 1</strong>
                    <b>Course Description</b>
                  </span>
                </a>
              </div>
              <div class="recent-work-item">
                <a href="#">
                  <em>
                    <img src="assets/frontend/pages/img/works/course2.jpg" alt="Amazing Project" class="img-responsive">
                    <div class="overlay"></div>
                  </em>
                  <span class="recent-work-description" href="#">
                    <strong>Course 2</strong>
                    <b>Course Description</b>
                  </span>
                </a>
              </div>
              <div class="recent-work-item">
                <a href="#">
                  <em>
                    <img src="assets/frontend/pages/img/works/course3.jpg" alt="Amazing Project" class="img-responsive">
                    <div class="overlay"></div>
                  </em>
                  <span class="recent-work-description" href="#">
                    <strong>Course 3</strong>
                    <b>Course Description</b>
                  </span>
                </a>
              </div>
              <div class="recent-work-item">
                <a href="#">
                  <em>
                    <img src="assets/frontend/pages/img/works/course4.jpg" alt="Amazing Project" class="img-responsive">
                    <div class="overlay"></div>
                  </em>
                  <span class="recent-work-description" href="#">
                    <strong>Course 4</strong>
                    <b>Course Description</b>
                  </span>
                </a>
              </div>
              <div class="recent-work-item">
                <a href="#">
                  <em>
                    <img src="assets/frontend/pages/img/works/course5.jpg" alt="Amazing Project" class="img-responsive">
                    <div class="overlay"></div>
                  </em>
                  <span class="recent-work-description" href="#">
                    <strong>Course 5</strong>
                    <b>Course Description</b>
                  </span>
                </a>
              </div>
              <div class="recent-work-item">
                <a href="#">
                  <em>
                    <img src="assets/frontend/pages/img/works/course6.jpg" alt="Amazing Project" class="img-responsive">
                    <div class="overlay"></div>
                  </em>
                  <span class="recent-work-description" href="#">
                    <strong>Course 6</strong>
                    <b>Course Description</b>
                  </span>
                </a>
              </div>
              <div class="recent-work-item">
                <a href="#">
                  <em>
                    <img src="assets/frontend/pages/img/works/course7.jpg" alt="Amazing Project" class="img-responsive">
                    <div class="overlay"></div>
                  </em>
                  <span class="recent-work-description" href="#">
                    <strong>Course 7</strong>
                    <b>Course Description</b>
                  </span>
                </a>
              </div>
              <div class="recent-work-item">
                <a href="#">
                  <em>
                    <img src="assets/frontend/pages/img/works/course8.jpg" alt="Amazing Project" class="img-responsive">
                    <div class="overlay"></div>
                  </em>
                  <span class="recent-work-description" href="#">
                    <strong>Course 8</strong>
                    <b>Course Description</b>
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div> -->
        <!-- END RECENT WORKS -->

        <!-- BEGIN TABS AND TESTIMONIALS -->
        <!-- <div class="row mix-block margin-bottom-40"> -->
          <!-- TABS -->
          <!-- <div class="col-md-7 tab-style-1">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab-1" data-toggle="tab">Goal based learning</a></li> -->
              <!-- <li><a href="#tab-2" data-toggle="tab">Unlimited Practice</a></li> -->
              <!-- <li><a href="#tab-3" data-toggle="tab">QCR & Illustration</a></li>
              <li><a href="#tab-4" data-toggle="tab">Powerful Detailed Analysis</a></li> -->
              <!-- <li><a href="#tab-5" data-toggle="tab">Study Planner</a></li> -->
              <!-- <li><a href="#tab-6" data-toggle="tab">Score optimizer</a></li>
            </ul> -->
            <!-- <div class="tab-content">
              <div class="tab-pane row fade in active" id="tab-1">
                <div class="col-md-3 col-sm-3">
                  <img class="img-responsive" src="assets/frontend/pages/img/photos/goal.jpg" alt="">
                </div>
                <div class="col-md-9 col-sm-9">
                  <p class="margin-bottom-10">Each Program has defined Goals & Learning Objectives which continuously measures your progress and keeps you on track.</p>
                  <p><a class="more" href="#">Read more <i class="icon-angle-right"></i></a></p>
                </div>
              </div>
              <div class="tab-pane row fade" id="tab-2">
                <div class="col-md-9 col-sm-9">
                  <p>Huge repository of different categories of questions. Detailed solutions for all questions with Special focus on shortcuts & speed tips to reduce attempt time.</p>
                </div>
                <div class="col-md-3 col-sm-3">
                  <img class="img-responsive" src="assets/frontend/pages/img/photos/exam-creation1.png" alt="">
                </div>
              </div>
              <div class="tab-pane fade" id="tab-3">
        				<div class="col-md-3 col-sm-3">
                  <img class="img-responsive" src="assets/frontend/pages/img/photos/result-analysis1.jpg" alt="">
                </div>
        				<div class="col-md-9 col-sm-9">
        					<p>Concepts Sheets with Illustrative Examples for Quick Revision & Practice</p>
        				</div>
              </div>
              <div class="tab-pane fade" id="tab-4">
        				<div class="col-md-9 col-sm-9">
        					<p>Detailed analysis that will pinpoint the areas of strengths, weakness and test taking strategy. Comparison of your score with toppers and cut-offs tell you Toppers Gap and Success Gap.</p>
        				</div>
        				<div class="col-md-3 col-sm-3">
                  <img class="img-responsive" src="assets/frontend/pages/img/photos/result-analysis1.jpg" alt="">
                </div>
              </div>
              <div class="tab-pane fade" id="tab-5">
                <div class="col-md-3 col-sm-3">
                  <img class="img-responsive" src="assets/frontend/pages/img/photos/question-bank.jpg" alt="">
                </div>
                <div class="col-md-9 col-sm-9">
                  <p>A Unique tool which helps in making a Workable Plan based on your Goal, course covered and time available.</p>
                </div>
              </div>
              <div class="tab-pane fade" id="tab-6">
                <div class="col-md-9 col-sm-9">
                  <p>A unique tool/methodology which helps in optimizing your test taking techniques to maximize your score.</p>
                </div>
                <div class="col-md-3 col-sm-3">
                  <img class="img-responsive" src="assets/frontend/pages/img/photos/score.png" alt="">
                </div>
              </div>
            </div>
          </div> -->
          <!-- END TABS -->

          <!-- TESTIMONIALS -->
          <!-- <div class="col-md-5 testimonials-v1">
            <div id="myCarousel" class="carousel slide"> -->
              <!-- Carousel items -->
              <!-- <div class="carousel-inner">
                <div class="active item">
                  <blockquote><p>Most of the topics in Class 11/12 are extension of what you have studied in class 9/ 10.The only difference is that in class 11/12 you study them in more details. AIETS IIT Foundation courses gives you more time to adapt to the quantum jump in level of difficulty by preparing a strong foundation in class 9/10.</p></blockquote>
                  <div class="carousel-info">
                    <img class="pull-left" src="assets/frontend/pages/img/people/man-icon.png" alt="">
                    <div class="pull-left">
                      <span class="testimonials-name">Satender Dubey</span>
                      <span class="testimonials-post">Parent, Ranchi</span>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <blockquote><p>Focus would be the right word for the AIETS programmes. It is concise, too the point and surprisingly it is more than sufficient.</p></blockquote>
                  <div class="carousel-info">
                    <img class="pull-left" src="assets/frontend/pages/img/people/man-icon.png" alt="">
                    <div class="pull-left">
                      <span class="testimonials-name">Abhishek Singh</span>
                      <span class="testimonials-post">Parent, Mumbai</span>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <blockquote><p>I found IIT Foundation coachings very expensive and moreover traveling was also a concern. Then my office colleague recommended AIETS IIT Foundation programme. Now my son is happy and I am satisfied that he is moving in the right direction.</p></blockquote>
                  <div class="carousel-info">
                    <img class="pull-left" src="assets/frontend/pages/img/people/man-icon.png" alt="">
                    <div class="pull-left">
                      <span class="testimonials-name">Mr Sen Gupta</span>
                      <span class="testimonials-post">Parent, Kolkata</span>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <blockquote><p>AIETS study package laid foundation for strong concepts and the tests kept the child on the right track. I am emotionally overwhelmed with the change it has bought in my Child and I thank AIETS for that.</p></blockquote>
                  <div class="carousel-info">
                    <img class="pull-left" src="assets/frontend/pages/img/people/man-icon.png" alt="">
                    <div class="pull-left">
                      <span class="testimonials-name">Ram Gopal</span>
                      <span class="testimonials-post">Parent, Mumbai</span>
                    </div>
                  </div>
                </div>
              </div> -->

              <!-- Carousel nav -->
              <!-- <a class="left-btn" href="#myCarousel" data-slide="prev"></a>
              <a class="right-btn" href="#myCarousel" data-slide="next"></a>
            </div>
          </div> -->
          <!-- END TESTIMONIALS -->
        <!-- </div> -->
        <!-- END TABS AND TESTIMONIALS -->

        <!-- BEGIN STEPS -->
        <!-- <div class="row margin-bottom-40 front-steps-wrapper front-steps-count-3">
          <div class="col-md-4 col-sm-4 front-step-col">
            <div class="front-step front-step1">
              <h2>Learn, Revise & Practice</h2>
              <p>Concepts Sheets with Illustrative Examples for Quick Revision & Practice.<br><br></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 front-step-col">
            <div class="front-step front-step2">
              <h2>Test, Analyze & Improve</h2>
              <p>Chapter-wise Tests, in-depth Analysis and tools like score optimizer helps in maximizing your score.<br><br></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 front-step-col">
            <div class="front-step front-step3">
              <h2>Compete, Succeed & Excel</h2>
              <p>Our unique study planner tools monitors your progress and keeps you on track and ensures that you Compete, succeed and Excel in Exams.</p>
            </div>
          </div>
        </div> -->
        <!-- END STEPS -->

        <!-- BEGIN CLIENTS -->
        <!-- <div class="row margin-bottom-40 our-clients">
          <div class="col-md-3">
            <h2><a href="#">Our Clients</a></h2>
            <p>Lorem dipsum folor margade sitede lametep eiusmod psumquis dolore.</p>
          </div>
          <div class="col-md-9">
            <div class="owl-carousel owl-carousel6-brands">
              <div class="client-item">
                <a href="#">
                  <img src="assets/frontend/pages/img/clients/client_1_gray.png" class="img-responsive" alt="">
                  <img src="assets/frontend/pages/img/clients/client_1.png" class="color-img img-responsive" alt="">
                </a>
              </div>
              <div class="client-item">
                <a href="#">
                  <img src="assets/frontend/pages/img/clients/client_2_gray.png" class="img-responsive" alt="">
                  <img src="assets/frontend/pages/img/clients/client_2.png" class="color-img img-responsive" alt="">
                </a>
              </div>
              <div class="client-item">
                <a href="#">
                  <img src="assets/frontend/pages/img/clients/client_3_gray.png" class="img-responsive" alt="">
                  <img src="assets/frontend/pages/img/clients/client_3.png" class="color-img img-responsive" alt="">
                </a>
              </div>
              <div class="client-item">
                <a href="#">
                  <img src="assets/frontend/pages/img/clients/client_4_gray.png" class="img-responsive" alt="">
                  <img src="assets/frontend/pages/img/clients/client_4.png" class="color-img img-responsive" alt="">
                </a>
              </div>
              <div class="client-item">
                <a href="#">
                  <img src="assets/frontend/pages/img/clients/client_5_gray.png" class="img-responsive" alt="">
                  <img src="assets/frontend/pages/img/clients/client_5.png" class="color-img img-responsive" alt="">
                </a>
              </div>
              <div class="client-item">
                <a href="#">
                  <img src="assets/frontend/pages/img/clients/client_6_gray.png" class="img-responsive" alt="">
                  <img src="assets/frontend/pages/img/clients/client_6.png" class="color-img img-responsive" alt="">
                </a>
              </div>
              <div class="client-item">
                <a href="#">
                  <img src="assets/frontend/pages/img/clients/client_7_gray.png" class="img-responsive" alt="">
                  <img src="assets/frontend/pages/img/clients/client_7.png" class="color-img img-responsive" alt="">
                </a>
              </div>
              <div class="client-item">
                <a href="#">
                  <img src="assets/frontend/pages/img/clients/client_8_gray.png" class="img-responsive" alt="">
                  <img src="assets/frontend/pages/img/clients/client_8.png" class="color-img img-responsive" alt="">
                </a>
              </div>
            </div>
          </div>
        </div> -->
        <!-- END CLIENTS -->
      </div>
    </div>
<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="modal fade" id="program-details-modal" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Program Details</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-4 form-group">
                <img src="assets/frontend/pages/img/works/course1.jpg" alt="Amazing Project" class="img-responsive image">
              </div>
              <div class="col-md-8 form-group">
                <h2><strong class="program-name">Course 1</strong></h2>
                <p class="text-justify description">A program in the College of Education that candidates complete to earn their Single Subject Credential in English,
                  which allows them to teach English at the middle and high school levels in the State of California.</p>
              </div>
            <!-- <div class="row">
              <div class="col-md-4 form-group">
                <button class="btn btn blue"><i class="fa fa-inr"></i> 250</button>
                <button type="button" class="btn blue"> Buy Now </button>
              </div> -->
              <div class="col-md-8 col-md-offset-2 form-group">
                <h4><strong>Number of Tests</strong></h4>
                <div class="table-scrollable" id="modal-table">
                  <table class="table table-striped table-hover table-bordered">
                    <thead>
                      <tr>
                        <th>Test Topic</th>
                        <th>No. of Tests</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Test Topic 1</td>
                        <td>5</td>
                      </tr>

                    </tbody>
                  </table>
                </div>
              </div>
              <div class="col-md-2"></div>
            </div>
            <!-- </div> -->
          </div>
          <div class="modal-footer">
            <span class="pull-left price">CAD 250</span>
            <div class="pull-right">
              <span class="promocode">Have a Promocode?</span>
              <div class="promocode-div pull-left mrg-right15" style="display:none;">
                <input type="text" class="form-control input-small pull-left mrg-right15" placeholder="Enter Promocode">
                <button type="button" class="btn grey-cascade"> Apply </button>
              </div>
              <button type="button" class="btn grey-cascade buy-program"> Buy Now </button>
              <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
<!-- /.modal -->
<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN PRE-FOOTER -->
	<?php //include('html/footer.php'); ?>
    <!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
    <script type="text/javascript" src="assets/js/custom/index.js"></script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->

    <script>
      $(document).ready(function(){
        $("span.promocode").click(function(){
            $("span.promocode").hide("slow");
            $("div.promocode-div").show("slow");
        });
      });
    </script>

</body>
<!-- END BODY -->
</html>