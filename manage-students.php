<?php include 'Access-API.php'; ?>
<?php include 'Access-Institute-API.php'; ?>
<?php require_once 'config.inc.test.php'; ?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<link href="admin/assets/global/plugins/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/ media="all">
	<?php include('html/student/head-tag.php'); ?>
	<?php
    	date_default_timezone_set('Asia/Calcutta');
    	$today = date('Y-m-d');
    	$today1 = date('Y-m-d');
	?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
	<?php include('html/navigation.php'); ?>
	<!-- Navigation END -->

	<div class="main">
	  <div class="container">
		<ul class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li><a href="#">Institute</a></li>
			<li class="active">Manage Students</li>
		</ul>
		<!-- BEGIN SIDEBAR & CONTENT -->
		<div class="row margin-bottom-40">
		  <!-- BEGIN SIDEBAR -->
		  <div class="sidebar col-md-3 col-sm-3">
			<?php include('html/student/sidebar.php'); ?>
		  </div>
		  <!-- END SIDEBAR -->

		  <!-- BEGIN CONTENT -->
		  <div class="col-md-9 col-sm-9">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2>
						Manage Students
						<a href="#add-student" class="btn green btn-icon-only btn-circle mrg5 tooltips" data-toggle="modal">
							<i class="fa fa-plus-circle"></i>
						</a>
						<a href="#filter-student" data-toggle="modal" class="btn btn-circle btn-icon-only blue mrg5 tooltips" data-container="body" data-placement="top" data-original-title="Filter">
							<i class="fa fa-filter"></i>
						</a>
					</h2>
				</div>
				<div class="col-md-6 col-sm-6"></div>
			</div>
			<div class="row mrg-bot20">
				<div class="col-md-12">
					<div class="margin-top-10 margin-bottom-10">
						<table id="students-table" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>
										S. No.
									</th>
									<th>
										Student Name
									</th>
									<th>
										Email
									</th>
									<th>
										Phone No
									</th>
									<th class="no-sort">
										Action
									</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		  </div>
		  <!-- END CONTENT -->
		</div>
		<!-- END SIDEBAR & CONTENT -->
	  </div>
	</div>
	<div class="modal fade" id="add-student" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Add Students</h4>
				</div>
				<form id="add-student-form" role="form">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">First Name</label><span class="required">* </span>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="fname form-control" placeholder="First Name">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Last Name</label>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="lname form-control" placeholder="Last Name">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Email<span class="required">* </span></label>
									<div class="row">
										<div class="col-sm-12">
											<input type="email" class="student_email form-control" placeholder="example@demo.com">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Mobile<span class="required">* </span></label>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="student_mobile form-control" placeholder="Mobile Number"  pattern="[a-zA-Z0-9]*">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn blue">Add</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="filter-student" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Filter Students</h4>
				</div>
				<form id="filter-form"" role="form">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Test Program</label>
									<div class="row">
										<div class="col-sm-12">
											<select class="test-programs form-control">
												<option value="0">Select Program</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Student Email ID</label>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="form-control studentemail" placeholder="Enter Phone Numbar">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Student Phone Number</label>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="form-control studentphone" placeholder="Enter Phone Numbar">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn blue">Filter</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
			<!-- /.modal -->
	<div class="modal fade" id="assign-program" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Assign Program</h4>
				</div>
				<form id="assign-program-form" role="form">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Select Program</label><span class="required">* </span>
									<div class="row">
										<div class="col-sm-12">
											<select class="test-programs form-control" name="assignProgram">
												<option value="0">Select Program</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Expire Date<span class="required">* </span></label>
									<div class="row">
										<div class="col-md-12">
											<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-viewmode="years" data-date-start-date="<?php echo $today1; ?>"  data-date-multidate="false" data-date-autoclose="false">
												<span class="input-group-btn">
													<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											<input type="text" class="form-control expire_date" readonly id="offDay"  name="expire_date">
										</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Total no of Paper<span class="required">* </span></label>
									<div class="row">
										<div class="col-sm-12">
											<input type="number" min="0" value="0" class="assigntotalpaper form-control">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6" id="ap_ins_lab_type">
								<div class="form-group">
									<label class="control-label">In Lab<span class="required">* </span></label>
									<div class="row">
										<div class="col-md-12">
											<input type="number" min="0" value="0"  class="assigninlab form-control">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12">
								<div class="form-group">
									<label class="control-label" id="credits-info"></label>
								</div>
							</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn blue">Assign</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="edit-assign-student" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Edit Student</h4>
				</div>
				<form id="edit-student-form" role="form">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">First Name</label><span class="required">* </span>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" name="student_fname"  class="form-control" pattern="[a-zA-Z0-9]*">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Last Name</label><span class="required">* </span>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" name="student_lname"  class="form-control" pattern="[a-zA-Z0-9]*">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Email</label><span class="required">* </span>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" name="student_email"  class="form-control" pattern="[a-zA-Z0-9]*" disabled="">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Mobile</label><span class="required">* </span>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" id="stu_mobile"  class="form-control" pattern="[0-9]*">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Password</label><span class="required">* </span>
									<div class="row">
										<div class="col-sm-12">
											<input type="Password" class="form-control" id="password" placeholder="Password" >
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label for="confirm-password" class="control-label">Confirm Password <span class="require">*</span></label>
									<div class="row">
										<div class="col-sm-12">
											<input type="password" class="form-control" id="confirm-password" placeholder="Confirm Password" >
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn blue">Update</button>
						<button type="button" class="btn default" data-dismiss="modal" on>Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="view-program" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">View Program</h4>
				</div>
				<div class="modal-body">
					<table id="program-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>
									S. No
								</th>
								<th>
									Test Program
								</th>
								<th>
									Total Tests
								</th>
								<th>
									Total Lab Tests
								</th>
								<th>
									Expiry Date
								</th>
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="edit-student-program" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Edit Program</h4>
				</div>
				<form id="edit-student-program" role="form">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Total no of Paper<span class="required">* </span></label>
									<div class="row">
										<div class="col-sm-12">
											<input type="number" min="0"  class="program_totalpaper form-control">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6" id="ep_on_ms">
								<div class="form-group">
									<label class="control-label">In Lab<span class="required">* </span></label>
									<div class="row">
										<div class="col-md-12">
											<input type="number" min="0" class="program_inlab form-control">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Expire Date<span class="required">* </span></label>
									<div class="row">
										<div class="col-md-12">
											<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-viewmode="years" data-date-multidate="false" data-date-autoclose="false" data-date-start-date="<?php echo $today; ?>" >
												<span class="input-group-btn">
													<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
												<input type="text" class="form-control program_expire_date" readonly id="offDay" name="offDay" name="">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn blue">Edit</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
	<!-- END FOOTER -->

	<!-- /modal -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
	<?php include('html/js-files.php'); ?>
	<?php include('html/student/js-files.php'); ?>
	<script type="text/javascript" src="admin/assets/global/plugins/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="admin/assets/global/plugins/dataTables.bootstrap.min.js"></script>

	<script src="assets/js/custom/institute/manage-students.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->

<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
