<?php
	include 'config.inc.test.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Effort Education</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Loading materilize -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="blade/main/css/style.css" rel="stylesheet">
	<link href="blade/main/css/materialize.min.css" rel="stylesheet">
	<link href="blade/main/css/materialdesignicons.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="images/favicon.ico">
</head>

<body>
	<!-- Preloader -->
<!-- <div id="preloader">
	<div id="status"></div>
</div> -->
<?php
	include HEADER;
?>

<section style="background-image:url('blade/main/images/team.jpg'); background-size: 100% 100%; background-repeat:no-repeat; height: 400px;
   position: relative">
     <div class="overlay1">
	    <div class="container">
		     <div class="row">
               <div class="col s12 center packages">
				 <h4 class="sec_heding white-text" style="margin-top: 120px;"> We are  <span class="orange-text"> here </span> for you.</h4>

                </div>
			  </div>
			</div>
		  </div>
		</div>
</section>
<br>
<br>
<div class="container">
<div class="team">
 <h2 class="text-center">Meet our <strong class="blue-text">Team</strong></h2>
</div>
</div>
     <center>
         <section>
		     <div class="container">
		     <br>
		     <br>
				<div class="row">
					<div class="col s12 m6 l4 ">
						<div class="employ_pic">
							<img src="blade/main/images/passportsize.jpg" class="responsive-img">
						</div>
					</div>

					<div class="col s12 m6 l8">
					  <div class="row">
					      <div class="col s12 employ_descr">
							<h5>Awantika shakya</h5>
						  </div>

                           <div class="col s12 employ_descr">
							<h6><b>web designer</b></h6>
						   </div>

						   <div class="col s12 employ_descr">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
						   </div>
						  </div>
						</div>
				 </div>
                 <br>
		     <br>
		     	<div class="row teamdiv">
					<div class="col s12 m6 l4 ">
						<div class="employ_pic">
							<img src="blade/main/images/passportsize.jpg" class="responsive-img">
						</div>
					</div>

					<div class="col s12 m6 l8 ">
					  <div class="row">
					      <div class="col s12 employ_descr">
							<h5>Awantika shakya</h5>
						  </div>

                           <div class="col s12 employ_descr">
							<h6><b>web designer</b></h6>
						   </div>

						   <div class="col s12 employ_descr">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
						   </div>
						  </div>
						</div>
				 </div>
				 <div class="row teamdiv2">
					<div class="col s12 m6 l8">
					  <div class="row">
					      <div class="col s12 employ_descr2">
							<h5>Samisth dhar</h5>
						  </div>

                           <div class="col s12 employ_descr2">
							<h6><b>web designer</b></h6>
						   </div>

						   <div class="col s12 employ_descr2" style="float: right;">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
						   </div>
						  </div>
						</div>
						<div class="col s11 m6 l4 ">
						<div class="employ_pic">
							<img src="blade/main/images/passportsize.jpg" class="responsive-img">
						</div>
					</div>
				 </div>
             <br>
		     <br>
		     	<div class="row">
					<div class="col s12 m6 l4 ">
						<div class="employ_pic">
							<img src="blade/main/images/passportsize.jpg" class="responsive-img">
						</div>
					</div>

					<div class="col s12 m6 l8">
					  <div class="row">
					      <div class="col s12 employ_descr">
							<h5>Awantika shakya</h5>
						  </div>

                           <div class="col s12 employ_descr">
							<h6><b>web designer</b></h6>
						   </div>

						   <div class="col s12 employ_descr">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
						   </div>
						  </div>
						</div>
				 </div>
                 <br>
		     <br>
		     	<div class="row teamdiv">
					<div class="col s12 m6 l4 ">
						<div class="employ_pic">
							<img src="blade/main/images/passportsize.jpg" class="responsive-img">
						</div>
					</div>

					<div class="col s12 m6 l8 ">
					  <div class="row">
					      <div class="col s12 employ_descr">
							<h5>Awantika shakya</h5>
						  </div>

                           <div class="col s12 employ_descr">
							<h6><b>web designer</b></h6>
						   </div>

						   <div class="col s12 employ_descr">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
						   </div>
						  </div>
						</div>
				 </div>
				 <div class="row teamdiv2">
					<div class="col s12 m6 l8">
					  <div class="row">
					      <div class="col s12 employ_descr2">
							<h5>Samisth dhar</h5>
						  </div>

                           <div class="col s12 employ_descr2">
							<h6><b>web designer</b></h6>
						   </div>

						   <div class="col s12 employ_descr2" style="float: right;">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
						   </div>
						  </div>
						</div>
						<div class="col s11 m6 l4 ">
						<div class="employ_pic">
							<img src="blade/main/images/passportsize.jpg" class="responsive-img">
						</div>
					</div>
				 </div>

				 <br>
		     <br>
		 </section>
     </center>

<?php
	include FOOTER;
?>

	<script type="text/javascript" src="blade/main/js/jquery.js"></script>
	<script type="text/javascript" src="blade/main/js/materialize.min.js"></script>
	<script type="text/javascript">
		$('.carousel.carousel-slider').carousel({full_width: true});

		$(document).ready(function(){
		$('.carousel').carousel({dist:0});
		window.setInterval(function(){$('.carousel').carousel('next')},3000)
	});

	</script>



</body>
</html>
