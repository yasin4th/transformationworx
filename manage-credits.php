<?php include 'Access-API.php'; ?>
<?php include 'Access-Institute-API.php'; ?>
<?php require_once 'config.inc.test.php'; ?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<link href="admin/assets/global/plugins/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/ media="all">
	<?php include('html/student/head-tag.php'); ?>
	<?php
    	date_default_timezone_set('Asia/Calcutta');
    	$today = date('Y-m-d');
    	$today1 = date('Y-m-d');
	?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
	<?php include('html/navigation.php'); ?>
	<!-- Navigation END -->

	<div class="main">
	  <div class="container">
		<ul class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li><a href="#">Institute</a></li>
			<li class="active">Manage Credits</li>
		</ul>
		<!-- BEGIN SIDEBAR & CONTENT -->
		<div class="row margin-bottom-40">
		  <!-- BEGIN SIDEBAR -->
		  <div class="sidebar col-md-3 col-sm-3">
			<?php include('html/student/sidebar.php'); ?>
		  </div>
		  <!-- END SIDEBAR -->
		  <!-- BEGIN CONTENT -->
		  <div class="col-md-9 col-sm-9">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2>
						Manage Credits

					</h2>
					<div id="credits-info"></div>
				</div>
				<div class="col-md-6 col-sm-6"></div>
			</div>
			<div class="row mrg-bot20">
				<div class="col-md-12">
					<div class="margin-top-10 margin-bottom-10">
						<table id="students-table" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>
										S. No.
									</th>
									<th>
										Message
									</th>
									<th>Date</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		  </div>
		  <!-- END CONTENT -->
		</div>
		<!-- END SIDEBAR & CONTENT -->
	  </div>
	</div>
	<!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
	<!-- END FOOTER -->

	<!-- /modal -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
	<?php include('html/js-files.php'); ?>
	<?php include('html/student/js-files.php'); ?>
	<script type="text/javascript" src="admin/assets/global/plugins/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="admin/assets/global/plugins/dataTables.bootstrap.min.js"></script>

	<script src="assets/js/custom/institute/manage-credits.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->

<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>