<?php
  // require_once 'config.inc.php';
  require_once 'config.inc.test.php';
  require_once INDEX;
  die();
  @session_start();
  if(isset($_SESSION['website']))
  {
    header('Location: my-program');
  }
  if(isset($_SESSION['role']) && $_SESSION['role'] != '4')
  {
    @session_destroy();
  }
?>
<!DOCTYPE html>
<html >

<head>
    <base href="<?=URI1;?>" />

    <?php @include 'new-html/head-tag.php';?>
    <link href="assets/frontend/onepage/css/style.css" rel="stylesheet">
    <link href="assets/frontend/onepage/css/style-responsive.css" rel="stylesheet">
    <script type="text/javascript">
    if (window.location.hash && window.location.hash == '#_=_') {
        window.location.hash = '';
    }
    </script>
</head>

<body class="home page page-id-12 page-template page-template-page-templates page-template-homepage page-template-page-templateshomepage-php  thim-body-preload siteorigin-panels siteorigin-panels-home group-blog" id="thim-body">
    <div id="preload">
        <div class="cssload-loader">
            <div class="cssload-inner cssload-one"></div>
            <div class="cssload-inner cssload-two"></div>
            <div class="cssload-inner cssload-three"></div>
        </div>
    </div>

    <div id="wrapper-container" class="wrapper-container">
        <div class="content-pusher ">
            <header id="masthead" class="site-header affix-top bg-custom-sticky sticky-header header_overlay header_v1">
                <div id="toolbar" class="toolbar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <aside id="text-2" class="widget widget_text">
                                    <div class="textwidget">
                                        <div class="thim-have-any-question">
                                            Have any question?
                                            <div class="mobile"><i class="fa fa-phone"></i><a href="tel:0882715488" class="value">(+91) 888 271 5488 </a>
                                            </div>
                                            <div class="email"><i class="fa fa-envelope"></i><a href="mailto:support@exampointer.com"><span class="" >support@exampointer.com</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </aside>
                                <aside id="login-menu-2" class="widget widget_login-menu">
                                    <div class="thim-widget-login-menu thim-widget-login-menu-base">
                                        <div class="thim-link-login">
                                                <?php
                                                    @session_start();
                                                    if(!isset($_SESSION['role']))
                                                    {
                                                ?>
                                                <a class="login" href="login">Login</a>
                                                <a class="register" href="register">Register</a>
                                                <?php
                                                    }else{
                                                ?>
                                                <ul class="list-unstyled list-inline pull-right">
                                                  <?php include 'html/student/navigation-ul.php';?>
                                                </ul>
                                                <?php
                                                    }
                                                ?>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="row">
                        <div class="navigation col-sm-12">
                            <div class="tm-table">
                                <div class="width-logo sm-logo">
                                    <!-- <a href="index" title="E-learning Demo" rel="home" class="no-sticky-logo"><img src="http://educationwp.thimpress.com/wp-content/uploads/2015/11/logo.png" alt="Education" width="153" height="40" /></a> -->
                                    <a href="<?=URI1?>" title="ExamPointer" rel="home" class="no-sticky-logo">
                                        <!-- <img src="assets/demo/LOGO11.png" alt="Education" width="60" height="40" style="margin-bottom: 0px;" /> -->
                                        <img src="assets/global/img/exampointer-logo-white-small.png" alt="Education"  style="margin-bottom: 0px;" />
                                        <!-- <span class="title">ExamPointer</span> -->
                                    </a>
                                    <a href="<?=URI1?>" title="ExamPointer" rel="home" class="sticky-logo">
                                        <!-- <img src="assets/demo/LOGO11.png" alt="Education" width="60" height="40" style="margin-bottom: 0px;" /> -->
                                        <img src="assets/global/img/exampointer-logo-colored-small.png" alt="Education" style="margin-bottom: 0px;" />
                                        <!-- <span class="title">ExamPointer</span> -->
                                    </a>
                                </div>
                                <nav class="width-navigation table-cell table-right">
                                    <ul class="nav navbar-nav menu-main-menu">

                                        <?php if(isset($_SESSION['role']))
                                          {
                                            if($_SESSION['role'] == 4)
                                            { ?>
                                              <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="my-program"><span data-hover="My Packages">My Packages</span></a></li>

                                              <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="profile"><span data-hover="Events">My Profile</span></a></li>

                                              </li>
                                              <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="report"><span data-hover="Events">Report Card</span></a></li>

                                              </li>
                                         <?php }} ?>
                                         <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="exams"><span data-hover="Exams">Exams</span></a></li>
                                         <!-- <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="#"><span data-hover="About Us">About Us</span></a></li> -->
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="features"><span data-hover="features">Features</span></a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_left standard"><a href="contact-us"><span data-hover="Contact">Contact</span></a></li>
                                        <!-- <li class="menu-right">
                                            <ul>
                                                <li id="courses-searching-2" class="widget widget_courses-searching">
                                                    <div class="thim-widget-courses-searching thim-widget-courses-searching-base">
                                                        <div class="thim-course-search-overlay">
                                                            <div class="search-toggle"><i class="fa fa-search"></i></div>
                                                            <div class="courses-searching layout-overlay">
                                                                <div class="search-popup-bg"></div>
                                                                <form method="get" action="http://educationwp.thimpress.com/">
                                                                    <input type="text" value="" name="s" placeholder="Search programs..." class="thim-s form-control courses-search-input" autocomplete="off" />
                                                                    <input type="hidden" value="course" name="ref" />
                                                                    <button type="submit"><i class="fa fa-search"></i></button>
                                                                    <span class="widget-search-close"></span>
                                                                </form>
                                                                <ul class="courses-list-search list-unstyled"></ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li> -->
                                    </ul>
                                </nav>
                                <div class="menu-mobile-effect navbar-toggle" data-effect="mobile-effect">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </div>
                                <br>
                            </div>

                        </div>
                    </div>
                </div>
            </header>

            <nav class="mobile-menu-container mobile-effect">
                <ul class="nav navbar-nav">
                    <?php if(isset($_SESSION['role']))
                      {
                        if($_SESSION['role'] == 4)
                        { ?>
                          <li><a href="my-program"><span data-hover="My Packages">My Packages</span></a></li>

                          <li><a href="profile"><span data-hover="Events">My Profile</span></a></li>

                          <li><a href="report"><span data-hover="Events">Report Card</span></a></li>

                     <?php }} ?>
                    <li><a href="exams"><span data-hover="Exams">Exams</span></a></li>
                    <li><a href="#"><span data-hover="About Us">About Us</span></a></li>
                    <li><a href="features"><span data-hover="features">Features</span></a></li>
                    <li><a href="contact-us"><span data-hover="Contact">Contact</span></a></li>
                    <li class="menu-right">
                        <ul>
                            <li id="courses-searching-2" class="widget widget_courses-searching">
                                <div class="thim-widget-courses-searching thim-widget-courses-searching-base">
                                    <div class="thim-course-search-overlay">
                                        <div class="search-toggle"><i class="fa fa-search"></i></div>
                                        <div class="courses-searching layout-overlay">
                                            <div class="search-popup-bg"></div>
                                            <form method="get" action="http://educationwp.thimpress.com/">
                                                <input type="text" value="" name="s" placeholder="Search programs..." class="thim-s form-control courses-search-input" autocomplete="off" />
                                                <input type="hidden" value="course" name="ref" />
                                                <button type="submit"><i class="fa fa-search"></i></button>
                                                <span class="widget-search-close"></span>
                                            </form>
                                            <ul class="courses-list-search list-unstyled"></ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>

            <div id="main-content">
                <div id="main-home-content" class="home-content home-page container" role="main">
                    <div id="pl-12">
                        <div class="panel-grid" id="pg-12-0">
                            <div class="siteorigin-panels-stretch thim-fix-stretched panel-row-style" style="height: 100vh;" data-stretch-type="full-stretched">
                                <div class="panel-grid-cell" id="pgc-12-0-0">
                                    <div class="so-panel widget widget_text panel-first-child panel-last-child" id="panel-12-0-0-0" data-index="0">
                                        <div class="textwidget">
                                            <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullscreen-container" style="background-color:transparent;padding:0px;background-image:url(assets/demo/top-slider.jpg);background-repeat:no-repeat;background-size:cover;background-position:center center;">

                                                <div id="rev_slider_4_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.2.4.1">
                                                    <ul>
                                                        <li data-index="rs-17" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="assets/demo/top-slider-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                                                            <img src="assets/demo/dummy.png" alt="" title="top-slider" width="1600" height="900" data-lazyload="assets/demo/top-slider.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>


                                                            <div class="tp-caption    thim-slider-sub-heading" id="slide-17-layer-1" data-x="['left','left','left','left']" data-hoffset="['15','15','15','15']" data-y="['middle','middle','middle','middle']" data-voffset="['-146','-150','-45','-20']" data-fontsize="['24','24','','20']" data-width="none" data-height="" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;s:1800;e:Power4.easeInOut;" data-transform_out="opacity:0;s:100;e:Power2.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="300" data-splitin="chars" data-splitout="none" data-responsive_offset="off" data-responsive="off" data-elementdelay="0.05" style="z-index: 5; max-width: px; max-width: px; white-space: nowrap; font-size: 24px; line-height: 30px; font-weight: 700; color: rgba(252, 252, 252, 1.00);text-transform:left;">WELCOME TO THE NEW WORLD OF </div>

                                                            <h3 class="tp-caption    thim-slider-heading" id="slide-17-layer-2" data-x="['left','left','left','left']" data-hoffset="['15','15','15','15']" data-y="['middle','middle','middle','middle']" data-voffset="['-75','-85','10','20']" data-fontsize="['100','100','','60']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="opacity:0;s:100;e:Power2.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="201.875" data-splitin="chars" data-splitout="none" data-responsive_offset="off" data-responsive="off" data-elementdelay="0.05" style="z-index: 6; white-space: nowrap; font-size: 100px; line-height: 100px; font-weight: 700; color: rgba(255, 255, 255, 1.00);text-transform:left;">ONLINE ASSESSMENT</h3>

                                                            <a class="tp-caption rev-btn   thim-slider-button" href="exams" target="_blank" id="slide-17-layer-3" data-x="['left','left','left','left']" data-hoffset="['15','15','15','15']" data-y="['middle','middle','middle','middle']" data-voffset="['20','10','80','80']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;" data-style_hover="c:rgba(51, 51, 51, 1.00);bc:rgba(0, 0, 0, 0);" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:100;e:Power2.easeIn;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-start="1300" data-splitin="none" data-splitout="none" data-actions='' data-responsive_offset="off" data-responsive="off" style="z-index: 7; white-space: nowrap; font-size: 13px; line-height: 20px; font-weight: 700; color: rgba(51, 51, 51, 1.00);text-transform:left;padding:10px 30px 10px 30px;border-color:rgba(51, 51, 51, 0);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"> PROGRAMS </a>

                                                            <div class="tp-caption  " id="slide-17-layer-4" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['40','40','30','20']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:100;" data-start="500" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"scrollbelow","offset":"0px"}]' data-basealign="slide" data-responsive_offset="off" data-responsive="off" style="z-index: 8; white-space: nowrap; font-size: 14px; line-height: 14px; font-weight: 400; color: rgba(191, 185, 184, 1.00);text-transform:left;background-color:rgba(255, 255, 255, 0);cursor:pointer;">
                                                                <div class="thim-click-to-bottom"><i class="fa  fa-chevron-down"></i></div>
                                                            </div>
                                                        </li>

                                                        <li data-index="rs-19" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="assets/demo/slide_5-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                                                            <img src="assets/demo/dummy.png" alt="" title="slide_5" width="1600" height="900" data-lazyload="assets/demo/slide_5.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>


                                                            <div class="tp-caption    thim-slider-sub-heading" id="slide-19-layer-1" data-x="['left','left','left','left']" data-hoffset="['15','15','15','15']" data-y="['middle','middle','middle','middle']" data-voffset="['-146','-150','-45','-20']" data-fontsize="['24','24','','20']" data-width="none" data-height="" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;s:1800;e:Power4.easeInOut;" data-transform_out="opacity:0;s:100;e:Power2.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="300" data-splitin="chars" data-splitout="none" data-responsive_offset="off" data-responsive="off" data-elementdelay="0.05" style="z-index: 5; max-width: px; max-width: px; white-space: nowrap; font-size: 24px; line-height: 30px; font-weight: 700; color: rgba(252, 252, 252, 1.00);text-transform:left;">WELCOME TO THE NEW WORLD OF </div>

                                                            <h3 class="tp-caption    thim-slider-heading" id="slide-19-layer-2" data-x="['left','left','left','left']" data-hoffset="['14','15','15','15']" data-y="['middle','middle','middle','middle']" data-voffset="['-75','-85','10','20']" data-fontsize="['100','100','','60']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="opacity:0;s:100;e:Power2.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="500" data-splitin="chars" data-splitout="none" data-responsive_offset="off" data-responsive="off" data-elementdelay="0.05" style="z-index: 6; white-space: nowrap; font-size: 100px; line-height: 100px; font-weight: 700; color: rgba(255, 255, 255, 1.00);text-transform:left;">ONLINE ASSESSMENT </h3>

                                                            <a class="tp-caption rev-btn   thim-slider-button" href="exams" target="_blank" id="slide-19-layer-3" data-x="['left','left','left','left']" data-hoffset="['15','15','15','15']" data-y="['middle','middle','middle','middle']" data-voffset="['20','10','80','80']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;" data-style_hover="c:rgba(51, 51, 51, 1.00);" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:100;e:Power2.easeIn;" data-mask_in="x:0px;y:[100%];" data-start="1300" data-splitin="none" data-splitout="none" data-actions='' data-responsive_offset="off" data-responsive="off" style="z-index: 7; white-space: nowrap; font-size: 13px; line-height: 20px; font-weight: 700; color: rgba(51, 51, 51, 1.00);text-transform:left;padding:10px 30px 10px 30px;border-color:rgba(0, 0, 0, 0);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"> PROGRAMS </a>

                                                            <div class="tp-caption  " id="slide-19-layer-4" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['40','40','30','20']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:100;" data-start="500" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"scrollbelow","offset":"0px"}]' data-basealign="slide" data-responsive_offset="off" data-responsive="off" style="z-index: 8; white-space: nowrap; font-size: 14px; line-height: 14px; font-weight: 400; color: rgba(191, 185, 184, 1.00);text-transform:left;background-color:rgba(255, 255, 255, 0);cursor:pointer;">
                                                                <div class="thim-click-to-bottom"><i class="fa  fa-chevron-down"></i></div>
                                                            </div>
                                                        </li>
                                                    </ul>

                                                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-grid" id="pg-12-1">
                                <div class="panel-row-style-thim-best-industry thim-best-industry panel-row-style">
                                    <div class="panel-grid-cell" id="pgc-12-1-0">
                                        <div class="so-panel widget widget_icon-box panel-first-child panel-last-child" id="panel-12-1-0-0" data-index="1">
                                            <div class="thim-widget-icon-box thim-widget-icon-box-base">
                                                <div class="wrapper-box-icon has_custom_image has_read_more text-left overlay " data-text-readmore="#ffb606">
                                                    <div class="smicon-box iconbox-left">
                                                        <div class="boxes-icon" style="width: 135px;height: 135px;">
                                                            <span class="inner-icon">
                                                                <span class="icon icon-images">
                                                                    <img src="http://educationwp.thimpress.com/wp-content/uploads/2015/10/logo-top-1.png" alt="logo-top-1" title="logo-top-1">
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="content-inner" style="width: calc( 100% - 135px - 15px);">
                                                            <div class="sc-heading article_heading">
                                                                <h3 class="heading__primary">Variety Of Tests</h3>
                                                            </div>
                                                            <a class="smicon-read sc-btn" href="exams" style="color: #ffb606;">View More<i class="fa fa-chevron-right"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-grid-cell" id="pgc-12-1-1">
                                        <div class="so-panel widget widget_icon-box panel-first-child panel-last-child" id="panel-12-1-1-0" data-index="2">
                                            <div class="thim-widget-icon-box thim-widget-icon-box-base">
                                                <div class="wrapper-box-icon has_custom_image has_read_more text-left overlay " data-text-readmore="#ffb606">
                                                    <div class="smicon-box iconbox-left">
                                                        <div class="boxes-icon" style="width: 135px;height: 135px;">
                                                            <span class="inner-icon">
                                                              <span class="icon icon-images">
                                                                  <img src="http://educationwp.thimpress.com/wp-content/uploads/2015/10/logo-top-2.png" alt="logo-top-2" title="logo-top-2">
                                                              </span>
                                                            </span>
                                                        </div>
                                                        <div class="content-inner" style="width: calc( 100% - 135px - 15px);">
                                                            <div class="sc-heading article_heading">
                                                                <h3 class="heading__primary">360° Analysis Report</h3>
                                                            </div>
                                                            <a class="smicon-read sc-btn" href="login" style="color: #ffb606;">View More<i class="fa fa-chevron-right"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-grid-cell" id="pgc-12-1-2">
                                        <div class="so-panel widget widget_icon-box panel-first-child panel-last-child" id="panel-12-1-2-0" data-index="3">
                                            <div class="thim-widget-icon-box thim-widget-icon-box-base">
                                                <div class="wrapper-box-icon has_custom_image has_read_more text-left overlay " data-text-readmore="#ffb606">
                                                    <div class="smicon-box iconbox-left">
                                                        <div class="boxes-icon" style="width: 135px;height: 135px;">
                                                            <span class="inner-icon">
                                                                <span class="icon icon-images">
                                                                    <img src="http://educationwp.thimpress.com/wp-content/uploads/2015/10/logo-top-3.png" alt="logo-top-3" title="logo-top-3">
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="content-inner" style="width: calc( 100% - 135px - 15px);">
                                                            <div class="sc-heading article_heading">
                                                                <h3 class="heading__primary"> Review Your Performance </h3>
                                                            </div>
                                                            <a class="smicon-read sc-btn" href="login" style="color: #ffb606;">View More<i class="fa fa-chevron-right"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-grid" id="pg-12-2">
                                <div class="panel-grid-cell" id="pgc-12-2-0">
                                    <div class="so-panel widget widget_heading panel-first-child" id="panel-12-2-0-0" data-index="4">
                                        <div class="thim-widget-heading thim-widget-heading-base">
                                            <div class="sc_heading text-left content text-center">
                                                <h2 class="title">Popular <strong>Packages</strong></h2>
                                                <!-- <span class="line"></span> -->
                                                <h4>Our Test Papers packages are designed by expert panel and our Test Platform interface is alike real time examination system.<br> Exam aspirants can boost his preparation and get indepth performance report.</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="cards ud-courseimpressiontracker inline-blocks-lg tac-force-lg popular-programs" data-tracking-type="mark-as-seen" data-context="home" data-subcontext="discover-popular-topics-and-courses" id="ud_courseimpressiontracker">

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row">
                      <div class="col-md-12">

                      </div>
                    </div> -->
                </div>
                <div class="checkout-block content">
                    <div class="container">
                        <div class="row">
                            <!-- <div class="col-md-10">
                                <h2>CHECK OUT ADMIN THEME! <em>Most Full Featured &amp; Powerfull Admin Theme</em></h2>
                            </div> -->
                            <div class="col-md-12 text-center">
                                <a href="exams" target="_blank" class="btn btn-success">View All Exams</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Message block BEGIN -->
                <!-- <div class="message-block content content-center valign-center" id="message-block">
                  <div class="valign-center-elem" style="position: absolute; top: 50%; margin-top: -59px; width: 100%; height: 118px;">
                    <h2>Start Your Exam Preparation Now! <strong>PRACTICE, ANALYZE AND IMPROVE!</strong></h2>
                    <a href="registration.php" class="btn btn-success btn-circle">Register Now</a>
                  </div>
                </div> -->
                <!-- Message block END -->

                <!-- Our features BEGIN -->
                <div class="choose-us-block content text-center margin-bottom-40 feature-bg">
                  <div class="overlay">
                      <div class="container">
                        <h2>SSC CGL <strong>TIER-1 2016</strong></h2>
                        <h4><strong>SSC CGL 2016</strong> is going to conduct online on the date <strong>27th August 2016</strong> as per the government policy. <br><strong>ExamPointer</strong> comes with featured package for the SSC aspiring students to boost the performance</h4>
                        <div class="row">
                          <div class="col-md-9 col-sm-9 col-xs-12 text-left">
                            <div class="feature-content">
                                <p><i class="fa fa-check-circle"></i> Test papers questions are designed by expert teachers.</p>
                                <p><i class="fa fa-check-circle"></i> Interface of the Test Platform would be similar to new pattern</p>
                                <p><i class="fa fa-check-circle"></i> Test Papers have similar sectional distribution like in live paper</p>
                                <p><i class="fa fa-check-circle"></i> Give your test as per your suitable time and place.</p>
                                <p><i class="fa fa-check-circle"></i> Detailed report would help to check preparation levels and find the weak points.</p>
                                <p><i class="fa fa-check-circle"></i> Compare yourself with all aspiring students.</p>
                            </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 text-left">
                            <img src="assets/frontend/onepage/img/ssc-logo.png" alt="Why to choose us" class="img-responsive">
                          </div>
                        </div>
                        <div class="row margin-top-20">
                          <div class="col-md-9 col-sm-9 col-xs-12 text-left">
                            <h3 class="discount">Enrolled today and get <strong>Flat 25%</strong> discount use coupon code <strong>SSC1-25</strong></h3>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-12 text-center checkout-block">
                            <a href="http://exampointer.com/exams/ssc-cgl-mock-test-2016_1" class="btn btn-success"><strong>View Details</strong></a>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
                <!-- Our features END -->


                <!-- Choose us block BEGIN -->
                <div class="choose-us-block content text-center margin-bottom-40" id="benefits">
                  <div class="container">
                    <h2>Our  <strong>Benefits</strong></h2>
                    <h4>ExamPointer is a web based interactive, independent and intelligent examination platform for students.<br> This platform covers wide range of institutions and boards.</h4>
                    <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-12 text-left">
                        <img src="assets/frontend/onepage/img/why-choose-us.png" alt="Why to choose us" class="img-responsive">
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 text-left">
                        <div class="panel-group" id="accordion1">
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h5 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_1">Content Quality</a>
                              </h5>
                            </div>
                            <div id="accordion1_1" class="panel-collapse collapse in">
                              <div class="panel-body">
                                <p>Created and reviewed by Experts and Top Faculty across the country, Find just the right content for any exam vetted by educators.</p>
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h5 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_2">Designed by experts</a>
                              </h5>
                            </div>
                            <div id="accordion1_2" class="panel-collapse collapse">
                              <div class="panel-body">
                                <p>The ExamPointer test series have been designed by India’s top experts in the respective domains while keeping the real examination pattern in mind. </p>
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h5 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_3">Rich Library</a>
                              </h5>
                            </div>
                            <div id="accordion1_3" class="panel-collapse collapse">
                              <div class="panel-body">
                                <p>We have compiled an exhaustive set of mock tests and practice exams to help students with their exam preparation. </p>
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h5 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_4">Customize Yourself</a>
                              </h5>
                            </div>
                            <div id="accordion1_4" class="panel-collapse collapse">
                              <div class="panel-body">
                                <p>Plan for EVERYONE, Customized your plan according to your need, select the best which suites you best</p>
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h5 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_5">Practice Exams</a>
                              </h5>
                            </div>
                            <div id="accordion1_5" class="panel-collapse collapse">
                              <div class="panel-body">
                                <p>Score more by practice more. Our unique designed practice module help you to gain knowledge even when you are giving exams.</p>
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h5 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_6">Time - Bound</a>
                              </h5>
                            </div>
                            <div id="accordion1_6" class="panel-collapse collapse">
                              <div class="panel-body">
                                <p>All the tests are time-bound and adhere to rules & syllabus that are applicable for the respective exams.</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Choose us block END -->
                <!-- Testimonials block BEGIN -->
                <div class="student-analytics">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="number">11,258+</div>
                        <div class="text">Students Love Us</div>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="number">66,072+</div>
                        <div class="text">Questions attempted</div>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="number">8,526+</div>
                        <div class="text">Tests attempted</div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Choose us block END -->
                <!-- Testimonials block BEGIN -->
                <div class="testimonials-block content content-center margin-bottom-65">
                  <div class="container">
                    <h2>Students <strong>testimonials</strong></h2>
                    <h4>Loved by Students across INDIA</h4>
                    <div class="carousel slide" data-ride="carousel" id="testimonials-block">
                      <!-- Wrapper for slides -->
                      <div class="carousel-inner">
                        <!-- Carousel items -->
                        <div class="active item">
                            <p>A very good question bank teamed up with an in-depth analysis of the individual subjects as well as an overall estimate of your performance are just the right ingredients a SSC aspirant needs.</p>
                          <span class="testimonials-name">Prakhar Goel</span>
                        </div>
                        <!-- Carousel items -->
                        <div class="item">
                            <p>There is no other site like Exampointer.com to provide a perfect Test series with ranking and all the right answers to know where we go wrong. And most important is the immediate results that are provided to you.</p>
                          <span class="testimonials-name">Kartik Chauhan</span>
                        </div>
                        <!-- Carousel items -->
                        <div class="item">
                            <p>A very good question bank teamed up with an in-depth analysis of the individual subjects as well as an overall estimate of your performance are just the right ingredients a SSC aspirant needs.</p>
                          <span class="testimonials-name">Deepak Kumar Singh</span>
                        </div>
                      </div>
                      <!-- Indicators -->
                      <ol class="carousel-indicators">
                        <li data-target="#testimonials-block" data-slide-to="0" class="active"></li>
                        <li data-target="#testimonials-block" data-slide-to="1"></li>
                        <li data-target="#testimonials-block" data-slide-to="2"></li>
                      </ol>
                    </div>
                  </div>
                </div>
                <!-- Testimonials block END -->
                <!-- Partners block BEGIN -->
                <div class="partners-block content">
                  <div class="container">
                    <h2>Our <strong>Clients</strong></h2>
                    <h4 class="margin-bottom-20">Loved by Students across INDIA</h4>
                    <div class="row">
                      <div class="col-md-1"></div>
                      <div class="col-md-2 col-sm-3 col-xs-12">
                        <img src="assets/frontend/pages/img/clients/arth-academy-small.png" alt="arthacademy" title="Arth Academy">
                      </div>
                      <div class="col-md-2 col-sm-3 col-xs-12">
                        <img src="assets/frontend/pages/img/clients/achieverzclasseslogo.png" alt="achieverzclasses" title="Achieverz Classes">
                      </div>
                      <div class="col-md-2 col-sm-3 col-xs-12">
                        <img src="assets/frontend/pages/img/clients/examguru-logo-small.png" alt="examguru" title="Examguru">
                      </div>
                      <div class="col-md-2 col-sm-3 col-xs-12">
                        <img src="assets/frontend/pages/img/clients/zeroinfy.JPG" alt="zeroinfy" title="Zeroinfy">
                      </div>
                      <div class="col-md-2 col-sm-3 col-xs-12">
                        <img src="assets/frontend/pages/img/clients/effort-edu-logo.jpg" alt="effort education" title="Effort Education">
                      </div>
                      <div class="col-md-1"></div>
                    </div>
                  </div>
                </div>
                <!-- Partners block END -->
                <?php @include 'new-html/footer.php';?>

            </div>


            <a href="#" id="back-to-top">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>

<div class="modal fade" id="program-details-modal" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Package Details</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4 form-group">
            <img src="assets/frontend/pages/img/works/course1.jpg" alt="Amazing Project" class="img-responsive image" style="width: 248px; height: 248px;">
          </div>
          <div class="col-md-8 form-group">
            <h2><strong class="program-name">Course 1</strong></h2>
            <p class="text-justify description"></p>
          </div>
          <!-- <div class="row">
            <div class="col-md-4 form-group">
              <button class="btn btn blue"><i class="fa fa-inr"></i> 250</button>
              <button type="button" class="btn blue"> Buy Now </button>
            </div> -->
          <div class="col-md-8 col-md-offset-2 form-group">
            <h4><strong>Number of Tests</strong></h4>
            <div class="table-scrollable" id="modal-table">
              <table class="table table-striped table-hover table-bordered">
                <thead>
                  <tr>
                    <th>Test Topic</th>
                    <th>No. of Tests</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Test Topic 1</td>
                    <td>5</td>
                  </tr>

                </tbody>
              </table>
            </div>
          </div>
          <div class="col-md-2"></div>
        </div>
        <!-- </div> -->
      </div>
      <div class="modal-footer">
        <span class="pull-left price">CAD 250</span>
        <div class="pull-right">
          <!-- <span class="promocode">Have a Promocode?</span>
          <div class="promocode-div pull-left mrg-right15" style="display:none;">
            <input type="text" class="form-control input-small pull-left mrg-right15" placeholder="Enter Promocode">
            <button type="button" class="btn grey-cascade"> Apply </button>
          </div> -->
          <button type="button" class="btn grey-cascade buy-program"> Buy Now </button>
          <button type="button" class="btn default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<div class="request-demo-div">
    <a href="demo.php" class="btn btn-success" id="demo-request">Request For Demo</a>
</div>

    <script data-cfasync="false" type="text/javascript">
        window.onload = function () {
            setTimeout(function () {
                var body = document.getElementById("thim-body"),
                    thim_preload = document.getElementById("preload"),
                    len = body.childNodes.length,
                    class_name = body.className.replace(/(?:^|\s)thim-body-preload(?!\S)/, '').replace(/(?:^|\s)thim-body-load-overlay(?!\S)/, '');

                body.className = class_name;
                if (typeof thim_preload !== 'undefined' && thim_preload !== null) {
                    for (var i = 0; i < len; i++) {
                        if (body.childNodes[i].id !== 'undefined' && body.childNodes[i].id == "preload") {
                            body.removeChild(body.childNodes[i]);
                            break;
                        }
                    }
                }
            }, 100);
        };
    </script>

    <script type='text/javascript' src='http://educationwp.thimpress.com/wp-content/themes/eduma/assets/js/main.min.js?ver=2.2.0.7'></script>

    <div class="tp_chameleon_overlay">
        <div class="tp_chameleon_progress">
            <div class="tp_chameleon_heading">Processing!</div>
        </div>
    </div>

    <script src="assets/global/plugins/bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <script src="admin/assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
    <script src="admin/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>


    <script type='text/javascript' src='assets/js/common.js'></script>
    <script type='text/javascript' src='assets/js/custom/index2.js'></script>
    <script type='text/javascript' src='assets/js/custom/index1.js'></script>
    <script src="assets/frontend/onepage/scripts/layout.js" type="text/javascript"></script>
    <script src="assets/frontend/onepage/scripts/jquery.nav.js" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        Layout.init();
      });
    </script>

</body>

</html>