<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="active">Why My Learning Graph?</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN CONTENT -->
          <div class="col-md-12 col-sm-12">
            <h1>Why My Learning Graph?</h1>
            <div class="content-page">
              <div class="row margin-bottom-30" style="text-align:justify;">
                <!-- BEGIN INFO BLOCK -->               
                <div class="col-md-7">
                  <p><strong>AIETS (All India Educational Testing Services)</strong> brings a unique & innovation learning program with is platform: <strong>My Learning Graph (MLG)</strong>.</p>
                  <p>Competitive exams are not merely a test of subject knowledge but also a test of application of knowledge in examination hall. But frequently it is seen that student spends 99% of his time in gaining subject knowledge and ignores/postpones the testing part. The result, he does not come to know his strong and weak areas until the end of preparation, which at times is too late to react. Remember, tests provide answers. They help a student by highlighting the problems and initiating the corrective measures.</p>
                  <p>MLG is an idea envisaged by AIETS over its 16 years of expertise.  We aim to provide a goal based online practice platform for students preparing for various competitive exams and also to those who are studying in schools from classes 1 to 12.</p>
                  <p>Our platform provides complete practice for every exam in the form of tests like Diagnostic, Full Syllabus, Scheduled, Chapter and Practice Tests. A quick recap of each concept under various subjects and chapters are also provided in Video and Text formats in our QCR & Illustration section.</p>
                </div>
                  <!--<h2 class="no-top-space">Vero eos et accusamus</h2>-->
                  <!-- <p>Test Engine is an online Test platform for the students who are studding in the different courses or preparing for the different comparative exams. This solution will work for the different people who are working in education domain.</p> 
                  <p>TestEngine is a one-stop-solution making course and college selection easy for students looking to pursue undergraduate (UG) and postgraduate (PG) courses in India. We offer specific information for students interested in UG/PG courses in India.</p> -->
				  <!--<p>Education seekers get a personalised experience on our site, based on educational background and career interest, enabling them to make well informed course and college decisions. The decision making is empowered with easy access to detailed information on career choices, courses, exams, colleges, admission criteria, eligibility, fees, placement statistics, rankings, reviews, scholarships, latest updates etc as well as by interacting with other (TestEngine's) users, experts, current students in colleges.</p>-->
				  <!-- <p>aietstestengine.com was founded in 1990 and is headquartered in India.</p> -->
                  <!-- BEGIN LISTS -->
                  <!--<div class="row front-lists-v1">
                    <div class="col-md-6">
                      <ul class="list-unstyled margin-bottom-20">
                        <li><i class="fa fa-check"></i> Officia deserunt molliti</li>
                        <li><i class="fa fa-check"></i> Consectetur adipiscing </li>
                        <li><i class="fa fa-check"></i> Deserunt fpicia</li>
                      </ul>
                    </div>
                    <div class="col-md-6">
                      <ul class="list-unstyled">
                        <li><i class="fa fa-check"></i> Officia deserunt molliti</li>
                        <li><i class="fa fa-check"></i> Consectetur adipiscing </li>
                        <li><i class="fa fa-check"></i> Deserunt fpicia</li>
                      </ul>
                    </div>
                  </div>-->
                  <!-- END LISTS -->
                <!-- END INFO BLOCK -->   

                <!-- BEGIN CAROUSEL -->            
                <div class="col-md-5 front-carousel">
                  <div id="myCarousel" class="carousel slide">
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                      <div class="item active">
                        <img src="assets/frontend/pages/img/pics/school-studends.jpg" alt="">
                        <!--<div class="carousel-caption">
                          <p>Excepturi sint occaecati cupiditate non provident</p>
                        </div>-->
                      </div>
                      <div class="item">
                        <img src="assets/frontend/pages/img/pics/classroom.jpg" alt="">
                        <!--<div class="carousel-caption">
                          <p>Ducimus qui blanditiis praesentium voluptatum</p>
                        </div>-->
                      </div>
                      <div class="item">
                        <img src="assets/frontend/pages/img/pics/books.jpg" alt="">
                        <!--<div class="carousel-caption">
                          <p>Ut non libero consectetur adipiscing elit magna</p>
                        </div>-->
                      </div>
                    </div>
                    <!-- Carousel nav -->
                    <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                      <i class="fa fa-angle-left"></i>
                    </a>
                    <a class="carousel-control right" href="#myCarousel" data-slide="next">
                      <i class="fa fa-angle-right"></i>
                    </a>
                  </div>                
                </div>
                <!-- END CAROUSEL -->

                <div class="col-md-12">
                  <p>We provide thousands of questions in multiple formats like single choice, multiple choices, fill in the blanks, matrix/rating scale, inter type etc., across various exams created over our long period of expertise. Detailed solutions are also available for each question to better understand each question and its corresponding answer.</p>
                  <p>Detailed analysis keeps you motivated through your practice. Performance Trackers and other reports like Speed Reports, All India Analysis, Success and Topper Gap Reports help you gauge your improvement over a period of time.</p>
                </div>
              </div>
            </div>

              <!--<div class="row margin-bottom-40">-->
                <!-- BEGIN TESTIMONIALS -->
                <!--<div class="col-md-7 testimonials-v1">
                  <h2>Clients Testimonials</h2>                
                  <div id="myCarousel1" class="carousel slide">
                    <!-- Carousel items -->
                    <!--<div class="carousel-inner">
                      <div class="active item">
                        <blockquote><p>Denim you probably haven't heard of. Lorem ipsum dolor met consectetur adipisicing sit amet, consectetur adipisicing elit, of them jean shorts sed magna aliqua. Lorem ipsum dolor met consectetur adipisicing sit amet do eiusmod dolore.</p></blockquote>
                        <div class="carousel-info">
                          <img class="pull-left" src="assets/frontend/pages/img/people/img1-small.jpg" alt="">
                          <div class="pull-left">
                            <span class="testimonials-name">Lina Mars</span>
                            <span class="testimonials-post">Commercial Director</span>
                          </div>
                        </div>
                      </div>
                      <div class="item">
                        <blockquote><p>Raw denim you Mustache cliche tempor, williamsburg carles vegan helvetica probably haven't heard of them jean shorts austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica.</p></blockquote>
                        <div class="carousel-info">
                          <img class="pull-left" src="assets/frontend/pages/img/people/img5-small.jpg" alt="">
                          <div class="pull-left">
                            <span class="testimonials-name">Kate Ford</span>
                            <span class="testimonials-post">Commercial Director</span>
                          </div>
                        </div>
                      </div>
                      <div class="item">
                        <blockquote><p>Reprehenderit butcher stache cliche tempor, williamsburg carles vegan helvetica.retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid Aliquip placeat salvia cillum iphone.</p></blockquote>
                        <div class="carousel-info">
                          <img class="pull-left" src="assets/frontend/pages/img/people/img2-small.jpg" alt="">
                          <div class="pull-left">
                            <span class="testimonials-name">Jake Witson</span>
                            <span class="testimonials-post">Commercial Director</span>
                          </div>
                        </div>
                      </div>
                    </div>-->
                    <!-- Carousel nav -->
                    <!--<a class="left-btn" href="#myCarousel1" data-slide="prev"></a>
                    <a class="right-btn" href="#myCarousel1" data-slide="next"></a>
                  </div>
                </div>-->
                <!-- END TESTIMONIALS --> 

                <!-- BEGIN PROGRESS BAR -->
                <!--<div class="col-md-5 front-skills">
                  <h2 class="block">Our Skills</h2>
                  <span>UI Design 90%</span>
                  <div class="progress">
                    <div role="progressbar" class="progress-bar" style="width: 90%;"></div>
                  </div>
                  <span>Wordpress CMS 60%</span>
                  <div class="progress">
                    <div role="progressbar" class="progress-bar" style="width: 60%;"></div>
                  </div>
                  <span>HTML/CSS &amp; JavaScirp 75%</span>
                  <div class="progress">
                    <div role="progressbar" class="progress-bar" style="width: 75%;"></div>
                  </div>
                </div>-->                    
                <!-- END PROGRESS BAR -->
              <!--</div>-->
			  
			  <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN CONTENT -->
          <div class="col-md-12 col-sm-12">
            <h1>What Do We Offer ?</h1>
            <div class="content-page we-offer">
              <div class="row">
                <!-- BEGIN SERVICE BLOCKS -->               
                <div class="col-md-12">
                  <div class="row margin-bottom-20">
                    <div class="col-md-4">
                      <div class="service-box-v1">
                        <div><i class="fa fa-crosshairs color-grey"></i></div>
                        <h2>Goal based learning</h2>
                        <p>Each Program has defined Goals & Learning Objectives which continuously measures your progress and keeps you on track.</p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="service-box-v1">
                        <div><i class="fa fa-tachometer color-grey"></i></div>
                        <h2>Unlimited Practice</h2>
                        <p>Huge repository of different categories of questions. Detailed solutions for all questions with Special focus on shortcuts & speed tips to reduce attempt time.</p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="service-box-v1">
                        <div><i class="fa fa-bar-chart color-grey"></i></div>
                        <h2>QCR & Illustration</h2>
                        <p>Concepts Sheets with Illustrative Examples for Quick Revision & Practice<br><br></p>
                      </div>
                    </div>
					          <div class="col-md-4">
                      <div class="service-box-v1">
                        <div><i class="fa fa-gavel color-grey"></i></div>
                        <h2>Powerful Detailed Analysis</h2>
                        <p>Detailed analysis that will pinpoint the areas of strengths, weakness and test taking strategy. Comparison of your score with toppers and cut-offs tell you Toppers Gap and Success Gap.</p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="service-box-v1">
                        <div><i class="fa fa-book color-grey"></i></div>
                        <h2>Study Planner</h2>
                        <p>A Unique tool which helps in making a Workable Plan based on your Goal, course covered and time available.</p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="service-box-v1">
                        <div><i class="fa fa-line-chart color-grey"></i></div>
                        <h2>Score optimizer</h2>
                        <p>A unique tool/methodology which helps in optimizing your test taking techniques to maximize your score.</p>
                      </div>
                    </div>
                  </div>
                  <!-- <div class="row margin-bottom-20">
                    <div class="col-md-3">
                      <div class="service-box-v1">
                        <div><i class="fa fa-dropbox color-grey"></i></div>
                        <h2>Exam Result Processing</h2>
                        <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse.</p>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="service-box-v1">
                        <div><i class="fa fa-gift color-grey"></i></div>
                        <h2>Knowledge Management</h2>
                        <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse.</p>
                      </div>
                    </div>
					          <div class="col-md-3">
                      <div class="service-box-v1">
                        <div><i class="fa fa-dropbox color-grey"></i></div>
                        <h2>Payment Management</h2>
                        <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse.</p>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="service-box-v1">
                        <div><i class="fa fa-gift color-grey"></i></div>
                        <h2>Report Management</h2>
                        <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse.</p>
                      </div>
                    </div>
                  </div> -->
                </div>
                <!-- END SERVICE BLOCKS --> 
			        </div>
		        </div>
	        </div>
	      </div>

              <!--<div class="row front-team">
                <ul class="list-unstyled">
                  <li class="col-md-3">
                    <div class="thumbnail">
                      <img alt="" src="assets/frontend/pages/img/people/img1-large.jpg">
                      <h3>
                        <strong>Lina Doe</strong> 
                        <small>Chief Executive Officer / CEO</small>
                      </h3>
                      <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...</p>
                      <ul class="social-icons social-icons-color">
                        <li><a class="facebook" data-original-title="Facebook" href="#"></a></li>
                        <li><a class="twitter" data-original-title="Twitter" href="#"></a></li>
                        <li><a class="googleplus" data-original-title="Goole Plus" href="#"></a></li>
                        <li><a class="linkedin" data-original-title="Linkedin" href="#"></a></li>
                      </ul>
                    </div>
                  </li>
                  <li class="col-md-3">
                    <div class="thumbnail">
                      <img alt="" src="assets/frontend/pages/img/people/img4-large.jpg">
                      <h3>
                        <strong>Carles Puyol</strong> 
                        <small>Chief Executive Officer / CEO</small>
                      </h3>
                      <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...</p>
                      <ul class="social-icons social-icons-color">
                        <li><a class="facebook" data-original-title="Facebook" href="#"></a></li>
                        <li><a class="twitter" data-original-title="Twitter" href="#"></a></li>
                        <li><a class="googleplus" data-original-title="Goole Plus" href="#"></a></li>
                        <li><a class="linkedin" data-original-title="Linkedin" href="#"></a></li>
                      </ul>
                    </div>
                  </li>
                  <li class="col-md-3">
                    <div class="thumbnail">
                      <img alt="" src="assets/frontend/pages/img/people/img2-large.jpg">
                      <h3>
                        <strong>Andres Iniesta</strong> 
                        <small>Chief Executive Officer / CEO</small>
                      </h3>
                      <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...</p>
                      <ul class="social-icons social-icons-color">
                        <li><a class="facebook" data-original-title="Facebook" href="#"></a></li>
                        <li><a class="twitter" data-original-title="Twitter" href="#"></a></li>
                        <li><a class="googleplus" data-original-title="Goole Plus" href="#"></a></li>
                        <li><a class="linkedin" data-original-title="Linkedin" href="#"></a></li>
                      </ul>
                    </div>
                  </li>
                  <li class="col-md-3">
                    <div class="thumbnail">
                      <img alt="" src="assets/frontend/pages/img/people/img5-large.jpg">
                      <h3>
                        <strong>Jessica Alba</strong> 
                        <small>Chief Executive Officer / CEO</small>
                      </h3>
                      <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...</p>
                      <ul class="social-icons social-icons-color">
                        <li><a class="facebook" data-original-title="Facebook" href="#"></a></li>
                        <li><a class="twitter" data-original-title="Twitter" href="#"></a></li>
                        <li><a class="googleplus" data-original-title="Goole Plus" href="#"></a></li>
                        <li><a class="linkedin" data-original-title="Linkedin" href="#"></a></li>
                      </ul>
                    </div>
                  </li>
                </ul>            
              </div>-->

            </div>
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
    <!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>