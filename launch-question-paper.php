<?php include 'Access-API.php'; ?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<style type="text/css">
	ul{
		list-style-type: none;
	}
	</style>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Start Header -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 question-header">
				<div class="row">
					<div class="col-md-3 col-sm-3">
						<h4 class="TestName"><strong class="test-paper-name"><!-- SBIPO-01 --></strong></h4>
					</div>
					<div class="col-md-9 col-sm-9">
						<h3 class="TestOrganiser"><strong> <!-- Aiets-TestEngine --></strong></h3>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9 padd15">
				<div class="col-md-12 border-bbb mrg15">
					<label><strong>Sections</strong></label><br>
					<div id="sections">
						<!-- <button type="button" class="btn blue mrg-bot5">English Language</button>
						<button type="button" class="btn btn-info mrg-bot5">Quantitative Apptitude</button>
						<button type="button" class="btn btn-info mrg-bot5">Logical Reasoning</button> -->
					</div>
				</div>
				<div id="questions" class="col-md-12 border-bbb">
					<!-- <div class="from-group">
						<label><strong>Question No 1</strong></label><br>
					</div>
					<div class="col-md-12">
						<div class="row border-top-bbb">
							<div class="col-md-12 border-right-bbb mrg15">
								
								<h5><strong>The narrator describes the pigeon as a 'revolting bird' because</strong></h5>
								<div class="radio-list">
									<label>
									<input type="radio" name="optionsRadios" id="optionsRadios22" value="option1"> he could not fly </label>
									<label>
									<input type="radio" name="optionsRadios" id="optionsRadios22" value="option1"> he had to be carried everywhere </label>
									<label>
									<input type="radio" name="optionsRadios" id="optionsRadios22" value="option1"> he had wrinkled skin covered with yellow feathers </label>
									<label>
									<input type="radio" name="optionsRadios" id="optionsRadios22" value="option1"> he was fat </label>
								</div>
							</div>
						</div>
						
					</div> -->
				</div>

				<div class="col-md-12 save-btnBox border-bbb">
					<div class="padd7">
						
						<div class="width100per">
							<a id="mark-review-next" class="btn blue mrg-bot5">Mark For Review & Next</a>
						</div>
						<div class="width100per">
							<a id="clear-response" disabled="true" class="btn blue mrg-bot5">Clear Response</a>
						</div>
						<div class="width100perR">
							<a id="save-next" class="btn blue mrg-bot5">Save & Next</a>
						</div>
					</div>
				</div>
				
				
			</div>
			<div class="col-md-3">
				<div class="row">
					<div class="col-md-12">
						<!-- <div class="col-md-6 col-xs-6 text-center">
							<img src="assets/frontend/layout/img/img12 copy.png" class="img-responsive padd20" />
							<i class="fa fa-user profile-icon"></i>
							<p style="margin-top: 3px; font-size: 9px;"><strong>Your photo appears here</strong></p>
						</div> -->
						<div class="col-md-12 col-xs-12 padd-top20">
							<p><strong id="countdown-timer" class="countdown-timer"></strong></p>
<!-- 							<p><a class="deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-pause"></i></a> <strong>Pause Test</strong></p>
 -->						</div>
					</div>
				</div>
				<div class="col-md-12 QuestionPalette-bg">
					<!-- <p>You are viewing <strong>EL</strong> section</p> -->
					<p>Question Palette:</p>
					<div class="row">
						<div class="form-group">
							<div class="scroller QuestionPalette-height" data-always-visible="1" data-rail-visible="0">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-2"></div>
										<div id="question-numbering" class="col-md-8 mrg-bot5 text-center">
											<!-- <a class="deleteOption btn btn-circle btn-icon-only red" style="margin-bottom: inherit;">1</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">2</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">3</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">4</a>

											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">17</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">18</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">19</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">20</a>

											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">21</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">22</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">23</a>
											<a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">24</a> -->
										</div>
										<div class="col-md-2"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<p><strong>Legend</strong></p>
								<div class="row">
									<div class="col-md-6">
										<h5><a class="deleteOption btn btn-circle btn-icon-only green"></a> Answered</h5>
									</div>
									<div class="col-md-6">
										<h5><a class="deleteOption btn btn-circle btn-icon-only red"></a> Not Answered</h5>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<h5><a class="deleteOption btn btn-circle btn-icon-only blue"></a> Marked</h5>
									</div>
									<div class="col-md-6">
										<h5><a class="deleteOption btn btn-circle btn-icon-only btn-default"></a> Not Visited</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<div class="btn-group-justified">
												<a class="btn blue" id="show-all-questions-modal"> Question Paper </a>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<div class="btn-group-justified">
												<a class="btn blue" href="#instructions-modal" data-toggle="modal">Instructions</a>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<!-- <div class="col-md-6">
										<div class="form-group">
											<div class="btn-group-justified">
												<a class="btn btn-info">Profile</a>
											</div>
										</div>
									</div> -->
									<div class="col-md-12">
										<div class="form-group">
											<div class="btn-group-justified">
												<a data-toggle="modal" href="#submit-div" id="submit" class="btn blue">Submit</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- /.modal -->
	<div class="modal fade bs-modal-lg" id="all-questions-modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Question Paper</h4>
				</div>
				<div class="modal-body">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	<!-- Start Instructions modal -->
	<div class="modal fade bs-modal-lg" id="instructions-modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Instructions</h4>
				</div>
				<div class="modal-body">
					<h4 class="question-paper-intruction-heading">
						<span>General Instructions</span><br>
						Please read the following instructions very carefully.
					</h4>
					<div id="test_paper_instructions"> </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- End Instructions modal -->

	<!-- start submit modal -->
	<div class="modal fade bs-modal-lg" id="result-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header ">
					<!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button> -->
					<h4 class="modal-title">Your Score</h4>
				</div>
				<div class="modal-body">
					<h3> 
					   Please Wait while result is generating...
					</h3>
					
					<div id="result-progressbar-div" class="progress" style="display: none;">
						<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">
					    	<h4> Please Wait... </h4>
						</div>
					</div>
					
					<div id="result-marks-div" style="display: none;">
						<h2 class="text-center">You Got <span id="marks-archived"></span> Out Of <span id="total-marks" >.</span></h2>
						<h3  class="optmizer text-center">Do you want to optimize this test ?</h3>
					</div>

				</div>
				<div class="modal-footer">
					<a id="create-test-optmizer" class="optmizer btn blue" disabled="true"> Create Optmizer </a>
					<a id="go-to-test-history" title="Go to Result Details" href="#" class="btn default" disabled="true"> See Results </a>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- end submit modal -->

	
	<div id="loader" style="width: 100%; height: 100%; position: fixed; top: 0px; left: 0px; z-index: 10000; display: none; background: rgba(0, 0, 0, 0.309804);">
		<img src="assets/global/img/ajax-loader.gif" style="position: fixed;
		top: 50%;
		left: 50%;">
	</div>

	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/countdown/jquery.countdown.min.js" type="text/javascript"></script>
	<script src="assets/js/custom/launch-question-paper/launch-question-paper.js" type="text/javascript"></script>
	<script src="assets/js/custom/launch-question-paper/launch-question-paper-events.js" type="text/javascript"></script>
    <script type="text/javascript">
	    /*
		* function to show ajax loader
		*/
		$(document).ajaxStart(function(){
		    $('#loader').show();
		});

		/*
		* function to hide ajax loader
		*/
		$(document).ajaxComplete(function(){
		    $('#loader').hide();
		});
		
	$(function() {
		//headerNotifications();
		//setNoteEvents();
	});
    </script>
	<!-- END CORE PLUGINS -->
	
<!-- END PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>