<?php
  require_once 'config.inc.test.php';
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <?php include('html/head-tag.php'); ?>
  <link href="admin/assets/admin/pages/css/error.css" rel="stylesheet" type="text/css">
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="page-404-full-page">

  <!-- Navigation START -->
  <?php include('html/navigation.php'); ?>
  <!-- Navigation END -->

  <div class="row">
    <div class="col-md-12 page-404 mrg-bot100">
      <div class="number">
         404
      </div>
      <div class="details">
        <h3>Page Not Found</h3>
        <p>
           We can not find the page you're looking for.<br/>
          Click to
          <a href="<?=URI1?>">
          Return home </a>
        </p>
       
        <!-- <form action="#">
          <div class="input-group input-medium">
            <input type="text" class="form-control" placeholder="keyword...">
            <span class="input-group-btn">
            <button type="submit" class="btn blue"><i class="fa fa-search"></i></button>
            </span>
          </div>
        </form> -->
      </div>
    </div>
  </div>

  <!-- BEGIN PRE-FOOTER -->
  <?php include('html/footer.php'); ?>
  <!-- END FOOTER -->

	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
    <!-- // <script type="text/javascript" src="assets/js/custom/index.js"></script> -->
    <!-- END PAGE LEVEL JAVASCRIPTS -->


</body>
<!-- END BODY -->
</html>