<?php
	require_once 'config.inc.php';
	require_once 'config.inc.test.php';
	@session_start();
	if (!isset($_SESSION['username']))
	{
		header('Location: profile');
	}
	include 'Access-API.php';
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
	<?php include HEADER; ?>
	<!-- Navigation END -->

	<div class="main" >
		<div class="container">
			<ul class="breadcrumb">
				<li><a href="<?=URI1?>">Home</a></li>
				<li class="active">My Certificates</li>
			</ul>
			<!-- BEGIN SIDEBAR & CONTENT -->
			<div class="row margin-bottom-40">
				<!-- BEGIN SIDEBAR -->
				<!-- <div class="sidebar col-md-2 col-sm-3">
				<?php //include('html/student/sidebar.php'); ?>
				</div> -->
				<!-- END SIDEBAR -->

				<!-- BEGIN CONTENT -->
				<div class="col-md-12 col-sm-12">
					<!-- <div class="row">
						<div class="col-md-6 col-sm-6">
							<h2>Currently You have no Certificate.</h2>
						</div>
						<div class="col-md-6 col-sm-6"></div>
					</div> -->
					<div class="row margin-bottom-20" id="CertificationDescriptions">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>S No.</th>
									<th>Package</th>
									<th>Transaction Hash</th>
									<th>Download</th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>

				</div>
			<!-- END CONTENT -->
			</div>
		<!-- END SIDEBAR & CONTENT -->
		</div>
	</div>

	<!-- BEGIN PRE-FOOTER -->
	<?php include FOOTER; ?>
	<!-- END FOOTER -->

	<!-- /modal -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
	<?php include('html/js-files.php'); ?>
	 <?php include('html/student/js-files.php'); ?>

	<script src="assets/js/custom/my-certification.js" type="text/javascript"></script>

	<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
