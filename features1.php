<?php
  include 'config.inc.test.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Effort Education</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- Loading materilize -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="blade/main/css/style.css" rel="stylesheet">
  <link href="blade/main/css/materialize.min.css" rel="stylesheet">
  <link href="blade/main/css/materialdesignicons.min.css" rel="stylesheet">
  <link rel="shortcut icon" href="images/favicon.ico">
  <link href="blade/main/fa/css/font-awesome.css" rel="stylesheet">
</head>

<body>
  <!-- Preloader -->
<!-- <div id="preloader">
  <div id="status"></div>
</div> -->
<?php
  include HEADER;
?>

<!-- Body BEGIN -->
<body>
          <div class="services-block">
            <div class="container">
               <div class="row">
                 <div class="col s12 m12 l12">
                <h2 class="center">Our <strong class=" blue-text">Features</strong></h2>
                <p>This platform works perfectly on any device like mobile, tab, laptop, desktop, and on any platform
                   like i.e Windows, Mac or Linux. This revolutionary product is a must for every institution who wants to make their pupil high achievers.
                </p>
               </div>
              </div>
              <div class="row features_content ">
                  <div class="col s12 m4 l4 center">
                      <img src="blade/main/images/quality.png">
                      <h3>Content Quality</h3>
                      <p> Created and reviewed by Experts and Top Faculty across the country,
                       Find just the right content for any exam vetted by educators.</p>
                  </div>
                  <div class="col s12 m4 l4 center">
                       <img src="blade/main/images/quality.png">
                      <h3>Designed by experts</h3>
                      <p>The ExamPointer test series have been designed by India’s top experts in the respective domains while keeping the real examination pattern in mind.
                      </p>
                  </div>
                  <div class="col s12 m4 l4 center">
                       <img src="blade/main/images/quality.png">
                      <h3>Rich Library</h3>
                      <p>We have compiled an exhaustive set of mock tests and practice exams to help students with their exam preparation.
                      </p>
                  </div>
                     </div>
              <div class="row features_content">
                  <div class="col s12 m4 l4 center">
                       <img src="blade/main/images/quality.png">
                      <h3>Customize Yourself</h3>
                      <p>Plan for EVERYONE, Customized your plan according to your need, select the best which suites you best
                      </p>
                  </div>
                  <div class="col s12 m4 l4 center">
                       <img src="blade/main/images/quality.png">
                      <h3>Practice Exams</h3>
                      <p>Score more by practice more. Our unique designed practice
                        module help you to gain knowledge even when you are giving exams.
                      </p>
                  </div>
                  <div class="col s12 m4 l4 center">
                       <img src="blade/main/images/quality.png">
                      <h3>Time - Bound</h3>
                      <p> All the tests are time-bound and adhere to rules & syllabus that are applicable for the respective exams.
                      </p>
                  </div>
              </div>
            </div>
              <!-- <div class="row">
                  <div class="col-md-4 col-sm-4 col-xs-12 item">
                    <i class="fa fa-laptop medium circle-white"></i>
                    <h3>Smart Devices Support</h3>
                    <p>We made the comfort of GINI for you, Enjoy the power of
                      running exams anytime, anywhere using you smart devices.</p>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12 item">
                    <i class="fa fa-male medium circle-white"></i>
                    <h3>Customize Yourself</h3>
                    <p>Plan for EVERYONE, Customized your plan according to your need, select
                      the best which suites you best.</p>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12 item">
                    <i class="fa fa-bell medium circle-white"></i>
                    <h3>SMS & Email Notifications</h3>
                    <p> Stay updated even in ZERO network, we will update you wherever you are
                     in the world with our world class Notification platform.</p>
                  </div>
              </div> -->
            </div>
          </div>
                  <!-- END CONTENT -->


  <!--Footer-->
<?php
  include FOOTER;
?>



</body>
<!-- END BODY -->
</html>