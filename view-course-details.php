<?php include 'Access-API.php'; ?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <!-- <li><a href="#">Student</a></li> -->
            <li><a href="my-program.php">My Program</a></li>
            <li><a class="test-program"><!-- Program Name --></a></li>
            <li><a class="subject">Subject</a></li>
            <li><a class="chapter">Chapter</a></li>
            <!-- <li><a class="back-selected-chapter">Test Types</a></li>
            <li class="active">View test</li> -->
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          <div class="sidebar col-md-2 col-sm-3">
            <?php include('html/student/sidebar.php'); ?>
          </div>
          <!-- END SIDEBAR -->

          <!-- BEGIN CONTENT -->
          <div class="col-md-10 col-sm-9">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2>View Test Type Details</h2>
				</div>
				<div class="col-md-6 col-sm-6"></div>
			</div>
			
			<!-- BEGIN DASHBOARD STATS -->
			<div class="row">
				<div class="col-md-12">
					<div class="table-scrollable">
						<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
							<thead>
							<tr>
								<th>
									 Test Name
								</th>
								<!-- <th>
									 Start Date
								</th>
								<th>
									 End Date
								</th> -->
								<th>
									 Total Attempts
								</th>
								<th>
									Rem Attempts
								</th>
								<th>
									Action
								</th>
								<th>
									Result
								</th>
							</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
			<!-- END DASHBOARD STATS -->
			
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
    <!-- END FOOTER -->

	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
	<?php include('html/student/js-files.php'); ?>
	
	<script src="assets/js/custom/student_program/view-course-details.js" type="text/javascript"></script>

    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>