<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
		<ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="active">Courses</li>
        </ul>
        <!-- Start Disclaimer Terms BOX -->   
        <div class="row service-box">
          <div class="col-md-12 col-sm-12">
              <h2>Courses</h2>
          </div>
        </div>
		<div class="row service-box margin-bottom-40">
          <div class="col-md-12 col-sm-12" style="text-align:justify;">
			<p>AIETS has the full rights to change the contents of any programs, resources or any information provided on the website at its discretion. Further AIETS can change the prices, eligibility, period of offer, offers expiry or the contents of the products and services being offered on the website from time to time. All these changes, if made, will be applicable to all the registered and non-registered users of the website.</p>
			<p>AIETS do not take any legal responsibility for any errors or misrepresentations that might have crept in. We have tried and made ourbest efforts to provide accurate uptodate information on the website. However we do make periodic changes in the website so as to provide most updated and errorless contents and informations. However we do make periodic changes in the website so as to provide most updated and errorless contents and informations.</p>
			
          </div>
        </div>
        <!-- End Disclaimer Terms BOX -->

      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
    <!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>