<?php include 'Access-API.php'; ?>
<?php include 'Access-Assistant-API.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<link href="admin/assets/global/plugins/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/ media="all">
	<?php include('html/student/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
	<?php include HEADER; ?>

	<!-- Navigation END -->

	<div class="main">
	  <div class="container">
		<ul class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li><a href="#">Institute</a></li>
			<li class="active">Manage Students</li>
		</ul>
		<!-- BEGIN SIDEBAR & CONTENT -->
		<div class="row margin-bottom-40">
		  <!-- BEGIN SIDEBAR -->
		  <div class="sidebar col-md-3 col-sm-3">
			<?php include('html/student/sidebar.php'); ?>
		  </div>
		  <!-- END SIDEBAR -->

		  <!-- BEGIN CONTENT -->
		  <div class="col-md-9 col-sm-7">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2>
						Manage Students</h2>
				</div>
				<div class="col-md-6 col-sm-6"></div>
			</div>
			<div class="row mrg-bot20">
				<div class="col-md-12">
					<div class="margin-top-10">
						<table id="students-table" class="tb-questionbank table table-striped table-hover table-bordered">
							<thead>
								<tr>
									<th class="no-sort">
										Sr. No.
									</th>
									<th>
										Student Name
									</th>
									<th>
										Email
									</th>
									<th>
										Phone No
									</th>
									<th class="no-sort">
										Action
									</th>
								</tr>
							</thead>
							<tbody></tbody>
							<tfoot></tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		  </div>
		  <!-- END CONTENT -->
		</div>
		<!-- END SIDEBAR & CONTENT -->
	  </div>
	</div>
	<div class="modal fade" id="edit-assistant-password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Edit Assistant Password</h4>
				</div>
				<form id="edit-assistant-password-form" role="form">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Password</label><span class="required">* </span>
									<div class="row">
										<div class="col-sm-12">
											<input type="Password" class="form-control" id="password" placeholder="Password" required="required">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label for="confirm-password" class="control-label">Confirm Password <span class="require">*</span></label>
									<div class="row">
										<div class="col-sm-12">
											<input type="password" class="form-control" id="confirm-password" placeholder="Confirm Password" required="required">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
						<div class="modal-footer">
							<button type="submit" class="btn blue">Edit</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
				</form>
			</div>
		</div>
	</div>
	<?php include FOOTER; ?>

	<!-- END FOOTER -->

	<!-- /modal -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
	<?php include('html/js-files.php'); ?>
	<?php include('html/student/js-files.php'); ?>
	<script type="text/javascript" src="admin/assets/global/plugins/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="admin/assets/global/plugins/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/custom/institute/assistant-profile.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->

<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>