<?php
    require_once 'config.inc.test.php';    
?>
<?php include 'Access-API.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <!-- <ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li><a href="my-program.php">My Package</a></li>
            <li class="active">Leaderboard</li>
        </ul> -->
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">

          <!-- BEGIN CONTENT -->
          <div class="col-md-12 col-sm-12">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2 id="paper-name"></h2>
				</div>
				<div class="col-md-6 col-sm-6"></div>
			</div>
			
			<!-- BEGIN DASHBOARD STATS -->
			<div class="row">
				<div class="col-md-12">
					<div class="table-scrollable">
						<table class="table table-striped table-hover table-bordered table-leader-board" id="leaderboard">
							<thead>
								<tr>
									<th>Rank</th>
									<th>Image</th>
									<th>Name</th>
									<th>Total Question</th>
									<th>Attempted Question</th>
									<th>Correct Question</th>
									<th>Net Score</th>
								</tr>
							</thead>
							<tbody>
								<!-- <tr>
									<td>550</td>
									<td><img src="assets/frontend/pages/img/people/man-icon.png" class="img-responsive"></td>
									<td>Ravi Arya</td>
									<td>200</td>
									<td>190</td>
									<td>175</td>
									<td>685</td>
								</tr>
								<tr>
									<td>850</td>
									<td><img src="assets/frontend/pages/img/people/man-icon.png" class="img-responsive"></td>
									<td>Utkarsh Pandey</td>
									<td>200</td>
									<td>160</td>
									<td>145</td>
									<td>565</td>
								</tr>
								<tr>
									<td>1250</td>
									<td><img src="assets/frontend/pages/img/people/man-icon.png" class="img-responsive"></td>
									<td>Deepak Kumar Singh</td>
									<td>200</td>
									<td>150</td>
									<td>135</td>
									<td>525</td>
								</tr> -->
							</tbody>
							<!-- <tfoot>
								<th colspan="7" class="text-center">
									&nbsp;				
								</th>
							</tfoot> -->
						</table>
					</div>
				</div>
			</div>
			<!-- END DASHBOARD STATS -->

          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include('new-html/footer.php'); ?>
    <!-- END FOOTER -->

	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
	<?php include('html/student/js-files.php'); ?>

	<script src="assets/js/custom/leaderboard.js" type="text/javascript"></script>

    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>