<?php

	@session_start();
	if (isset($_SESSION['role']))
	{
		if($_SESSION['role'] == 4)
		{
			include 'student-navigation.php';
		}
		elseif($_SESSION['role'] == 5)
		{
			include 'institute-navigation.php';
		}
		elseif($_SESSION['role'] == 6)
		{
			include 'assistant-navigation.php';
		}
	}
	else
	{
		include 'public-navigation.php';
	}
?>