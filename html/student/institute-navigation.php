<!-- <li><a class="btn green" data-toggle="modal" href="#redeem-coupon-modal">Apply Coupon</a></li> -->
<li class="dropdown dropdown-user header-menu-li-user">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
	<img alt="" class="user-image img-circle" src="admin/assets/admin/layout/img/avatar3_small.jpg" style="width: 29px; height: 29px;"/>
	<span class="user-name username-hide-on-mobile"></span>
	<i class="fa fa-angle-down"></i></a>
	<ul class="dropdown-menu dropdown-menu-default">
		<li><a href="institute-profile.php">My Profile</a></li>
		<li><a href="manage-students.php">Manage Students</a></li>
		<li><a href="institute-report-card.php">Report Card</a></li>
		<li><a href="logout.php"> Log Out </a></li>
	</ul>
</li>