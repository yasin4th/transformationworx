
<!-- 
    <div class="pre-header">
        <div class="container">
            <div class="row">
               
                <div class="col-md-6 col-sm-6 additional-shop-info">
                  <?php if(!isset($_SESSION['mode'])) { ?>
                    <ul class="list-unstyled list-inline">
                        <li><i class="fa fa-phone"></i><span>+91-22-86868686</span></li>
                        <li><a href="mailto:support@e-learningdmo.com"><i class="fa fa-envelope-o"></i><span>support@e-learningdmo.com</span></a></li>
                    </ul>
                  <?php } ?>

                </div>
               
                <div class="col-md-6 col-sm-6 additional-nav">
                    <ul class="list-unstyled list-inline pull-right">
                      <?php //include 'html/student/navigation-ul.php';?>
                    </ul>
                </div>
               
            </div>
        </div>        
    </div>
    
    
    <div class="header">
      <div class="container">
        <?php if(!isset($_SESSION['logo'])) { ?>
        <a class="site-logo" href="index.php"><img src="assets/instituteimages/logo/default.png" alt="Demo" title="Learning demo"></a>
        <?php }else{ ?>
        <a class="site-logo" href="<?=$_SESSION['website'];?>"><img src="<?=$_SESSION['logo'];?>" style="height: 80px;margin: 7%;"alt="" title=""></a>

        <?php } ?>
        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

       
        <div class="header-navigation pull-right font-transform-inherit">
          <ul>
          <?php if(!isset($_SESSION['logo'])) { ?>
            <li><a href="index.php">Home</a></li>
            <li><a href="my-learning-graph.php">Why MLG?</a></li>
            <li><a href="all-programs.php">Programs</a></li>
            <li><a href="contact_us.php">Contact Us</a></li>
          <?php } ?>
            
            <li class="menu-search">
              <span class="sep"></span>
              <i class="fa fa-search search-btn"></i>
              <div class="search-box">
                <form action="#">
                  <div class="input-group">
                    <input type="text" placeholder="Search" class="form-control">
                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="submit">Search</button>
                    </span>
                  </div>
                </form>
              </div> 
            </li>
           
          </ul>
        </div>
        
      </div>
    </div>
    -->
<div id="wrapper-container" class="wrapper-container">
  <div class="content-pusher ">
    <header id="masthead" class="site-header affix bg-custom-sticky sticky-header header_overlay header_v1">
      <div id="toolbar" class="toolbar">
          <div class="container">
              <div class="row">
                  <div class="col-sm-12">
                      <aside id="text-2" class="widget widget_text">
                          <div class="textwidget">
                              <div class="thim-have-any-question">
                                  Have any question?
                                  <div class="mobile"><i class="fa fa-phone"></i><a href="tel:0882715488" class="value">(+91) 888 271 5488 </a>
                                  </div>
                                  <div class="email"><i class="fa fa-envelope"></i><a href="mailto:info@4thpointer.com"><span class="__cf_email__" >info@4thpointer.com</span></a>
                                  </div>
                              </div>
                          </div>
                      </aside>
                      <aside id="login-menu-2" class="widget widget_login-menu">
                          <div class="thim-widget-login-menu thim-widget-login-menu-base">
                              <div class="thim-link-login">                                           
                                      
                                      <?php 
                                          @session_start();
                                          if(!isset($_SESSION['role']))
                                          {
                                      ?>
                                      <a class="login" href="login.php">Login</a>
                                      <a class="register" href="registration.php">Register</a>
                                      <?php
                                          }else{
                                      ?>
                                      <ul class="list-unstyled list-inline pull-right">
                                        <?php include 'html/student/navigation-ul.php';?>
                                      </ul>
                                      <?php } ?>
                              </div>
                          </div>
                      </aside>
                  </div>
              </div>
          </div>
      </div>

      <div class="container">
          <div class="row">
              <div class="navigation col-sm-12">
                  <div class="tm-table">
                      <div class="width-logo table-cell sm-logo">
                         
                          <a href="index.php" title="E-learning Demo" rel="home" class="no-sticky-logo"><img src="assets/demo/LOGO11.png" alt="Education" width="60" height="40" style="margin-bottom: 0px;" /><span class="title">E-Learning</span></a>
                          <a href="index.php" title="E-learning Demo" rel="home" class="sticky-logo">
                              <img src="assets/demo/LOGO11.png" alt="Education" width="60" height="40" style="margin-bottom: 0px;" /><span class="title">E-Learning</span></a>
                      </div>
                      <nav class="width-navigation table-cell table-right">
                          <ul class="nav navbar-nav menu-main-menu">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="all-programs.php"><span data-hover="Programs">Packages</span></a></li>
                           <?php if(isset($_SESSION['role']))
                                {
                                  if($_SESSION['role'] == 4) 
                                  { ?>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="my-program.php"><span data-hover="My Packages">My Packages</span></a></li>
                                    
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="student-profile.php"><span data-hover="Events">My Profile</span></a></li>
                                       
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="report-card.php"><span data-hover="Events">Report Card</span></a></li>
                                       
                                    </li>
                            <?php }} ?>
                              
                              
                              <!-- <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="#"><span data-hover="Blog">Blog</span></a></li> -->
                              <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_left standard"><a href="contact_us.php"><span data-hover="Contact">Contact</span></a></li>
                              <?php if(!isset($_SESSION['role']))
                                {?>
                                    <li class="menu-right">
                                        <ul>
                                            <li id="courses-searching-2" class="widget widget_courses-searching">
                                                <div class="thim-widget-courses-searching thim-widget-courses-searching-base">
                                                    <div class="thim-course-search-overlay">
                                                        <div class="search-toggle"><i class="fa fa-search"></i></div>
                                                        <div class="courses-searching layout-overlay">
                                                            <div class="search-popup-bg"></div>
                                                            <form method="get" action="http://educationwp.thimpress.com/">
                                                                <input type="text" value="" name="s" placeholder="Search programs..." class="thim-s form-control courses-search-input" autocomplete="off" />
                                                                <input type="hidden" value="course" name="ref" />
                                                                <button type="submit"><i class="fa fa-search"></i></button>
                                                                <span class="widget-search-close"></span>
                                                            </form>
                                                            <ul class="courses-list-search list-unstyled"></ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                  
                            <?php } ?>
                          </ul>
                      </nav>
                      <div class="menu-mobile-effect navbar-toggle" data-effect="mobile-effect">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                      </div>
                  </div>

              </div>
          </div>
      </div>
  </header>

  <div style="padding-top:150px;"></div>


