<!-- <li><a class="btn green" data-toggle="modal" href="#redeem-coupon-modal">Apply Coupon</a></li> -->
<li class="dropdown dropdown-user header-menu-li-user">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
	<img alt="" class="user-image img-circle" src="admin/assets/admin/layout/img/avatar3_small.jpg" style="width: 29px; height: 29px;"/>
	<span class="user-name username-hide-on-mobile text-capitalize"></span>
	<i class="fa fa-angle-down"></i></a>
	<ul class="dropdown-menu dropdown-menu-default">
		<li><a data-toggle="modal" href="#" onclick="$('#redeem-coupon-modal').modal('show');">Apply Coupon</a></li>
		<li><a href="my-program">My Exams</a></li>
		<!-- <li><a href="my-certification">Get Transcripts</a></li> -->
		<li><a href="profile">My Profile</a></li>
		<!-- <li><a href="study-planner.php">Study Planner</a></li> -->
		<!-- <li><a href="report">Report Card</a></li> -->
		<li><a href="logout"> Log Out </a></li>
	</ul>
</li>