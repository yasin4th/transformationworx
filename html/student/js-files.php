<script src="assets/js/custom/studentLogin.js" type="text/javascript"></script>

	<!--<script src="admin/assets/global/plugins/jquery.min.js" type="text/javascript"></script>-->
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="admin/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<!--<script src="admin/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>-->
<!--<script src="admin/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>-->
<!-- <script src="admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> -->
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->

<script type="text/javascript" src="admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="admin/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<!-- <script src="admin/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="admin/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script> -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="admin/assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="admin/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="admin/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript" src="admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="admin/assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
<!-- END PAGE LEsVEL SCRIPTS -->
<script src="admin/assets/admin/pages/scripts/ui-toastr.js"></script>
<!-- <script src="admin/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script> -->
<!-- <script src="assets/frontend/layout/scripts/layout.js" type="text/javascript"></script> -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!-- <script src="admin/assets/global/scripts/metronic.js" type="text/javascript"></script> -->
<!-- <script src="admin/assets/admin/layout/scripts/layout.js" type="text/javascript"></script> -->
<script src="admin/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<!-- <script src="admin/assets/admin/layout/scripts/demo.js" type="text/javascript"></script> -->
<script src="admin/assets/admin/pages/scripts/calendar.js"></script>
<script src="admin/assets/admin/pages/scripts/components-pickers.js"></script>
<script src="admin/assets/global/plugins/moment.min.js"></script>
<script src="admin/assets/global/plugins/fullcalendar/fullcalendar.min.js"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script>
jQuery(document).ready(function() {
    // initiate layout and plugins
    // Metronic.init(); // init metronic core components
	// Layout.init(); // init current layout
	QuickSidebar.init(); // init quick sidebar
	// Demo.init(); // init demo features
    Calendar.init();
    ComponentsPickers.init();
	UIToastr.init();
	FormWizard.init();
});
</script>