	<!-- Load javascripts at bottom, this will reduce page load time -->
	<!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
	<!--[if lt IE 9]>
	<script src="assets/global/plugins/respond.min.js"></script>
	<![endif]-->
	<script src="admin/assets/admin/pages/scripts/form-wizard.js"></script>
	<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/frontend/layout/scripts/back-to-top.js" type="text/javascript"></script>
	<!--<script src="assets/frontend/layout/scripts/back-to-top.js" type="text/javascript"></script>-->

	<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/toastr.js" type="text/javascript"></script>
	<script src="assets/js/common.js" type="text/javascript"></script>
	<!-- END CORE PLUGINS -->

	<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
	<script src="assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
	<script src="assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
	<!-- <script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script> -->
	<!-- <script src="assets/global/plugins/gmaps/gmaps.js" type="text/javascript"></script> -->
	<script src="admin/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
	<!-- BEGIN RevolutionSlider -->

	<script src="assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
	<script src="assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
	<script src="assets/frontend/pages/scripts/revo-slider-init.js" type="text/javascript"></script>
	<!-- END RevolutionSlider -->

	<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
	<script src="assets/frontend/layout/scripts/layout.js" type="text/javascript"></script>
	<script src="admin/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			Metronic.init(); // init metronic core components
			Layout.init();
			RevosliderInit.initRevoSlider();
			//Layout.initTwitter();
			Demo.init(); // init demo features
			//Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header)*/
			//Layout.initNavScrolling();
			jQuery(document).on("click",".menu-mobile-effect",function(e){
				e.stopPropagation();
				jQuery(".wrapper-container").toggleClass("mobile-menu-open");
			});
			jQuery(document).on("click",".mobile-menu-open #main-content",function(){
				jQuery(".wrapper-container.mobile-menu-open").removeClass("mobile-menu-open")
			});
			window.addEventListener("load",function(){
				var e=document.getElementById("main-content");
				e&&e.addEventListener("touchstart",function(e){
					jQuery(".wrapper-container").removeClass("mobile-menu-open")})
			},!1);
		});
	</script>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-75827374-1', 'auto');
		ga('send', 'pageview');
	</script>

	<script>
		$(document).ready(function() {
			$('#gettransscript').off('submit');
			$('#gettransscript').on('submit', function(e){
				e.preventDefault();
				var req = {};
				req.license_no = $('input[name=phrase]').val()

				req.action = "getTranscriptDetails";

				$.ajax({
					'type'	:	'post',
					'url'	:	EndPoint,
					'data'	:	JSON.stringify(req)

				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1)
					{
						fillTranscriptDetails(res.data);
						$('#gettransscript [type=submit]').addClass('hide');
						$('#downloadTranscript').removeClass('hide');
						$('#downloadTranscript').attr('href',`/certificates/${req.license_no}/Certificate.pdf`);
					} else {
						toastr.error('Something went wrong');
					}
				});
			});
			$('#get-certificate').on('hidden.bs.modal', function () {
				$('#gettransscript [type=submit]').removeClass('hide');
				$('#downloadTranscript').addClass('hide');
				$('#gettransscript table tbody').html('');
				$('input[name=phrase]').val('')
			})

		})
		fillTranscriptDetails = function(tx) {
			html = `
				<tr>
					<td><strong>First Name</strong></td>
					<td>${tx.firstname}</td>
				</tr>
				<tr>
					<td><strong>Last Name</strong></td>
					<td>${tx.lastname}</td>
				</tr>
				<tr>
					<td><strong>Certification</strong></td>
					<td>${tx.coursename}</td>
				</tr>
				<tr>
					<td><strong>Issued On</strong></td>
					<td>${tx.issuedOn}</td>
				</tr>
				<tr>
					<td><strong>Valid Until</strong></td>
					<td>${tx.validUntil}</td>
				</tr>
			`;
			$('#gettransscript table tbody').html(html);
		}

	</script>
	<!-- END PAGE LEVEL JAVASCRIPTS -->


<?php
  if(defined('JSFILES')) {
    include(JSFILES);
  }
?>
