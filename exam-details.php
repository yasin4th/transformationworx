<?php
	require_once 'config.inc.php';
	require_once 'config.inc.test.php';
	@session_start();
	 if(isset($_SESSION['role']) && $_SESSION['role'] != '4')
		{
			if (!isset($_SESSION['username']))
			{
					header('Location: profile');
			}
		}
	//include 'Access-API.php';
?>

<?php

	$pos = strpos($_SERVER['REQUEST_URI'], 'utm_source=');
	if($pos)
	{
		$pos=$pos + 11;
		$utm = trim(substr($_SERVER['REQUEST_URI'], $pos));
		@session_start();
		$cookie_name = "utm";

		setcookie($cookie_name, $utm, time() + (86400 * 30), "/"); // 86400 = 1 day
	}

	ob_start();

	/*if(isset($_GET['utm_source']))
	{
		@session_start();
		$cookie_name = "utm";
		$cookie_value = $_GET['utm_source'];
		setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

	}*/

	if(isset($_GET['id']))
	{
		if(!empty($_GET['id']))
		{

			$id = $_GET['id'];
			$request = new stdClass();
			$request->action = 'filterPrograms';
			$request->test_program_id = $id;

			$request->option = 1;

			$json = json_encode($request);
			require('vendor/api.php');
			ob_clean();

			if($res->status == 0)
			{
				header('Location: ../404');
			}
		}
		else
		{
			header('Location: ../404');
		}
	}else{
		header('Location: ../404');
	}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- Head BEGIN -->
<head>
	<base href="<?=URI1;?>" />
	<?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
		<?php include('html/navigation.php'); ?>
		<!-- Navigation END -->

		<div class="main">

			<div class="container">
				<ul class="breadcrumb">
						<li><a href="<?=URI1;?>">Home</a></li>
						<li><a href="exams">Exams</a></li>
						<li class="active">Exam Details</li>
				</ul>

				<!-- BEGIN CONTENT -->
				<div class="content-page">
					<div class="row">
						<div class="col-md-9 col-sm-9 exam-details-div">
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<h2 class="margin-top-0"><span><?=$res->data[0]['name'];?></span> <span class="strem"></span></h2>
								</div>
								<div class="col-md-4 col-sm-4">
									<img alt="" src="<?=$res->data[0]['image'];?>" class="img-responsive" id="plan-image">
								</div>
								<div class="col-md-8 col-sm-8">
									<?=$res->data[0]['description'];?>
								</div>
							</div>
							<div class="row margin-top-20">
								<!-- <div class="col-md-12">
									<table class="table table-striped table-hover table-bordered dark-table-header"><thead><tr><th>Test Type</th><th>No. of Tests</th></tr></thead><tbody>
									<tr>
									<td>Mock Test</td>
									<td>
									<?php
										$count = 0;
										for ($i=0; $i < count($res->papers); $i++)
										{
											if($res->papers[$i]['test_program_id'] == $id && $res->papers[$i]['test_paper_type_id'] == '4')
											{
												echo $res->papers[$i]['count'];
											}
										}
									?>
									</td>

									</tr>

									</tbody></table>
								</div> -->
							</div>
						</div>

						<div class="col-md-3 col-sm-3 gray-bg">
							<div class="margin-bottom-20">
								<h5></h5>
								<!-- <h4 class="text-center regular-price">Regular Price : <i class="fa fa-inr"></i> <span><s>749</s></span></h4> -->
								<div id="pricing">
									<h3>Enrollment Price : CAD <span><?=$res->data[0]['price'];?></span></h3>
									<br/>
								</div>
								<div class="input-group enroll-code-input">
									<input type="text" class="form-control input-sm" name="enroll-code" id="enroll-code" placeholder="Enter Enroll Code">
									<span class="input-group-btn">
										<button class="btn blue btn-sm" type="button" id="apply-enroll-code">Enroll</button>
									</span>
								</div>
								<a href='my-subscription-package.php' style="color:#fff" id="subscribe" data-id="<?=$res->data[0]['id'];?>" class="btn btn-primary btn-lg btn-block margin-bottom-10 hide">Buy Now</a>
								<!-- <a href="#" id="dis1">Have a Promo Code</a> -->
								<div>

									<!-- <div class="promo-code-div">
										 <a class="have-code">Have a Promo code ?</a> <a class="pull-right cancel-promo-code" style="display:none;"><i class="fa fa-times-circle"></i></a>
										 <div class="input-group promo-code-input" style="display:none;">
											 <input type="text" class="form-control input-sm" name="promo-code" id="promo-code" placeholder="Enter Promo Code">
											 <span class="input-group-btn">
												 <button class="btn blue btn-sm" type="button" id="apply-promo-code">Apply</button>
											 </span>
										 </div>
									</div> -->

								</div>
								<span id="info" class="">

								</span>
							</div>
						</div>
					</div>

					<div class="col-md-12 col-sm-12">
					<!-- <h3>You may also like...</h3> -->
						<!-- <div id="programs-container" class="row margin-bottom-20">

						</div> -->
					</div>

			</div>
		</div>
		<form action="payments.php" method="post" id="paypal" name="paypal" class="hide">
			<input type="hidden" name="cmd" value="_xclick">
			<input type="hidden" name="no_note" value="1">
			<input type="hidden" name="item_name" value="<?=$res->data[0]['name']?>">
			<input type="hidden" name="item_number" value="<?=$id?>">
			<input type="hidden" name="custom" value="<?=$_SESSION['userId']?>">
			<input type="hidden" name="currency_code" value="CAD">
			<input type="hidden" name="amount" value="<?=$res->data[0]['price']?>">
			<input type="hidden" name="first_name" value="<?=$_SESSION['username']?>">
			<input type="hidden" name="payer_email" value="<?=$_SESSION['user']?>">
			<input type="hidden" name="on0" value="user_id">
			<input type="hidden" name="os0" value="<?=$_SESSION['userId']?>">
			<input type="hidden" name="on1" value="enroll">
			<input type="hidden" name="os1" value="0">

			<input type="hidden" name="bn" value="PP-BuyNowBF">
			<input type="submit" name="submit1" value="Submit Payment">
		</form>

		<!-- <form id="idform" action="payment-redirect.php" method="post" class="hide">
			<input type="hidden" name="discount" value="0" />
			<input type="hidden" name="_amount" value="" />
			<input type="hidden" name="amount" value="" />
			<input type="hidden" name="productinfo" value="" />
			<input type="hidden" name="enroll" value="0" />
			<input type="hidden" name="service_provider" value="payu_paisa"  />
			<input type="submit" id="pay_now" value="Pay Now" /></td>
		</form> -->


		<!-- BEGIN PRE-FOOTER -->
	<?php include('new-html/footer.php'); ?>
		<!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
		<?php include('html/js-files.php'); ?>
		<!-- END PAGE LEVEL JAVASCRIPTS -->
		<script type="text/javascript">
				$(function() {
						$('#dis1').click(function(e){
								e.preventDefault();
								$(this).addClass('hide');
								$('#dis2').removeClass('hide');
						})

						$('#subscribe').click(function(e){
								e.preventDefault();
								var req = {};
								req.action = "ifBought";
								req.program_id = $(this).attr('data-id');
								$.ajax({
								 'type'  :   'post',
								 'url'   :   EndPoint,
								 'data'  :   JSON.stringify(req)
								}).done(function(res){
										res = $.parseJSON(res);
										if(res.status == 1)
										{
												toastr.success(res.message);
										}
										else if(res.status == 2)
										{
												toastr.error(res.message);
												setTimeout(function() {document.location = "login?redirect="+document.URL;}, 1000);

										}
										else if(res.status == 0)
										{
												toastr.error(res.message);
										}
										else if(res.status == 3)
										{
											document.getElementById('paypal').submit();
										}
								});
						})


						var req = {};
						req.action = "filterPrograms";
						req.option = 1;
						$.ajax({
						 'type'  :   'post',
						 'url'   :   EndPoint,
						 'data'  :   JSON.stringify(req)
						}).done(function(res){
								res = $.parseJSON(res);
								if(res.status == 1)
								{
									html = '';
									$.each(res.data, function(i, program){
										if(program.id != $('#subscribe').attr('data-id')){
											html += '<div class="col-md-3">';
											html += '<a class="test-program" data-id="'+program.id+'" data-price="'+program.price+'" href="exams/'+program.name.dash()+'_'+program.id+'">';
											html += '<div class="course-item">';
											html += '<div class="course-thumbnail">';
													html += '<!--<a class="test-program" data-id="'+program.id+'" data-price="'+program.price+'" href="#program-details-modal">--><img src="'+program.image+'" alt="Introduction LearnPress &#8211; LMS plugin" title="course-4" ><!--</a>--><!--<a class="course-readmore test-program" href="#">Buy Now</a>--></div>';
											html += '<div class="thim-course-content">';

											html += '<h2 class="course-title">';
											html += '<span class="test-program" data-id="'+program.id+'" data-price="'+program.price+'" href="#program-details-modal"> ' + program.name + '</span>';
											html += '</h2>';
											html += '<div class="course-meta">';
											html += '<div class="course-students">';

											html += '</div>';

											html += '<div class="course-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';
											html += '<div class="value free-course" itemprop="price" content="Free">';
											html += 'CAD ';
											html += program.price+'</div>';
											html += '<meta itemprop="priceCurrency" content="&#36;" />';
											html += '</div>';
											html += '</div>';
											html += '</div>';
											html += '</div>';
											html += '</a>';
											html += '</div>';
										}
									})
									$('#programs-container').html(html);
								}
						});
				})
		</script>

<script type="text/javascript">
	$(function() {
		 $('a.have-code').click(function () {
			 if ($(".promo-code-input").is(":hidden")) {
				 $('.promo-code-input').show(1000);
				 $('.cancel-promo-code').show(1000);
				 $('a.have-code').animate({
						 fontSize: "10px",
				 }, 500 );
			 }else {
				 $('.promo-code-input').hide(1000);
				 $('a.have-code').animate({
						 fontSize: "13px",
				 }, 500 );
			 }
		 });

		 $('a.cancel-promo-code').click(function () {
				 $('.cancel-promo-code').hide(1000);
				 $('.promo-code-input').hide(1000);
				 $('a.have-code').animate({
						 fontSize: "13px",
				 }, 500 );
		 });


		 $('#apply-promo-code').on('click', function() {
			promo = $('#promo-code').val().trim();
			if(promo.length > 0)
			{
				program = $('#subscribe').attr('data-id');

				var req = {};

				req.action = "applyDiscount";
				req.promo = promo;
				req.program = program;
				$.ajax({
					type: 'post',
					url: EndPoint,
					data: JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1)
					{
						toastr.success(res.message);
						$('#pricing h3').html('Price : CAD <span><s>'+res._amount+'</s> '+res.amount+'</span>');
						$('.cancel-promo-code').click();
						$('#idform [name=amount]').val(res.amount);
						$('#idform [name=discount]').val(res.id);
					}
					else
					{
						toastr.error(res.message);
					}
				});
			}
		})

		$('#apply-enroll-code').on('click', function() {
			enroll = $('#enroll-code').val().trim();
			if(enroll.length > 0)
			{
				program = $('#subscribe').attr('data-id');

				var req = {};

				req.action = "applyEnrollment";
				req.promo = enroll;
				req.program = program;
				$.ajax({
					type: 'post',
					url: EndPoint,
					data: JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1)
					{
						$('.enroll-code-input').addClass('hide');
						$('#subscribe').removeClass('hide');
						$('#paypal [name=custom]').val($('#paypal [name=custom]').val()+'_'+res.enrollment.id);
						$('#paypal [name=os1]').val(res.enrollment.id);

						toastr.success(res.message);
					}
					else
					{
						toastr.error(res.message);
					}
				});
			}
		})
})

</script>
<!-- <script type="text/javascript" src="assets/js/custom/exam-details1.js"></script> -->

</body>
<!-- END BODY -->
</html>
