<?php
    require_once 'config.inc.test.php';
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
		<ul class="breadcrumb">
            <li><a href="<?=URI1?>">Home</a></li>
            <li class="active">Terms & Condition</li>
        </ul>
        <!-- Start TERMS AND CONDITION BOX -->
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <h2>Terms & Condition</h2>
          </div>
        </div>
        <div class="row service-box margin-bottom-40">
          <div class="col-md-12 col-sm-12 p-mrg-left-40 p">
			      <!-- <div class="service-box-heading">
              <span>Terms & Condition</span>
            </div> -->
            <!-- <p>The domain name www.transformationworx.com (hereinafter referred to as "Website") is owned by transformationworx.com Private Limited a company incorporated under the Companies Act, 1956 with its registered office at D-44, First Floor, Sector 6, Noida, India, 201301 (hereinafter referred to as "TransformationWorx").</p> -->
            <h5>User Account, Password, and Security:</h5>
            <p>If you use the website, you are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer to prevent unauthorized access to your account. You agree to accept responsibility for all activities that occur under your account or password. You should take all necessary steps to ensure that the password is kept confidential and secure and should inform us immediately if you have any reason to believe that your password has become known to anyone else, or if the password is being, or is likely to be, used in an unauthorized manner. Please ensure that the details you provide us with are correct and complete and inform us immediately of any changes to the information that you provided when registering. You can access and update much of the information you provided us with in the Your Profile area of the website. TransformationWorx (www.transformationworx.com) reserves the right to refuse access to the website, terminate accounts, remove or edit content at any time without notice to you.</p>
            <h5>Payment Modes:</h5>
            <ul>
              <li>Credit Card</li>
              <li>Debit Card</li>
              <li>Net Banking</li>
            </ul>
            <h5>Refund Policy:</h5>
            <p>Electronic products (digital) are FIRM SALE and cannot be returned for refund.</p>
            <h5>Delivery:</h5>
            <p>After your payment is successfully processed, you can view your program(s) under your “My Program”. Just click to view your content.</p>
            <h5>GRANT OF LICENSE:</h5>
            <p>Under this LICENCE, TransformationWorx grants you with a non-exclusive and non-transferable license. This LICENCE does not include the right to sub-license or distribute the program(s) to any other individual or entity.</p>
            <h5>LICENSE TERM:</h5>
            <p>The License Term commences from the date of purchase and expires in 12 (Twelve) months, unless otherwise stated.</p>
            <h5>DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS:</h5>
            <h5>Not for Resale:</h5>
            <p>The program(s) must not be resold, or otherwise transferred.</p>
            <h5>Rental:</h5>
            <p>You must not rent or lease the program(s).</p>
            <h5>File Transfer:</h5>
            <p>You must not allow copying of the eText / program(s) including, but not limited to, copying through file sharing networks.</p>
            <h5>Marks of Ownership:</h5>
            <p>You must not remove or alter any mark of ownership, copyright, trademark watermarks or other property right which is embodied in the eText / program(s) including its documentation.</p>
            <h5>Copyright:</h5>
            <p>All title, Copyright and intellectual property rights in and to the eText / program(s), are owned by the relevant publisher and/or author. Storage of this intellectual property by any means electronic or otherwise that is contrary to this license agreement may result in prosecution.</p>
            <h5>Termination:</h5>
            <p>Without prejudice to any other rights, TransformationWorx may terminate this LICENCE if you fail to comply with the terms and conditions of this LICENCE.</p>
            <h5>EXPORT RESTRICTION:</h5>
            <p>You must not export or otherwise transfer the eText / program(s) from your country of residence, including, without limitation, to a country or jurisdiction whose laws allow or mandate loss of the title and intellectual property rights.</p>
            <h5>GOVERNING LAWS AND JURISDICTION</h5>
            <p>This license is governed by the laws of the NCT of Delhi, India.</p>
            <h5>CUSTOMER REMEDIES</h5>
            <p>To the maximum extent permitted by applicable law, TransformationWorx's entire liability and your exclusive remedy, shall be, at TransformationWorx's discretion, either</p>
            <ul>
              <li>Refund of the License Fee, if any applies, or</li>
              <li>Repair or replacement of the eText / program(s) that does not meet this warranty and that is returned to TransformationWorx. This warranty is void if failure of the eText / program(s) has resulted from accident, abuse or misapplication.</li>
            </ul>
            <h5>NO OTHER WARRANTIES:</h5>
            <p>To the maximum extent permitted by applicable law, TransformationWorx disclaims all other warranties and conditions, either express or implied, including, but not limited to, implied warranties or conditions of merchantability, fitness for a particular purpose, title and non-infringement, with regard to the eText / program(s). This warranty gives you specific legal rights. You may nevertheless have the benefit of certain rights or remedies pursuant to consumer protection acts and laws. This warranty does not preclude such rights.</p>
            <h5>LIABILITY LIMITATION:</h5>
            <p>To the maximum extent permitted by the applicable law, in no event shall TransformationWorx be liable for any special, incidental, indirect, or consequential damages whatsoever (including, without limitation, damages for the loss of Profits, Interruption, loss of Information, or any other Pecuniary Loss) arising out of the use of or inability to use the eText / program(s) even if TransformationWorx has been advised of the possibility of such damages. In any case, TransformationWorx total liability under any provision of this eText / program(s) shall be limited to the amount of the License Fee, if any.</p>
            <p>If you have any questions regarding this, please email us at support@transformationworx.com</p>
          </div>
          </div>
        <!-- End TERMS AND CONDITION BOX -->

      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include FOOTER ?>
    <!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>