<?php
	require_once 'config.inc.test.php';
?>
<?php include 'Access-API.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
	<?php include HEADER; ?>
	<!-- Navigation END -->

	<div class="main">
	  <div class="container">
		<marquee direction="" class="hide"></marquee>
		<ul class="breadcrumb">
			<li><a href="<?=URI1?>">Home</a></li>
			<!-- <li><a href="#">Student</a></li> -->
			<li><a href="my-program">My Package</a></li>
			<li class="active">Exams</li>
			<!-- <li><a class="back-selected-chapter">Test Types</a></li>
			<li class="active">View test</li> -->
			<ul class="pull-right">
				<div id="countprogram" class="hide">
				Total Paper : <span id="totalCount"></span> |
				Total Lab : <span id="labcount"></span><br>
				<strong style="font-size:18px;" class="text-danger">Expiry Date : <span id="expiry" ></span></strong>
				</div>
			</ul>
		</ul>
		<!-- BEGIN SIDEBAR & CONTENT -->
		<div class="row margin-bottom-40">
		  <!-- BEGIN CONTENT -->
		  <div class="col-md-12 col-sm-12">
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<h2>Exams</h2>
				</div>
				<div class="col-md-8 col-sm-8">
					<span id="fail-cutoff" class="hide" style="font-size: 16px; font-weight: 600;"></span>
				</div>
			</div>

			<!-- BEGIN DASHBOARD STATS -->
			<div class="row">
				<div class="col-md-12">
					<div class="table-scrollable">
						<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
							<thead>
							<tr>
								<th>
									Exam Name
								</th>
								<th>
									Exam Type
								</th>
								<th>
									Total Questions
								</th>
								<th>
									 Start Date
								</th>
								<th>
									 End Date
								</th>
								<!-- <th>
									 Total Attempts
								</th> -->
								<!-- <th>
									Rem Attempts
								</th> -->
								<th>Passing Marks</th>
								<th>
									Action
								</th>
								<th>Marks</th>
								<th>Result</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
							<tfoot>
								<th colspan="10" class="text-center">
									&nbsp;
								</th>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- END DASHBOARD STATS -->

		  </div>
		  <!-- END CONTENT -->

		  <div class="col-md-12 col-sm-12" id="ref_study_material" style="display: none;">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2>Capstone Project Template</h2>
				</div>
				<div class="col-md-6 col-sm-6"></div>
			</div>

			<!-- BEGIN DASHBOARD STATS -->
			<div class="row">
				<div class="col-md-12">
					<div class="table-scrollable">
						<table class="table table-striped table-hover table-bordered" id="listing-table">
							<thead>
							<tr>
								<th>
									Course
								</th>
								<th>
									Material Name
								</th>
								<th>
									Download
								</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- END DASHBOARD STATS -->
		</div>

		<div class="col-md-12">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-md-offset-3 text-center">
					<h1>Submit Capstone Project</h1><hr>
				</div>
			</div>
		</div>

		<div class="col-md-12">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2>Upload Capstone Project</h2>
				</div>
				<div class="col-md-6 col-sm-6">
					<h2>Send By Email</h2>
				</div>
			</div>

			<!-- BEGIN DASHBOARD STATS -->
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="well well-md">
						<div class="row">
							<form id="assignmentUploadForm" method="post" enctype="multipart/form-data" action="">
								<div class="col-md-8 col-sm-8">
									<input type="file" id="assignment_file" name="assignment_file" accept="application/msword,application/pdf" required>
									<input type="hidden" id="action" name="action" value="uploadStudentAssignment" />
								</div>
								<div class="col-md-4 col-sm-4 text-right">
									<input type="submit" class="btn btn-primary" value="Upload">
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6" id="no-cert">
					<div class="well well-md">
						<button class="btn btn-primary" onclick="sendByEmail()">Send By Email</button>
						<span class="pull-right">Email your submission to <a href="mailto:dkhan@redmobileco.com">dkhan@redmobileco.com</a></span>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-12">
			<div class="row hide" id="assignments">
				<h2>Capstone Project</h2>
				<table id="student-assignment-table" class="table table-bordered table-striped table-hover">
					<thead>
					<tr>
					<th>
					<div style="padding:5px;"> S No </div>
					</th>
					<th>
					<div style="padding:5px;"> Doc Name </div>
					</th>
					<th>
					<div style="padding:5px;"> Download </div>
					</th>
					<th>
					<div style="padding:5px;"> Status </div>
					</th>
					<th style="width: 82px;">
					<div style="padding:5px;"> Action </div>
					</th>
					</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>



		  <!-- END CONTENT -->
		</div>
		<!-- END SIDEBAR & CONTENT -->
	  </div>
	</div>

		<!-- BEGIN PRE-FOOTER -->
	<?php include FOOTER; ?>
		<!-- END FOOTER -->

	<!-- START PAGE LEVEL JAVASCRIPTS -->
		<?php include('html/js-files.php'); ?>
	<?php include('html/student/js-files.php'); ?>

	<script src="assets/js/custom/test-papers.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
