<?php 
	header('Location: my-program.php');
?>
<?php include 'Access-API.php'; ?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <!-- <li><a href="#">Student</a></li> -->
            <li><a href="my-program.php">My Program</a></li>
            <li><a class="program-name"><!-- Program Name --></a></li>
            <li><a class="subject"><!-- Subject --></a></li>
            <li class="active"><a class="chapter"><!-- Chapter --></a></li>
            <!-- <li class="active">Test Types</li> -->
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          <div class="sidebar col-md-2 col-sm-3">
            <?php include('html/student/sidebar.php'); ?>
          </div>
          <!-- END SIDEBAR -->

          <!-- BEGIN CONTENT -->
          <div class="col-md-10 col-sm-9">
          	<!-- strat div for meter -->
          	<div class="row">
				<div class="col-md-12 col-sm-12">
					
				</div>
			</div>
			<!-- end div for meter -->
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2 class="program-name"><!-- Course 1 --></h2>
				</div>
				<div class="col-md-6 col-sm-6"></div>
			</div>
			
			<!-- BEGIN DASHBOARD STATS -->
			<div class="row VTP-right-side">
				<!-- Test Types -->
				<div id="illustration" class="col-md-3 col-sm-6 col-xs-12">
				    <div data-category="2" class="dashboard-stat red-intense">
				        <div class="visual">
			            	<span class="count"> 0</span>
				            <!-- <i class="fa fa-bar-chart-o"></i> -->
				        </div>
				        <div class="details">
				            <div class="desc">
				                <a id="view-illustration" class="color-white" href="#" title="QCR &amp; Illustration">QCR &amp; Illustration</a>
				            </div>
				        </div>
				    </div>
				</div>
				<div id="chapter-test" class="col-md-3 col-sm-6 col-xs-12">
				    <div data-category="3" class="dashboard-stat green-haze">
				        <div class="visual">
			            	<span class="count"> 0</span>
				        </div>
				        <div class="details">
				            <div class="desc">
				                <a class="view-test color-white" href="#" title="Chapter Test">Chapter Test</a>
				            </div>
				        </div>
				    </div>
				</div>
				<div id="practice-test" class="col-md-3 col-sm-6 col-xs-12">
				    <div data-category="6" class="dashboard-stat purple-plum">
				        <div class="visual">
			            	<span class="count"> 0</span>
				            <!-- <i class="fa fa-globe"></i> -->
				        </div>
				        <div class="details">
				            <div class="desc">
				                <a class="view-test color-white" href="#" title="Practice Test">Practice Test</a>
				            </div>
				        </div>
				    </div>
				</div>

				<!-- <div id="test-gentrator" class="col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat red-intense">
						<div class="visual">
						</div>
						<div class="details">
							<div class="desc">
								<a class="color-white" href="add-test-paper.php" title="Gentrate Test">Test Gentrator</a>
							</div>
						</div>
					</div>
				</div> -->
				<div class="col-md-3 col-sm-6"></div>
			</div>
			<!-- END DASHBOARD STATS -->
			
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
    <!-- END FOOTER -->
	
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
	<?php include('html/student/js-files.php'); ?>

	<script src="assets/js/custom/student-course-chapter-tests-types.js" type="text/javascript"></script>
	

    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>