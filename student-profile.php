<?php
	require_once 'config.inc.test.php';
	include 'Access-API.php';
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
	<?php include HEADER; ?>
	<!-- Navigation END -->

	<div class="main">
	  <div class="container">
		<ul class="breadcrumb">
			<li><a href="<?=URI1?>">Home</a></li>
			<li class="active">Profile</li>
		</ul>
		<!-- BEGIN SIDEBAR & CONTENT -->
		<div class="row margin-bottom-40">
		  <!-- BEGIN SIDEBAR -->
		  <div class="sidebar col-md-2 col-sm-3">
			<?php //include('html/student/sidebar.php'); ?>
		  </div>
		  <!-- END SIDEBAR -->

		  <!-- BEGIN CONTENT -->
		  <div class="col-md-12 col-sm-12">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2>Student Profile</h2>
				</div>
				<div class="col-md-6 col-sm-6">
					<?php
					@session_start();
					if (!isset($_SESSION['username']))
					{?>
					<div class="alert alert-danger">
						<strong>Please fill your profile.</strong>
					</div>
					<?php } ?>

				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
				<div class="col-md-4">
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar">
						<!-- PORTLET MAIN -->
						<div class="portlet light profile-sidebar-portlet">
							<!-- SIDEBAR USERPIC -->
							<div class="profile-userpic">
								<img id="display-picture" src="profilePictures/default.png" class="img-responsive" alt="">
							</div>
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
							<div class="profile-usertitle">
								<div class="profile-usertitle-name">
									<!-- Marcus Doe -->
								</div>
								<div class="email">
								<?php
									@session_start();
									if(isset($_SESSION['user']) && isset($_SESSION['userId']))
									{
										if($_SESSION['user'] != $_SESSION['userId'])
										{
											echo $_SESSION['user'];
										}
									}
								?>
								</div>
								<div class="profile-usertitle-job">
									Student
								</div>
							</div>
							<!-- END SIDEBAR USER TITLE -->
						</div>
						<!-- END PORTLET MAIN -->
					</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
			</div>

			<div class="col-md-8">
			<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light">
									<div class="portlet-title tabbable-line">
										<ul class="nav nav-tabs">
											<li class="active">
												<a href="#tab_1_1" data-toggle="tab">Personal Info</a>
											</li>
											<li>
												<a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
											</li>
											<li>
												<a href="#tab_1_3" data-toggle="tab">Change Password</a>
											</li>
										</ul>
									</div>
									<div class="portlet-body">
										<div class="tab-content">
											<!-- PERSONAL INFO TAB -->
											<div class="tab-pane active" id="tab_1_1">
												<div class="alert-danger text-center">
													<b>Please check your First and Last Name carefully. Exactly same name will be printed on your Certificate.</b>
												</div>
												<br/>

												<form id="student-details-form" role="form" method="post">
													<div class="form-group">
														<label class="control-label">First Name
														<span class="require">*</span>
														</label>
														<input type="text" placeholder="First Name" class="form-control" name="first_name" id="first_name" required="required"/>
													</div>
													<div class="form-group">
														<label class="control-label">Last Name</label>
														<input type="text" placeholder="Last Name" class="form-control" name="last_name" id="last_name"/>
													</div>
													<?php
														@session_start();
														if(isset($_SESSION['user']) && isset($_SESSION['userId']))
														{
															if($_SESSION['user'] == $_SESSION['userId'])
															{
													?>
													<div class="form-group">
														<label class="control-label">Email Address
														<span class="require">*</span>
														</label>
														<input type="text" placeholder="Email Address" class="form-control" name="email" id="email" required="required" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$"/>
													</div>
													<?php
															}
														}
													?>

													<div class="form-group">
														<label class="control-label">Mobile Number <!-- <span class="require">*</span> --></label>
														<input type="text" placeholder="Mobile Number" class="form-control" name="mobile" id="mobile" pattern="[0-9]{10}$"/>
													</div>
													<!-- <div class="form-group">
														<label class="control-label">Interests</label>
														<input type="text" placeholder="Interests" class="form-control"/ name="interests" id="interests">
													</div> -->
													<!-- <div class="form-group">
														<label class="control-label">Occupation</label>
														<input type="text" placeholder="Occupation" class="form-control"/ name="occupation" id="occupation">
													</div> -->
													<!-- <div class="form-group">
														<label class="control-label">About</label>
														<textarea class="form-control" rows="3" placeholder="About" name="about" id="about"></textarea>
													</div>
													<div class="form-group">
														<label class="control-label">Website Url</label>
														<input type="text" placeholder="Website Url" class="form-control" name="website_url" id="website_url"/>
													</div> -->
													<div class="form-group">
														<label class="control-label">Address</label>
														<textarea class="form-control" name="address" id="address" placeholder="Please enter your mailing address (In cases where the certification is mailed to you)"></textarea>
													</div>
													<div class="margiv-top-10">
														<button type="submit" class="btn green-haze mrg-bot5">
														Save Changes </button>
														<!-- <a href="#" class="btn default mrg-bot5">
														Cancel </a> -->
													</div>
												</form>
											</div>
											<!-- END PERSONAL INFO TAB -->
											<!-- CHANGE AVATAR TAB -->
											<div class="tab-pane" id="tab_1_2">
												<p>
													 <!-- Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. -->
												</p>
												<form id="form-change-profile-picture" action="vendor/fileUploads.php" role="form" method="post" enctype="multipart/form-data">
													<div class="form-group">
														<div class="fileinput fileinput-new" data-provides="fileinput">
															<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
																<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" id ="edit-profile-image" alt=""/>
															</div>
															<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
															</div>
															<div>
																<span class="btn default btn-file">
																<span class="fileinput-new">
																Select image </span>
																<span class="fileinput-exists">
																Change </span>
																<input type="file" id="profile-picture" name="profile-picture" accept="image/*">
																<input type="hidden" id="action" name="action" value="studentChangeProfilePicture" />
																</span>
																<a href="" class="btn default fileinput-exists" data-dismiss="fileinput">
																Remove </a>
															</div>
														</div>

													</div>
													<div class="margin-top-10">
														<lable id="percentage"></lable>
														<button type="submit" id="upload-profile-pic" class="btn green-haze">
														Submit
														</button>
														<a href="#" class="btn default" id="btn-cancel">
														Cancel </a>
													</div>
												</form>
											</div>
											<!-- END CHANGE AVATAR TAB -->
											<!-- CHANGE PASSWORD TAB -->
											<div class="tab-pane" id="tab_1_3">
												<form id="change-password-form" method="post">
													<div class="form-group">
														<label class="control-label">Current Password</label>
														<input type="password" class="form-control" id="current_password" name="current_password" required="required"/>
													</div>
													<div class="form-group">
														<label class="control-label">New Password</label>
														<input type="password" class="form-control" id="new_password" name="new_password" required="required"/>
													</div>
													<div class="form-group">
														<label class="control-label">Re-type New Password</label>
														<input type="password" class="form-control" id="confirm_password" name="confirm_password" required="required"/>
													</div>
													<div class="margin-top-10">
														<button type="submit" class="btn green-haze mrg-bot5">Change Password </button>
													</div>
												</form>
											</div>
											<!-- END CHANGE PASSWORD TAB -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END PROFILE CONTENT -->
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		  </div>
		  <!-- END CONTENT -->
		</div>
		<!-- END SIDEBAR & CONTENT -->
	  </div>
	</div>

	<!-- BEGIN PRE-FOOTER -->

	<?php include('new-html/footer.php'); ?>

	<!-- END FOOTER -->

	<!-- /modal -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
	<?php include('html/js-files.php'); ?>
	<script src="assets/global/plugins/jquery.form.js"></script>
	<script src="assets/js/custom/studentProfile.js" type="text/javascript"></script>

	<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
