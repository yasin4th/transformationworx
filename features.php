<?php
	require_once 'config.inc.php';
	require_once 'config.inc.test.php';

	@session_start();
	if(isset($_SESSION['role']) && $_SESSION['role'] != '4')
	{
		@session_destroy();
	}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
		<?php include HEADER; ?>
		<!-- Navigation END -->
	 <div style="padding-top: 100px;"></div>
		<div class="main">
			<div class="container">
				<ul class="breadcrumb">
					<li><a href="<?=URI1?>">Home</a></li>
					<li class="active">Features</li>
				</ul>
				<!-- BEGIN SIDEBAR & CONTENT -->
				<div class="row margin-bottom-40">
					<!-- BEGIN CONTENT -->
					<div class="services-block">
						<div class="container">
							<h2 class="text-center">Our <strong>features</strong></h2>
							<p class="font-size17">This platform works perfectly on any device like mobile, tab, laptop, desktop, and on any platform
								like i.e Windows, Mac or Linux. This revolutionary
								product is a must for every institution who wants to make their pupil high achievers.</p>
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-12 item">
									<i class="fa fa-thumbs-o-up medium circle-white"></i>
									<h3>Content Quality</h3>
									<p> Created and reviewed by Experts and Top Faculty across the country,
									 Find just the right content for any exam vetted by educators.</p>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-12 item">
									<i class="fa medium circle-white">
										<img class="experts"
										src="blade/main/images/experticon.png"
										onmouseover="this.src='blade/main/images/experticon2.png';"
										onmouseout="this.src='blade/main/images/experticon.png';">
									</i>
									<h3>Designed by experts</h3>
									<p>The ExamPointer test series have been designed by India’s top experts in the respective domains while keeping the real examination pattern in mind.
									</p>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-12 item">
									<i class="fa medium circle-white">
									<img class="librari"
										src="blade/main/images/libraryicon.png"
										onmouseover="this.src='blade/main/images/libraryicon2.png';"
										onmouseout="this.src='blade/main/images/libraryicon.png';"></i>
									<h3>Rich Library</h3>
									<p>We have compiled an exhaustive set of mock tests and practice exams to help students with their exam preparation.
									</p>
								</div>
							</div>

							<div class="row">
									<div class="col-md-4 col-sm-4 col-xs-12 item">
										<i class="fa fa-briefcase medium circle-white"></i>
										<h3>Customize Yourself</h3>
										<p>Plan for EVERYONE, Customized your plan according to your need, select the best which suites you best
										</p>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-12 item">
										<i class="fa fa-book medium circle-white"></i>
										<h3>Practice Exams</h3>
										<p>Score more by practice more. Our unique designed practice
											module help you to gain knowledge even when you are giving exams.</p>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-12 item">
										<i class="fa fa-clock-o medium circle-white "></i>
										<h3>Time - Bound</h3>
										<p> All the tests are time-bound and adhere to rules & syllabus that are applicable for the respective exams.
										</p>
									</div>
							</div>
						</div>
					</div>
					<!-- END CONTENT -->
				</div>
			</div>
		</div>
		<!-- BEGIN PRE-FOOTER -->
	<?php include FOOTER; ?>
		<!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
		<?php include('html/js-files.php'); ?>
		<!-- END PAGE LEVEL JAVASCRIPTS -->

</body>
<!-- END BODY -->
</html>