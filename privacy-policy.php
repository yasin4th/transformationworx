<?php
    require_once 'config.inc.test.php';
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
		<ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="active">Privacy Policy</li>
        </ul>
        <!-- Start Privacy Policy BOX -->
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <h2>Privacy Policy</h2>
          </div>
        </div>
        <div class="row service-box margin-bottom-40">
          <div class="col-md-12 col-sm-12 p-mrg-left-40 p">
			<p>TransformationWorx believes that the responsible use of personal information on its website is critical to its business objectives and reputation. As part of our commitment to privacy, we have adopted this Online Policy Statement.</p>
      <h5>Information Collected</h5>
      <p>When you visit our website we make a record of your visit and log the following information for statistical purposes;</p>
      <ul>
        <li>Your server address.</li>
        <li>Your top level domain name.</li>
        <li>The time and date you visited the site.</li>
        <li>The pages you accessed and the information you downloaded.</li>
      </ul>
      <h5>Cookies</h5>
			<p>TransformationWorx only uses session cookies, and only during a search query. Our ISP has ensured that no cookies are employed except for those to assist with the associated shopping basket. The website statistics are gathered by logs as outlined above.</p>
      <h5>Access to information collected</h5>
      <p>TransformationWorx will not make any attempt to identify users or their browsing activities.</p>
      <h5>Use of information collected</h5>
      <p>Your email address will only be used for the purposes for which you supplied it and will not be added to any 3rd party mailing lists.</p>
          </div>
        </div>
        <!-- End Privacy Policy BOX -->

      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include FOOTER; ?>
    <!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>