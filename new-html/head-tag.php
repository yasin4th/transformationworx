    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Online Exam Practice for SSC CGL  Gate Clerk Bank PO | ExamPointer</title>

    <meta content="Practice and track your competitive exam preparation with our online exam platform.ExamPointer is the online place for conducting and practicing online exams. We provide a marketplace for the institutes, couching’s, individual teachers to spread his practice papers and a well organize web platform for the students to practice of online exams in real time environment." name="description">
    <meta content="SSC CGL Online Exam papers, SSC CGL Online Exam practice, Online exam and Assesment Software, Best online exam software." name="keywords">
    <meta content="ExamPointer" name="author">

    <meta property="og:site_name" content="ExamPointer">
    <meta property="og:title" content="ExamPointer">
    <meta property="og:description" content="Practice and track your competitive exam preparation with our online exam platform.ExamPointer is the online place for conducting and practicing online exams. We provide a marketplace for the institutes, couching’s, individual teachers to spread his practice papers and a well organize web platform for the students to practice of online exams in real time environment.">
    <meta property="og:type" content="website">
    <meta property="og:image" content=""><!-- link to image for socio -->
    <meta property="og:url" content="http://exampointer.com/">

    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="assets/global/css/components.css" rel="stylesheet"> -->

    <link href="assets/global/plugins/toastr.css" rel="stylesheet">

    <link rel='stylesheet' id='rs-plugin-settings-css' href='http://educationwp.thimpress.com/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.2.4.1' type='text/css' media='all' />
    <style id='rs-plugin-settings-inline-css' type='text/css'>
    #rs-demo-id {}
    </style>
    <link rel='stylesheet' id='thim-css-style-css' href='assets/demo/custom-style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='thim-style-css' href='assets/demo/style-1.css' type='text/css' media='all' />

    <link rel='stylesheet' id='tf-google-webfont-roboto-css' href='//fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C300%2C300italic%2C400%2Citalic%2C500%2C500italic%2C700%2C700italic%2C900%2C900italic&#038;subset=greek-ext%2Cgreek%2Ccyrillic-ext%2Clatin-ext%2Clatin%2Cvietnamese%2Ccyrillic&#038;ver=4.5.1' type='text/css' media='all' />

    <link rel='stylesheet' id='tf-google-webfont-roboto-slab-css' href='//fonts.googleapis.com/css?family=Roboto+Slab%3A100%2C300%2C400%2C700&#038;subset=greek-ext%2Cgreek%2Ccyrillic-ext%2Clatin-ext%2Clatin%2Cvietnamese%2Ccyrillic&#038;ver=4.5.1' type='text/css' media='all' />

    <script type='text/javascript' src='assets/global/plugins/jquery.min.js'></script>


    <script type='text/javascript' src='assets/demo/jquery.themepunch.tools.min.js'></script>
    <script type='text/javascript' src='assets/demo/jquery.themepunch.revolution.min.js'></script>
    <link href="assets/frontend/layout/css/custom.css" rel="stylesheet">

    
    <!-- <link href="assets/global/demo/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
    <style type="text/css" media="all" id="siteorigin-panels-grids-wp_head">
        #pg-12-0,
        #pg-12-1,
        #pg-12-3,
        #pg-12-4,
        #pg-12-5{
            margin-bottom: 30px
        }
        #pl-12 .panel-grid-cell .so-panel {
            margin-bottom: 0px
        }
        
        #pgc-12-1-0,
        #pgc-12-1-1,
        #pgc-12-1-2 {
            width: 33.333%
        }
        
        #pg-12-1 .panel-grid-cell,
        #pg-12-3 .panel-grid-cell {
            float: left
        }
        
        #pg-12-2 {
            margin-bottom: 0px
        }
        
        #pgc-12-3-0,
        #pgc-12-3-1 {
            width: 50%
        }
        
        #pg-12-6 {
            margin-bottom: 65px
        }
        
        #pl-12 .panel-grid-cell .so-panel:last-child {
            margin-bottom: 0px
        }
        
        #pg-12-1 {
            margin-left: -2px;
            margin-right: -2px
        }
        
        #pg-12-1 .panel-grid-cell {
            padding-left: 2px;
            padding-right: 2px
        }
        
        #pg-12-3 {
            margin-left: -15px;
            margin-right: -15px
        }
        
        #pg-12-3 .panel-grid-cell {
            padding-left: 15px;
            padding-right: 15px
        }
        
        @media (max-width:767px) {
            #pg-12-0 .panel-grid-cell,
            #pg-12-1 .panel-grid-cell,
            #pg-12-2 .panel-grid-cell,
            #pg-12-3 .panel-grid-cell,
            #pg-12-4 .panel-grid-cell,
            #pg-12-5 .panel-grid-cell,
            #pg-12-6 .panel-grid-cell,
            #pg-12-7 .panel-grid-cell {
                float: none;
                width: auto
            }
            #pgc-12-1-0,
            #pgc-12-1-1,
            #pgc-12-3-0 {
                margin-bottom: 15px
            }
            #pl-12 .panel-grid {
                margin-left: 0;
                margin-right: 0
            }
            #pl-12 .panel-grid-cell {
                padding: 0
            }

            .cards li {
                margin: 20px 0 0 0 !important;
            }
            .message-block h2 {
              font-size: 26px !important;
              line-height: 30px !important;
            }
        }

        .no-sticky-logo span.title {
            font-size: 20px;
            font-weight: 600;
            color: #FFF !important;
            font-family: "Roboto Slab" !important;
        }

        .sticky-logo span.title {
            font-size: 20px;
            font-weight: 600;
            color: #000 !important;
            font-family: "Roboto Slab" !important;
        }

        .width-logo>a {
            width: 210px !important;
            margin-top: 4px;
        }

        #main-content {
            background: #fff !important;
        }

        ul.cards {
            list-style: none;
            margin: 0;
            padding: 0;
            margin-right: -15px;
        }
        .cards .title {
            /*font-size: 18px;
            color: #16222D;*/
            font-size: 16px;
            /*color: #ff5700;*/
            color: #333;
            display: block;
            line-height: 1.2;
            font-weight: normal;
            text-transform: capitalize;
            text-align: center;
        }
        .cards .by {
            color: #8190A1;
            display: block;
            margin-top: 5px;
            font-size: 14px;
        }
        .cards .desc {
            display: none;
            color: #293F4F;
            margin-top: 10px;
        }
        .cards .card-info {
            padding: 10px 0px;
        }
        .cards:before,.cards:after {
            content: "  ";
            display: table;
        }

        .cards li:nth-child(4n+1) {
            clear: left;
            margin-left: 0;
        }
        .cards li {
            float: left;
            width: 280px;
            margin: 20px 0 0 20px;
            vertical-align: top;
        }
        .cards li a {
            background: #fff;
            min-height: 260px;
            padding: 10px;
            border: 1px solid #eee;
        }
        .cards .price {
            /*font-size: 20px;
            color: #54B551;
            font-weight: bold;*/
            font-size: 13px;
            color: #f69329;
            font-weight: 600;
        }
        .cards .stud-icon {
            font-size: 20px;
            color: #8190a1;
        }
        
        .cards .stud-count {
            color: #f69329;
            margin-left: 5px;
            font-weight: 600;
            margin-right: 0px;
        }

        .card-details {
            width: 100%;
            margin-top: 10px;
        }
        .fs13 {
            font-size: 13px;
        }
        .mr15 {
            margin-right: 15px;
        }
        .fx {
            -webkit-box-flex: 1;
            -webkit-flex: 1;
            -moz-flex: 1;
            -ms-flex: 1;
            -o-flex: 1;
            flex: 1;
            min-width: 1px;
        }
        .flex, .fx {
            -webkit-box-flex: 1;
            -webkit-flex: 1;
            -ms-flex: 1;
            flex: 1;
            min-width: 1px;
        }
        .fx-dc, .fxdc {
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-direction: column;
            -moz-flex-direction: column;
            -ms-flex-direction: column;
            -o-flex-direction: column;
            flex-direction: column;
        }
        .fxdc {
            width: 100%;
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
        }
.dropup.open > .dropdown-toggle,
.dropdown.open > .dropdown-toggle {
  border-color: #ddd;
}

/***
Dropdown Menu
***/
.dropdown-menu {
  min-width: 175px;
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 1000;
  display: none;
  float: left;
  list-style: none;
  text-shadow: none;
  padding: 0px;
  margin: 10px  0px 0px 0px;
  background-color: #fffff;
  box-shadow: 5px 5px rgba(102, 102, 102, 0.1);
  border: 1px solid #eee;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  -ms-border-radius: 4px;
  -o-border-radius: 4px;
  border-radius: 4px;
}
.dropdown-menu li.divider {
  background: #f1f3f6;
}
.dropdown-menu li > a {
  padding: 8px 14px;
  color: #555;
  text-decoration: none;
  display: block;
  clear: both;
  font-weight: 300;
  line-height: 18px;
  white-space: nowrap;
}
.dropdown-menu li > a > [class^="fa-"],
.dropdown-menu li > a > [class*=" fa-"] {
  color: #888;
}
.dropdown-menu li > a > [class^="icon-"],
.dropdown-menu li > a > [class*=" icon-"] {
  color: #666;
}
.dropdown-menu li > a > [class^="glyphicon-"],
.dropdown-menu li > a > [class*=" glyphicon-"] {
  color: #888;
}
.dropdown-menu li > a:hover,
.dropdown-menu .active > a,
.dropdown-menu .active > a:hover {
  text-decoration: none;
  background-image: none;
  background-color: #f6f6f6;
  color: #555;
  filter: none;
}
.dropdown-menu.bottom-up {
  top: auto;
  bottom: 100%;
  margin-bottom: 2px;
}

.dropdown > .dropdown-menu,
.dropdown-toggle > .dropdown-menu,
.btn-group > .dropdown-menu {
  margin-top: 10px;
}
.dropdown > .dropdown-menu:before,
.dropdown-toggle > .dropdown-menu:before,
.btn-group > .dropdown-menu:before {
  position: absolute;
  top: -8px;
  left: 9px;
  right: auto;
  display: inline-block !important;
  border-right: 8px solid transparent;
  border-bottom: 8px solid #e0e0e0;
  border-left: 8px solid transparent;
  content: '';
}
.dropdown > .dropdown-menu:after,
.dropdown-toggle > .dropdown-menu:after,
.btn-group > .dropdown-menu:after {
  position: absolute;
  top: -7px;
  left: 10px;
  right: auto;
  display: inline-block !important;
  border-right: 7px solid transparent;
  border-bottom: 7px solid #fff;
  border-left: 7px solid transparent;
  content: '';
}
.dropdown > .dropdown-menu.pull-left:before,
.dropdown-toggle > .dropdown-menu.pull-left:before,
.btn-group > .dropdown-menu.pull-left:before {
  left: auto;
  right: 9px;
}
.dropdown > .dropdown-menu.pull-left:after,
.dropdown-toggle > .dropdown-menu.pull-left:after,
.btn-group > .dropdown-menu.pull-left:after {
  left: auto;
  right: 10px;
}
.dropdown > .dropdown-menu.pull-right:before,
.dropdown-toggle > .dropdown-menu.pull-right:before,
.btn-group > .dropdown-menu.pull-right:before {
  left: auto;
  right: 9px;
}
.dropdown > .dropdown-menu.pull-right:after,
.dropdown-toggle > .dropdown-menu.pull-right:after,
.btn-group > .dropdown-menu.pull-right:after {
  left: auto;
  right: 10px;
}
.dropdown.dropup > .dropdown-menu,
.dropdown-toggle.dropup > .dropdown-menu,
.btn-group.dropup > .dropdown-menu {
  margin-top: 0px;
  margin-bottom: 10px;
}
.dropdown.dropup > .dropdown-menu:after, .dropdown.dropup > .dropdown-menu:before,
.dropdown-toggle.dropup > .dropdown-menu:after,
.dropdown-toggle.dropup > .dropdown-menu:before,
.btn-group.dropup > .dropdown-menu:after,
.btn-group.dropup > .dropdown-menu:before {
  display: none !important;
}

/* Dropdown submenu support for Bootsrap 3 */
.dropdown-submenu {
  position: relative;
}
.dropdown-submenu > .dropdown-menu {
  top: 5px;
  left: 100%;
  margin-top: -6px;
  margin-left: -1px;
}
.dropdown-submenu > a:after {
  position: absolute;
  display: inline-block;
  font-size: 14px;
  right: 7px;
  top: 7px;
  font-family: FontAwesome;
  height: auto;
  content: "\f105";
  font-weight: 300;
}
.dropdown-submenu:hover > .dropdown-menu {
  display: block;
}
.dropdown-submenu:hover > a:after {
  border-left-color: #ffffff;
}
.dropdown-submenu.pull-left {
  float: none;
}
.dropdown-submenu.pull-left > .dropdown-menu {
  left: -100%;
  margin-left: 10px;
}
.dropup .dropdown-submenu > .dropdown-menu {
  top: auto;
  bottom: 0;
  margin-top: 0;
  margin-bottom: -2px;
}

.nav.pull-right > li > .dropdown-menu,
.nav > li > .dropdown-menu.pull-right {
  right: 0;
  left: auto;
}
.nav.pull-right > li > .dropdown-menu:before,
.nav > li > .dropdown-menu.pull-right:before {
  right: 12px;
  left: auto;
}
.nav.pull-right > li > .dropdown-menu:after,
.nav > li > .dropdown-menu.pull-right:after {
  right: 13px;
  left: auto;
}
.nav.pull-right > li > .dropdown-menu .dropdown-menu,
.nav > li > .dropdown-menu.pull-right .dropdown-menu {
  right: 100%;
  left: auto;
  margin-right: -1px;
  margin-left: 0;
}
    </style>