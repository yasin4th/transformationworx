<?php
	require_once 'config.inc.php';
	require_once 'config.inc.test.php';
	@session_start(); if(isset($_SESSION['role'])) { header('location: my-program');}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
	<?php include HEADER; ?>

	<!-- Navigation END -->

	<div class="main">
	  <div class="container">
		<ul class="breadcrumb">
			<li><a href="<?=URI1?>">Home</a></li>
			<li class="active">Login</li>
		</ul>
		<!-- BEGIN SIDEBAR & CONTENT -->
		<div class="row margin-bottom-40">
			<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			  <!-- BEGIN CONTENT -->
			  <div class="register-div">
				<br>
				<h1 class="text-center">Login</h1>
				<div class="content-form-page">
				  <div class="row">
					<div class="col-md-12 col-sm-12">
						<form id="login-form" class="form-horizontal" role="form" method="POST">
							<div class="col-lg-12">
								<div class="row">
									<!-- <div class="col-lg-8 col-lg-offset-2"> -->
										<div class="text-center alert" style="display:none;"></div>
									<!-- </div> -->
									<!-- <div class="col-lg-2"></div> -->

								</div>
							</div>
							<div class="form-group">
								<label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
								<div class="col-lg-8">
									<input type="text" class="form-control" id="email" name="email" placeholder="Email" pattern="[a-zA-Z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-zA-Z]{2,3}$" required="required" />
								</div>
							</div>
							<div class="form-group">
								<label for="password" class="col-lg-4 control-label">Password <span class="require">*</span></label>
								<div class="col-lg-8">
									<input class="form-control" id="password" type="password" autocomplete="off" placeholder="Password" name="password" required="required" />
									<!--<input type="text" class="form-control" id="password">-->
								</div>
							</div>
							<div class="row">
								<div class="col-lg-8 col-md-offset-4 padding-left-0">
									<input type="submit" class="btn btn-primary text-white" value="Login"/>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
									<a href="forgot_password.php">Forgot Password?</a>
									<!-- <p>Need an account ?<strong> <a href="register">click here.</a></strong></p> -->
								</div>
							</div>
							<!-- <div class="row">
								<div class="col-lg-12 text-center margin-top-20">
									<h6>OR</h6>
								</div>
								<div class="col-lg-12 text-center margin-top-20">
									<a href="fbaccess.php" class="btn btn-facebook btn-sociallogin" title="Facebook"><i class="fa fa-facebook"></i> Sign in with Facebook</a>
								</div>
							</div> -->
						</form>
					</div>

				  </div>
				</div>
			  </div>
			</div>
		  </div>
		  <!-- END CONTENT -->
		</div>
		<!-- END SIDEBAR & CONTENT -->
	  </div>

	<!-- BEGIN PRE-FOOTER -->
	<?php include FOOTER; ?>

	<!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
	<?php include('html/js-files.php'); ?>



	<script src="assets/js/custom/studentLogin.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
