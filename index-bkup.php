<html >

<head>
    <?php @include 'new-html/head-tag.php';?>
</head>

<body class="home page page-id-12 page-template page-template-page-templates page-template-homepage page-template-page-templateshomepage-php  thim-body-preload siteorigin-panels siteorigin-panels-home group-blog" id="thim-body">
    <div id="preload">
        <div class="cssload-loader">
            <div class="cssload-inner cssload-one"></div>
            <div class="cssload-inner cssload-two"></div>
            <div class="cssload-inner cssload-three"></div>
        </div>
    </div>

    <div id="wrapper-container" class="wrapper-container">
        <div class="content-pusher ">
            <header id="masthead" class="site-header affix-top bg-custom-sticky sticky-header header_overlay header_v1">
                <div id="toolbar" class="toolbar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <aside id="text-2" class="widget widget_text">
                                    <div class="textwidget">
                                        <div class="thim-have-any-question">
                                            Have any question?
                                            <div class="mobile"><i class="fa fa-phone"></i><a href="tel:0882715488" class="value">(+91) 888 271 5488 </a>
                                            </div>
                                            <div class="email"><i class="fa fa-envelope"></i><a href="mailto:info@4thpointer.com"><span class="" >info@4thpointer.com</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </aside>
                                <aside id="login-menu-2" class="widget widget_login-menu">
                                    <div class="thim-widget-login-menu thim-widget-login-menu-base">
                                        <div class="thim-link-login">
                                                
                                                <?php 
                                                    @session_start();
                                                    if(!isset($_SESSION['role']))
                                                    {
                                                ?>
                                                <a class="login" href="login.php">Login</a>
                                                <a class="register" href="registration.php">Register</a>
                                                <?php
                                                    }else{
                                                ?>
                                                <ul class="list-unstyled list-inline pull-right">
                                                  <?php include 'html/student/navigation-ul.php';?>
                                                </ul>
                                                <?php
                                                    }
                                                ?>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="row">
                        <div class="navigation col-sm-12">
                            <div class="tm-table">
                                <div class="width-logo table-cell sm-logo">
                                    <!-- <a href="index.php" title="E-learning Demo" rel="home" class="no-sticky-logo"><img src="http://educationwp.thimpress.com/wp-content/uploads/2015/11/logo.png" alt="Education" width="153" height="40" /></a> -->
                                    <a href="index.php" title="E-learning Demo" rel="home" class="no-sticky-logo">
                                        <img src="assets/demo/LOGO11.png" alt="Education" width="60" height="40" style="margin-bottom: 0px;" />
                                        <span class="title">E-Learning</span>
                                    </a>
                                    <a href="index.php" title="Education WP - Education WordPress Theme" rel="home" class="sticky-logo">
                                        <img src="assets/demo/LOGO11.png" alt="Education" width="60" height="40" style="margin-bottom: 0px;" />
                                        <span class="title">E-Learning</span>
                                    </a>
                                </div>
                                <nav class="width-navigation table-cell table-right">
                                    <ul class="nav navbar-nav menu-main-menu">
                                        <!-- <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-has-children drop_to_right multicolumn"><span class="disable_link"><span data-hover="Demos">Demos</span></span>
                                            <ul class="sub-menu megacol submenu_columns_3">
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children">
                                                    <ul class="sub-menu">
                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home"><a href="//educationwp.thimpress.com/">Demo Version 1</a></li>
                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-2/">Demo Version 2</a></li>
                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-3/">Demo Version 3</a></li>
                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-languages-school/">Demo Languages School</a></li>
                                                    </ul>
                                                </li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                                                    <ul class="sub-menu">
                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-university/">Demo University 1</a></li>
                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-university-2/">Demo University 2</a></li>
                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom drop_to_right standard"><a href="//educationwp.thimpress.com/demo-courses-hub/">Demo Courses Hub</a></li>
                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-one-instructor/">Demo One Instructor</a></li>
                                                    </ul>
                                                </li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                                                    <ul class="sub-menu">
                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-one-course/">Demo One Course</a></li>
                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-boxed/">Demo Boxed</a></li>
                                                        <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-rtl/">Demo RTL</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li> -->
                                        <!--<li class="menu-item menu-item-type-custom menu-item-object-custom drop_to_right widget_area"><a href="all-programs.php"><span>Courses</span></a>
                                             <ul class="sub-menu submenu_columns_1 submenu-widget">
                                               <li id="siteorigin-panels-builder-10" class="widget widget_siteorigin-panels-builder">
                                                   <div id="pl-w56d3feac8c634">
                                                       <style scoped>
                                                           #pgc-w56d3feac8c634-0-0,
                                                           #pgc-w56d3feac8c634-0-1 {
                                                               width: 31%
                                                           }
                                                           
                                                           #pgc-w56d3feac8c634-0-2 {
                                                               width: 38%
                                                           }
                                                           
                                                           #pg-w56d3feac8c634-0 .panel-grid-cell {
                                                               float: left
                                                           }
                                                           
                                                           #pl-w56d3feac8c634 .panel-grid-cell .so-panel {
                                                               margin-bottom: 30px
                                                           }
                                                           
                                                           #pl-w56d3feac8c634 .panel-grid-cell .so-panel:last-child {
                                                               margin-bottom: 0px
                                                           }
                                                           
                                                           #pg-w56d3feac8c634-0 {
                                                               margin-left: -15px;
                                                               margin-right: -15px
                                                           }
                                                           
                                                           #pg-w56d3feac8c634-0 .panel-grid-cell {
                                                               padding-left: 15px;
                                                               padding-right: 15px
                                                           }
                                                           
                                                           @media (max-width:767px) {
                                                               #pg-w56d3feac8c634-0 .panel-grid-cell {
                                                                   float: none;
                                                                   width: auto
                                                               }
                                                               #pgc-w56d3feac8c634-0-0,
                                                               #pgc-w56d3feac8c634-0-1 {
                                                                   margin-bottom: 30px
                                                               }
                                                               #pl-w56d3feac8c634 .panel-grid {
                                                                   margin-left: 0;
                                                                   margin-right: 0
                                                               }
                                                               #pl-w56d3feac8c634 .panel-grid-cell {
                                                                   padding: 0
                                                               }
                                                           }
                                                       </style>
                                                       <div class="panel-grid" id="pg-w56d3feac8c634-0">
                                                           <div class="panel-row-style-thim-megamenu-row thim-megamenu-row panel-row-style">
                                                               <div class="panel-grid-cell" id="pgc-w56d3feac8c634-0-0">
                                                                   <div class="so-panel widget widget_nav_menu panel-first-child panel-last-child" id="panel-w56d3feac8c634-0-0-0" data-index="0">
                                                                       <h3 class="widget-title">About Courses</h3>
                                                                       <div class=" megaWrapper">
                                                                           <ul id="menu-about-courses" class="menu">
                                                                               <li class="menu-item menu-item-type-post_type menu-item-object-lp_course drop_to_right standard"><a href="http://educationwp.thimpress.com/courses/learnpress-101/"><span data-hover="Free Access Type">Free Access Type</span></a></li>
                                                                               <li class="menu-item menu-item-type-post_type menu-item-object-lp_course drop_to_right standard"><a href="http://educationwp.thimpress.com/courses/node/"><span data-hover="Other Free Type">Other Free Type</span></a></li>
                                                                               <li class="menu-item menu-item-type-post_type menu-item-object-lp_course drop_to_right standard"><a href="http://educationwp.thimpress.com/courses/become-a-php-master-and-make-money-fast/"><span data-hover="Paid Type">Paid Type</span></a></li>
                                                                               <li class="menu-item menu-item-type-post_type menu-item-object-lp_course drop_to_right standard"><a href="http://educationwp.thimpress.com/courses/improve-your-css-workflow-with-sass/"><span data-hover="Other Paid Type">Other Paid Type</span></a></li>
                                                                               <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="http://educationwp.thimpress.com/courses/"><span data-hover="Courses Archive">Courses Archive</span></a></li>
                                                                               <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="http://educationwp.thimpress.com/demo-accounts/"><span data-hover="Demo Accounts">Demo Accounts</span></a></li>
                                                                               <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="http://educationwp.thimpress.com/become-a-teacher/"><span data-hover="Become an Instructor">Become an Instructor</span></a></li>
                                                                               <li class="menu-item menu-item-type-custom menu-item-object-custom drop_to_right standard"><a href="//educationwp.thimpress.com/profile/keny/"><span data-hover="Instructor Profile">Instructor Profile</span></a></li>
                                                                           </ul>
                                                                       </div>
                                                                   </div>
                                                               </div>
                                                               <div class="panel-grid-cell" id="pgc-w56d3feac8c634-0-1">
                                                                   <div class="so-panel widget widget_courses panel-first-child panel-last-child" id="panel-w56d3feac8c634-0-1-0" data-index="1">
                                                                       <div class="thim-widget-courses thim-widget-courses-base">
                                                                           <h3 class="widget-title">New Course</h3>
                                                                           <div class="thim-course-megamenu">
                                                                               <div class="lpr_course course-grid-1">
                                                                                   <div class="course-item">
                                                                                       <div class="course-thumbnail">
                                                                                           <a href="http://educationwp.thimpress.com/courses/learnpress-101/"><img src="http://educationwp.thimpress.com/wp-content/uploads/2015/11/course-4-450x450.jpg" alt="Introduction LearnPress &#8211; LMS plugin" title="course-4" width="450" height="450"></a>
                                                                                       </div>
                                                                                       <div class="thim-course-content">
                                                                                           <h2 class="course-title">
                                                                                           <a href="http://educationwp.thimpress.com/courses/learnpress-101/"> Introduction LearnPress &#8211; LMS plugin</a>
                                                                                       </h2>
                                                                                           <div class="course-meta">
                                                                                               <div class="course-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                                                                                   <div class="value free-course" itemprop="price" content="Free">
                                                                                                       Free </div>
                                                                                                   <meta itemprop="priceCurrency" content="&#36;" />
                                                                                               </div>
                                                                                           </div>
                                                                                           <a class="course-readmore" href="http://educationwp.thimpress.com/courses/learnpress-101/">Read More</a> </div>
                                                                                   </div>
                                                                               </div>
                                                                           </div>
                                                                       </div>
                                                                   </div>
                                                               </div>
                                                               <div class="panel-grid-cell" id="pgc-w56d3feac8c634-0-2">
                                                                   <div class="so-panel widget widget_single-images panel-first-child panel-last-child" id="panel-w56d3feac8c634-0-2-0" data-index="2">
                                                                       <div class="thim-widget-single-images thim-widget-single-images-base">
                                                                           <div class="single-image text-left">
                                                                               <a href="#">
                                                                                   <img src="http://educationwp.thimpress.com/wp-content/uploads/2016/02/megamenu.jpg" width="252" height="359" alt="" />
                                                                               </a>
                                                                           </div>
                                                                       </div>
                                                                   </div>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </li>
                                           </ul> 
                                        </li>-->
                                         <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="all-programs.php"><span data-hover="Programs">Packages</span></a></li>
                                        <?php if(isset($_SESSION['role']))
                                        {
                                          if($_SESSION['role'] == 4) 
                                          { ?>
                                            <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="my-program.php"><span data-hover="My Packages">My Packages</span></a></li>
                                            
                                            <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="student-profile.php"><span data-hover="Events">My Profile</span></a></li>
                                               
                                            </li>
                                            <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="report-card.php"><span data-hover="Events">Report Card</span></a></li>
                                               
                                            </li>
                                    <?php }} ?>
                                        <!-- <li class="menu-item menu-item-type-custom menu-item-object-custom drop_to_left standard"><span class="disable_link"><span data-hover="Features">Features</span></span>
                                            <ul class="sub-menu">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#">Gallery</a></li>
                                                <li class="menu-item menu-item-type-post_type_archive menu-item-object-forum"><a href="#">Forums</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#">About Us</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#">FAQs</a></li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="#">404 Page</a></li>
                                            </ul>
                                        </li> -->
                                        <!-- <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="#"><span data-hover="Events">EVENTS</span></a></li> -->
                                            <!-- <ul class="sub-menu">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#">Event Account</a></li>
                                            </ul> -->
                                        <!-- </li> -->
                                        <!-- <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children drop_to_right standard"><span class="disable_link"><span data-hover="Portfolio">Portfolio</span></span>
                                            <ul class="sub-menu">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#">Portfolio Grid</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#">Portfolio Masonry</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#">Portfolio Multigrid</a></li>
                                            </ul>
                                        </li> -->
                                        <!-- <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="#"><span data-hover="Blog">Blog</span></a></li> -->
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_left standard"><a href="contact_us.php"><span data-hover="Contact">Contact</span></a></li>
                                        <!-- <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="#"><span data-hover="Shop">Shop</span></a></li> -->
                                        <li class="menu-right">
                                            <ul>
                                                <li id="courses-searching-2" >
                                                    <div >
                                                        <div>
                                                            <!-- <div class="search-toggle"><i class="fa fa-search"></i></div> -->
                                                            <a href="#search-packages" data-toggle="modal" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></a>
                                                            <!-- Start modal -->
                                                            <div class="modal fade bs-modal-lg" id="search-packages" tabindex="-1" role="dialog" aria-hidden="true">
                                                            <div class="modal-dialog modal-sm">
                                                            <form id="search-packages-form" method="get" action="all-programs.php">
                                                            <div class="modal-content">
                     
                                                            <div class="modal-body">
                                                            <div class="row">
                                                            <div class="col-md-12">
                                                            <div class="row">
                                                            <div class="col-md-12">
                                                            <div class="form-group"> <div class="col-md-10"> <input id="search-box" name="q" class="form-control input-sm" placeholder="Search packages..." required="required"></div>
                                                            <div class="col-md-2"><button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button></div>
                                                             </div>
                                                          </div>
                                                    </div>
                                                  </div>
                                                    </div>
                                                </div>
                                                           <!--  <div class="modal-footer"> <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button></div> -->
                                                        </div>
                                                  <!-- /.modal-content -->
                                                  </form>
                                                </div>
                                            <!-- /.modal-dialog -->
                                            </div>
                                              <!-- End modal -->
                                                            <!-- <div class="courses-searching layout-overlay">
                                                                <div class="search-popup-bg"></div>
                                                                <form method="get" action="all-programs.php">
                                                                    <input type="text" value="" name="q" placeholder="Search programs..." class="thim-s form-control courses-search-input" autocomplete="off" />
                                                                    <input type="hidden" value="course" name="ref" />
                                                                    <button type="submit"><i class="fa fa-search"></i></button>
                                                                    <span class="widget-search-close"></span>
                                                                </form>
                                                                <ul class="courses-list-search list-unstyled"></ul>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                                <div class="menu-mobile-effect navbar-toggle" data-effect="mobile-effect">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </div>
                                <br>
                            </div>

                        </div>
                    </div>
                </div>
            </header>

            <nav class="mobile-menu-container mobile-effect">
                <ul class="nav navbar-nav">
                    <!-- <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-has-children drop_to_right multicolumn"><span class="disable_link"><span data-hover="Demos">Demos</span></span>
                        <ul class="sub-menu megacol submenu_columns_3">
                            <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children">
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home"><a href="//educationwp.thimpress.com/">Demo Version 1</a></li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-2/">Demo Version 2</a></li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-3/">Demo Version 3</a></li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-languages-school/">Demo Languages School</a></li>
                                </ul>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-university/">Demo University 1</a></li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-university-2/">Demo University 2</a></li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom drop_to_right standard"><a href="//educationwp.thimpress.com/demo-courses-hub/">Demo Courses Hub</a></li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-one-instructor/">Demo One Instructor</a></li>
                                </ul>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-one-course/">Demo One Course</a></li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-boxed/">Demo Boxed</a></li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/demo-rtl/">Demo RTL</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom drop_to_right widget_area"><a href="//educationwp.thimpress.com/courses/"><span data-hover="Courses">Courses</span></a>
                        <ul class="sub-menu submenu_columns_1 submenu-widget">
                            <li id="siteorigin-panels-builder-10" class="widget widget_siteorigin-panels-builder">
                                <div id="pl-w56d3feac8c634">
                                    <style scoped>
                                        #pgc-w56d3feac8c634-0-0,
                                        #pgc-w56d3feac8c634-0-1 {
                                            width: 31%
                                        }
                                        
                                        #pgc-w56d3feac8c634-0-2 {
                                            width: 38%
                                        }
                                        
                                        #pg-w56d3feac8c634-0 .panel-grid-cell {
                                            float: left
                                        }
                                        
                                        #pl-w56d3feac8c634 .panel-grid-cell .so-panel {
                                            margin-bottom: 30px
                                        }
                                        
                                        #pl-w56d3feac8c634 .panel-grid-cell .so-panel:last-child {
                                            margin-bottom: 0px
                                        }
                                        
                                        #pg-w56d3feac8c634-0 {
                                            margin-left: -15px;
                                            margin-right: -15px
                                        }
                                        
                                        #pg-w56d3feac8c634-0 .panel-grid-cell {
                                            padding-left: 15px;
                                            padding-right: 15px
                                        }
                                        
                                        @media (max-width:767px) {
                                            #pg-w56d3feac8c634-0 .panel-grid-cell {
                                                float: none;
                                                width: auto
                                            }
                                            #pgc-w56d3feac8c634-0-0,
                                            #pgc-w56d3feac8c634-0-1 {
                                                margin-bottom: 30px
                                            }
                                            #pl-w56d3feac8c634 .panel-grid {
                                                margin-left: 0;
                                                margin-right: 0
                                            }
                                            #pl-w56d3feac8c634 .panel-grid-cell {
                                                padding: 0
                                            }
                                        }
                                    </style>
                                    <div class="panel-grid" id="pg-w56d3feac8c634-0">
                                        <div class="panel-row-style-thim-megamenu-row thim-megamenu-row panel-row-style">
                                            <div class="panel-grid-cell" id="pgc-w56d3feac8c634-0-0">
                                                <div class="so-panel widget widget_nav_menu panel-first-child panel-last-child" id="panel-w56d3feac8c634-0-0-0" data-index="0">
                                                    <h3 class="widget-title">About Courses</h3>
                                                    <div class=" megaWrapper">
                                                        <ul id="menu-about-courses-1" class="menu">
                                                            <li class="menu-item menu-item-type-post_type menu-item-object-lp_course drop_to_right standard"><a href="http://educationwp.thimpress.com/courses/learnpress-101/"><span data-hover="Free Access Type">Free Access Type</span></a></li>
                                                            <li class="menu-item menu-item-type-post_type menu-item-object-lp_course drop_to_right standard"><a href="http://educationwp.thimpress.com/courses/node/"><span data-hover="Other Free Type">Other Free Type</span></a></li>
                                                            <li class="menu-item menu-item-type-post_type menu-item-object-lp_course drop_to_right standard"><a href="http://educationwp.thimpress.com/courses/become-a-php-master-and-make-money-fast/"><span data-hover="Paid Type">Paid Type</span></a></li>
                                                            <li class="menu-item menu-item-type-post_type menu-item-object-lp_course drop_to_right standard"><a href="http://educationwp.thimpress.com/courses/improve-your-css-workflow-with-sass/"><span data-hover="Other Paid Type">Other Paid Type</span></a></li>
                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="http://educationwp.thimpress.com/courses/"><span data-hover="Courses Archive">Courses Archive</span></a></li>
                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="http://educationwp.thimpress.com/demo-accounts/"><span data-hover="Demo Accounts">Demo Accounts</span></a></li>
                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="http://educationwp.thimpress.com/become-a-teacher/"><span data-hover="Become an Instructor">Become an Instructor</span></a></li>
                                                            <li class="menu-item menu-item-type-custom menu-item-object-custom drop_to_right standard"><a href="//educationwp.thimpress.com/profile/keny/"><span data-hover="Instructor Profile">Instructor Profile</span></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-grid-cell" id="pgc-w56d3feac8c634-0-1">
                                                <div class="so-panel widget widget_courses panel-first-child panel-last-child" id="panel-w56d3feac8c634-0-1-0" data-index="1">
                                                    <div class="thim-widget-courses thim-widget-courses-base">
                                                        <h3 class="widget-title">New Course</h3>
                                                        <div class="thim-course-megamenu">
                                                            <div class="lpr_course course-grid-1">
                                                                <div class="course-item">
                                                                    <div class="course-thumbnail">
                                                                        <a href="http://educationwp.thimpress.com/courses/learnpress-101/"><img src="http://educationwp.thimpress.com/wp-content/uploads/2015/11/course-4-450x450.jpg" alt="Introduction LearnPress &#8211; LMS plugin" title="course-4" width="450" height="450"></a>
                                                                    </div>
                                                                    <div class="thim-course-content">
                                                                        <h2 class="course-title">
                                            <a href="http://educationwp.thimpress.com/courses/learnpress-101/"> Introduction LearnPress &#8211; LMS plugin</a>
                                        </h2>
                                                                        <div class="course-meta">
                                                                            <div class="course-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                                                                <div class="value free-course" itemprop="price" content="Free">
                                                                                    Free </div>
                                                                                <meta itemprop="priceCurrency" content="&#36;" />
                                                                            </div>
                                                                        </div>
                                                                        <a class="course-readmore" href="http://educationwp.thimpress.com/courses/learnpress-101/">Read More</a> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-grid-cell" id="pgc-w56d3feac8c634-0-2">
                                                <div class="so-panel widget widget_single-images panel-first-child panel-last-child" id="panel-w56d3feac8c634-0-2-0" data-index="2">
                                                    <div class="thim-widget-single-images thim-widget-single-images-base">
                                                        <div class="single-image text-left">
                                                            <a href="#"><img src="http://educationwp.thimpress.com/wp-content/uploads/2016/02/megamenu.jpg" width="252" height="359" alt="" /></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children drop_to_left standard"><span class="disable_link"><span data-hover="Features">Features</span></span>
                        <ul class="sub-menu">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://educationwp.thimpress.com/gallery/">Gallery</a></li>
                            <li class="menu-item menu-item-type-post_type_archive menu-item-object-forum"><a href="http://educationwp.thimpress.com/forums/">Forums</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://educationwp.thimpress.com/about-us/">About Us</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://educationwp.thimpress.com/faqs/">FAQs</a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="//educationwp.thimpress.com/404-page">404 Page</a></li>
                        </ul>
                    </li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children drop_to_right standard"><a href="//educationwp.thimpress.com/events/"><span data-hover="Events">Events</span></a>
                        <ul class="sub-menu">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://educationwp.thimpress.com/event-account/">Event Account</a></li>
                        </ul>
                    </li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children drop_to_right standard"><span class="disable_link"><span data-hover="Portfolio">Portfolio</span></span>
                        <ul class="sub-menu">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://educationwp.thimpress.com/portfolio-grid/">Portfolio Grid</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://educationwp.thimpress.com/portfolio-masonry/">Portfolio Masonry</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://educationwp.thimpress.com/portfolio-multigrid/">Portfolio Multigrid</a></li>
                        </ul>
                    </li> -->
                    <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="all-programs.php"><span data-hover="Programs">Programs</span></a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_left standard"><a href="#"><span data-hover="Features">Features</span></a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="#"><span data-hover="Events">Events</span></a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_left standard"><a href="#"><span data-hover="Blog">Blog</span></a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="contact_us.php"><span data-hover="Contact">Contact</span></a></li>
                </ul>
            </nav>
            <div id="main-content">
                <div id="main-home-content" class="home-content home-page container" role="main">
                    <div id="pl-12">
                        <div class="panel-grid" id="pg-12-0">
                            <div class="siteorigin-panels-stretch thim-fix-stretched panel-row-style" style="height: 100vh;" data-stretch-type="full-stretched">
                                <div class="panel-grid-cell" id="pgc-12-0-0">
                                    <div class="so-panel widget widget_text panel-first-child panel-last-child" id="panel-12-0-0-0" data-index="0">
                                        <div class="textwidget">
                                            <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullscreen-container" style="background-color:transparent;padding:0px;background-image:url(assets/demo/top-slider.jpg);background-repeat:no-repeat;background-size:cover;background-position:center center;">

                                                <div id="rev_slider_4_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.2.4.1">
                                                    <ul>
                                                        <li data-index="rs-17" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="assets/demo/top-slider-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                                                            <img src="assets/demo/dummy.png" alt="" title="top-slider" width="1600" height="900" data-lazyload="assets/demo/top-slider.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>


                                                            <div class="tp-caption    thim-slider-sub-heading" id="slide-17-layer-1" data-x="['left','left','left','left']" data-hoffset="['15','15','15','15']" data-y="['middle','middle','middle','middle']" data-voffset="['-146','-150','-45','-20']" data-fontsize="['24','24','','20']" data-width="none" data-height="" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;s:1800;e:Power4.easeInOut;" data-transform_out="opacity:0;s:100;e:Power2.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="300" data-splitin="chars" data-splitout="none" data-responsive_offset="off" data-responsive="off" data-elementdelay="0.05" style="z-index: 5; max-width: px; max-width: px; white-space: nowrap; font-size: 24px; line-height: 30px; font-weight: 700; color: rgba(252, 252, 252, 1.00);text-transform:left;">WELCOME TO THE NEW WORLD OF </div>

                                                            <h3 class="tp-caption    thim-slider-heading" id="slide-17-layer-2" data-x="['left','left','left','left']" data-hoffset="['15','15','15','15']" data-y="['middle','middle','middle','middle']" data-voffset="['-75','-85','10','20']" data-fontsize="['100','100','','60']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="opacity:0;s:100;e:Power2.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="201.875" data-splitin="chars" data-splitout="none" data-responsive_offset="off" data-responsive="off" data-elementdelay="0.05" style="z-index: 6; white-space: nowrap; font-size: 100px; line-height: 100px; font-weight: 700; color: rgba(255, 255, 255, 1.00);text-transform:left;">ONLINE ASSESSMENT</h3>

                                                            <a class="tp-caption rev-btn   thim-slider-button" href="all-programs.php" target="_blank" id="slide-17-layer-3" data-x="['left','left','left','left']" data-hoffset="['15','15','15','15']" data-y="['middle','middle','middle','middle']" data-voffset="['20','10','80','80']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;" data-style_hover="c:rgba(51, 51, 51, 1.00);bc:rgba(0, 0, 0, 0);" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:100;e:Power2.easeIn;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-start="1300" data-splitin="none" data-splitout="none" data-actions='' data-responsive_offset="off" data-responsive="off" style="z-index: 7; white-space: nowrap; font-size: 13px; line-height: 20px; font-weight: 700; color: rgba(51, 51, 51, 1.00);text-transform:left;padding:10px 30px 10px 30px;border-color:rgba(51, 51, 51, 0);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"> PROGRAMS </a>

                                                            <div class="tp-caption  " id="slide-17-layer-4" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['40','40','30','20']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:100;" data-start="500" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"scrollbelow","offset":"0px"}]' data-basealign="slide" data-responsive_offset="off" data-responsive="off" style="z-index: 8; white-space: nowrap; font-size: 14px; line-height: 14px; font-weight: 400; color: rgba(191, 185, 184, 1.00);text-transform:left;background-color:rgba(255, 255, 255, 0);cursor:pointer;">
                                                                <div class="thim-click-to-bottom"><i class="fa  fa-chevron-down"></i></div>
                                                            </div>
                                                        </li>

                                                        <li data-index="rs-19" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="assets/demo/slide_5-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                                                            <img src="assets/demo/dummy.png" alt="" title="slide_5" width="1600" height="900" data-lazyload="assets/demo/slide_5.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>


                                                            <div class="tp-caption    thim-slider-sub-heading" id="slide-19-layer-1" data-x="['left','left','left','left']" data-hoffset="['15','15','15','15']" data-y="['middle','middle','middle','middle']" data-voffset="['-146','-150','-45','-20']" data-fontsize="['24','24','','20']" data-width="none" data-height="" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;s:1800;e:Power4.easeInOut;" data-transform_out="opacity:0;s:100;e:Power2.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="300" data-splitin="chars" data-splitout="none" data-responsive_offset="off" data-responsive="off" data-elementdelay="0.05" style="z-index: 5; max-width: px; max-width: px; white-space: nowrap; font-size: 24px; line-height: 30px; font-weight: 700; color: rgba(252, 252, 252, 1.00);text-transform:left;">WELCOME TO THE NEW WORLD OF </div>

                                                            <h3 class="tp-caption    thim-slider-heading" id="slide-19-layer-2" data-x="['left','left','left','left']" data-hoffset="['14','15','15','15']" data-y="['middle','middle','middle','middle']" data-voffset="['-75','-85','10','20']" data-fontsize="['100','100','','60']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="opacity:0;s:100;e:Power2.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="500" data-splitin="chars" data-splitout="none" data-responsive_offset="off" data-responsive="off" data-elementdelay="0.05" style="z-index: 6; white-space: nowrap; font-size: 100px; line-height: 100px; font-weight: 700; color: rgba(255, 255, 255, 1.00);text-transform:left;">ONLINE ASSESSMENT </h3>

                                                            <a class="tp-caption rev-btn   thim-slider-button" href="all-programs.php" target="_blank" id="slide-19-layer-3" data-x="['left','left','left','left']" data-hoffset="['15','15','15','15']" data-y="['middle','middle','middle','middle']" data-voffset="['20','10','80','80']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;" data-style_hover="c:rgba(51, 51, 51, 1.00);" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:100;e:Power2.easeIn;" data-mask_in="x:0px;y:[100%];" data-start="1300" data-splitin="none" data-splitout="none" data-actions='' data-responsive_offset="off" data-responsive="off" style="z-index: 7; white-space: nowrap; font-size: 13px; line-height: 20px; font-weight: 700; color: rgba(51, 51, 51, 1.00);text-transform:left;padding:10px 30px 10px 30px;border-color:rgba(0, 0, 0, 0);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"> PROGRAMS </a>

                                                            <div class="tp-caption  " id="slide-19-layer-4" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['40','40','30','20']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:100;" data-start="500" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"scrollbelow","offset":"0px"}]' data-basealign="slide" data-responsive_offset="off" data-responsive="off" style="z-index: 8; white-space: nowrap; font-size: 14px; line-height: 14px; font-weight: 400; color: rgba(191, 185, 184, 1.00);text-transform:left;background-color:rgba(255, 255, 255, 0);cursor:pointer;">
                                                                <div class="thim-click-to-bottom"><i class="fa  fa-chevron-down"></i></div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <script>
                                                        var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                                        var htmlDivCss = "";
                                                        if (htmlDiv) {
                                                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                        } else {
                                                            var htmlDiv = document.createElement("div");
                                                            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                                            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                                        }
                                                    </script>
                                                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                                                </div>
                                                <script>
                                                    var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                                    var htmlDivCss = "";
                                                    if (htmlDiv) {
                                                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                    } else {
                                                        var htmlDiv = document.createElement("div");
                                                        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                                        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                                    }
                                                </script>
                                                <script type="text/javascript">
                                                    /******************************************
                                                                    -   PREPARE PLACEHOLDER FOR SLIDER  -
                                                                    ******************************************/

                                                    var setREVStartSize = function () {
                                                        try {
                                                            var e = new Object,
                                                                i = jQuery(window).width(),
                                                                t = 9999,
                                                                r = 0,
                                                                n = 0,
                                                                l = 0,
                                                                f = 0,
                                                                s = 0,
                                                                h = 0;
                                                            e.c = jQuery('#rev_slider_4_1');
                                                            e.responsiveLevels = [1240, 1024, 778, 480];
                                                            e.gridwidth = [1200, 960, 768, 481];
                                                            e.gridheight = [500, 400, 400, 320];

                                                            e.sliderLayout = "fullscreen";
                                                            e.fullScreenAutoWidth = 'on';
                                                            e.fullScreenAlignForce = 'off';
                                                            e.fullScreenOffsetContainer = '';
                                                            e.fullScreenOffset = '';
                                                            if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                                                                    f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                                                                }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                                                                var u = (e.c.width(), jQuery(window).height());
                                                                if (void 0 != e.fullScreenOffsetContainer) {
                                                                    var c = e.fullScreenOffsetContainer.split(",");
                                                                    if (c) jQuery.each(c, function (e, i) {
                                                                        u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                                                                    }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                                                                }
                                                                f = u
                                                            } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                                                            e.c.closest(".rev_slider_wrapper").css({
                                                                height: f
                                                            })

                                                        } catch (d) {
                                                            console.log("Failure at Presize of Slider:" + d)
                                                        }
                                                    };

                                                    setREVStartSize();

                                                    var tpj = jQuery;

                                                    var revapi4;
                                                    tpj(document).ready(function () {
                                                        if (tpj("#rev_slider_4_1").revolution == undefined) {
                                                            revslider_showDoubleJqueryError("#rev_slider_4_1");
                                                        } else {
                                                            revapi4 = tpj("#rev_slider_4_1").show().revolution({
                                                                sliderType: "standard",
                                                                jsFileLocation: "//educationwp.thimpress.com/wp-content/plugins/revslider/public/assets/js/",
                                                                sliderLayout: "fullscreen",
                                                                dottedOverlay: "none",
                                                                delay: 9000,
                                                                navigation: {
                                                                    keyboardNavigation: "off",
                                                                    keyboard_direction: "horizontal",
                                                                    mouseScrollNavigation: "off",
                                                                    mouseScrollReverse: "default",
                                                                    onHoverStop: "off",
                                                                    arrows: {
                                                                        style: "zeus",
                                                                        enable: true,
                                                                        hide_onmobile: true,
                                                                        hide_under: 1024,
                                                                        hide_onleave: true,
                                                                        hide_delay: 200,
                                                                        hide_delay_mobile: 1200,
                                                                        tmp: '<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div> </div>',
                                                                        left: {
                                                                            h_align: "left",
                                                                            v_align: "center",
                                                                            h_offset: 20,
                                                                            v_offset: 0
                                                                        },
                                                                        right: {
                                                                            h_align: "right",
                                                                            v_align: "center",
                                                                            h_offset: 20,
                                                                            v_offset: 0
                                                                        }
                                                                    }
                                                                },
                                                                responsiveLevels: [1240, 1024, 778, 480],
                                                                visibilityLevels: [1240, 1024, 778, 480],
                                                                gridwidth: [1200, 960, 768, 481],
                                                                gridheight: [500, 400, 400, 320],
                                                                lazyType: "single",
                                                                shadow: 0,
                                                                spinner: "spinner2",
                                                                stopLoop: "off",
                                                                stopAfterLoops: -1,
                                                                stopAtSlide: -1,
                                                                shuffle: "off",
                                                                autoHeight: "off",
                                                                fullScreenAutoWidth: "on",
                                                                fullScreenAlignForce: "off",
                                                                fullScreenOffsetContainer: "",
                                                                fullScreenOffset: "",
                                                                disableProgressBar: "on",
                                                                hideThumbsOnMobile: "off",
                                                                hideSliderAtLimit: 0,
                                                                hideCaptionAtLimit: 0,
                                                                hideAllCaptionAtLilmit: 0,
                                                                debugMode: false,
                                                                fallbacks: {
                                                                    simplifyAll: "off",
                                                                    nextSlideOnWindowFocus: "off",
                                                                    disableFocusListener: false,
                                                                    panZoomDisableOnMobile: "on",
                                                                }
                                                            });
                                                        }
                                                    }); /*ready*/
                                                </script>
                                                <script>
                                                    var htmlDivCss = ' #rev_slider_4_1_wrapper .tp-loader.spinner2{ background-color: #FFFFFF !important; } ';
                                                    var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                                    if (htmlDiv) {
                                                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                    } else {
                                                        var htmlDiv = document.createElement('div');
                                                        htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                                        document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                                    }
                                                </script>
                                                <script>
                                                    var htmlDivCss = unescape("%23rev_slider_4_1%20.zeus.tparrows%20%7B%0A%20%20cursor%3Apointer%3B%0A%20%20min-width%3A70px%3B%0A%20%20min-height%3A70px%3B%0A%20%20position%3Aabsolute%3B%0A%20%20display%3Ablock%3B%0A%20%20z-index%3A100%3B%0A%20%20border-radius%3A50%25%3B%20%20%20%0A%20%20overflow%3Ahidden%3B%0A%20%20background%3Argba%280%2C0%2C0%2C0.1%29%3B%0A%7D%0A%0A%23rev_slider_4_1%20.zeus.tparrows%3Abefore%20%7B%0A%20%20font-family%3A%20%22revicons%22%3B%0A%20%20font-size%3A20px%3B%0A%20%20color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%20%20display%3Ablock%3B%0A%20%20line-height%3A%2070px%3B%0A%20%20text-align%3A%20center%3B%20%20%20%20%0A%20%20z-index%3A2%3B%0A%20%20position%3Arelative%3B%0A%7D%0A%23rev_slider_4_1%20.zeus.tparrows.tp-leftarrow%3Abefore%20%7B%0A%20%20content%3A%20%22%5Ce824%22%3B%0A%7D%0A%23rev_slider_4_1%20.zeus.tparrows.tp-rightarrow%3Abefore%20%7B%0A%20%20content%3A%20%22%5Ce825%22%3B%0A%7D%0A%0A%23rev_slider_4_1%20.zeus%20.tp-title-wrap%20%7B%0A%20%20background%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%20%20width%3A100%25%3B%0A%20%20height%3A100%25%3B%0A%20%20top%3A0px%3B%0A%20%20left%3A0px%3B%0A%20%20position%3Aabsolute%3B%0A%20%20opacity%3A0%3B%0A%20%20transform%3Ascale%280%29%3B%0A%20%20-webkit-transform%3Ascale%280%29%3B%0A%20%20%20transition%3A%20all%200.3s%3B%0A%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%20-moz-transition%3Aall%200.3s%3B%0A%20%20%20border-radius%3A50%25%3B%0A%20%7D%0A%23rev_slider_4_1%20.zeus%20.tp-arr-imgholder%20%7B%0A%20%20width%3A100%25%3B%0A%20%20height%3A100%25%3B%0A%20%20position%3Aabsolute%3B%0A%20%20top%3A0px%3B%0A%20%20left%3A0px%3B%0A%20%20background-position%3Acenter%20center%3B%0A%20%20background-size%3Acover%3B%0A%20%20border-radius%3A50%25%3B%0A%20%20transform%3Atranslatex%28-100%25%29%3B%0A%20%20-webkit-transform%3Atranslatex%28-100%25%29%3B%0A%20%20%20transition%3A%20all%200.3s%3B%0A%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%20-moz-transition%3Aall%200.3s%3B%0A%0A%20%7D%0A%23rev_slider_4_1%20.zeus.tp-rightarrow%20.tp-arr-imgholder%20%7B%0A%20%20%20%20transform%3Atranslatex%28100%25%29%3B%0A%20%20-webkit-transform%3Atranslatex%28100%25%29%3B%0A%20%20%20%20%20%20%7D%0A%23rev_slider_4_1%20.zeus.tparrows%3Ahover%20.tp-arr-imgholder%20%7B%0A%20%20transform%3Atranslatex%280%29%3B%0A%20%20-webkit-transform%3Atranslatex%280%29%3B%0A%20%20opacity%3A1%3B%0A%7D%0A%20%20%20%20%20%20%0A%23rev_slider_4_1%20.zeus.tparrows%3Ahover%20.tp-title-wrap%20%7B%0A%20%20transform%3Ascale%281%29%3B%0A%20%20-webkit-transform%3Ascale%281%29%3B%0A%20%20opacity%3A1%3B%0A%7D%0A%20%0A");
                                                    var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                                    if (htmlDiv) {
                                                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                    } else {
                                                        var htmlDiv = document.createElement('div');
                                                        htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                                        document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                                    }
                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-grid" id="pg-12-1">
                            <div class="panel-row-style-thim-best-industry thim-best-industry panel-row-style">
                                <div class="panel-grid-cell" id="pgc-12-1-0">
                                    <div class="so-panel widget widget_icon-box panel-first-child panel-last-child" id="panel-12-1-0-0" data-index="1">
                                        <div class="thim-widget-icon-box thim-widget-icon-box-base">
                                            <div class="wrapper-box-icon has_custom_image has_read_more text-left overlay " data-text-readmore="#ffb606">
                                                <div class="smicon-box iconbox-left">
                                                    <div class="boxes-icon" style="width: 135px;height: 135px;">
                                                        <span class="inner-icon">
                                                            <span class="icon icon-images">
                                                                <img src="http://educationwp.thimpress.com/wp-content/uploads/2015/10/logo-top-1.png" alt="logo-top-1" title="logo-top-1">
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <div class="content-inner" style="width: calc( 100% - 135px - 15px);">
                                                        <div class="sc-heading article_heading">
                                                            <h3 class="heading__primary">Variety Of Tests</h3>
                                                        </div>
                                                        <a class="smicon-read sc-btn" href="#" style="color: #ffb606;">View More<i class="fa fa-chevron-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-grid-cell" id="pgc-12-1-1">
                                    <div class="so-panel widget widget_icon-box panel-first-child panel-last-child" id="panel-12-1-1-0" data-index="2">
                                        <div class="thim-widget-icon-box thim-widget-icon-box-base">
                                            <div class="wrapper-box-icon has_custom_image has_read_more text-left overlay " data-text-readmore="#ffb606">
                                                <div class="smicon-box iconbox-left">
                                                    <div class="boxes-icon" style="width: 135px;height: 135px;">
                                                        <span class="inner-icon">
                                                          <span class="icon icon-images">
                                                              <img src="http://educationwp.thimpress.com/wp-content/uploads/2015/10/logo-top-2.png" alt="logo-top-2" title="logo-top-2">
                                                          </span>
                                                        </span>
                                                    </div>
                                                    <div class="content-inner" style="width: calc( 100% - 135px - 15px);">
                                                        <div class="sc-heading article_heading">
                                                            <h3 class="heading__primary">360° Analysis Report</h3>
                                                        </div>
                                                        <a class="smicon-read sc-btn" href="#" style="color: #ffb606;">View More<i class="fa fa-chevron-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-grid-cell" id="pgc-12-1-2">
                                    <div class="so-panel widget widget_icon-box panel-first-child panel-last-child" id="panel-12-1-2-0" data-index="3">
                                        <div class="thim-widget-icon-box thim-widget-icon-box-base">
                                            <div class="wrapper-box-icon has_custom_image has_read_more text-left overlay " data-text-readmore="#ffb606">
                                                <div class="smicon-box iconbox-left">
                                                    <div class="boxes-icon" style="width: 135px;height: 135px;">
                                                        <span class="inner-icon">
                                                            <span class="icon icon-images">
                                                                <img src="http://educationwp.thimpress.com/wp-content/uploads/2015/10/logo-top-3.png" alt="logo-top-3" title="logo-top-3">
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <div class="content-inner" style="width: calc( 100% - 135px - 15px);">
                                                        <div class="sc-heading article_heading">
                                                            <h3 class="heading__primary"> Review Your Performance </h3>
                                                        </div>
                                                        <a class="smicon-read sc-btn" href="#" style="color: #ffb606;">View More<i class="fa fa-chevron-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-grid" id="pg-12-2">
                            <div class="panel-grid-cell" id="pgc-12-2-0">
                                <div class="so-panel widget widget_heading panel-first-child" id="panel-12-2-0-0" data-index="4">
                                    <div class="thim-widget-heading thim-widget-heading-base">
                                        <div class="sc_heading text-left">
                                            <h3 class="title">Popular Packages</h3>
                                            <span class="line"></span>
                                        </div>
                                    </div>
                                </div>
                                <ul class="cards ud-courseimpressiontracker inline-blocks-lg tac-force-lg popular-programs" data-tracking-type="mark-as-seen" data-context="home" data-subcontext="discover-popular-topics-and-courses" id="ud_courseimpressiontracker">
                 

    
                                    <!-- <li data-courseid="362328" class="fn-force-lg m10-force-lg">
                                       <a href="" class="fxdc mh290-force-xs">
                                           <span class="course-thumb">
                                               <img src="https://udemy-images.udemy.com/course/480x270/362328_91f3_10.jpg" alt="AWS Certified Solutions Architect - Associate 2016">
                                           </span>
                                   
                                           <span class="card-info fx fxdc">
                                               <span class="title">
                                                   AWS Certified Solutions Architect - Associate 2016
                                               </span>
                                   
                                   
                                               <span class="by fx fs14-xs">
                                                   By Ryan Kroonenburg
                                               </span>
                                   
                                   
                                               <span class="desc">
                                                   Want to pass the AWS Solutions Architect - Associate Exam? Want to become Amazon Web Services Certified? Do this course!
                                               </span>
                                               <span class="card-details fxac dib-lg">
                                   
                                   
                                                   <i class="icon-group stud-icon"></i>
                                                   <span class="stud-count fx fs13 mr15">
                                                       34.1K Enrolled
                                                   </span>
                                   
                                                   <span class="price">
                                                       $40
                                                   </span>
                                               </span>
                                           </span>
                                       </a>
                                   </li>   -->       
                                </ul>
                                <!--<div class="so-panel widget widget_courses panel-last-child" id="panel-12-2-0-1" data-index="5">
                                    <div class="thim-widget-courses thim-widget-courses-base">
                                        <div class="thim-carousel-wrapper thim-course-carousel thim-course-grid" data-visible="4" data-pagination="0" data-navigation="1" data-autoplay="0" id="popular-programs">
                                            
                        
        
                                             <div class="course-item">
                                                <div class="course-thumbnail">
                                                    <a href="http://educationwp.thimpress.com/courses/learnpress-101/"><img src="http://educationwp.thimpress.com/wp-content/uploads/2015/11/course-4-450x450.jpg" alt="Introduction LearnPress &#8211; LMS plugin" title="course-4" width="450" height="450"></a><a class="course-readmore" href="#">Buy Now</a></div>
                                                <div class="thim-course-content">
                                                    <div class="course-author" itemscope itemtype="http://schema.org/Person">
                                                        <img alt="" src='http://educationwp.thimpress.com/wp-content/uploads/gravatar/37e6600c81e3765b84dd2eaa751da782-s40.jpg' class="avatar avatar-40" width="40" height="40" />
                                                        <div class="author-contain">
                                                            <label itemprop="jobTitle">Teacher</label>
                                                            <div class="value" itemprop="name">
                                                                <a href="http://educationwp.thimpress.com/profile/keny">
                                                                    Keny White 
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h2 class="course-title">
                                                    <a href="http://educationwp.thimpress.com/courses/learnpress-101/"> Introduction LearnPress &#8211; LMS plugin</a>
                                                    </h2>
                                                    <div class="course-meta">
                                                        <div class="course-students">
                                                            <label>Students</label>
                                                            <div class="value"><i class="fa fa-group"></i> 313 </div>
                                                        </div>
                                                        <div class="course-comments-count">
                                                            <div class="value"><i class="fa fa-comment"></i>4</div>
                                                        </div>
                                                        <div class="course-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                                            <div class="value free-course" itemprop="price" content="Free">
                                                                Free </div>
                                                            <meta itemprop="priceCurrency" content="&#36;" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->



                                            <!-- <div class="course-item">
                                                <div class="course-thumbnail">
                                                    <a href="http://educationwp.thimpress.com/courses/node/"><img src="http://educationwp.thimpress.com/wp-content/uploads/2015/06/course-1-450x450.jpg" alt="From Zero to Hero with Nodejs" title="course-1" width="450" height="450"></a><a class="course-readmore" href="http://educationwp.thimpress.com/courses/node/">Read More</a></div>
                                                <div class="thim-course-content">
                                                    <div class="course-author" itemscope itemtype="http://schema.org/Person">
                                                        <img alt="" src='http://educationwp.thimpress.com/wp-content/uploads/gravatar/37e6600c81e3765b84dd2eaa751da782-s40.jpg' class="avatar avatar-40" width="40" height="40" />
                                                        <div class="author-contain">
                                                            <label itemprop="jobTitle">Teacher</label>
                                                            <div class="value" itemprop="name">
                                                                <a href="http://educationwp.thimpress.com/profile/keny">
                                                                Keny White </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h2 class="course-title">
                                                        <a href="http://educationwp.thimpress.com/courses/node/"> From Zero to Hero with Nodejs</a>
                                                    </h2>
                                                                                        <div class="course-meta">
                                                                                            <div class="course-students">
                                                                                                <label>Students</label>
                                                                                                <div class="value"><i class="fa fa-group"></i> 252 </div>
                                                                                            </div>
                                                                                            <div class="course-comments-count">
                                                                                                <div class="value"><i class="fa fa-comment"></i>2</div>
                                                                                            </div>
                                                                                            <div class="course-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                                                                                <div class="value free-course" itemprop="price" content="Free">
                                                                                                    Free </div>
                                                                                                <meta itemprop="priceCurrency" content="&#36;" />
                                                                                            </div>
                                                                                        </div>
                                                </div>
                                            </div>
                                            <div class="course-item">
                                                <div class="course-thumbnail">
                                                    <a href="http://educationwp.thimpress.com/courses/python/"><img src="http://educationwp.thimpress.com/wp-content/uploads/2015/12/course-16-450x450.jpg" alt="Learn Python &#8211; Interactive Python Tutorial" title="course-16" width="450" height="450"></a><a class="course-readmore" href="http://educationwp.thimpress.com/courses/python/">Read More</a></div>
                                                <div class="thim-course-content">
                                                    <div class="course-author" itemscope itemtype="http://schema.org/Person">
                                                        <img alt="" src='http://educationwp.thimpress.com/wp-content/uploads/gravatar/3afebebd67ca1626a28e05822bc85301-s40.jpg' class="avatar avatar-40" width="40" height="40" />
                                                        <div class="author-contain">
                                                            <label itemprop="jobTitle">Teacher</label>
                                                            <div class="value" itemprop="name">
                                                                <a href="http://educationwp.thimpress.com/profile/admin">
                                                                    Hinata Hyuga
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h2 class="course-title">
                                                        <a href="http://educationwp.thimpress.com/courses/python/"> Learn Python &#8211; Interactive Python Tutorial</a>
                                                    </h2>
                                                    <div class="course-meta">
                                                        <div class="course-students">
                                                            <label>Students</label>
                                                            <div class="value"><i class="fa fa-group"></i> 51 </div>
                                                        </div>
                                                        <div class="course-comments-count">
                                                            <div class="value"><i class="fa fa-comment"></i>1</div>
                                                        </div>
                                                        <div class="course-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                                            <div class="value " itemprop="price" content="&#036;69.00">
                                                                &#036;69.00 </div>
                                                            <meta itemprop="priceCurrency" content="&#36;" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="course-item">
                                                <div class="course-thumbnail">
                                                    <a href="#">
                                                    <img src="#" alt="Your Complete Guide to Photography" title="course-9" width="450" height="450">
                                                    </a>
                                                    <a class="course-readmore" href="#">Read More</a>
                                                    </div>
                                                <div class="thim-course-content">
                                                    <div class="course-author" itemscope itemtype="http://schema.org/Person">
                                                        <img alt="" src='http://educationwp.thimpress.com/wp-content/uploads/gravatar/3afebebd67ca1626a28e05822bc85301-s40.jpg' class="avatar avatar-40" width="40" height="40" />
                                                        <div class="author-contain">
                                                            <label itemprop="jobTitle">Teacher</label>
                                                            <div class="value" itemprop="name">
                                                                <a href="http://educationwp.thimpress.com/profile/admin">
                                                                Hinata Hyuga 
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h2 class="course-title">
                                                    <a href="http://educationwp.thimpress.com/courses/your-complete-guide-to-photography/"> Your Complete Guide to Photography</a>
                                                    </h2>
                                                    <div class="course-meta">
                                                        <div class="course-students">
                                                            <label>Students</label>
                                                            <div class="value"><i class="fa fa-group"></i> 46 </div>
                                                        </div>
                                                        <div class="course-comments-count">
                                                            <div class="value"><i class="fa fa-comment"></i>1</div>
                                                        </div>
                                                        <div class="course-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                                            <div class="value " itemprop="price" content="&#036;60.00">
                                                                &#036;60.00 </div>
                                                            <meta itemprop="priceCurrency" content="&#36;" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="course-item">
                                                <div class="course-thumbnail">
                                                    <a href="http://educationwp.thimpress.com/courses/learning-jquery-mobile-for-beginners/"><img src="http://educationwp.thimpress.com/wp-content/uploads/2015/06/course-5-450x450.jpg" alt="Learning jQuery Mobile for Beginners" title="course-5" width="450" height="450"></a><a class="course-readmore" href="http://educationwp.thimpress.com/courses/learning-jquery-mobile-for-beginners/">Read More</a></div>
                                                <div class="thim-course-content">
                                                    <div class="course-author" itemscope itemtype="http://schema.org/Person">
                                                        <img alt="" src='http://educationwp.thimpress.com/wp-content/uploads/gravatar/3afebebd67ca1626a28e05822bc85301-s40.jpg' class="avatar avatar-40" width="40" height="40" />
                                                        <div class="author-contain">
                                                            <label itemprop="jobTitle">Teacher</label>
                                                            <div class="value" itemprop="name">
                                                                <a href="http://educationwp.thimpress.com/profile/admin">
                                                                Hinata Hyuga 
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h2 class="course-title">
                                                    <a href="http://educationwp.thimpress.com/courses/learning-jquery-mobile-for-beginners/"> Learning jQuery Mobile for Beginners</a>
                                                    </h2>
                                                    <div class="course-meta">
                                                        <div class="course-students">
                                                            <label>Students</label>
                                                            <div class="value"><i class="fa fa-group"></i> 32 </div>
                                                        </div>
                                                        <div class="course-comments-count">
                                                            <div class="value"><i class="fa fa-comment"></i>1</div>
                                                        </div>
                                                        <div class="course-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                                            <div class="value " itemprop="price" content="&#036;30.00">
                                                                &#036;30.00 </div>
                                                            <meta itemprop="priceCurrency" content="&#36;" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="course-item">
                                                <div class="course-thumbnail">
                                                    <a href="http://educationwp.thimpress.com/courses/become-a-php-master-and-make-money-fast/"><img src="http://educationwp.thimpress.com/wp-content/uploads/2015/06/course-2-450x450.jpg" alt="Become a PHP Master and Make Money Fast" title="course-2" width="450" height="450"></a><a class="course-readmore" href="http://educationwp.thimpress.com/courses/become-a-php-master-and-make-money-fast/">Read More</a>
                                                </div>
                                                <div class="thim-course-content">
                                                    <div class="course-author" itemscope itemtype="http://schema.org/Person">
                                                        <img alt="" src='http://educationwp.thimpress.com/wp-content/uploads/gravatar/37e6600c81e3765b84dd2eaa751da782-s40.jpg' class="avatar avatar-40" width="40" height="40" />
                                                        <div class="author-contain">
                                                            <label itemprop="jobTitle">Teacher</label>
                                                            <div class="value" itemprop="name">
                                                                <a href="http://educationwp.thimpress.com/profile/keny">
                                                                Keny White 
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h2 class="course-title">
                                                        <a href="#"> Become a PHP Master and Make Money Fast</a>
                                                    </h2>
                                                    <div class="course-meta">
                                                        <div class="course-students">
                                                            <label>Students</label>
                                                            <div class="value"><i class="fa fa-group"></i> 30 </div>
                                                        </div>
                                                        <div class="course-comments-count">
                                                            <div class="value"><i class="fa fa-comment"></i>1</div>
                                                        </div>
                                                        <div class="course-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                                            <div class="value " itemprop="price" content="&#036;80.00">
                                                                &#036;80.00 </div>
                                                            <meta itemprop="priceCurrency" content="&#36;" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>-->
                            </div>
                        </div>


                       <!--  <div class="panel-grid" id="pg-12-3">
                           <div class="panel-row-style-thim-bg-overlay siteorigin-panels-stretch thim-bg-overlay panel-row-style" style="min-height: 615px;background-image: url(http://educationwp.thimpress.com/wp-content/uploads/2015/10/bg_register_now.jpg); background-position: center; background-repeat: no-repeat;background-size: cover;" data-stretch-type="full" data-stellar-background-ratio="0.5">
                               <div class="panel-grid-cell" id="pgc-12-3-0">
                                   <div class="so-panel widget widget_text panel-first-child" id="panel-12-3-0-0" data-index="6">
                                       <div class="textwidget">
                                           <div class="thim-get-100s">
                                               <p class="get-100s">Get 100s of online <span class="thim-color">Courses For Free</span></p>
                                               <h2>Register Now</h2>
                                           </div>
                                       </div>
                                   </div>
                                   <div class="so-panel widget widget_countdown-box panel-last-child" id="panel-12-3-0-1" data-index="7">
                                       <div class="thim-widget-countdown-box thim-widget-countdown-box-base">
                                           <div class="text-left color-white" id="coming-soon-counter5721acea0c9db"></div>
                                           <script type="text/javascript">
                                               jQuery(function () {
                                                   jQuery(document).ready(function () {
                                                       jQuery("#coming-soon-counter5721acea0c9db").mbComingsoon({
                                                           expiryDate: new Date(2016, 4, 14, 1),
                                                           localization: {
                                                               days: "days",
                                                               hours: "hours",
                                                               minutes: "minutes",
                                                               seconds: "seconds"
                                                           },
                                                           speed: 100
                                                       });
                                                       setTimeout(function () {
                                                           jQuery(window).resize();
                                                       }, 200);
                                                   });
                                               });
                                           </script>
                                       </div>
                                   </div>
                               </div>
                               <div class="panel-grid-cell" id="pgc-12-3-1">
                                   <div class="so-panel widget widget_text panel-first-child panel-last-child" id="panel-12-3-1-0" data-index="8">
                                       <div class="textwidget">
                                           <div class="thim-register-now-form">
                                               <h3 class="title"><span>Create your free account now and get immediate access to 100s of online courses.</span></h3>
                                               <div role="form" class="wpcf7" id="wpcf7-f85-p12-o1" lang="en-US" dir="ltr">
                                                   <div class="screen-reader-response"></div>
                                                   <form action="/#wpcf7-f85-p12-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                                       <div style="display: none;">
                                                           <input type="hidden" name="_wpcf7" value="85" />
                                                           <input type="hidden" name="_wpcf7_version" value="4.4.1" />
                                                           <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                                           <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f85-p12-o1" />
                                                           <input type="hidden" name="_wpnonce" value="63a39bf727" />
                                                       </div>
                                                       <p><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Your Name *"/></span> </p>
                                                       <p><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email *"/></span> </p>
                                                       <p><span class="wpcf7-form-control-wrap phone"><input type="tel" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Phone *"/></span></p>
                                                       <p><input type="submit" value="Get It Now" class="wpcf7-form-control wpcf7-submit" /></p>
                                                       <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                   </form>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div> -->
                       <!--  <div class="panel-grid" id="pg-12-4">
                           <div class="panel-grid-cell" id="pgc-12-4-0">
                               <div class="so-panel widget widget_heading panel-first-child" id="panel-12-4-0-0" data-index="9">
                                   <div class="thim-widget-heading thim-widget-heading-base">
                                       <div class="sc_heading text-left">
                                           <h3 class="title">Events</h3>
                                           <p class="sub-heading" style="">Upcoming Education Events to feed your brain.</p><span class="line"></span></div>
                                   </div>
                               </div>
                               <div class="so-panel widget widget_list-event panel-last-child" id="panel-12-4-0-1" data-index="10">
                                   <div class="thim-widget-list-event thim-widget-list-event-base">
                                       <div class="thim-list-event"><a class="view-all" href="http://educationwp.thimpress.com/events/">View All</a>
                                           <div class="item-event post-2953 tp_event type-tp_event status-tp-event-happenning has-post-thumbnail hentry">
                                               <div class="time-from">
                                                   <div class="date">
                                                       24 </div>
                                                   <div class="month">
                                                       June </div>
                                               </div>
                                               <div class="image"><img src="http://educationwp.thimpress.com/wp-content/uploads/2015/11/event-4-450x233.jpg" alt="event-4" title="event-4" width="450" height="233"></div>
                                               <div class="event-wrapper">
                                                   <h5 class="title">
                                                   <a href="http://educationwp.thimpress.com/events/build-education-website-using-wordpress/"> Build Education Website Using WordPress</a>
                                                   </h5>
                                                   <div class="meta">
                                                       <div class="time">
                                                           <i class="fa fa-clock-o"></i> 8:00 AM - 5:00 PM </div>
                                                       <div class="location">
                                                           <i class="fa fa-map-marker"></i> Chicago, US </div>
                                                   </div>
                                                   <div class="description">
                                                       <p>Tech you how to build a complete Learning Management System with WordPress and LearnPress.</p>
                                                   </div>
                                               </div>
                                           </div>
                                           
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div> -->
                        <!-- <div class="panel-grid" id="pg-12-5">
                            <div class="panel-row-style-thim-bg-overlay siteorigin-panels-stretch thim-bg-overlay panel-row-style" style="padding-bottom: 85px;background-image: url(http://educationwp.thimpress.com/wp-content/uploads/2015/10/bg_lastest_new.jpg); background-position: center; background-repeat: no-repeat;background-size: cover;" data-stretch-type="full" data-stellar-background-ratio="0.5">
                                <div class="panel-grid-cell" id="pgc-12-5-0">
                                    <div class="so-panel widget widget_heading panel-first-child" id="panel-12-5-0-0" data-index="11">
                                        <div style="padding: 30px 0px 0px;" class="panel-widget-style">
                                            <div class="thim-widget-heading thim-widget-heading-base">
                                                <div class="sc_heading text-left">
                                                    <h3 style="color:#ffffff;" class="title">Latest News</h3>
                                                    <p class="sub-heading" style="color:#ffffff;">Education news all over the world.</p><span style="background-color:#ffffff" class="line"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="so-panel widget widget_carousel-post panel-last-child" id="panel-12-5-0-1" data-index="12">
                                        <div class="thim-widget-carousel-post thim-widget-carousel-post-base">
                                            <div class="thim-owl-carousel-post thim-carousel-wrapper" data-visible="3" data-pagination="0" data-navigation="1" data-autoplay="0">
                                                <div class="item">
                                                    <div class="image">
                                                        <a href="http://educationwp.thimpress.com/online-learning-glossary/">
                                                            <img src="http://educationwp.thimpress.com/wp-content/uploads/2016/01/blog-8-450x267.jpg" alt="Online Learning Glossary" title="blog-8" width="450" height="267"> </a>
                                                    </div>
                                                    <div class="content">
                                                        <div class="info">
                                                            <div class="author">
                                                                <span>Anthony</span> 
                                                            </div>
                                                            <div class="date">
                                                                Jan 20&sbquo; 2016 
                                                            </div>
                                                        </div>
                                                        <h4 class="title">
                                                            <a href="http://educationwp.thimpress.com/online-learning-glossary/">Online Learning Glossary</a>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="image">
                                                        <a href="http://educationwp.thimpress.com/tips-to-succeed-in-an-online-course/">
                                                            <img src="http://educationwp.thimpress.com/wp-content/uploads/2016/01/blog-5-450x267.jpg" alt="Tips to Succeed in an Online Course" title="blog-5" width="450" height="267"> </a>
                                                    </div>
                                                    <div class="content">
                                                        <div class="info">
                                                            <div class="author">
                                                                <span>Anthony</span> </div>
                                                            <div class="date">
                                                                Jan 20&sbquo; 2016 </div>
                                                        </div>
                                                        <h4 class="title">
                                                            <a href="http://educationwp.thimpress.com/tips-to-succeed-in-an-online-course/">Tips to Succeed in an Online Course</a>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="image">
                                                        <a href="http://educationwp.thimpress.com/introducing-dr-deniz-zeynep-2/">
                                                            <img src="http://educationwp.thimpress.com/wp-content/uploads/2015/10/blog-3-450x267.jpg" alt="Introducing: Dr. Deniz Zeynep" title="blog-3" width="450" height="267"> </a>
                                                    </div>
                                                    <div class="content">
                                                        <div class="info">
                                                            <div class="author">
                                                                <span>Hinata Hyuga</span> </div>
                                                            <div class="date">
                                                                Oct 20&sbquo; 2015 </div>
                                                        </div>
                                                        <h4 class="title">
                                                                        <a href="http://educationwp.thimpress.com/introducing-dr-deniz-zeynep-2/">Introducing: Dr. Deniz Zeynep</a>
                                                                    </h4>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="image">
                                                        <a href="http://educationwp.thimpress.com/lms-wordpress-plugin/">
                                                            <img src="http://educationwp.thimpress.com/wp-content/uploads/2015/10/blog-2-450x267.jpg" alt="LMS WordPress plugin" title="blog-2" width="450" height="267"> </a>
                                                    </div>
                                                    <div class="content">
                                                        <div class="info">
                                                            <div class="author">
                                                                <span>Hinata Hyuga</span> </div>
                                                            <div class="date">
                                                                Oct 20&sbquo; 2015 </div>
                                                        </div>
                                                        <h4 class="title">
                                                                                    <a href="http://educationwp.thimpress.com/lms-wordpress-plugin/">LMS WordPress plugin</a>
                                                                                </h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-grid" id="pg-12-6">
                            <div class="panel-grid-cell" id="pgc-12-6-0">
                                <div class="so-panel widget widget_heading panel-first-child" id="panel-12-6-0-0" data-index="13">
                                    <div class="thim-widget-heading thim-widget-heading-base">
                                        <div class="sc_heading text-center">
                                            <h3 class="title">What People Say</h3>
                                            <p class="sub-heading" style="">How real people said about Education WordPress Theme.</p><span class="line"></span></div>
                                    </div>
                                </div>
                                <div class="so-panel widget widget_testimonials panel-last-child" id="panel-12-6-0-1" data-index="14">
                                    <div class="thim-widget-testimonials thim-widget-testimonials-base">
                                        <div class="thim-testimonial-slider" data-visible="5" data-auto="0" data-mousewheel="0">
                                            <div class="item">
                                                <div class="image"><img src="http://educationwp.thimpress.com/wp-content/uploads/2015/11/peter-100x100.jpg" alt="peter" title="peter" width="100" height="100"></div>
                                                <div class="content">
                                                    <h3 class="title">Peter Packer</h3>
                                                    <div class="regency">Front-end Developer</div>
                                                    <div class="description">“ LearnPress WordPress LMS Plugin designed with flexible & scalable eLearning system in mind. This WordPress eLearning Plugin comes up with 10+ addons (and counting) to extend the ability of this WordPress Learning Management System. This is incredible. ”</div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="image"><img src="http://educationwp.thimpress.com/wp-content/uploads/2015/11/manuel-100x100.jpg" alt="manuel" title="manuel" width="100" height="100"></div>
                                                <div class="content">
                                                    <h3 class="title">Manuel</h3>
                                                    <div class="regency">Designer</div>
                                                    <div class="description">“ LearnPress is a comprehensive LMS solution for WordPress. This WordPress LMS Plugin can be used to easily create & sell courses online. Each course curriculum can be made with lessons & quizzes which can be managed with easy-to-use user interface, it never gets easier with LearnPress. ”</div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="image"><img src="http://educationwp.thimpress.com/wp-content/uploads/2015/11/john-doe-100x100.jpg" alt="john-doe" title="john-doe" width="100" height="100"></div>
                                                <div class="content">
                                                    <h3 class="title">John Doe</h3>
                                                    <div class="regency">Art director</div>
                                                    <div class="description">“ LearnPress is a WordPress complete solution for creating a Learning Management System (LMS). It can help me to create courses, lessons and quizzes and manage them as easy as I want. I've learned a lot, and I highly recommend it. Thank you. ”</div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="image"><img src="http://educationwp.thimpress.com/wp-content/uploads/2015/11/elsie-100x100.jpg" alt="elsie" title="elsie" width="100" height="100"></div>
                                                <div class="content">
                                                    <h3 class="title">Elsie</h3>
                                                    <div class="regency">Copyrighter</div>
                                                    <div class="description">“ You don't need a whole ecommerce system to sell your online courses. Paypal, Stripe payment methods integration can help you sell your courses out of the box. In the case you wanna use WooCommerce, this awesome WordPress LMS Plugin will serve you well too. ”</div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="image"><img src="http://educationwp.thimpress.com/wp-content/uploads/2015/11/anthony-100x100.jpg" alt="anthony" title="anthony" width="100" height="100"></div>
                                                <div class="content">
                                                    <h3 class="title">Anthony</h3>
                                                    <div class="regency">CEO at Thimpress</div>
                                                    <div class="description">“ Education WP Theme is a comprehensive LMS solution for WordPress Theme. This beautiful theme based on LearnPress - the best WordPress LMS plugin. Education WP theme will bring you the best LMS experience ever with super friendly UX and complete eLearning features. ”</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                      <!--   <div class="panel-grid" id="pg-12-7">
                            <div class="panel-grid-cell" id="pgc-12-7-0">
                                <div class="so-panel widget widget_text panel-first-child panel-last-child" id="panel-12-7-0-0" data-index="15">
                                    <div class="textwidget">
                                        <div class="thim-newlleter-homepage">
                                            <p class="description">Subscribe now and receive weekly newsletter with educational materials, new courses, interesting posts, popular books and much more!</p>
                                            <script type="text/javascript">
                                                (function () {
                                                    if (!window.mc4wp) {
                                                        window.mc4wp = {
                                                            listeners: [],
                                                            forms: {
                                                                on: function (event, callback) {
                                                                    window.mc4wp.listeners.push({
                                                                        event: event,
                                                                        callback: callback
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    }
                                                })();
                                            </script>
                                            <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-3101 mc4wp-form-basic" method="post" data-id="3101" data-name="Default sign-up form">
                                                <div class="mc4wp-form-fields"><input type="email" id="mc4wp_email" name="EMAIL" placeholder="Your email here" required /><input type="submit" value="Subscribe" />
                                                    <div style="display: none;"><input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off" /></div><input type="hidden" name="_mc4wp_timestamp" value="1461824751" /><input type="hidden" name="_mc4wp_form_id" value="3101" /><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1" /></div>
                                                <div class="mc4wp-response"></div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
                <?php @include 'new-html/footer.php';?>

            <!-- <div class="footer-bottom">
                <div class="container">
                    <aside id="siteorigin-panels-builder-7" class="widget widget_siteorigin-panels-builder footer_bottom_widget">
                        <div id="pl-w56a872ba25776">
                            <style scoped>
                                #pg-w56a872ba25776-0,
                                #pl-w56a872ba25776 .panel-grid-cell .so-panel:last-child {
                                    margin-bottom: 0px
                                }
                                
                                #pl-w56a872ba25776 .panel-grid-cell .so-panel {
                                    margin-bottom: 30px
                                }
                                
                                @media (max-width:767px) {
                                    #pg-w56a872ba25776-0 .panel-grid-cell {
                                        float: none;
                                        width: auto
                                    }
                                    #pl-w56a872ba25776 .panel-grid {
                                        margin-left: 0;
                                        margin-right: 0
                                    }
                                    #pl-w56a872ba25776 .panel-grid-cell {
                                        padding: 0
                                    }
                                }
                            </style>
                            <div class="panel-grid" id="pg-w56a872ba25776-0">
                                <div class="panel-row-style-thim-bg-overlay-color-half siteorigin-panels-stretch thim-bg-overlay-color-half panel-row-style" style="min-height: 450px;background-image: url(http://educationwp.thimpress.com/wp-content/uploads/2015/12/bg-footer.jpg);background-size: cover;" data-stretch-type="full">
                                    <div class="panel-grid-cell" id="pgc-w56a872ba25776-0-0">
                                        <div class="so-panel widget widget_heading panel-first-child" id="panel-w56a872ba25776-0-0-0" data-index="0">
                                            <div class="thim-widget-heading thim-widget-heading-base">
                                                <div class="sc_heading text-center">
                                                    <h3 style="color:#333333;" class="title">Become an instructor?</h3>
                                                    <p class="sub-heading" style="color:#333333;">Join thousand of instructors and earn money hassle free!</p><span class="line"></span></div>
                                            </div>
                                        </div>
                                        <div class="so-panel widget widget_button panel-last-child" id="panel-w56a872ba25776-0-0-1" data-index="1">
                                            <div class="thim-widget-button thim-widget-button-base"><a class="widget-button  normal" href="#" style="" data-hover="">Get Started Now</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
            </div> -->
        </div>
        <a href="#" id="back-to-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

    <script type="text/javascript">
        jQuery(function ($) {


            ;
            (function () {
                var $form = $('#learn-press-checkout');
                $form.on('learn_press_checkout_place_order', function () {
                    var chosen = $('input[type="radio"]:checked', $form);
                    $form.find('input[name="woocommerce_chosen_method"]').remove();
                    if (chosen.val() == 'woocommerce') {
                        $form.append('<input type="hidden" name="woocommerce_chosen_method" value="' + chosen.data('method') + '"/>');
                    }
                });
            })();


        });
    </script>
    <!-- <div class="gallery-slider-content"></div> -->
    <!-- <div id="tp_style_selector">
        <div class="tp_style_selector_container">
            <div class="tp_chameleon_customize">
                <div class="tp-buy-theme">
                    <a href="http://themeforest.net/item/education-wordpress-theme-education-wp/14058034?utm_source=edumademo&#038;utm_medium=switcher1&#038;ref=thimpress&#038;utm_campaign=eduma" title="Buy Now">Buy Now!</a>
                </div>
                <div class="box-title">Primary Color</div>
                <div class="predefined primary_color">
                    <a href="#" style="background-color: #e14938" data-name="color1" data-color="e14938"></a>
                    <a href="#" style="background-color: #9db668" data-name="color2" data-color="9db668"></a>
                    <a href="#" style="background-color: #ffb606" data-name="color3" data-color="ffb606"></a>
                    <a href="#" style="background-color: #67b7e1" data-name="color4" data-color="67b7e1"></a>
                    <a href="#" style="background-color: #d870ad" data-name="color5" data-color="d870ad"></a>
                    <a href="#" style="background-color: #00a885" data-name="color6" data-color="00a885"></a>
                </div>
                <div class="box-title">Body Font</div>
                <div class="input-box">
                    <div class="input">
                        <select name="body_font">
                                            <option value="ABeeZee" data-id="ABeeZee">ABeeZee</option>
                                            <option value="Abel" data-id="Abel">Abel</option>
                                            <option value="Abril Fatface" data-id="Abril_Fatface">Abril Fatface</option>
                                            <option value="Aclonica" data-id="Aclonica">Aclonica</option>
                                            <option value="Acme" data-id="Acme">Acme</option>
                                            <option value="Actor" data-id="Actor">Actor</option>
                                            <option value="Adamina" data-id="Adamina">Adamina</option>
                                            <option value="Advent Pro" data-id="Advent_Pro">Advent Pro</option>
                                            <option value="Aguafina Script" data-id="Aguafina_Script">Aguafina Script</option>
                                            <option value="Akronim" data-id="Akronim">Akronim</option>
                                            <option value="Aladin" data-id="Aladin">Aladin</option>
                                            <option value="Aldrich" data-id="Aldrich">Aldrich</option>
                                            <option value="Alef" data-id="Alef">Alef</option>
                                            <option value="Alegreya" data-id="Alegreya">Alegreya</option>
                                            <option value="Alegreya SC" data-id="Alegreya_SC">Alegreya SC</option>
                                            <option value="Alegreya Sans" data-id="Alegreya_Sans">Alegreya Sans</option>
                                            <option value="Alegreya Sans SC" data-id="Alegreya_Sans_SC">Alegreya Sans SC</option>
                                            <option value="Alex Brush" data-id="Alex_Brush">Alex Brush</option>
                                            <option value="Alfa Slab One" data-id="Alfa_Slab_One">Alfa Slab One</option>
                                            <option value="Alice" data-id="Alice">Alice</option>
                                            <option value="Alike" data-id="Alike">Alike</option>
                                            <option value="Alike Angular" data-id="Alike_Angular">Alike Angular</option>
                                            <option value="Allan" data-id="Allan">Allan</option>
                                            <option value="Allerta" data-id="Allerta">Allerta</option>
                                            <option value="Allerta Stencil" data-id="Allerta_Stencil">Allerta Stencil</option>
                                            <option value="Allura" data-id="Allura">Allura</option>
                                            <option value="Almendra" data-id="Almendra">Almendra</option>
                                            <option value="Almendra Display" data-id="Almendra_Display">Almendra Display</option>
                                            <option value="Almendra SC" data-id="Almendra_SC">Almendra SC</option>
                                            <option value="Amarante" data-id="Amarante">Amarante</option>
                                            <option value="Amaranth" data-id="Amaranth">Amaranth</option>
                                            <option value="Amatic SC" data-id="Amatic_SC">Amatic SC</option>
                                            <option value="Amethysta" data-id="Amethysta">Amethysta</option>
                                            <option value="Amiri" data-id="Amiri">Amiri</option>
                                            <option value="Amita" data-id="Amita">Amita</option>
                                            <option value="Anaheim" data-id="Anaheim">Anaheim</option>
                                            <option value="Andada" data-id="Andada">Andada</option>
                                            <option value="Andika" data-id="Andika">Andika</option>
                                            <option value="Angkor" data-id="Angkor">Angkor</option>
                                            <option value="Annie Use Your Telescope" data-id="Annie_Use_Your_Telescope">Annie Use Your Telescope</option>
                                            <option value="Anonymous Pro" data-id="Anonymous_Pro">Anonymous Pro</option>
                                            <option value="Antic" data-id="Antic">Antic</option>
                                            <option value="Antic Didone" data-id="Antic_Didone">Antic Didone</option>
                                            <option value="Antic Slab" data-id="Antic_Slab">Antic Slab</option>
                                            <option value="Anton" data-id="Anton">Anton</option>
                                            <option value="Arapey" data-id="Arapey">Arapey</option>
                                            <option value="Arbutus" data-id="Arbutus">Arbutus</option>
                                            <option value="Arbutus Slab" data-id="Arbutus_Slab">Arbutus Slab</option>
                                            <option value="Architects Daughter" data-id="Architects_Daughter">Architects Daughter</option>
                                            <option value="Archivo Black" data-id="Archivo_Black">Archivo Black</option>
                                            <option value="Archivo Narrow" data-id="Archivo_Narrow">Archivo Narrow</option>
                                            <option value="Arimo" data-id="Arimo">Arimo</option>
                                            <option value="Arizonia" data-id="Arizonia">Arizonia</option>
                                            <option value="Armata" data-id="Armata">Armata</option>
                                            <option value="Artifika" data-id="Artifika">Artifika</option>
                                            <option value="Arvo" data-id="Arvo">Arvo</option>
                                            <option value="Arya" data-id="Arya">Arya</option>
                                            <option value="Asap" data-id="Asap">Asap</option>
                                            <option value="Asar" data-id="Asar">Asar</option>
                                            <option value="Asset" data-id="Asset">Asset</option>
                                            <option value="Astloch" data-id="Astloch">Astloch</option>
                                            <option value="Asul" data-id="Asul">Asul</option>
                                            <option value="Atomic Age" data-id="Atomic_Age">Atomic Age</option>
                                            <option value="Aubrey" data-id="Aubrey">Aubrey</option>
                                            <option value="Audiowide" data-id="Audiowide">Audiowide</option>
                                            <option value="Autour One" data-id="Autour_One">Autour One</option>
                                            <option value="Average" data-id="Average">Average</option>
                                            <option value="Average Sans" data-id="Average_Sans">Average Sans</option>
                                            <option value="Averia Gruesa Libre" data-id="Averia_Gruesa_Libre">Averia Gruesa Libre</option>
                                            <option value="Averia Libre" data-id="Averia_Libre">Averia Libre</option>
                                            <option value="Averia Sans Libre" data-id="Averia_Sans_Libre">Averia Sans Libre</option>
                                            <option value="Averia Serif Libre" data-id="Averia_Serif_Libre">Averia Serif Libre</option>
                                            <option value="Bad Script" data-id="Bad_Script">Bad Script</option>
                                            <option value="Balthazar" data-id="Balthazar">Balthazar</option>
                                            <option value="Bangers" data-id="Bangers">Bangers</option>
                                            <option value="Basic" data-id="Basic">Basic</option>
                                            <option value="Battambang" data-id="Battambang">Battambang</option>
                                            <option value="Baumans" data-id="Baumans">Baumans</option>
                                            <option value="Bayon" data-id="Bayon">Bayon</option>
                                            <option value="Belgrano" data-id="Belgrano">Belgrano</option>
                                            <option value="Belleza" data-id="Belleza">Belleza</option>
                                            <option value="BenchNine" data-id="BenchNine">BenchNine</option>
                                            <option value="Bentham" data-id="Bentham">Bentham</option>
                                            <option value="Berkshire Swash" data-id="Berkshire_Swash">Berkshire Swash</option>
                                            <option value="Bevan" data-id="Bevan">Bevan</option>
                                            <option value="Bigelow Rules" data-id="Bigelow_Rules">Bigelow Rules</option>
                                            <option value="Bigshot One" data-id="Bigshot_One">Bigshot One</option>
                                            <option value="Bilbo" data-id="Bilbo">Bilbo</option>
                                            <option value="Bilbo Swash Caps" data-id="Bilbo_Swash_Caps">Bilbo Swash Caps</option>
                                            <option value="Biryani" data-id="Biryani">Biryani</option>
                                            <option value="Bitter" data-id="Bitter">Bitter</option>
                                            <option value="Black Ops One" data-id="Black_Ops_One">Black Ops One</option>
                                            <option value="Bokor" data-id="Bokor">Bokor</option>
                                            <option value="Bonbon" data-id="Bonbon">Bonbon</option>
                                            <option value="Boogaloo" data-id="Boogaloo">Boogaloo</option>
                                            <option value="Bowlby One" data-id="Bowlby_One">Bowlby One</option>
                                            <option value="Bowlby One SC" data-id="Bowlby_One_SC">Bowlby One SC</option>
                                            <option value="Brawler" data-id="Brawler">Brawler</option>
                                            <option value="Bree Serif" data-id="Bree_Serif">Bree Serif</option>
                                            <option value="Bubblegum Sans" data-id="Bubblegum_Sans">Bubblegum Sans</option>
                                            <option value="Bubbler One" data-id="Bubbler_One">Bubbler One</option>
                                            <option value="Buda" data-id="Buda">Buda</option>
                                            <option value="Buenard" data-id="Buenard">Buenard</option>
                                            <option value="Butcherman" data-id="Butcherman">Butcherman</option>
                                            <option value="Butterfly Kids" data-id="Butterfly_Kids">Butterfly Kids</option>
                                            <option value="Cabin" data-id="Cabin">Cabin</option>
                                            <option value="Cabin Condensed" data-id="Cabin_Condensed">Cabin Condensed</option>
                                            <option value="Cabin Sketch" data-id="Cabin_Sketch">Cabin Sketch</option>
                                            <option value="Caesar Dressing" data-id="Caesar_Dressing">Caesar Dressing</option>
                                            <option value="Cagliostro" data-id="Cagliostro">Cagliostro</option>
                                            <option value="Calligraffitti" data-id="Calligraffitti">Calligraffitti</option>
                                            <option value="Cambay" data-id="Cambay">Cambay</option>
                                            <option value="Cambo" data-id="Cambo">Cambo</option>
                                            <option value="Candal" data-id="Candal">Candal</option>
                                            <option value="Cantarell" data-id="Cantarell">Cantarell</option>
                                            <option value="Cantata One" data-id="Cantata_One">Cantata One</option>
                                            <option value="Cantora One" data-id="Cantora_One">Cantora One</option>
                                            <option value="Capriola" data-id="Capriola">Capriola</option>
                                            <option value="Cardo" data-id="Cardo">Cardo</option>
                                            <option value="Carme" data-id="Carme">Carme</option>
                                            <option value="Carrois Gothic" data-id="Carrois_Gothic">Carrois Gothic</option>
                                            <option value="Carrois Gothic SC" data-id="Carrois_Gothic_SC">Carrois Gothic SC</option>
                                            <option value="Carter One" data-id="Carter_One">Carter One</option>
                                            <option value="Catamaran" data-id="Catamaran">Catamaran</option>
                                            <option value="Caudex" data-id="Caudex">Caudex</option>
                                            <option value="Cedarville Cursive" data-id="Cedarville_Cursive">Cedarville Cursive</option>
                                            <option value="Ceviche One" data-id="Ceviche_One">Ceviche One</option>
                                            <option value="Changa One" data-id="Changa_One">Changa One</option>
                                            <option value="Chango" data-id="Chango">Chango</option>
                                            <option value="Chau Philomene One" data-id="Chau_Philomene_One">Chau Philomene One</option>
                                            <option value="Chela One" data-id="Chela_One">Chela One</option>
                                            <option value="Chelsea Market" data-id="Chelsea_Market">Chelsea Market</option>
                                            <option value="Chenla" data-id="Chenla">Chenla</option>
                                            <option value="Cherry Cream Soda" data-id="Cherry_Cream_Soda">Cherry Cream Soda</option>
                                            <option value="Cherry Swash" data-id="Cherry_Swash">Cherry Swash</option>
                                            <option value="Chewy" data-id="Chewy">Chewy</option>
                                            <option value="Chicle" data-id="Chicle">Chicle</option>
                                            <option value="Chivo" data-id="Chivo">Chivo</option>
                                            <option value="Chonburi" data-id="Chonburi">Chonburi</option>
                                            <option value="Cinzel" data-id="Cinzel">Cinzel</option>
                                            <option value="Cinzel Decorative" data-id="Cinzel_Decorative">Cinzel Decorative</option>
                                            <option value="Clicker Script" data-id="Clicker_Script">Clicker Script</option>
                                            <option value="Coda" data-id="Coda">Coda</option>
                                            <option value="Coda Caption" data-id="Coda_Caption">Coda Caption</option>
                                            <option value="Codystar" data-id="Codystar">Codystar</option>
                                            <option value="Combo" data-id="Combo">Combo</option>
                                            <option value="Comfortaa" data-id="Comfortaa">Comfortaa</option>
                                            <option value="Coming Soon" data-id="Coming_Soon">Coming Soon</option>
                                            <option value="Concert One" data-id="Concert_One">Concert One</option>
                                            <option value="Condiment" data-id="Condiment">Condiment</option>
                                            <option value="Content" data-id="Content">Content</option>
                                            <option value="Contrail One" data-id="Contrail_One">Contrail One</option>
                                            <option value="Convergence" data-id="Convergence">Convergence</option>
                                            <option value="Cookie" data-id="Cookie">Cookie</option>
                                            <option value="Copse" data-id="Copse">Copse</option>
                                            <option value="Corben" data-id="Corben">Corben</option>
                                            <option value="Courgette" data-id="Courgette">Courgette</option>
                                            <option value="Cousine" data-id="Cousine">Cousine</option>
                                            <option value="Coustard" data-id="Coustard">Coustard</option>
                                            <option value="Covered By Your Grace" data-id="Covered_By_Your_Grace">Covered By Your Grace</option>
                                            <option value="Crafty Girls" data-id="Crafty_Girls">Crafty Girls</option>
                                            <option value="Creepster" data-id="Creepster">Creepster</option>
                                            <option value="Crete Round" data-id="Crete_Round">Crete Round</option>
                                            <option value="Crimson Text" data-id="Crimson_Text">Crimson Text</option>
                                            <option value="Croissant One" data-id="Croissant_One">Croissant One</option>
                                            <option value="Crushed" data-id="Crushed">Crushed</option>
                                            <option value="Cuprum" data-id="Cuprum">Cuprum</option>
                                            <option value="Cutive" data-id="Cutive">Cutive</option>
                                            <option value="Cutive Mono" data-id="Cutive_Mono">Cutive Mono</option>
                                            <option value="Damion" data-id="Damion">Damion</option>
                                            <option value="Dancing Script" data-id="Dancing_Script">Dancing Script</option>
                                            <option value="Dangrek" data-id="Dangrek">Dangrek</option>
                                            <option value="Dawning of a New Day" data-id="Dawning_of_a_New_Day">Dawning of a New Day</option>
                                            <option value="Days One" data-id="Days_One">Days One</option>
                                            <option value="Dekko" data-id="Dekko">Dekko</option>
                                            <option value="Delius" data-id="Delius">Delius</option>
                                            <option value="Delius Swash Caps" data-id="Delius_Swash_Caps">Delius Swash Caps</option>
                                            <option value="Delius Unicase" data-id="Delius_Unicase">Delius Unicase</option>
                                            <option value="Della Respira" data-id="Della_Respira">Della Respira</option>
                                            <option value="Denk One" data-id="Denk_One">Denk One</option>
                                            <option value="Devonshire" data-id="Devonshire">Devonshire</option>
                                            <option value="Dhurjati" data-id="Dhurjati">Dhurjati</option>
                                            <option value="Didact Gothic" data-id="Didact_Gothic">Didact Gothic</option>
                                            <option value="Diplomata" data-id="Diplomata">Diplomata</option>
                                            <option value="Diplomata SC" data-id="Diplomata_SC">Diplomata SC</option>
                                            <option value="Domine" data-id="Domine">Domine</option>
                                            <option value="Donegal One" data-id="Donegal_One">Donegal One</option>
                                            <option value="Doppio One" data-id="Doppio_One">Doppio One</option>
                                            <option value="Dorsa" data-id="Dorsa">Dorsa</option>
                                            <option value="Dosis" data-id="Dosis">Dosis</option>
                                            <option value="Dr Sugiyama" data-id="Dr_Sugiyama">Dr Sugiyama</option>
                                            <option value="Droid Sans" data-id="Droid_Sans">Droid Sans</option>
                                            <option value="Droid Sans Mono" data-id="Droid_Sans_Mono">Droid Sans Mono</option>
                                            <option value="Droid Serif" data-id="Droid_Serif">Droid Serif</option>
                                            <option value="Duru Sans" data-id="Duru_Sans">Duru Sans</option>
                                            <option value="Dynalight" data-id="Dynalight">Dynalight</option>
                                            <option value="EB Garamond" data-id="EB_Garamond">EB Garamond</option>
                                            <option value="Eagle Lake" data-id="Eagle_Lake">Eagle Lake</option>
                                            <option value="Eater" data-id="Eater">Eater</option>
                                            <option value="Economica" data-id="Economica">Economica</option>
                                            <option value="Eczar" data-id="Eczar">Eczar</option>
                                            <option value="Ek Mukta" data-id="Ek_Mukta">Ek Mukta</option>
                                            <option value="Electrolize" data-id="Electrolize">Electrolize</option>
                                            <option value="Elsie" data-id="Elsie">Elsie</option>
                                            <option value="Elsie Swash Caps" data-id="Elsie_Swash_Caps">Elsie Swash Caps</option>
                                            <option value="Emblema One" data-id="Emblema_One">Emblema One</option>
                                            <option value="Emilys Candy" data-id="Emilys_Candy">Emilys Candy</option>
                                            <option value="Engagement" data-id="Engagement">Engagement</option>
                                            <option value="Englebert" data-id="Englebert">Englebert</option>
                                            <option value="Enriqueta" data-id="Enriqueta">Enriqueta</option>
                                            <option value="Erica One" data-id="Erica_One">Erica One</option>
                                            <option value="Esteban" data-id="Esteban">Esteban</option>
                                            <option value="Euphoria Script" data-id="Euphoria_Script">Euphoria Script</option>
                                            <option value="Ewert" data-id="Ewert">Ewert</option>
                                            <option value="Exo" data-id="Exo">Exo</option>
                                            <option value="Exo 2" data-id="Exo_2">Exo 2</option>
                                            <option value="Expletus Sans" data-id="Expletus_Sans">Expletus Sans</option>
                                            <option value="Fanwood Text" data-id="Fanwood_Text">Fanwood Text</option>
                                            <option value="Fascinate" data-id="Fascinate">Fascinate</option>
                                            <option value="Fascinate Inline" data-id="Fascinate_Inline">Fascinate Inline</option>
                                            <option value="Faster One" data-id="Faster_One">Faster One</option>
                                            <option value="Fasthand" data-id="Fasthand">Fasthand</option>
                                            <option value="Fauna One" data-id="Fauna_One">Fauna One</option>
                                            <option value="Federant" data-id="Federant">Federant</option>
                                            <option value="Federo" data-id="Federo">Federo</option>
                                            <option value="Felipa" data-id="Felipa">Felipa</option>
                                            <option value="Fenix" data-id="Fenix">Fenix</option>
                                            <option value="Finger Paint" data-id="Finger_Paint">Finger Paint</option>
                                            <option value="Fira Mono" data-id="Fira_Mono">Fira Mono</option>
                                            <option value="Fira Sans" data-id="Fira_Sans">Fira Sans</option>
                                            <option value="Fjalla One" data-id="Fjalla_One">Fjalla One</option>
                                            <option value="Fjord One" data-id="Fjord_One">Fjord One</option>
                                            <option value="Flamenco" data-id="Flamenco">Flamenco</option>
                                            <option value="Flavors" data-id="Flavors">Flavors</option>
                                            <option value="Fondamento" data-id="Fondamento">Fondamento</option>
                                            <option value="Fontdiner Swanky" data-id="Fontdiner_Swanky">Fontdiner Swanky</option>
                                            <option value="Forum" data-id="Forum">Forum</option>
                                            <option value="Francois One" data-id="Francois_One">Francois One</option>
                                            <option value="Freckle Face" data-id="Freckle_Face">Freckle Face</option>
                                            <option value="Fredericka the Great" data-id="Fredericka_the_Great">Fredericka the Great</option>
                                            <option value="Fredoka One" data-id="Fredoka_One">Fredoka One</option>
                                            <option value="Freehand" data-id="Freehand">Freehand</option>
                                            <option value="Fresca" data-id="Fresca">Fresca</option>
                                            <option value="Frijole" data-id="Frijole">Frijole</option>
                                            <option value="Fruktur" data-id="Fruktur">Fruktur</option>
                                            <option value="Fugaz One" data-id="Fugaz_One">Fugaz One</option>
                                            <option value="GFS Didot" data-id="GFS_Didot">GFS Didot</option>
                                            <option value="GFS Neohellenic" data-id="GFS_Neohellenic">GFS Neohellenic</option>
                                            <option value="Gabriela" data-id="Gabriela">Gabriela</option>
                                            <option value="Gafata" data-id="Gafata">Gafata</option>
                                            <option value="Galdeano" data-id="Galdeano">Galdeano</option>
                                            <option value="Galindo" data-id="Galindo">Galindo</option>
                                            <option value="Gentium Basic" data-id="Gentium_Basic">Gentium Basic</option>
                                            <option value="Gentium Book Basic" data-id="Gentium_Book_Basic">Gentium Book Basic</option>
                                            <option value="Geo" data-id="Geo">Geo</option>
                                            <option value="Geostar" data-id="Geostar">Geostar</option>
                                            <option value="Geostar Fill" data-id="Geostar_Fill">Geostar Fill</option>
                                            <option value="Germania One" data-id="Germania_One">Germania One</option>
                                            <option value="Gidugu" data-id="Gidugu">Gidugu</option>
                                            <option value="Gilda Display" data-id="Gilda_Display">Gilda Display</option>
                                            <option value="Give You Glory" data-id="Give_You_Glory">Give You Glory</option>
                                            <option value="Glass Antiqua" data-id="Glass_Antiqua">Glass Antiqua</option>
                                            <option value="Glegoo" data-id="Glegoo">Glegoo</option>
                                            <option value="Gloria Hallelujah" data-id="Gloria_Hallelujah">Gloria Hallelujah</option>
                                            <option value="Goblin One" data-id="Goblin_One">Goblin One</option>
                                            <option value="Gochi Hand" data-id="Gochi_Hand">Gochi Hand</option>
                                            <option value="Gorditas" data-id="Gorditas">Gorditas</option>
                                            <option value="Goudy Bookletter 1911" data-id="Goudy_Bookletter_1911">Goudy Bookletter 1911</option>
                                            <option value="Graduate" data-id="Graduate">Graduate</option>
                                            <option value="Grand Hotel" data-id="Grand_Hotel">Grand Hotel</option>
                                            <option value="Gravitas One" data-id="Gravitas_One">Gravitas One</option>
                                            <option value="Great Vibes" data-id="Great_Vibes">Great Vibes</option>
                                            <option value="Griffy" data-id="Griffy">Griffy</option>
                                            <option value="Gruppo" data-id="Gruppo">Gruppo</option>
                                            <option value="Gudea" data-id="Gudea">Gudea</option>
                                            <option value="Gurajada" data-id="Gurajada">Gurajada</option>
                                            <option value="Habibi" data-id="Habibi">Habibi</option>
                                            <option value="Halant" data-id="Halant">Halant</option>
                                            <option value="Hammersmith One" data-id="Hammersmith_One">Hammersmith One</option>
                                            <option value="Hanalei" data-id="Hanalei">Hanalei</option>
                                            <option value="Hanalei Fill" data-id="Hanalei_Fill">Hanalei Fill</option>
                                            <option value="Handlee" data-id="Handlee">Handlee</option>
                                            <option value="Hanuman" data-id="Hanuman">Hanuman</option>
                                            <option value="Happy Monkey" data-id="Happy_Monkey">Happy Monkey</option>
                                            <option value="Headland One" data-id="Headland_One">Headland One</option>
                                            <option value="Henny Penny" data-id="Henny_Penny">Henny Penny</option>
                                            <option value="Herr Von Muellerhoff" data-id="Herr_Von_Muellerhoff">Herr Von Muellerhoff</option>
                                            <option value="Hind" data-id="Hind">Hind</option>
                                            <option value="Hind Siliguri" data-id="Hind_Siliguri">Hind Siliguri</option>
                                            <option value="Hind Vadodara" data-id="Hind_Vadodara">Hind Vadodara</option>
                                            <option value="Holtwood One SC" data-id="Holtwood_One_SC">Holtwood One SC</option>
                                            <option value="Homemade Apple" data-id="Homemade_Apple">Homemade Apple</option>
                                            <option value="Homenaje" data-id="Homenaje">Homenaje</option>
                                            <option value="IM Fell DW Pica" data-id="IM_Fell_DW_Pica">IM Fell DW Pica</option>
                                            <option value="IM Fell DW Pica SC" data-id="IM_Fell_DW_Pica_SC">IM Fell DW Pica SC</option>
                                            <option value="IM Fell Double Pica" data-id="IM_Fell_Double_Pica">IM Fell Double Pica</option>
                                            <option value="IM Fell Double Pica SC" data-id="IM_Fell_Double_Pica_SC">IM Fell Double Pica SC</option>
                                            <option value="IM Fell English" data-id="IM_Fell_English">IM Fell English</option>
                                            <option value="IM Fell English SC" data-id="IM_Fell_English_SC">IM Fell English SC</option>
                                            <option value="IM Fell French Canon" data-id="IM_Fell_French_Canon">IM Fell French Canon</option>
                                            <option value="IM Fell French Canon SC" data-id="IM_Fell_French_Canon_SC">IM Fell French Canon SC</option>
                                            <option value="IM Fell Great Primer" data-id="IM_Fell_Great_Primer">IM Fell Great Primer</option>
                                            <option value="IM Fell Great Primer SC" data-id="IM_Fell_Great_Primer_SC">IM Fell Great Primer SC</option>
                                            <option value="Iceberg" data-id="Iceberg">Iceberg</option>
                                            <option value="Iceland" data-id="Iceland">Iceland</option>
                                            <option value="Imprima" data-id="Imprima">Imprima</option>
                                            <option value="Inconsolata" data-id="Inconsolata">Inconsolata</option>
                                            <option value="Inder" data-id="Inder">Inder</option>
                                            <option value="Indie Flower" data-id="Indie_Flower">Indie Flower</option>
                                            <option value="Inika" data-id="Inika">Inika</option>
                                            <option value="Inknut Antiqua" data-id="Inknut_Antiqua">Inknut Antiqua</option>
                                            <option value="Irish Grover" data-id="Irish_Grover">Irish Grover</option>
                                            <option value="Istok Web" data-id="Istok_Web">Istok Web</option>
                                            <option value="Italiana" data-id="Italiana">Italiana</option>
                                            <option value="Italianno" data-id="Italianno">Italianno</option>
                                            <option value="Itim" data-id="Itim">Itim</option>
                                            <option value="Jacques Francois" data-id="Jacques_Francois">Jacques Francois</option>
                                            <option value="Jacques Francois Shadow" data-id="Jacques_Francois_Shadow">Jacques Francois Shadow</option>
                                            <option value="Jaldi" data-id="Jaldi">Jaldi</option>
                                            <option value="Jim Nightshade" data-id="Jim_Nightshade">Jim Nightshade</option>
                                            <option value="Jockey One" data-id="Jockey_One">Jockey One</option>
                                            <option value="Jolly Lodger" data-id="Jolly_Lodger">Jolly Lodger</option>
                                            <option value="Josefin Sans" data-id="Josefin_Sans">Josefin Sans</option>
                                            <option value="Josefin Slab" data-id="Josefin_Slab">Josefin Slab</option>
                                            <option value="Joti One" data-id="Joti_One">Joti One</option>
                                            <option value="Judson" data-id="Judson">Judson</option>
                                            <option value="Julee" data-id="Julee">Julee</option>
                                            <option value="Julius Sans One" data-id="Julius_Sans_One">Julius Sans One</option>
                                            <option value="Junge" data-id="Junge">Junge</option>
                                            <option value="Jura" data-id="Jura">Jura</option>
                                            <option value="Just Another Hand" data-id="Just_Another_Hand">Just Another Hand</option>
                                            <option value="Just Me Again Down Here" data-id="Just_Me_Again_Down_Here">Just Me Again Down Here</option>
                                            <option value="Kadwa" data-id="Kadwa">Kadwa</option>
                                            <option value="Kalam" data-id="Kalam">Kalam</option>
                                            <option value="Kameron" data-id="Kameron">Kameron</option>
                                            <option value="Kantumruy" data-id="Kantumruy">Kantumruy</option>
                                            <option value="Karla" data-id="Karla">Karla</option>
                                            <option value="Karma" data-id="Karma">Karma</option>
                                            <option value="Kaushan Script" data-id="Kaushan_Script">Kaushan Script</option>
                                            <option value="Kavoon" data-id="Kavoon">Kavoon</option>
                                            <option value="Kdam Thmor" data-id="Kdam_Thmor">Kdam Thmor</option>
                                            <option value="Keania One" data-id="Keania_One">Keania One</option>
                                            <option value="Kelly Slab" data-id="Kelly_Slab">Kelly Slab</option>
                                            <option value="Kenia" data-id="Kenia">Kenia</option>
                                            <option value="Khand" data-id="Khand">Khand</option>
                                            <option value="Khmer" data-id="Khmer">Khmer</option>
                                            <option value="Khula" data-id="Khula">Khula</option>
                                            <option value="Kite One" data-id="Kite_One">Kite One</option>
                                            <option value="Knewave" data-id="Knewave">Knewave</option>
                                            <option value="Kotta One" data-id="Kotta_One">Kotta One</option>
                                            <option value="Koulen" data-id="Koulen">Koulen</option>
                                            <option value="Kranky" data-id="Kranky">Kranky</option>
                                            <option value="Kreon" data-id="Kreon">Kreon</option>
                                            <option value="Kristi" data-id="Kristi">Kristi</option>
                                            <option value="Krona One" data-id="Krona_One">Krona One</option>
                                            <option value="Kurale" data-id="Kurale">Kurale</option>
                                            <option value="La Belle Aurore" data-id="La_Belle_Aurore">La Belle Aurore</option>
                                            <option value="Laila" data-id="Laila">Laila</option>
                                            <option value="Lakki Reddy" data-id="Lakki_Reddy">Lakki Reddy</option>
                                            <option value="Lancelot" data-id="Lancelot">Lancelot</option>
                                            <option value="Lateef" data-id="Lateef">Lateef</option>
                                            <option value="Lato" data-id="Lato">Lato</option>
                                            <option value="League Script" data-id="League_Script">League Script</option>
                                            <option value="Leckerli One" data-id="Leckerli_One">Leckerli One</option>
                                            <option value="Ledger" data-id="Ledger">Ledger</option>
                                            <option value="Lekton" data-id="Lekton">Lekton</option>
                                            <option value="Lemon" data-id="Lemon">Lemon</option>
                                            <option value="Libre Baskerville" data-id="Libre_Baskerville">Libre Baskerville</option>
                                            <option value="Life Savers" data-id="Life_Savers">Life Savers</option>
                                            <option value="Lilita One" data-id="Lilita_One">Lilita One</option>
                                            <option value="Lily Script One" data-id="Lily_Script_One">Lily Script One</option>
                                            <option value="Limelight" data-id="Limelight">Limelight</option>
                                            <option value="Linden Hill" data-id="Linden_Hill">Linden Hill</option>
                                            <option value="Lobster" data-id="Lobster">Lobster</option>
                                            <option value="Lobster Two" data-id="Lobster_Two">Lobster Two</option>
                                            <option value="Londrina Outline" data-id="Londrina_Outline">Londrina Outline</option>
                                            <option value="Londrina Shadow" data-id="Londrina_Shadow">Londrina Shadow</option>
                                            <option value="Londrina Sketch" data-id="Londrina_Sketch">Londrina Sketch</option>
                                            <option value="Londrina Solid" data-id="Londrina_Solid">Londrina Solid</option>
                                            <option value="Lora" data-id="Lora">Lora</option>
                                            <option value="Love Ya Like A Sister" data-id="Love_Ya_Like_A_Sister">Love Ya Like A Sister</option>
                                            <option value="Loved by the King" data-id="Loved_by_the_King">Loved by the King</option>
                                            <option value="Lovers Quarrel" data-id="Lovers_Quarrel">Lovers Quarrel</option>
                                            <option value="Luckiest Guy" data-id="Luckiest_Guy">Luckiest Guy</option>
                                            <option value="Lusitana" data-id="Lusitana">Lusitana</option>
                                            <option value="Lustria" data-id="Lustria">Lustria</option>
                                            <option value="Macondo" data-id="Macondo">Macondo</option>
                                            <option value="Macondo Swash Caps" data-id="Macondo_Swash_Caps">Macondo Swash Caps</option>
                                            <option value="Magra" data-id="Magra">Magra</option>
                                            <option value="Maiden Orange" data-id="Maiden_Orange">Maiden Orange</option>
                                            <option value="Mako" data-id="Mako">Mako</option>
                                            <option value="Mallanna" data-id="Mallanna">Mallanna</option>
                                            <option value="Mandali" data-id="Mandali">Mandali</option>
                                            <option value="Marcellus" data-id="Marcellus">Marcellus</option>
                                            <option value="Marcellus SC" data-id="Marcellus_SC">Marcellus SC</option>
                                            <option value="Marck Script" data-id="Marck_Script">Marck Script</option>
                                            <option value="Margarine" data-id="Margarine">Margarine</option>
                                            <option value="Marko One" data-id="Marko_One">Marko One</option>
                                            <option value="Marmelad" data-id="Marmelad">Marmelad</option>
                                            <option value="Martel" data-id="Martel">Martel</option>
                                            <option value="Martel Sans" data-id="Martel_Sans">Martel Sans</option>
                                            <option value="Marvel" data-id="Marvel">Marvel</option>
                                            <option value="Mate" data-id="Mate">Mate</option>
                                            <option value="Mate SC" data-id="Mate_SC">Mate SC</option>
                                            <option value="Maven Pro" data-id="Maven_Pro">Maven Pro</option>
                                            <option value="McLaren" data-id="McLaren">McLaren</option>
                                            <option value="Meddon" data-id="Meddon">Meddon</option>
                                            <option value="MedievalSharp" data-id="MedievalSharp">MedievalSharp</option>
                                            <option value="Medula One" data-id="Medula_One">Medula One</option>
                                            <option value="Megrim" data-id="Megrim">Megrim</option>
                                            <option value="Meie Script" data-id="Meie_Script">Meie Script</option>
                                            <option value="Merienda" data-id="Merienda">Merienda</option>
                                            <option value="Merienda One" data-id="Merienda_One">Merienda One</option>
                                            <option value="Merriweather" data-id="Merriweather">Merriweather</option>
                                            <option value="Merriweather Sans" data-id="Merriweather_Sans">Merriweather Sans</option>
                                            <option value="Metal" data-id="Metal">Metal</option>
                                            <option value="Metal Mania" data-id="Metal_Mania">Metal Mania</option>
                                            <option value="Metamorphous" data-id="Metamorphous">Metamorphous</option>
                                            <option value="Metrophobic" data-id="Metrophobic">Metrophobic</option>
                                            <option value="Michroma" data-id="Michroma">Michroma</option>
                                            <option value="Milonga" data-id="Milonga">Milonga</option>
                                            <option value="Miltonian" data-id="Miltonian">Miltonian</option>
                                            <option value="Miltonian Tattoo" data-id="Miltonian_Tattoo">Miltonian Tattoo</option>
                                            <option value="Miniver" data-id="Miniver">Miniver</option>
                                            <option value="Miss Fajardose" data-id="Miss_Fajardose">Miss Fajardose</option>
                                            <option value="Modak" data-id="Modak">Modak</option>
                                            <option value="Modern Antiqua" data-id="Modern_Antiqua">Modern Antiqua</option>
                                            <option value="Molengo" data-id="Molengo">Molengo</option>
                                            <option value="Molle" data-id="Molle">Molle</option>
                                            <option value="Monda" data-id="Monda">Monda</option>
                                            <option value="Monofett" data-id="Monofett">Monofett</option>
                                            <option value="Monoton" data-id="Monoton">Monoton</option>
                                            <option value="Monsieur La Doulaise" data-id="Monsieur_La_Doulaise">Monsieur La Doulaise</option>
                                            <option value="Montaga" data-id="Montaga">Montaga</option>
                                            <option value="Montez" data-id="Montez">Montez</option>
                                            <option value="Montserrat" data-id="Montserrat">Montserrat</option>
                                            <option value="Montserrat Alternates" data-id="Montserrat_Alternates">Montserrat Alternates</option>
                                            <option value="Montserrat Subrayada" data-id="Montserrat_Subrayada">Montserrat Subrayada</option>
                                            <option value="Moul" data-id="Moul">Moul</option>
                                            <option value="Moulpali" data-id="Moulpali">Moulpali</option>
                                            <option value="Mountains of Christmas" data-id="Mountains_of_Christmas">Mountains of Christmas</option>
                                            <option value="Mouse Memoirs" data-id="Mouse_Memoirs">Mouse Memoirs</option>
                                            <option value="Mr Bedfort" data-id="Mr_Bedfort">Mr Bedfort</option>
                                            <option value="Mr Dafoe" data-id="Mr_Dafoe">Mr Dafoe</option>
                                            <option value="Mr De Haviland" data-id="Mr_De_Haviland">Mr De Haviland</option>
                                            <option value="Mrs Saint Delafield" data-id="Mrs_Saint_Delafield">Mrs Saint Delafield</option>
                                            <option value="Mrs Sheppards" data-id="Mrs_Sheppards">Mrs Sheppards</option>
                                            <option value="Muli" data-id="Muli">Muli</option>
                                            <option value="Mystery Quest" data-id="Mystery_Quest">Mystery Quest</option>
                                            <option value="NTR" data-id="NTR">NTR</option>
                                            <option value="Neucha" data-id="Neucha">Neucha</option>
                                            <option value="Neuton" data-id="Neuton">Neuton</option>
                                            <option value="New Rocker" data-id="New_Rocker">New Rocker</option>
                                            <option value="News Cycle" data-id="News_Cycle">News Cycle</option>
                                            <option value="Niconne" data-id="Niconne">Niconne</option>
                                            <option value="Nixie One" data-id="Nixie_One">Nixie One</option>
                                            <option value="Nobile" data-id="Nobile">Nobile</option>
                                            <option value="Nokora" data-id="Nokora">Nokora</option>
                                            <option value="Norican" data-id="Norican">Norican</option>
                                            <option value="Nosifer" data-id="Nosifer">Nosifer</option>
                                            <option value="Nothing You Could Do" data-id="Nothing_You_Could_Do">Nothing You Could Do</option>
                                            <option value="Noticia Text" data-id="Noticia_Text">Noticia Text</option>
                                            <option value="Noto Sans" data-id="Noto_Sans">Noto Sans</option>
                                            <option value="Noto Serif" data-id="Noto_Serif">Noto Serif</option>
                                            <option value="Nova Cut" data-id="Nova_Cut">Nova Cut</option>
                                            <option value="Nova Flat" data-id="Nova_Flat">Nova Flat</option>
                                            <option value="Nova Mono" data-id="Nova_Mono">Nova Mono</option>
                                            <option value="Nova Oval" data-id="Nova_Oval">Nova Oval</option>
                                            <option value="Nova Round" data-id="Nova_Round">Nova Round</option>
                                            <option value="Nova Script" data-id="Nova_Script">Nova Script</option>
                                            <option value="Nova Slim" data-id="Nova_Slim">Nova Slim</option>
                                            <option value="Nova Square" data-id="Nova_Square">Nova Square</option>
                                            <option value="Numans" data-id="Numans">Numans</option>
                                            <option value="Nunito" data-id="Nunito">Nunito</option>
                                            <option value="Odor Mean Chey" data-id="Odor_Mean_Chey">Odor Mean Chey</option>
                                            <option value="Offside" data-id="Offside">Offside</option>
                                            <option value="Old Standard TT" data-id="Old_Standard_TT">Old Standard TT</option>
                                            <option value="Oldenburg" data-id="Oldenburg">Oldenburg</option>
                                            <option value="Oleo Script" data-id="Oleo_Script">Oleo Script</option>
                                            <option value="Oleo Script Swash Caps" data-id="Oleo_Script_Swash_Caps">Oleo Script Swash Caps</option>
                                            <option value="Open Sans" data-id="Open_Sans">Open Sans</option>
                                            <option value="Open Sans Condensed" data-id="Open_Sans_Condensed">Open Sans Condensed</option>
                                            <option value="Oranienbaum" data-id="Oranienbaum">Oranienbaum</option>
                                            <option value="Orbitron" data-id="Orbitron">Orbitron</option>
                                            <option value="Oregano" data-id="Oregano">Oregano</option>
                                            <option value="Orienta" data-id="Orienta">Orienta</option>
                                            <option value="Original Surfer" data-id="Original_Surfer">Original Surfer</option>
                                            <option value="Oswald" data-id="Oswald">Oswald</option>
                                            <option value="Over the Rainbow" data-id="Over_the_Rainbow">Over the Rainbow</option>
                                            <option value="Overlock" data-id="Overlock">Overlock</option>
                                            <option value="Overlock SC" data-id="Overlock_SC">Overlock SC</option>
                                            <option value="Ovo" data-id="Ovo">Ovo</option>
                                            <option value="Oxygen" data-id="Oxygen">Oxygen</option>
                                            <option value="Oxygen Mono" data-id="Oxygen_Mono">Oxygen Mono</option>
                                            <option value="PT Mono" data-id="PT_Mono">PT Mono</option>
                                            <option value="PT Sans" data-id="PT_Sans">PT Sans</option>
                                            <option value="PT Sans Caption" data-id="PT_Sans_Caption">PT Sans Caption</option>
                                            <option value="PT Sans Narrow" data-id="PT_Sans_Narrow">PT Sans Narrow</option>
                                            <option value="PT Serif" data-id="PT_Serif">PT Serif</option>
                                            <option value="PT Serif Caption" data-id="PT_Serif_Caption">PT Serif Caption</option>
                                            <option value="Pacifico" data-id="Pacifico">Pacifico</option>
                                            <option value="Palanquin" data-id="Palanquin">Palanquin</option>
                                            <option value="Palanquin Dark" data-id="Palanquin_Dark">Palanquin Dark</option>
                                            <option value="Paprika" data-id="Paprika">Paprika</option>
                                            <option value="Parisienne" data-id="Parisienne">Parisienne</option>
                                            <option value="Passero One" data-id="Passero_One">Passero One</option>
                                            <option value="Passion One" data-id="Passion_One">Passion One</option>
                                            <option value="Pathway Gothic One" data-id="Pathway_Gothic_One">Pathway Gothic One</option>
                                            <option value="Patrick Hand" data-id="Patrick_Hand">Patrick Hand</option>
                                            <option value="Patrick Hand SC" data-id="Patrick_Hand_SC">Patrick Hand SC</option>
                                            <option value="Patua One" data-id="Patua_One">Patua One</option>
                                            <option value="Paytone One" data-id="Paytone_One">Paytone One</option>
                                            <option value="Peddana" data-id="Peddana">Peddana</option>
                                            <option value="Peralta" data-id="Peralta">Peralta</option>
                                            <option value="Permanent Marker" data-id="Permanent_Marker">Permanent Marker</option>
                                            <option value="Petit Formal Script" data-id="Petit_Formal_Script">Petit Formal Script</option>
                                            <option value="Petrona" data-id="Petrona">Petrona</option>
                                            <option value="Philosopher" data-id="Philosopher">Philosopher</option>
                                            <option value="Piedra" data-id="Piedra">Piedra</option>
                                            <option value="Pinyon Script" data-id="Pinyon_Script">Pinyon Script</option>
                                            <option value="Pirata One" data-id="Pirata_One">Pirata One</option>
                                            <option value="Plaster" data-id="Plaster">Plaster</option>
                                            <option value="Play" data-id="Play">Play</option>
                                            <option value="Playball" data-id="Playball">Playball</option>
                                            <option value="Playfair Display" data-id="Playfair_Display">Playfair Display</option>
                                            <option value="Playfair Display SC" data-id="Playfair_Display_SC">Playfair Display SC</option>
                                            <option value="Podkova" data-id="Podkova">Podkova</option>
                                            <option value="Poiret One" data-id="Poiret_One">Poiret One</option>
                                            <option value="Poller One" data-id="Poller_One">Poller One</option>
                                            <option value="Poly" data-id="Poly">Poly</option>
                                            <option value="Pompiere" data-id="Pompiere">Pompiere</option>
                                            <option value="Pontano Sans" data-id="Pontano_Sans">Pontano Sans</option>
                                            <option value="Poppins" data-id="Poppins">Poppins</option>
                                            <option value="Port Lligat Sans" data-id="Port_Lligat_Sans">Port Lligat Sans</option>
                                            <option value="Port Lligat Slab" data-id="Port_Lligat_Slab">Port Lligat Slab</option>
                                            <option value="Pragati Narrow" data-id="Pragati_Narrow">Pragati Narrow</option>
                                            <option value="Prata" data-id="Prata">Prata</option>
                                            <option value="Preahvihear" data-id="Preahvihear">Preahvihear</option>
                                            <option value="Press Start 2P" data-id="Press_Start_2P">Press Start 2P</option>
                                            <option value="Princess Sofia" data-id="Princess_Sofia">Princess Sofia</option>
                                            <option value="Prociono" data-id="Prociono">Prociono</option>
                                            <option value="Prosto One" data-id="Prosto_One">Prosto One</option>
                                            <option value="Puritan" data-id="Puritan">Puritan</option>
                                            <option value="Purple Purse" data-id="Purple_Purse">Purple Purse</option>
                                            <option value="Quando" data-id="Quando">Quando</option>
                                            <option value="Quantico" data-id="Quantico">Quantico</option>
                                            <option value="Quattrocento" data-id="Quattrocento">Quattrocento</option>
                                            <option value="Quattrocento Sans" data-id="Quattrocento_Sans">Quattrocento Sans</option>
                                            <option value="Questrial" data-id="Questrial">Questrial</option>
                                            <option value="Quicksand" data-id="Quicksand">Quicksand</option>
                                            <option value="Quintessential" data-id="Quintessential">Quintessential</option>
                                            <option value="Qwigley" data-id="Qwigley">Qwigley</option>
                                            <option value="Racing Sans One" data-id="Racing_Sans_One">Racing Sans One</option>
                                            <option value="Radley" data-id="Radley">Radley</option>
                                            <option value="Rajdhani" data-id="Rajdhani">Rajdhani</option>
                                            <option value="Raleway" data-id="Raleway">Raleway</option>
                                            <option value="Raleway Dots" data-id="Raleway_Dots">Raleway Dots</option>
                                            <option value="Ramabhadra" data-id="Ramabhadra">Ramabhadra</option>
                                            <option value="Ramaraja" data-id="Ramaraja">Ramaraja</option>
                                            <option value="Rambla" data-id="Rambla">Rambla</option>
                                            <option value="Rammetto One" data-id="Rammetto_One">Rammetto One</option>
                                            <option value="Ranchers" data-id="Ranchers">Ranchers</option>
                                            <option value="Rancho" data-id="Rancho">Rancho</option>
                                            <option value="Ranga" data-id="Ranga">Ranga</option>
                                            <option value="Rationale" data-id="Rationale">Rationale</option>
                                            <option value="Ravi Prakash" data-id="Ravi_Prakash">Ravi Prakash</option>
                                            <option value="Redressed" data-id="Redressed">Redressed</option>
                                            <option value="Reenie Beanie" data-id="Reenie_Beanie">Reenie Beanie</option>
                                            <option value="Revalia" data-id="Revalia">Revalia</option>
                                            <option value="Rhodium Libre" data-id="Rhodium_Libre">Rhodium Libre</option>
                                            <option value="Ribeye" data-id="Ribeye">Ribeye</option>
                                            <option value="Ribeye Marrow" data-id="Ribeye_Marrow">Ribeye Marrow</option>
                                            <option value="Righteous" data-id="Righteous">Righteous</option>
                                            <option value="Risque" data-id="Risque">Risque</option>
                                            <option value="Roboto" data-id="Roboto" selected='selected'>Roboto</option>
                                            <option value="Roboto Condensed" data-id="Roboto_Condensed">Roboto Condensed</option>
                                            <option value="Roboto Mono" data-id="Roboto_Mono">Roboto Mono</option>
                                            <option value="Roboto Slab" data-id="Roboto_Slab">Roboto Slab</option>
                                            <option value="Rochester" data-id="Rochester">Rochester</option>
                                            <option value="Rock Salt" data-id="Rock_Salt">Rock Salt</option>
                                            <option value="Rokkitt" data-id="Rokkitt">Rokkitt</option>
                                            <option value="Romanesco" data-id="Romanesco">Romanesco</option>
                                            <option value="Ropa Sans" data-id="Ropa_Sans">Ropa Sans</option>
                                            <option value="Rosario" data-id="Rosario">Rosario</option>
                                            <option value="Rosarivo" data-id="Rosarivo">Rosarivo</option>
                                            <option value="Rouge Script" data-id="Rouge_Script">Rouge Script</option>
                                            <option value="Rozha One" data-id="Rozha_One">Rozha One</option>
                                            <option value="Rubik" data-id="Rubik">Rubik</option>
                                            <option value="Rubik Mono One" data-id="Rubik_Mono_One">Rubik Mono One</option>
                                            <option value="Rubik One" data-id="Rubik_One">Rubik One</option>
                                            <option value="Ruda" data-id="Ruda">Ruda</option>
                                            <option value="Rufina" data-id="Rufina">Rufina</option>
                                            <option value="Ruge Boogie" data-id="Ruge_Boogie">Ruge Boogie</option>
                                            <option value="Ruluko" data-id="Ruluko">Ruluko</option>
                                            <option value="Rum Raisin" data-id="Rum_Raisin">Rum Raisin</option>
                                            <option value="Ruslan Display" data-id="Ruslan_Display">Ruslan Display</option>
                                            <option value="Russo One" data-id="Russo_One">Russo One</option>
                                            <option value="Ruthie" data-id="Ruthie">Ruthie</option>
                                            <option value="Rye" data-id="Rye">Rye</option>
                                            <option value="Sacramento" data-id="Sacramento">Sacramento</option>
                                            <option value="Sahitya" data-id="Sahitya">Sahitya</option>
                                            <option value="Sail" data-id="Sail">Sail</option>
                                            <option value="Salsa" data-id="Salsa">Salsa</option>
                                            <option value="Sanchez" data-id="Sanchez">Sanchez</option>
                                            <option value="Sancreek" data-id="Sancreek">Sancreek</option>
                                            <option value="Sansita One" data-id="Sansita_One">Sansita One</option>
                                            <option value="Sarala" data-id="Sarala">Sarala</option>
                                            <option value="Sarina" data-id="Sarina">Sarina</option>
                                            <option value="Sarpanch" data-id="Sarpanch">Sarpanch</option>
                                            <option value="Satisfy" data-id="Satisfy">Satisfy</option>
                                            <option value="Scada" data-id="Scada">Scada</option>
                                            <option value="Scheherazade" data-id="Scheherazade">Scheherazade</option>
                                            <option value="Schoolbell" data-id="Schoolbell">Schoolbell</option>
                                            <option value="Seaweed Script" data-id="Seaweed_Script">Seaweed Script</option>
                                            <option value="Sevillana" data-id="Sevillana">Sevillana</option>
                                            <option value="Seymour One" data-id="Seymour_One">Seymour One</option>
                                            <option value="Shadows Into Light" data-id="Shadows_Into_Light">Shadows Into Light</option>
                                            <option value="Shadows Into Light Two" data-id="Shadows_Into_Light_Two">Shadows Into Light Two</option>
                                            <option value="Shanti" data-id="Shanti">Shanti</option>
                                            <option value="Share" data-id="Share">Share</option>
                                            <option value="Share Tech" data-id="Share_Tech">Share Tech</option>
                                            <option value="Share Tech Mono" data-id="Share_Tech_Mono">Share Tech Mono</option>
                                            <option value="Shojumaru" data-id="Shojumaru">Shojumaru</option>
                                            <option value="Short Stack" data-id="Short_Stack">Short Stack</option>
                                            <option value="Siemreap" data-id="Siemreap">Siemreap</option>
                                            <option value="Sigmar One" data-id="Sigmar_One">Sigmar One</option>
                                            <option value="Signika" data-id="Signika">Signika</option>
                                            <option value="Signika Negative" data-id="Signika_Negative">Signika Negative</option>
                                            <option value="Simonetta" data-id="Simonetta">Simonetta</option>
                                            <option value="Sintony" data-id="Sintony">Sintony</option>
                                            <option value="Sirin Stencil" data-id="Sirin_Stencil">Sirin Stencil</option>
                                            <option value="Six Caps" data-id="Six_Caps">Six Caps</option>
                                            <option value="Skranji" data-id="Skranji">Skranji</option>
                                            <option value="Slabo 13px" data-id="Slabo_13px">Slabo 13px</option>
                                            <option value="Slabo 27px" data-id="Slabo_27px">Slabo 27px</option>
                                            <option value="Slackey" data-id="Slackey">Slackey</option>
                                            <option value="Smokum" data-id="Smokum">Smokum</option>
                                            <option value="Smythe" data-id="Smythe">Smythe</option>
                                            <option value="Sniglet" data-id="Sniglet">Sniglet</option>
                                            <option value="Snippet" data-id="Snippet">Snippet</option>
                                            <option value="Snowburst One" data-id="Snowburst_One">Snowburst One</option>
                                            <option value="Sofadi One" data-id="Sofadi_One">Sofadi One</option>
                                            <option value="Sofia" data-id="Sofia">Sofia</option>
                                            <option value="Sonsie One" data-id="Sonsie_One">Sonsie One</option>
                                            <option value="Sorts Mill Goudy" data-id="Sorts_Mill_Goudy">Sorts Mill Goudy</option>
                                            <option value="Source Code Pro" data-id="Source_Code_Pro">Source Code Pro</option>
                                            <option value="Source Sans Pro" data-id="Source_Sans_Pro">Source Sans Pro</option>
                                            <option value="Source Serif Pro" data-id="Source_Serif_Pro">Source Serif Pro</option>
                                            <option value="Special Elite" data-id="Special_Elite">Special Elite</option>
                                            <option value="Spicy Rice" data-id="Spicy_Rice">Spicy Rice</option>
                                            <option value="Spinnaker" data-id="Spinnaker">Spinnaker</option>
                                            <option value="Spirax" data-id="Spirax">Spirax</option>
                                            <option value="Squada One" data-id="Squada_One">Squada One</option>
                                            <option value="Sree Krushnadevaraya" data-id="Sree_Krushnadevaraya">Sree Krushnadevaraya</option>
                                            <option value="Stalemate" data-id="Stalemate">Stalemate</option>
                                            <option value="Stalinist One" data-id="Stalinist_One">Stalinist One</option>
                                            <option value="Stardos Stencil" data-id="Stardos_Stencil">Stardos Stencil</option>
                                            <option value="Stint Ultra Condensed" data-id="Stint_Ultra_Condensed">Stint Ultra Condensed</option>
                                            <option value="Stint Ultra Expanded" data-id="Stint_Ultra_Expanded">Stint Ultra Expanded</option>
                                            <option value="Stoke" data-id="Stoke">Stoke</option>
                                            <option value="Strait" data-id="Strait">Strait</option>
                                            <option value="Sue Ellen Francisco" data-id="Sue_Ellen_Francisco">Sue Ellen Francisco</option>
                                            <option value="Sumana" data-id="Sumana">Sumana</option>
                                            <option value="Sunshiney" data-id="Sunshiney">Sunshiney</option>
                                            <option value="Supermercado One" data-id="Supermercado_One">Supermercado One</option>
                                            <option value="Sura" data-id="Sura">Sura</option>
                                            <option value="Suranna" data-id="Suranna">Suranna</option>
                                            <option value="Suravaram" data-id="Suravaram">Suravaram</option>
                                            <option value="Suwannaphum" data-id="Suwannaphum">Suwannaphum</option>
                                            <option value="Swanky and Moo Moo" data-id="Swanky_and_Moo_Moo">Swanky and Moo Moo</option>
                                            <option value="Syncopate" data-id="Syncopate">Syncopate</option>
                                            <option value="Tangerine" data-id="Tangerine">Tangerine</option>
                                            <option value="Taprom" data-id="Taprom">Taprom</option>
                                            <option value="Tauri" data-id="Tauri">Tauri</option>
                                            <option value="Teko" data-id="Teko">Teko</option>
                                            <option value="Telex" data-id="Telex">Telex</option>
                                            <option value="Tenali Ramakrishna" data-id="Tenali_Ramakrishna">Tenali Ramakrishna</option>
                                            <option value="Tenor Sans" data-id="Tenor_Sans">Tenor Sans</option>
                                            <option value="Text Me One" data-id="Text_Me_One">Text Me One</option>
                                            <option value="The Girl Next Door" data-id="The_Girl_Next_Door">The Girl Next Door</option>
                                            <option value="Tienne" data-id="Tienne">Tienne</option>
                                            <option value="Tillana" data-id="Tillana">Tillana</option>
                                            <option value="Timmana" data-id="Timmana">Timmana</option>
                                            <option value="Tinos" data-id="Tinos">Tinos</option>
                                            <option value="Titan One" data-id="Titan_One">Titan One</option>
                                            <option value="Titillium Web" data-id="Titillium_Web">Titillium Web</option>
                                            <option value="Trade Winds" data-id="Trade_Winds">Trade Winds</option>
                                            <option value="Trocchi" data-id="Trocchi">Trocchi</option>
                                            <option value="Trochut" data-id="Trochut">Trochut</option>
                                            <option value="Trykker" data-id="Trykker">Trykker</option>
                                            <option value="Tulpen One" data-id="Tulpen_One">Tulpen One</option>
                                            <option value="Ubuntu" data-id="Ubuntu">Ubuntu</option>
                                            <option value="Ubuntu Condensed" data-id="Ubuntu_Condensed">Ubuntu Condensed</option>
                                            <option value="Ubuntu Mono" data-id="Ubuntu_Mono">Ubuntu Mono</option>
                                            <option value="Ultra" data-id="Ultra">Ultra</option>
                                            <option value="Uncial Antiqua" data-id="Uncial_Antiqua">Uncial Antiqua</option>
                                            <option value="Underdog" data-id="Underdog">Underdog</option>
                                            <option value="Unica One" data-id="Unica_One">Unica One</option>
                                            <option value="UnifrakturCook" data-id="UnifrakturCook">UnifrakturCook</option>
                                            <option value="UnifrakturMaguntia" data-id="UnifrakturMaguntia">UnifrakturMaguntia</option>
                                            <option value="Unkempt" data-id="Unkempt">Unkempt</option>
                                            <option value="Unlock" data-id="Unlock">Unlock</option>
                                            <option value="Unna" data-id="Unna">Unna</option>
                                            <option value="VT323" data-id="VT323">VT323</option>
                                            <option value="Vampiro One" data-id="Vampiro_One">Vampiro One</option>
                                            <option value="Varela" data-id="Varela">Varela</option>
                                            <option value="Varela Round" data-id="Varela_Round">Varela Round</option>
                                            <option value="Vast Shadow" data-id="Vast_Shadow">Vast Shadow</option>
                                            <option value="Vesper Libre" data-id="Vesper_Libre">Vesper Libre</option>
                                            <option value="Vibur" data-id="Vibur">Vibur</option>
                                            <option value="Vidaloka" data-id="Vidaloka">Vidaloka</option>
                                            <option value="Viga" data-id="Viga">Viga</option>
                                            <option value="Voces" data-id="Voces">Voces</option>
                                            <option value="Volkhov" data-id="Volkhov">Volkhov</option>
                                            <option value="Vollkorn" data-id="Vollkorn">Vollkorn</option>
                                            <option value="Voltaire" data-id="Voltaire">Voltaire</option>
                                            <option value="Waiting for the Sunrise" data-id="Waiting_for_the_Sunrise">Waiting for the Sunrise</option>
                                            <option value="Wallpoet" data-id="Wallpoet">Wallpoet</option>
                                            <option value="Walter Turncoat" data-id="Walter_Turncoat">Walter Turncoat</option>
                                            <option value="Warnes" data-id="Warnes">Warnes</option>
                                            <option value="Wellfleet" data-id="Wellfleet">Wellfleet</option>
                                            <option value="Wendy One" data-id="Wendy_One">Wendy One</option>
                                            <option value="Wire One" data-id="Wire_One">Wire One</option>
                                            <option value="Work Sans" data-id="Work_Sans">Work Sans</option>
                                            <option value="Yanone Kaffeesatz" data-id="Yanone_Kaffeesatz">Yanone Kaffeesatz</option>
                                            <option value="Yantramanav" data-id="Yantramanav">Yantramanav</option>
                                            <option value="Yellowtail" data-id="Yellowtail">Yellowtail</option>
                                            <option value="Yeseva One" data-id="Yeseva_One">Yeseva One</option>
                                            <option value="Yesteryear" data-id="Yesteryear">Yesteryear</option>
                                            <option value="Zeyada" data-id="Zeyada">Zeyada</option>
                                        </select>
                    </div>
                </div>
                <div class="box-title">Heading Font</div>
                <div class="input-box">
                    <div class="input">
                        <select name="heading_font">
                                            <option value="ABeeZee" data-id="ABeeZee">ABeeZee</option>
                                            <option value="Abel" data-id="Abel">Abel</option>
                                            <option value="Abril Fatface" data-id="Abril_Fatface">Abril Fatface</option>
                                            <option value="Aclonica" data-id="Aclonica">Aclonica</option>
                                            <option value="Acme" data-id="Acme">Acme</option>
                                            <option value="Actor" data-id="Actor">Actor</option>
                                            <option value="Adamina" data-id="Adamina">Adamina</option>
                                            <option value="Advent Pro" data-id="Advent_Pro">Advent Pro</option>
                                            <option value="Aguafina Script" data-id="Aguafina_Script">Aguafina Script</option>
                                            <option value="Akronim" data-id="Akronim">Akronim</option>
                                            <option value="Aladin" data-id="Aladin">Aladin</option>
                                            <option value="Aldrich" data-id="Aldrich">Aldrich</option>
                                            <option value="Alef" data-id="Alef">Alef</option>
                                            <option value="Alegreya" data-id="Alegreya">Alegreya</option>
                                            <option value="Alegreya SC" data-id="Alegreya_SC">Alegreya SC</option>
                                            <option value="Alegreya Sans" data-id="Alegreya_Sans">Alegreya Sans</option>
                                            <option value="Alegreya Sans SC" data-id="Alegreya_Sans_SC">Alegreya Sans SC</option>
                                            <option value="Alex Brush" data-id="Alex_Brush">Alex Brush</option>
                                            <option value="Alfa Slab One" data-id="Alfa_Slab_One">Alfa Slab One</option>
                                            <option value="Alice" data-id="Alice">Alice</option>
                                            <option value="Alike" data-id="Alike">Alike</option>
                                            <option value="Alike Angular" data-id="Alike_Angular">Alike Angular</option>
                                            <option value="Allan" data-id="Allan">Allan</option>
                                            <option value="Allerta" data-id="Allerta">Allerta</option>
                                            <option value="Allerta Stencil" data-id="Allerta_Stencil">Allerta Stencil</option>
                                            <option value="Allura" data-id="Allura">Allura</option>
                                            <option value="Almendra" data-id="Almendra">Almendra</option>
                                            <option value="Almendra Display" data-id="Almendra_Display">Almendra Display</option>
                                            <option value="Almendra SC" data-id="Almendra_SC">Almendra SC</option>
                                            <option value="Amarante" data-id="Amarante">Amarante</option>
                                            <option value="Amaranth" data-id="Amaranth">Amaranth</option>
                                            <option value="Amatic SC" data-id="Amatic_SC">Amatic SC</option>
                                            <option value="Amethysta" data-id="Amethysta">Amethysta</option>
                                            <option value="Amiri" data-id="Amiri">Amiri</option>
                                            <option value="Amita" data-id="Amita">Amita</option>
                                            <option value="Anaheim" data-id="Anaheim">Anaheim</option>
                                            <option value="Andada" data-id="Andada">Andada</option>
                                            <option value="Andika" data-id="Andika">Andika</option>
                                            <option value="Angkor" data-id="Angkor">Angkor</option>
                                            <option value="Annie Use Your Telescope" data-id="Annie_Use_Your_Telescope">Annie Use Your Telescope</option>
                                            <option value="Anonymous Pro" data-id="Anonymous_Pro">Anonymous Pro</option>
                                            <option value="Antic" data-id="Antic">Antic</option>
                                            <option value="Antic Didone" data-id="Antic_Didone">Antic Didone</option>
                                            <option value="Antic Slab" data-id="Antic_Slab">Antic Slab</option>
                                            <option value="Anton" data-id="Anton">Anton</option>
                                            <option value="Arapey" data-id="Arapey">Arapey</option>
                                            <option value="Arbutus" data-id="Arbutus">Arbutus</option>
                                            <option value="Arbutus Slab" data-id="Arbutus_Slab">Arbutus Slab</option>
                                            <option value="Architects Daughter" data-id="Architects_Daughter">Architects Daughter</option>
                                            <option value="Archivo Black" data-id="Archivo_Black">Archivo Black</option>
                                            <option value="Archivo Narrow" data-id="Archivo_Narrow">Archivo Narrow</option>
                                            <option value="Arimo" data-id="Arimo">Arimo</option>
                                            <option value="Arizonia" data-id="Arizonia">Arizonia</option>
                                            <option value="Armata" data-id="Armata">Armata</option>
                                            <option value="Artifika" data-id="Artifika">Artifika</option>
                                            <option value="Arvo" data-id="Arvo">Arvo</option>
                                            <option value="Arya" data-id="Arya">Arya</option>
                                            <option value="Asap" data-id="Asap">Asap</option>
                                            <option value="Asar" data-id="Asar">Asar</option>
                                            <option value="Asset" data-id="Asset">Asset</option>
                                            <option value="Astloch" data-id="Astloch">Astloch</option>
                                            <option value="Asul" data-id="Asul">Asul</option>
                                            <option value="Atomic Age" data-id="Atomic_Age">Atomic Age</option>
                                            <option value="Aubrey" data-id="Aubrey">Aubrey</option>
                                            <option value="Audiowide" data-id="Audiowide">Audiowide</option>
                                            <option value="Autour One" data-id="Autour_One">Autour One</option>
                                            <option value="Average" data-id="Average">Average</option>
                                            <option value="Average Sans" data-id="Average_Sans">Average Sans</option>
                                            <option value="Averia Gruesa Libre" data-id="Averia_Gruesa_Libre">Averia Gruesa Libre</option>
                                            <option value="Averia Libre" data-id="Averia_Libre">Averia Libre</option>
                                            <option value="Averia Sans Libre" data-id="Averia_Sans_Libre">Averia Sans Libre</option>
                                            <option value="Averia Serif Libre" data-id="Averia_Serif_Libre">Averia Serif Libre</option>
                                            <option value="Bad Script" data-id="Bad_Script">Bad Script</option>
                                            <option value="Balthazar" data-id="Balthazar">Balthazar</option>
                                            <option value="Bangers" data-id="Bangers">Bangers</option>
                                            <option value="Basic" data-id="Basic">Basic</option>
                                            <option value="Battambang" data-id="Battambang">Battambang</option>
                                            <option value="Baumans" data-id="Baumans">Baumans</option>
                                            <option value="Bayon" data-id="Bayon">Bayon</option>
                                            <option value="Belgrano" data-id="Belgrano">Belgrano</option>
                                            <option value="Belleza" data-id="Belleza">Belleza</option>
                                            <option value="BenchNine" data-id="BenchNine">BenchNine</option>
                                            <option value="Bentham" data-id="Bentham">Bentham</option>
                                            <option value="Berkshire Swash" data-id="Berkshire_Swash">Berkshire Swash</option>
                                            <option value="Bevan" data-id="Bevan">Bevan</option>
                                            <option value="Bigelow Rules" data-id="Bigelow_Rules">Bigelow Rules</option>
                                            <option value="Bigshot One" data-id="Bigshot_One">Bigshot One</option>
                                            <option value="Bilbo" data-id="Bilbo">Bilbo</option>
                                            <option value="Bilbo Swash Caps" data-id="Bilbo_Swash_Caps">Bilbo Swash Caps</option>
                                            <option value="Biryani" data-id="Biryani">Biryani</option>
                                            <option value="Bitter" data-id="Bitter">Bitter</option>
                                            <option value="Black Ops One" data-id="Black_Ops_One">Black Ops One</option>
                                            <option value="Bokor" data-id="Bokor">Bokor</option>
                                            <option value="Bonbon" data-id="Bonbon">Bonbon</option>
                                            <option value="Boogaloo" data-id="Boogaloo">Boogaloo</option>
                                            <option value="Bowlby One" data-id="Bowlby_One">Bowlby One</option>
                                            <option value="Bowlby One SC" data-id="Bowlby_One_SC">Bowlby One SC</option>
                                            <option value="Brawler" data-id="Brawler">Brawler</option>
                                            <option value="Bree Serif" data-id="Bree_Serif">Bree Serif</option>
                                            <option value="Bubblegum Sans" data-id="Bubblegum_Sans">Bubblegum Sans</option>
                                            <option value="Bubbler One" data-id="Bubbler_One">Bubbler One</option>
                                            <option value="Buda" data-id="Buda">Buda</option>
                                            <option value="Buenard" data-id="Buenard">Buenard</option>
                                            <option value="Butcherman" data-id="Butcherman">Butcherman</option>
                                            <option value="Butterfly Kids" data-id="Butterfly_Kids">Butterfly Kids</option>
                                            <option value="Cabin" data-id="Cabin">Cabin</option>
                                            <option value="Cabin Condensed" data-id="Cabin_Condensed">Cabin Condensed</option>
                                            <option value="Cabin Sketch" data-id="Cabin_Sketch">Cabin Sketch</option>
                                            <option value="Caesar Dressing" data-id="Caesar_Dressing">Caesar Dressing</option>
                                            <option value="Cagliostro" data-id="Cagliostro">Cagliostro</option>
                                            <option value="Calligraffitti" data-id="Calligraffitti">Calligraffitti</option>
                                            <option value="Cambay" data-id="Cambay">Cambay</option>
                                            <option value="Cambo" data-id="Cambo">Cambo</option>
                                            <option value="Candal" data-id="Candal">Candal</option>
                                            <option value="Cantarell" data-id="Cantarell">Cantarell</option>
                                            <option value="Cantata One" data-id="Cantata_One">Cantata One</option>
                                            <option value="Cantora One" data-id="Cantora_One">Cantora One</option>
                                            <option value="Capriola" data-id="Capriola">Capriola</option>
                                            <option value="Cardo" data-id="Cardo">Cardo</option>
                                            <option value="Carme" data-id="Carme">Carme</option>
                                            <option value="Carrois Gothic" data-id="Carrois_Gothic">Carrois Gothic</option>
                                            <option value="Carrois Gothic SC" data-id="Carrois_Gothic_SC">Carrois Gothic SC</option>
                                            <option value="Carter One" data-id="Carter_One">Carter One</option>
                                            <option value="Catamaran" data-id="Catamaran">Catamaran</option>
                                            <option value="Caudex" data-id="Caudex">Caudex</option>
                                            <option value="Cedarville Cursive" data-id="Cedarville_Cursive">Cedarville Cursive</option>
                                            <option value="Ceviche One" data-id="Ceviche_One">Ceviche One</option>
                                            <option value="Changa One" data-id="Changa_One">Changa One</option>
                                            <option value="Chango" data-id="Chango">Chango</option>
                                            <option value="Chau Philomene One" data-id="Chau_Philomene_One">Chau Philomene One</option>
                                            <option value="Chela One" data-id="Chela_One">Chela One</option>
                                            <option value="Chelsea Market" data-id="Chelsea_Market">Chelsea Market</option>
                                            <option value="Chenla" data-id="Chenla">Chenla</option>
                                            <option value="Cherry Cream Soda" data-id="Cherry_Cream_Soda">Cherry Cream Soda</option>
                                            <option value="Cherry Swash" data-id="Cherry_Swash">Cherry Swash</option>
                                            <option value="Chewy" data-id="Chewy">Chewy</option>
                                            <option value="Chicle" data-id="Chicle">Chicle</option>
                                            <option value="Chivo" data-id="Chivo">Chivo</option>
                                            <option value="Chonburi" data-id="Chonburi">Chonburi</option>
                                            <option value="Cinzel" data-id="Cinzel">Cinzel</option>
                                            <option value="Cinzel Decorative" data-id="Cinzel_Decorative">Cinzel Decorative</option>
                                            <option value="Clicker Script" data-id="Clicker_Script">Clicker Script</option>
                                            <option value="Coda" data-id="Coda">Coda</option>
                                            <option value="Coda Caption" data-id="Coda_Caption">Coda Caption</option>
                                            <option value="Codystar" data-id="Codystar">Codystar</option>
                                            <option value="Combo" data-id="Combo">Combo</option>
                                            <option value="Comfortaa" data-id="Comfortaa">Comfortaa</option>
                                            <option value="Coming Soon" data-id="Coming_Soon">Coming Soon</option>
                                            <option value="Concert One" data-id="Concert_One">Concert One</option>
                                            <option value="Condiment" data-id="Condiment">Condiment</option>
                                            <option value="Content" data-id="Content">Content</option>
                                            <option value="Contrail One" data-id="Contrail_One">Contrail One</option>
                                            <option value="Convergence" data-id="Convergence">Convergence</option>
                                            <option value="Cookie" data-id="Cookie">Cookie</option>
                                            <option value="Copse" data-id="Copse">Copse</option>
                                            <option value="Corben" data-id="Corben">Corben</option>
                                            <option value="Courgette" data-id="Courgette">Courgette</option>
                                            <option value="Cousine" data-id="Cousine">Cousine</option>
                                            <option value="Coustard" data-id="Coustard">Coustard</option>
                                            <option value="Covered By Your Grace" data-id="Covered_By_Your_Grace">Covered By Your Grace</option>
                                            <option value="Crafty Girls" data-id="Crafty_Girls">Crafty Girls</option>
                                            <option value="Creepster" data-id="Creepster">Creepster</option>
                                            <option value="Crete Round" data-id="Crete_Round">Crete Round</option>
                                            <option value="Crimson Text" data-id="Crimson_Text">Crimson Text</option>
                                            <option value="Croissant One" data-id="Croissant_One">Croissant One</option>
                                            <option value="Crushed" data-id="Crushed">Crushed</option>
                                            <option value="Cuprum" data-id="Cuprum">Cuprum</option>
                                            <option value="Cutive" data-id="Cutive">Cutive</option>
                                            <option value="Cutive Mono" data-id="Cutive_Mono">Cutive Mono</option>
                                            <option value="Damion" data-id="Damion">Damion</option>
                                            <option value="Dancing Script" data-id="Dancing_Script">Dancing Script</option>
                                            <option value="Dangrek" data-id="Dangrek">Dangrek</option>
                                            <option value="Dawning of a New Day" data-id="Dawning_of_a_New_Day">Dawning of a New Day</option>
                                            <option value="Days One" data-id="Days_One">Days One</option>
                                            <option value="Dekko" data-id="Dekko">Dekko</option>
                                            <option value="Delius" data-id="Delius">Delius</option>
                                            <option value="Delius Swash Caps" data-id="Delius_Swash_Caps">Delius Swash Caps</option>
                                            <option value="Delius Unicase" data-id="Delius_Unicase">Delius Unicase</option>
                                            <option value="Della Respira" data-id="Della_Respira">Della Respira</option>
                                            <option value="Denk One" data-id="Denk_One">Denk One</option>
                                            <option value="Devonshire" data-id="Devonshire">Devonshire</option>
                                            <option value="Dhurjati" data-id="Dhurjati">Dhurjati</option>
                                            <option value="Didact Gothic" data-id="Didact_Gothic">Didact Gothic</option>
                                            <option value="Diplomata" data-id="Diplomata">Diplomata</option>
                                            <option value="Diplomata SC" data-id="Diplomata_SC">Diplomata SC</option>
                                            <option value="Domine" data-id="Domine">Domine</option>
                                            <option value="Donegal One" data-id="Donegal_One">Donegal One</option>
                                            <option value="Doppio One" data-id="Doppio_One">Doppio One</option>
                                            <option value="Dorsa" data-id="Dorsa">Dorsa</option>
                                            <option value="Dosis" data-id="Dosis">Dosis</option>
                                            <option value="Dr Sugiyama" data-id="Dr_Sugiyama">Dr Sugiyama</option>
                                            <option value="Droid Sans" data-id="Droid_Sans">Droid Sans</option>
                                            <option value="Droid Sans Mono" data-id="Droid_Sans_Mono">Droid Sans Mono</option>
                                            <option value="Droid Serif" data-id="Droid_Serif">Droid Serif</option>
                                            <option value="Duru Sans" data-id="Duru_Sans">Duru Sans</option>
                                            <option value="Dynalight" data-id="Dynalight">Dynalight</option>
                                            <option value="EB Garamond" data-id="EB_Garamond">EB Garamond</option>
                                            <option value="Eagle Lake" data-id="Eagle_Lake">Eagle Lake</option>
                                            <option value="Eater" data-id="Eater">Eater</option>
                                            <option value="Economica" data-id="Economica">Economica</option>
                                            <option value="Eczar" data-id="Eczar">Eczar</option>
                                            <option value="Ek Mukta" data-id="Ek_Mukta">Ek Mukta</option>
                                            <option value="Electrolize" data-id="Electrolize">Electrolize</option>
                                            <option value="Elsie" data-id="Elsie">Elsie</option>
                                            <option value="Elsie Swash Caps" data-id="Elsie_Swash_Caps">Elsie Swash Caps</option>
                                            <option value="Emblema One" data-id="Emblema_One">Emblema One</option>
                                            <option value="Emilys Candy" data-id="Emilys_Candy">Emilys Candy</option>
                                            <option value="Engagement" data-id="Engagement">Engagement</option>
                                            <option value="Englebert" data-id="Englebert">Englebert</option>
                                            <option value="Enriqueta" data-id="Enriqueta">Enriqueta</option>
                                            <option value="Erica One" data-id="Erica_One">Erica One</option>
                                            <option value="Esteban" data-id="Esteban">Esteban</option>
                                            <option value="Euphoria Script" data-id="Euphoria_Script">Euphoria Script</option>
                                            <option value="Ewert" data-id="Ewert">Ewert</option>
                                            <option value="Exo" data-id="Exo">Exo</option>
                                            <option value="Exo 2" data-id="Exo_2">Exo 2</option>
                                            <option value="Expletus Sans" data-id="Expletus_Sans">Expletus Sans</option>
                                            <option value="Fanwood Text" data-id="Fanwood_Text">Fanwood Text</option>
                                            <option value="Fascinate" data-id="Fascinate">Fascinate</option>
                                            <option value="Fascinate Inline" data-id="Fascinate_Inline">Fascinate Inline</option>
                                            <option value="Faster One" data-id="Faster_One">Faster One</option>
                                            <option value="Fasthand" data-id="Fasthand">Fasthand</option>
                                            <option value="Fauna One" data-id="Fauna_One">Fauna One</option>
                                            <option value="Federant" data-id="Federant">Federant</option>
                                            <option value="Federo" data-id="Federo">Federo</option>
                                            <option value="Felipa" data-id="Felipa">Felipa</option>
                                            <option value="Fenix" data-id="Fenix">Fenix</option>
                                            <option value="Finger Paint" data-id="Finger_Paint">Finger Paint</option>
                                            <option value="Fira Mono" data-id="Fira_Mono">Fira Mono</option>
                                            <option value="Fira Sans" data-id="Fira_Sans">Fira Sans</option>
                                            <option value="Fjalla One" data-id="Fjalla_One">Fjalla One</option>
                                            <option value="Fjord One" data-id="Fjord_One">Fjord One</option>
                                            <option value="Flamenco" data-id="Flamenco">Flamenco</option>
                                            <option value="Flavors" data-id="Flavors">Flavors</option>
                                            <option value="Fondamento" data-id="Fondamento">Fondamento</option>
                                            <option value="Fontdiner Swanky" data-id="Fontdiner_Swanky">Fontdiner Swanky</option>
                                            <option value="Forum" data-id="Forum">Forum</option>
                                            <option value="Francois One" data-id="Francois_One">Francois One</option>
                                            <option value="Freckle Face" data-id="Freckle_Face">Freckle Face</option>
                                            <option value="Fredericka the Great" data-id="Fredericka_the_Great">Fredericka the Great</option>
                                            <option value="Fredoka One" data-id="Fredoka_One">Fredoka One</option>
                                            <option value="Freehand" data-id="Freehand">Freehand</option>
                                            <option value="Fresca" data-id="Fresca">Fresca</option>
                                            <option value="Frijole" data-id="Frijole">Frijole</option>
                                            <option value="Fruktur" data-id="Fruktur">Fruktur</option>
                                            <option value="Fugaz One" data-id="Fugaz_One">Fugaz One</option>
                                            <option value="GFS Didot" data-id="GFS_Didot">GFS Didot</option>
                                            <option value="GFS Neohellenic" data-id="GFS_Neohellenic">GFS Neohellenic</option>
                                            <option value="Gabriela" data-id="Gabriela">Gabriela</option>
                                            <option value="Gafata" data-id="Gafata">Gafata</option>
                                            <option value="Galdeano" data-id="Galdeano">Galdeano</option>
                                            <option value="Galindo" data-id="Galindo">Galindo</option>
                                            <option value="Gentium Basic" data-id="Gentium_Basic">Gentium Basic</option>
                                            <option value="Gentium Book Basic" data-id="Gentium_Book_Basic">Gentium Book Basic</option>
                                            <option value="Geo" data-id="Geo">Geo</option>
                                            <option value="Geostar" data-id="Geostar">Geostar</option>
                                            <option value="Geostar Fill" data-id="Geostar_Fill">Geostar Fill</option>
                                            <option value="Germania One" data-id="Germania_One">Germania One</option>
                                            <option value="Gidugu" data-id="Gidugu">Gidugu</option>
                                            <option value="Gilda Display" data-id="Gilda_Display">Gilda Display</option>
                                            <option value="Give You Glory" data-id="Give_You_Glory">Give You Glory</option>
                                            <option value="Glass Antiqua" data-id="Glass_Antiqua">Glass Antiqua</option>
                                            <option value="Glegoo" data-id="Glegoo">Glegoo</option>
                                            <option value="Gloria Hallelujah" data-id="Gloria_Hallelujah">Gloria Hallelujah</option>
                                            <option value="Goblin One" data-id="Goblin_One">Goblin One</option>
                                            <option value="Gochi Hand" data-id="Gochi_Hand">Gochi Hand</option>
                                            <option value="Gorditas" data-id="Gorditas">Gorditas</option>
                                            <option value="Goudy Bookletter 1911" data-id="Goudy_Bookletter_1911">Goudy Bookletter 1911</option>
                                            <option value="Graduate" data-id="Graduate">Graduate</option>
                                            <option value="Grand Hotel" data-id="Grand_Hotel">Grand Hotel</option>
                                            <option value="Gravitas One" data-id="Gravitas_One">Gravitas One</option>
                                            <option value="Great Vibes" data-id="Great_Vibes">Great Vibes</option>
                                            <option value="Griffy" data-id="Griffy">Griffy</option>
                                            <option value="Gruppo" data-id="Gruppo">Gruppo</option>
                                            <option value="Gudea" data-id="Gudea">Gudea</option>
                                            <option value="Gurajada" data-id="Gurajada">Gurajada</option>
                                            <option value="Habibi" data-id="Habibi">Habibi</option>
                                            <option value="Halant" data-id="Halant">Halant</option>
                                            <option value="Hammersmith One" data-id="Hammersmith_One">Hammersmith One</option>
                                            <option value="Hanalei" data-id="Hanalei">Hanalei</option>
                                            <option value="Hanalei Fill" data-id="Hanalei_Fill">Hanalei Fill</option>
                                            <option value="Handlee" data-id="Handlee">Handlee</option>
                                            <option value="Hanuman" data-id="Hanuman">Hanuman</option>
                                            <option value="Happy Monkey" data-id="Happy_Monkey">Happy Monkey</option>
                                            <option value="Headland One" data-id="Headland_One">Headland One</option>
                                            <option value="Henny Penny" data-id="Henny_Penny">Henny Penny</option>
                                            <option value="Herr Von Muellerhoff" data-id="Herr_Von_Muellerhoff">Herr Von Muellerhoff</option>
                                            <option value="Hind" data-id="Hind">Hind</option>
                                            <option value="Hind Siliguri" data-id="Hind_Siliguri">Hind Siliguri</option>
                                            <option value="Hind Vadodara" data-id="Hind_Vadodara">Hind Vadodara</option>
                                            <option value="Holtwood One SC" data-id="Holtwood_One_SC">Holtwood One SC</option>
                                            <option value="Homemade Apple" data-id="Homemade_Apple">Homemade Apple</option>
                                            <option value="Homenaje" data-id="Homenaje">Homenaje</option>
                                            <option value="IM Fell DW Pica" data-id="IM_Fell_DW_Pica">IM Fell DW Pica</option>
                                            <option value="IM Fell DW Pica SC" data-id="IM_Fell_DW_Pica_SC">IM Fell DW Pica SC</option>
                                            <option value="IM Fell Double Pica" data-id="IM_Fell_Double_Pica">IM Fell Double Pica</option>
                                            <option value="IM Fell Double Pica SC" data-id="IM_Fell_Double_Pica_SC">IM Fell Double Pica SC</option>
                                            <option value="IM Fell English" data-id="IM_Fell_English">IM Fell English</option>
                                            <option value="IM Fell English SC" data-id="IM_Fell_English_SC">IM Fell English SC</option>
                                            <option value="IM Fell French Canon" data-id="IM_Fell_French_Canon">IM Fell French Canon</option>
                                            <option value="IM Fell French Canon SC" data-id="IM_Fell_French_Canon_SC">IM Fell French Canon SC</option>
                                            <option value="IM Fell Great Primer" data-id="IM_Fell_Great_Primer">IM Fell Great Primer</option>
                                            <option value="IM Fell Great Primer SC" data-id="IM_Fell_Great_Primer_SC">IM Fell Great Primer SC</option>
                                            <option value="Iceberg" data-id="Iceberg">Iceberg</option>
                                            <option value="Iceland" data-id="Iceland">Iceland</option>
                                            <option value="Imprima" data-id="Imprima">Imprima</option>
                                            <option value="Inconsolata" data-id="Inconsolata">Inconsolata</option>
                                            <option value="Inder" data-id="Inder">Inder</option>
                                            <option value="Indie Flower" data-id="Indie_Flower">Indie Flower</option>
                                            <option value="Inika" data-id="Inika">Inika</option>
                                            <option value="Inknut Antiqua" data-id="Inknut_Antiqua">Inknut Antiqua</option>
                                            <option value="Irish Grover" data-id="Irish_Grover">Irish Grover</option>
                                            <option value="Istok Web" data-id="Istok_Web">Istok Web</option>
                                            <option value="Italiana" data-id="Italiana">Italiana</option>
                                            <option value="Italianno" data-id="Italianno">Italianno</option>
                                            <option value="Itim" data-id="Itim">Itim</option>
                                            <option value="Jacques Francois" data-id="Jacques_Francois">Jacques Francois</option>
                                            <option value="Jacques Francois Shadow" data-id="Jacques_Francois_Shadow">Jacques Francois Shadow</option>
                                            <option value="Jaldi" data-id="Jaldi">Jaldi</option>
                                            <option value="Jim Nightshade" data-id="Jim_Nightshade">Jim Nightshade</option>
                                            <option value="Jockey One" data-id="Jockey_One">Jockey One</option>
                                            <option value="Jolly Lodger" data-id="Jolly_Lodger">Jolly Lodger</option>
                                            <option value="Josefin Sans" data-id="Josefin_Sans">Josefin Sans</option>
                                            <option value="Josefin Slab" data-id="Josefin_Slab">Josefin Slab</option>
                                            <option value="Joti One" data-id="Joti_One">Joti One</option>
                                            <option value="Judson" data-id="Judson">Judson</option>
                                            <option value="Julee" data-id="Julee">Julee</option>
                                            <option value="Julius Sans One" data-id="Julius_Sans_One">Julius Sans One</option>
                                            <option value="Junge" data-id="Junge">Junge</option>
                                            <option value="Jura" data-id="Jura">Jura</option>
                                            <option value="Just Another Hand" data-id="Just_Another_Hand">Just Another Hand</option>
                                            <option value="Just Me Again Down Here" data-id="Just_Me_Again_Down_Here">Just Me Again Down Here</option>
                                            <option value="Kadwa" data-id="Kadwa">Kadwa</option>
                                            <option value="Kalam" data-id="Kalam">Kalam</option>
                                            <option value="Kameron" data-id="Kameron">Kameron</option>
                                            <option value="Kantumruy" data-id="Kantumruy">Kantumruy</option>
                                            <option value="Karla" data-id="Karla">Karla</option>
                                            <option value="Karma" data-id="Karma">Karma</option>
                                            <option value="Kaushan Script" data-id="Kaushan_Script">Kaushan Script</option>
                                            <option value="Kavoon" data-id="Kavoon">Kavoon</option>
                                            <option value="Kdam Thmor" data-id="Kdam_Thmor">Kdam Thmor</option>
                                            <option value="Keania One" data-id="Keania_One">Keania One</option>
                                            <option value="Kelly Slab" data-id="Kelly_Slab">Kelly Slab</option>
                                            <option value="Kenia" data-id="Kenia">Kenia</option>
                                            <option value="Khand" data-id="Khand">Khand</option>
                                            <option value="Khmer" data-id="Khmer">Khmer</option>
                                            <option value="Khula" data-id="Khula">Khula</option>
                                            <option value="Kite One" data-id="Kite_One">Kite One</option>
                                            <option value="Knewave" data-id="Knewave">Knewave</option>
                                            <option value="Kotta One" data-id="Kotta_One">Kotta One</option>
                                            <option value="Koulen" data-id="Koulen">Koulen</option>
                                            <option value="Kranky" data-id="Kranky">Kranky</option>
                                            <option value="Kreon" data-id="Kreon">Kreon</option>
                                            <option value="Kristi" data-id="Kristi">Kristi</option>
                                            <option value="Krona One" data-id="Krona_One">Krona One</option>
                                            <option value="Kurale" data-id="Kurale">Kurale</option>
                                            <option value="La Belle Aurore" data-id="La_Belle_Aurore">La Belle Aurore</option>
                                            <option value="Laila" data-id="Laila">Laila</option>
                                            <option value="Lakki Reddy" data-id="Lakki_Reddy">Lakki Reddy</option>
                                            <option value="Lancelot" data-id="Lancelot">Lancelot</option>
                                            <option value="Lateef" data-id="Lateef">Lateef</option>
                                            <option value="Lato" data-id="Lato">Lato</option>
                                            <option value="League Script" data-id="League_Script">League Script</option>
                                            <option value="Leckerli One" data-id="Leckerli_One">Leckerli One</option>
                                            <option value="Ledger" data-id="Ledger">Ledger</option>
                                            <option value="Lekton" data-id="Lekton">Lekton</option>
                                            <option value="Lemon" data-id="Lemon">Lemon</option>
                                            <option value="Libre Baskerville" data-id="Libre_Baskerville">Libre Baskerville</option>
                                            <option value="Life Savers" data-id="Life_Savers">Life Savers</option>
                                            <option value="Lilita One" data-id="Lilita_One">Lilita One</option>
                                            <option value="Lily Script One" data-id="Lily_Script_One">Lily Script One</option>
                                            <option value="Limelight" data-id="Limelight">Limelight</option>
                                            <option value="Linden Hill" data-id="Linden_Hill">Linden Hill</option>
                                            <option value="Lobster" data-id="Lobster">Lobster</option>
                                            <option value="Lobster Two" data-id="Lobster_Two">Lobster Two</option>
                                            <option value="Londrina Outline" data-id="Londrina_Outline">Londrina Outline</option>
                                            <option value="Londrina Shadow" data-id="Londrina_Shadow">Londrina Shadow</option>
                                            <option value="Londrina Sketch" data-id="Londrina_Sketch">Londrina Sketch</option>
                                            <option value="Londrina Solid" data-id="Londrina_Solid">Londrina Solid</option>
                                            <option value="Lora" data-id="Lora">Lora</option>
                                            <option value="Love Ya Like A Sister" data-id="Love_Ya_Like_A_Sister">Love Ya Like A Sister</option>
                                            <option value="Loved by the King" data-id="Loved_by_the_King">Loved by the King</option>
                                            <option value="Lovers Quarrel" data-id="Lovers_Quarrel">Lovers Quarrel</option>
                                            <option value="Luckiest Guy" data-id="Luckiest_Guy">Luckiest Guy</option>
                                            <option value="Lusitana" data-id="Lusitana">Lusitana</option>
                                            <option value="Lustria" data-id="Lustria">Lustria</option>
                                            <option value="Macondo" data-id="Macondo">Macondo</option>
                                            <option value="Macondo Swash Caps" data-id="Macondo_Swash_Caps">Macondo Swash Caps</option>
                                            <option value="Magra" data-id="Magra">Magra</option>
                                            <option value="Maiden Orange" data-id="Maiden_Orange">Maiden Orange</option>
                                            <option value="Mako" data-id="Mako">Mako</option>
                                            <option value="Mallanna" data-id="Mallanna">Mallanna</option>
                                            <option value="Mandali" data-id="Mandali">Mandali</option>
                                            <option value="Marcellus" data-id="Marcellus">Marcellus</option>
                                            <option value="Marcellus SC" data-id="Marcellus_SC">Marcellus SC</option>
                                            <option value="Marck Script" data-id="Marck_Script">Marck Script</option>
                                            <option value="Margarine" data-id="Margarine">Margarine</option>
                                            <option value="Marko One" data-id="Marko_One">Marko One</option>
                                            <option value="Marmelad" data-id="Marmelad">Marmelad</option>
                                            <option value="Martel" data-id="Martel">Martel</option>
                                            <option value="Martel Sans" data-id="Martel_Sans">Martel Sans</option>
                                            <option value="Marvel" data-id="Marvel">Marvel</option>
                                            <option value="Mate" data-id="Mate">Mate</option>
                                            <option value="Mate SC" data-id="Mate_SC">Mate SC</option>
                                            <option value="Maven Pro" data-id="Maven_Pro">Maven Pro</option>
                                            <option value="McLaren" data-id="McLaren">McLaren</option>
                                            <option value="Meddon" data-id="Meddon">Meddon</option>
                                            <option value="MedievalSharp" data-id="MedievalSharp">MedievalSharp</option>
                                            <option value="Medula One" data-id="Medula_One">Medula One</option>
                                            <option value="Megrim" data-id="Megrim">Megrim</option>
                                            <option value="Meie Script" data-id="Meie_Script">Meie Script</option>
                                            <option value="Merienda" data-id="Merienda">Merienda</option>
                                            <option value="Merienda One" data-id="Merienda_One">Merienda One</option>
                                            <option value="Merriweather" data-id="Merriweather">Merriweather</option>
                                            <option value="Merriweather Sans" data-id="Merriweather_Sans">Merriweather Sans</option>
                                            <option value="Metal" data-id="Metal">Metal</option>
                                            <option value="Metal Mania" data-id="Metal_Mania">Metal Mania</option>
                                            <option value="Metamorphous" data-id="Metamorphous">Metamorphous</option>
                                            <option value="Metrophobic" data-id="Metrophobic">Metrophobic</option>
                                            <option value="Michroma" data-id="Michroma">Michroma</option>
                                            <option value="Milonga" data-id="Milonga">Milonga</option>
                                            <option value="Miltonian" data-id="Miltonian">Miltonian</option>
                                            <option value="Miltonian Tattoo" data-id="Miltonian_Tattoo">Miltonian Tattoo</option>
                                            <option value="Miniver" data-id="Miniver">Miniver</option>
                                            <option value="Miss Fajardose" data-id="Miss_Fajardose">Miss Fajardose</option>
                                            <option value="Modak" data-id="Modak">Modak</option>
                                            <option value="Modern Antiqua" data-id="Modern_Antiqua">Modern Antiqua</option>
                                            <option value="Molengo" data-id="Molengo">Molengo</option>
                                            <option value="Molle" data-id="Molle">Molle</option>
                                            <option value="Monda" data-id="Monda">Monda</option>
                                            <option value="Monofett" data-id="Monofett">Monofett</option>
                                            <option value="Monoton" data-id="Monoton">Monoton</option>
                                            <option value="Monsieur La Doulaise" data-id="Monsieur_La_Doulaise">Monsieur La Doulaise</option>
                                            <option value="Montaga" data-id="Montaga">Montaga</option>
                                            <option value="Montez" data-id="Montez">Montez</option>
                                            <option value="Montserrat" data-id="Montserrat">Montserrat</option>
                                            <option value="Montserrat Alternates" data-id="Montserrat_Alternates">Montserrat Alternates</option>
                                            <option value="Montserrat Subrayada" data-id="Montserrat_Subrayada">Montserrat Subrayada</option>
                                            <option value="Moul" data-id="Moul">Moul</option>
                                            <option value="Moulpali" data-id="Moulpali">Moulpali</option>
                                            <option value="Mountains of Christmas" data-id="Mountains_of_Christmas">Mountains of Christmas</option>
                                            <option value="Mouse Memoirs" data-id="Mouse_Memoirs">Mouse Memoirs</option>
                                            <option value="Mr Bedfort" data-id="Mr_Bedfort">Mr Bedfort</option>
                                            <option value="Mr Dafoe" data-id="Mr_Dafoe">Mr Dafoe</option>
                                            <option value="Mr De Haviland" data-id="Mr_De_Haviland">Mr De Haviland</option>
                                            <option value="Mrs Saint Delafield" data-id="Mrs_Saint_Delafield">Mrs Saint Delafield</option>
                                            <option value="Mrs Sheppards" data-id="Mrs_Sheppards">Mrs Sheppards</option>
                                            <option value="Muli" data-id="Muli">Muli</option>
                                            <option value="Mystery Quest" data-id="Mystery_Quest">Mystery Quest</option>
                                            <option value="NTR" data-id="NTR">NTR</option>
                                            <option value="Neucha" data-id="Neucha">Neucha</option>
                                            <option value="Neuton" data-id="Neuton">Neuton</option>
                                            <option value="New Rocker" data-id="New_Rocker">New Rocker</option>
                                            <option value="News Cycle" data-id="News_Cycle">News Cycle</option>
                                            <option value="Niconne" data-id="Niconne">Niconne</option>
                                            <option value="Nixie One" data-id="Nixie_One">Nixie One</option>
                                            <option value="Nobile" data-id="Nobile">Nobile</option>
                                            <option value="Nokora" data-id="Nokora">Nokora</option>
                                            <option value="Norican" data-id="Norican">Norican</option>
                                            <option value="Nosifer" data-id="Nosifer">Nosifer</option>
                                            <option value="Nothing You Could Do" data-id="Nothing_You_Could_Do">Nothing You Could Do</option>
                                            <option value="Noticia Text" data-id="Noticia_Text">Noticia Text</option>
                                            <option value="Noto Sans" data-id="Noto_Sans">Noto Sans</option>
                                            <option value="Noto Serif" data-id="Noto_Serif">Noto Serif</option>
                                            <option value="Nova Cut" data-id="Nova_Cut">Nova Cut</option>
                                            <option value="Nova Flat" data-id="Nova_Flat">Nova Flat</option>
                                            <option value="Nova Mono" data-id="Nova_Mono">Nova Mono</option>
                                            <option value="Nova Oval" data-id="Nova_Oval">Nova Oval</option>
                                            <option value="Nova Round" data-id="Nova_Round">Nova Round</option>
                                            <option value="Nova Script" data-id="Nova_Script">Nova Script</option>
                                            <option value="Nova Slim" data-id="Nova_Slim">Nova Slim</option>
                                            <option value="Nova Square" data-id="Nova_Square">Nova Square</option>
                                            <option value="Numans" data-id="Numans">Numans</option>
                                            <option value="Nunito" data-id="Nunito">Nunito</option>
                                            <option value="Odor Mean Chey" data-id="Odor_Mean_Chey">Odor Mean Chey</option>
                                            <option value="Offside" data-id="Offside">Offside</option>
                                            <option value="Old Standard TT" data-id="Old_Standard_TT">Old Standard TT</option>
                                            <option value="Oldenburg" data-id="Oldenburg">Oldenburg</option>
                                            <option value="Oleo Script" data-id="Oleo_Script">Oleo Script</option>
                                            <option value="Oleo Script Swash Caps" data-id="Oleo_Script_Swash_Caps">Oleo Script Swash Caps</option>
                                            <option value="Open Sans" data-id="Open_Sans">Open Sans</option>
                                            <option value="Open Sans Condensed" data-id="Open_Sans_Condensed">Open Sans Condensed</option>
                                            <option value="Oranienbaum" data-id="Oranienbaum">Oranienbaum</option>
                                            <option value="Orbitron" data-id="Orbitron">Orbitron</option>
                                            <option value="Oregano" data-id="Oregano">Oregano</option>
                                            <option value="Orienta" data-id="Orienta">Orienta</option>
                                            <option value="Original Surfer" data-id="Original_Surfer">Original Surfer</option>
                                            <option value="Oswald" data-id="Oswald">Oswald</option>
                                            <option value="Over the Rainbow" data-id="Over_the_Rainbow">Over the Rainbow</option>
                                            <option value="Overlock" data-id="Overlock">Overlock</option>
                                            <option value="Overlock SC" data-id="Overlock_SC">Overlock SC</option>
                                            <option value="Ovo" data-id="Ovo">Ovo</option>
                                            <option value="Oxygen" data-id="Oxygen">Oxygen</option>
                                            <option value="Oxygen Mono" data-id="Oxygen_Mono">Oxygen Mono</option>
                                            <option value="PT Mono" data-id="PT_Mono">PT Mono</option>
                                            <option value="PT Sans" data-id="PT_Sans">PT Sans</option>
                                            <option value="PT Sans Caption" data-id="PT_Sans_Caption">PT Sans Caption</option>
                                            <option value="PT Sans Narrow" data-id="PT_Sans_Narrow">PT Sans Narrow</option>
                                            <option value="PT Serif" data-id="PT_Serif">PT Serif</option>
                                            <option value="PT Serif Caption" data-id="PT_Serif_Caption">PT Serif Caption</option>
                                            <option value="Pacifico" data-id="Pacifico">Pacifico</option>
                                            <option value="Palanquin" data-id="Palanquin">Palanquin</option>
                                            <option value="Palanquin Dark" data-id="Palanquin_Dark">Palanquin Dark</option>
                                            <option value="Paprika" data-id="Paprika">Paprika</option>
                                            <option value="Parisienne" data-id="Parisienne">Parisienne</option>
                                            <option value="Passero One" data-id="Passero_One">Passero One</option>
                                            <option value="Passion One" data-id="Passion_One">Passion One</option>
                                            <option value="Pathway Gothic One" data-id="Pathway_Gothic_One">Pathway Gothic One</option>
                                            <option value="Patrick Hand" data-id="Patrick_Hand">Patrick Hand</option>
                                            <option value="Patrick Hand SC" data-id="Patrick_Hand_SC">Patrick Hand SC</option>
                                            <option value="Patua One" data-id="Patua_One">Patua One</option>
                                            <option value="Paytone One" data-id="Paytone_One">Paytone One</option>
                                            <option value="Peddana" data-id="Peddana">Peddana</option>
                                            <option value="Peralta" data-id="Peralta">Peralta</option>
                                            <option value="Permanent Marker" data-id="Permanent_Marker">Permanent Marker</option>
                                            <option value="Petit Formal Script" data-id="Petit_Formal_Script">Petit Formal Script</option>
                                            <option value="Petrona" data-id="Petrona">Petrona</option>
                                            <option value="Philosopher" data-id="Philosopher">Philosopher</option>
                                            <option value="Piedra" data-id="Piedra">Piedra</option>
                                            <option value="Pinyon Script" data-id="Pinyon_Script">Pinyon Script</option>
                                            <option value="Pirata One" data-id="Pirata_One">Pirata One</option>
                                            <option value="Plaster" data-id="Plaster">Plaster</option>
                                            <option value="Play" data-id="Play">Play</option>
                                            <option value="Playball" data-id="Playball">Playball</option>
                                            <option value="Playfair Display" data-id="Playfair_Display">Playfair Display</option>
                                            <option value="Playfair Display SC" data-id="Playfair_Display_SC">Playfair Display SC</option>
                                            <option value="Podkova" data-id="Podkova">Podkova</option>
                                            <option value="Poiret One" data-id="Poiret_One">Poiret One</option>
                                            <option value="Poller One" data-id="Poller_One">Poller One</option>
                                            <option value="Poly" data-id="Poly">Poly</option>
                                            <option value="Pompiere" data-id="Pompiere">Pompiere</option>
                                            <option value="Pontano Sans" data-id="Pontano_Sans">Pontano Sans</option>
                                            <option value="Poppins" data-id="Poppins">Poppins</option>
                                            <option value="Port Lligat Sans" data-id="Port_Lligat_Sans">Port Lligat Sans</option>
                                            <option value="Port Lligat Slab" data-id="Port_Lligat_Slab">Port Lligat Slab</option>
                                            <option value="Pragati Narrow" data-id="Pragati_Narrow">Pragati Narrow</option>
                                            <option value="Prata" data-id="Prata">Prata</option>
                                            <option value="Preahvihear" data-id="Preahvihear">Preahvihear</option>
                                            <option value="Press Start 2P" data-id="Press_Start_2P">Press Start 2P</option>
                                            <option value="Princess Sofia" data-id="Princess_Sofia">Princess Sofia</option>
                                            <option value="Prociono" data-id="Prociono">Prociono</option>
                                            <option value="Prosto One" data-id="Prosto_One">Prosto One</option>
                                            <option value="Puritan" data-id="Puritan">Puritan</option>
                                            <option value="Purple Purse" data-id="Purple_Purse">Purple Purse</option>
                                            <option value="Quando" data-id="Quando">Quando</option>
                                            <option value="Quantico" data-id="Quantico">Quantico</option>
                                            <option value="Quattrocento" data-id="Quattrocento">Quattrocento</option>
                                            <option value="Quattrocento Sans" data-id="Quattrocento_Sans">Quattrocento Sans</option>
                                            <option value="Questrial" data-id="Questrial">Questrial</option>
                                            <option value="Quicksand" data-id="Quicksand">Quicksand</option>
                                            <option value="Quintessential" data-id="Quintessential">Quintessential</option>
                                            <option value="Qwigley" data-id="Qwigley">Qwigley</option>
                                            <option value="Racing Sans One" data-id="Racing_Sans_One">Racing Sans One</option>
                                            <option value="Radley" data-id="Radley">Radley</option>
                                            <option value="Rajdhani" data-id="Rajdhani">Rajdhani</option>
                                            <option value="Raleway" data-id="Raleway">Raleway</option>
                                            <option value="Raleway Dots" data-id="Raleway_Dots">Raleway Dots</option>
                                            <option value="Ramabhadra" data-id="Ramabhadra">Ramabhadra</option>
                                            <option value="Ramaraja" data-id="Ramaraja">Ramaraja</option>
                                            <option value="Rambla" data-id="Rambla">Rambla</option>
                                            <option value="Rammetto One" data-id="Rammetto_One">Rammetto One</option>
                                            <option value="Ranchers" data-id="Ranchers">Ranchers</option>
                                            <option value="Rancho" data-id="Rancho">Rancho</option>
                                            <option value="Ranga" data-id="Ranga">Ranga</option>
                                            <option value="Rationale" data-id="Rationale">Rationale</option>
                                            <option value="Ravi Prakash" data-id="Ravi_Prakash">Ravi Prakash</option>
                                            <option value="Redressed" data-id="Redressed">Redressed</option>
                                            <option value="Reenie Beanie" data-id="Reenie_Beanie">Reenie Beanie</option>
                                            <option value="Revalia" data-id="Revalia">Revalia</option>
                                            <option value="Rhodium Libre" data-id="Rhodium_Libre">Rhodium Libre</option>
                                            <option value="Ribeye" data-id="Ribeye">Ribeye</option>
                                            <option value="Ribeye Marrow" data-id="Ribeye_Marrow">Ribeye Marrow</option>
                                            <option value="Righteous" data-id="Righteous">Righteous</option>
                                            <option value="Risque" data-id="Risque">Risque</option>
                                            <option value="Roboto" data-id="Roboto">Roboto</option>
                                            <option value="Roboto Condensed" data-id="Roboto_Condensed">Roboto Condensed</option>
                                            <option value="Roboto Mono" data-id="Roboto_Mono">Roboto Mono</option>
                                            <option value="Roboto Slab" data-id="Roboto_Slab" selected='selected'>Roboto Slab</option>
                                            <option value="Rochester" data-id="Rochester">Rochester</option>
                                            <option value="Rock Salt" data-id="Rock_Salt">Rock Salt</option>
                                            <option value="Rokkitt" data-id="Rokkitt">Rokkitt</option>
                                            <option value="Romanesco" data-id="Romanesco">Romanesco</option>
                                            <option value="Ropa Sans" data-id="Ropa_Sans">Ropa Sans</option>
                                            <option value="Rosario" data-id="Rosario">Rosario</option>
                                            <option value="Rosarivo" data-id="Rosarivo">Rosarivo</option>
                                            <option value="Rouge Script" data-id="Rouge_Script">Rouge Script</option>
                                            <option value="Rozha One" data-id="Rozha_One">Rozha One</option>
                                            <option value="Rubik" data-id="Rubik">Rubik</option>
                                            <option value="Rubik Mono One" data-id="Rubik_Mono_One">Rubik Mono One</option>
                                            <option value="Rubik One" data-id="Rubik_One">Rubik One</option>
                                            <option value="Ruda" data-id="Ruda">Ruda</option>
                                            <option value="Rufina" data-id="Rufina">Rufina</option>
                                            <option value="Ruge Boogie" data-id="Ruge_Boogie">Ruge Boogie</option>
                                            <option value="Ruluko" data-id="Ruluko">Ruluko</option>
                                            <option value="Rum Raisin" data-id="Rum_Raisin">Rum Raisin</option>
                                            <option value="Ruslan Display" data-id="Ruslan_Display">Ruslan Display</option>
                                            <option value="Russo One" data-id="Russo_One">Russo One</option>
                                            <option value="Ruthie" data-id="Ruthie">Ruthie</option>
                                            <option value="Rye" data-id="Rye">Rye</option>
                                            <option value="Sacramento" data-id="Sacramento">Sacramento</option>
                                            <option value="Sahitya" data-id="Sahitya">Sahitya</option>
                                            <option value="Sail" data-id="Sail">Sail</option>
                                            <option value="Salsa" data-id="Salsa">Salsa</option>
                                            <option value="Sanchez" data-id="Sanchez">Sanchez</option>
                                            <option value="Sancreek" data-id="Sancreek">Sancreek</option>
                                            <option value="Sansita One" data-id="Sansita_One">Sansita One</option>
                                            <option value="Sarala" data-id="Sarala">Sarala</option>
                                            <option value="Sarina" data-id="Sarina">Sarina</option>
                                            <option value="Sarpanch" data-id="Sarpanch">Sarpanch</option>
                                            <option value="Satisfy" data-id="Satisfy">Satisfy</option>
                                            <option value="Scada" data-id="Scada">Scada</option>
                                            <option value="Scheherazade" data-id="Scheherazade">Scheherazade</option>
                                            <option value="Schoolbell" data-id="Schoolbell">Schoolbell</option>
                                            <option value="Seaweed Script" data-id="Seaweed_Script">Seaweed Script</option>
                                            <option value="Sevillana" data-id="Sevillana">Sevillana</option>
                                            <option value="Seymour One" data-id="Seymour_One">Seymour One</option>
                                            <option value="Shadows Into Light" data-id="Shadows_Into_Light">Shadows Into Light</option>
                                            <option value="Shadows Into Light Two" data-id="Shadows_Into_Light_Two">Shadows Into Light Two</option>
                                            <option value="Shanti" data-id="Shanti">Shanti</option>
                                            <option value="Share" data-id="Share">Share</option>
                                            <option value="Share Tech" data-id="Share_Tech">Share Tech</option>
                                            <option value="Share Tech Mono" data-id="Share_Tech_Mono">Share Tech Mono</option>
                                            <option value="Shojumaru" data-id="Shojumaru">Shojumaru</option>
                                            <option value="Short Stack" data-id="Short_Stack">Short Stack</option>
                                            <option value="Siemreap" data-id="Siemreap">Siemreap</option>
                                            <option value="Sigmar One" data-id="Sigmar_One">Sigmar One</option>
                                            <option value="Signika" data-id="Signika">Signika</option>
                                            <option value="Signika Negative" data-id="Signika_Negative">Signika Negative</option>
                                            <option value="Simonetta" data-id="Simonetta">Simonetta</option>
                                            <option value="Sintony" data-id="Sintony">Sintony</option>
                                            <option value="Sirin Stencil" data-id="Sirin_Stencil">Sirin Stencil</option>
                                            <option value="Six Caps" data-id="Six_Caps">Six Caps</option>
                                            <option value="Skranji" data-id="Skranji">Skranji</option>
                                            <option value="Slabo 13px" data-id="Slabo_13px">Slabo 13px</option>
                                            <option value="Slabo 27px" data-id="Slabo_27px">Slabo 27px</option>
                                            <option value="Slackey" data-id="Slackey">Slackey</option>
                                            <option value="Smokum" data-id="Smokum">Smokum</option>
                                            <option value="Smythe" data-id="Smythe">Smythe</option>
                                            <option value="Sniglet" data-id="Sniglet">Sniglet</option>
                                            <option value="Snippet" data-id="Snippet">Snippet</option>
                                            <option value="Snowburst One" data-id="Snowburst_One">Snowburst One</option>
                                            <option value="Sofadi One" data-id="Sofadi_One">Sofadi One</option>
                                            <option value="Sofia" data-id="Sofia">Sofia</option>
                                            <option value="Sonsie One" data-id="Sonsie_One">Sonsie One</option>
                                            <option value="Sorts Mill Goudy" data-id="Sorts_Mill_Goudy">Sorts Mill Goudy</option>
                                            <option value="Source Code Pro" data-id="Source_Code_Pro">Source Code Pro</option>
                                            <option value="Source Sans Pro" data-id="Source_Sans_Pro">Source Sans Pro</option>
                                            <option value="Source Serif Pro" data-id="Source_Serif_Pro">Source Serif Pro</option>
                                            <option value="Special Elite" data-id="Special_Elite">Special Elite</option>
                                            <option value="Spicy Rice" data-id="Spicy_Rice">Spicy Rice</option>
                                            <option value="Spinnaker" data-id="Spinnaker">Spinnaker</option>
                                            <option value="Spirax" data-id="Spirax">Spirax</option>
                                            <option value="Squada One" data-id="Squada_One">Squada One</option>
                                            <option value="Sree Krushnadevaraya" data-id="Sree_Krushnadevaraya">Sree Krushnadevaraya</option>
                                            <option value="Stalemate" data-id="Stalemate">Stalemate</option>
                                            <option value="Stalinist One" data-id="Stalinist_One">Stalinist One</option>
                                            <option value="Stardos Stencil" data-id="Stardos_Stencil">Stardos Stencil</option>
                                            <option value="Stint Ultra Condensed" data-id="Stint_Ultra_Condensed">Stint Ultra Condensed</option>
                                            <option value="Stint Ultra Expanded" data-id="Stint_Ultra_Expanded">Stint Ultra Expanded</option>
                                            <option value="Stoke" data-id="Stoke">Stoke</option>
                                            <option value="Strait" data-id="Strait">Strait</option>
                                            <option value="Sue Ellen Francisco" data-id="Sue_Ellen_Francisco">Sue Ellen Francisco</option>
                                            <option value="Sumana" data-id="Sumana">Sumana</option>
                                            <option value="Sunshiney" data-id="Sunshiney">Sunshiney</option>
                                            <option value="Supermercado One" data-id="Supermercado_One">Supermercado One</option>
                                            <option value="Sura" data-id="Sura">Sura</option>
                                            <option value="Suranna" data-id="Suranna">Suranna</option>
                                            <option value="Suravaram" data-id="Suravaram">Suravaram</option>
                                            <option value="Suwannaphum" data-id="Suwannaphum">Suwannaphum</option>
                                            <option value="Swanky and Moo Moo" data-id="Swanky_and_Moo_Moo">Swanky and Moo Moo</option>
                                            <option value="Syncopate" data-id="Syncopate">Syncopate</option>
                                            <option value="Tangerine" data-id="Tangerine">Tangerine</option>
                                            <option value="Taprom" data-id="Taprom">Taprom</option>
                                            <option value="Tauri" data-id="Tauri">Tauri</option>
                                            <option value="Teko" data-id="Teko">Teko</option>
                                            <option value="Telex" data-id="Telex">Telex</option>
                                            <option value="Tenali Ramakrishna" data-id="Tenali_Ramakrishna">Tenali Ramakrishna</option>
                                            <option value="Tenor Sans" data-id="Tenor_Sans">Tenor Sans</option>
                                            <option value="Text Me One" data-id="Text_Me_One">Text Me One</option>
                                            <option value="The Girl Next Door" data-id="The_Girl_Next_Door">The Girl Next Door</option>
                                            <option value="Tienne" data-id="Tienne">Tienne</option>
                                            <option value="Tillana" data-id="Tillana">Tillana</option>
                                            <option value="Timmana" data-id="Timmana">Timmana</option>
                                            <option value="Tinos" data-id="Tinos">Tinos</option>
                                            <option value="Titan One" data-id="Titan_One">Titan One</option>
                                            <option value="Titillium Web" data-id="Titillium_Web">Titillium Web</option>
                                            <option value="Trade Winds" data-id="Trade_Winds">Trade Winds</option>
                                            <option value="Trocchi" data-id="Trocchi">Trocchi</option>
                                            <option value="Trochut" data-id="Trochut">Trochut</option>
                                            <option value="Trykker" data-id="Trykker">Trykker</option>
                                            <option value="Tulpen One" data-id="Tulpen_One">Tulpen One</option>
                                            <option value="Ubuntu" data-id="Ubuntu">Ubuntu</option>
                                            <option value="Ubuntu Condensed" data-id="Ubuntu_Condensed">Ubuntu Condensed</option>
                                            <option value="Ubuntu Mono" data-id="Ubuntu_Mono">Ubuntu Mono</option>
                                            <option value="Ultra" data-id="Ultra">Ultra</option>
                                            <option value="Uncial Antiqua" data-id="Uncial_Antiqua">Uncial Antiqua</option>
                                            <option value="Underdog" data-id="Underdog">Underdog</option>
                                            <option value="Unica One" data-id="Unica_One">Unica One</option>
                                            <option value="UnifrakturCook" data-id="UnifrakturCook">UnifrakturCook</option>
                                            <option value="UnifrakturMaguntia" data-id="UnifrakturMaguntia">UnifrakturMaguntia</option>
                                            <option value="Unkempt" data-id="Unkempt">Unkempt</option>
                                            <option value="Unlock" data-id="Unlock">Unlock</option>
                                            <option value="Unna" data-id="Unna">Unna</option>
                                            <option value="VT323" data-id="VT323">VT323</option>
                                            <option value="Vampiro One" data-id="Vampiro_One">Vampiro One</option>
                                            <option value="Varela" data-id="Varela">Varela</option>
                                            <option value="Varela Round" data-id="Varela_Round">Varela Round</option>
                                            <option value="Vast Shadow" data-id="Vast_Shadow">Vast Shadow</option>
                                            <option value="Vesper Libre" data-id="Vesper_Libre">Vesper Libre</option>
                                            <option value="Vibur" data-id="Vibur">Vibur</option>
                                            <option value="Vidaloka" data-id="Vidaloka">Vidaloka</option>
                                            <option value="Viga" data-id="Viga">Viga</option>
                                            <option value="Voces" data-id="Voces">Voces</option>
                                            <option value="Volkhov" data-id="Volkhov">Volkhov</option>
                                            <option value="Vollkorn" data-id="Vollkorn">Vollkorn</option>
                                            <option value="Voltaire" data-id="Voltaire">Voltaire</option>
                                            <option value="Waiting for the Sunrise" data-id="Waiting_for_the_Sunrise">Waiting for the Sunrise</option>
                                            <option value="Wallpoet" data-id="Wallpoet">Wallpoet</option>
                                            <option value="Walter Turncoat" data-id="Walter_Turncoat">Walter Turncoat</option>
                                            <option value="Warnes" data-id="Warnes">Warnes</option>
                                            <option value="Wellfleet" data-id="Wellfleet">Wellfleet</option>
                                            <option value="Wendy One" data-id="Wendy_One">Wendy One</option>
                                            <option value="Wire One" data-id="Wire_One">Wire One</option>
                                            <option value="Work Sans" data-id="Work_Sans">Work Sans</option>
                                            <option value="Yanone Kaffeesatz" data-id="Yanone_Kaffeesatz">Yanone Kaffeesatz</option>
                                            <option value="Yantramanav" data-id="Yantramanav">Yantramanav</option>
                                            <option value="Yellowtail" data-id="Yellowtail">Yellowtail</option>
                                            <option value="Yeseva One" data-id="Yeseva_One">Yeseva One</option>
                                            <option value="Yesteryear" data-id="Yesteryear">Yesteryear</option>
                                            <option value="Zeyada" data-id="Zeyada">Zeyada</option>
                                        </select>
                    </div>
                </div>
                <div class="input-box">
                    <a href="#" class="tp-btn tp-chameleon-clear">Clear</a>
                    <br class="clear_fix">
                </div>
            </div>
            <div class="input-box">
                <button class="tp-btn tp-chameleon-more-demo" data-text="More Demo" data-active="All Demo">More Demo</button>
                <br class="clear_fix">
            </div>
            <div class="tp_chameleon_demos">
            </div>
        </div>
        <div class="style-toggle close"></div>
        <div class="style-toggle open"></div>
    </div> -->


<div class="modal fade" id="program-details-modal" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Package Details</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4 form-group">
            <img src="assets/frontend/pages/img/works/course1.jpg" alt="Amazing Project" class="img-responsive image">
          </div>
          <div class="col-md-8 form-group">
            <h2><strong class="program-name">Course 1</strong></h2>
            <p class="text-justify description"></p>
          </div>
          <!-- <div class="row">
            <div class="col-md-4 form-group">
              <button class="btn btn blue"><i class="fa fa-inr"></i> 250</button>
              <button type="button" class="btn blue"> Buy Now </button>
            </div> -->
          <div class="col-md-8 col-md-offset-2 form-group">
            <h4><strong>Number of Tests</strong></h4>
            <div class="table-scrollable" id="modal-table">
              <table class="table table-striped table-hover table-bordered">
                <thead>
                  <tr>
                    <th>Test Topic</th>
                    <th>No. of Tests</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Test Topic 1</td>
                    <td>5</td>
                  </tr>

                </tbody>
              </table>
            </div>
          </div>
          <div class="col-md-2"></div>
        </div>
        <!-- </div> -->
      </div>
      <div class="modal-footer">
        <span class="pull-left price"><i class="fa fa-inr"></i> 250</span>
        <div class="pull-right">
          <!-- <span class="promocode">Have a Promocode?</span>
          <div class="promocode-div pull-left mrg-right15" style="display:none;">
            <input type="text" class="form-control input-small pull-left mrg-right15" placeholder="Enter Promocode">
            <button type="button" class="btn grey-cascade"> Apply </button>
          </div> -->
          <button type="button" class="btn grey-cascade buy-program"> Buy Now </button>
          <button type="button" class="btn default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

    <script type="text/javascript">
        var tp_chameleon_url_stylesheet = 'http://educationwp.thimpress.com/wp-content/themes/eduma/style-1.css';
        var tp_chameleon_url_admin_ajax = 'http://educationwp.thimpress.com/wp-admin/admin-ajax.php';
        var tp_chameleon_wp_nonce = '942217f09a';
        var tp_chameleon_primary_color = 'rgb(255, 182, 6)';
        var tp_chameleon_selector_wrapper_box = '.content-pusher';
        var tp_chameleon_class_boxed = 'boxed-area';
        var tp_chameleon_src_patterns = 'http://educationwp.thimpress.com/wp-content/plugins/tp-chameleon/images/patterns/';
        var tp_tp_chameleon_selectors_body_fonts = 'body';
        var tp_tp_chameleon_selectors_heading_fonts = 'h1, h2, h3, h4, h5, h6';
        var tp_chameleon_setting = {
            layout: tp_chameleon_getCookie('tp_chameleon_layout'),
            pattern_type: tp_chameleon_getCookie('tp_chameleon_pattern_type'),
            pattern_src: tp_chameleon_getCookie('tp_chameleon_pattern_src'),
            primary_color: tp_chameleon_getCookie('tp_chameleon_primary_color'),
            primary_color_rgb: tp_chameleon_getCookie('tp_chameleon_primary_color_rgb'),
            body_font: tp_chameleon_getCookie('tp_chameleon_body_font'),
            body_font_code: tp_chameleon_getCookie('tp_chameleon_body_font_code'),
            heading_font: tp_chameleon_getCookie('tp_chameleon_heading_font'),
            heading_font_code: tp_chameleon_getCookie('tp_chameleon_heading_font_code')
        };

        if (top !== self) top.location.replace(self.location.href);

        var tp_chameleon_demos = [{
            "data_dir": "\/var\/www\/educationwp.thimpress.com\/htdocs\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-01",
            "thumbnail_url": "http:\/\/educationwp.thimpress.com\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-01\/screenshot.jpg",
            "title": "Demo 01",
            "demo_url": "http:\/\/educationwp.thimpress.com"
        }, {
            "data_dir": "\/var\/www\/educationwp.thimpress.com\/htdocs\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-02",
            "thumbnail_url": "http:\/\/educationwp.thimpress.com\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-02\/screenshot.jpg",
            "title": "Demo 02",
            "demo_url": "http:\/\/educationwp.thimpress.com\/demo-2\/"
        }, {
            "data_dir": "\/var\/www\/educationwp.thimpress.com\/htdocs\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-03",
            "thumbnail_url": "http:\/\/educationwp.thimpress.com\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-03\/screenshot.jpg",
            "title": "Demo 03",
            "demo_url": "http:\/\/educationwp.thimpress.com\/demo-3\/"
        }, {
            "data_dir": "\/var\/www\/educationwp.thimpress.com\/htdocs\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-boxed",
            "thumbnail_url": "http:\/\/educationwp.thimpress.com\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-boxed\/screenshot.jpg",
            "title": "Demo Boxed",
            "demo_url": "http:\/\/educationwp.thimpress.com\/demo-boxed\/"
        }, {
            "data_dir": "\/var\/www\/educationwp.thimpress.com\/htdocs\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-rtl",
            "thumbnail_url": "http:\/\/educationwp.thimpress.com\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-rtl\/screenshot.jpg",
            "title": "Demo RTL",
            "demo_url": "http:\/\/educationwp.thimpress.com\/demo-rtl\/"
        }, {
            "data_dir": "\/var\/www\/educationwp.thimpress.com\/htdocs\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-one-course",
            "thumbnail_url": "http:\/\/educationwp.thimpress.com\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-one-course\/screenshot.jpg",
            "title": "Demo One Course",
            "demo_url": "http:\/\/educationwp.thimpress.com\/demo-one-course\/"
        }, {
            "data_dir": "\/var\/www\/educationwp.thimpress.com\/htdocs\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-one-instructor",
            "thumbnail_url": "http:\/\/educationwp.thimpress.com\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-one-instructor\/screenshot.jpg",
            "title": "Demo One Instructor",
            "demo_url": "http:\/\/educationwp.thimpress.com\/demo-one-instructor\/"
        }, {
            "data_dir": "\/var\/www\/educationwp.thimpress.com\/htdocs\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-university",
            "thumbnail_url": "http:\/\/educationwp.thimpress.com\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-university\/screenshot.jpg",
            "title": "Demo University 1",
            "demo_url": "http:\/\/educationwp.thimpress.com\/demo-university\/"
        }, {
            "data_dir": "\/var\/www\/educationwp.thimpress.com\/htdocs\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-university-2",
            "thumbnail_url": "http:\/\/educationwp.thimpress.com\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-university-2\/screenshot.jpg",
            "title": "Demo University 2",
            "demo_url": "http:\/\/educationwp.thimpress.com\/demo-university-2\/"
        }, {
            "data_dir": "\/var\/www\/educationwp.thimpress.com\/htdocs\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-languages-school",
            "thumbnail_url": "http:\/\/educationwp.thimpress.com\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-languages-school\/screenshot.jpg",
            "title": "Demo Languages School",
            "demo_url": "http:\/\/educationwp.thimpress.com\/demo-languages-school\/"
        }, {
            "data_dir": "\/var\/www\/educationwp.thimpress.com\/htdocs\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-courses-hub",
            "thumbnail_url": "http:\/\/educationwp.thimpress.com\/wp-content\/themes\/eduma\/inc\/admin\/data\/demo-courses-hub\/screenshot.jpg",
            "title": "Demo Courses Hub",
            "demo_url": "http:\/\/educationwp.thimpress.com\/demo-courses-hub\/"
        }, ];
        var tp_chameleon_site_url = '//educationwp.thimpress.com';

        function tp_chameleon_setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        }

        function tp_chameleon_deleteCookie(cname) {
            var d = new Date();
            d.setTime(d.getTime() + (24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=; " + expires;
        }

        function tp_chameleon_deleteAllCookie() {
            var all_cookie = [
                'tp_chameleon_layout',
                'tp_chameleon_pattern_type',
                'tp_chameleon_pattern_src',
                'tp_chameleon_primary_color',
                'tp_chameleon_primary_color_rgb',
                'tp_chameleon_body_font',
                'tp_chameleon_body_font_code',
                'tp_chameleon_heading_font',
                'tp_chameleon_heading_font_code'
            ];

            for (var i = 0; i < all_cookie.length; i++) {
                tp_chameleon_deleteCookie(all_cookie[i]);
            }
        }

        function tp_chameleon_getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
            }

            return '';
        }

        function tp_chameleon_set_first_visit() {
            tp_chameleon_setCookie('tp_chameleon_first_visit', 1, 1);
        }

        function tp_chameleon_check_first_visit() {
            return !(tp_chameleon_getCookie('tp_chameleon_first_visit') == '1');
        }

        function color2rgba(rgb) {
            if (rgb == 'transparent') {
                return 'rgba(0, 0, 0, 0)';
            }

            var pattern_rgb = /^(rgb\s*?\(\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?\))$/i;
            var check_rgb = pattern_rgb.test(rgb);

            var pattern_rgba = /^(rgba\s*?\(\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(0|0\.\d*|1|1.0*)\s*?\))$/i;

            var opacity = 1;
            if (check_rgb == true) {
                rgb = rgb.match(pattern_rgb);
            } else {
                rgb = rgb.match(pattern_rgba);
                opacity = parseFloat(rgb[5]);
            }

            var red = parseInt(rgb[2]);
            var green = parseInt(rgb[3]);
            var blue = parseInt(rgb[4]);

            return 'rgba(' + red + ', ' + green + ', ' + blue + ', ' + opacity + ')';
        }

        function color2rgb(rgb) {
            if (rgb == 'transparent') {
                return 'rgb(256, 256, 256)';
            }

            var pattern_rgb = /^(rgb\s*?\(\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?\))$/i;
            var check_rgb = pattern_rgb.test(rgb);

            var pattern_rgba = /^(rgba\s*?\(\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(0|0\.\d*|1|1.0*)\s*?\))$/i;

            if (check_rgb == true) {
                rgb = rgb.match(pattern_rgb);
            } else {
                rgb = rgb.match(pattern_rgba);
            }

            var red = parseInt(rgb[2]);
            var green = parseInt(rgb[3]);
            var blue = parseInt(rgb[4]);

            return 'rgb(' + red + ', ' + green + ', ' + blue + ')';
        }

        function get_opacity_color(rgb) {
            if (rgb == 'transparent') {
                return 0;
            }

            var pattern_rgb = /^(rgb\s*?\(\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?\))$/i;
            var check_rgb = pattern_rgb.test(rgb);

            var pattern_rgba = /^(rgba\s*?\(\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(0|0\.\d*|1|1.0*)\s*?\))$/i;

            var opacity = 1;
            if (check_rgb != true) {
                rgb = rgb.match(pattern_rgba);
                opacity = parseFloat(rgb[5]);
            }

            return opacity;
        }

        function rgb2rgba(rgb, opacity) {
            var pattern_rgb = /^(rgb\s*?\(\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?\))$/i;
            var check_rgb = pattern_rgb.test(rgb);

            var pattern_rgba = /^(rgba\s*?\(\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\s*?,\s*?(0|0\.\d*|1|1.0*)\s*?\))$/i;

            if (check_rgb == true) {
                rgb = rgb.match(pattern_rgb);
            } else {
                rgb = rgb.match(pattern_rgba);
                opacity = parseFloat(rgb[5]);
            }

            var red = parseInt(rgb[2]);
            var green = parseInt(rgb[3]);
            var blue = parseInt(rgb[4]);

            return 'rgba(' + red + ', ' + green + ', ' + blue + ', ' + opacity + ')';
        }

        jQuery(document).ready(function ($) {
            function tp_chameleon_show_demos() {
                var $demos = $('.tp_chameleon_demos');

                var html = '';

                for (var i = 0; i < tp_chameleon_demos.length; i++) {
                    var img_src = tp_chameleon_demos[i].thumbnail_url;
                    var demo_url = tp_chameleon_demos[i].demo_url;
                    var demo_title = tp_chameleon_demos[i].title;

                    console.log(demo_url.indexOf(tp_chameleon_site_url));
                    var e = '';
                    if (demo_url.indexOf(tp_chameleon_site_url) !== -1) {
                        e += '<div class="tp_demo active">';
                    } else {
                        e += '<div class="tp_demo">';
                    }

                    e += '<a href="' + demo_url + '" title="' + demo_title + '" target="_blank">' +
                        '<img width="400" height="300" src="' + img_src + '">' +
                        '<div class="sub_title">' + demo_title + '</div>' +
                        '</a>' +
                        '</div>';
                    html += e;
                }

                $demos.html(html);
                $demos.slideDown("slow", function () {
                    $('.tp-chameleon-more-demo').text($('.tp-chameleon-more-demo').attr('data-active'));
                    $('#tp_style_selector').focus();
                });
                var tp_height_customize = $('.tp_chameleon_customize').outerHeight(true) + 40;
                $('.tp_style_selector_container').animate({
                    scrollTop: tp_height_customize
                }, 1000);
            }

            function tp_chameleon_hide_demos() {
                var $demos = $('.tp_chameleon_demos');

                $demos.slideUp("slow", function () {
                    $('.tp-chameleon-more-demo').text($('.tp-chameleon-more-demo').attr('data-text'));
                });
            }

            function tp_chameleon_open() {
                tp_chameleon_set_first_visit();
                $('#tp_style_selector').animate({
                    right: '0px'
                }, 'medium');
                $('#tp_style_selector .open').hide();
                $('#tp_style_selector .close').show();
            }

            function tp_chameleon_close() {
                $('#tp_style_selector').animate({
                    right: '-300px'
                }, 'medium');
                $('#tp_style_selector .close').hide();
                $('#tp_style_selector .open').show();
            }


            function tp_change_background_pattern(url_pattern) {
                var $body = $('body');
                $body.removeClass('tp_background_image');
                $body.addClass('tp_background_pattern');
                $body.css('background-image', 'url("' + url_pattern + '")')
            }

            function tp_change_background_image(url_image) {
                var $body = $('body');
                $body.removeClass('tp_background_pattern');
                $body.addClass('tp_background_image');
                $body.css('background-image', 'url("' + url_image + '")')
            }

            function tp_chameleon_change_layout_wide() {
                tp_chameleon_setCookie('tp_chameleon_layout', 'wide', 1);

                var $body = $('body');
                $('.tp-change-layout').removeClass('active');
                $('.tp-change-layout.layout-wide').addClass('active');
                $('#tp_style_selector .boxed-mode').slideUp(300);
                $(tp_chameleon_selector_wrapper_box).removeClass(tp_chameleon_class_boxed);
                $body.css('background-image', 'none');
            }

            function tp_chameleon_change_layout_boxed() {
                tp_chameleon_setCookie('tp_chameleon_layout', 'boxed', 1);
                $('.tp-change-layout').removeClass('active');
                $('.tp-change-layout.layout-boxed').addClass('active');
                $('#tp_style_selector .boxed-mode').slideDown(300);
                $(tp_chameleon_selector_wrapper_box).addClass(tp_chameleon_class_boxed);
            }

            function tp_chameleon_change_primary_color(hex_color, rgb_color) {
                var new_color = hex_color;
                var x_new_color = rgb_color;

                $('#tp_style_selector .primary_color a').removeClass('active');
                $('#tp_style_selector .primary_color a[data-color="' + new_color + '"]').addClass('active');

                tp_chameleon_setCookie('tp_chameleon_primary_color', new_color, 1);
                tp_chameleon_setCookie('tp_chameleon_primary_color_rgb', x_new_color, 1);

                $('body .wrapper-container *').each(function (index) {
                    var color = $(this).css('color');
                    var opacity_color = get_opacity_color(color);
                    var color_rgba = rgb2rgba(x_new_color, opacity_color);

                    var background_color = $(this).css('background-color');
                    var opacity_background_color = get_opacity_color(background_color);
                    var background_color_rgba = rgb2rgba(x_new_color, opacity_background_color);

                    if ($(this).hasClass('tp_primary_background_color')) {
                        $(this).css('background-color', background_color_rgba);
                    } else if (color2rgb(background_color) == tp_chameleon_primary_color) {
                        $(this).css('background-color', background_color_rgba);
                        $(this).addClass('tp_primary_background_color');
                    }

                    if ($(this).hasClass('tp_primary_color')) {
                        $(this).css('color', color_rgba);
                    } else if (color2rgb(color) == tp_chameleon_primary_color) {
                        $(this).css('color', color_rgba);
                        $(this).addClass('tp_primary_color');
                    }
                });

                var $tp_stylesheet = $('#tp_chameleon_stylesheet');
                var tp_new_url_stylesheet = tp_chameleon_url_admin_ajax + '?action=tp_chameleon_get_stylesheet&new_color=' + new_color;
                $tp_stylesheet.attr('href', tp_new_url_stylesheet);
            }

            function tp_chameleon_change_body_font(current, current_code) {
                tp_chameleon_setCookie('tp_chameleon_body_font', current, 1);
                tp_chameleon_setCookie('tp_chameleon_body_font_code', current_code, 1);
                var style = tp_tp_chameleon_selectors_body_fonts + '{font-family:' + current + '}';

                var $font = $('#tp_chameleon_list_google_fonts #' + current_code);
                if ($font.length == 0) {
                    $('#tp_chameleon_list_google_fonts').append('<link id="' + current_code + '" href="//fonts.googleapis.com/css?family=' + current + '" type="text/css" rel="stylesheet">');
                }

                $('#tp_chameleon_body_font').html(style);
            }

            function tp_chameleon_change_heading_font(current, current_code) {
                tp_chameleon_setCookie('tp_chameleon_heading_font', current, 1);
                tp_chameleon_setCookie('tp_chameleon_heading_font_code', current_code, 1);
                var style = tp_tp_chameleon_selectors_heading_fonts + '{font-family:' + current + '!important}';

                var $font = $('#tp_chameleon_list_google_fonts #' + current_code);
                if ($font.length == 0) {
                    $('#tp_chameleon_list_google_fonts').append('<link id="' + current_code + '" href="//fonts.googleapis.com/css?family=' + current + '" type="text/css" rel="stylesheet">');
                }

                $('#tp_chameleon_heading_font').html(style);
            }

            function tp_chameleon_change_background_pattern(pattern_src) {
                tp_chameleon_setCookie('tp_chameleon_pattern_src', pattern_src, 1);
                tp_chameleon_setCookie('tp_chameleon_pattern_type', 'pattern', 1);
                var pattern_url = tp_chameleon_src_patterns + pattern_src;
                tp_change_background_pattern(pattern_url);
            }

            function tp_chameleon_change_background_image(pattern_src) {
                tp_chameleon_setCookie('tp_chameleon_pattern_src', pattern_src, 1);
                tp_chameleon_setCookie('tp_chameleon_pattern_type', 'image', 1);
                var pattern_url = tp_chameleon_src_patterns + pattern_src;
                tp_change_background_image(pattern_url);
            }

            var $body_font = '<style id="tp_chameleon_body_font" type="text/css"></style>';
            var $heading_font = '<style id="tp_chameleon_heading_font" type="text/css"></style>';
            var $stylesheet = '<link id="tp_chameleon_stylesheet" type="text/css" rel="stylesheet">';

            var $tp_head = $('head');
            $tp_head.append($stylesheet);

            var $tp_body = $('body');
            $tp_body.append($body_font);
            $tp_body.append($heading_font);

            if (tp_chameleon_setting.layout == 'wide') {
                tp_chameleon_change_layout_wide();
            }
            if (tp_chameleon_setting.layout == 'boxed') {
                tp_chameleon_change_layout_boxed();

                if (tp_chameleon_setting.pattern_type == 'pattern' && tp_chameleon_setting.pattern_src != '') {
                    tp_chameleon_change_background_pattern(tp_chameleon_setting.pattern_src);
                    $('.tp_pattern[data-src="' + tp_chameleon_setting.pattern_src + '"]').addClass('active');
                }

                if (tp_chameleon_setting.pattern_type == 'image' && tp_chameleon_setting.pattern_src != '') {
                    tp_chameleon_change_background_image(tp_chameleon_setting.pattern_src);
                    $('.tp_image[data-src="' + tp_chameleon_setting.pattern_src + '"]').addClass('active');
                }
            }

            if (tp_chameleon_setting.primary_color != '' && tp_chameleon_setting.primary_color_rgb != '') {
                tp_chameleon_change_primary_color(tp_chameleon_setting.primary_color, tp_chameleon_setting.primary_color_rgb);
            }

            if (tp_chameleon_setting.body_font != '') {
                tp_chameleon_change_body_font(tp_chameleon_setting.body_font, tp_chameleon_setting.body_font_code);
                $('#tp_style_selector select[name=body_font] option').removeAttr('selected');
                $font = $('#tp_style_selector select[name=body_font] option[value="' + tp_chameleon_setting.body_font + '"]');
                $font.attr('selected', 'selected');
            }

            if (tp_chameleon_setting.heading_font != '') {
                tp_chameleon_change_heading_font(tp_chameleon_setting.heading_font, tp_chameleon_setting.heading_font_code);
                $('#tp_style_selector select[name=heading_font] option').removeAttr('selected');
                $font = $('#tp_style_selector select[name=heading_font] option[value="' + tp_chameleon_setting.heading_font + '"]');
                $font.attr('selected', 'selected');
            }

            $('.tp-chameleon-more-demo').on('click', function (event) {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    tp_chameleon_hide_demos();
                } else {
                    $(this).addClass('active');
                    event.preventDefault();
                    tp_chameleon_show_demos();
                }
            });

            $('.tp-chameleon-clear').click(function (event) {
                event.preventDefault();
                tp_chameleon_deleteAllCookie();
                document.location.reload();
            });

            $('.tp-btn.tp-change-layout').click(function (event) {
                event.preventDefault();

                if ($(this).hasClass('layout-wide')) {
                    tp_chameleon_change_layout_wide();

                } else {
                    tp_chameleon_change_layout_boxed();

                }
            });

            $('.tp_pattern').click(function (event) {
                event.preventDefault();
                $('.tp_pattern').removeClass('active');
                $('.tp_image').removeClass('active');
                $(this).addClass('active');
                var pattern_src = $(this).attr('data-src');
                tp_chameleon_change_background_pattern(pattern_src);
            });

            $('.tp_image').click(function (event) {
                event.preventDefault();
                $('.tp_pattern').removeClass('active');
                $('.tp_image').removeClass('active');
                $(this).addClass('active');
                var pattern_src = $(this).attr('data-src');
                tp_chameleon_change_background_image(pattern_src);
            });

            /**
             * Select body font
             */
            $('#tp_style_selector select[name=body_font]').change(function () {
                var current = $(this).find('option:selected').val();
                var current_code = $(this).find('option:selected').attr('data-id');
                tp_chameleon_change_body_font(current, current_code);

                location.reload();
            });

            /**
             * Select heading font
             */
            $('#tp_style_selector select[name=heading_font]').change(function () {
                var current = $(this).find('option:selected').val();
                var current_code = $(this).find('option:selected').attr('data-id');
                tp_chameleon_change_heading_font(current, current_code);

                location.reload();
            });

            /**
             * Open/Close box
             */
            $('#tp_style_selector .close').click(function (e) {
                e.preventDefault();
                tp_chameleon_close();
            });

            $('#tp_style_selector .open').click(function (e) {
                e.preventDefault();
                tp_chameleon_open();
            });

            /**
             * Primary color
             */
            $('.primary_color a').click(function (event) {
                event.preventDefault();

                var x_new_color = $(this).css('background-color');
                var new_color = $(this).attr('data-color');
                tp_chameleon_change_primary_color(new_color, x_new_color);
            });

            /**
             * Check firt visit
             */
            //      if (tp_chameleon_check_first_visit()) {
            //          setTimeout(tp_chameleon_open, 10000);
            //      } else {
            //          $('#tp_style_selector').click(function (event) {
            //              tp_chameleon_set_first_visit();
            //          });
            //      }
        });
    </script>
    <div id="tp_chameleon_list_google_fonts"></div>
    <script data-cfasync="false" type="text/javascript">
        window.onload = function () {
            setTimeout(function () {
                var body = document.getElementById("thim-body"),
                    thim_preload = document.getElementById("preload"),
                    len = body.childNodes.length,
                    class_name = body.className.replace(/(?:^|\s)thim-body-preload(?!\S)/, '').replace(/(?:^|\s)thim-body-load-overlay(?!\S)/, '');

                body.className = class_name;
                if (typeof thim_preload !== 'undefined' && thim_preload !== null) {
                    for (var i = 0; i < len; i++) {
                        if (body.childNodes[i].id !== 'undefined' && body.childNodes[i].id == "preload") {
                            body.removeChild(body.childNodes[i]);
                            break;
                        }
                    }
                }
            }, 100);
        };
    </script>
    <script type="text/javascript">
        function revslider_showDoubleJqueryError(sliderID) {
            var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
            errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
            errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
            errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
            errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
            jQuery(sliderID).show().html(errorMessage);
        }
    </script>
    <script type='text/javascript' src='http://educationwp.thimpress.com/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var _wpcf7 = {
            "loaderUrl": "http:\/\/educationwp.thimpress.com\/wp-content\/themes\/eduma\/images\/ajax-loader.gif",
            "recaptchaEmpty": "Please verify that you are not a robot.",
            "sending": "Sending ...",
            "cached": 0
        };
        /* ]]> */
    </script>
    <!-- // <script type='text/javascript' src='http://educationwp.thimpress.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.4.1'></script>
    // <script type='text/javascript' src='http://educationwp.thimpress.com/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
    // <script type='text/javascript' src='http://educationwp.thimpress.com/wp-includes/js/backbone.min.js?ver=1.2.3'></script>
    // <script type='text/javascript' src='http://educationwp.thimpress.com/wp-content/plugins/learnpress/assets/js/frontend/course-lesson.js?ver=1.0.3'></script>
    // <script type='text/javascript' src='http://educationwp.thimpress.com/wp-content/plugins/learnpress/assets/js/jquery.alert.js?ver=1.0.3'></script>
    // <script type='text/javascript' src='http://educationwp.thimpress.com/wp-content/plugins/learnpress/assets/js/global.js?ver=1.0.3'></script>
    // <script type='text/javascript' src='http://educationwp.thimpress.com/wp-content/themes/eduma/assets/js/main.min.js?ver=2.2.0.7'></script> -->
     <script type='text/javascript'>
        /* <![CDATA[ */
        var thim_placeholder = {
            "login": "Username",
            "password": "Password"
        };
        /* ]]> */
    </script>
     <!-- <script type='text/javascript' src='http://educationwp.thimpress.com/wp-content/themes/eduma/assets/js/custom-script-v1.min.js?ver=2.2.0.7'></script>
    // <script type='text/javascript' src='http://educationwp.thimpress.com/wp-includes/js/wp-embed.min.js?ver=4.5.1'></script> -->
    <script type='text/javascript'>
        /* <![CDATA[ */
        var panelsStyles = {
            "fullContainer": "body"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='http://educationwp.thimpress.com/wp-content/plugins/siteorigin-panels/js/styling-24.min.js?ver=2.4.6'></script>
    <script type='text/javascript' src='http://educationwp.thimpress.com/wp-content/plugins/siteorigin-panels/js/jquery.stellar.min.js?ver=2.4.6'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var mc4wp_forms_config = [];
        /* ]]> */
    </script>
    <!-- // <script type='text/javascript' src='http://educationwp.thimpress.com/wp-content/plugins/mailchimp-for-wp/assets/js/forms-api.min.js?ver=3.1.6'></script> -->
    <script type="text/javascript">
        (function () {
            function addEventListener(element, event, handler) {
                if (element.addEventListener) {
                    element.addEventListener(event, handler, false);
                } else if (element.attachEvent) {
                    element.attachEvent('on' + event, handler);
                }
            }
        })();
    </script>
    <div class="tp_chameleon_overlay">
        <div class="tp_chameleon_progress">
            <div class="tp_chameleon_heading">Processing!</div>
        </div>
    </div>    

    <script src="assets/global/plugins/bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <script type='text/javascript' src='assets/js/common.js'></script>
    <script type='text/javascript' src='assets/js/custom/index1.js'></script>

</body>

</html>