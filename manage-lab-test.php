<?php include 'Access-API.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
    <?php include('html/head-tag.php'); ?>
    <?php include('html/student/head-tag.php'); ?>
    <!-- <link href="admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"> -->

    
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li><a href="#">Institute</a></li>
            <li class="active">Manage Lab Tests</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          <div class="sidebar col-md-3 col-sm-3">
            <?php include('html/student/sidebar.php'); ?>
          </div>
          <!-- END SIDEBAR -->

          <!-- BEGIN CONTENT -->
          <div class="col-md-9 col-sm-9">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h2>
                        Manage Lab Tests                        
                    </h2>
                </div>
                <div class="col-md-6 col-sm-6"></div>
            </div>
            <div class="row mrg-bot20">
                <div class="col-md-12">
                    <h5>Package</h5>
                    <select id="test-programs" class="form-control">
                        <option value="0">Select Package</option>
                    </select>

                    <div class="margin-top-10 margin-bottom-10 table-scrollable">
                        <table id="tests-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        S. No.
                                    </th>
                                    <th>
                                        Test Paper Name
                                    </th>
                                    <th>
                                        Type
                                    </th>                                    
                                    <th>
                                        Labs
                                    </th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>
    <div class="modal fade" id="add-student" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Students</h4>
                </div>
                <form id="add-student-form" role="form">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">First Name</label><span class="required">* </span>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="text" class="fname form-control" placeholder="First Name">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Last Name</label>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="text" class="lname form-control" placeholder="Last Name">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Email<span class="required">* </span></label>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="email" class="student_email form-control" placeholder="example@demo.com" required pattern="[a-zA-Z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-zA-Z]{2,3}$">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Mobile<span class="required">* </span></label>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="text" class="student_mobile form-control" placeholder="Mobile Number" required pattern="[0-9]{10,11}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Test Program<span class="required">* </span></label>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <select id="test-programs" class="form-control">
                                                <option value="0">Select Program</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Total no of Paper<span class="required">* </span></label>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="number" min="1" value="1" class="totalpaper form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">In Lab<span class="required">* </span></label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="number" min="1" value="1" class="inlab form-control">
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Expiry Date<span class="required">* </span></label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="text" id="expiry" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <div class="modal-footer">
                            <button type="submit" class="btn blue">Add</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form> 
            </div>
        </div>
    </div>
    
    <!-- BEGIN PRE-FOOTER -->
    <?php include('html/footer.php'); ?>
    <!-- END FOOTER -->
    
    <!-- /modal -->
    <!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
    <?php include('html/student/js-files.php'); ?>
    
    <script src="assets/js/custom/institute/manage-lab-test.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->

<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>