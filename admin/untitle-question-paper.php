<?php include 'Access-API-sup-sme.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<!-- <title>AIETS - Test Engine</title> -->
	<?php include 'html/general/headtags.php'; ?>
	<style>
		.modal {
		    z-index: 9050;
		}
	</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square">
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php';?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php';?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="name page-title">
			Untitle Question Paper
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="test_paper.php">Test Paper</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="add-test-paper.php">Add Test Paper</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">View Question Paper</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-file-text-o"></i><span class="name"> Untitle Question Paper</span>
							</div>
							<div class="pull-right">
								<a href="#print-paper-modal" data-toggle="modal" class="btn blue"><i class="fa fa-print"></i></a>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form">
								<div id="sections" class="form-body"></div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-12">

											<a id="show-save-test-paper-modal" class="btn green"><i class="fa fa-save"></i> &nbsp; Save Test Paper</a>
											<!-- <a href="#print-paper-modal" data-toggle="modal" class="btn blue"><i class="fa fa-print"></i></a> -->
											<button id="cancel" class="btn default"> <i class="fa fa-times-circle"></i> &nbsp; Cancel</button>

										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php';?>
<!-- END FOOTER -->

<!-- Modal -->
	<div id="question-details-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title control-label"><strong>Infomation</strong></h4>
				</div>
				<div class="modal-body">
					<ul class="list-group">
						<li class="list-group-item">
							 Class : <span id="class-li"></span>
						</li>
						<li class="list-group-item">
							 Subject : <span id="subject-li"></span>
						</li>
						<li class="list-group-item">
							 Unit : <span id="unit-li"></span>
						</li>
						<li class="list-group-item">
							 Chapter : <span id="chapter-li"></span>
						</li>
						<li class="list-group-item">
							 Topic : <span id="topic-li"></span>
						</li>
						<li class="list-group-item">
							 Difficulty Level : <span id="difficulty-li"></span>
						</li>
						<li class="list-group-item">
							 Skill : <span id="skill-li"></span>
						</li>
						<li class="list-group-item">
							 Tags : <span id="tag-li"></span>
						</li>
					</ul>
				</div>
				<div class="modal-footer">
					<button data-dismiss="modal" class="btn green">OK</button>
				</div>
			</div>
		</div>
	</div>
	<div id="import-question-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title control-label"><strong>Import Question</strong></h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-1 col-xs-2">
							<div class="checkbox-list">
								<label><input type="checkbox"></label>
							</div>
						</div>
						<div class="col-md-11 col-xs-10">
							<strong>Q.1 </strong>Look at this series: 2, 1, (1/2), (1/4), ... What number Should come next ?
							<div class="row">
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>A. </strong></span>&nbsp; (1/3)</p>
								</div>
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>B. </strong></span>&nbsp; (1/8)</p>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>C. </strong></span>&nbsp; (2/8)</p>
								</div>
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>D. </strong></span>&nbsp; (2/16)</p>
								</div>
							</div>
						</div>
					</div><hr/>
					<div class="row">
						<div class="col-md-1 col-xs-2">
							<div class="checkbox-list">
								<label><input type="checkbox"></label>
							</div>
						</div>
						<div class="col-md-11 col-xs-10">
							<strong>Q.2 </strong>Look at this series: 7, 10, 8, 11, 9, 12, ... What number Should come next ?
							<div class="row">
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>A. </strong></span>&nbsp; 7</p>
								</div>
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>B. </strong></span>&nbsp; 10</p>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>C. </strong></span>&nbsp; 12</p>
								</div>
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>D. </strong></span>&nbsp; 13</p>
								</div>
							</div>
						</div>
					</div><hr/>
					<div class="row">
						<div class="col-md-1 col-xs-2">
							<div class="checkbox-list">
								<label><input type="checkbox"></label>
							</div>
						</div>
						<div class="col-md-11 col-xs-10">
							<strong>Q.3 </strong>Look at this series: 36, 34, 30, 28, 24, ... What number Should come next ?
							<div class="row">
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>A.aa </strong></span>&nbsp; 20</p>
								</div>
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>B.aa </strong></span>&nbsp; 22</p>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>C.aa </strong></span>&nbsp; 23</p>
								</div>
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>D. aa</strong></span>&nbsp; 26</p>
								</div>
							</div>
						</div>
					</div><hr/>
					<div class="row">
						<div class="col-md-1 col-xs-2">
							<div class="checkbox-list">
								<label><input type="checkbox"></label>
							</div>
						</div>
						<div class="col-md-11 col-xs-10">
							<strong>Q.4 </strong>Look at this series: 22, 21, 23, 22, 24, 23, ... What number Should come next ?
							<div class="row">
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>A. </strong></span>&nbsp; 22</p>
								</div>
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>B. </strong></span>&nbsp; 24</p>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>C. </strong></span>&nbsp; 25</p>
								</div>
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>D. </strong></span>&nbsp; 26</p>
								</div>
							</div>
						</div>
					</div><hr/>
					<div class="row">
						<div class="col-md-1 col-xs-2">
							<div class="checkbox-list">
								<label><input type="checkbox"></label>
							</div>
						</div>
						<div class="col-md-11 col-xs-10">
							<strong>Q.5 </strong>Look at this series: 7, 10, 8, 11, 9, 12, ... What number Should come next ?
							<div class="row">
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>A. </strong></span>&nbsp; 7</p>
								</div>
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>B. </strong></span>&nbsp; 10</p>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>C. </strong></span>&nbsp; 12</p>
								</div>
								<div class="col-md-6 col-xs-6">
									<p><span class="text-info"><strong>D. </strong></span>&nbsp; 13</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="insertQuestion" class="btn green"><i class="fa fa-save"></i> &nbsp; Import</button>
					<button class="btn default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle"></i> &nbsp; Cancel</button>
				</div>
			</div>
		</div>.
	</div>
	<div id="save-test-paper-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title control-label"><strong>Test Paper</strong></h4>
				</div>
				<div class="modal-body">
					<div class="portlet-body form">
						<form class="form-horizontal" role="form">
							<div class="form-body">
								<div class="form-group">
									<label class="col-md-3 control-label">Name:</label>
									<div class="col-md-9">
										<input id="test-paper-name" type="text" class="form-control" placeholder="Test Paper">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Type:</label>
									<div class="col-md-9">
										<select id="test-paper-type" class="form-control">
											<option value="0">Select Type</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Class:</label>
									<div class="col-md-9">
										<select id="test-paper-class" class="form-control">
											<option value="0">Select Class</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Subject:</label>
									<div class="col-md-9">
										<select id="test-paper-subject" multiple="multiple" class="form-control">
											<option value="0">Select Subject</option>
										</select>
									</div>
								</div>

								<!-- start -->
								<div id="test-time" class="row">
									<div class="col-md-6">
										<div class="form-group">
											<div class="col-md-12">
												<label class="control-label">Start Date / Time</label>
											</div>
											<div class="col-md-12">
												<div class="input-group date">
													<input id="test-paper-start" type="text" class="form-control" placeholder="Start Date/Time">
								                    <span class="input-group-addon">
								                        <span class="glyphicon glyphicon-calendar"></span>
								                    </span>
								                </div>
							                </div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<div class="col-md-12">
												<label class="control-label">End Date / Time</label>
											</div>
											<div class="col-md-12">
												<div class="input-group date">
													<input id="test-paper-end" type="text" class="form-control" placeholder="End Date/Time">
													<span class="input-group-addon">
								                        <span class="glyphicon glyphicon-calendar"></span>
								                    </span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- end -->

								<div id="marks-time-div">

									<div class="form-group">
										<label class="col-md-3 control-label">Time(In Minutes):</label>
										<div class="col-md-9">
											<input id="test-paper-time" type="number" min="0" class="form-control" placeholder="Time ( In Minutes )" value="0">
										</div>
									</div>
									<div class="form-group" id="attempts">
										<label class="col-md-3 control-label">Total Attempts</label>
										<div class="col-md-9">
											<input id="test-paper-attempts" type="number" class="form-control" placeholder="No of Attempts">
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-3 control-label">Instructions</label>
										<div class="col-md-9">
										<!-- <textarea id="test-paper-instructions" class="form-control" rows="3" style="margin: 0px -6.625px 0px 0px; width: 100%; height: 150px;" placeholder="Enter Instructions for the test paper"></textarea> -->
										<div id="test-paper-instructions" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); min-height: 70px; position: relative;" data-ng-model="myModel"><p></p></div>

										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="modal-footer">
					<button id="save-test-paper" class="btn green pull-left"><i class="fa fa-save"></i> &nbsp; Save</button>
					<button class="btn default pull-left" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle"></i> &nbsp; Cancel</button>
				</div>
			</div>
		</div>
	</div>
<!-- modal Print -->
<div class="modal fade" id="print-paper-modal" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Print Untitle Question Paper</h4>
			</div>
			<div class="modal-body">
				 <div class="form-group">
					<div class="radio-list">
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<label class="radio-inline">
								<input type="radio" name="print-paper-radio" value="without-answer"> Print without answer </label>
							</div>
							<div class="col-md-6 col-sm-6">
								<label class="radio-inline">
								<input type="radio" name="print-paper-radio" value="with-answer"> Print with answer </label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<button id="print-paper" type="button" class="btn blue"><i class="fa fa-print"></i> &nbsp; Print</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal Print -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/ckeditor/ckeditor.js"></script>


<script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-markdown/lib/markdown.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<script src="js/custom/untitle-question-paper.js" type="text/javascript"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features
});
</script>
<script type="text/javascript">
  $(function () {
		$('#sidebar li').removeClass('active open');
		$('#sidebar li:eq(3)').addClass('active open');
		$('#program li').removeClass('active');
		$('#program li:eq(1)').addClass('active');
		mathsjax();
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>