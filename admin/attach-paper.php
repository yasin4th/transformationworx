<?php include 'Access-API-sup-sme.php'; ?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.1
Version: 3.6
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<!-- <title>AIETS - Test Engine</title> -->
	<?php include 'html/general/headtags.php'; ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square"> 
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php';?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php';?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="qcr-illustration-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<form id="test-program-illustration" role="form" action="../vendor/fileUploads.php" method="post" enctype="multipart/form-data">
									<div class="form-group">
											
										<div class="col-lg-6">
											<input type="text" name="illustration-name" id="illustration-name" class="form-control" placeholder="Name" />								
										</div>
										
										<div class="col-lg-6">
											<span class="btn btn-info btn-file">
												Choose File
												<input type="file" name="illustration-file" id="illustration-file" class="form-control" />
											</span>
										</div>

										<input type="hidden" name="action" id="action" value="add_illustration_file" />
										<input type="hidden" name="program_id" id="program_id" />
										<input type="hidden" name="chapter_id" id="chapter_id" />
									</div>
								</form>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" id="upload-illustration-file" class="btn blue"> Upload </button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			<span class="test-program"></span>
			<!-- <a id="show-edit-test-program-modal" class="EntranceExam"><i class="fa fa-edit"></i></a> -->
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="test-program.php">Test Program</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a class="test-program">Program Name</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a class="subject-navigation" href="view-test-program.php">Subject</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a class="chapter">Chapter</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			
			<!-- BEGIN DASHBOARD STATS -->
			<div class="row VTP-right-side">
				<!-- 
					<div id="diagnostic" class="col-lg-12 col-md-4 col-sm-6 col-xs-12">
						<div class="dashboard-stat blue-madison">
							<div class="visual">
								<i class="fa fa-comments"></i>
							</div>
							<div class="details">
								<div class="number">
									 0
								</div>
								<div class="desc">
									 Diagnostic
								</div>
							</div>
							<div class="more-details">
								<a class="view-test color-white" href="#">View Test</a>
								<a data-type="1" class="add-test-paper margin-top-3 pull-right"><i class="fa fa-plus-circle m-icon-white"></i></a>
							</div>
						</div>
					</div>
					<div id="full-syllabus-test" class="col-lg-12 col-md-4 col-sm-6 col-xs-12">
						<div class="dashboard-stat red-intense">
							<div class="visual">
								<i class="fa fa-globe"></i>
							</div>
							<div class="details">
								<div class="number">
									 0
								</div>
								<div class="desc">
									 Full syllabus Test
								</div>
							</div>
							<div class="more-details">
								<a class="view-test color-white" href="#">View Test</a>
								<a data-type="4" class="add-test-paper margin-top-3 pull-right"><i class="fa fa-plus-circle m-icon-white"></i></a>

							</div>
						</div>
					</div>
					<div id="scheduled-test" class="col-lg-12 col-md-4 col-sm-6 col-xs-12">
						<div class="dashboard-stat green-haze">
							<div class="visual">
								<i class="fa fa-comments"></i>
							</div>
							<div class="details">
								<div class="number">
									 0
								</div>
								<div class="desc">
									 Scheduled test
								</div>
							</div>
							<div class="more-details">
								<a class="view-test color-white" href="#">View Test</a>
								<a data-type="5" class="add-test-paper margin-top-3 pull-right"><i class="fa fa-plus-circle m-icon-white"></i></a>
							</div>
						</div>
					</div>
				 -->

			 
				<div id="illustration" class="col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat red-intense">
						<div class="visual">
							<!-- <i class="fa fa-bar-chart-o"></i> -->
						</div>
						<div class="details">
							<div class="desc">
								<span class="count"> 0</span>
								<a id="view-illustration" class="color-white" href="#" title="QCR & Illustration">QCR & Illustration</a>
							</div>
						</div>
						<div class="VTP-add-test">
							<a href="#qcr-illustration-modal" data-toggle="modal"> Add File </a>
						</div>
						<!-- <div class="more-details">
							<a class="view-test color-white" href="#">View Test</a>
							<a data-type="2" class="add-test-paper margin-top-3 pull-right"><i class="fa fa-plus-circle m-icon-white"></i></a>

						</div> -->
					</div>
				</div>
				<div id="chapter-test" class="col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat green-haze">
						<div class="visual">
							<!-- <i class="fa fa-shopping-cart"></i> -->
						</div>
						<div class="details">
							<div class="desc">
								<span class="count"> 0</span>
								<a class="view-test color-white" href="#" title="Chapter Test">Chapter Test</a>
							</div>
						</div>
						<div class="VTP-add-test">
							<a data-type="3" class="add-test-paper margin-top-3" title="Add Test">Add Test</a>
						</div>
						<!-- <div class="more-details">
							<a class="view-test color-white" href="#">View Test</a>
							<a data-type="3" class="add-test-paper margin-top-3 pull-right"><i class="fa fa-plus-circle m-icon-white"></i></a>

						</div> -->
					</div>
				</div>
				<div id="practice-test" class="col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat purple-plum">
						<div class="visual">
							<!-- <i class="fa fa-globe"></i> -->
						</div>
						<div class="details">
							<div class="desc">
								<span class="count"> 0</span>
								<a class="view-test color-white" href="#" title="Practice Test">Practice Test</a>
							</div>
						</div>
						<div class="VTP-add-test">
							<a data-type="6" class="add-test-paper margin-top-3" title="Add Test">Add Test</a>
						</div>
						<!-- <div class="more-details">
							<a class="view-test color-white" href="#">View Test</a>
							<a data-type="6" class="add-test-paper margin-top-3 pull-right"><i class="fa fa-plus-circle m-icon-white"></i></a>
						</div> -->
					</div>
				</div>
				<div class="col-md-3 col-sm-6"></div>
			
			
			</div>
			
		</div>
	</div>
			
			
	<!-- END PAGE CONTENT-->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php';?>
<!-- END FOOTER -->

	<div id="test-paper-by-type-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title control-label">Add Test</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<a href="add-test-paper.php" class="btn green">Set New Test Paper</a>
						</div>
						<div class="col-md-6">
							<a id="saveAttachedTestPaper" class="btn green pull-right"><i class="fa fa-save"></i> &nbsp; Save</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							
							<div class="margin-top-10 margin-bottom-10">
								<label>Import From Existing Test Paper</label>
								<div class="table-scrollable">
									<table id="test-paper-by-type-modal-table" class="table table-bordered table-striped table-hover">
									<thead>
									<tr>
										<th>
											 <i class="fa fa-caret-down"></i> Name
										</th>
									<!-- 	<th>
											 <i class="fa fa-caret-down"></i> Type
										</th>
										<th>
											 <i class="fa fa-caret-down"></i> Select
										</th> -->
									</tr>
									</thead>
									<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal myModal12 -->
	<!-- .modal large -->
	<div class="modal fade bs-modal-lg" id="edit-test-program-modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Test Program</h4>
				</div>
				<div class="modal-body">
					 <form id="form-add-test-program" class="form-horizontal" role="form">
						<div class="form-body">
							<div class="form-group">
								<label class="col-md-3 control-label">Name</label>
								<div class="col-md-6">
									<input id="test-program-name" type="text" class="form-control" placeholder="Test Program Name">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label"> Status </label>
								<div class="col-md-6">
									<select id="test-program-status" class="form-control">
										<option value="0"> Not For Sale </option>
										<option value="1"> For Sale </option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Class</label>
								<div class="col-md-6">
									<select id="test-program-class" class="form-control" placeholder="Related class for the Test Program">
										<option value="0"> Select Test program </option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Subjects</label>
								<div class="col-md-6">
									<select id="test-program-subjects" multiple="multiple" class="form-control" placeholder="Related Subjects for the Test Program"></select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Price</label>
								<div class="col-md-6">
									<input id="test-program-price" type="text" class="form-control" placeholder="Price for the Test Program (CAD)">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Description</label>
								<div class="col-md-8">
									<div id="test-program-description" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); min-height: 70px; position: relative;" data-ng-model="myModel"><p> Please Fill Instructions... </p></div>
									<!-- <textarea id="test-program-description" class="form-control" rows="3" placeholder="Enter Description here"></textarea> -->
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<a id="update_program" class="btn blue"><i class="fa fa-save"></i> &nbsp; Save</a>
					<button class="btn default" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal large -->
	
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
<script src="assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
<script src="assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-markdown/lib/markdown.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script src="../assets/global/plugins/jquery.form.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<script src="js/custom/attach-paper.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features 
   Index.init();   
   Index.initDashboardDaterange();
   Index.initJQVMAP(); // init index page's custom scripts
   Index.initCalendar(); // init index page's custom scripts
   Index.initCharts(); // init index page's custom scripts
   Index.initChat();
   Index.initMiniCharts();
   Tasks.initDashboardWidget();
});
</script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(3)').addClass('active open');
  });
  $(function () {
	  $('#program li').removeClass('active');
	  $('#program li:eq(0)').addClass('active');
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>