<title>Transformationworx | admin</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/ media="all">
<link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/ media="all">
<link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/ media="all">
<link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/ media="all">
<link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/ media="all">
<link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/ media="all">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/ media="all">
<!-- <link href="assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/ media="all"> -->
<!-- <link href="assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/ media="all"> -->

<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-select/bootstrap-select.min.css"/ media="all">
<link rel="stylesheet" type="text/css" href="assets/global/plugins/jquery-multi-select/css/multi-select.css"/ media="all">
<!-- END PAGE LEVEL PLUGIN STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/jstree/dist/themes/default/style.min.css"/ media="all">

<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/ media="all">
<link rel="stylesheet" type="text/css" href="assets/global/css/select2-bootstrap.min.css"/ media="all">
<!-- <link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/ media="all"> -->


<link href="assets/admin/pages/css/login3.css" rel="stylesheet" type="text/css"/ media="all">
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN PLUGINS USED BY X-EDITABLE -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/ media="all">
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/ media="all">
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/ media="all">
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/ media="all">
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css"/ media="all">
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-editable/inputs-ext/address/address.css"/ media="all">
<!-- END PLUGINS USED BY X-EDITABLE -->
<!-- BEGIN PAGE STYLES -->
<link href="assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/ media="all">
<!-- END PAGE STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" media="all">
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-summernote/summernote.css" media="all">
<link href="assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/ media="all">
<link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/ media="all">
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
<link href="assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/ media="all">
<link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/ media="all">
<link href="assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/ media="all">
<link href="assets/admin/layout/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/ media="all">
<link id="style_color" href="assets/admin/layout/css/themes/light2.css" rel="stylesheet" type="text/css"/ media="all">
<link rel="stylesheet" type="text/css" href="assets/global/plugins/toastr/toastr.min.css"/ media="all">
<link href="assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/ media="all">
<!-- END THEME STYLES -->
<!-- <link rel="shortcut icon" href="../favicon.ico"/ media="all"> -->
<!--  Begin Style  -->
<style>
	.required {
	color: #e02222;
	font-size: 12px;
	}
</style>
<!--  End Style  -->