<?php 
	@session_start();
    if(isset($_SESSION['role']))
    {       
        switch ($_SESSION['role']) {
            case '1':
                include "super-admin-sidebar.php";
                break;
            
            case '2':
                include 'sme-sidebar.php';
                break;
            
            default:
                include "dtp-sidebar.php";
                break;
        }
    }
?>