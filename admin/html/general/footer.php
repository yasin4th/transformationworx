<div id="loader" style="width: 100%; height: 100%; position: fixed; top: 0px; left: 0px; z-index: 10000; display: none; background: rgba(0, 0, 0, 0.309804);">
	<img src="assets/global/images/ajax-loader.gif" style="position: fixed; 
	top: 50%;
	left: 50%;">
</div>
<div class="page-footer">
	<div class="page-footer-inner">
		 <p>2017 &copy; Transformationworx. All Rights Reserved.</p>
		 <p>Powered by - <a href="http://dltlabs.ca" target="_blank">Transformationworx</a></p>
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
