<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
		<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
		<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
		<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<ul id="sidebar" class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
			<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
			<li class="sidebar-toggler-wrapper">
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler">
				</div>
				<!-- END SIDEBAR TOGGLER BUTTON -->
			</li>
			<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
			<li class="sidebar-search-wrapper">
				<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
				<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
				<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
				<form class="sidebar-search " action="extra_search.html" method="POST">
					<a href="javascript:;" class="remove">
					<i class="icon-close"></i>
					</a>
				</form>
				<!-- END RESPONSIVE QUICK SEARCH FORM -->
			</li>
			<li class="start active open">
				<a href="dashboard.php">
				<i class="icon-home"></i>
				<span class="title">Dashboard</span>
				<span class="selected"></span>
				</a>
			</li>
			<li>
				<a href="javascript:;">
				<i class="icon-basket"></i>
				<span class="title">Test Program</span>
				<span class="arrow "></span>
				</a>
				<ul id="program" class="sub-menu">
					<li>
						<a href="test-program.php">
						<i class="fa fa-file-text-o"></i>
						Test Package</a>
					</li>
					<li>
						<a href="test_paper.php">
						<i class="fa fa-file-text-o"></i>
						Test Paper</a>
					</li>
					<li>
						<a href="question-bank.php">
						<i class="fa fa-question-circle"></i>
						Question Bank</a>
					</li>
					<!-- <li>
						<a href="question-bank2.php">
						<i class="fa fa-question-circle"></i>
						Question Bank 2</a>
					</li> -->
				</ul>
			</li>
			<li>
				<a href="manage-student.php">
				<i class="fa fa-group"></i>
				<span class="title">Manage Students</span>
				</a>
			</li>
			<li>
				<a href="javascript:;">
				<i class="fa fa-suitcase"></i>
				<span class="title">Master Data</span>
				<span class="arrow "></span>
				</a>
				<ul id="data" class="sub-menu">
					<li>
						<a href="class.php">
						<i class="icon-home"></i>
						Class / Exam</a>
					</li>
					<li>
						<a href="batch.php">
						<i class="icon-home"></i>
						Batch</a>
					</li>
					<li>
						<a href="tag.php">
						<i class="icon-tag"></i>
						Tags</a>
					</li>
					<li>
						<a href="skills.php">
						<i class="fa fa-book"></i>
						Skills</a>
					</li>
					<li>
						<a href="difficulty-level.php">
						<i class="fa fa-bolt"></i>
						Difficulty Level</a>
					</li>
					<li>
						<a href="category.php">
						<i class="fa fa-bolt"></i>
						Category</a>
					</li>
					<li>
						<a href="mapping.php">
						<i class="fa fa-bolt"></i>
						Mapping</a>
					</li>
				</ul>
			</li>
			<!-- <li>
				<a href="exam-attempt-reports.php">
				<i class="fa fa-file-text-o"></i>
				<span class="title">Exam Attempt Reports</span>
				</a>
			</li> -->
			<li>
				<a href="results-and-reports.php">
				<i class="fa fa-file-text-o"></i>
				<span class="title">Results & Reports</span>
				</a>
			</li>
			<li>
				<a href="user-manage-role.php">
				<i class="fa fa-user"></i>
				<span class="title">User Roles</span>
				</a>
			</li>
			<li>
				<a href="student-assignment.php">
				<i class="fa fa-book"></i>
				<span class="title">Student Assignment</span>
				</a>
			</li>
			<li>
				<a href="manage-certificates.php">
				<i class="fa fa-book"></i>
				<span class="title">Manage Certificates</span>
				</a>
			</li>
			<!-- <li>
				<a href="manage-institute.php">
				<i class="fa fa-university"></i>
				<span class="title">Manage Institute</span>
				</a>
			</li> -->
			<!--  <li>
				<a href="manage-order.php">
				<i class="icon-basket"></i>
				<span class="title">Manage Orders</span>
				</a>
			</li>
			<li>
				<a href="manage-discount.php">
				<i class="fa fa-university"></i>
				<span class="title">Manage Discount</span>
				</a>
			</li> -->
			<li>
				<a href="change-password.php">
				<i class="fa fa-key"></i>
				<span class="title">Change Password</span>
				</a>
			</li>
			<!-- <li>
				<a href="import-pdf.php">
				<i class="fa fa-key"></i>
				<span class="title">Import PDF</span>
				</a>
			</li> -->
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>