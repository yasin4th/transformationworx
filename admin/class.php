<?php include 'Access-API-sup.php'; ?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.1
Version: 3.6
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<!-- <title>AIETS - Test Engine</title> -->
	<?php include 'html/general/headtags.php' ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square"> 
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php' ?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php' ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Class / Exam
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Master Data</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="class.php">Class / Exam</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<div class="row">
				<div class="col-md-6">
					<!-- Dialog box for add Class -->
					<div id="class-dialog" class="portlet box blue" data-mode="new">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-plus"></i> Add Class / Exam
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="#" class="form-horizontal">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3">Name <span class="required">* </span></label>
										<div class="col-md-9">
											<input type="text" class="form-control" id="class_name" placeholder="Name" required="required">
											<!--<span class="help-block">
											Something may have gone wrong </span>-->
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Description </label>
										<div class="col-md-9">
											<textarea class="form-control" id="class_description" placeholder="Description" ></textarea>
											<!--<span class="help-block">
											Please correct the error </span>-->
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="save btn green">Save</button>
											<button type="button" class="cancel btn default">Cancel</button>
											<a class="delete btn btn-circle btn-icon-only red pull-right">
												<i class="fa fa-trash-o"></i>
											</a>
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- /Dialog box for add class -->
					<!-- Dialog box for add subject -->
					<div id="subject-dialog" class="portlet box blue hidden" data-mode="new">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Add Subject
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="#" class="form-horizontal">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3">Name <span class="required">* </span></label>
										<div class="col-md-9">
											<input type="text" class="form-control" id="subject_name" placeholder="Name" required="required">
											<!--<span class="help-block">
											Something may have gone wrong </span>-->
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Description </label>
										<div class="col-md-9">
											<textarea class="form-control" id="subject_description" placeholder="description" ></textarea>
											<!--<span class="help-block">
											Please correct the error </span>-->
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="save btn green">Save</button>
											<button type="button" class="cancel btn default" >Cancel</button>
											<a class="delete btn btn-circle btn-icon-only red pull-right">
												<i class="fa fa-trash-o"></i>
											</a>
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- /Dialog box for add subject -->
					<!-- Dialog box for add unit -->
					<div id="unit-dialog" class="portlet box blue hidden" data-mode="new">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Add Unit
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="#" class="form-horizontal">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3">Name <span class="required">* </span></label>
										<div class="col-md-9">
											<input type="text" class="form-control" id="unit_name" placeholder="Name" required="required">
											<!--<span class="help-block">
											Something may have gone wrong </span>-->
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Description </label>
										<div class="col-md-9">
											<textarea class="form-control" id="unit_description" placeholder="description" ></textarea>
											<!--<span class="help-block">
											Please correct the error </span>-->
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="save btn green">Save</button>
											<button type="button" class="cancel btn default">Cancel</button>
											<a class="delete btn btn-circle btn-icon-only red pull-right">
												<i class="fa fa-trash-o"></i>
											</a>
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- /Dialog box for add unit -->
					<!-- Dialog box for add Chapter -->
					<div id="chapter-dialog" class="portlet box blue hidden" data-mode="new">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Add Chapter
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="#" class="form-horizontal">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3">Name <span class="required">* </span></label>
										<div class="col-md-9">
											<input type="text" class="form-control" id="chapter_name" placeholder="Name" required="required">
											<!--<span class="help-block">
											Something may have gone wrong </span>-->
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Time(In Hours) <span class="required">* </span></label>
										<div class="col-md-9">
											<input type="number" class="form-control" id="chapter_time" placeholder="Time(In Hours)" required="required">
											<!--<span class="help-block">
											Something may have gone wrong </span>-->
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Description </label>
										<div class="col-md-9">
											<textarea class="form-control" id="chapter_description" placeholder="description" ></textarea>
											<!--<span class="help-block">
											Please correct the error </span>-->
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="save btn green">Save</button>
											<button type="button" class="cancel btn default">Cancel</button>
											<a class="delete btn btn-circle btn-icon-only red pull-right">
												<i class="fa fa-trash-o"></i>
											</a>
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- /Dialog box for add Chapter -->
				<!-- Dialog box for add Topic -->
					<div id="topic-dialog" class="portlet box blue hidden" data-mode="new">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Add Topic
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="#" class="form-horizontal">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3">Name <span class="required">* </span></label>
										<div class="col-md-9">
											<input type="text" class="form-control" id="topic_name" placeholder="Name" required="required">
											<!--<span class="help-block">
											Something may have gone wrong </span>-->
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Description </label>
										<div class="col-md-9">
											<textarea class="form-control" id="topic_description" placeholder="description" ></textarea>
											<!--<span class="help-block">
											Please correct the error </span>-->
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="save btn green">Save</button>
											<button type="button" class="cancel btn default">Cancel</button>
											<a class="delete btn btn-circle btn-icon-only red pull-right">
												<i class="fa fa-trash-o"></i>
											</a>
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- /Dialog box for add Topic -->
				
				</div>
				<div class="col-md-6">
					<div class="portlet blue-hoki box">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-home"></i> Class / Exam
							</div>
						</div>
						<div class="portlet-body" style="display: block;">
							<div id="tree_1" class="tree-demo">
							<!--
								<ul>
									<li>
										 Class 1
										<ul>
											<li data-jstree='{ "opened" : true }'>
												 Subject 1
												<ul>
													<li data-jstree='{ "opened" : true }'>
														 Unit 1
														 <ul>
															<li data-jstree='{ "opened" : true }'>
																 Chapter 1
																 <ul>
																	<li data-jstree='{ "opened" : true }'>
																		Topic 1
																	</li>
																	<li data-jstree='{ "opened" : true }'>
																		Topic 2
																	</li>
																	<li data-fun="addTopicDialog" data-jstree='{ "type" : "file" }'>
																		Add Topic
																	</li>
																</ul>
															</li>
															<li data-jstree='{ "opened" : true }'>
																 Chapter 2
															</li>
															<li data-fun="addChapterDialog" data-jstree='{ "type" : "file" }'>
																 Add Chapter
															</li>
														</ul>
													</li>
													<li data-fun="addUnitDialog" data-jstree='{ "type" : "file" }'>
														 Add Unit
													</li>
												</ul>
											</li>
											<li data-fun="addSubjectDialog" data-jstree='{ "type" : "file" }'>
												 Add Subject
											</li>
										</ul>
									</li>
									<li data-fun="addClassDialog" data-jstree='{ "type" : "file" }'>
										Add Class 
									</li>
								</ul>
							-->
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php' ?>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/plugins/jstree/dist/jstree.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/ui-tree.js"></script>
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script>
    jQuery(document).ready(function() {       
        // initiate layout and plugins
		Metronic.init(); // init metronic core components
		Layout.init(); // init current layout
		QuickSidebar.init(); // init quick sidebar
		Demo.init(); // init demo features
          
    });
    </script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(8)').addClass('active open');
  });
  $(function () {
	  $('#data li').removeClass('active');
	  $('#data li:eq(0)').addClass('active');
  });
</script>
<script src="js/common.js" type="text/javascript"></script>
<script src="js/custom/class.js" type="text/javascript"></script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>