<?php include 'Access-API-sup-sme.php'; ?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.1
Version: 3.6
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<!-- <title>AIETS - Test Engine</title> -->
	<?php include 'html/general/headtags.php'; ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square"> 
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php';?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php';?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-9">
					<h3 class="page-title" style="display:inline-bolck">
						<span class="test-program"></span>
						<a id="show-edit-test-program-modal" class="EntranceExam">
							<i class="fa fa-edit"></i>
						</a>
					</h3>	
				</div>
				<div class="col-md-3"></div>
				<!-- <div class="col-md-1">
					<span class="" id="percentage"></span>
				</div>
				<div class="col-md-2">
					<form id="test-program-form" role="form" action="../vendor/fileUploads.php" method="post" enctype="multipart/form-data">
						<span class="btn blue btn-file pull-right">
								Change Image
								<input type="file" name="test-program-image" id="test-program-image" accept="image/*"/>
								<input type="hidden" name="action" id="action" value="change_test_program_image" />
								<input type="hidden" name="program_id" id="program_id" />
						</span>
					</form>
				</div> -->
			</div>	
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="test-program.php">Test Package</a>
						<i class="fa fa-angle-right"></i>
					</li>
					
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			
			<!-- BEGIN DASHBOARD STATS -->
			<div class="row">
				<!-- Subjects -->
				<div class="col-md-12">
					<div id="subjects-div" class="row">
						<table id="program-table" class="table table-striped table-hover table-bordered">
							<thead>
								<th>Class</th>
								<!-- <th>Chapter Test</th> -->
								<th>Practice Test</th>
								<!-- <th>Diagnostic Test</th> -->
								<th>Mock Test</th>
								<th>Scheduled Test</th>
								<th>Assignment Template</th>
								<th>Action</th>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
				<!--/ Subjects -->			

			</div>
			<!-- END DASHBOARD STATS -->
			
		</div>
	</div>
			
			
	<!-- END PAGE CONTENT-->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php';?>
<!-- END FOOTER -->

	<div id="test-paper-by-type-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title control-label">Add Test</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<a href="add-test-paper.php" class="btn green">Set New Test Paper</a>
						</div>
						<div class="col-md-6">
							<a id="saveAttachedTestPaper" class="btn green pull-right"><i class="fa fa-save"></i> &nbsp; Save</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							
							<div class="margin-top-10 margin-bottom-10">
								<label>Import From Existing Test Paper</label>
								<div class="table-scrollable">
									<table id="test-paper-by-type-modal-table" class="table table-bordered table-striped table-hover">
									<thead>
									<tr>
										<th>
											 <i class="fa fa-caret-down"></i> Name
										</th>
										<!--<th>
											 <i class="fa fa-caret-down"></i> Type
										</th>
										<th>
											 <i class="fa fa-caret-down"></i> Select
										</th> -->
									</tr>
									</thead>
									<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.modal myModal12 -->
	<!-- .modal large -->
	<div class="modal fade bs-modal-lg" id="edit-test-program-modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Test Package</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-3">
							<div class="col-md-12 col-sm-12">
								<span class="" id="percentage"></span>
							</div>
							<div class="col-md-12 col-sm-12 form-group">
								<img id="test-program-image" src="../assets/frontend/pages/img/test_program/default.jpg" class="img-responsive center-block" alt="Test Program" title="Test Program">
							</div>
							<div class="col-md-12 col-sm-12 form-body">
								<form id="test-program-form" role="form" action="../vendor/fileUploads.php" method="post" enctype="multipart/form-data">
									<span class="btn blue btn-file center-block mrg-bot10">
											Change Image
											<input type="file" name="change-test-program-image" id="change-test-program-image" accept="image/*"/>
											<input type="hidden" name="action" id="action" value="change_test_program_image" />
											<input type="hidden" name="program_id" id="program_id" />
									</span>
								</form>
							</div>
							<div class="col-md-12 col-sm-12 form-body text-center"> 
							<p> Max Image Size: 1 MB <br>Resolution: 500 X 500 px </p>
							</div>
						</div>
						<div class="col-md-9 edit-test-program-bor">
							 <form id="form-add-test-program" class="form-horizontal" role="form">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Name</label>
										<div class="col-md-9">
											<input id="test-program-name" type="text" class="form-control" placeholder="Test Package Name">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label"> Status </label>
										<div class="col-md-9">
											<select id="test-program-status" class="form-control">
												<option value="0"> Not For Sale </option>
												<option value="1"> For Sale </option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Package Type</label>
										<div class="col-md-9">
											<select id="test-program-type" class="form-control" placeholder="Related class for the Test Package">
												<option value="0"> Select Package Type </option>
												<option value="1"> Private </option>
												<option value="2"> Public </option>
												<option value="3"> All </option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Class/Exam</label>
										<div class="col-md-9">
											<select id="test-program-class" class="form-control" placeholder="Related class for the Test Package">
												<option value="0"> Select Class/Exam </option>
											</select>
										</div>
									</div>
									<div class="form-group hidden" id="batch-frm-grp">
										<label class="col-md-3 control-label">Batch</label>
										<div class="col-md-9">
											<select id="batch" multiple="multiple" class="form-control" placeholder="Select Batches for the Test Package"></select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Category</label>
										<div class="col-md-9">
											<select id="test-program-category" class="form-control" placeholder="Related class for the Test Package">
												<option value="0"> Select Category </option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Subjects</label>
										<div class="col-md-9">
											<select id="test-program-subjects" multiple="multiple" class="form-control" placeholder="Related Subjects for the Test Package"></select>
										</div>
									</div>
									<div class="form-group" id="price-frm-grp">
										<label class="col-md-3 control-label">Price</label>
										<div class="col-md-9">
											<input id="test-program-price" type="text" class="form-control" placeholder="Price for the Test Package (CAD)">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Cut Off</label>
										<div class="col-md-9">
											<input id="cut-off" type="text" class="form-control" maxlength="3" placeholder="Cut Off">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Description</label>
										<div class="col-md-9">
											<div id="test-program-description" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); min-height: 70px; position: relative;" data-ng-model="myModel"><p></p></div>
											<!-- <textarea id="test-program-description" class="form-control" rows="3" placeholder="Enter Description here"></textarea> -->
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a id="update_program" class="btn blue"><i class="fa fa-save"></i> &nbsp; Save</a>
					<button class="btn default" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal large -->

	<div id="test-paper-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title control-label">Add Test</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
						<label><strong>Test Paper Type</strong></label>
							<select name="type" id="test-paper-type" class="form-control">
								<option value="0">Select Type</option>
								<!-- <option value="1">Diagnostic Test</option> -->
								<!-- <option value="3">Chapter Test</option> -->
								<option value="4">Mock Test</option>
								<option value="5">Scheduled Test</option>
								<option value="6">Practice Test</option>
							</select>
						</div>
						<div class="col-md-6">
							<a id="saveAttachedPaper" class="btn green pull-right"><i class="fa fa-save"></i> &nbsp; Save</a>
						</div>
					</div>
					
					
					<div class="row">
						<div class="col-md-12">
							
							<div class="margin-top-10 margin-bottom-10">
								<!-- <label>Import From Existing Test Paper</label> -->
								<div class="table-scrollable">
									<table id="testpapertable" class="table table-bordered table-striped table-hover">
									<thead>
									<tr>
										<th>
											 <i class="fa fa-caret-down"></i> Name
										</th>									
									</tr>
									</thead>
									<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="qcr-illustration-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
		<div class="modal-dialog">
			<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Add Assignment Template</h4>
					</div>
				<form id="test-program-illustration" role="form" action="../vendor/fileUploads.php" method="post" enctype="multipart/form-data">
					<div class="modal-body">
						<div class="row">
								<div class="form-group">
										
									<div class="col-lg-12">
										<input type="text" name="illustration-name" id="illustration-name" class="form-control" placeholder="Name" />
									</div>
									
									<div class="col-lg-12">
										<span class="btn btn-info btn-file">
											Choose File
											<input type="file" name="illustration-file" id="illustration-file" class="form-control" multiple/>
										</span>
										<span id="file-name" class="mrg-left10"></span>
									</div>

									<input type="hidden" name="action" id="action" value="add_illustration_file" />
									<input type="hidden" name="program_id" id="program_id" />
									<input type="hidden" name="chapter_id" id="chapter_id" />
								</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" id="upload-illustration-file" class="btn blue"> Upload </button>
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>

<div class="modal fade" id="qcr-illustration-listing-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
		<div class="modal-dialog">
			<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Assignment Template</h4>
					</div>
				<div class="modal-body">
						<table class='table' id="listing-table">
						<thead>
							<tr>
								<th>Sr. No.</th>
								<th>Subject</th>
								<th>Assignment Template</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
					</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
<script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-markdown/lib/markdown.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script src="../assets/global/plugins/jquery.form.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<script src="js/custom/view-test-program1.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features 
});
</script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(3)').addClass('active open');
  });
  $(function () {
	  $('#program li').removeClass('active');
	  $('#program li:eq(0)').addClass('active');
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>