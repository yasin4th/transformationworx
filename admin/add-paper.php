<?php include 'Access-API-sup-sme.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php include 'html/general/headtags.php'; ?>
<!--  Begin Style  -->
<style>
	.required {
	color: #e02222;
	font-size: 12px;
	}
</style>
<!--  End Style  -->

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="page-header-fixed page-quick-sidebar-over-content page-style-square">
<!-- BEGIN HEADER -->
	<?php include 'html/general/header.php';?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
		<?php include 'html/general/sidebar.php';?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-6">
					<h3 class="page-title">
					Add Test Paper
					</h3>
				</div>
				<div class="col-md-6">
					<a id="genratePaper" href="untitle-question-paper.php" type="submit" class="btn btn-primary pull-right"><i class="fa fa-print"></i> &nbsp; Generate Paper </a>
				</div>
			</div>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<!-- <li>
						<a href="#">Test Program</a>
						<i class="fa fa-angle-right"></i>
					</li> -->
					<li>
						<a href="test_paper.php">Test Paper</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="add-test-paper.php">Add Test Paper</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="row">
					<div class="section">
						<div id="section1" class="portlet light bg-inverse mrg-bot10">
							<div class="portlet-title" style="border: none; margin: 0px;">
								<div class="caption">
									<i class="fa fa-puzzle-piece"></i> Section 1
								</div>
								<div class="tools">
									<!-- <a class="tooltips delete" data-container="body" data-placement="top" data-original-title="Delete Section" aria-describedby="tooltip735432"><i class="fa fa-trash-o"></i></a> -->
									<a class="collapse">
									</a>
								</div>
							</div>
							<div class="portlet-body form">
								<form role="form">
									<div class="form-body">
										<label>Please select the criteria to set question paper</label>
										<div class="row">
											<div class="col-md-3 col-xs-6">
												<div class="form-group">
													<label class="control-label"><span class="required">* </span>Section Name</label>
													<div class="row">
														<div class="col-md-12">
															<input type="text" class="section-name form-control pull-right" placeholder="Enter Section Name" value="Section 1">
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-5 col-xs-2"></div>
											<div class="col-md-2 col-xs-4">
												<div class="form-group">
													<label class="control-label">Cut Off Marks&nbsp;</label>
													<input  type="text" class="cut-off-marks form-control pull-right" placeholder="Cut Off Marks">
												</div>
											</div>
											<div class="col-md-2 col-xs-4">
												<div class="form-group">
													<label class="control-label">Total-Questions&nbsp;</label>
													<input  type="text" class="totalQuestions form-control pull-right" placeholder="Total Questions">
												</div>
											</div>
										</div>
										<div class="classAndSubject">
											<div class="col-md-12 purple-border mrg-bot10 form-body">
												<div class="chapterAndTopic col-md-12">
													<div class="row">
														<div class="col-md-12">
															<!-- <a class="tooltips delete pull-right mrg-left5" data-container="body" data-placement="top" data-original-title="Delete Criteria"><i class="fa fa-trash-o"></i></a> -->
														</div>
													</div>
													<div class="row">
														<div class="col-md-3">
															<div class="form-group">
																<label class="control-label"><span class="required">* </span>Class</label>
																<div class="row">
																	<div class="col-md-12">
																		<select class="classes form-control">
																			<option value="0">Select Class</option>
																		</select>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<label class="control-label"><span class="required">* </span>Subject</label>
																<div class="row">
																	<div class="col-md-12">
																		<select class="subjects form-control" data-tags="true">
																			<option value="0">Select Subject</option>
																		</select>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<label class="control-label">Unit</label>
																<div class="row">
																	<div class="col-md-12">
																		<select class="units form-control">
																			<option value="0">Select Unit</option>
																		</select>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<label class="control-label">Tags</label>
																<div class="row">
																	<div class="col-md-12">
																		<select class="tags form-control" multiple="multiple"></select>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="col-md-12 green-border mrg-bot10 form-body ">
														<div class="col-md-12">
															<div class="row">
																<div class="col-md-3">
																	<div class="form-group">
																		<div class="row">
																			<div class="col-md-12">
																				<select class="chapters form-control">
																					<option value="0">Select Chapter</option>
																				</select>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-md-3">
																	<div class="form-group">
																		<div class="row">
																			<div class="col-md-12">
																				<select class="topics form-control">
																				<option value="0">Select Topic</option>
																				</select>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-md-2 col-md-offset-4">
																	<!-- <a class="delete-green-box tooltips delete pull-right mrg-left10" data-container="body" data-placement="top" data-original-title="Delete Multiple Chapter"><i class="fa fa-trash-o"></i></a> -->
																	<!-- <a class="addChapterAndTopic tooltips add-color pull-right" data-container="body" data-placement="top" data-original-title="Add Multiple Chapter"><i class="fa fa-plus-circle"></i></a> -->
																	<!--<button class="addChapterAndTopic btn popovers btn-primary pull-right" data-container="body" data-trigger="hover" data-placement="left" data-content="To add multiple chapter under the selected subject & unit" data-original-title="Add Multiple Chapter">&nbsp; <i class="fa fa-plus-circle"></i> &nbsp;</button>-->
																</div>
															</div>
															<div class="col-md-12 blue-border form-body">
																<div class="questions col-md-11">
																	<div class="question form-group">
																		<div class="row">
																			<div class="col-md-11">
																				<div class="row">
																					<div class="row">
																						<div class="col-md-12">
																							<div class="col-md-2 col-xs-2">
																								<span class="required">* </span>
																								<select class="question-types form-control pull-right width-select-type">
																									<option value="0">Select Question Type</option>

																								</select>
																							</div>
																							<div class="col-md-2 col-xs-2">
																							<span class="required">* </span>
																							<input type="text" class="count form-control inline pull-right width-select-type" placeholder="Count" />
																							</div>
																							<div class="col-md-2 col-xs-2">
																								<select class="difficulty-levels form-control" placeholder="Select Class">
																									<option value="0">Difficulty Level</option>
																								</select>
																							</div>
																							<div class="col-md-2 col-xs-2">
																								<select class="skills form-control" placeholder="Select Class">
																									<option value="0">Skill</option>
																								</select>
																							</div>
																							<div class="col-md-2 col-xs-1">
																								<input type="text" class="max-mark form-control" placeholder="Max Marks">
																							</div>
																							<div class="col-md-2 col-xs-1">
																								<input type="text" class="neg-mark form-control" placeholder="Neg Marks">
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-md-1">
																				<!-- <a class="delete-blue-box tooltips delete pull-right mrg-left10" data-container="body" data-placement="top" data-original-title="Delete Multiple Question"><i class="fa fa-trash-o"></i></a> -->
																				<!--<button class="btn blue pull-right">&nbsp; <i class="fa fa-trash-o"></i> &nbsp;</button>-->
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-md-1 col-xs-2">
																	<a class="addQuestionType tooltips add-color pull-right" data-container="body" data-placement="top" data-original-title="Add Multiple Question"><i class="fa fa-plus-circle"></i></a>
																	<!--<button class="addQuestionType btn popovers green pull-right" data-container="body" data-trigger="hover" data-placement="bottom" data-content="To add multiple question type, difficulty level & skill" data-original-title="Add Multiple Question">&nbsp; <i class="fa fa-plus-circle"></i> &nbsp;</button>-->
																</div>
															</div>
														</div>
													</div>

												</div>
												<a class="addChapterAndTopic btn tooltips add-color pull-right" data-container="body" data-placement="top" data-original-title="Add Multiple Chapter"><i class="fa fa-plus-circle"></i></a>
											</div>

										</div>
										<button class="addCriteria btn green" style="margin-top:10px;"><i class="fa fa-plus-circle"></i> &nbsp; Another Criteria</button>
										<div class="row">
											<div class="col-md-6" style="margin-top:10px;">
											</div>
											<div class="col-md-6"></div>
										</div>
									</div>
								</form>
							</div>
						</div>

					</div>
					</div>
				</div>
					<!-- END SAMPLE FORM PORTLET-->
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6" style="margin-top:10px;">
							<div class="form-group">
								<!-- <div class="checkbox-list">
									<label><input type="checkbox"> Group some question type together</label>
								</div> -->
								<button class="addSection btn btn-primary"><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Section &nbsp;&nbsp;&nbsp;</button>
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
				</div>
			</div>
		</div>


		<!-- END PAGE CONTENT-->
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
   <?php include 'html/general/footer.php';?>
<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->

<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>

<script src="js/custom/add-test-paper/add-test-paper.js" type="text/javascript"></script>
<script src="js/custom/add-test-paper/add-test-paper_allEvents.js" type="text/javascript"></script>
<script src="js/custom/add-test-paper/add-test-paper-addMoreChaptersAndTopics.js" type="text/javascript"></script>
<script src="js/custom/add-test-paper/delete-functions.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features
});
</script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(3)').addClass('active open');
  });
  $(function () {
	  $('#program li').removeClass('active');
	  $('#program li:eq(1)').addClass('active');
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>