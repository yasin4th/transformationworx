<?php include 'Access-API-sup.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<!-- <title>AIETS - Test Engine</title> -->
	<?php include 'html/general/headtags.php' ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square">
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php' ?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php' ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Manage Discount
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="manage-discount.php">Manage Discount</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-university"></i> Manage Discount
							</div>
						</div>
						<div class="portlet-body">							
							<div class="row">
								<div class="col-md-6">									
									<a href="#add-discount-modal" data-toggle="modal" class="btn btn-circle btn-icon-only green mrg5 tooltips" data-container="body" data-placement="top" data-original-title="Add Discount">
										<i class="fa fa-plus-circle"></i>
									</a>
								</div>
								<div class="col-md-6">
									<ul class="question-table-pagination pagination pull-right"></ul>
								</div>
							</div>
							<div class="margin-top-10 margin-bottom-10 table-scrollable">
								<table id="discount-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>
												S.No.
											</th>											
											<th>
												Title
											</th>
											<th>
												Code
											</th>
											<th>
												type
											</th>
											<th>
												Value
											</th>
											<th>
												Expiry
											</th>
											<th>
												Maximum Uses
											</th>
											<th>
												Total Uses
											</th>
											<th>
												Status
											</th>
											<th style="width:150px;">
												Action
											</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
							<div class="row">
								<div class="col-md-12">
									<ul class="question-table-pagination pagination pull-right"></ul>
								</div>
							</div>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>


			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php' ?>
<!-- END FOOTER -->
<!-- Start Add Institute Modal -->
<div class="modal fade" id="add-discount-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add Discount</h4>
			</div>
			<form id="create-discount-form" role="form">
				<div id="add-manage-institute">
					<div class="modal-body">

						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Title</label>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="title form-control" placeholder="Discount Title" id="title">
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Code</label>
									<div class="row">
										<div class="col-sm-12">
											<input id="code" type="text" class="code form-control" placeholder="Code">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Type</label>
									<div class="row">
										<div class="col-md-12">
											<select id="type" class="form-control">
												<option value="Percent">Percent</option>
												<option value="Fixed">Fixed</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Maximum Uses</label>
									<div class="row">
										<div class="col-md-12">
											<input type="number" class="form-control" name="maxuses" id="maxuses" value="1" min="1">	
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Value</label>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="name form-control" placeholder="Discount Value" id="value">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Min Price</label>
									<div class="row">
										<div class="col-sm-12">
											<input type="number" class="email form-control" placeholder="Min Price" id="min" min="0" value="0">
										</div>
									</div>
								</div>
							</div>

						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Expiry</label>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group date">
												<input id="expiry" type="text" class="form-control" placeholder="Start Date/Time">
							                    <span class="input-group-addon">
							                        <span class="glyphicon glyphicon-calendar"></span>
							                    </span>
							                </div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Status</label>
									<div class="row">
										<div class="col-sm-12">
											<select name="status" id="status" class="form-control">
												<option value="1">Active</option>
												<option value="0">Inactive</option>
											</select>
										</div>
									</div>
								</div>
							</div>

						</div>
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div class="form-group">
									<label class="control-label">Description</label>
									<textarea id="description" class="form-control"></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn green"><i class="fa fa-plus-circle"></i> &nbsp; Add </button>
							<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</div>
			</form>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>
<!-- End Add Institute Modal -->
<!-- Start Edit Institute Modal -->
<div class="modal fade" id="edit-discount-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit Discount</h4>
			</div>
			<form id="edit-discount-form" role="form">
				<div id="edit-manage-discount">
					<div class="modal-body">

						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Title</label>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="title form-control" placeholder="Discount Title" id="edit-title">
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Code</label>
									<div class="row">
										<div class="col-sm-12">
											<input id="edit-code" type="text" class="code form-control" placeholder="Code">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Type</label>
									<div class="row">
										<div class="col-md-12">
											<select id="edit-type" class="form-control">
												<option value="Percent">Percent</option>
												<option value="Fixed">Fixed</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Maximum Uses</label>
									<div class="row">
										<div class="col-md-12">
											<input type="number" class="form-control" name="edit-maxuses" id="edit-maxuses" value="1">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Value</label>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="name form-control" placeholder="Discount Value" id="edit-value">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Min Price</label>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="email form-control" placeholder="Min Price" id="edit-min">
										</div>
									</div>
								</div>
							</div>

						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Expiry</label>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-group date">
												<input id="edit-expiry" type="text" class="form-control" placeholder="Start Date/Time">
							                    <span class="input-group-addon">
							                        <span class="glyphicon glyphicon-calendar"></span>
							                    </span>
							                </div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Status</label>
									<div class="row">
										<div class="col-sm-12">
											<select name="status" id="edit-status" class="form-control">
												<option value="1">Active</option>
												<option value="0">Inactive</option>
											</select>
										</div>
									</div>
								</div>
							</div>

						</div>
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div class="form-group">
									<label class="control-label">Description</label>
									<textarea id="edit-description" class="form-control"></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn green"><i class="fa fa-plus-circle"></i> &nbsp; Save </button>
							<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</div>
			</form>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>
<!-- End Edit Institute Modal -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<script src="js/custom/manage-discount.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features
   Index.init();
});
</script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(20)').addClass('active open');
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>