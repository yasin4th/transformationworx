<?php @session_start(); 
if(isset($_SESSION['role']) && $_SESSION['role']!=4)
 { 
  header('location: dashboard.php');
  }
  if(isset($_SESSION['role']) && $_SESSION['role']==4)
 { 
  header('location: ../index.php');
  }

?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.1
Version: 3.6
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<!-- <title>AIETS - Test Engine</title> -->
	<?php include 'html/general/headtags.php' ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
	<div class="logo">
		<a href="dashboard.php">
		<!-- <img src="assets/admin/layout/img/default.png" alt=""/> -->
		<h3>TransformationWorx Admin</h3>
		</a>
	</div>
	<!-- END LOGO -->
	<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
	<div class="menu-toggler sidebar-toggler">
	</div>
	<!-- END SIDEBAR TOGGLER BUTTON -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<!-- BEGIN LOGIN FORM -->
		<form id="loginForm" class="login-form">
			<h3 class="form-title">Admin Log in </h3>
			<div class="alert alert-danger display-hide">
				<button class="close" data-close="alert"></button>
				<span>
				Enter any username and password. </span>
			</div>
			<div class="form-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">Email Id</label>
				<div class="input-icon">
					<i class="fa fa-user"></i>
					<input id="username" class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email Id / Username" name="username"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label visible-ie8 visible-ie9">Password</label>
				<div class="input-icon">
					<i class="fa fa-lock"></i>
					
					<input id="password" class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/> 
				</div>
			</div>
			<div class="form-actions" style="border-bottom: none;">
				<label class="checkbox">
				<input type="checkbox" name="remember" value="1"/> Remember me </label>
				<button id="login" type="submit" class="btn green-haze pull-right">
				Login <i class="m-icon-swapright m-icon-white"></i>
				</button>
				<!--<button type="submit" class="btn green-haze pull-right">
				Login <i class="m-icon-swapright m-icon-white"></i>
				</button>-->
			</div>
			<!-- <div class="forget-password">
				<h4>Forgot your password ?</h4>
				<p>
					 no worries, click <a href="javascript:;" id="forget-password">
					here </a>
					to reset your password.
				</p>
			</div> -->
		</form>
		<!-- END LOGIN FORM -->
		<!-- BEGIN FORGOT PASSWORD FORM -->
		<form id="forgetPassForm" class="forget-form" action="dashboard.php" method="post">
			<h3>Forget Password ?</h3>
			<p>
				 Enter your e-mail address below to reset your password.
			</p>
			<div class="form-group">
				<div class="input-icon">
					<i class="fa fa-envelope"></i>
					<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
				</div>
			</div>
			<div class="form-actions">
				<button type="button" id="back-btn" class="btn">
				<i class="m-icon-swapleft"></i> Back </button>
				<button type="submit" class="btn green-haze pull-right">
				Submit <i class="m-icon-swapright m-icon-white"></i>
				</button>
			</div>
		</form>
		<!-- END FORGOT PASSWORD FORM -->
		
	</div>
	<!-- END LOGIN -->
	<!-- BEGIN COPYRIGHT -->
	<div class="copyright">
		 2017 &copy; TransformationWorx. All Rights Reserved.
	</div>
	<!-- END COPYRIGHT -->
	<?php include 'html/general/scripts.php'; ?>
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/global/plugins/toastr/toastr.min.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
	<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
	<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
	<script src="assets/admin/pages/scripts/login.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	
	<script>
	jQuery(document).ready(function() {     
	  Metronic.init(); // init metronic core components
	  Layout.init(); // init current layout
	  Login.init();
	  Demo.init();
	});
	</script>
	<script type="text/javascript" src="js/custom/index.js"></script>
</body>
<!-- END BODY -->
</html>