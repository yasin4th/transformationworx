<?php include 'Access-API-sup.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<!-- <title>AIETS - Test Engine</title> -->
	<?php include 'html/general/headtags.php' ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square">
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php' ?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php' ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Manage Enrollment Ids
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="test-program.php">Test Package</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Manage Enrollment Ids</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-university"></i> Manage Entrollment
							</div>
						</div>
						<div class="portlet-body">
						<!--<div id="add-manage-institute">
							<div class="row">
								<div class="col-md-4 col-sm-4">
									<div class="form-group">
										<label class="control-label">Select Test Program</label>
										<div class="row">
											<div class="col-sm-12">
												<select id="test-programs" class="name form-control">
													<option>Select Test Program</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 col-sm-4">
									<div class="form-group">
										<label class="control-label">Select Status</label>
										<div class="row">
											<div class="col-sm-12">
												<select id="coupon-status" class="name form-control">
													<option value="0">Select Status</option>
													<option value="1">Used</option>
													<option value="2">Unused</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 col-sm-4">
									<div class="form-group">
										<label class="control-label">&nbsp;</label>
										<div class="row">
											<div class="col-sm-12">
												<button id="filter" class="btn green"><i class="fa fa-filter"></i> &nbsp; Filter </button>
												<button onClick="resetForm();" class="btn green"> <i class="fa fa-repeat"></i> &nbsp; Reset </button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<button id="download-selected-coupons" class="btn green mar-top45"> <i class="fa fa-download"></i> &nbsp; Download as Text </button>
							</div>
						</div> -->
							<div class="margin-top-10 margin-bottom-10 table-scrollable">
								<table id="institute-coupons-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th style="width:100px;">
												<input type="checkbox" class="select-all"> S/N
											</th>
											<th style="width:200px;">
												Enrollment Id
											</th>
											<th>
												Test Program
											</th>
											<th>
												Status
											</th>
											<th>
												Created On
											</th>
											<!-- <th>
												Expiry Date
											</th> -->
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<div class="modal fade" id="students-details-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Student Details</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<span class="col-md-6">
									Name :
								</span>
								<span class="student-name">
									Demo Name
								</span>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<span class="col-md-6">
									E-mail :
								</span>
								<span class="student-e-mail">
									Demo@Email.com
								</span>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<span class="col-md-6">
									Last Logged In On :
								</span>
								<span class="login-date">
									Date
								</span>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<!-- <button type="button" class="btn blue">Save changes</button> -->
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php' ?>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>

<script src="js/common.js" type="text/javascript"></script>
<script src="js/custom/program-enrollment.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features
   Index.init();
});
</script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(15)').addClass('active open');
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
