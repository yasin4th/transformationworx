<?php include 'Access-API-sup.php'; ?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.1
Version: 3.6
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<style type="text/css">
	    table {
	      border-collapse: collapse;
	    }
	    caption {
	      background: #D3D3D3;
	    }
	    /*th {
	      background: #A7C942;
	      border: 1px solid #98BF21;
	      color: #ffffff;
	      font-weight: bold;
	      text-align: left;
	    }*/
	    td {
	      border: 1px solid #98BF21;
	      text-align: left;
	      font-weight: normal;
	      color: #000000;
	    }
	    tr:nth-child(odd) {
	      background: #ffffff;
	    }
	    tbody tr:nth-child(odd) th {
	      background: #ffffff;
	      color: #000000;
	    }
	    /*tr:nth-child(even) {
	      background: #EAF2D3;
	    }*/
	    tbody tr:nth-child(even) th {
	      background: #EAF2D3;
	      color: #000000;
	    }
	    #target {
	      width: 600px;
	      height: 400px;
	    }
	    #skilltarget {
	      width: 600px;
	      height: 400px;
	    }
	    .list-group-item {
		   	display: inline-block !important;
		   	border: 0px !important;
		    /*float: left;*/
		}
		.list-group.margin-bottom-25.sidebar-menu {
			border-bottom: 1px solid #747F8C;
		}
}
	</style>

<!-- <title>AIETS - Test Engine</title> -->
	<?php include 'html/general/headtags.php' ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square"> 
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php' ?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php' ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-6">
					<h3 class="page-title">
					Student Reports
					</h3>
				</div>
				<div class="col-md-6">
					<!-- <ul class="pagination pull-right">
						<li>
							<a href="#">
							Prev </a>
						</li>
						<li>
							<a href="#">
							Attempt #1 </a>
						</li>
						<li>
							<a href="#">
							Next </a>
						</li>
					</ul> -->
				</div>
			</div>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="results-and-reports.php">Results & Reports</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Student Reports</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<!-- BEGIN PAGE CONTENT-->

			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-3">
							<label>Name of the student:</label> <span class="student-name"></span> 
						</div>
						<div class="col-md-3">
							<!-- <label>Date of test:</label> <span class="test-date"></span> -->
						</div>
						<div class="col-md-3"></div>
						<div class="col-md-3"></div>
					</div>
				</div>
				<div class="col-md-12">
					<!-- BEGIN CONTENT -->
		        	<div class="">
		      			<!-- <div class="row">
		      				<div class="col-md-6 col-sm-6">
		      					<h2>My Test History</h2>
		      				</div>
		      				<div class="col-md-6 col-sm-6"></div>
		      			</div> -->
		      			<div class="row margin-bottom-20">
		              <div class="col-md-12">

		                <div class="portlet box grey-cascade">
		                  <div class="portlet-title">
		                    <div class="caption">
		                      <strong>Test Name</strong> : <span id="paper-name"><!-- Series Completion --></span>
		                    </div>
		                    <div class="tools">
		                      <!-- <div class="pull-left fonts-black">
		                        Test Date : <span class="test-date"></span>
		                      </div> -->
		                    </div>
		                  </div>
		                  <div class="portlet-body form">
		                    <div class="tabbable-line">
		                      <div class="col-md-12 col-sm-12 mrg-top20">
		                        <!-- <ul class="nav nav-tabs ">
		                          <li class="active">
		                            <a href="#tab1" data-toggle="tab">
		                            Self </a>
		                          </li>
		                          <li>
		                            <a href="#tab2" data-toggle="tab">
		                            Comparative </a>
		                          </li>
		                        </ul> -->
		                         <div class="">
		                                          <div class="col-md-12 col-sm-12 text-center score-card-details well tiles  mrg-top5">
		                         					<!-- <span class="label font24 label-success"> Report Summary </span> -->
		                                            <h4 class="text-center mrg-tb20 text-info"><strong>Attempted <span class="attemted-questions"></span> out of <span class="total-questions"></span></strong></h4>
		                                            <div class="tiles-details">
			                                            <div class="tile medium bg-white blue">
			                                              <div class="tile-body">
			                                                <i class="fa fa-graduation-cap"></i>
			                                              </div>
			                                              <div class="tile-object">
			                                                <div class="name">
			                                                   Percentile Rank: 
			                                                   <!-- Average Time: <span id="avg-time" class="number"> 00 min 00 sec </span> -->
			                                                </div>
			                                                <div id="percentile-rank" class="number">
			                                                	85
			                                                </div>
			                                              </div>
			                                            </div>
			                                            <div class="tile bg-white green medium">
			                                              <div class="tile-body">
			                                                <i class="fa fa-thumbs-up"></i>
			                                              </div>
			                                              <div class="tile-object">
			                                                <div class="name">
			                                                   Correct
			                                                </div>
			                                                <div class="correct-answers correct number">
			                                                  00/00
			                                                </div>
			                                              </div>
			                                            </div>
			                                            <div class="tile bg-white purpal medium">
			                                              <div class="tile-body">
			                                                <i class="glyphicon glyphicon-stats"></i>
			                                              </div>
			                                              <div class="tile-object">
			                                                <div class="name">
			                                                  Score
			                                                </div>
			                                                <div class="your-score number">
			                                                  00
			                                                </div>
			                                              </div>
			                                            </div>
			                                            <div class="pull-left">
			                                              <div class="tile medium bg-white red mrg-right0">
			                                                <div class="tile-body">
			                                                  <i class="fa fa-thumbs-down"></i><!-- <i class="fa fa-frown-o"></i> -->
			                                                </div>
			                                                <div class="tile-object">
			                                                  <div class="name">
			                                                    Wrong/Skipped
			                                                  </div>
			                                                  <div class="wrong-unattempted number">
			                                                    00/00
			                                                  </div>
			                                                </div>
			                                              </div>
			                                              <!-- <a href="#" class="padd5" style="display:block">Take Test</a> -->
			                                            </div>
		                                          	</div>
		                                          </div>
		                                          <!-- <button class="btn blue pull-left mrg-bot10">Generate Similar Test</button>
		                                          <a href="#" class="pull-right">Review</a> -->
		                                        </div>

		                      </div>
		                      
		                      <div class="tab-content">
		                        <div class="tab-pane active" id="tab1">
		                          <div class="row">
		                            <div class="col-md-12 col-sm-12 mrg-top20 mrg-bot20">
		                              <!-- BEGIN SIDEBAR -->
		                              <div class="sidebar col-md-12 col-sm-12">
		                                <ul class="list-group margin-bottom-25 sidebar-menu">
		                                  <li class="list-group-item clearfix li-brief active"><a  data-toggle="tab" href="#brief"> Brief Report</a></li>
		                                  <!-- <li class="list-group-item clearfix li-topicwise"><a  data-toggle="tab" href="#topicwise"> Chapter-wise</a></li> -->
		                                  <li class="list-group-item clearfix li-skillwise"><a  data-toggle="tab" href="#skillwise"> Skill-wise</a></li>
		                                  <li class="list-group-item clearfix li-difficultywise"><a  data-toggle="tab" href="#difficultywise"> Difficulty-wise</a></li>
		                                  <li class="list-group-item successgap clearfix"><a  data-toggle="tab" href="#successgap"> Success Gap</a></li>
		                                  <!-- <li class="list-group-item clearfix li-percentilerank"><a  data-toggle="tab" href="#percentilerank"> Percentile Rank</a></li> -->
		                                  <!-- <li class="list-group-item clearfix li-timewise"><a  data-toggle="tab" href="#timewise"> Time-wise</a></li> -->
		                                  <li class="list-group-item clearfix li-questionwise"><a  data-toggle="tab" href="#questionwise"> Question-wise</a></li>
		                                  <li class="list-group-item clearfix li-detailedtestreport"><a  data-toggle="tab" href="#detailedtestreport"> Detailed Report</a></li>
		                                </ul>
		                              </div>
		                              <!-- END SIDEBAR -->
		                              <div class="">
		                                <div class="col-md-12 col-sm-12 tab-content">
		                               
		                                  <!-- start Brief -->
		                                  <div id="brief" class="tab-pane active">
		                                    <span class="label font24 label-default"> Score Card </span>
		                                    <!-- <span class="label font24 label-default pull-right">Percentile Rank: <span id="percentile-rank">00</span> </span> -->
		                                    <div class="borderccc mrg-top5">
		                                      <div class="row">
		                                        <div class="col-md-12 col-sm-12">
		                                          <!-- <div class="row">
		                                            <div class="col-md-12 col-sm-12">
		                                              <h4 class="pull-right">Rank : <span class="label label-success"> <strong>00123</strong> </span></h4>
		                                            </div>
		                                          </div> -->
		                                          <div class="row">
		                                            <div class="col-md-7 col-sm-7">
		                                              <!-- <h4>Percentile</h4> -->
		                                              <div id="questions-pie-chart" class="chart" style="height: 300px;"></div>
		                                            </div>
		                                            <div class="col-md-5 col-sm-5">
		                                              <h4>Score</h4>
		                                              <div id="chart_5" class="chart" style="height: 300px;"></div>
		                                            </div>
		                                          </div>
		                                        </div>

		                                        <!-- <div class="col-md-12 col-sm-12">
		                                          
		                                        </div> -->
		                                       
		                                        <!-- <div class="time-analysis">
		                                          <div class="col-md-12 col-sm-12 border-top-ccc">
		                                            <div class="row mrg-top20">
		                                              <h4><span class="label font24 label-success"> Your Analysis </span></h4>
		                                            </div>
		                                            <div class="row">
		                                              <div class="col-md-4 col-sm-4 border-right-999">
		                                                <h4>Your Question-wise Analysis</h4>
		                                                <p>No. of unattempted Question : <span class="unattempted">15</span></p>
		                                                <p>Wrong : <span class="wrong">15</span></p>
		                                                <a data-toggle="tab" href="#questionwise" class="pull-right questionwise">Question-wise Analysis</a>
		                                              </div>
		                                              <div class="col-md-4 col-sm-4 border-right-999">
		                                                <h4>Time Analysis of Your Test</h4>
		                                                <p>Average time of each question<br> was <span class="avg-time">00 min 00 sec</span></p>
		                                                <a data-toggle="tab" href="#timewise" class="pull-right timewise">Time Analysis</a>
		                                              </div>
		                                              <div class="col-md-4 col-sm-4 average-time">
		                                                <h4>Your Chapter-wise Analysis</h4>
		                                                <p>Weak area : Coordination Compounda and Org...</p>
		                                                
		                                                <a data-toggle="tab" href="#topicwise" class="pull-right topicwise">Chapter-wise Analysis</a>
		                                              </div>
		                                            </div>
		                                          </div>
		                                        </div> -->

		                                      </div>
		                                    </div>
		                                  </div>
		                                  <!-- End Brief -->
		                                  <!-- start Percentile Rank -->
		                                  <div id="percentilerank" class="tab-pane">
		                                    <h1>Percentile Rank</h1>
		                                      <div class="row">
		                                        <div class="col-md-12">
		                                          <div id="percentile-pie-chart" style="height: 300px;"></div>
		                                        </div>
		                                      </div>
		                                      <div class="row">
		                                        <div class="col-md-12">
		                                          <table id="tests-detail-table" class="table table-striped table-bordered table-hover">
		                                            <thead>
		                                              <tr>
		                                                <th>
		                                                  TEST ID
		                                                </th>
		                                                <th>
		                                                  No. Of Q
		                                                </th>
		                                                <th>
		                                                  Qu. Attempted
		                                                </th>
		                                                <th>
		                                                  Correct Qu.
		                                                </th>
		                                                <th>
		                                                  Net Score
		                                                </th>
		                                                <th>
		                                                  Per Marks
		                                                </th>
		                                                <th>
		                                                  Speed
		                                                </th>
		                                                <th>
		                                                  Strike Rate
		                                                </th>
		                                                <th>
		                                                  EAS
		                                                </th>
		                                              </tr>
		                                            </thead>
		                                            <tbody></tbody>
		                                          </table>
		                                        </div> 
		                                      </div>
		                                        

		                                  </div>
		                                  <!-- End Percentile Rank -->
		                                  <!-- start success gap -->
		                                  <div id="successgap" class="tab-pane">
		                                    <h4 class="text-primary"><strong>Success Gap Analysis</strong></h4>
		                                    <div class="row">
		                                      <div class="col-md-6">
		                                        <div id="success-gap-graph"></div>
		                                      </div>
		                                      <div class="col-md-6">
		                                        <div id="topper-gap-graph"></div>
		                                      </div>
		                                    </div>
		                                    <div class="margin-top-10 margin-bottom-10 table-scrollable">
		                                      <table id="section-wise-report-table" class="table table-striped table-bordered table-hover">
		                                        <thead>
		                                        	<tr>
														<th rowspan="2" class="text-center">
															section name		                                            
														</th>
			                                            <th colspan="3" class="text-center">
			                                            	Success Gap
			                                            </th>
			                                            <th colspan="2" class="text-center">
			                                              Topper Gap
			                                            </th>
			                                        </tr>													
													<tr>
														<th>
			                                    			Marks  
			                                            </th>
			                                            <th>
			                                    			Cutoff Marks  
			                                            </th>
			                                            <th>
			                                    			Success Gap
			                                            </th>
			                                            <th>
			                                    			Topper Marks
			                                            </th>
			                                            <th>
			                                        		Topper Gap
			                                            </th>
		                                        	</tr>
		                                        </thead>
		                                        <tbody>
		                                          
		                                        </tbody>
		                                      </table>
		                                    </div>
		                                  </div><!-- End success gap -->
		                                  <!-- start question wise -->
		                                  <div id="questionwise" class="tab-pane">
		                                    <h4 class="text-primary"><strong>Question wise Reports</strong></h4>
		                                    <div class="margin-top-10 margin-bottom-10 table-scrollable">
		                                      <table id="question-wise-report-table" class="table table-striped table-bordered table-hover">
		                                        <thead>
		                                        <tr>
		                                          <th>
		                                            Q No.
		                                          </th>
		                                          <th>
		                                            Unit
		                                          </th>
		                                          <th>
		                                            Chapter
		                                          </th>
		                                          <th>
		                                            Topics
		                                          </th>
		                                          <th>
		                                            Difficulty
		                                          </th>
		                                          <th>
		                                            Section
		                                          </th>
		                                          <th>
		                                            Correct
		                                          </th>
		                                          <th>
		                                            Time<br>taken<br>(sec)
		                                          </th>
		                                          <th>
		                                            Explanation
		                                          </th>
		                                        </tr>
		                                        </thead>
		                                        <tbody></tbody>
		                                      </table>
		                                    </div>
		                                  </div><!-- End question wise -->
		                                  <!-- start topic wise -->
		                                  <div id="topicwise" class="tab-pane">
		                                    <div class="row">
		                                      <div class="col-md-12">
		                                        <h4 class="text-primary"><strong>Chapter wise report</strong></h4>
		                                        <div class="row">
		                                          <div class="col-md-12 col-sm-12">
		                                            <!-- <div id="chart_topicwise_reports" class="chart" style="height: 400px;"></div> -->
		                                            <div class="mrg-tb20">
		                                              <div id="topic-wise-report-chart"></div>
		                                            </div>
		                                          </div>
		                                        </div>
		                                        <div class="margin-top-10 margin-bottom-10 table-scrollable">
		                                          <table id="topic-wise-report-table" class="table table-striped table-bordered table-hover">
		                                            <thead>
		                                            <tr>
		                                              <th>
		                                                Section
		                                              </th>
		                                              <th>
		                                                Name of Chapter
		                                              </th>
		                                              <th>
		                                                Total Question
		                                              </th>
		                                              <th>
		                                                Correct / Attempt
		                                              </th>
		                                              <th>
		                                                Maximum Marks
		                                              </th>
		                                              <th>
		                                                Marks Scored
		                                              </th>
		                                              <th>
		                                                % Marks
		                                              </th>
		                                            </tr>
		                                            </thead>
		                                            <tbody></tbody>
		                                          </table>
		                                        </div>
		                                      </div>
		                                    </div>
		                                  </div><!-- End topic wise -->
		                                  <!-- start skill wise -->
		                                  <div id="skillwise" class="tab-pane">
		                                    <div class="row">
		                                      <div class="col-md-12">
		                                        <h4 class="text-primary"><strong>Skill wise report</strong></h4>
		                                      <div class="row">
		                                        <div class="col-md-12 col-sm-12">
		                                          <!-- <div id="chart_skillwise_reports" class="chart" style="height: 400px;"></div> -->
		                                          <div class="mrg-tb20">
		                                              <div id="skill-wise-report-chart"></div>
		                                          </div>
		                                        </div>
		                                      </div>
		                                        <div class="margin-top-10 margin-bottom-10 table-scrollable">
		                                          <table id="difficulty-wise-report-table" class="table table-striped table-bordered table-hover">
		                                            <thead>
		                                            <tr>
		                                              <th>
		                                                Section
		                                              </th>
		                                              <th>
		                                                Name of Skill
		                                              </th>
		                                              <th>
		                                                Total Question
		                                              </th>
		                                              <th>
		                                                Correct / Attempt
		                                              </th>
		                                              <th>
		                                                Maximum Marks
		                                              </th>
		                                              <th>
		                                                Marks Scored
		                                              </th>
		                                              <th>
		                                                % Marks
		                                              </th>
		                                            </tr>
		                                            </thead>
		                                            <tbody></tbody>
		                                          </table>
		                                        </div>
		                                      </div>
		                                    </div>
		                                  </div><!-- End skill wise -->
		                                  <!-- start difficulty wise -->
		                                  <div id="difficultywise" class="tab-pane">
		                                    <div class="row">
		                                      <div class="col-md-12">
		                                        <h4 class="text-primary"><strong>Difficulty wise report</strong></h4>
		                                        <div class="row">
		                                          <div class="col-md-12 col-sm-12">
		                                              <div id="difficulty-wise-report-chart"></div>
		                                          </div>
		                                        </div>
		                                        <div class="margin-top-10 margin-bottom-10 table-scrollable">
		                                          <table id="skill-wise-report-table" class="table table-striped table-bordered table-hover">
		                                            <thead>
		                                            <tr>
		                                              <th>
		                                                Section
		                                              </th>
		                                              <th>
		                                                Difficulty Level
		                                              </th>
		                                              <th>
		                                                Total Question
		                                              </th>
		                                              <th>
		                                                Correct / Attempt
		                                              </th>
		                                              <th>
		                                                Maximum Marks
		                                              </th>
		                                              <th>
		                                                Marks Scored
		                                              </th>
		                                              <th>
		                                                % Marks
		                                              </th>
		                                            </tr>
		                                            </thead>
		                                            <tbody></tbody>
		                                          </table>
		                                        </div>
		                                      </div>
		                                    </div>
		                                  </div><!-- End difficulty wise -->
		                                  <!-- start time wise -->
		                                  <div id="timewise" class="tab-pane">
		                                    <div class="row">
		                                      <div class="col-md-12">
		                                        <h4 class="text-primary"><strong>Time wise report</strong></h4>
		                                        <div class="row">
		                                          <div class="col-md-12">
		                                            <h4><strong>Your Time Takers</strong></h4>
		                                            <div class="margin-top-10 margin-bottom-10 table-scrollable">
		                                              <table id="time-takers-table" class="table table-striped table-bordered table-hover">
		                                                <thead>
		                                                <tr>
		                                                  <th>
		                                                    Q No.
		                                                  </th>
		                                                  <th>
		                                                    Chapter
		                                                  </th>
		                                                  <th>
		                                                    Diff. Level
		                                                  </th>
		                                                  <th>
		                                                    Your Time
		                                                  </th>
		                                                  <th>
		                                                    Ideal Time
		                                                  </th>
		                                                </tr>
		                                                </thead>
		                                                <tbody></tbody>
		                                                <tfoot></tfoot>
		                                              </table>
		                                            </div>
		                                          </div>
		                                          <div class="col-md-12">
		                                            <h4 style="margin-bottom:0px;"><strong>Your Time Savers</strong></h4>
		                                            <div class="margin-bottom-10 table-scrollable">
		                                              <table  id="time-savers-table" class="table table-striped table-bordered table-hover">
		                                                <thead>
		                                                <tr>
		                                                  <th>
		                                                    Q No.
		                                                  </th>
		                                                  <th>
		                                                    Chapter
		                                                  </th>
		                                                  <th>
		                                                    Diff. Level
		                                                  </th>
		                                                  <th>
		                                                    Your Time
		                                                  </th>
		                                                  <th>
		                                                    Ideal Time
		                                                  </th>
		                                                </tr>
		                                                </thead>
		                                                <tbody></tbody>
		                                                <tfoot></tfoot>
		                                              </table>
		                                            </div>
		                                          </div>
		                                        </div>
		                                      </div>
		                                    </div>
		                                  </div><!-- End time wise -->
		                                  <!-- start detailed test report -->
		                                  <div id="detailedtestreport" class="tab-pane">
                                    <div class="row">
                                      <div class="col-md-12">
                                        <h4 class="text-primary"><strong>Detailed Test Report</strong></h4>
                                        <div class="row mrg-top40">
                                          <div class="col-md-4">
                                            <div class="form-group">
                                              <div class="row">
                                                <div class="col-md-12">
                                                  <select id="short-questions-dorp-down" class="form-control">
                                                    <option value="0">Default</option>
                                                    <option value="1">Correct</option>
                                                    <option value="2">In-Correct</option>
                                                    <option value="3">Not Attempt</option>
                                                  </select>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-md-8"></div>
                                        </div>
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="bor-ccc light-bg-color">
                                              <div>
                                                <div class="col-md-6">
                                                  <div class="time-taken"><h4 class=""><i class="fa fa-clock-o"></i> Time Taken</h4></div>
                                                </div>
                                                <div class="col-md-6">
                                                  <div class="score  pull-right"><span>Score:<span class="student-score"></span></span></div>
                                                </div>
                                              </div>
                                              <div class="detailed-Q-R border-top-ccc white-bg">
                                                <div class="detailed-Q-R-div border-right-ccc">
                                                  <h4>Total Question</h4>
                                                  <h5 class="total-questions">20</h5>
                                                </div>
                                                <div class="detailed-Q-R-div border-right-ccc">
                                                  <h4>Attempted </h4>
                                                  <h5 class="attemted-questions">20</h5>
                                                </div>
                                                <div class="detailed-Q-R-div border-right-ccc">
                                                  <h4>Correct</h4>
                                                  <h5 class="correct-answers">10</h5>
                                                </div>
                                                <div class="detailed-Q-R-div border-right-ccc">
                                                  <h4>Wrong</h4>
                                                  <h5 class="wrong">7</h5>
                                                </div>
                                                <div class="detailed-Q-R-div">
                                                  <h4>Skipped</h4>
                                                  <h5 class="unattempted">3</h5>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div id="detailed-questions-report">
                                          <!-- start SCQ type -->
                                            <div class="row mrg-top40">
                                              <div class="col-md-12">
                                                <div class="row">
                                                  <div class="col-md-12">
                                                    <div class="col-md-1">
                                                      <h4><strong>Q 1.</strong></h4>
                                                    </div>
                                                    <div class="col-md-11 borderccc">
                                                      <div class="well mrg-bot20">
                                                        SCQ... When N is divided by 4, the remainder is 3. What is the remainder when 2N is divided by 4?
                                                      </div>
                                                      <div class="row option padd-left30">
                                                        <div class="col-md-12">
                                                          <div class="pull-left">
                                                            <p>4 </p>
                                                          </div>
                                                          <div class="pull-left mrg-left5">
                                                            <p class="in-correct"><i class="fa fa-times"></i></p>
                                                          </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                          <div class="pull-left">
                                                            <p>2 </p>
                                                          </div>
                                                          <div class="pull-left mrg-left5">
                                                            <p class="correct"><i class="fa fa-check"></i></p>
                                                          </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                          <div class="pull-left">
                                                            <p>3 </p>
                                                          </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                          <div class="pull-left">
                                                            <p>8 </p>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-12">
                                                          <h5 class="pull-right">Score <span>0/2</span></h5>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-1"></div>
                                                  <div class="col-md-11">
                                                    <!-- <div class="mrg-tb20 correct-answer">
                                                      <h4><strong>Correct Answer : </strong> <span>2</span></h4>
                                                    </div> -->
                                                    <h4><strong>Solution / Explanation</strong></h4>
                                                    <div class="borderccc sol-expla-scrllbar">
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                    </div>
                                                    <div class="row mrg-top20">
                                                      <div class="col-md-12 col-sm-12">
                                                        <h4><strong>Choose Option :</strong></h4>
                                                        <div class="form-group">
                                                          <div class="checkbox-list">
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox1" value="option1"> Revision </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox2" value="option2"> Unable to Soluion </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox3" value="option3"> Important Question </label>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <!-- End SCQ type -->
                                            <!-- start MAQ type -->
                                            <div class="row mrg-top40">
                                              <div class="col-md-12">
                                                <div class="row">
                                                  <div class="col-md-12">
                                                    <div class="col-md-1">
                                                      <h4><strong>Q 2.</strong></h4>
                                                    </div>
                                                    <div class="col-md-11 borderccc">
                                                      <div class="well mrg-bot20">
                                                        MAQ... When N is divided by 4, the remainder is 3. What is the remainder when 2N is divided by 4?
                                                      </div>
                                                      <div class="row option padd-left30">
                                                        <div class="col-md-12">
                                                          <div class="pull-left">
                                                            <p>4 </p>
                                                          </div>
                                                          <div class="pull-left mrg-left5">
                                                            <p class="in-correct"><i class="fa fa-times"></i></p>
                                                          </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                          <div class="pull-left">
                                                            <p>2 </p>
                                                          </div>
                                                          <div class="pull-left mrg-left5">
                                                            <p class="correct"><i class="fa fa-check"></i></p>
                                                          </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                          <div class="pull-left">
                                                            <p>3 </p>
                                                          </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                          <div class="pull-left">
                                                            <p>8 </p>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-12">
                                                          <h5 class="pull-right">Score <span>0/2</span></h5>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-1"></div>
                                                  <div class="col-md-11">
                                                    <div class="mrg-tb20 correct-answer">
                                                      <h4><strong>Correct Answer : </strong> <span>2</span></h4>
                                                    </div>
                                                    <h4><strong>Solution / Explanation</strong></h4>
                                                    <div class="borderccc sol-expla-scrllbar">
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                    </div>
                                                    <div class="row mrg-top20">
                                                      <div class="col-md-12 col-sm-12">
                                                        <h4><strong>Choose Option :</strong></h4>
                                                        <div class="form-group">
                                                          <div class="checkbox-list">
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox1" value="option1"> Revision </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox2" value="option2"> Unable to Soluion </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox3" value="option3"> Important Question </label>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <!-- End MAQ type -->
                                            <!-- start Secquence type -->
                                            <div class="row mrg-top40">
                                              <div class="col-md-12">
                                                <div class="row">
                                                  <div class="col-md-12">
                                                    <div class="col-md-1">
                                                      <h4><strong>Q 3.</strong></h4>
                                                    </div>
                                                    <div class="col-md-11 borderccc">
                                                      <div class="well mrg-bot20">
                                                        Secquence... When N is divided by 4, the remainder is 3. What is the remainder when 2N is divided by 4?
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-12">
                                                          <ul class="sequencing-list ui-sortable list-style-none">
                                                            <li class="option mrg-bot5 ui-sortable-handle">
                                                              <div class="row">
                                                                <div class="btn btn-default col-md-4"> 
                                                                  <label draggable="true" style="display:block"> 
                                                                    <div class="pull-left">
                                                                      <p>S...hohogtflgnkl</p>
                                                                    </div> 
                                                                  </label>
                                                                </div>
                                                              </div>
                                                            </li>
                                                            <li class="option mrg-bot5 ui-sortable-handle">
                                                              <div class="row">
                                                                <div class="btn btn-default col-md-4"> 
                                                                  <label draggable="true" style="display:block"> 
                                                                    <div class="pull-left">
                                                                      <p>S...jkfdhkfdsndsndd</p>
                                                                    </div> 
                                                                  </label>
                                                                </div>
                                                              </div>
                                                            </li>
                                                            <li class="option mrg-bot5 ui-sortable-handle">
                                                              <div class="row">
                                                                <div class="btn btn-default col-md-4"> 
                                                                  <label draggable="true" style="display:block"> 
                                                                    <div class="pull-left">
                                                                      <p>S....efofhldfnughg</p>
                                                                    </div> 
                                                                  </label>
                                                                </div>
                                                              </div>
                                                            </li>
                                                            <li class="option mrg-bot5 ui-sortable-handle">
                                                              <div class="row">
                                                                <div class="btn btn-default col-md-4"> 
                                                                  <label draggable="true" style="display:block"> 
                                                                    <div class="pull-left">
                                                                      <p>S..dgdfgf dfdsf</p>
                                                                    </div> 
                                                                  </label>
                                                                </div>
                                                              </div>
                                                            </li>
                                                          </ul>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-12">
                                                          <h5 class="pull-right">Score <span>0/2</span></h5>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-1"></div>
                                                  <div class="col-md-11">
                                                    <div class="mrg-tb20 correct-answer">
                                                      <h4><strong>Correct Answer : </strong> <span>2</span></h4>
                                                    </div>
                                                    <h4><strong>Solution / Explanation</strong></h4>
                                                    <div class="borderccc sol-expla-scrllbar">
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                    </div>
                                                    <div class="row mrg-top20">
                                                      <div class="col-md-12 col-sm-12">
                                                        <h4><strong>Choose Option :</strong></h4>
                                                        <div class="form-group">
                                                          <div class="checkbox-list">
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox1" value="option1"> Revision </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox2" value="option2"> Unable to Soluion </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox3" value="option3"> Important Question </label>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <!-- End Secquence type -->
                                            <!-- start Match the following type -->
                                            <div class="row mrg-top40">
                                              <div class="col-md-12">
                                                <div class="row">
                                                  <div class="col-md-12">
                                                    <div class="col-md-1">
                                                      <h4><strong>Q 4.</strong></h4>
                                                    </div>
                                                    <div class="col-md-11 borderccc">
                                                      <div class="well mrg-bot20">
                                                        Match the Following... When N is divided by 4, the remainder is 3. What is the remainder when 2N is divided by 4?
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-6"> 
                                                          <div class="col-md-11"> 
                                                            <div class="row" style="border: 1px solid #ccc; padding: 10px 10px 0px;">
                                                              <div class="col-md-12 mrg-bot10" style="border: 1px solid #ccc;">
                                                                <p>M....gudgfdsv</p>
                                                              </div>
                                                              <div class="col-md-12 mrg-bot10" style="border: 1px solid #ccc;">
                                                                <p>M...ifbdfdfsdbnlf</p>
                                                              </div>
                                                              <div class="col-md-12 mrg-bot10" style="border: 1px solid #ccc;">
                                                                <p>M.fdkbdkfdks</p>
                                                              </div>
                                                              <div class="col-md-12 mrg-bot10" style="border: 1px solid #ccc;">
                                                                <p>M..ugsdffvj</p>
                                                              </div>
                                                            </div> 
                                                          </div> 
                                                        </div>
                                                        <div class="col-md-6"> 
                                                          <div class="col-md-11"> 
                                                            <div class="row answers sequencing-list ui-sortable" style="border: 1px solid #ccc; padding: 10px 10px 0px;">
                                                              <div class="col-md-12 mrg-bot10 option ui-sortable-handle" style="border: 1px solid #ccc;">
                                                                <p>(M)...hjfbjkfvdufbdjkfj</p>
                                                              </div>
                                                              <div class="col-md-12 mrg-bot10 option ui-sortable-handle" style="border: 1px solid #ccc;">
                                                                <p>(M)..gvfidfbdsgf</p>
                                                              </div>
                                                              <div class="col-md-12 mrg-bot10 option ui-sortable-handle" style="border: 1px solid rgb(204, 204, 204); position: relative;">
                                                                <p>(M)....uydgbirfgeufhd</p>
                                                              </div>
                                                              <div class="col-md-12 mrg-bot10 option ui-sortable-handle" style="border: 1px solid #ccc;">
                                                                <p>(M).jlfsdnlfdhkf</p>
                                                              </div>
                                                            </div> 
                                                          </div> 
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-12">
                                                          <h5 class="pull-right">Score <span>0/2</span></h5>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-1"></div>
                                                  <div class="col-md-11">
                                                    <div class="mrg-tb20 correct-answer">
                                                      <h4><strong>Correct Answer : </strong> <span>2</span></h4>
                                                    </div>
                                                    <h4><strong>Solution / Explanation</strong></h4>
                                                    <div class="borderccc sol-expla-scrllbar">
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                    </div>
                                                    <div class="row mrg-top20">
                                                      <div class="col-md-12 col-sm-12">
                                                        <h4><strong>Choose Option :</strong></h4>
                                                        <div class="form-group">
                                                          <div class="checkbox-list">
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox1" value="option1"> Revision </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox2" value="option2"> Unable to Soluion </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox3" value="option3"> Important Question </label>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <!-- End Match the following type -->
                                            <!-- start FB Normal type -->
                                            <div class="row mrg-top40">
                                              <div class="col-md-12">
                                                <div class="row">
                                                  <div class="col-md-12">
                                                    <div class="col-md-1">
                                                      <h4><strong>Q 5.</strong></h4>
                                                    </div>
                                                    <div class="col-md-11 borderccc">
                                                      <div class="well mrg-bot20">
                                                        <p>Fill in blank normal&nbsp;
                                                          <input class="input-droppable ui-droppable" data-id="input2" disabled="true" size="8">
                                                          &nbsp;iufoi&nbsp;
                                                          <input class="input-droppable ui-droppable" data-id="input3" disabled="true" size="8">
                                                          &nbsp;ufds fjd&nbsp;
                                                          <input class="input-droppable ui-droppable" data-id="input4" disabled="true" size="8">&nbsp;dgdsgd&nbsp;
                                                          <input class="input-droppable ui-droppable" data-id="input5" disabled="true" size="8">
                                                          &nbsp;fdgfdg
                                                        </p>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-12">
                                                          <div class="row">
                                                            <div class="col-md-10 form-group"> 
                                                              <input type="text" class="option form-control" name="ord-option0" data-id="input2" id="option0" value="F.sagfskajfbkh  "> 
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                          </div>
                                                          <div class="row">
                                                            <div class="col-md-10 form-group"> 
                                                              <input type="text" class="option form-control" name="ord-option0" data-id="input2" id="option0" value="F..uyfdkjfvdfbdj  "> 
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                          </div>
                                                          <div class="row">
                                                            <div class="col-md-10 form-group"> 
                                                              <input type="text" class="option form-control" name="ord-option0" data-id="input2" id="option0" value="F...dsvfkdkhbdk  "> 
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                          </div>
                                                          <div class="row">
                                                            <div class="col-md-10 form-group"> 
                                                              <input type="text" class="option form-control" name="ord-option0" data-id="input2" id="option0" value="F....jhfvbdskgbdsjbg  "> 
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-12">
                                                          <h5 class="pull-right">Score <span>0/2</span></h5>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-1"></div>
                                                  <div class="col-md-11">
                                                    <div class="mrg-tb20 correct-answer">
                                                      <h4><strong>Correct Answer : </strong> <span>2</span></h4>
                                                    </div>
                                                    <h4><strong>Solution / Explanation</strong></h4>
                                                    <div class="borderccc sol-expla-scrllbar">
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                    </div>
                                                    <div class="row mrg-top20">
                                                      <div class="col-md-12 col-sm-12">
                                                        <h4><strong>Choose Option :</strong></h4>
                                                        <div class="form-group">
                                                          <div class="checkbox-list">
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox1" value="option1"> Revision </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox2" value="option2"> Unable to Soluion </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox3" value="option3"> Important Question </label>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <!-- End FB Normal type -->
                                            <!-- start FB Drop Down type -->
                                            <div class="row mrg-top40">
                                              <div class="col-md-12">
                                                <div class="row">
                                                  <div class="col-md-12">
                                                    <div class="col-md-1">
                                                      <h4><strong>Q 6.</strong></h4>
                                                    </div>
                                                    <div class="col-md-11 borderccc">
                                                      <div class="well mrg-bot20">
                                                        <p>
                                                          Fill in the blank Drop Down&nbsp;
                                                          <select style="width:10%">
                                                            <option> F1.2 hfkdsdsbfjdbs</option>
                                                            <option> F1.1 bfdsbdsbk</option>
                                                          </select>
                                                          &nbsp;gkfdskjfbdk&nbsp;
                                                          <select style="width:10%">
                                                            <option> F2.1 uhjdfbdfbd</option>
                                                            <option> F2.2 fdfdshgdsjb</option>
                                                          </select>
                                                          &nbsp;ggfdfd</p>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-12">
                                                          <label> Drop Down 1 Options </label>
                                                          <div class="row">
                                                            <div class="options col-md-10 form-group"> 
                                                              <input class="form-control" name="ord-option0" value="F1.2 hfkdsdsbfjdbs"> 
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                          </div>
                                                          <div class="row">
                                                            <div class="options col-md-10 form-group"> 
                                                              <input class="form-control" name="ord-option0" value="F1.1 bfdsbdsbk"> 
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                          </div>
                                                          <label> Drop Down 2 Options </label>
                                                          <div class="row">
                                                            <div class="options col-md-10 form-group"> 
                                                              <input class="form-control" name="ord-option0" value="F2.1 uhjdfbdfbd"> 
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                          </div>
                                                          <div class="row">
                                                            <div class="options col-md-10 form-group"> 
                                                              <input class="form-control" name="ord-option0" value="F2.2 fdfdshgdsjb"> 
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-12">
                                                          <h5 class="pull-right">Score <span>0/2</span></h5>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-1"></div>
                                                  <div class="col-md-11">
                                                    <div class="mrg-tb20 correct-answer">
                                                      <h4><strong>Correct Answer : </strong> <span>2</span></h4>
                                                    </div>
                                                    <h4><strong>Solution / Explanation</strong></h4>
                                                    <div class="borderccc sol-expla-scrllbar">
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                    </div>
                                                    <div class="row mrg-top20">
                                                      <div class="col-md-12 col-sm-12">
                                                        <h4><strong>Choose Option :</strong></h4>
                                                        <div class="form-group">
                                                          <div class="checkbox-list">
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox1" value="option1"> Revision </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox2" value="option2"> Unable to Soluion </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox3" value="option3"> Important Question </label>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <!-- End FB Drop Down type -->
                                            <!-- start FB Drag-n-Drop type -->
                                            <div class="row mrg-top40">
                                              <div class="col-md-12">
                                                <div class="row">
                                                  <div class="col-md-12">
                                                    <div class="col-md-1">
                                                      <h4><strong>Q 7.</strong></h4>
                                                    </div>
                                                    <div class="col-md-11 borderccc">
                                                      <div class="well mrg-bot20">
                                                        <p>Fill in the blank Drag-n-Drop&nbsp;
                                                          <input class="input-droppable ui-droppable" disabled="true" size="8">
                                                          &nbsp;hgvdsksvdj&nbsp;
                                                          <input class="input-droppable ui-droppable" disabled="true" size="8">
                                                          &nbsp;mjvmnv dv&nbsp;
                                                          <input class="input-droppable ui-droppable" disabled="true" size="8">
                                                          &nbsp;hgjgfgf&nbsp;
                                                          <input class="input-droppable ui-droppable" disabled="true" size="8">
                                                          &nbsp;hfhyd
                                                        </p>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-12">
                                                          <div class="Fb-list input-droppable ui-droppable">
                                                            <span class="option mrg-bot5"> 
                                                              <div class="mrg-bot10">
                                                                <span class="draggable border-ccc ui-draggable ui-draggable-handle" style="position: relative;">F-DnD... shufsjdhbfkdbj  </span>
                                                              </div>
                                                            </span>
                                                            <span class="option mrg-bot5"> 
                                                              <div class="mrg-bot10">
                                                                <span class="draggable border-ccc ui-draggable ui-draggable-handle" style="position: relative;"> F-DnD. jvdjbdk  
                                                                </span>
                                                              </div>
                                                            </span>
                                                            <span class="option mrg-bot5"> 
                                                              <div class="mrg-bot10">
                                                                <span class="draggable border-ccc ui-draggable ui-draggable-handle" style="position: relative;">F-DND.. ujfdkfvdufdu  </span>
                                                              </div>
                                                            </span>
                                                            <span class="option mrg-bot5"> 
                                                              <div class="mrg-bot10">
                                                                <span class="draggable border-ccc ui-draggable ui-draggable-handle" style="position: relative;">F-DnD.... uvfdjsfjdvfdsb fdj  
                                                                </span>
                                                              </div>
                                                            </span>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-12">
                                                          <h5 class="pull-right">Score <span>0/2</span></h5>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-1"></div>
                                                  <div class="col-md-11">
                                                    <div class="mrg-tb20 correct-answer">
                                                      <h4><strong>Correct Answer : </strong> <span>2</span></h4>
                                                    </div>
                                                    <h4><strong>Solution / Explanation</strong></h4>
                                                    <div class="borderccc sol-expla-scrllbar">
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                    </div>
                                                    <div class="row mrg-top20">
                                                      <div class="col-md-12 col-sm-12">
                                                        <h4><strong>Choose Option :</strong></h4>
                                                        <div class="form-group">
                                                          <div class="checkbox-list">
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox1" value="option1"> Revision </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox2" value="option2"> Unable to Soluion </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox3" value="option3"> Important Question </label>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <!-- End FB Drag-n-Drop type -->
                                            <!-- start Passage type -->
                                            <div class="row mrg-top40">
                                              <div class="col-md-12">
                                                <div class="row">
                                                  <div class="col-md-12">
                                                    <h4><strong>Passage</strong></h4>
                                                    <div class="well">
                                                      stdvsv rudbks cdcb c8gcb cuscm cubc  c bckx tcxcx cxcx cxcx cuxcxk
                                                      stdvsv rudbks cdcb c8gcb cuscm cubc  c bckx tcxcx cxcx cxcx cuxcxk
                                                      stdvsv rudbks cdcb c8gcb cuscm cubc  c bckx tcxcx cxcx cxcx cuxcxk
                                                      stdvsv rudbks cdcb c8gcb cuscm cubc  c bckx tcxcx cxcx cxcx cuxcxk
                                                      stdvsv rudbks cdcb c8gcb cuscm cubc  c bckx tcxcx cxcx cxcx cuxcxk
                                                      stdvsv rudbks cdcb c8gcb cuscm cubc  c bckx tcxcx cxcx cxcx cuxcxk
                                                      stdvsv rudbks cdcb c8gcb cuscm cubc  c bckx tcxcx cxcx cxcx cuxcxk
                                                      stdvsv rudbks cdcb c8gcb cuscm cubc  c bckx tcxcx cxcx cxcx cuxcxk
                                                      stdvsv rudbks cdcb c8gcb cuscm cubc  c bckx tcxcx cxcx cxcx cuxcxk
                                                      stdvsv rudbks cdcb c8gcb cuscm cubc  c bckx tcxcx cxcx cxcx cuxcxk
                                                    </div>
                                                    <div class="col-md-1">
                                                      <h4><strong>Q 8.</strong></h4>
                                                    </div>
                                                    <div class="col-md-11 borderccc">
                                                      <div class="well mrg-bot20">
                                                        Gvjegfd ifdhfd fdfdgfd sfdv fd fkd vufdlfd fdslf dsufdslf dsfdskf dsuf dsgfds v fdfds dus fd 
                                                      </div>
                                                      <div class="row option padd-left30">
                                                        <div class="col-md-12">
                                                          <div class="pull-left">
                                                            <p>ojfhjgnfi </p>
                                                          </div>
                                                          <div class="pull-left mrg-left5">
                                                            <p class="in-correct"><i class="fa fa-times"></i></p>
                                                          </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                          <div class="pull-left">
                                                            <p>kihfkdvjdj </p>
                                                          </div>
                                                          <div class="pull-left mrg-left5">
                                                            <p class="correct"><i class="fa fa-check"></i></p>
                                                          </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                          <div class="pull-left">
                                                            <p>onblnjff </p>
                                                          </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                          <div class="pull-left">
                                                            <p>fbvdkfbdkb </p>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-12">
                                                          <h5 class="pull-right">Score <span>0/2</span></h5>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-1"></div>
                                                  <div class="col-md-11">
                                                    <div class="mrg-tb20 correct-answer">
                                                      <h4><strong>Correct Answer : </strong> <span>2</span></h4>
                                                    </div>
                                                    <h4><strong>Solution / Explanation</strong></h4>
                                                    <div class="borderccc sol-expla-scrllbar">
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                    </div>
                                                    <div class="row mrg-top20">
                                                      <div class="col-md-12 col-sm-12">
                                                        <h4><strong>Choose Option :</strong></h4>
                                                        <div class="form-group">
                                                          <div class="checkbox-list">
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox1" value="option1"> Revision </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox2" value="option2"> Unable to Soluion </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox3" value="option3"> Important Question </label>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <!-- End Passage type -->
                                            <!-- start Matrix type -->
                                            <div class="row mrg-top40">
                                              <div class="col-md-12">
                                                <div class="row">
                                                  <div class="col-md-12">
                                                    <div class="col-md-1">
                                                      <h4><strong>Q 9.</strong></h4>
                                                    </div>
                                                    <div class="col-md-11 borderccc">
                                                      <div class="well mrg-bot20">
                                                        Matrix... When N is divided by 4, the remainder is 3. What is the remainder when 2N is divided by 4?
                                                      </div>
                                                      <div class="row option padd-left30">
                                                        <div class="col-md-11 matrix-table-options"> 
                                                          <div class="margin-bottom-10 table-scrollable">
                                                            <table id="matrix-table" class="table table-bordered table-striped table-hover">
                                                              <thead>
                                                                <tr>
                                                                  <th></th>
                                                                  <th> 
                                                                    <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;">ravdyf</div> 
                                                                  </th>
                                                                  <th> 
                                                                    <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;">tyu</div> 
                                                                  </th>
                                                                  <th> 
                                                                    <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;">fu</div> 
                                                                  </th>
                                                                  <th> 
                                                                    <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;">tyu</div> 
                                                                  </th>
                                                                </tr> 
                                                              </thead> 
                                                              <tbody>
                                                                <tr> 
                                                                  <td> 
                                                                    <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"> edfgrtert </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" checked="true" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" checked="true" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                </tr>
                                                                <tr> 
                                                                  <td> 
                                                                    <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"> werrycfgh </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" checked="true" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" checked="true" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                </tr>
                                                                <tr> 
                                                                  <td> 
                                                                    <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"> wqeeterqw </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" checked="true" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                </tr>
                                                                <tr> 
                                                                  <td> 
                                                                    <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"> tert </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" checked="true" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                </tr>
                                                                <tr> 
                                                                  <td> 
                                                                    <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"> ert </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" checked="true" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" checked="true" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                  <td>
                                                                    <div class="checkbox-list">
                                                                      <label>
                                                                        <input type="checkbox" disabled="">
                                                                      </label>
                                                                    </div>
                                                                  </td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </div> 
                                                        </div>
                                                      </div>
                                                      <div class="row">
                                                        <div class="col-md-12">
                                                          <h5 class="pull-right">Score <span>0/2</span></h5>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-1"></div>
                                                  <div class="col-md-11">
                                                    <div class="mrg-tb20 correct-answer">
                                                      <h4><strong>Correct Answer : </strong> <span>2</span></h4>
                                                    </div>
                                                    <h4><strong>Solution / Explanation</strong></h4>
                                                    <div class="borderccc sol-expla-scrllbar">
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                      <p>ugfiif tifdfdsuifd ghjffhyd<br> tyhsafygi  9sbf  gbnl d<br> dy9d </p>
                                                    </div>
                                                    <div class="row mrg-top20">
                                                      <div class="col-md-12 col-sm-12">
                                                        <h4><strong>Choose Option :</strong></h4>
                                                        <div class="form-group">
                                                          <div class="checkbox-list">
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox1" value="option1"> Revision </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox2" value="option2"> Unable to Soluion </label>
                                                            <label class="checkbox-inline">
                                                            <input type="checkbox" id="inlineCheckbox3" value="option3"> Important Question </label>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          <!-- End Matrix type -->
                                        </div>
                                      </div>
                                    </div>
                                  </div>
		                                  <!-- End  detailed test report -->
		                                </div>
		                              </div>
		                            </div>
		                          </div>
		                        </div>
		                        <div class="tab-pane" id="tab2">
		                          <h1>Comparative</h1>
		                        </div><!-- end tab2 -->
		                      </div><!-- end tab-content -->
		                      </div>
		                    </div>
		                  </div>
		                </div>

		              </div>
		      		</div>
		          </div>
		          <!-- END CONTENT -->
				</div>
			</div>

			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<div class="modal fade" id="question-explanation-modal" tabindex="-1" data-backdrop="static" role="basic" aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Question</h4>
		</div>
		<div class="modal-body">
		<div class="row">
		<div class="col-md-12 col-sm-12 mrg-bot20">
		  <h4 id="selected-question" class="text-primary"></h4>
		</div>
		</div>
		<div class="row">
		<div class="col-md-12 col-sm-12">
		  <p>Options :-</p>
		</div>
		  
		    <div id="question-options">
		      <div class="col-md-12 col-sm-12">
		        <h5> <strong>1 :</strong> </h5>
		      </div>
		      <div class="col-md-12 col-sm-12">
		      <h5> <strong>2 :</strong> </h5>
		      </div>
		      <div class="col-md-12 col-sm-12">
		      <h5> <strong>3 :</strong> </h5>
		      </div>
		      <div class="col-md-12 col-sm-12">
		      <h5> <strong>4 :</strong> </h5>
		      </div>
		    </div>
		  
		</div>
		<div class="row">
		<div class="col-md-12 col-sm-12 mrg-top20 mrg-bot10">
		  <h5>Solution / Explanation</h5>
		  <div id="solution-explanation" class="borderccc sol-expla-scrllbar">
		    <p>dgfdh fdf dfdh flf dfdfkdodslfdf dfd h ddnd ddshfdnfld dsi dfdh  ijv </p>
		  </div>
		</div>
		<div class="col-md-12 col-sm-12">
		  <h5> <strong>Answer Given:</strong> </h5>
		  <div id="student-answers">  </div>
		</div>
		</div>


		</div>
		<div class="modal-footer">
			<button type="button" class="btn default" data-dismiss="modal">Close</button>
		</div>
		</div>
		<!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php' ?>

<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script> -->
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script src="assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="assets/global/plugins/highcharts/highcharts.js" type="text/javascript"></script>
<script src="assets/global/plugins/highcharts/highcharts-3d.js" type="text/javascript"></script>
<script src="assets/global/plugins/highcharts/modules/exporting.js" type="text/javascript"></script>


<script type="text/javascript" src="js/custom/detailed-report.js"></script>
<script src="js/common.js" type="text/javascript"></script>
<script src="js/custom/student-reports.js" type="text/javascript"></script>

<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features 
   /*Index.init();   
   Index.initDashboardDaterange();
   Index.initJQVMAP(); // init index page's custom scripts
   Index.initCalendar(); // init index page's custom scripts
   Index.initCharts(); // init index page's custom scripts
   Index.initChat();
   Index.initMiniCharts();
   Tasks.initDashboardWidget();*/
});
</script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(13)').addClass('active open');
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>