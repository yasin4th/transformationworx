var EndPoint = '../vendor/api.php';
var FormDataEndPoint = '../vendor/fileUploads.php';

$(function(){
	//mathsjax();

	/*
	* function to show ajax loader
	*
	*/
	$(document).ajaxStart(function(){
	    $('#loader').show();
	    // $('#loader').fadeIn();
	});

	/*
	* function to hide ajax loader
	*
	*/
	$(document).ajaxComplete(function(){
	    $('#loader').hide();
	    // $('#loader').fadeOut();
	});



// function
	$('#logout').on('click',function(){
		req = {};
		req.action = "logout";
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			console.log(res);
			$.parseJSON(res);
		});
	});

});

function getUrlParameter(sParam) {
	sParam = sParam.toLowerCase();
	var sPageURL = window.location.search.substring(1).toLowerCase();
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++)
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam)
		{
			return sParameterName[1];
		}
	}
}
function setError(where, what) {
	var parent = $(where).parents('div:eq(0)');
	parent.addClass('has-error').append('<span class="help-block">' + what + '</span>');
}

function unsetError(where) {
	var parent = $(where).parents('div:eq(0)');
	parent.removeClass('has-error').find('.help-block').remove();
}

function mathsjax() {
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src  = "http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML";
	document.getElementsByTagName("head")[0].appendChild(script);
}

String.prototype.caps = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

String.prototype.esc = function() {
	return this.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"').replace(/&amp;/g, '&');
}

Number.prototype.commas = function() {
    return this.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};