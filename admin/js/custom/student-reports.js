
var test;
var student;
var correct;
var wrong;
var unattempted;
var your_score;
var topper_score;
var average_score;
var attempt_id;
var question_avg_time;
var test_paper_time;
var test_paper_avg_time;

$(function() {
	test  = getUrlParameter("test");
	student = getUrlParameter("student");

	fetchSelectedStudentTestReport();
	mathsjax();
});


function fetchSelectedStudentTestReport() {
	var req = {};
	req.test_paper = test;
	req.student = student;
	req.action = 'fetchSelectedStudentTestReport';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 1) {

			/*
			* for breif pie chart
			*/
            $('#paper-name').html(res.test_paper);
			correct = res.correct;
			wrong = res.wrong;
			unattempted = res.unattempted;
			question_attempt_pie_chart();
            test_paper_time = res.test_paper_time * 60;
            test_paper_avg_time = parseInt(test_paper_time)/parseInt(res.total_questions);
			/*
			* for breif serial chart
			*/
			your_score = res.your_score;
			topper_score = res.topper_score;
			average_score = res.average_score;

			question_attempt_serial_chart();
            question_avg_time = parseInt(res.percentileTable.total_time / res.percentileTable.total_questions);

            if(res.topicWiseReportChart.length == 0)
            {
                
                    $('.topicwise').off('click');
                    $('.topicwise').on('click', function(e) {
                        e.preventDefault();
                    });
            }


			createPercentilePieChart();
			createTopicWiseReportChart(res.topicWiseReportChart);
			createDifficultyWiseReportChart(res.difficultyWiseReportChart);
			createSkillWiseReportChart(res.skillWiseReportChart);
			
			percentileTable(res.percentileTable);
			createQuestionWiseReport(res.questionWiseReportRows);
			createTopicWiseReportTable(res.topicWiseReportChart);
			createDifficultyWiseReportTable(res.skillWiseReportChart);
			createSkillWiseReportTable(res.difficultyWiseTableReportRows);
			cerateDetailedReport(res.detailedReport);
            fillSuccessgapTable(res.section_wise_report);

			setEventShowQuestionExplanationModal();
           
            
            $('#percentile-rank').html(res.percentile_rank);
			$(".student-name").text(res.student.first_name+' '+res.student.last_name);
			$(".correct-answers").text(res.correct+'/'+res.total_questions);
			$(".wrong").text(res.wrong+'/'+res.total_questions);
            $(".your-score").text(res.your_score);
			// $(".your-score").text(res.your_score+'/'+res.percentileTable.total_marks);
			$(".wrong-unattempted").text((res.total_questions - res.correct)+'/'+res.total_questions);
			$(".unattempted").text(res.unattempted+'/'+res.total_questions);
			$(".attemted-questions").text(res.attempted);
            $(".total-questions").text(res.total_questions);
            $(".test-date").text(res.test_date);
            // $(".total-score").text(res.percentileTable.total_marks);
			$(".student-score").text(res.percentileTable.marks_archived);

           $(".avg-time").text(parseInt(test_paper_avg_time / 60) + ' min ' + parseInt(test_paper_avg_time % 60) + ' sec');

            /*** Optimizer Report **/
            if (res.optimizer_id == 0) {
                $('.optmizer-test').remove();
            }
            else {
                $('.optmizer-test').attr('href', 'optmizer-report.php?paper='+res.optimizer_id);
            }
		};
		
	});
}

function question_attempt_pie_chart() {

   Highcharts.setOptions({
		colors: ['#90ee7e', 'rgb(149,206,255)', '#ff0066', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
	}); // red: #FF1212, blue: #223DE6, green: #22FF04,
    
    var question_attempt_pie_chart = $('#questions-pie-chart').highcharts({
        chart: {
            // plotBackgroundColor: null,
            // plotBorderWidth: null,
            // plotShadow: false,
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
                
            }
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                innerSize: 100,
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: "Percentage",
            colorByPoint: true,
            data: [{
                name: "Correct",
                y: correct
            },{
                name: "Unattempted",
                y: unattempted
            }, {
                name: "Wrong",
                y: wrong
            }]
        }]
    });
}

function question_attempt_serial_chart()
{

    your_score_1 = your_score;
    topper_score_1 = topper_score;
    average_score_1 = average_score;

    if(your_score < 0)
    {
        your_score_1 = 0;
    }
    if(topper_score < 0)
    {
        topper_score_1 = 0;
    }
    if(average_score < 0)
    {
        average_score_1 = 0;
    }
    
    var chart = AmCharts.makeChart("chart_5", {
        "theme": "light",
        "type": "serial",
        "startDuration": 2,

        "fontFamily": 'Open Sans',
        
        "color":    '#888',

        "dataProvider": [{
            "student": "You",
            "score": your_score_1,
            "color": "#FF0F00"
        }, {
            "student": "Topper",
            "score": topper_score_1,
            "color": "#04D215"
        }, {
            "student": "Average",
            "score": average_score_1,
            "color": "#2A0CD0"
        }],
        "valueAxes": [{
            "position": "left",
            "axisAlpha": 0,
            "gridAlpha": 0
        }],
        "graphs": [{
            "balloonText": "[[category]]: <b>[[value]]</b>",
            "colorField": "color",
            "fillAlphas": 0.85,
            "lineAlpha": 0.1,
            "type": "column",
            "topRadius": 1,
            "valueField": "score"
        }],
        "depth3D": 40,
        "angle": 30,
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "student",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0

        },
        "exportConfig": {
            "menuTop": "20px",
            "menuRight": "20px",
            "menuItems": [{
                "icon": '/lib/3/images/export.png',
                "format": 'png'
            }]
        }
    }, 0);
}


function createQuestionWiseReport(rows) {
   questionWisehtml = '';
    var time_taken = 0;
    var time_saved = 0;
    $.each(rows, function(i, row) {
        questionWisehtml += '<tr data-question_attempt_id='+row.id+'>'
        questionWisehtml += '<td>' +(i+1)+ '</td>' + '<td>' +row.units+ '</td>' + '<td>' +row.chapters+ '</td>' + '<td>' + row.topics + '</td>' + '<td>' +row.difficulty+ '</td>' + '<td>'+row.section+'</td>' + '<td>';
        if(row.correct == 0)
            questionWisehtml += '<p class="in-correct"><i class="fa fa-times"></i></p>';
        else
            questionWisehtml += '<p class="correct"><i class="fa fa-check"></i></p>';

        questionWisehtml += '</td>' + '<td>' +parseInt(row.time)+ '</td>' + '<td> <a class="question-explanation">View</a> </td>';
        questionWisehtml += '</tr>';

        timeWise_html = '';
        timeWise_html += '<tr data-question_attempt_id='+row.id+'>'
        timeWise_html += '<td><a class="question-explanation">' +(i+1)+ '</a></td>' + '<td>' +row.chapters+ '</td>' + '<td>' +row.difficulty+ '</td>' + '<td>' +parseInt(row.time)+ '</td>' + '<td>' +parseInt(test_paper_avg_time)+ '</td>';
        //timeWise_html += '<td><a class="question-explanation">' +(i+1)+ '</a></td>' + '<td>' +row.chapters+ '</td>' + '<td>' +row.difficulty+ '</td>' + '<td>' +parseInt(row.time)+ '</td>' + '<td>' +parseInt(row.avgtimeall)+ '</td>';
        timeWise_html += '</tr>';
        if(parseInt(row.time) > parseInt(test_paper_avg_time)) {
            time_taken += parseInt(row.time) - parseInt(test_paper_avg_time);
            $('#time-takers-table tbody').append(timeWise_html);
        } else{
            time_saved += parseInt(test_paper_avg_time) - parseInt(row.time);
            $('#time-savers-table tbody').append(timeWise_html);
        };
    });

    $('#question-wise-report-table tbody').html(questionWisehtml);                                  

    $('#time-takers-table tfoot').html('<tr><td colspan="5">You took '+parseInt(time_taken/60)+' minutes '+time_taken%60+' seconds extra than average time.</td></tr>');
    $('#time-savers-table tfoot').html('<tr><td colspan="5">You saved '+parseInt(time_saved/60)+' minutes '+time_saved%60+' seconds.</td></tr>');
}

function setEventShowQuestionExplanationModal() {
	$('.question-explanation').off('click');
	$('.question-explanation').on('click', function(e) {
		e.preventDefault();
		question_attempt_id = $(this).parents('tr:eq(0)').data('question_attempt_id');
		fetchQuestionExplanation(question_attempt_id);

		$('#question-explanation-modal').modal('show');
	})
}

function fetchQuestionExplanation(question_attempt_id) {
	var req = {};
	req.action = 'attemptedQuestionExplanation';
	req.question_attempt_id = question_attempt_id;
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req) 
	}).done(function(res){
		res = $.parseJSON(res);
		if (res.status == 1) {
			//console.log(res);
			fillQuestionExplanation(res.details);
		} else{
			
		}; 

	});
}

function fillQuestionExplanation(data) {
   
    
	$('#selected-question').html(data.question);
	$('#solution-explanation').html(data.description);
	var html = '';
	$.each(data.options, function(i, option) {
		html += '<div class="col-md-12 col-sm-12"><h5 class="float-left"> <strong> '+(i+1)+':&nbsp; </strong> '+option.option+' </h5></div>';
	})
	$('#question-options').html(html);

	var html = ''
	$.each(data.studentAnswers, function(i, answer) {
		html += '<div class="col-md-12 col-sm-12"><h5 class="float-left"> <strong> '+(i+1)+':&nbsp; </strong> '+answer.option+' </h5></div>';
	})
	$('#student-answers').html(html);

	MathJax.Hub.Typeset();

}

function  createTopicWiseReportChart (chart) {
	categories = [];
    attemted = [];
    unattempted = [];
    correct = [];
    wrong = [];
    x = [];

    $.each(chart, function(i, topic) {
        categories.push(topic.name);
        attemted.push(parseInt(topic.attempted));
        unattempted.push(parseInt(topic.unattempted));        
        correct.push(parseInt(topic.correct));
        wrong.push(parseInt(topic.wrong));        
        x.push(topic.section+'-'+topic.name);
    })
    $('#topic-wise-report-chart').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Chapter Wise Report'
        },
        xAxis: {
            categories: x
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total number of questions'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                pointWidth: 40,
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                }
            }
        },
        series: [{
            name: 'Unattempted',
            data: unattempted,
            color: '#dfba49'
            
        }, {
            name: 'Wrong',
            data: wrong,
            color: '#F72424'
        }, {
            name: 'Correct',
            data: correct,
            color: '#1CA22B'
        }]
    });
}


function  createSkillWiseReportChart (chart) {
	categories = [];
    attemted = [];
    unattempted = [];
    correct = [];
    wrong = [];
    x = [];
    
    if(chart.length == 0)
    {
        $('#skill-wise-report-chart').html('No Skill Wise Report Found.');
        return;
    }

    $.each(chart, function(i, topic) {
        categories.push(topic.name);
        attemted.push(parseInt(topic.attempted));
        unattempted.push(parseInt(topic.unattempted));        
        correct.push(parseInt(topic.correct));
        wrong.push(parseInt(topic.wrong));
        x.push(topic.section+'-'+topic.name);
    })
    $('#skill-wise-report-chart').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Skill Wise Report'
        },
        xAxis: {
            categories: x
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total number of questions'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                pointWidth: 40,
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                }
            }
        },
        series: [{
            name: 'Unattempted',
            data: unattempted,
            color: '#dfba49'
            
        }, {
            name: 'Wrong',
            data: wrong,
            color: '#F72424'
        }, {
            name: 'Correct',
            data: correct,
            color: '#1CA22B'
        }]
    });
}


function  createDifficultyWiseReportChart (chart) {
	categories = [];
    unattempted = [];
    correct = [];
    wrong = [];
    x = [];

    if(chart.length == 0)
    {
        $('#difficulty-wise-report-chart').html('No Difficulty Wise Report Found.');
        return;
    }

    $.each(chart, function(i, difficulty) {
        categories.push(difficulty.name);
        unattempted.push(parseInt(difficulty.unattempted));
        correct.push(parseInt(difficulty.correct));
        wrong.push(parseInt(difficulty.wrong));
        x.push(difficulty.section+'-'+difficulty.name);
    })

    $('#difficulty-wise-report-chart').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Difficulty Wise Report'
        },
        xAxis: {
            categories: x
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Questions'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                pointWidth: 40,
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                }
            }
        },
        series: [{
            name: 'Unattempted',
            data: unattempted,
            color: '#dfba49'
            
        }, {
            name: 'Wrong',
            data: wrong,
            color: '#F72424'
        }, {
            name: 'Correct',
            data: correct,
            color: '#1CA22B'
        }]
    });
}

function createTopicWiseReportTable(skillWiseTableReportRows) {
	html = '';
	$.each(skillWiseTableReportRows, function(i, row) {
		html += '<tr>'
		html += '<td>' +row.section+ '</td>' +'<td>' +row.name+ '</td>' + '<td>' +row.total_questions+ '</td>' + '<td>' + row.correct + '/' + row.attempted  + '</td>' + '<td>' +row.max_marks+ '</td>' + '<td>' +row.score+ '</td>' + '<td>' +(parseInt(row.score)*100/parseInt(row.max_marks)).toFixed(2) + '</td>';
        html += '</tr>'
	});
    if(skillWiseTableReportRows.length == 0)
    {
        $('#topic-wise-report-table tbody').html('<tr><td colspan="7">No Data Found.</td></tr>');
    }
    else
    {
        $('#topic-wise-report-table tbody').html(html);
    }
    
}

function createDifficultyWiseReportTable(skillWiseTableReportRows) {
	html = '';
	$.each(skillWiseTableReportRows, function(i, row) {
		html += '<tr>'
		html += '<td>' +row.section+ '</td>' + '<td>' +row.name+ '</td>' + '<td>' +row.total_questions+ '</td>' + '<td>' + row.correct + '/' + row.attempted  + '</td>' + '<td>' +row.max_marks+ '</td>' + '<td>' +row.score+ '</td>' + '<td>' +(parseInt(row.score)*100/parseInt(row.max_marks)).toFixed(2) + '</td>';
        html += '</tr>';
	});
    $('#difficulty-wise-report-table tbody').html(html);
    if(skillWiseTableReportRows.length == 0)
    {
        $('#difficulty-wise-report-table tbody').html('<tr><td colspan="7">No Data Found.</td></tr>');
    }
    else
    {
        $('#difficulty-wise-report-table tbody').html(html);
    }
}

function createSkillWiseReportTable(skillWiseTableReportRows) {
	html = '';
	$.each(skillWiseTableReportRows, function(i, row) {
		html += '<tr>'
		html += '<td>' +row.section+ '</td>' + '<td>' +row.name+ '</td>' + '<td>' +row.total_questions+ '</td>' + '<td>' + row.correct + '/' + parseInt(row.attempted) + '</td>' + '<td>' +row.max_marks+ '</td>' + '<td>' +row.score+ '</td>' + '<td>' +(parseInt(row.score)*100/parseInt(row.max_marks)).toFixed(2)+ '</td>';
        html += '</tr>';
	});
    if(skillWiseTableReportRows.length == 0)
    {
        $('#skill-wise-report-table tbody').html('<tr><td colspan="7">No Data Found.</td></tr>');
    }
    else
    {
        $('#skill-wise-report-table tbody').html(html);
    }    
}

function createPercentilePieChart() {

	var percentile_pie_chart = $('#percentile-pie-chart').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Percentage'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: "Percentile",
            colorByPoint: true,
            data: [{
                name: "Correct",
                y: correct
            },{
                name: "Unattempted",
                y: unattempted
            }, {
                name: "Wrong",
                y: wrong
            }]
        }]
    });
}

function percentileTable(percentileTable) {
    html = '<tr>';
    percentileTable.total_time = percentileTable.total_time / 60;
    html += '<td>' + percentileTable.id + '</td>';
    html += '<td>' + percentileTable.total_questions + '</td>';
    html += '<td>' + percentileTable.attempted + '</td>';
    html += '<td>' + percentileTable.correct + '</td>';
    html += '<td>' + percentileTable.marks_archived + '</td>';
    html += '<td>' + parseFloat(percentileTable.percent_marks).toFixed(3) + '</td>';
    html += '<td>' + parseFloat(percentileTable.attempted / percentileTable.total_time).toFixed(3) + '</td>';
    html += '<td>' + parseFloat((percentileTable.correct / percentileTable.attempted) * 100).toFixed(3) + '</td>';
    html += '<td>' + parseFloat(percentileTable.correct / percentileTable.total_time).toFixed(3) + '</td>';
    
    html += '</tr>';

     $('#tests-detail-table tbody').html(html);
}


function fillSuccessgapTable(sections) {
    var html = '';
    var total_cut_off_marks = 0;
    var total_success_gap = 0;
    var total_topper_marks = 0;
    var total_topper_gap = 0;
    
    var section_names = [];
    var cut_off_marks = [];
    var success_gaps = [];
    var topper_gaps = [];
    var topper_marks = [];

    $.each(sections, function(i, section) {
        html += '<tr>';
        html += '<td>' + section.name + '</td>';
        html += '<td>' + section.marks + '</td>';
        html += '<td>' + section.cut_off_marks + '</td>';
        success_gap = (section.success_gap<0 ? 0:section.success_gap);
        html += '<td>' + success_gap + '</td>';
        html += '<td>' + section.topper_marks + '</td>';
        html += '<td>' + section.topper_gap + '</td>';
        html += '</tr>';
        
        /*total_cut_off_marks = parseInt(total_cut_off_marks) + parseInt(section.cut_off_marks);
        total_success_gap = parseInt(total_success_gap) + parseInt(section.success_gap);
        total_topper_marks = parseInt(total_topper_marks) + parseInt(section.topper_marks);
        total_topper_gap = parseInt(total_topper_gap) + parseInt(section.topper_gap);*/
        
        section_names.push(section.name);
        cut_off_marks.push(parseInt(section.cut_off_marks));
        success_gaps.push(parseInt(section.success_gap));
        topper_marks.push(parseInt(section.topper_marks));
        topper_gaps.push(parseInt(section.topper_gap));
        
    })
    //html += '<td> Overall Analysis </td><td>' + total_cut_off_marks + '</td> <td>' + total_success_gap + '</td> <td>' + total_topper_marks + '</td> <td>' + total_topper_gap + '</td>'; 
    $('#section-wise-report-table tbody').html(html);

    $('#success-gap-graph').highcharts({
        title: {
            text: 'Success Gap Report'
        },
        xAxis: {
            categories: section_names
        },
        yAxis: {
            min: 0,
            // max: 10,
            tickInterval: 1,
            title: {
                text: 'Marks',
            }  
        },
        plotOptions: {
            column: {
                pointWidth: 40             
                
            }
        },
        series: [{
            type: 'column',
            name: 'Cut Off Marks',
            color: '#235A9C',
            data: cut_off_marks
        }, {
            type: 'column',
            name: 'Success Gap',
            color: '#F72424',
            data: success_gaps
        }]
    });


    $('#topper-gap-graph').highcharts({
        title: {
            text: 'Topper Gap Report'
        },
        xAxis: {
            categories: section_names
        },
        yAxis: {
            // min: 0,
            // max: 10,
            tickInterval: 1,
            title: {
                text: 'Marks',
            }  
        },
        plotOptions: {
            column: {
                pointWidth: 40             
                
            }
        },
        series: [{
            type: 'column',
            name: 'Topper Marks',
            color: '#235A9C',
            data: topper_marks
        }, {
            type: 'column',
            name: 'Topper Gap',
            color: '#F72424',
            data: topper_gaps
        }]
    });
}