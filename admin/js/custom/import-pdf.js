var EndPoint = 'vendor/api.php';



$(document).ready(function() { 
    $('#uploadForm').on('submit',function(e) {	
        
        e.preventDefault();
        if($('#questions_file').val()) {
                                 
            $.ajax({
                'type'  :   'post',
                'url'   :   FormDataEndPoint,
                // 'data'   :   JSON.stringify(req)
                data: new FormData($('#uploadForm').get(0)),
                processData: false,
                contentType: false

            }).done(function(res)
            {
                
                res = $.parseJSON(res);
                return false;
            });
        }
    });
});