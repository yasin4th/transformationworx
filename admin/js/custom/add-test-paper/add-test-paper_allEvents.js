function setEventClassChange() {
	// this event load subjects when admin select a class
	$('.classes').off('change');
	$('.classes').on('change', function(e) {
		changeInArea = $(this).parents('.purple-border');
		$(changeInArea).find(".subjects").html('<option value="0">Select Subject</option>');
		$(changeInArea).find(".units").html('<option value="0">Select Unit</option>');
		$(changeInArea).find(".chapters").html('<option value="0">Select Chapter</option>');
		$(changeInArea).find(".topics").html('<option value="0">Select Topic</option>');
		id= $(this).find('option:selected').data('id');
		fetchSubjects(id,changeInArea);
	});
}

function setEventSubjectChange() {
	// this event load units when admin select subjects
	$('.subjects').off('change');
	$('.subjects').on('change', function(e) {
		changeInArea = $(this).parents('.purple-border');
		$(changeInArea).find(".units").html('<option value="0">Select Unit</option>');
		$(changeInArea).find(".chapters").html('<option value="0">Select Chapter</option>');
		$(changeInArea).find(".topics").html('<option value="0">Select Topic</option>');
		id = $(this).find('option:selected').data('id');
		fetchUnits(id,changeInArea);
	});
}

function setEventUnitChange() {
	// this event load topics when admin select units
	$('.units').off('change');
	$('.units').on('change', function(e) {
		changeInArea = $(this).parents('.purple-border');
		$(changeInArea).find(".chapters").html('<option value="0">Select Chapter</option>');
		$(changeInArea).find(".topics").html('<option value="0">Select Topic</option>');
		id = $(this).find('option:selected').data('id');
		fetchChapters(id,changeInArea);
	});
}

function setEventChapeterChange() {
	// this event load topics when admin select units
	$('.chapters').off('change');
	$('.chapters').on('change', function(e) {
		topicToChange = $(this).parents('.green-border').find('.topics');
		$(changeInArea).find(".topics").html('<option value="0">Select Topic</option>');
		id = $(this).find('option:selected').data('id');
		fetchTopics(id,topicToChange);
	});
}

// function to fetch classes
function fetchClasses() {
	req = {};
	req.action = 'fetchClasses';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillClasses(res.classes);
			classes = res.classes;
		}
		else{
			toastr.error('Error');
		}
	});

}

// function to fill classes
function fillClasses(classes){
	var html = "<option value='0'> Select Class </option>";
	$.each(classes,function( i,cls) {
		html += "<option value='"+(i+1)+"' data-id="+cls.id+">" +cls.name+ "</option>";
	});
	$('.classes').html(html);
}

// get the class id selected by admin
function getSelectedClassId() {
	id = $(".classes").find('option:selected').data('id');
	return id;
}

// function to fetch subjects
function fetchSubjects(id,area) {
	req = {};
	req.action = 'fetchSubjects';
	req.class_id = id;
	if(req.class_id) {
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fillSubjects(area,res.subjects);
			}
			else{
				$('.subjects').html('');
				// toastr.error('No Subject Found In Selected Class.');
			}
		});
	}
}

// function to fill subjects
function fillSubjects(area,subjects) {
	var html ='<option value="0"> Select Subject </option>';
	$.each(subjects,function( i,sub) {
		html += "<option value='"+(i+1)+"' data-id="+sub.id+">" +sub.name+ "</option>";
	});
	$(area).find('.subjects').html(html);
}

// function get the subject ids selected by admin
function getSelectedSubjectId() {
	sub = $(".subjects").find('option:selected');
	id = [];
	$.each(sub, function(i,sub_id){
		id.push($(sub_id).data('id'));
	})
	return id;
}

// function to fetch units
function fetchUnits(id,area) {
	req = {};
	req.action = 'fetchUnits';
	req.subject_id = [id];
	if(req.subject_id != '') {
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fillUnits(area,res.units);
			}
			else{
				// toastr.error('Error Units not load');
			}
		});
	}
}


// function to fill units
function fillUnits(area,units) {
	var subject_id = getSelectedSubjectId();
	var html = '<option value="0"> Select Unit </option>';
	$.each(units,function( i,unit) {
			html += "<option value='"+(i+1)+"' data-id="+unit.id+">" +unit.name+ "</option>";
	});
	$(area).find('.units').html(html);
}

// function get the unit ids selected by admin
function getSelectedUnitId() {
	units = $(".units").find('option:selected')
	id = [];
	$.each(units, function(i,unit_id){
		id.push($(unit_id).data('id'));
	})
	return id;
}

// function to fetch chapters
function fetchChapters(id,area) {
	req = {};
	req.action = 'fetchChapters';
	req.unit_id = [id];
	if(req.unit_id != '') {
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fillChapters(area,res.chapters);
				chapters = res.chapters;
			}
			else {
				// toastr.error('Error Chapters not load');
			}
		});
	}
}

// function to fill chapters
function fillChapters(area,chapters) {
	var html = '<option value="0"> Select Chapter </option>';
	$.each(chapters,function( i,chap) {
		html += "<option value='"+(i+1)+"' data-id="+chap.id+">" +chap.name+ "</option>";
	});
	$(area).find('.chapters').html(html);
}


// function get the chapter ids selected by admin
function getSelectedChapterId() {
	chapter = $(".chapters").find('option:selected')
	id = [];
	$.each(chapter, function(i,chapter_id){
		id.push($(chapter_id).data('id'));
	})
	return id;
}

// function to fetch topics
function fetchTopics(id,area) {
	req = {};
	req.action = 'fetchTopics';
	req.chapter_id = [id];
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillTopics(area,res.topics);
		}
		else{
			// toastr.error('Error Topics not load');
		}
	});
}

// function to fill Topics
function fillTopics(changeDropdown,topics){
	var html = '<option value="0"> Select Topic </option>';
	$.each(topics,function( i,topic) {
		html += "<option value='"+(i+1)+"' data-id="+topic.id+">" +topic.name+ "</option>";
	});
	$(changeDropdown).html(html);
}

// function to fetch tags
function fetchTags() {
	req = {};
	req.action = 'fetchTags';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		tags = '';
		if(res.status == 1){
			tags = res.tags;
			var html = "";
			$.each(res.tags,function( i,tag) {
				html += "<option value='"+(i+1)+"' data-id="+tag.id+">" +tag.tag+ "</option>";
			});
			$('.tags').html(html);
			$('.tags').select2({
				placeholder: "Select Tags"
				});
		}
		else{
			toastr.error('Error Tags not load');
		}
	});
}

// function to fill Tags
function fillTags() {
	if(tags.length)
	{
		var html = "";
		$.each(tags,function( i,tag) {
					html += "<option value='"+(i+1)+"' data-id="+tag.id+">" +tag.tag+ "</option>";
		});
		$(area).find('.tags').eq(-1).html(html);
		$(area).find('.tags').eq(-1).select2({
			placeholder: 'Select Tags'
		});
	}
}

// function to fetch difficulty-level
function fetchDifficultyLevel() {
	req = {};
	req.action = 'fetchDifficultyLevel';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillDifficultyLevel(res.difficulty);
			difficultyLevels = res.difficulty;
		}
		else{
			toastr.error('Error Difficulty Level not load');
		}
	});
}

// function to fill Difficulty Level
function fillDifficultyLevel(difficultyLevels){
	var html = '<option value="0"> Difficulty-Levels </option>';
	$.each(difficultyLevels,function( i,difficultyLevel) {
				html += "<option value='"+(i+1)+"' data-id="+difficultyLevel.id+">" +difficultyLevel.difficulty+ "</option>";
	});
	$('.difficulty-levels').html(html);
}

// function to fetch skills
function fetchSkills() {
	req = {};
	req.action = 'fetchSkills';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillSkills(res.skills);
			skills = res.skills;
		}
		else{
			// toastr.error('Error Skills not load');
		}
	});
}

// function to fill Skills
function fillSkills(skills){
	var html = '<option value="0"> Skills </option>';
	$.each(skills,function( i,skill) {
		html += "<option value='"+(i+1)+"' data-id="+skill.id+">" +skill.skill+ "</option>";
	});
	$('.skills').html(html);
}

// function to fetch question types
function fetchQuestionTypes() {
	var req = {};
	req.action = 'fetchQuestionTypes';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		fillQuestionTypes(res.data);
		questionTypes = res.data;
	});

}

// function to fill questions types
function fillQuestionTypes(questionTypes){
	var html = '<option value="0"> Question-Types </option>';
	$.each(questionTypes,function( i,questionType) {
		html += "<option value='"+(i+1)+"' data-id='"+questionType.id+"'>" +questionType.question_type+ "</option>";
	});
	$('.question-types').html(html);
}


function getTags(area) {
	id = [];
	TAGS = $(area).find('.tags option:selected');
	$.each(TAGS, function(i,tag_id){
		id.push($(tag_id).data('id'));
	});
	return id;
}

function validate() {
	var toast = true;
	var check = true;

	$.each($('.classes option:selected'), function(i,cls){
		if($(cls).val() == 0) {
			if(toast)
			{
				toastr.info('Please fill Class.');
				toast= false;
			}
			// setError($(cls), 'Please fill class.');
			check = false;
		}
	});

	$.each($('.subjects option:selected'), function(i,sub){
		if($(sub).val() == 0) {
			if(toast)
			{
				toastr.info('Please fill Subject.');
				toast = false;
			}
			// setError($(sub), 'Please fill subjects.');
			check = false;
		}
	});

	$.each($('.question-types option:selected'), function(i,questionType){
		if($(questionType).val() == 0) {
			if(toast)
			{
				toastr.info('Please specify Question-type.');
				toast = false;
			}
			// setError($(questionType), 'Please fill question-type.');
			check = false;
		}
	});
	$('.count').each(function(i, box) {
		if ($(box).val() == "" || parseInt($(box).val()) < 1) {
			if(toast)
			{
				toastr.error('All question count atleast contain 1 question.');
				toast = false;
			}
			check = false;

		};
	})

	$.each($('.section-name'), function(i,sectionName) {
		if($(sectionName).val() == '') {
			if(toast)
			{
				toastr.info('Please fill Section Name.');
				toast = false;
			}
			$(sectionName).focus();
			// setError($(sectionName), 'Please fill section name.');
			check = false;

		}
	});

	$.each($('.cut-off-marks'), function(i,cutOff) {
		if($(cutOff).val() == '' || $(cutOff).val() == 0) {
			if(toast)
			{
				toastr.info('Please fill Cut Off Marks.');
				toast = false;
			}
			// $(cutOff).focus();
			// setError($(cutOff), 'Please fill section name.');
			check = false;

		}
	});

	$.each($('.totalQuestions'), function(i,totalQuestion) {
		var count =0;
		if($(totalQuestion).val() <= 0) {
			if(toast)
			{
				toastr.info('Please fill Total Questions.');
				toast = false;
			}
			// setError($(totalQuestion), 'Please fill total-questions.');
			// toastr.error('Please Fill Total-Questions.');
			check = false;

		}
		$.each($(totalQuestion).parents('.portlet').find('.question') ,function(i,question){
			count += parseInt($(question).find('.count').val());
		});
		if(parseInt($(totalQuestion).val()) != count)
		{
			if(toast)
			{
				toastr.info('Please fill correct total-questions, count is not matched.');
				toast= false;
			}
			check = false;

		}
	});

	var regx = /^\d+(\.{1}\d+){0,1}$/;
	$.each($('.max-mark'), function(i,maxMarks) {
		if($(maxMarks).val().length == 0) {
			if(toast)
			{
				toastr.info('Please fill maximum Marks.');
				toast = false;
			}
			// setError($(totalQuestion), 'Please fill total-questions.');
			// toastr.error('Please Fill Total-Questions.');
			check = false;

		}
		if(!regx.test($(maxMarks).val()))
		{
			if(toast)
			{
				toastr.info('Please fill positive numeric value in Maximum Marks field.');
				toast = false;
			}
			check = false;

		}
	});

	$.each($('.neg-mark'), function(i,minMarks) {
		if($(minMarks).val().length == 0) {
			if(toast)
			{
				toastr.info('Please fill Negative Marks.');
				toast = false;
			}
			// setError($(totalQuestion), 'Please fill total-questions.');
			// toastr.error('Please Fill Total-Questions.');
			check = false;
		}
		if(!regx.test($(minMarks).val()))
		{
			if(toast)
			{
				toastr.info('Please fill positive numeric value in Negative Marks field.');
				toast = false;
			}
			check = false;
		}

	});



	return check;
}

function getData() {
	Section = [];
	$.each($('.section').find('.portlet'), function(s,section){
		sec = {};
		var	quest = [];

		$.each($(section).find('.purple-border'), function(c, purpleBox) {


			$.each($(purpleBox).find('.green-border'), function(ch,greenBox) {


				$.each($(greenBox).find('.question'), function(qt,question) {
					var questions = {};
					questions.classId = 0;
					questions.subjectId = 0;
					questions.unitId = 0;
					questions.Tags = 0;
					questions.chapter = 0;
					questions.topic = 0;
					questions.questionType = 0;
					questions.numberOfQuestions = 0;
					questions.difficulty = 0;
					questions.skill = 0;
					questions.maxMark = 0;
					questions.negMark = 0;

					if($(purpleBox).find('.classes option:selected').val() != 0)
						questions.classId = $(purpleBox).find('.classes option:selected').data('id');
					if($(purpleBox).find('.subjects option:selected').val() != 0)
						questions.subjectId = $(purpleBox).find('.subjects option:selected').data('id');
					if($(purpleBox).find('.units option:selected').val() != 0)
						questions.unitId = $(purpleBox).find('.units option:selected').data('id');
					if(getTags(purpleBox) != '')
						questions.Tags = getTags(purpleBox);
					if($(greenBox).find('.chapters option:selected').val() != 0)
						questions.chapter = $(greenBox).find('.chapters option:selected').data('id');
					if($(greenBox).find('.topics option:selected').val() != 0)
						questions.topic = $(greenBox).find('.topics option:selected').data('id');
					if($(question).find('.question-types option:selected').val() != 0)
						questions.questionType = $(question).find('.question-types option:selected').data('id');
					if($(question).find('.count').val() != '')
						questions.numberOfQuestions = $(question).find('.count').val();
					if($(question).find('.difficulty-levels option:selected').data('id') != undefined)
						questions.difficulty = $(question).find('.difficulty-levels option:selected').data('id');
					if($(question).find('.skills option:selected').data('id') != undefined)
						questions.skill = $(question).find('.skills option:selected').data('id');
					if($(question).find('.max-mark').val() != 0)
						questions.maxMark = $(question).find('.max-mark').val();
					if($(question).find('.neg-mark').val() != 0)
						questions.negMark = $(question).find('.neg-mark').val();
					quest.push(questions);
				});
			});
		});
		sec.sectionName = $(section).find('.section-name').val();
		sec.cut_off_marks = $(section).find('.cut-off-marks').val();
		sec.qc_status = $(section).find('.qc-status option:selected').val();
		sec.totalQuestion = $(section).find('.totalQuestions').val();
		sec.questions = quest;
		Section.push(sec);
	});
	return Section;
}