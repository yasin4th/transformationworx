var classes;
var tags;
var difficultyLevels;
var skills;
var questionTypes;
var chapters;
var check = true;

$(function() {

	fetchClasses();

	fetchTags();
	fetchDifficultyLevel();
	fetchSkills();
	fetchQuestionTypes();
	setAddQuestionType();
	setAddChapterAndTopic();
	setAddCriteria();
	setAddSection();
	setEventClassChange();
	setEventSubjectChange();
	setEventUnitChange();
	setEventChapeterChange();
	// setValidationForTotalQuestons();
	deleteFunctions();

	$('#genratePaper').on('click', function(e){
		e.preventDefault();
		//$('#genratePaper').attr('disabled',  true);

		if(validate()) {
			var req = {};
			req.action = "createTestPaper";
			req.sections = getData();

			$.ajax({
				'type' : 'post',
				'url' : EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);

				if(res.status == 1) {
					window.location = "untitle-question-paper.php?test_paper="+res.test_paper+"&status=0&paper_name=Untitled Questions";
				}
				else if(res.status == 0) {
					toastr.error(res.message);
				}
			});
		}
		else {
			//$('#genratePaper').attr('disabled',  false);
		}
	});

});

