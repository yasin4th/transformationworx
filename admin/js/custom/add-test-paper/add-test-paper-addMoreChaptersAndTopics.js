function setAddQuestionType() {
	$('.addQuestionType').off('click');
	$('.addQuestionType').on('click',function(e) {
		e.preventDefault();
		html='<div class="question form-group"><div class="row"><div class="col-md-11"><div class="row"><div class="row"><div id="questions" class="col-md-12"><div class="col-md-2 col-xs-2"><span class="required">* </span><select class="question-types form-control pull-right width-select-type"><option>Select Question Type</option></select></div><div class="col-md-2 col-xs-2"><span class="required">* </span><input type="text" class="count form-control inline pull-right width-select-type" placeholder="Count"></div><div class="col-md-2 col-xs-2"><select class="difficulty-levels form-control" placeholder="Select Class" ><option>Difficulty Level</option></select></div><div class="col-md-2 col-xs-2"><select class="skills form-control" placeholder="Select Class" ><option>Skill</option></select></div><div class="col-md-2 col-xs-1"><input type="text" class="max-mark form-control" placeholder="Max Marks"></div><div class="col-md-2 col-xs-1"><input type="text" class="neg-mark form-control" placeholder="Neg Marks"></div></div></div></div></div><div class="col-md-1"><a class="delete-blue-box tooltips delete pull-right mrg-left10" data-container="body" data-placement="top" data-original-title="Delete Multiple Question"><i class="fa fa-trash-o"></i></a></div></div></div>';
		$(this).parents('.blue-border').find('.questions').append(html);

		var html = "<option> Difficulty-Levels </option>";
		$.each(difficultyLevels,function( i,difficultyLevel) {
			html += "<option val='"+i+"' data-id="+difficultyLevel.id+">" +difficultyLevel.difficulty+ "</option>";
		});
		$(this).parents('.blue-border').find('.difficulty-levels').eq(-1).html(html);

		var html = "<option> Skills </option>";
		$.each(skills,function( i,skill) {
			html += "<option val='"+i+"' data-id="+skill.id+">" +skill.skill+ "</option>";
		});
		$(this).parents('.blue-border').find('.skills').eq(-1).html(html);

		var html = "<option>Question-Types</option>";
		$.each(questionTypes,function( i,question_type) {
			html += "<option val='"+i+"' data-id="+question_type.id+">" +question_type.question_type+ "</option>";
		});
		$(this).parents('.blue-border').find('.question-types').eq(-1).html(html);
		deleteBlueBox();
		// $(this).parents('.portlet').find('.totalQuestions').val($(this).parents('.portlet').find('.question').length);

	});
}

var clicked;
function setAddChapterAndTopic() {
	$('.addChapterAndTopic').off('click');
	$('.addChapterAndTopic').on('click',function(e) {
		e.preventDefault();
		clicked = $(this);
		html='<div class="col-md-12 green-border mrg-bot10 form-body "><div class="col-md-12"><div class="row"><div class="col-md-3"><div class="form-group"><div class="row"><div class="col-md-12"><select class="chapters form-control"></select></div></div></div></div><div class="col-md-3"><div class="form-group"><div class="row"><div class="col-md-12"><select class="topics form-control"></select></div></div></div></div><div class="col-md-2 col-md-offset-4"><a class="delete-green-box tooltips delete pull-right mrg-left10" data-container="body" data-placement="top" data-original-title="Delete Multiple Question"><i class="fa fa-trash-o"></i></a></div></div><div class="col-md-12 blue-border form-body"><div class="questions col-md-11"></div><div class="col-md-1 col-xs-2"><a class="addQuestionType tooltips add-color pull-right" data-container="body" data-placement="top" data-original-title="Add Multiple Question"><i class="fa fa-plus-circle"></i></a></div></div></div></div>';
		$(this).parent().find('.chapterAndTopic').append(html);
		// $('.chapterAndTopic').append(html);
		setAddQuestionType();
		var html = $(this).parents('.purple-border').eq(0).find('.chapters').eq(0).html();
		$(this).parents('.purple-border').find('.chapters').eq(-1).html(html);
		setEventChapeterChange();
		$(this).parents('.purple-border').find('.addQuestionType').eq(-1).click();
		deleteBlueBox();
		deleteGreenBox();
		// $(this).parents('.portlet').find('.totalQuestions').val($(this).parents('.portlet').find('.question').length);
	});
}

function setAddCriteria() {
	$('.addCriteria').off('click');
	$('.addCriteria').on('click',function(e) {
		e.preventDefault();
		html= '<div class="col-md-12 purple-border mrg-bot10 form-body"><div class="chapterAndTopic col-md-12"><div class="row"><div class="col-md-12"><a class="delete-purple-box tooltips delete pull-right mrg-left5" data-container="body" data-placement="top" data-original-title="Delete Criteria"><i class="fa fa-trash-o"></i></a></div></div><div class="row"><div class="col-md-3"><div class="form-group"><label class="control-label"><span class="required">* </span>Class</label><div class="row"><div class="col-md-12"><select class="classes form-control"><option>Select Class</option></select></div></div></div></div><div class="col-md-3"><div class="form-group"><label class="control-label"><span class="required">* </span>Subject</label><div class="row"><div class="col-md-12"><select class="subjects form-control" data-tags="true"></select></div></div></div></div><div class="col-md-3"><div class="form-group"><label class="control-label"><span class="required">* </span>Unit</label><div class="row"><div class="col-md-12"><select class="units form-control"></select></div></div></div></div><div class="col-md-3"><div class="form-group"><label class="control-label"><span class="required">* </span>Tags</label><div class="row"><div class="col-md-12"><select class="tags form-control" multiple="multiple"></select></div></div></div></div></div><div class="col-md-12 green-border mrg-bot10 form-body "><div class="col-md-12"><div class="row"><div class="col-md-3"><div class="form-group"><div class="row"><div class="col-md-12"><select class="chapters form-control"></select></div></div></div></div><div class="col-md-3"><div class="form-group"><div class="row"><div class="col-md-12"><select class="topics form-control"></select></div></div></div></div><div class="col-md-2 col-md-offset-4"><a class="delete-green-box tooltips delete pull-right mrg-left10" data-container="body" data-placement="top" data-original-title="Delete Multiple Chapter"><i class="fa fa-trash-o"></i></a></div></div><div class="col-md-12 blue-border form-body"><div class="questions col-md-11"></div><div class="col-md-1 col-xs-2"><a class="addQuestionType tooltips add-color pull-right" data-container="body" data-placement="top" data-original-title="Add Multiple Question"><i class="fa fa-plus-circle"></i></a></div></div></div></div></div><a class="addChapterAndTopic btn tooltips add-color pull-right" data-container="body" data-placement="top" data-original-title="Add Multiple Chapter"><i class="fa fa-plus-circle"></i></a></div>';
		$(this).parents('div').eq(0).find('.classAndSubject').append(html);

		var html = $(this).parents('div').eq(0).find('.classes').eq(0).html();
		$(this).parents('div').eq(0).find('.classes').eq(-1).html(html);
		// $('.classAndSubject').append(html);
		setEventClassChange();
		setEventSubjectChange();
		setEventUnitChange();
		setEventChapeterChange();
		setAddChapterAndTopic();
		setAddQuestionType();
		$(this).parents('.portlet-body').find('.purple-border').eq(-1).find('.addQuestionType').click();
		area = $(this).parents('.portlet-body').find('.purple-border');
		fillTags(area);
		deleteBlueBox();
		deleteGreenBox();
		deletePurpleBox();
		// $(this).parents('.portlet').find('.totalQuestions').val($(this).parents('.portlet').find('.question').length);

	});
}



var sec = 1;
function setAddSection() {
	$('.addSection').on('click',function(e) {
		e.preventDefault();
		if(true) {
			sec += 1;
			html= '<div id="section'+sec+'" class="portlet light bg-inverse mrg-bot10"> <div class="portlet-title" style="border: none; margin: 0px;"> <div class="caption"><i class="fa fa-puzzle-piece"></i> Section '+sec+'</div> <div class="tools"><a class="delete-section tooltips delete" data-container="body" data-placement="top" data-original-title="Delete Section" aria-describedby="tooltip735432"><i class="fa fa-trash-o"></i></a> <a class="collapse"></a> </div> </div> <div class="portlet-body form"> <form role="form"> <div class="classAndSubject form-body"> <label>Please select the criteria to set question paper</label> <div class="row"> <div class="col-md-3 col-xs-6"> <div class="form-group"> <label class="control-label"><span class="required">* </span>Section Name</label> <div class="row"> <div class="col-md-12"> <input type="text" class="section-name form-control pull-right" placeholder="Enter Section Name" value="Section '+sec+'"> </div> </div> </div> </div> <div class="col-md-3 col-xs-2"></div> <div class="col-md-2 col-xs-4"> <div class="form-group"> <label class="control-label">QC Status&nbsp;</label> <select class="qc-status form-control"> <option value="">Select Status</option> <option value="0">Pending</option> <option value="1">Done</option> </select> </div> </div><div class="col-md-2 col-xs-4"> <div class="form-group"> <label class="control-label"><span class="required">* </span>Cut Off Marks&nbsp;</label> <input  type="text" class="cut-off-marks form-control pull-right" placeholder="Cut Off Marks"> </div> </div> <div class="col-md-2 col-xs-4"> <div class="form-group"> <label class="control-label"><span class="required">* </span>Total-Questions &nbsp;</label> <input type="text" class="totalQuestions form-control pull-right" placeholder="Total Questions"> </div> </div> </div> <div class="classAndSubject"> <div class="col-md-12 purple-border mrg-bot10 form-body"> <div class="chapterAndTopic col-md-12"> <div class="row"> <div class="col-md-12"><a class="delete-purple-box tooltips delete pull-right mrg-left5" data-container="body" data-placement="top" data-original-title="Delete Criteria"><i class="fa fa-trash-o"></i></a></div> </div> <div class="row"> <div class="col-md-3"> <div class="form-group"> <label class="control-label"><span class="required">* </span>Class</label> <div class="row"> <div class="col-md-12"> <select class="classes form-control"> <option>Select Class</option> </select> </div> </div> </div> </div> <div class="col-md-3"> <div class="form-group"> <label class="control-label"><span class="required">* </span>Subject</label> <div class="row"> <div class="col-md-12"> <select class="subjects form-control" data-tags="true"></select> </div> </div> </div> </div> <div class="col-md-3"> <div class="form-group"> <label class="control-label"><span class="required">* </span>Unit</label> <div class="row"> <div class="col-md-12"> <select class="units form-control"></select> </div> </div> </div> </div> <div class="col-md-3"> <div class="form-group"> <label class="control-label"><span class="required">* </span>Tags</label> <div class="row"> <div class="col-md-12"> <select class="tags form-control" multiple="multiple"></select> </div> </div> </div> </div> </div> <div class="col-md-12 green-border mrg-bot10 form-body "> <div class="col-md-12"> <div class="row"> <div class="col-md-3"> <div class="form-group"> <div class="row"> <div class="col-md-12"> <select class="chapters form-control"></select> </div> </div> </div> </div> <div class="col-md-3"> <div class="form-group"> <div class="row"> <div class="col-md-12"> <select class="topics form-control"></select> </div> </div> </div> </div> <div class="col-md-2 col-md-offset-4"><a class="delete-green-box tooltips delete pull-right mrg-left10" data-container="body" data-placement="top" data-original-title="Delete Multiple Chapter"><i class="fa fa-trash-o"></i></a></div> </div> <div class="col-md-12 blue-border form-body"> <div class="questions col-md-11"></div> <div class="col-md-1 col-xs-2"><a class="addQuestionType tooltips add-color pull-right" data-container="body" data-placement="top" data-original-title="Add Multiple Question"><i class="fa fa-plus-circle"></i></a></div> </div> </div> </div> </div> <a class="addChapterAndTopic btn tooltips add-color pull-right" data-container="body" data-placement="top" data-original-title="Add Multiple Chapter"><i class="fa fa-plus-circle"></i></a> </div> </div> <button type="submit" class="addCriteria btn green" style="margin-top:10px;"><i class="fa fa-plus-circle"></i> &nbsp; Another Criteria</button> <div class="row"> <div class="col-md-6" style="margin-top:10px;"></div> <div class="col-md-6"></div> </div> </div> </form> </div></div>';
			$('.section').append(html);
		 	area = $('.section').find('.purple-border').eq(-1);
			fillTags(area);
			setEventClassChange();
			setEventSubjectChange();
			setEventUnitChange();
			setEventChapeterChange();
			setAddQuestionType();
			setAddChapterAndTopic();
			setAddCriteria();
			var html = $('.classes').eq(0).html();
			$('.classes').eq(-1).html(html);
			$('.section').find('.portlet-body').eq(-1).find('.addQuestionType').click();
			deleteBlueBox();
			deleteGreenBox();
			deletePurpleBox();
			deleteSection();
			// $(this).parents('.portlet').find('.totalQuestions').val($(this).parents('.portlet').find('.question').length);
		}
		else {
			toastr.info('Please Fill Section Name first')
		}
	});
}
// function changeTotalAttempt(){
// 	$.each($('.portlet'), function(i,portlet){
// 		$(portlet).off('change');
// 		$(portlet).on('change', function(){
// 			$(this).find('.totalQuestions').val($(this).find('.question').length);
// 		});
// 	});
// }
// $(function(){
// // 	$('.section').off('change');
// // 	$('.section').on('change',function(e){
// // 	$(this).find('.totalQuestions').val($(this).find('.question').length);
// // });
// 	changeTotalAttempt();
// });
