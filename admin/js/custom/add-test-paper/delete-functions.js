function deleteBlueBox() {
	$('.delete-blue-box').off('click');
	$('.delete-blue-box').on('click', function(e) {
		e.preventDefault();
		var parent=$(this).parents('.portlet');
		if($(this).parents('.questions').find('.question').length > 1) {
			$(this).parents('.question').remove();
			// $(parent).find('.totalQuestions').val($(parent).find('.question').length);
		}
	});
}


function deleteGreenBox() {
	$('.delete-green-box').off('click');
	$('.delete-green-box').on('click', function(e) {
		e.preventDefault();
		var parent=$(this).parents('.portlet');
		if($(this).parents('.chapterAndTopic').find('.green-border').length > 1) {
			$(this).parents('.green-border').remove();
			// $(parent).find('.totalQuestions').val($(parent).find('.question').length);
		}
	});

}

function deletePurpleBox() {
	$('.delete-purple-box').off('click');
	$('.delete-purple-box').on('click', function(e) {
		e.preventDefault();
		var parent=$(this).parents('.portlet');
		if($(this).parents('.classAndSubject').find('.purple-border').length > 1) {
			$(this).parents('.purple-border').remove();
			// $(parent).find('.totalQuestions').val($(parent).find('.question').length);
		}
	});

}

function deleteSection() {
	$('.delete-section').off('click');
	$('.delete-section').on('click', function(e) {
		e.preventDefault();
		var parent=$(this).parents('.portlet');
		if($(this).parents('.section').find('.portlet').length > 1) {
			$(this).parents('.portlet').remove();
			// $(parent).find('.totalQuestions').val($(parent).find('.question').length);
		}
	});

}


function deleteFunctions() {
	deleteBlueBox();
	deleteGreenBox();
	deletePurpleBox();
	deleteSection();
}