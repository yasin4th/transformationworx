var program;
$(function() {
	program = getUrlParameter('id');;
	getTestprograms();
	fetchInstitutCoupons();
	selectAllEvent();
	FilterEvent();
	downloadCoupons();
});

function getTestprograms() {
	var req = {};
	req.action = "getTestprograms";
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillTestprograms(res.data);
		}
		else if(res.status == 0) {
			toastr.error(res.message);
		}
	});
}

function fillTestprograms(programs) {
	html = '<option value="0"> Select Program </option>';
	$.each(programs, function(p, program) {
		html += '<option value="'+program.id+'""><td>'+program.name+'</option>';
	});
	$('#test-programs').html(html);
}

function fetchInstitutCoupons() {
	var req = {};
	req.action = "fetchEnrollment";
	req.test_program_id = program;

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
			fillInstitutCoupons(res.coupons);
		else if(res.status == 0)
			toastr.error(res.message);
	});
}

function fillInstitutCoupons(coupons) {
	html = '';
	$.each(coupons, function(p, coupon) {
		html += '<tr data-id="'+coupon.id+'"" data-program="'+coupon.test_program_id+'"" data-status="'+coupon.student_id +'"" >'

		html += '<td> <input type="checkbox" class="select-coupon"> '+(p+1)+'</td>';
		html += '<td> '+coupon.code+'</td>'
		html += '<td>'+coupon.program_name+'</td>';

		if (coupon.student_id != 0) {
			html += '<td> <a class="get-student"> Used </a> </td>';
		} else{
			html += '<td > Unused </td>';
		};

		html += '<td> '+coupon.created+'</td>';
		// html += '<td> '+coupon.expiry_date+'</td>';

		html += '</tr>';
	});
	$('#institute-coupons-table tbody').html(html);
	Metronic.init();
	getStudent();
}


function selectAllEvent() {
	$('.select-all').off('change');
	$('.select-all').on('change', function(e) {
		e.preventDefault();
		if ($('.select-all').prop('checked')) {
			$('.select-coupon').prop('checked', true).parent().addClass('checked');;
		} else{
			$('.select-coupon').prop('checked', false).parent().removeClass('checked');;
		};

	})
}

function FilterEvent() {
	$('#filter').off('click');
	$('#filter').on('click', function(e) {
		e.preventDefault();
		program = $('#test-programs').val();
		status = $('#coupon-status').val();

		$('#institute-coupons-table tbody tr').addClass('hidden');
		if (program != 0) {

			switch(status) {

				case "0":
					$('#institute-coupons-table tbody tr[data-program="'+program+'"]').removeClass('hidden');
					break;

				case "1":
					$('#institute-coupons-table tbody tr[data-program="'+program+'"]').removeClass('hidden');
					$('#institute-coupons-table tbody tr[data-program="'+program+'"][data-status="'+0+'"]').addClass('hidden');
					break;


				case "2":
					$('#institute-coupons-table tbody tr[data-program="'+program+'"][data-status="'+0+'"]').removeClass('hidden');
					break;

				default:
					break;

			}
		}
		else {
			switch(status) {
				case "0":
					$('#institute-coupons-table tbody tr').removeClass('hidden');
					break;

				case "1":
					$('#institute-coupons-table tbody tr').removeClass('hidden');
					$('#institute-coupons-table tbody tr[data-status="'+0+'"]').addClass('hidden');
					break;


				case "2":
					$('#institute-coupons-table tbody tr[data-status="'+0+'"]').removeClass('hidden');
					break;

				default:
					break;
			}
		}
	})
}

function downloadCoupons() {
	$('#download-selected-coupons').off('click');
	$('#download-selected-coupons').on('click', function(e) {
		e.preventDefault();
		text = ''
		$.each($('.select-coupon:checked'), function(i, box) {
			text += $(box).parents('tr').find('td:eq(1)').text();
			text += ',';
		})

		uriContent = "data:application/octet-stream," +escape(text);
		window.open(uriContent, 'myDocument');
	});
}


function resetForm() {
	$('select').val(0);
}

function scroll () {
	if(($("#").innerHeight() - $("div .chats").scrollTop()) < 600) {
		$("div .chats").scrollTop($("#chats").innerHeight());
	}
}

function getStudent() {
	$('.get-student').off('click');
	$('.get-student').on('click', function(e) {
		e.preventDefault();
		id = $(this).parents('tr:eq(0)').data('status');
		var req = {};
		req.action = "getAdminDetails"
		req.user_id = id;
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
				console.log(res);
			if(res.status == 1)
				showStudentDetails(res.users);
			else if(res.status == 0)
				toastr.error(res.message);
		});
	})
}

function showStudentDetails(user) {
	lastLogin = Date(user.last_login).substr(0,25)
	$('#students-details-modal .student-name').text(user.name);
	$('#students-details-modal .student-e-mail').text(user.email);
	$('#students-details-modal .login-date').text(lastLogin);
	$('#students-details-modal').modal('show')

}