var limit = 0;
var page_no = 1;
$(function() {

	$('#expiry').datetimepicker({
    	format: "yyyy-mm-dd hh:ii:ss",
    	startDate: Date(),
    	showMeridian: true,
        autoclose: true,
        todayHighlight: true
    });

	createDiscount();
	fetchDiscount();
	saveDiscount();

});
createDiscount = function() {
	$('#create-discount-form').off('submit');
	$('#create-discount-form').on('submit', function(e) {
		e.preventDefault();
		if(validate()){
			var req = {};
			req.action = 'createDiscount';
			req.title = $('#title').val();
			req.code = $('#code').val();
			req.type = $('#type').val();
			req.min = $('#min').val();
			req.value = $('#value').val();
			req.expiry = $('#expiry').val();
			req.status = $('#status').val();
			req.maxuses = $('#maxuses').val();
			req.description = $('#description').val();
			$.ajax({
				type	:	'post',
				url		:	EndPoint,
				data	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);

				$('#add-discount-modal').hide();
				fetchDiscount();
				Metronic.init();

			});
		}
	});
}

validate = function() {
	return true;
}

fetchDiscount = function() {

	var req = {};
	req.action = 'fetchDiscount';
	req.limit = limit;

	$.ajax({
		type	:	'post',
		url		:	EndPoint,
		data	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			fillData(res.result);
			createPagination(res.total_questions);
			Metronic.init();
			//MathJax.Hub.Typeset();
		}
	});
}

fillData = function(data) {
	html = '';
	$.each(data,function(i,discount) {
		html += '<tr>';
		html += '<td>'+(sn++)+'</td>';
		html += '<td>'+discount.title+'</td>';
		html += '<td>'+discount.code+'</td>';
		html += '<td>'+discount.type+'</td>';
		html += '<td>'+discount.value+'</td>';
		html += '<td>'+discount.expiry+'</td>';
		html += '<td>'+discount.maxuses+'</td>';
		html += '<td>'+discount.uses+'</td>';
		if(discount.status == '1'){
			html += '<td><span class="label label-sm label-info">Active</span></td>';
		}
		else{
			html += '<td><span class="label label-sm label-info">Inactive</span></td>';
		}

		html += '<td><div>';
		if(discount.status == 1)
			html += '<a data-id="'+discount.id+'" data-status="0" class="change-status" title="Deactivate"><i class="fa fa-times-circle"></i></a>';
		else
			html += '<a data-id="'+discount.id+'" data-status="1" class="change-status" title="Activate"><i class="fa fa-check-square-o"></i></a> ';

		html += '| <a data-id="'+discount.id+'" class="edit-discount" title="Edit"><i class="fa fa-pencil"></i></a> | <a class="delete" data-id="'+discount.id+'" data-title="'+discount.title+'" style="color: rgb(230, 12, 12);font-weight: bold;" title="Delete"><i class="fa fa-trash-o"></i></a></div></td>';
		html += '</tr>';
	});
	$('#discount-table tbody').html(html);

	setEventEditDiscount();
	setEvents();

}

updateDiscount = function() {

	var req = {};
	req.action = 'updateDiscount';

	$.ajax({
		type	:	'post',
		url		:	EndPoint,
		data	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){


			Metronic.init();
			//MathJax.Hub.Typeset();

		}

	});

}

setEventEditDiscount = function() {
	$('.edit-discount').off('click');
	$('.edit-discount').on('click', function(e) {

		var req = {};
		req.action = 'fetchDiscount';
		req.id = $(this).data('id');
		$.ajax({
			type	:	'post',
			url		:	EndPoint,
			data	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){

				toastr.success(res.message);
				if(res.result.length==1){
					var q = res.result[0];
					$('#edit-title').val(q.title);
					$('#edit-code').val(q.code);
					$('#edit-type').val(q.type);
					$('#edit-min').val(q.min_price);
					$('#edit-value').val(q.value);
					$('#edit-expiry').val(q.expiry);
					$('#edit-status').val(q.status);
					$('#edit-maxuses').val(q.maxuses);
					$('#edit-description').val(q.description);
					$('#edit-discount-modal').attr('data-id',req.id);
					$('#edit-discount-modal').modal('show');
				}
				//Metronic.init();
			}
		});

	});


}
saveDiscount = function () {

	$('#edit-discount-form').off('submit');
	$('#edit-discount-form').on('submit', function(e) {
		e.preventDefault();

		var req = {};
		req.id = $('#edit-discount-modal').data('id');
		console.log($('#edit-discount-modal').data('id'));
		req.action = 'updateDiscount';
		req.title = $('#edit-title').val();
		req.code = $('#edit-code').val();
		req.type = $('#edit-type').val();
		req.min = $('#edit-min').val();
		req.value = $('#edit-value').val();
		req.expiry = $('#edit-expiry').val();
		req.status = $('#edit-status').val();
		req.maxuses = $('#edit-maxuses').val();
		req.description = $('#edit-description').val();
		$.ajax({
			type	:	'post',
			url		:	EndPoint,
			data	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1)
			{
				toastr.success(res.message);
				$('#edit-discount-modal').modal('hide');
				fetchDiscount();
			}

		});

	});


}

function setEvents() {
	$('.delete').off('click');
	$('.delete').on('click', function(e) {
		e.preventDefault();
		id = $(this).data('id');
		title = $(this).data('title');
		if(confirm('Are you sure want to delete Discount: '+title)) {
			var req = {};
			req.action = 'updateDiscount';
			req.id = id;
			req.deleted = 1;
			$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 1){
					fetchDiscount();
					toastr.success(res.message);
				}
				else {
					toastr.error(res.message,'Error:');
				}
			});
		}
	});

	$('.change-status').off('click');
	$('.change-status').on('click', function(e) {
		e.preventDefault();
		id = $(this).data('id');

		var req = {};
		req.action = 'updateDiscount';
		req.id = id;
		req.status =  $(this).data('status');;
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fetchDiscount();
				toastr.success(res.message);
			}
			else {
				toastr.error(res.message,'Error:');
			}
		});
	});
}

/*
* Pagination functions
*
*/
var sn = 1;
function createPagination(total_questions) {
	if (total_questions % 50 == 0) {
		pages = total_questions / 50;
	} else{
		pages = parseInt(total_questions / 50) + 1;
	};

	html = '';
	html += '<li><a class="page-number" value="1">&laquo; &laquo; </a></li>';
	html += '<li><a class="previous-page">&laquo; Prev </a></li>';
	// html += '<span class="page-numbers">';

	for (var i = 0; i < pages; i++) {
		html += '<li class=""><a class="page-numbers page-number" value="'+(i+1)+'">'+(i+1)+'</a></li>';
	};
	// html += '</span>';
	html += '<li><a class="next-page">Next &raquo;</a></li>';
	html += '<li><a class="page-number" value="'+pages+'">&raquo;&raquo;</a></li>';

	$('.question-table-pagination').html(html);

	$('.page-number').off('click');
	$(".page-number").on('click', function(e) {
		e.preventDefault();

		page_no = parseInt($(this).attr('value'));
		limit = parseInt(page_no - 1) * 50;
		
		sn = (limit == 0)?1:limit+1; // set serial number
		
		/* calling function to fetch data*/
		fetchDiscount();

	});
	
	// previous button functions
	$('.previous-page').off('click');
	$('.previous-page').on('click', function(e) {
		e.preventDefault();
		if(!($(this).parent().hasClass('disabled'))) {

			page_no = parseInt($('.question-table-pagination:eq(0)').find('.active a').attr('value'));
			
			$('.question-table-pagination:eq(0) li a[value="'+(page_no - 1)+'"]').click();
		}
	});
	
	// next button functions
	$('.next-page').off('click');
	$('.next-page').on('click', function(e) {
		e.preventDefault();
		if(!($(this).parent().hasClass('disabled'))) {

			page_no = parseInt($('.question-table-pagination:eq(0)').find('.active a').attr('value'));
			$('.question-table-pagination:eq(0) li a[value="'+(page_no + 1)+'"]').click();
		}
	});
	

	// disable previous button function
	if(page_no == 1) {
		$('.question-table-pagination li .previous-page').parent().addClass('disabled');
	}
	// disable next button function
	if(page_no == parseInt($('.question-table-pagination:eq(0) li').length - 4)) {
		$('.question-table-pagination li .next-page').parent().addClass('disabled');
	}

	// adding active class to show which page is active
	$('.question-table-pagination').find('.active').removeClass('active');
	$('.question-table-pagination li a[value="'+page_no+'"]').parent().addClass('active');
	$('.question-table-pagination li .page-numbers').parent().addClass('hidden');
	$('.question-table-pagination li .page-numbers[value="'+(page_no- 1)+'"], .page-numbers[value="'+page_no+'"], .page-numbers[value="'+(page_no+ 1)+'"]').parent().removeClass('hidden');

}