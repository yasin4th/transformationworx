$(function() {
	FormData.Events.fetchResultAndReport();
	FormData.fetchProgram();
	FormData.Events.programChanged();
	$('#sub').hide();
});
var FormData = {};
FormData.Events = {};

FormData.fetchProgram = function() {
	var req = {};
	req.action = "getTestprograms";
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
			FormData.fillPrograms(res.data);
		else if(res.status == 0)
			toastr.error(res.message);
	});
}

FormData.fillPrograms =  function(programs) {
	html = '<option value="0">Select Package Name</option>';
	$.each(programs, function(p,program) {
		html += '<option value="'+program.id+'">'+program.name+'</option>';
	});
	$('.test-programs').html(html);
	// $('#test-programs-table').DataTable();
}

FormData.Events.programChanged = function() {
	$('.test-programs').off('change');
	$('.test-programs').on('change', function(e) {
		e.preventDefault();

		$('#sub').hide();
        if(this.value == 0)
        {
            $('.test-program-subjects').html('<option value="0"> Select Subject </option>');
        }else
        {

            if($('.test-type').val() == 3)
            {
                $('#sub').show(100);
                //$('#select-test-type')
                FormData.Events.fetchProgramSubjects();
            }
        }

		// $('.test-program-subjects').html('');
		// FormData.Events.fetchProgramSubjects();
	})
	//$('.test-type').off('change');
	$('.test-type').on('change', function (e) {
		e.preventDefault();

		var html = '';
        if(this.value == 3)
        {
            $('#sub').show(100);
            FormData.Events.fetchProgramSubjects();
        }
        else
        {
            $('#sub').hide(100);
            $('.test-program-subjects').html('<option value="0"> Select Subject </option>');
        }
	});
}

/**
* function to fetch subjects os programs
*/
FormData.Events.fetchProgramSubjects = function() {
	var req = {};
	req.action = "getTestProgramsSubjects";
	req.program_id = $('.test-programs').val();

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			FormData.Events.fillProgramSubjects(res.data);
		}
	});
}

/**
* function to fill subjects
*/
FormData.Events.fillProgramSubjects = function(subjects) {
	var html = '<option value="0"> Select Subject </option>';
	$.each(subjects, function(i, subject) {
		html += '<option  value="'+subject.id+'">';
		html += subject.name;
		html += '</option>';
	})
	$('.test-program-subjects').html(html);
}

FormData.Events.fetchResultAndReport = function() {
	$('#result-and-report-form').off('submit');
	$('#result-and-report-form').on('submit', function(e) {
		e.preventDefault();
		var req = {};
		req.action = 'fetchTestsReport';
		req.test_program = $('#result-and-report-form .test-programs').val();
		subject = $('#result-and-report-form .test-program-subjects').val();
		if(subject != 0)
		{
			req.subject = $('#result-and-report-form .test-program-subjects').val();			
		}
		req.test_type = $('#result-and-report-form .test-type').val();
		
		$.ajax({
			type: "post", 
			url: EndPoint,
			data: JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if (res.status == 1) {
				FormData.Events.fillResultAndReport(res.data);
				console.log(res.data);
			} else{
				toastr.error(res.message);
				console.log(res);
			};
			$('.create-report-page .tools a').removeClass('expand').addClass('collapse');
			$(".create-report-page .portlet-body").removeClass('hide');
		});
	})
}

FormData.Events.fillResultAndReport = function(reports) {
	var html = '';

	$.each(reports, function(i,report) {
		html += '<tr>';
		html += '<td>'+(i+1)+'</td>';
		html += '<td><a href="test-report.php?test='+report.test_paper_id+'">'+report.test_paper_id+'</a></td>';
		html += '<td>'+report.name+'</td>';
		html += '<td>'+report.total_questions+'</td>';
		html += '<td>'+report.total_marks+'</td>';
		html += '<td>'+parseFloat(report.avg_marks).toFixed(2)+'</td>';
		html += '<td>'+parseFloat(report.avg_percent_marks).toFixed(2)+'</td>';
		html += '<td>'+report.total_students+'</td>';
		html += '<td>'+report.student_attempted+'</td>';
		// html += '<td>'+report.total_questions+'</td>';
		// html += '<td>'+report.+'</td>';
	})

	$('#result-and-report-table tbody').html(html);
}