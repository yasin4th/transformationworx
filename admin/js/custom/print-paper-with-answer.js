test_paper_id = getUrlParameter('test_paper');



$(function() {
	fetchQuestionsOfTestPaper();
	fetchTestPaperDetails();
})



// this function fetch saved questions of test papers
function fetchQuestionsOfTestPaper() {
	var req	=	{};
	req.action = "fetchQuestionsOfTestPaper";
	req.testPaper = test_paper_id;
	$.ajax({
		'type' : 'post',
		'url'	:	EndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {

			html = genratePaper(res.data);
			$('#sections').html(html);
    		$.each($('input:radio'), function(i, radio){radio.disabled=true})
			mathsjax();
			MathJax.Hub.Typeset();
			setTimeout(function() {
          //       var divContents = $('body').html();
		        // var printWindow = window.open('','', 'width=800,height=400');
		        // printWindow.document.write('<html><head><title>'+$('#test-paper-name').val()+'</title>');
		        // printWindow.document.write('</head><body >');
		        // printWindow.document.write(divContents);
		        // printWindow.document.write('</body></html>');
		        // printWindow.document.close();
		        window.print();
			},3000)
		}
		else
			toastr.error(res.message,"ERROR:")
	});
}


// function to create html of test paper when question are set
// function genratePaper(sections) {
// 	var html = "";
// 	$.each(sections, function(i,section) {
// 		// html += '<div data-s_id="'+section.section.id+'" data-totalQuestion="'+section.section.total_question+'" class="section portlet box blue"> <div class="portlet-title"> <div class="caption"> <i class="fa fa-gift"></i> '+section.section.name+'</div> <div class="tools"> <a href="#import-section-modal" data-toggle="modal"><span class="white i-font-size"><i class="fa fa-cloud-download" style="margin-top:0px"></i></span></a> <a class="collapse"></a> </div> </div> <div class="portlet-body form"> <div id="questions" class="form-body">';
// 		html += '<div data-s_id="'+section.section.id+'" data-totalQuestion="'+section.section.total_question+'" class="section portlet box blue"> <div class="portlet-title"> <div class="caption"> <i class="fa fa-pencil"></i> '+section.section.name+'</div> <div class="tools"> <a class="collapse"></a> </div> </div> <div class="portlet-body form"> <div id="questions" class="form-body">';
// 		$.each(section.section.questionData, function(x,question) {
// 			html += '<div data-q_id="'+ question.question_id +'" data-section_id="'+question.section_id+'" data-lable_id="'+question.lable_id+'" class="question portlet box purple"> <div class="portlet-title"> <div class="caption"> Question '+(x+1)+' </div> <div class="tools"> <a href="" class="collapse"></a> </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> </div> <div class="col-md-11 col-xs-10"> <div class="option row"> <div class="col-md-12 col-xs-12"> <ol class="upper-alpha">';
// 			$.each(question.options,function(z,option) {
// 				html += '<li><span>'+option.option+'</span></li>';
// 			});
// 			html += '<ol> </div></div> </div> </div>';
// 			html += '<div class="row">  <div class="col-md-12">  </div> </div>';
// 			html += '<div class="row"> <div class="col-md-12 col-xs-12"> <label style="font-weight: 700;">Solution/Explanation</label> <div> '+question.description+' </div> </div> </div>';
// 			html += '</div> </div> </div>';
// 		});
// 		html += '</div> </div></div>';
// 	});
// 	return html;
// }


function fetchTestPaperDetails() {
	var req = {};
	req.action = "fetchTestPaperDetails";
	req.test_paper_id = test_paper_id;
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			$('#title').text(res.data.name);
		}
		else
			toastr.error(res.message,"ERROR:");
	});	
}



// function to create html of test paper when question are set
function genratePaper(sections) {
	var html = "";
	$.each(sections, function(i,section) {
		//html += '<div data-s_id="'+section.section.id+'" data-totalQuestion="'+section.section.total_question+'" class="section portlet box blue"> <div class="portlet-title"> <div class="caption"> <i class="fa fa-pencil"></i> '+section.section.name+'</div> <div class="tools"> <a class="collapse"></a> </div> </div> <div class="portlet-body form"> <div id="questions" class="form-body">';
		
		html += '<div data-s_id="'+section.section.id+'" data-totalQuestion="'+section.section.total_question+'" class="section portlet"> <div class="portlet-title"> <div class="caption"> '+section.section.name+'</div> <div class="tools"> <a class="collapse"></a> </div> </div> <div class="portlet-body form"> <div id="questions" class="form-body">';
		$.each(section.section.questionData, function(x,question) {
			html += getQuestionHtml (x+1,question);
			
		});
		html += '</div> </div></div>';
	});
	return html;
}

/*
* function return question html by its type
*/
var parent = 0;
function getQuestionHtml (number,question) {
	html = '';
	switch(parseInt(question.type)) {
		
		case 1:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			if(parent == 0){
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			}else{
			html += ' <span style="float: left;">Q '+parent+'.'+(number)+'&nbsp; </span>';
			}
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			$.each(question.options,function(z,option) {
				if(option.answer == 0)
				{
					html += '<span style="display: block;"><input type="radio" style="float: left;margin-right: 5px;"/ disabled> '+option.option+' </span> ';
				}
				else
				{
					html += '<span style="display: block;"><input type="radio" style="float: left;margin-right: 5px;"/ disabled checked> '+option.option+' </span> ';
				}
			});
			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;

		case 2:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			if(parent == 0){
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			}else{
			html += ' <span style="float: left;">Q '+parent+'.'+(number)+'&nbsp; </span>';
			}
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			$.each(question.options,function(z,option) {
				if(option.answer == 0)
				{
					html += '<span style="display: block;"><input type="checkbox" style="float: left;margin-right: 5px;"/ disabled> '+option.option+' </span> ';
				}
				else
				{
					html += '<span style="display: block;"><input type="checkbox" style="float: left;margin-right: 5px;"/ disabled checked> '+option.option+' </span> ';					
				}
			});
			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;


		case 3:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			if(parent == 0){
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			}else{
			html += ' <span style="float: left;">Q '+parent+'.'+(number)+'&nbsp; </span>';
			}
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			$.each(question.options,function(z,option) {
				html += '<span style="display: block;"><input type="checkbox" style="float: left;margin-right: 5px;"/ disabled> '+option.option+' </span> ';
			});
			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;


		case 4:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			$.each(question.options,function(z,option) {
				html += '<span style="display: block;"><span style="float: left;margin-right: 5px;">(&nbsp;&nbsp; )</span> '+option.option+' </span> ';
			});
			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;


		case 5:

			html += '<div style="font-size: 13px; font-weight: bold;">';
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			html += '<span> '+question.question+' </span>';
			html += ' </div>';
			var opt1 = [], opt2 = [];
			//console.log(question.option);
			//html += '<div class="question portlet"> <div class="portlet-title"> <div class="caption"> Question '+(number+1)+' </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> </div> <div class="col-md-11 col-xs-10"> <div class="option row"><div class="col-md-12 col-xs-12"><div class="row text-center"> <div class="col-md-6"> <div class="col-md-11" > <div class="row" style="border: 1px solid #ccc; padding: 10px 10px 0px;">';
			$.each(question.options, function(z,option) {
				if(option.number == '1') {
					opt1.push(option.option);				
					
				}
				if(option.number == '2') {
					opt2.push(option.option);				
					
				}
			});
						
			html += '<table>';
			for(var x=0 ; x<opt1.length ; x++){
				html += '<tr>';
				html += '<td style="border:1px solid #ccc;"  width="300">'+opt1[x]+'</td>';
				html += '<td width="100"></td>';
				html += '<td style="border:1px solid #ccc;"  width="300">'+opt2[x]+'</td>';
				html += '</tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';				
			}
			if(opt2.length > opt1.length){
				for (var x = opt1.length; x < opt2.length; x++) {
					html += '<tr>';
					html += '<td width="300"></td>';
					html += '<td width="100"></td>';
					html += '<td style="border:1px solid #ccc;"  width="300">'+opt2[x]+'</td>';
					html += '</tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';	
				};
			}

				html += '</table>';
						
			html += '</div> </div> </div>';
			html += '</div> </div></div> </div> </div>';
			html += '<div class="row solution-explanation">  <div class="col-md-12"> <label style="font-weight: 700;">Solution/Explanation</label> <div class="sol-exp-border">'+question.description+'</div> </div> </div>';
			html += '</div> </div> </div>';
		break;

		case 6:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			if(parent == 0){
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			}else{
			html += ' <span style="float: left;">Q '+parent+'.'+(number)+'&nbsp; </span>';
			}
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			// $.each(question.options,function(z,option) {
			// 	html += '<span style="display: block;"><span style="float: left;margin-right: 5px;">(&nbsp;&nbsp; )</span> '+option.option+' </span> ';
			// });
			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;



		case 7:
			$('body').append('<div id="virtual-div"></div>');
			$('#virtual-div').html(question.question);
			dropDowns = $('#virtual-div select');
			$.each(dropDowns,function(x,dropDown) {
				$(dropDown).before('<input disabled>').remove();
			});
			virtualQuestion = $('#virtual-div').html();
			$('#virtual-div').remove();

			html += '<div style="font-size: 13px; font-weight: bold;">';
			if(parent == 0){
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			}else{
			html += ' <span style="float: left;">Q '+parent+'.'+(number)+'&nbsp; </span>';
			}
			html += '<span> '+virtualQuestion+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			for (var x = 1; x <= question.options[question.options.length - 1].number; x++) {				
				html += '<div class="form-group"> <label class=""> Select the correct options for drop down '+ x +' </label>';
				html += ' <div class="">';

				$.each(question.options,function(z,option) {
					html += '<span style="display: block;margin-left: 10px;"><input type="radio" style="float: left;margin-right: 5px;"/ disabled> '+option.option+' </span> ';
				});
				
				html += ' </div> ';				
				html += ' </div> ';
			};
			// $.each(question.options,function(z,option) {
			// 	html += '<span style="display: block;"><span style="float: left;margin-right: 5px;">(&nbsp;&nbsp; )</span> '+option.option+' </span> ';
			// });
			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;


		case 8:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			if(parent == 0){
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			}else{
			html += ' <span style="float: left;">Q '+parent+'.'+(number)+'&nbsp; </span>';
			}
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			$.each(question.options,function(z,option) {
				html += '<span style="display: block;"><span style="float: left;margin-right: 5px;">'+(z+1)+': </span> '+option.option+' </span> ';
			});
			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			html += '</div> </div> </div>';
		break;


		case 10:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;

		case 11:
			//console.log(question.passageQuestions);
			html += '<div style="font-size: 13px; font-weight: bold;">';
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			$.each(question.passageQuestions,function(t,passageQuestion) {
				parent = number;
				html += '';
				html += getQuestionHtml(t+1,passageQuestion);
				html += '';
				parent = 0;
			});
			html += '</div>';

			html += '</div>';
			html += '</div>';
			break;


		case 12:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class=""><table class="table table-bordered table-striped table-hover text-center" style="max-width: 720px;"><thead><tr><th></th>';
			/*
			* creating table head
			*/
			$.each(question.options, function(z,option) {
				if(option.answer == 1) {
					html += '<th class="text-center"> <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;">'+ option.option +'</div> </th>';
				}
			});
			html += '</tr> </thead> ';

			/*
			* creating table body
			*/
			var row = 2;
			html += '<tbody>'
			$.each(question.options ,function(i,option) {
				if(option.answer == row) {
					html += '<tr> <td> <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"> '+option.option+' </div></td>';
					option.answer = 0;
					$.each(question.options ,function(x,opt) {
						if(opt.answer == row) {
						
							html += '<td><div class="checkbox-list"><label><input type="checkbox" disabled></label></div></td>';
						
						}
					});				
					row++;
				}
			});
			html += '</tbody></table>';

			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;


		case 13:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			// $.each(question.options,function(z,option) {
			// 	if (option.number ==  1) {
			// 		from = option.option;
			// 	};
			// 	if (option.number ==  2) {
			// 		to = option.option;
			// 	};
			// });
			// html += '<span style="display: block;"><span style="float: left;margin-right: 5px;">From: </span> '+from+' </span> ';
			// html += '<span style="display: block;"><span style="float: left;margin-right: 5px;">To: </span> '+to+' </span> ';

			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;

		default:
		break;

	}
	return html;
}

function getQuestionHtmlForPassageQuestion(number,question) {
html = '';
	switch(parseInt(question.type)) {
		
		default:
			html += '<div data-q_id="'+ question.question_id +'" data-section_id="'+question.section_id+'" data-lable_id="'+question.lable_id+'" data-marks="'+question.max_marks+'" data-neg-marks="'+question.neg_marks+'" class="question portlet box purple"> <div class="portlet-title"> <div class="caption"> Question '+(number+1)+' </div> <div class="tools"> <a href="" class="collapse"></a> </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> </div> <div class="col-md-11 col-xs-10"> <div class="option row"><div class="col-md-12 col-xs-12"><div class="row">';
			$.each(question.options,function(z,option) {
				html += '<div class="col-md-12"><div class="qp-opt-border form-group"><span class="pull-left mrg-right10"><input type="radio" name="option" /></span>'+option.option+'</div></div>';
			});
			html += '</div> </div></div> </div> </div>';
			html += '<div class="row">  <div class="col-md-12"> <label style="font-weight: 700;">Solution/Explanation</label> <div class="sol-exp-border">'+question.description+'</div> </div> </div>';
			html += '</div> </div> </div>';
		break;
	}

	return html;

}