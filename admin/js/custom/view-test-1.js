test_program = getUrlParameter('test_program');
parent_id = getUrlParameter('parent_id')
parent_type = getUrlParameter('parent_type')

$(function(){
	
	$(".test-program-name").attr('href', 'view-test-program.php?test_program='+test_program)

	fetchAttachedTestPaper();
	fetchTestPaperTypes();
	setEventSaveTestPaperDetails();
	setDateTimePicker();
})

function fetchAttachedTestPaper() {
	var req = {};
	req.action = 'fetchAttachedTestPaper';
	req.parent_id = parent_id;
	req.parent_type = parent_type;
	req.test_program = test_program;
	req.type = getUrlParameter('type');

	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			// console.log(res);
			$(".test-program-name").text(res.test_program);
			fillTestPapers(res.data);
		}
		else {
			toastr.error(res.message,'Error');
		}
	});
}

function fillTestPapers(test_paper) {
	html = '';
	$.each(test_paper,function(x,paper) {
		html += '<tr data-test-paper-id="'+paper.id+'" data-id="'+paper.row_id+'"> <td> '+paper.name+' </td> <td> '+paper.start_time+' </td> <td> '+paper.end_time+' </td> <td class="center"> '+paper.total_attempts+' </td> <td> <a class="view-details">View Test</a>/<a href="untitle-question-paper.php?test_paper='+paper.id+'&status='+paper.status+'">View Questions</a> </td> <td> <a class="delete-test" style="color: rgb(230, 12, 12);font-weight: bold; text-align:center;"><i class="fa fa-trash-o"></i></a> </td>  </tr>';
	});
	$('#test-papers-table tbody').html(html);	
	setViewDetailsEvent();
	setEventDeleteTest();
}

/*
* this function set event to show test paper details in a model 
*/
function setViewDetailsEvent() {
	$('.view-details').off('click');
	$('.view-details').on('click', function(e) {
		e.preventDefault();
		
		var req = {};
		req.action = 'fetchTestPaperDetails';
		req.test_paper_id = $(this).parents('tr').eq(0).data('test-paper-id');
		
		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);			

			$('#test-paper-details').modal({backdrop: 'static'});
		
			if(res.data.start_time.length == 1)
				$('#test-paper-schedule').prop('checked', false).parent().removeClass('checked');
			else if(res.data.start_time.length > 1)
				$('#test-paper-schedule').prop('checked', true).parent().addClass('checked');

			if(!$('#test-paper-schedule').prop('checked'))
				$('#test-time').hide();
			else
				$('#test-time').show();

			$("#save-test-paper-modal").modal("show");
			$("#test-paper-instructions").attr('contenteditable',true);
		
			// fillSubjects(res.subjectsList);
			
			$('#test-paper-details').find('.name').val(res.data.name);
			$('#test-paper-id').val(res.data.id);
			$('#test-paper-name').val(res.data.name);
			$('#test-paper-type').val(res.data.type);
			$('#test-paper-class').val(res.data.class);
			$('#test-paper-subjects').val(res.data.subjects);
			$('#test-paper-time').val(res.data.time);
			$('#test-paper-attempts').val(res.data.total_attempts);
			$('#test-paper-start').val(res.data.start_time);
			$('#test-paper-end').val(res.data.end_time);
			$('#test-paper-instructions').html(res.data.instructions);			
		});
	});
}

function fetchTestPaperTypes() {
	var req = {};
	req.action = "fetchTestPaperTypes";
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
			fillPaperTypes(res.data);
		else
			toastr.error("ERROR:"+res.message)
	});

}

function fillPaperTypes(types) {
	html = '<option value="0">Select Type</option>';
	$.each(types,function(x,type) {
		html += '<option value="'+type.id+'">'+type.type+'</option>';
	});
	$('#test-paper-type').html(html);
}

function setEventSaveTestPaperDetails() {
	$('#save-test-paper').on('click', function(e){
		e.preventDefault();
		var req = {};
		req.action = "saveTestPaperDetails";
		req.id	=	$('#test-paper-id').val();
		req.test_paper_name = $('#test-paper-name').val();
		req.test_paper_type = $('#test-paper-type').val();
		req.test_paper_attempts = $('#test-paper-attempts').val();
		req.test_paper_start = $('#test-paper-start').val();
		req.test_paper_end = $('#test-paper-end').val();
		req.test_paper_instructions = $('#test-paper-instructions').val();
		$.ajax({
			type	: "post",
			url: EndPoint,
			data: JSON.stringify(req) 
			
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				fetchAttachedTestPaper();
				$("#test-paper-details").modal("hide");
			}

		});

		

	});
}

function setEventDeleteTest() {
	$('.delete-test').off('click');
	$('.delete-test').on('click', function() {
		var req = {};
		req.action = 'deleteAttachedTest';
		req.id = $(this).parents('tr').data('id');
		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req)  
		}).done(function(res) {
			res = $.parseJSON(res);
			if (res.status == 1) {
				fetchAttachedTestPaper();
				toastr.success(res.message);
			} else{
				toastr.error(res.message);
			};
		});
	});
}

function setDateTimePicker() {
	$('#test-paper-start').datetimepicker();
    $('#test-paper-end').datetimepicker();
    $("#test-paper-start").on("dp.change", function (e) {
        $('#test-paper-end').data("DateTimePicker").minDate(e.date);
    });
    $("#test-paper-end").on("dp.change", function (e) {
        $('#test-paper-start').data("DateTimePicker").maxDate(e.date);
    });
}