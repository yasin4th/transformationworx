var question;
var description;
var question_id =  getUrlParameter('question_id');
var parent_id = 0;
var hintEditor;

$(function() {

	$('.question-id').text('QU00'+question_id);
	// $("#classes").select2();
	$("#subjects").select2({
		placeholder: "Subjects ( multiple subjects )"
	});
	$("#units").select2({
		placeholder: "Units ( multiple units )"
	});
	$("#chapters").select2({
		placeholder: "Chapters ( multiple Chapter )"
	});
	$("#topics").select2({
		placeholder: "Topics ( multiple Topic )"
	});
	$("#tags").select2({
		placeholder: "Tags ( multiple Tags )"
	});
	$("#difficulty-levels").select2({
		placeholder: "Difficulty-Levels ( multiple Difficulty Levels )"
	});
	$("#skills").select2({
		placeholder: "Skills ( multiple Skills )"
	});

	getQuestionDetails();
	
	CKEDITOR.config.customConfig = 'full-toolbar-config.js';
	CKEDITOR.disableAutoInline = true;
});

// function to fetch question tags
function getQuestionDetails() {
	var req = {};
	req.action = 'getQuestionDetails';
	req.question_id = question_id;
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
			// console.log(res);
		if(res.status == 1) {
			//console.log(res);
			fillQuestionDetails(res);
		}
		else{
			toastr.error('Error');
		}
	});	
}

// function fill the tagged data of questions
function fillQuestionDetails(data) {
	if(data.fill.classes != undefined)
		fillClasses(data.fill.classes);
	if((data.fill.subjects) != undefined)
		fillSubjects(data.fill.subjects);
	if((data.fill.units) != undefined)
		fillUnits(data.fill.units);
	if((data.fill.chapters) != undefined)
		fillChapters(data.fill.chapters);
	if((data.fill.topics) != undefined)
		fillTopics(data.fill.topics);
	if(data.fill.tags != undefined)
		fillTags(data.fill.tags);
	if(data.fill.difficulty != undefined)
		fillDifficultyLevel(data.fill.difficulty);
	if(data.fill.skills != undefined)
		fillSkills(data.fill.skills);
	// question = CKEDITOR.inline('question-box', {noChangeEvent : 1});
	//alert(data.questionData.type);
	//$('#question-box').html(data.questionData.question);
	//console.log(data.questionData.question);
	
	//$('#question-box').html(data.questionData.question);	    
	//questioneditor = CKEDITOR.inline('question-box');
	alert("hi");
	/*questioneditor.on("instanceReady", function(event)
	{
	    //CKEDITOR.instances['question-box'].setData( data.questionData.question );
	    if(data.questionData.type == '8'){

	 	}
		questioneditor = CKEDITOR.inline('question-box');
	});*/
	
	descriptionEditor = CKEDITOR.inline('description-box');
	
	
	CKEDITOR.instances['description-box'].setData(data.questionData.description);
	$('#description-box').html(data.questionData.description);
	setTimeout(function(){$('#description-box').html(data.questionData.description)},1500);
	
	$('#question-box').attr('type', data.questionData.type);
	$('#question-status').val(data.questionData.quality_check);
	
	html = fillOptions(data.questionData.type,data.options);	

	$('#options').append(html);
	$('#options-box').removeClass('hidden');

	Metronic.init();
	setTaggedData(data.tagged);
	// setEventTOUpdateOption();
	$("#classes").val(data.questionData.class_id);
	setEventOnClickSaveButton();
	setEventUpdateQC();
	loadFunctionsOfQuestion();
	setEventDeleteOptions();
	initCkeditor();


	//remove in case of passage type question
 	if(data.questionData.type == '11'){
 		$('#options-box').remove();
 		$('#solution').remove();
 		$('#buttons').remove();
 		$('#buttonsdd').remove();
		$('#buttonsdnd').remove();

 	}

 	
 	if(data.questionData.type == '6'){
 		
 		$('#buttons').show();
 		$('#buttonsdd').remove();
		$('#buttonsdnd').remove();

 		setEventAddBlank();
 		setEventDeleteOptions();
 		setEventDeleteFbOptions();
 		
 		//$('#solution').remove();
 	}
 	if(data.questionData.type == '7'){
 		
 		$('#buttonsdd').show();
 		$('#buttons').remove();
 		$('#buttonsdnd').remove();

 		setEventAddDropDown();
		setEventAddOptionsInDropDown();
		setEventKeyup();
		setEventDeleteFbDDOptions();
 		//console.log($('#question-box').parent());
 		//$('#solution').remove();

 	}
 	if(data.questionData.type == '8'){
 		
 		$('#buttonsdnd').show();
 		$('#buttons').remove();
 		$('#buttonsdd').remove();

 		setEventDeleteFbDndOptions();
		setEventFbDndAddBlank();
 		//console.log($('#question-box').parent());
 		//$('#solution').remove();

 	}
	// $("#classes").select2();
}

function setTaggedData(data) {
	if((data.subject).length > 0)
		$("#subjects").val(data.subject);
	if((data.unit).length > 0)
		$("#units").val(data.unit);
	if((data.chapter).length > 0)
		$("#chapters").val(data.chapter);
	if((data.topic).length > 0)
		$("#topics").val(data.topic);
	if((data.difficulty).length > 0)
		$('#difficulty-levels').val(data.difficulty);
	if((data.tag).length > 0)
		$('#tags').val(data.tag);
	if((data.skill).length > 0)
		$('#skills').val(data.skill);

	$("#subjects").select2();
	$("#units").select2();
	$("#chapters").select2();
	$("#topics").select2();
	$('#difficulty-levels').select2();
	$('#tags').select2();
	$('#skills').select2();	
}

function setEventUpdateQC() {
	$('#question-status').off('change');
	$('#question-status').on('change', function(e){
		e.preventDefault();
		var req  = {};
		req.action = "changeQualityCheck";
		req.quality_check =  $(this).val();
		req.question_id	=	question_id;
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
				// console.log(res);
			if(res.status == 1){
				console.log(res);
			}
			else{
				toastr.error('Error: '+res.message);
			}
		});
	});
}

function setEventTOUpdateOption() {
	// function fire on blur to save option
	$('.option').on('blur',function(e){
		e.preventDefault();
		var req  = {};
		req.action = "updateOption";
		req.option =   $(this).html();
		req.option_id	=	$(this).data('id');
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
				// console.log(res);
			if(res.status == 1){
				console.log(res);
			}
			else{
				toastr.error('Error: '+res.message);
			}
		});
	})
}

function setEventOnFormSubmit() {
	// event fired when admin adds a question
	$('.btn_AddQuestion').off('click');
	$('.btn_AddQuestion').on('click', function(e) {
		e.preventDefault();
		if($("#classes").val() != 0 && $("#subjects").val() != null) {
			id = $('.passage-question-types option:selected').data('id');
			switch(id) {
				case 1:
					saveSCQ();
					setTimeout(function() {location.reload();}, 1000);
					break;
			
				case 2:
					saveMAQ();
					setTimeout(function() {location.reload();}, 1000);
					break;
				
				case 3:
					saveTF();
					setTimeout(function() {location.reload();}, 1000);
					break;


				case 6:
					saveFb();
					setTimeout(function() {location.reload();}, 1000);
					break;

				case 7:
					saveFbDD(data);
					setTimeout(function() {location.reload();}, 1000);
					break;

				case 8:
					saveFbDnd();
					setTimeout(function() {location.reload();}, 1000);
					break;


			
				default:
					toastr.error('Please Select A question type.');
					break;
			}
		}
		else {
			toastr.error('Please Select Class and atleast one Subjects.');		
		}
	});
}