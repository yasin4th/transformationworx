$(function() {
	fetchStudentAssignments();
	setEvent();
});

function fetchAssigmentsReadyForCertificate(argument) {
	var req = {};
	req.action = "fetchAssigmentsReadyForCertificate";
	req.id = getUrlParameter('id');
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		if(res.status == 1){
			fillStudentAssignments(res.up_assignments);
		}
		else if(res.status == 0)
			toastr.error(res.message);
	});
}

function fetchStudentAssignments() {

	var req = {};
	req.action = "fetchStudentAssignments";
	req.id = getUrlParameter('id');
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		if(res.status == 1){
			fillStudentAssignments(res.up_assignments);
		}
		else if(res.status == 0)
			toastr.error(res.message);
	});
}


function fillStudentAssignments(up_assignments) {

	if(up_assignments.length > 0) {
		document.getElementById('student-name').innerHTML = up_assignments[0].first_name + ' ' + up_assignments[0].last_name;
		document.getElementById('email').innerHTML = up_assignments[0].email;
		document.getElementById('document').innerHTML = up_assignments[0].doc_name;
		document.getElementById('course').innerHTML = up_assignments[0].class_name;
		document.getElementById('status').innerHTML = (up_assignments[0].status == '1') ? 'Pass' : 'Fail';
	}
}

function setEvent() {
	$('#license_form').off('submit');
	$('#license_form').on('submit', function(e) {
		e.preventDefault();
		var req = {}
		req.license = $('#license').val()
		req.id = getUrlParameter('id');
		req.action = 'generateCertificate'
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == 1){
				// fillStudentAssignments(res.up_assignments);
			}
			else if(res.status == 0)
				toastr.error(res.message);
		});
	});
}
