$(function() {
	printer();
})






// this function fetch saved questions of test papers
function printer() {
	console.log(DataOfQuestions);
	// html = genratePaper(res.data);
	// $('#sections').html(html);
	// mathsjax();
	// setTimeout(function() { window.print();},3000)
}


// function to create html of test paper when question are set
function genratePaper(sections) {
	var html = "";
	$.each(sections, function(i,section) {
		// html += '<div data-s_id="'+section.section.id+'" data-totalQuestion="'+section.section.total_question+'" class="section portlet box blue"> <div class="portlet-title"> <div class="caption"> <i class="fa fa-gift"></i> '+section.section.name+'</div> <div class="tools"> <a href="#import-section-modal" data-toggle="modal"><span class="white i-font-size"><i class="fa fa-cloud-download" style="margin-top:0px"></i></span></a> <a class="collapse"></a> </div> </div> <div class="portlet-body form"> <div id="questions" class="form-body">';
		html += '<div data-s_id="'+section.section.id+'" data-totalQuestion="'+section.section.total_question+'" class="section portlet box blue"> <div class="portlet-title"> <div class="caption"> <i class="fa fa-pencil"></i> '+section.section.name+'</div> <div class="tools"> <a class="collapse"></a> </div> </div> <div class="portlet-body form"> <div id="questions" class="form-body">';
		$.each(section.section.questionData, function(x,question) {
			html += '<div data-q_id="'+ question.question_id +'" data-section_id="'+question.section_id+'" data-lable_id="'+question.lable_id+'" class="question portlet box purple"> <div class="portlet-title"> <div class="caption"> Question '+(x+1)+' </div> <div class="tools"> <a href="" class="collapse"></a> </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> <a class="show-question-info-modal"><span class="i-circle"><i class="fa fa-info"></i></span></a> </div> <div class="col-md-11 col-xs-10"> <div class="option row"> <div class="col-md-12 col-xs-12"> <ol class="upper-alpha">';
			$.each(question.options,function(z,option) {
				html += '<li><span>'+option.option+'</span></li>';
			});
			html += '<ol> </div></div> </div> </div>';
			html += '<div class="row">  <div class="col-md-12">  </div> </div>';
			html += '</div> </div> </div>';
		});
		html += '</div> </div></div>';
	});
	return html;
}


function fetchTestPaperDetails() {
	var req = {};
	req.action = "fetchTestPaperDetails";
	req.test_paper_id = test_paper_id;
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			$('title, .name').text(res.data.name);
		}
		else
			toastr.error(res.message,"ERROR:");
	});	
}
