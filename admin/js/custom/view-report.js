var test_id = getUrlParameter("test");
var student_id = getUrlParameter("id");
$(function() {
	//fetchTestReport();
	fetchTestForStudent();
})

function fetchTestReport() {
	var req = {};
	req.action = "fetchSelectedTestReport";
	req.test_paper = test_id;
	
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		fillTestReport(res.data);
	})
}

function fillTestPaper(test_papers) {
	var html = '';
	if(test_papers.length!=0){
		$.each(test_papers, function(i,test_paper) {
			html += '<tr>';
			html += '<td> '+(i+1)+' </td>';
			html += '<td> '+test_paper.program_name+' </td>';
			html += '<td> '+test_paper.name+' </td>';
			html += '<td> '+test_paper.marks_archived+' </td>';
			html += '<td> '+test_paper.total_marks+' </td>';
			 html += '<td> '+test_paper.attempt_data_time+' </td>';
			html += '<td><a href=student-reports.php?test='+test_paper.id+'&student='+student_id+'>View Result</a></td>';
			html += '</tr>';
		});
	}
	else
	{
		var html = '<tr><td colspan="7">No test found.</td></tr>';	
	}

	$("#test-report-table tbody").html(html);
}


function fetchTestForStudent(){
	var req = {};
	req.action = "fetchTestForStudent";
	//req.test_paper = test_id;
	req.student_id = student_id;
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		$('#student-name').html("<h4>Student Name : " + res.username + "</h4>");
		 fillTestPaper(res.test_paper);
	})
}