var user_id;
$(function() {
	//filterStudents();
	user_id =  getUrlParameter('id');
	fetchClasses();
	$('#batch').select2({placeholder: "Select Batch"});
	updateStudent();
	changePassword();
	$('#class').on('change', function(e){
		e.preventDefault();
		if($('#class').val() != 0){
			$("#batch").select2('val',null);
			fetchBatches($('#class').val(),'0');
		}
		else
		{	
			$("#batch").select2('val',null);
			var html = "<option value='0'> Select Batch </option>";
			$('#batch').html(html);
		}
	});
	$('#cancel-btn').on('click', function(){
		window.location = 'manage-student.php';
	})

})

fetchStudents = function() {	
	req = {};
	req.action = 'fetchStudents';
	req.user_id = user_id;
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1){
			fillStudents(res);
			//$('#total-students').html(count(res.data));

		}
		else{
			// toastr.error('Error');
		}
	});	
}

// function to fill classes
fillStudents = function(student) {	
		$('#firstname').val(student.data[0].first_name);
		$('#lastname').val(student.data[0].last_name);
		$('#email').val(student.data[0].email);
		$('#mobile').val(student.data[0].mobile);
		console.log(student.data[0].class);
		$('#class').val($('#class').find("[text='"+student.data[0].class+"']").val());
		if(student.batch.length == 0){
		fetchBatches($('#class').find("[text='"+student.data[0].class+"']").val(),'0');
		}
		else{

		fetchBatches($('#class').find("[text='"+student.data[0].class+"']").val(),student.batch);
		}
		

}

updateStudent = function() {
	$('#edit-form').off('submit');
	$('#edit-form').on('submit', function(e) {
		e.preventDefault();
		if(validate()){

			var req = {};
			req.action = "updateStudent";
			req.firstname = $('#firstname').val();
			req.lastname = $('#lastname').val();
			req.email = $('#email').val();
			req.mobile = $('#mobile').val();
			req.class = $('#class option:selected').text();
			req.batch = $('#batch').select2('val');
			req.user_id = getUrlParameter('id');

			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)				

			}).done(function(res) {
				res = $.parseJSON(res);
				console.log(res);
				if(res.status == 1) {					
					toastr.success(res.message);
					setTimeout(function() {document.location = "manage-student.php";}, 800);
				}
				else {
					toastr.error(res.message);
				}
			});
		}
	});
}
validate = function() {

	firstname = $('#firstname').val();
	if(firstname==''){
		toastr.error("Please Enter Name.");
		return false;
	}
	email = $('#email').val();
	if(email==''){
		toastr.error("Please Enter Email.");
		return false;
	}
	if($('#class').val() == 0 ){
		toastr.error("Please Select Class.");
		return false;
	}
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(!regex.test(email)) {
		toastr.error("Please Enter Correct Email.");
		return;
	}
	return true;
}

changePassword = function() {
	$('#change-password-form').off('submit');
	$('#change-password-form').on('submit', function(e) {
		e.preventDefault();
		if(validatePassword()){
		//alert("hi");

			var req = {};
			req.action = "changeProfilePassword1";
			
			req.user_id = getUrlParameter('id');
			//alert(req.user_id);
			
			req.new_password = $("#new-password").val();
			req.confirm_password = $("#new-password1").val();

			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)				

			}).done(function(res) {
				res = $.parseJSON(res);
				console.log(res);
				if(res.status == 2)
				{
					toastr.error(res.data);

				}
				else if(res.status == 1)
				{
					$("#new-password").val('');
					$("#new-password1").val('');
					$("#change-password-modal").modal('hide');
					toastr.success("Password successfully updated");
				}
			});
		}
	});
}

validatePassword = function() {
	
	new_password = $("#new-password").val();
	confirm_password = $("#new-password1").val();
	if(new_password == confirm_password){
		return true;
	}
	toastr.error("Password not matching.");
	return false;

}


fetchClasses = function() {
	req = {};
	req.action = 'fetchClasses';
	$.ajax({
		'type'	:	'post',
		'url'	:	'../vendor/api.php',
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillClasses(res.classes);
		}
		else{
			toastr.error('Error');
		}
	});
				
}

fillClasses = function(classes){
	var html = "<option value='0'> Select Class </option>";
	$.each(classes,function( i,cls) {
		html += "<option text='"+cls.name+"' value='"+cls.id+"' data-id="+cls.id+">" +cls.name+ "</option>";
	});
	$('#class').html(html);
	fetchStudents();
 }

 fetchBatches = function(class_id,batch_id){
	req = {};
	req.action = 'fetchBatches';
	req.class = class_id;
	$.ajax({
		'type'	:	'post',
		'url'	:	'../vendor/api.php',
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillBatches(res.batches,batch_id);
		}
		else{
			toastr.error('Error');
		}
	});
}

fillBatches = function(batches,batch_id){
	var html = "<option value='0'> Select Batch </option>";
	$.each(batches,function( i, batch) {
		html += "<option  value='"+batch.id+"' data-id="+batch.id+">" +batch.name+ "</option>";
	});
	$('#batch').html(html);
	if(batch_id != 0){
		$('#batch').val(batch_id).select2();
	}

}

