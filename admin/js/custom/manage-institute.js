var limit = 0;
var page_no = 1;
var institute = 0;
$(function() {
	Institute.Events.getTheInstitutes();
	Institute.Events.createInstitute();
	Institute.Events.updateInstituteDetails();
	Institute.Events.filterInstitute();
	getTestprograms();


})

var Institute = Institute || {};
Institute.Events = {};
Institute.Validations = {};

Institute.Events.getTheInstitutes = function() {
	var req = {};
	req.action = "getTheInstitutes";
	req.limit = limit;
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		// console.log(res);
		if (res.status == 1) {
			Institute.Events.fillDataInTable(res.users);
			createPagination(res.total_questions);

			/** Setting all events **/
			Institute.Events.editDeatils();
			Institute.Events.changeStatus();
			Institute.Events.deleteInstitute();

			$("#create-institute-form .name").focus();

		} else{
			toastr.error(res.message, "Error:");
		};
	});
}

/**
* function to fill users Data In Table
*/
Institute.Events.fillDataInTable = function(users) {
	var html = '';
	$.each(users, function(i, user) {
		html += '<tr data-id="'+user.id+'">';

		html += '<td>' + user.name + '</td>';
		html += '<td>' + user.email + '</td>';
		html += '<td>' + user.mobile + '</td>';
		html += '<td>' + user.credits + '</td>';
		if (user.ins_lab_type == 1) {
			html += '<td>With Lab</td>';
		} else{
			html += '<td>Without Lab</td>';
		};
		html += '<td>' + user.status + '</td>';

		html += '<td>';
		html += '<a href="#edit-institute-modal" data-toggle="modal" class="edit" title="Edit"><i class="fa fa-pencil"></i></a> | ';
		if (user.active == 0) {
			html += '<a class="change-status" title="Activate"><i class="fa fa-check-square-o"></i></a> | ';
		} else{
			html += '<a class="change-status" title="Deactivate"><i class="fa fa-times-circle"></i></a> | '
		};
		html += '<a class="delete" style="font-weight: bold;" title="Delete"><i class="fa fa-trash-o"></i></a> | ';
		html += '<a href="generate-coupon.php?institute='+user.id+'" class="generate-coupon" title="Generate Coupon"><i class="fa fa-tag"></i></a> | '+
		'<a href="institute-coupons.php?institute='+user.id+'" class="coupons" title="Coupons"><i class="fa fa-tags"></i></a> | '+
		'<a href="manage-banner.php?institute='+user.id+'" class="banner" title="Bannner"><i class="fa fa-cogs"></i></a> | '+
		'<a href="#generatestudent-id-modal"  class="studs" data-id="'+user.id+'" title="Generate Student Ids"><i class="fa fa-code-fork"></i></a> | '+
		'<a href="#assign" class="assign-credits" data-id="'+user.id+'" title="Assign Credits"><i class="fa fa-plus-circle"></i></a> | ' +
		'<a href="#assign" class="assign" data-id="'+user.id+'" title="Assign Program"><i class="fa fa-plus-circle"></i></a>';
 		html += '</td>';


		html += '</tr>';
	});
	$("#institute-table tbody").html(html);
	setEvents();
}

/*
* create institute event
*/
Institute.Events.createInstitute = function() {
	$("#create-institute-form").off('submit');
	$("#create-institute-form").on('submit', function(e) {

        e.preventDefault();

		if (Institute.Validations.create()) {
			var req = {};
			req.action = "createInstitute";
			req.name = $("#create-institute-form .name").val();
			req.email = $("#create-institute-form .email").val();
			req.password = $("#create-institute-form .password").val();
			req.phone_no = $("#create-institute-form .phn-no").val();
			req.details = $("#create-institute-form .details").val();
			req.ins_lab_type = $("#create-institute-form .ins_lab_type:checked").val();

			Institute.Events.resetForm();
			$.ajax({
				type: "post",
				url: EndPoint,
				data: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				// console.log(res);
				if (res.status == 1) {
					Institute.Events.getTheInstitutes();
					toastr.success("Institute successfully created.");
					$("#add-institute-modal").modal('hide');
				} else{
					toastr.error(res.message, "Error:");
				};
			});
		}
	})
}

/*
* create institute validations
*/
Institute.Validations.create = function() {
	name = $("#create-institute-form .name").val();
	email = $("#create-institute-form .email").val();
	password = $("#create-institute-form .password").val();
	phone_no = $("#create-institute-form .phn-no").val();
	details = $("#create-institute-form .details").val();

	unsetError($('#create-institute-form .name'));
	unsetError($('#create-institute-form .email'));
	unsetError($('#create-institute-form .password'));
	unsetError($('#create-institute-form .phn-no'));
	unsetError($('#create-institute-form .details'));

	if(name == '' || name.length < 5)
	{
		setError($('#create-institute-form .name'), "Name must contain 5 characters.");
	}

	if(email == '' || email.length < 5)
	{
		setError($('#create-institute-form .email'), "Please fill a valid E-mail.");
	}

	if(password == '' || password.length < 5)
	{
		setError($('#create-institute-form .password'), "Password must contain 5 characters.");
	}

	if(phone_no == '' || phone_no.length < 10)
	{
		setError($('#create-institute-form .phn-no'), "Please enter valid phone number.");
	}

	// if(details == '' || details.length < 10)
	// {
	// 	setError($('#create-institute-form .user-role'), "Please Fill details.");
	// }


	if ($('#create-institute-form .has-error').length == 0) {
		return true;
	};
	return false;
}


/*
*  Event to edit institute details
*/
Institute.Events.editDeatils = function() {
	$(".edit").off('click');
	$(".edit").on('click', function(e) {
		e.preventDefault();
		user_id = $(this).parents('tr:eq(0)').data('id');
		var req = {};
		req.action = "getInstituteDetails";
		req.user_id = user_id;

		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req),
		}).done(function(res) {
			res = $.parseJSON(res);

			if (res.status == 1) {
				$("#edit-institute-form").data('id', user_id);
				$("#edit-institute-form .name").val(res.users.name);
				$("#edit-institute-form .email").val(res.users.email);
				$("#edit-institute-form .password").val('');
				$("#edit-institute-form .phn-no").val(res.users.mobile);
				$("#edit-institute-form .details").val(res.users.details);
				$('#edit-institute-form input[type=radio][value=' + res.users.ins_lab_type + ']').prop('checked', true);
				Metronic.init();
				// $("#create-institute-form").show('slow');
				// $("#edit-institute-form").hide('slow');
				// $("#create-institute-form").hide('slow');
				// $("#edit-institute-form").show('slow');
				// $('#edit-institute-form .name').focus();

			} else{
				toastr.error(res.message, "Error:");
			};
		});
	});


	// $('#edit-institute-form .cancel').off('click');
	// $('#edit-institute-form .cancel').on('click', function(e) {
	// 	e.preventDefault();
	// 	Institute.Events.resetForm();
	// 	$("#create-institute-form").show('slow');
	// 	$("#edit-institute-form").hide('slow');
	// 	$("#edit-institute-form").data('id', 0);
	// });
}


/*
*  Event to Update institute details
*/
Institute.Events.updateInstituteDetails = function() {
	$("form#edit-institute-form").off('submit');
	$("form#edit-institute-form").on('submit', function(e) {
		e.preventDefault();

		if(Institute.Validations.edit()) {
			var req = {};
			req.action = "updateInstituteDetails";
			req.id = $("#edit-institute-form").data('id');
			req.name = $("#edit-institute-form .name").val();
			req.email = $("#edit-institute-form .email").val();
			req.password = $("#edit-institute-form .password").val();
			req.mobile = $("#edit-institute-form .phn-no").val();

			req.details = $("#edit-institute-form .details").val();
			req.ins_lab_type = $("#edit-institute-form .ins_lab_type:checked").val();

			$.ajax({
				type: "post",
				url: EndPoint,
				data: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				// console.log(res);
				if (res.status == 1) {
					Institute.Events.getTheInstitutes();
					toastr.success(res.message);
					// $('#edit-institute-form .cancel').click();
					$("#edit-institute-modal").modal('hide');
				} else{
					toastr.error(res.message, "Error:");
				};
			});
		}
	})
}

/*
* edit institute validations
*/
Institute.Validations.edit = function() {
	name = $("#edit-institute-form .name").val();
	email = $("#edit-institute-form .email").val();
	password = $("#edit-institute-form .password").val();
	phone_no = $("#edit-institute-form .phn-no").val();
	details = $("#edit-institute-form .details").val();

	unsetError($('#edit-institute-form .name'));
	unsetError($('#edit-institute-form .email'));
	unsetError($('#edit-institute-form .password'));
	unsetError($('#edit-institute-form .phn-no'));
	unsetError($('#edit-institute-form .details'));

	if(name == '' || name.length < 5)
	{
		setError($('#edit-institute-form .name'), "Name must contain 5 characters.");
	}

	if(email == '' || email.length < 5)
	{
		setError($('#edit-institute-form .email'), "Please fill a valid E-mail.");
	}

	if(password == '' || password.length < 5)
	{
		setError($('#edit-institute-form .password'), "Password must contain 5 characters.");
	}

	if(phone_no == '' || phone_no.length < 10)
	{
		setError($('#edit-institute-form .phn-no'), "Please enter valid phone number.");
	}

	// if(details == '' || details.length < 10)
	// {
	// 	setError($('#edit-institute-form .user-role'), "Please Fill details.");
	// }


	if ($('#edit-institute-form .has-error').length == 0) {
		return true;
	};

	return false;
}

/*
*  Event to Reset form
*/
Institute.Events.resetForm = function() {
	$('input,textarea').val('');
	$('select').val('0');
	Institute.Events.getTheInstitutes();
}


/**
* function to change Status(active and In-active)
*/
Institute.Events.changeStatus = function() {
	$(".change-status").off('click');
	$(".change-status").on('click', function(e) {
		e.preventDefault();
		user_id = $(this).parents('tr:eq(0)').data('id');
		var req = {}
		req.action = "changeUserStatus";
		req.user_id = user_id;
		req.changed_status = 1;
		if ($(this).find('i').hasClass('fa-times-circle')) {
			req.changed_status = 0;
		};

		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req),
		}).done(function(res) {
			res = $.parseJSON(res);
			// console.log(res);
			if (res.status == 1) {
				toastr.success(res.message);
				Institute.Events.getTheInstitutes();
			}
			else {
				toastr.error(res.status);
			}
		});

	});
}

Institute.Events.deleteInstitute = function() {
	$(".delete").off('click');
	$(".delete").on('click', function(e) {
		e.preventDefault();
		if (confirm('Are you sure dant to delete '+$(this).parents('tr:eq(0)').find('td:eq(0)').text()+'?')) {
			user_id = $(this).parents('tr:eq(0)').data('id');
			var req = {}
			req.action = "deleteUser";
			req.user_id = user_id;

			$.ajax({
				type: "post",
				url: EndPoint,
				data: JSON.stringify(req),
			}).done(function(res) {
				res = $.parseJSON(res);

				if (res.status == 1) {
					toastr.success(res.message);
					Institute.Events.getTheInstitutes();
				}
				else {
					toastr.error(res.status);
				}
			});

		};
	});
}


Institute.Events.filterInstitute = function() {
	$("#filters").off('click');
	$("#filters").on('click', function() {
		$("#institute-filter-modal").modal("show");

	});


	$('#filter-form').off('submit');
	$('#filter-form').on('submit', function(e) {
		e.preventDefault();
		var req = {};

		req.action = "getTheInstitutes";
		req.limit = limit;

		if ($('#institute').val() != "") {
			req.name = $('#institute').val();
		};
		if ($('#email').val() != "") {
			req.email = $('#email').val();
		};

		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == 1)
			{	$("#institute-filter-modal").modal("hide");
				Institute.Events.fillDataInTable(res.users);
				createPagination(res.total_questions);
				/** Setting all events **/
				Institute.Events.editDeatils();
				Institute.Events.changeStatus();
				Institute.Events.deleteInstitute();

				$("#create-institute-form .name").focus();
			}
				//fillTestprograms(res.data);
			else if(res.status == 0)
				toastr.error(res.message);
		});

	});
}



/*
* Pagination functions
*
*/
var sn = 1;
function createPagination(total_questions) {
	if (total_questions % 50 == 0) {
		pages = total_questions / 50;
	} else{
		pages = parseInt(total_questions / 50) + 1;
	};

	html = '';
	html += '<li><a class="page-number" value="1">&laquo; &laquo; </a></li>';
	html += '<li><a class="previous-page">&laquo; Prev </a></li>';
	// html += '<span class="page-numbers">';

	for (var i = 0; i < pages; i++) {
		html += '<li class=""><a class="page-numbers page-number" value="'+(i+1)+'">'+(i+1)+'</a></li>';
	};
	// html += '</span>';
	html += '<li><a class="next-page">Next &raquo;</a></li>';
	html += '<li><a class="page-number" value="'+pages+'">&raquo;&raquo;</a></li>';

	$('.question-table-pagination').html(html);

	$('.page-number').off('click');
	$(".page-number").on('click', function(e) {
		e.preventDefault();

		page_no = parseInt($(this).attr('value'));
		limit = parseInt(page_no - 1) * 50;

		sn = (limit == 0)?1:limit+1; // set serial number

		/* calling function to fetch data*/
		Institute.Events.getTheInstitutes();

	});

	// previous button functions
	$('.previous-page').off('click');
	$('.previous-page').on('click', function(e) {
		e.preventDefault();
		if(!($(this).parent().hasClass('disabled'))) {

			page_no = parseInt($('.question-table-pagination:eq(0)').find('.active a').attr('value'));

			$('.question-table-pagination:eq(0) li a[value="'+(page_no - 1)+'"]').click();
		}
	});

	// next button functions
	$('.next-page').off('click');
	$('.next-page').on('click', function(e) {
		e.preventDefault();
		if(!($(this).parent().hasClass('disabled'))) {

			page_no = parseInt($('.question-table-pagination:eq(0)').find('.active a').attr('value'));
			$('.question-table-pagination:eq(0) li a[value="'+(page_no + 1)+'"]').click();
		}
	});


	// disable previous button function
	if(page_no == 1) {
		$('.question-table-pagination li .previous-page').parent().addClass('disabled');
	}
	// disable next button function
	if(page_no == parseInt($('.question-table-pagination:eq(0) li').length - 4)) {
		$('.question-table-pagination li .next-page').parent().addClass('disabled');
	}

	// adding active class to show which page is active
	$('.question-table-pagination').find('.active').removeClass('active');
	$('.question-table-pagination li a[value="'+page_no+'"]').parent().addClass('active');
	$('.question-table-pagination li .page-numbers').parent().addClass('hidden');
	$('.question-table-pagination li .page-numbers[value="'+(page_no- 1)+'"], .page-numbers[value="'+page_no+'"], .page-numbers[value="'+(page_no+ 1)+'"]').parent().removeClass('hidden');

}


getTestprograms = function() {
	var req = {};
	req.action = 'getTestprograms';
	req.limit = 0;
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		if(res.status==1)
		{
			fillPrograms(res.data);
		}
	})
}

fillPrograms = function(programs)
{
	html = '';
	html += '<option value="0">Select Program</option>'
	$.each(programs,function(i,p) {
		html += '<option value="'+p.id+'">'+p.name+'</option>';
	})
	$('#programs').html(html);
	$('#allPrograms').html(html);
}

updateDownloadLink = function()
{
	program = $('#programs').val();
	number = $('#number').val();
	if(program != 0 && !isNaN(number) && number > 0)
	{

		$('#download').attr('href',"../download-template.php?institute="+institute+"&program="+program+"&number="+number);

		$('#download').removeClass('disabled');

	}
	else
	{
		$('#download').addClass('disabled');
	}
}
setEvents = function()
{
	$('.assign-credits').on('click', function(e) {
		e.preventDefault();
		$('#assign-credits-modal').attr('data-id',$(this).attr('data-id'));
		$('#assign-credits-modal').modal('show');
	});

	$('.studs').on('click',function(e) {
		e.preventDefault();
		institute = $(this).data('id');
		$('#generatestudent-id-modal').modal('show');
	});

	$('#programs').on('change',function(e){
		updateDownloadLink();
	})
	$('#number').on('keyup',function(e){
		updateDownloadLink();
	})

	$('.assign').on('click',function(e) {
		e.preventDefault();
		institute = $(this).data('id');
		$('#assign-modal').attr('data-id', institute);
		$('#assign-modal').modal('show');
	});

	$('#assign-credits-program').off('submit');
	$('#assign-credits-program').on('submit', function(e) {
		e.preventDefault();
		var req = {};
		req.id = $('#assign-credits-modal').attr('data-id');
		req.credits = $('#credits').val();
		req.action = 'addCredits';

		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			toastr.options.timeOut = 2000;
			toastr.success(res.message);
			$('#assign-credits-modal').modal('hide');
			Institute.Events.getTheInstitutes();
		});
	});


	$('#assign-program').off('submit');
	$('#assign-program').on('submit', function(e) {
		e.preventDefault();
		if ($('#allPrograms').val() != "0")
		{

			var req = {};

			req.action = "assignProgramsInstitute";
			req.program = $('#allPrograms').val();

			if ($('#assign-modal').attr('data-id') != "") {
				req.institute = $('#assign-modal').attr('data-id');
			};

			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res){
				res = $.parseJSON(res);
				if(res.status == 1)
				{
					$("#assign-modal").modal("hide");
					toastr.success(res.message);
				}
				else if(res.status == 0)
				{
					$("#assign-modal").modal("hide");
					toastr.error(res.message);
				}
			});

		};
	});

	/*$('#download').on('click',function(e) {
		e.preventDefault();
		$('#download1').addClass('disabled');
		program = $('#programs').val();
		number = $('#number').val();
		if(program != 0 && !isNaN(number) && number > 0)
		{

			$('#download1').attr('href',"../download-template.php?institute="+institute+"&program="+program+"&number="+number);

			$('#download1').removeClass('disabled');


		}
		else
		{
			toastr.error("Invalid form data");
		}
	});*/
}