$(function() {
	users.getTheAdmins();
	
	users.create.setEventCreareAdmin();
	users.edit.setEventUpdateAdminDetails();
})

var users = {};


/**
* function to fetch users Data
*/
users.getTheAdmins = function() {
		var req = {};
		req.action = "getTheAdmins";
		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			console.log(res);
			if (res.status == 1) {
				users.fillDataInTable(res.users);
				users.edit.editEvent();
				users.edit.changeUserStatus();
				users.edit.deleteUser();

			} else{
				toastr.error(res.message, "Error:");
			};
		});
}


/**
* function to fill users Data In Table
*/
users.fillDataInTable = function(users) {
	var html = '';
	$.each(users, function(i, user) {
		html += '<tr data-id="'+user.id+'">';  
		
		html += '<td>' + user.name + '</td>';
		html += '<td>' + user.role + '</td>';
		html += '<td>' + user.status + '</td>';
		
		html += '<td>';
		html += '<a class="edit" title="Edit"><i class="fa fa-pencil"></i></a> | ';
		if (user.active == 0) {
			html += '<a class="change-status" title="Activate"><i class="fa fa-check-square-o"></i></a> | ';
		} else{
			html += '<a class="change-status" title="Deactivate"><i class="fa fa-times-circle"></i></a> | '
		};
		html += '<a class="delete" style="font-weight: bold;" title="Delete"><i class="fa fa-trash-o"></i></a>';
 		html += '</td>';

		
		html += '</tr>';
	});

	$("#users-table tbody").html(html);
}

users.create = {};
/**
* function to create admin
*/
users.create.setEventCreareAdmin = function() {
	$("#create-admin").off('submit');
	$("#create-admin").on('submit', function(e) {
		e.preventDefault();
			
		if (users.create.checkValue()) {
			var req = {};
			req.action = "createAdmin";
			req.name = $("#add-admin .name").val();
			req.email = $("#add-admin .email").val();
			req.password = $("#add-admin .password").val();
			req.role = $("#add-admin .user-role").val();
			users.resetForm();
			$.ajax({
				type: "post",
				url: EndPoint,
				data: JSON.stringify(req) 
			}).done(function(res) {
				res = $.parseJSON(res);
				// console.log(res);
				if (res.status == 1) {
					users.getTheAdmins();
					toastr.success("User: "+res.user.email+" successfully created.");
				} else{
					toastr.error(res.message, "Error:");
				};
			});
		}
	})
}


/**
* function to check and show error
*/
users.create.checkValue = function() {
	name = $("#add-admin .name").val();
	email = $("#add-admin .email").val();
	password = $("#add-admin .password").val();
	role = $("#add-admin .user-role").val();

	unsetError($('#add-admin .name'));
	unsetError($('#add-admin .email'));
	unsetError($('#add-admin .password'));
	unsetError($('#add-admin .user-role'));

	if(name == '' || name.length < 5)
	{
		setError($('#add-admin .name'), "Name must contain 5 characters.");
	}
	
	if(email == '' || email.length < 5)
	{
		setError($('#add-admin .email'), "Please fill a valid E-mail.");
	}
	
	if(password == '' || password.length < 5)
	{
		setError($('#add-admin .password'), "Password must contain 5 characters.");
	}
	
	if(role == 0)
	{
		setError($('#add-admin .user-role'), "Please select a User-Role for user.");
	}

	if ($('#add-admin .has-error').length == 0) {
		return true;
	};
	return false;
}


users.edit = {};
users.edit.editEvent = function() {
	$(".edit").off('click');
	$(".edit").on('click', function(e) {
		e.preventDefault();
		user_id = $(this).parents('tr:eq(0)').data('id');
		var req = {}
		req.action = "getAdminDetails";
		req.user_id = user_id;
		
		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req), 
		}).done(function(res) {
			res = $.parseJSON(res);
			console.log(res);
			if (res.status == 1) {
				$("#edit-admin .name").val(res.users.name);
				$("#edit-admin .email").val(res.users.email);
				$("#edit-admin .user-role").val(res.users.role);
				$("#edit-admin .password").val('');
				
				$("#add-admin").show('slow');
				$("#edit-admin").hide('slow');
				$("#add-admin").hide('slow');
				$("#edit-admin").show('slow');
				$("#edit-admin").data('id', user_id);
				$('#edit-admin .name').focus();

			} else{
				toastr.error(res.message, "Error:");
			};
		});
	});

	$('#edit-admin .cancel').on('click', function(e) {
		users.resetForm();
		$("#add-admin").show('slow');
		$("#edit-admin").hide('slow');
		$("#edit-admin").data('id', 0);
	})
}


/**
* function to create admin
*/
users.edit.setEventUpdateAdminDetails = function() {
	$("form#edit-admin-details").off('submit');
	$("form#edit-admin-details").on('submit', function(e) {
		e.preventDefault();
		
			
		if (users.edit.checkValue()) {
			var req = {};
			req.action = "updateAdminDetails";
			req.id = $("#edit-admin").data('id');
			req.name = $("#edit-admin .name").val();
			req.email = $("#edit-admin .email").val();
			req.password = $("#edit-admin .password").val();
			req.role = $("#edit-admin .user-role").val();
			$.ajax({
				type: "post",
				url: EndPoint,
				data: JSON.stringify(req) 
			}).done(function(res) {
				res = $.parseJSON(res);
				// console.log(res);
				if (res.status == 1) {
					users.getTheAdmins();
					toastr.success(res.message);
					$('#edit-admin .cancel').click();
				} else{
					toastr.error(res.message, "Error:");
				};
			});
		}
	})
}


/**
* function to check and show error when update admin details
*/
users.edit.checkValue = function() {
	name = $("#edit-admin .name").val();
	email = $("#edit-admin .email").val();
	password = $("#edit-admin .password").val();
	role = $("#edit-admin .user-role").val();

	unsetError($('#edit-admin .name'));
	unsetError($('#edit-admin .email'));
	unsetError($('#edit-admin .password'));
	unsetError($('#edit-admin .user-role'));

	if(name == '' || name.length < 5)
	{
		setError($('#edit-admin .name'), "Name must contain 5 characters.");
	}
	
	if(email == '' || email.length < 5)
	{
		setError($('#edit-admin .email'), "Please fill a valid E-mail.");
	}
	
	if(password == '' || password.length < 5)
	{
		setError($('#edit-admin .password'), "Password must contain 5 characters.");
	}
	
	if(role == 0)
	{
		setError($('#edit-admin .user-role'), "Please select a User-Role for user.");
	}

	if ($('#edit-admin .has-error').length == 0) {
		return true;
	};
	return false;
}

/**
* function to change Admin Status(active and In-active)
*/
users.edit.changeUserStatus = function() {
	$(".change-status").off('click');
	$(".change-status").on('click', function(e) {
		e.preventDefault();
		user_id = $(this).parents('tr:eq(0)').data('id');
		var req = {}
		req.action = "changeUserStatus";
		req.user_id = user_id;
		req.changed_status = 1;
		if ($(this).find('i').hasClass('fa-times-circle')) {
			req.changed_status = 0;
		};

		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req), 
		}).done(function(res) {
			res = $.parseJSON(res);
			// console.log(res);
			if (res.status == 1) {
				toastr.success(res.message);
				users.getTheAdmins();
			}
			else {
				toastr.error(res.status);
			}	
		});
		
	});
}

/**
* function to delete any Admin
*/
users.edit.deleteUser = function() {
	$(".delete").off('click');
	$(".delete").on('click', function(e) {
		e.preventDefault();
		user_id = $(this).parents('tr:eq(0)').data('id');
		var req = {}
		req.action = "deleteUser";
		req.user_id = user_id;

		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req), 
		}).done(function(res) {
			res = $.parseJSON(res);
			console.log(res);
			if (res.status == 1) {
				toastr.success(res.message);
				users.getTheAdmins();
			}
			else {
				toastr.error(res.status);
			}	
		});
		
	});
}

users.resetForm = function() {
	$('input').val('')
	$('select').val('0')
}