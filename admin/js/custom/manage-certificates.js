var STATUS = ['Pending', 'Pass', 'Fail']

$(function() {
	fetchAssignmentsReadyForCertificate();
	generateCertificate();
});

function fetchAssignmentsReadyForCertificate(argument) {
	var req = {};
	req.action = "fetchAssignmentsReadyForCertificate";
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		if(res.status == 1){
			fillData(res.data);
		}
		else if(res.status == 0)
			toastr.error(res.message);
	});
}

/*
* this funtion genrate html and display assignment
*/
function fillData(rows) {
	var html = "";
	$.each(rows, function(index, row) {
		var certificate = (row.license_no)? 'License no: '+row.license_no: '<button class="btn btn-primary btn-xs" onclick="createCertificate('+row.assignment_id+')">Generate</button>';
		html += `<tr>
			<td>
				`+row.first_name+' '+row.last_name+`
			</td>
			<td>
				`+row.course_name+`
			</td>
			<td>
				`+STATUS[row.assigment_status]+`
			</td>
			<td>
				`+row.marks_archived+`
			</td>
			<td>
				`+row.cut_off+`
			</td>
			<td>
				`+certificate+`
			</td>
		</tr>`
	});
	if (typeof oTable != "undefined") {
		oTable.fnDestroy();
	}
	$('#assigments-ready-for-certificate-table tbody').html(html);

	oTable = $('#assigments-ready-for-certificate-table').dataTable();
	//return html;
}

function createCertificate(assignment_id) {
	$('#assignment_id').val(assignment_id);
	$('#portlet-config').modal('show');
}


function generateCertificate() {
	$('#license_form').off('submit');
	$('#license_form').on('submit', function(e) {
		e.preventDefault();
		var req = {}
		req.license = $('#license').val();
		req.id = $('#assignment_id').val();
		req.action = 'generateCertificate'
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == 1){
				fetchAssignmentsReadyForCertificate();
				$('#portlet-config').modal('hide');
				toastr.success(res.message)
			}
			else if(res.status == 0)
				toastr.error(res.message);
		});
	});
}
