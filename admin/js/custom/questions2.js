var class_id;
var subject_ids;
var unit_ids;
var chapter_ids;
var topic_ids;
var tag_ids;
var difficultyLevel_ids;
var skill_ids;

// event fired when admin adds a question
$('#form-add-question').on('submit', function(e) {
	e.preventDefault();
	id = $('#question-types option:selected').data('id');
	switch(id) {
		case 1:
			saveSCQ(data);
			break;
	
		case 2:
			saveMAQ(data);
			break;
		
		case 3:
			saveTF(data);
			break;

		case 4:
			saveOrd(data);
			break;

		case 5:
			saveMatch(data);
			break;

		case 6:
			saveFb(data);
			break;

		case 7:
			// saveFbDD(data);
			break;

		case 8:
			saveFb(data);
			break;

		case 9:
			saveAr(data);
			break;

		case 10:
			saveDescriptive(data);
			break;

		case 11:
			break;
	
		default:
			toastr.error('Please Select A question type.');
			break;
	}
});


// Event Fired When user changes Question Type
$('#question-types').on('change', function(e) {
	id = $('#question-types option:selected').data('id');
	hintDiv(id);
	switch(id) {
		case 1:
			$('#multiChoise').addClass('hidden');
			$('#true-false').addClass('hidden');
			$('#ordering').addClass('hidden');
			$('#fill-blanks-dd').addClass('hidden');
			$('#fill-blanks').addClass('hidden');
			$('#descriptive').addClass('hidden');
			$('#assertion-reason').addClass('hidden');;
			$('#matching').addClass('hidden');
			$('#singleChoise').removeClass('hidden');
			scqLoadEditor();
			break;
		
		case 2:
			$('#singleChoise').addClass('hidden');
			$('#true-false').addClass('hidden');
			$('#ordering').addClass('hidden');
			$('#fill-blanks-dd').addClass('hidden');
			$('#fill-blanks').addClass('hidden');
			$('#descriptive').addClass('hidden');
			$('#assertion-reason').addClass('hidden');;
			$('#matching').addClass('hidden');
			$('#multiChoise').removeClass('hidden');
			maqLoadEditor();
			break;

		case 3:
			$('#singleChoise').addClass('hidden');
			$('#multiChoise').addClass('hidden');
			$('#ordering').addClass('hidden');
			$('#fill-blanks-dd').addClass('hidden');
			$('#fill-blanks').addClass('hidden');
			$('#descriptive').addClass('hidden');
			$('#assertion-reason').addClass('hidden');;
			$('#matching').addClass('hidden');
			$('#true-false').removeClass('hidden');
			trueFalseLoadEditor();
			break;
		
		case 4:
			$('#singleChoise').addClass('hidden');
			$('#multiChoise').addClass('hidden');
			$('#true-false').addClass('hidden');
			$('#fill-blanks-dd').addClass('hidden');
			$('#fill-blanks').addClass('hidden');
			$('#descriptive').addClass('hidden');
			$('#assertion-reason').addClass('hidden');;
			$('#matching').addClass('hidden');
			$('#ordering').removeClass('hidden');
			orderingLoadEditor();
			break;
		
		case 5:
			$('#singleChoise').addClass('hidden');
			$('#multiChoise').addClass('hidden');
			$('#true-false').addClass('hidden');
			$('#ordering').addClass('hidden');
			$('#fill-blanks-dd').addClass('hidden');
			$('#fill-blanks').addClass('hidden');
			$('#descriptive').addClass('hidden');
			$('#matching').addClass('hidden');
			$('#assertion-reason').addClass('hidden');;
			$('#matching').removeClass('hidden');
			matchLoadEditor();		
			break;

		case 6:
			$('#singleChoise').addClass('hidden');
			$('#multiChoise').addClass('hidden');
			$('#true-false').addClass('hidden');
			$('#ordering').addClass('hidden');
			$('#fill-blanks-dd').addClass('hidden');
			$('#descriptive').addClass('hidden');
			$('#assertion-reason').addClass('hidden');;
			$('#matching').addClass('hidden');
			$('#fill-blanks').removeClass('hidden');
			fbLoadEditor();
			break;
			
		case 7:
			$('#singleChoise').addClass('hidden');
			$('#multiChoise').addClass('hidden');
			$('#true-false').addClass('hidden');
			$('#ordering').addClass('hidden');
			$('#fill-blanks-dd').addClass('hidden');
			$('#descriptive').addClass('hidden');
			$('#fill-blanks').addClass('hidden');
			$('#matching').addClass('hidden');
			$('#assertion-reason').addClass('hidden');;
			$('#fill-blanks-dd').removeClass('hidden');
			fbdDDLoadEditor();
			break;

		case 8:
			$('#singleChoise').addClass('hidden');
			$('#multiChoise').addClass('hidden');
			$('#true-false').addClass('hidden');
			$('#ordering').addClass('hidden');
			$('#fill-blanks-dd').addClass('hidden');
			$('#descriptive').addClass('hidden');
			$('#assertion-reason').addClass('hidden');;
			$('#matching').addClass('hidden');
			$('#fill-blanks').removeClass('hidden');
			fbLoadEditor();
			break;

		case 9:
			$('#singleChoise').addClass('hidden');
			$('#multiChoise').addClass('hidden');
			$('#true-false').addClass('hidden');
			$('#ordering').addClass('hidden');
			$('#fill-blanks-dd').addClass('hidden');
			$('#fill-blanks').addClass('hidden');
			$('#descriptive').addClass('hidden');
			$('#matching').addClass('hidden');
			$('#assertion-reason').removeClass('hidden');
			arLoadEditor();
			break;
		
		case 10:
			$('#singleChoise').addClass('hidden');
			$('#multiChoise').addClass('hidden');
			$('#true-false').addClass('hidden');
			$('#ordering').addClass('hidden');
			$('#fill-blanks-dd').addClass('hidden');
			$('#fill-blanks').addClass('hidden');
			$('#assertion-reason').addClass('hidden');
			$('#matching').addClass('hidden');
			$('#descriptive').removeClass('hidden');
			descriptiveLoadEditor();
			break;

		case 11:
			$('#singleChoise').addClass('hidden');
			$('#multiChoise').addClass('hidden');
			$('#true-false').addClass('hidden');
			$('#ordering').addClass('hidden');
			$('#fill-blanks-dd').addClass('hidden');
			$('#fill-blanks').addClass('hidden');
			$('#assertion-reason').addClass('hidden');;
			$('#matching').addClass('hidden');
			$('#descriptive').addClass('hidden');
			break;

		default:
			$('#singleChoise').addClass('hidden');
			$('#multiChoise').addClass('hidden');
			$('#true-false').addClass('hidden');
			$('#ordering').addClass('hidden');
			$('#fill-blanks-dd').addClass('hidden');
			$('#fill-blanks').addClass('hidden');
			$('#descriptive').addClass('hidden');
			$('#matching').addClass('hidden');
			$('#assertion-reason').addClass('hidden');
			break;

	}
});

function hintDiv(questionType) {
	if(questionType == 0) {
		$('.hint').addClass('hidden')
	}
	else {
		$('.hint').removeClass('hidden')
		
	}

}

function eventAddHintRemoveHint() {
	$('.btn_AddHint').off('click')
	$('.btn_AddHint').on('click',function(e) {
		$('.AddHint').removeClass('hidden');
		$('.btn_AddHint').addClass('hidden');
		hintEditor = CKEDITOR.inline('hint-box');
		// descriptionEditor.on('blur', function(e){
			// data = descriptionEditor.getData();
		// });
	})
	$('.btn_removeHint').off('click')
	$('.btn_removeHint').on('click',function(e) {
		$('.AddHint').addClass('hidden');
		$('.btn_AddHint').removeClass('hidden');
	})

}