var limit = 0;
var page_no = 1;
var filter = false;
$(function() {
	getTestprograms();
	fetchClasses();
	$("#subjects").select2();
	setEventFilterTestProgram();
	setDateTimePicker();

	$('#classes').on('change', function(e) {
		$("#subjects").select2('val',null);
		$("#subjects").html('');

		var class_id = $(this).val();
		var target = $("#subjects");
		fetchSubjects(class_id ,target);
	});


	$('#create-coupons-form').off('submit');
	$('#create-coupons-form').on('submit', function(e) {
		e.preventDefault();

		var req = {};
		req.id = $('#generate-coupon-modal').data('program-id');
		req.action = "downloadCoupons";
		req.number_of_coupons = $('#number-of-coupons').val();
		req.prefix = $('#coupon-prefix').val();
		req.start_date = $('#start-date').data("datetimepicker").getDate().valueOf()/1000;
		req.end_date = $('#end-date').data("datetimepicker").getDate().valueOf()/1000;
		// req.institute = getUrlParameter('institute');

		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			// console.log(res);
			$('#generate-coupon-modal').modal('hide');
			toastr.success(res.message);

			uriContent = "data:application/octet-stream," +escape(res.coupon.toString());
		});
	});

	$('#create-enroll-form').off('submit');
	$('#create-enroll-form').on('submit', function(e) {
		e.preventDefault();

		var req = {};
		req.id = $('#generate-enroll-modal').attr('data-program-id');
		req.action = "createEnrollment";
		req.code = $('#enroll-code').val();

		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			// console.log(res);
			$('#generate-enroll-modal').modal('hide');
			toastr.success(res.message);

			// uriContent = "data:application/octet-stream," +escape(res.coupon.toString());
		});
	});
});

function getTestprograms() {
	var req = {};
	req.action = "getTestprograms";
	req.limit = limit;
	filter = false;
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		if(res.status == 1){
			fillTestprograms(res.data);
			createPagination(res.total_questions);
		}else if(res.status == 0)
			toastr.error(res.message);
	});
}

function fillTestprograms(programs) {
	html = '';
	$.each(programs, function(p,tr) {
		html += '<tr data-id="'+tr.id+'"">';
		html += '<td>'+tr.name+'</td>';
		html += '<td> '+tr.class+'</td>';
		html += '<td>'+tr.price+'</td>';
		html += '<td>'+tr.test_papers+'</td>';
		html += '<td>'+tr.credits+'</td>';
		html += '<td><a href="view-test-program1.php?test_program='+tr.id+'">View</a></td>';
		html += '<td> <div style="text-align:center;">';
		var status = (parseInt(tr.status))? '<i title="Active" class="fa fa-check-circle">' : '<i title="In-active" class="fa fa-times-circle">';
		html += '<a class="change-package-status"> ' + status +' </i></a>&nbsp;|&nbsp;';
		html += '<a class="delete" style="color: rgb(230, 12, 12);font-weight: bold; text-align:center;"><i class="fa fa-trash-o"></i></a> | ';
		html += '<a href="#generate-coupon-modal" class="generate-coupon" title="Generate Coupon" data-id="'+tr.id+'"><i class="fa fa-tag"></i></a> | '
		html += '<a href="#generate-enroll-modal" class="generate-enroll" title="Generate Enrollment" data-id="'+tr.id+'"><i class="fa fa-bolt"></i></a> | '
		html += '<a href="program-enrollment.php?id='+tr.id+'" title="View Enrollment Ids"><i class="fa fa-eye"></i></a> | '
		html += '</div>';
		html += '</td>';
		html += '</tr>';
	});
	//console.log(programs);

	if(programs.length == 0)
	{
		$('#test-programs-table tbody').html('<tr><td colspan="6">No Package Found.</td></tr>');
	}
	else
	{
		$('#test-programs-table tbody').html(html);
	}
	setEventDeleteTestProgram();
	changeStatus();
}


/*
* set event to delete test paper
*/
function setEventDeleteTestProgram() {
	$('.delete').off('click');
	$('.delete').on('click', function(e){
		e.preventDefault();
		if(confirm("Are you sure want to delete this Program?")) {
			var req = {};
			req.action = "deleteTestProgram";
			req.id	=	$(this).parents('tr').eq(0).data('id');
			$.ajax({
				type	: "post",
				url: EndPoint,
				data: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.success(res.message,'Test Program');
					getTestprograms();
				}
			});
		}
	});

	$('.generate-coupon').off('click');
	$('.generate-coupon').on('click', function(e){
		e.preventDefault();
		id = $(this).parents('tr:eq(0)').data('id');
		tp_name = $(this).parents('tr:eq(0)').find('td:eq(0)').text();
		$('#test-program-name').text(tp_name);
		$('#generate-coupon-modal').attr('data-program-id', id);
		$('#generate-coupon-modal').modal('show');
		// $('#generate-coupon-modal').attr('data_id',$(this).attr('data-id'));
		// $('#generate-coupon-modal').modal('show');
	});

	$('.generate-enroll').off('click');
	$('.generate-enroll').on('click', function(e){
		e.preventDefault();
		id = $(this).parents('tr:eq(0)').data('id');
		tp_name = $(this).parents('tr:eq(0)').find('td:eq(0)').text();
		$('#test-enroll-name').text(tp_name);
		$('#generate-enroll-modal').attr('data-program-id', id);
		$('#generate-enroll-modal').modal('show');
		// $('#generate-coupon-modal').attr('data_id',$(this).attr('data-id'));
		// $('#generate-coupon-modal').modal('show');
	});
}


// function to fetch classes
function fetchClasses() {
	req = {};
	req.action = 'fetchClasses';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillClasses(res.classes);
		}
		else{
			// toastr.error('Error');
		}
	});

}

// function to fill classes
function fillClasses(classes) {
	var html = "<option value='0'> Select Class </option>";
	$.each(classes,function( i,cls) {
		html += "<option value='"+cls.id+"' data-id="+cls.id+">" +cls.name+ "</option>";
	});
	$('#test-paper-class').html(html);
	$('#classes').html(html);
}

// function to fetch subjects
function fetchSubjects(class_id, target) {
	req = {};
	req.action = 'fetchSubjects';
	req.class_id = class_id;
	if(req.class_id) {
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				fillSubjects(res.subjects, target);
			}
			else{
				// toastr.error("Unknown class");
			}
		});
	}
}

// function to fill subjects
function fillSubjects(subjects, target) {
	var html = "";
	$.each(subjects,function( i,sub) {
		html += '<option val="'+(i+1)+'" value="'+sub.id+'" data-id="'+sub.id+'">'+sub.name+'</option>';
	});
	$(target).html(html);
}

function setEventFilterTestProgram() {
	$('#filter-form').off('submit');
	$('#filter-form').on('submit', function(e) {
		$('#test-program-filter-modal').modal('hide');
		e.preventDefault();
		filterTestProgram();

	});
}

function filterTestProgram() {
		var req = {};
		filter = true;
		req.action = "getTestprograms";
		if ($('#classes').val() != "0") {
			req.class = $('#classes').val();
		};

		if ($('#subjects').val() != null) {
			req.subjects = $('#subjects').val();
		};

		if ($('#test-program-status').val() != "2") {
			req.status = $('#test-program-status').val();
		};

		if ($('#text').val() != "") {
			req.name = $('#text').val();
		};

		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == 1){
				fillTestprograms(res.data);
				createPagination(res.total_questions);
			}
			else if(res.status == 0)
				toastr.error(res.message);
		});

}

function resetForm(){
	$('input,textarea').val('');
	$('select').val('0');
	getTestprograms();
}


/*
* Pagination functions
*
*/
var sn = 1;
function createPagination(total_questions) {
	if (total_questions % 50 == 0) {
		pages = total_questions / 50;
	} else{
		pages = parseInt(total_questions / 50) + 1;
	};

	html = '';
	html += '<li><a class="page-number" value="1">&laquo; &laquo; </a></li>';
	html += '<li><a class="previous-page">&laquo; Prev </a></li>';
	// html += '<span class="page-numbers">';

	for (var i = 0; i < pages; i++) {
		html += '<li class=""><a class="page-numbers page-number" value="'+(i+1)+'">'+(i+1)+'</a></li>';
	};
	// html += '</span>';
	html += '<li><a class="next-page">Next &raquo;</a></li>';
	html += '<li><a class="page-number" value="'+pages+'">&raquo;&raquo;</a></li>';

	$('.question-table-pagination').html(html);

	$('.page-number').off('click');
	$(".page-number").on('click', function(e) {
		e.preventDefault();

		page_no = parseInt($(this).attr('value'));
		limit = parseInt(page_no - 1) * 50;

		sn = (limit == 0)?1:limit+1; // set serial number

		/* calling function to fetch data*/
		if(filter){
			filterTestProgram();
		}else{
			getTestprograms();
		}

	});

	// previous button functions
	$('.previous-page').off('click');
	$('.previous-page').on('click', function(e) {
		e.preventDefault();
		if(!($(this).parent().hasClass('disabled'))) {

			page_no = parseInt($('.question-table-pagination:eq(0)').find('.active a').attr('value'));

			$('.question-table-pagination:eq(0) li a[value="'+(page_no - 1)+'"]').click();
		}
	});

	// next button functions
	$('.next-page').off('click');
	$('.next-page').on('click', function(e) {
		e.preventDefault();
		if(!($(this).parent().hasClass('disabled'))) {

			page_no = parseInt($('.question-table-pagination:eq(0)').find('.active a').attr('value'));
			$('.question-table-pagination:eq(0) li a[value="'+(page_no + 1)+'"]').click();
		}
	});


	// disable previous button function
	if(page_no == 1) {
		$('.question-table-pagination li .previous-page').parent().addClass('disabled');
	}
	// disable next button function
	if(page_no == parseInt($('.question-table-pagination:eq(0) li').length - 4)) {
		$('.question-table-pagination li .next-page').parent().addClass('disabled');
	}

	// adding active class to show which page is active
	$('.question-table-pagination').find('.active').removeClass('active');
	$('.question-table-pagination li a[value="'+page_no+'"]').parent().addClass('active');
	$('.question-table-pagination li .page-numbers').parent().addClass('hidden');
	$('.question-table-pagination li .page-numbers[value="'+(page_no- 1)+'"], .page-numbers[value="'+page_no+'"], .page-numbers[value="'+(page_no+ 1)+'"]').parent().removeClass('hidden');

}

function changeStatus(){
	$('.change-package-status').off('click');
	$('.change-package-status').on('click',function(e){
		e.preventDefault();
		var req ={};
		req.action = "changePackageStatus";
		req.package_id = $(this).parents('tr').data('id');
		req.package_status = 0;
		if($(this).find('i:eq(0)').hasClass('fa-times-circle')){
			req.package_status = 1;
		}
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				toastr.success(res.message);
				getTestprograms();
			}
			else{
				toastr.error(res.message);
			}
		});



	})
}

function setDateTimePicker() {

    $('#start-date').datetimepicker({
    	format: "dd MM yyyy - hh:ii",
    	startDate: new Date,
    	showMeridian: true,
        autoclose: true,
        todayHighlight: true
    	// todayBtn: true
    });
    $('#end-date').datetimepicker({
    	format: "dd MM yyyy - hh:ii",
    	startDate: new Date,
    	showMeridian: true,
        autoclose: true,
        todayHighlight: true
    	// todayBtn: true
    });


    $("#start-date").datetimepicker().on('changeDate', function (e) {
		$('#end-date').datetimepicker('setStartDate', $(this).val());
    });
     $("#end-date").datetimepicker().on('changeDate', function (e) {
		$('#start-date').datetimepicker('setEndDate', $(this).val());
    });
}