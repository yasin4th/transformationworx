
var question_id =  getUrlParameter('question_id');
// function to fetch classes
function fetchClasses() {
	req = {};
	req.action = 'fetchClasses';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillClasses(res.classes);
		}
		else{
			// toastr.error('Class Not Loaded.','Error:');
		}
	});

}

// function to fill classes
function fillClasses(classes) {
	var html = "<option value='0'> Select Class </option>";
	$.each(classes,function( i,cls) {
		html += "<option val='"+(i+1)+"' value='"+cls.id+"' data-id="+cls.id+">" +cls.name+ "</option>";
	});
	$('#classes').html(html);
}

// function to fetch map keyword
function fetchMapKeyword() {
	req = {};
	req.action = 'fetchMapKeywords';
	$.ajax({
		'type'  :   'post',
		'url'   :   EndPoint,
		'data'  :   JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1 && res.mapKeywords){
			fillMapKeyword(res.mapKeywords);
			//mapKeywords = res.mapKeywords;
		}
	});

}

// function to fill map keyword
function fillMapKeyword(keywords){
	var html = "<option value='0'> Select Keyword </option>";
	$.each(keywords,function( i,keyw) {
		html += "<option value="+keyw.id+" data-id="+keyw.id+">" +keyw.map_keyword+ "</option>";
	});
	$('#map_keyword').html(html);
}

// function to fetch all questions details using keyword
function autoFillUsingKeyword(key_id){
	$("#units").val('').change();
	$("#chapters").val('').change();
	if(key_id == 0 )
	{
		toastr.error("Please Select Mapping Keyword.");
		return;
	}
	var req = {};
	req.action = 'getQuestionDetailsUsingKey';
	req.key_id = key_id;
	$.ajax({
		'type'  :   'post',
		'url'   :   EndPoint,
		'data'  :   JSON.stringify(req)

	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillQuestionDetailsUsingKey(res);
		}
		else if(res.status == 2)
		{
			window.location="404-error.php";
		}
		else{
			toastr.error(res.message);
		}
	});
}

function fillQuestionDetailsUsingKey(data){
	if(data.fill.classes != undefined)
		fillClasses(data.fill.classes);
	if((data.fill.subjects) != undefined)
		fillSubjects(data.fill.subjects);
	if((data.fill.units) != undefined)
		fillUnits(data.fill.units);
	if((data.fill.chapters) != undefined)
		fillChapters(data.fill.chapters);
	if((data.fill.topics) != undefined)
		fillTopics(data.fill.topics);
	if(data.fill.difficulty != undefined)
		fillDifficultyLevel(data.fill.difficulty);
	if(data.fill.skills != undefined)
		fillSkills(data.fill.skills);
	if(data.fill.tags != undefined)
		fillTags(data.fill.tags);

	setTaggedData(data.tagged);
	$("#classes").val(data.keywordData.class_id);
	$("#map_keyword_on_edit").val(data.keywordData.map_keyword);
	$("#description").val(data.keywordData.description);
}

function setTaggedData(data) {
	if((data.subject).length > 0)
		$("#subjects").val(data.subject);
	if((data.unit).length > 0)
		$("#units").val(data.unit);
	if((data.chapter).length > 0)
		$("#chapters").val(data.chapter);
	if((data.topic).length > 0)
		$("#topics").val(data.topic);
	if((data.difficulty).length > 0)
		$('#difficulty-levels').val(data.difficulty);
	if((data.tag).length > 0)
		$('#tags').val(data.tag);
	if((data.skill).length > 0)
		$('#skills').val(data.skill);

	$("#subjects").select2();
	$("#units").select2();
	$("#chapters").select2();
	$("#topics").select2();
	$('#difficulty-levels').select2();
	$('#tags').select2();
	$('#skills').select2();
}

// function to fetch subjects
function fetchSubjects() {
	req = {};
	req.action = 'fetchSubjects';
	req.class_id = $("#classes").find('option:selected').data('id');
	if(req.class_id) {
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fillSubjects(res.subjects);
			}
			else{
				$('#subjects').html('');
				// toastr.error('No Subject Found In Selected Class.');
			}
		});
	}
}

// function to fill subjects
function fillSubjects(subjects) {
	var html = "";
	$.each(subjects,function( i,sub) {
		html += '<option val="'+(i+1)+'" value="'+sub.id+'" data-id="'+sub.id+'">'+sub.name+'</option>';
	});
	$('#subjects').html(html);
}

// function get the subject ids selected by admin
function getSelectedSubjectId() {
	sub = $("#subjects").find('option:selected');
	id = [];
	$.each(sub, function(i,sub_id){
		id.push($(sub_id).data('id'));
	})
	return id;
}

// function to fetch units
function fetchUnits() {
	req = {};
	req.action = 'fetchUnits';
	req.subject_id = getSelectedSubjectId();
	if(req.subject_id != '') {
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fillUnits(res.units);
			}
			else{
				// toastr.error('Error Units not load');
			}
		});
	}
}


// function to fill units
function fillUnits(units) {
	var subject_id = getSelectedSubjectId();
	var html = "";
	$.each(units,function( i,unit) {
			html += "<option val='"+(i+1)+"' value='"+unit.id+"' data-id="+unit.id+">" +unit.name+ "</option>";
	});
	$('#units').html(html);
}

// function get the unit ids selected by admin
function getSelectedUnitId() {
	units = $("#units").find('option:selected')
	id = [];
	$.each(units, function(i,unit_id){
		id.push($(unit_id).data('id'));
	})
	return id;
}

// function to fetch chapters
function fetchChapters() {
	req = {};
	req.action = 'fetchChapters';
	req.unit_id = getSelectedUnitId();
	if(req.unit_id != '') {
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fillChapters(res.chapters);
			}
			else{
				// toastr.error('Error Chapters not load');
			}
		});
	}
}

// function to fill chapters
function fillChapters(chapters) {
	var html = "";
	$.each(chapters,function( i,chap) {
		html += "<option val='"+i+"' value='"+chap.id+"' data-id="+chap.id+">" +chap.name+ "</option>";
	});
	$('#chapters').html(html);
}


// function get the chapter ids selected by admin
function getSelectedChapterId() {
	chapter = $("#chapters").find('option:selected')
	id = [];
	$.each(chapter, function(i,chapter_id){
		id.push($(chapter_id).data('id'));
	})
	return id;
}

// function to fetch topics
function fetchTopics() {
	req = {};
	req.action = 'fetchTopics';
	req.chapter_id = getSelectedChapterId();
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillTopics(res.topics);
		}
		else{
			// toastr.error('Error Topics not load');
		}
	});
}

// function to fill Topics
function fillTopics(topics){
	var html = "";
	$.each(topics,function( i,topic) {
		html += "<option val='"+i+"' value='"+topic.id+"' data-id="+topic.id+">" +topic.name+ "</option>";
	});
	$('#topics').html(html);
}

// function get the topic ids selected by admin
function getSelectedTopicId() {
	topics = $("#topics").find('option:selected');
	id = [];
	$.each(topics, function(i,topic_id){
		id.push($(topic_id).data('id'));
	})
	topic_ids = id;
	return topic_ids;
}

// function to fetch tags
function fetchTags() {
	req = {};
	req.action = 'fetchTags';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillTags(res.tags);
		}
		else{
			// toastr.error('Error Tags not load');
		}
	});
}

// function to fill Tags
function fillTags(tags){
	var html = "";
	$.each(tags,function( i,tag) {
				html += "<option val='"+i+"' value='"+tag.id+"' data-id="+tag.id+">" +tag.tag+ "</option>";
	});
	$('#tags').html(html);
}


// function get the tag's ids selected by admin
function getSelectedTagId() {
	tags = $("#tags").find('option:selected');
	id = [];
	$.each(tags, function(i,tag_id){
		id.push($(tag_id).data('id'));
	})
	tag_ids = id;
	return tag_ids;
}



// function to fetch difficulty-level
function fetchDifficultyLevel() {
	req = {};
	req.action = 'fetchDifficultyLevel';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillDifficultyLevel(res.difficulty);
		}
		else{
			// toastr.error('Error Difficulty Level not load');
		}
	});
}

// function to fill Difficulty Level
function fillDifficultyLevel(difficultyLevels){
	var html = "";
	$.each(difficultyLevels,function( i,difficultyLevel) {
				html += "<option val='"+i+"' value='"+difficultyLevel.id+"' data-id="+difficultyLevel.id+">" +difficultyLevel.difficulty+ "</option>";
	});
	$('#difficulty-levels').html(html);
}


// function get the Difficulty's ids selected by admin
function getSelectedDifficultyLevelId() {
	difficulty = $("#difficulty-levels").find('option:selected');
	id = [];
	$.each(difficulty, function(i,difficulty_id){
		id.push($(difficulty_id).data('id'));
	})
	difficulty_ids = id;
	return difficulty_ids;
}


// function to fetch skills
function fetchSkills() {
	req = {};
	req.action = 'fetchSkills';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillSkills(res.skills);
		}
		else{
			toastr.error('Error Skills not load');
		}
	});
}

// function to fill Skills
function fillSkills(skills){
	var html = "";
	$.each(skills,function( i,skill) {
		html += "<option val='"+i+"' value='"+skill.id+"' data-id="+skill.id+">" +skill.skill+ "</option>";
	});
	$('#skills').html(html);
}


// function get the Skill's ids selected by admin
function getSelectedskillId() {
	skills = $("#skills").find('option:selected');
	id = [];
	$.each(skills, function(i,skill_id){
		id.push($(skill_id).data('id'));
	})
	skill_ids = id;
	return skill_ids;
}


function fetchQuestionTypes() {
	req = {};
	req.action = 'fetchQuestionTypes';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillQuestionTypes(res.data);
		}
		else{
			// toastr.error('Error:Question Types not loaded. '+res.message);
		}
	});
}
// function to fill questions types
function fillQuestionTypes(question_types){
	var html = "<option value='0' data-id='0'> Select Question Type</option>";
	$.each(question_types, function(x,type) {
		html += '<option val="'+(x+1)+'" value="'+type.id+'" data-id="'+type.id+'"> '+type.question_type+' </option>';
	});
	$('#question-types').html(html);
	/*if(pid != undefined){
		$('#question-types').val(11).change();
		getQuestionDetails();
	}*/
}
/*function getQuestionDetails() {
	var req = {};
	req.action = 'getQuestionDetails';
	req.question_id = pid;
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
			// console.log(res);
		if(res.status == 1) {
			//console.log(res);
			//fillQuestionDetails(res.fill,res.tagged);
			$('#passage-box').html(res.questionData.question);

			$("#classes").val(res.questionData.class_id);
			$("#classes").change();

			if((res.tagged.subject).length > 0){
				$("#subjects").val(res.tagged.subject);
				$("#subjects").change();
			}
			if((res.tagged.unit).length > 0){
				$("#units").val(res.tagged.unit);
				$("#units").change();
			}


		}
		else{
			toastr.error('Error');
		}
	});
}
*/
