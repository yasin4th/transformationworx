$(function() {})


// function set editor in integer div
function integerLoadEditor() {
	integerQuestionEditor = CKEDITOR.inline('integer-question-box');
}

function integerGetOptions() {
	var value = []

	var choises = {};
		choises.option = $('#from').val();
		choises.number = 1;
		choises.answer = 1;
	value.push(choises);
	
	var choises = {};
		choises.option = $('#to').val();
		choises.number = 2;
		choises.answer = 1;
	value.push(choises);
	return value;
}

// this function save integer type question
function saveInteger() {
	if(validateInteger()){
		var req = {};
		req.action = "addQuestion";
		req.questionData = integerQuestionEditor.getData();
		req.DescriptionData = "No Description.";
		if(hintEditor != undefined)
			req.DescriptionData = hintEditor.getData();
		req.parent_id = parent_id;
		req.questionType = 13;
		req.options = integerGetOptions();
		req.class_id = $("#classes").find('option:selected').data('id');
		req.subject_ids = getSelectedSubjectId();
		req.unit_ids = getSelectedUnitId();
		req.chapter_ids = getSelectedChapterId();
		req.topic_ids = getSelectedTopicId();
		req.tag_ids = getSelectedTagId();
		req.difficultyLevel_ids = getSelectedDifficultyLevelId();
		req.skill_ids = getSelectedskillId();
		
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				resetPageValues();
				toastr.success(res.message,'Question Id: 00'+res.questionId);
			}
			else {
				toastr.error(res.message,'Error: Question Not Saved');
			}
		});
		
	}
}

function validateInteger(){
	if(integerQuestionEditor.getData().length <= 8 || integerQuestionEditor.getData() == ""){		
		toastr.error("Please enter Integer Question.");
		return false;
	}
	
	option = integerGetOptions();
	var msg = true;

	$.each(option, function(i,opt){
		if(opt.option.length == 0){
			msg =false;
		}
	})

	if(msg == false){
		toastr.error("Please enter Options");
	}
	return msg;	
}