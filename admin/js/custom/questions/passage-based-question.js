
function loadPassageBasedQuestion() {
	passage = CKEDITOR.inline('passage-box');
	setEventAddQuestion();
	setEventAppendQuestion();
}

function setEventAppendQuestion() {
	$('.passage-question-types').off('change');
	$('.passage-question-types').on('change', function() {
		question_type = $(this).val();
		
		html = getTheHtmlOfSelectedQuestionType(parseInt(question_type));
		$(this).parents('.passage-question').find('.selected-passage-question').html(html);
		loadEventOfSelectedQuestionType(parseInt(question_type));
		showDescriptionBox(question_type);
	});
}


function setEventAddQuestion() {
	$('.btn_AddQuestion').off('click');
	$('.btn_AddQuestion').on('click', function(e) {
		e.preventDefault();
		type = $('.passage-question-types option:selected').data('id');
		switch(type) {
			case 1:
				req = getSCQ_Data();
				$.ajax({
					'type'	:	'post',
					'url'	:	EndPoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1) {
						toastr.success(res.message,'Question Id: 00'+res.questionId);
						addAnotherPassageQuestion();
					}
					else {
						toastr.error(res.message,'Error: Question Not Saved');
					}
				});
				break;

			case 2:
				req = getMAQ_Data();
				$.ajax({
					'type'	:	'post',
					'url'	:	EndPoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1) {
						toastr.success(res.message,'Question Id: 00'+res.questionId);
						addAnotherPassageQuestion();
					}
					else {
						toastr.error(res.message,'Error: Question Not Saved');
					}
				});
				break;
			
			case 3:
				req = getTF_Data();
				$.ajax({
					'type'	:	'post',
					'url'	:	EndPoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1) {
						toastr.success(res.message,'Question Id: 00'+res.questionId);
						addAnotherPassageQuestion();
					}
					else {
						toastr.error(res.message,'Error: Question Not Saved')
					}
				});
				break;

			case 6:
				req = getFB_data();
				$.ajax({
					'type'	:	'post',
					'url'	:	EndPoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1) {
						toastr.success(res.message,'Question Id: 00'+res.questionId);
						addAnotherPassageQuestion();
					}
					else {
						toastr.error(res.message,'Error: Question Not Saved');
					}
				});
				break;

			case 7:
				req = getFbDD_Data();
				$.ajax({
					'type'	:	'post',
					'url'	:	EndPoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1) {
						toastr.success(res.message,'Question Id: 00'+res.questionId);
						addAnotherPassageQuestion();
					}
					else {
						toastr.error(res.message,'Error: Question Not Saved');
					}
				});
				break;

			case 8:
				req = getFBDnd_data();
				$.ajax({
					'type'	:	'post',
					'url'	:	EndPoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1) {
						toastr.success(res.message,'Question Id: 00'+res.questionId);
						addAnotherPassageQuestion();
					}
					else {
						toastr.error(res.message,'Error: Question Not Saved');
					}
				});
				break;

			
		}
	});
}

function addAnotherPassageQuestion() {
	html = '<div class="passage-question col-md-12 col-xs-12"> <div class="col-md-8 form-group"> <label class="control-label"><span class="required">* </span>Select Question Type</label> <select class="passage-question-types form-control"> <option value="0" data-id="0"> Select Question Type</option> <option value="1" data-id="1"> Single Choice Questions </option> <option value="2" data-id="2"> Multiple Choice Questions </option> <option value="3" data-id="3"> True/False </option> <option value="6" data-id="6"> Fill In The Blanks </option>  <option value="7" data-id="7"> Fill In The Blanks With Drop-Down Options </option> <option value="8" data-id="8"> Fill In The Blanks With Drag and Drop Options </option>  </select> </div> <div class="col-md-3"></div> <div class="col-md-12 col-xs-12"> <div class="selected-passage-question col-md-12 col-xs-12" style="border:1px solid rgb(232, 227, 245);"> <!-- Question Type Area --> </div> </div> </div>';
	$('#passbase-questions-div').html(html);
	setEventAppendQuestion();
}

// this function save maq type question
function savePassageBasedQuestion() {
	req = getPassageBasedQuestion_Data();

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			parent_id = res.questionId;
			toastr.success(res.message,'Question Id: 00'+res.questionId);
			changeFormSubmitAction();
			$('#hint-box').html('');

			$('#passage-box').attr('contenteditable',false)
			$('#passage-child-question').removeClass('hidden');
		}
		else {
			toastr.error(res.message,'Error: Question Not Saved');
		}
	});
}

function getPassageBasedQuestion_Data() {
	
	var req = {};
	// req.action = "addPassageBasedQuestion";
	req.action = "addQuestion";

	req.questionData = passage.getData();
	req.parent_id = parent_id;
	req.DescriptionData = "";
	req.questionType = 11;
	req.options = [];
	req.class_id = $("#classes").find('option:selected').data('id');
	req.subject_ids = getSelectedSubjectId();
	req.unit_ids = getSelectedUnitId();
	req.chapter_ids = getSelectedChapterId();
	req.topic_ids = getSelectedTopicId();
	req.tag_ids = getSelectedTagId();
	req.difficultyLevel_ids = getSelectedDifficultyLevelId();
	req.skill_ids = getSelectedskillId();
	return req;
}

function addPassageQuestion() {
	question = $('#passivebase .passage-question');
	$(question).find('.passage-question-type').val();
	
	$(question).find('.selected-passage-question') 
	
}

function changeFormSubmitAction() {
	$('#form-add-question').off('submit');
	$('#form-add-question').on('submit', function(e) {
		e.preventDefault();
		type = $('.passage-question-types option:selected').data('id');
		switch(type) {
			case 1:
				req = getSCQ_Data();
				$.ajax({
					'type'	:	'post',
					'url'	:	EndPoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1) {
						toastr.success(res.message,'Question Id: 00'+res.questionId);
						addAnotherPassageQuestion();
					}
					else {
						toastr.error(res.message,'Error: Question Not Saved');
					}
				});
				break;

			case 2:
				req = getMAQ_Data();
				$.ajax({
					'type'	:	'post',
					'url'	:	EndPoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1) {
						toastr.success(res.message,'Question Id: 00'+res.questionId);
						addAnotherPassageQuestion();
					}
					else {
						toastr.error(res.message,'Error: Question Not Saved');
					}
				});
				break;
			
			case 3:
				req = getTF_Data();
				$.ajax({
					'type'	:	'post',
					'url'	:	EndPoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1) {
						toastr.success(res.message,'Question Id: 00'+res.questionId);
						addAnotherPassageQuestion();
					}
					else {
						toastr.error(res.message,'Error: Question Not Saved')
					}
				});
				break;

			case 6:
				req = getFB_data();
				$.ajax({
					'type'	:	'post',
					'url'	:	EndPoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1) {
						toastr.success(res.message,'Question Id: 00'+res.questionId);
						addAnotherPassageQuestion();
					}
					else {
						toastr.error(res.message,'Error: Question Not Saved');
					}
				});
				break;

			case 7:
				req = getFbDD_Data();
				$.ajax({
					'type'	:	'post',
					'url'	:	EndPoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1) {
						toastr.success(res.message,'Question Id: 00'+res.questionId);
						addAnotherPassageQuestion();
					}
					else {
						toastr.error(res.message,'Error: Question Not Saved');
					}
				});
				break;

			case 8:
				req = getFBDnd_data();
				$.ajax({
					'type'	:	'post',
					'url'	:	EndPoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1) {
						toastr.success(res.message,'Question Id: 00'+res.questionId);
						addAnotherPassageQuestion();
					}
					else {
						toastr.error(res.message,'Error: Question Not Saved');
					}
				});
				break;
		}

	});
}