$(function() {
	fetchClasses();
	fetchDifficultyLevel();
	fetchTags();
	fetchSkills();
	fetchQuestionTypes();
	setEventOnFormSubmit();
	fetchMapKeyword();

	$("#subjects").select2({
		placeholder: "Subjects ( multiple subjects )"
	});
	$("#units").select2({
		placeholder: "Units ( multiple units )"
	});
	$("#chapters").select2({
		placeholder: "Chapters ( multiple Chapter )"
	});
	$("#topics").select2({
		placeholder: "Topics ( multiple Topic )"
	});
	$("#tags").select2({
		placeholder: "Tags ( multiple Tags )"
	});
	$("#difficulty-levels").select2({
		placeholder: "Difficulty-Levels ( multiple Difficulty Levels )"
	});
	$("#skills").select2({
		placeholder: "Skills ( multiple Skills )"
	});

	CKEDITOR.config.customConfig = 'http://exampointer.com/admin/assets/global/plugins/ckeditor/config-new.js';

	CKEDITOR.disableAutoInline = true;
});
