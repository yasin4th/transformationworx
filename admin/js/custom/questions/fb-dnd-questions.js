$(function() {})

function setEventAddBlank() {
	// function to add option
	var i = 0;
	$('#fb-addBlank').off('click')
	$('#fb-addBlank').on('click', function(e) {
		e.preventDefault();
		i++;
		html = '<div class="row"> <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label class="index"></label> </div> </div> </div> <div class="col-md-10 form-group"> <input class=" form-control option" data-id="input'+(i+1)+'" placeholder="Option"/> </div>';
		html += '<div class="col-md-1"><a class="fb-deleteOption btn btn-circle btn-icon-only blue" data-id="input'+(i+1)+'"><i class="fa fa-trash-o"></i></a></div>';
		html += '</div></div>'
		
		divText = fbQuestionEditor.getData();
		$('#fb-question-box').html(divText.slice(0,-5)+'<input  class="input-droppable" disabled="true" data-id="input'+(i+1)+'" size="8"> </p>');
		$('#fb-options-div').append(html);
		//divText= "<input class=\"input-droppable\" disabled=\"true\" data-id=\"input"+(i+1)+"\" size=\"8\">";
		//console.log(divText);
		//fbQuestionEditor.insertHtml(divText.slice(0,-5)+divText='</p>','html');
		// var selection = e.editor.getSelection();
		// var range = selection.getRanges()[0];
		// var cursor_position = range.startOffset;
		setEventDeleteOptions();
	});
}

// function to delete option
function setEventDeleteOptions() {
	$('.fb-deleteOption').off('click');
	$('.fb-deleteOption').on('click', function() {
		
		var remove = $(this).parents(".fb-options-div").find(".option").length;		
		var id=$(this).attr('data-id');
		console.log(id);
		$(this).parents().eq(1).remove();
		$('#fb-question-box input[data-id='+id+']').remove();
		/*opt = $(this).parents(".fb-options-div").find(".option");
		$.each(opt,function(x,o) {
			remove = x+1;
			$(o).attr('placeholder','Option '+remove);
		});*/
		
	});
	$('.scq-deleteOption').off('click');
	$('.scq-deleteOption').on('click', function() {
		//alert("clicked on delete");
		var remove = $(this).parents(".scq-options-div").find(".option").length;
		if(remove>2) {
			$(this).parents().eq(1).remove();
			opt = $(this).parents(".scq-options-div").find(".option");
			$.each(opt,function(x,o) {
				remove = x+1;
				$(o).attr('placeholder','Option '+remove);
			});
		}
	});

}
	
function setEventAddOptionFbDnd() {
	// function to add option
	var i = 0;
	$('#add-option-fb-dnd').off('click')
	$('#add-option-fb-dnd').on('click', function(e) {
		e.preventDefault();
		i++;
		html = '<div class="row"> <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label class="index"></label> </div> </div> </div> <div class="col-md-10 form-group"> <input class=" form-control option" data-id="input'+(i+1)+'" placeholder="Option" style="background-color: aliceblue;"/> </div> </div></div>'
		divText = fbQuestionEditor.getData();
		// $('#fb-question-box').html(divText.slice(0,-5)+'<input disabled="true" data-id="input'+(i+1)+'" size="8" maxlength="0"></p>');
		$('#fb-dd-extra-options-div').append(html);
	});
}
	

// function to delete option
/*function setEventDeleteFbOptions() {
	$('#fb-question-box').keyup(function(e) {
	    e.preventDefault();
	    blank = $('#fb-question-box input').length;
	    option = $("#fb-options-div .option").length;
	    if (blank != option) {
	        options = $("#fb-options-div .option")
	        $.each(options, function(i, opt) {
	            a = $(opt).data('id');
			        if ($('[data-id="' + a + '"]').length != 2)
			            $(opt).parents('.row').eq(0).remove();
			})
		}
	});
}*/

// this function fetch fb option from page
function getOptionsFbDnd() {
	options = $("#fb-options-div .option");
	value = [];
	$.each(options, function(i,option) {
		choises = {};
		//choises.option = $(option).html();
		choises.option = $(option).val();
		choises.answer = 1;
		value.push(choises);
	});
	
	options = $("#fb-dd-extra-options-div .option");
	$.each(options, function(i,option) {
		choises = {};
		choises.option = $(option).val();
		choises.answer = 0;
		value.push(choises);
	});
	return value;
}

// function set editor in fb div
function fbDndLoadEditor() {
	setEventAddBlank();
	setEventDeleteFbOptions(); // function set events to delete on options
	
	//fbQuestionEditor.destroy();
	fbQuestionEditor = CKEDITOR.inline('fb-question-box');
	setEventAddOptionFbDnd();
}

// this function save fb type question
function saveFbDnd(data) {
	if(validateFbDnD()){
		req = getFBDnd_data();
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				resetPageValues();
				toastr.success(res.message,'Question Id: 00'+res.questionId);
			}
			else {
				toastr.error(res.message,'Error: Question Not Saved');
			}
		});
	}
}

function getFBDnd_data() {
	var req = {};
	req.action = "addQuestion";
	req.questionData = fbQuestionEditor.getData();
	req.DescriptionData = "No Description.";
	if(hintEditor != undefined)
		req.DescriptionData = hintEditor.getData();
	req.parent_id = parent_id;
	req.questionType = 8;
	req.options = getOptionsFbDnd();
	req.class_id = $("#classes").find('option:selected').data('id');
	req.subject_ids = getSelectedSubjectId();
	req.unit_ids = getSelectedUnitId();
	req.chapter_ids = getSelectedChapterId();
	req.topic_ids = getSelectedTopicId();
	req.tag_ids = getSelectedTagId();
	req.difficultyLevel_ids = getSelectedDifficultyLevelId();
	req.skill_ids = getSelectedskillId();
	return req;
}


function validateFbDnD(){
	if(fbQuestionEditor.getData().length <= 11){		
		toastr.error("Please enter Fb Drag-n-Drop Question");
		return false;
	}
	
	option = getOptionsFbDnd();
	var msg = true;

	$.each(option, function(i,opt){
		if(opt.option.length == 0){
			msg =false;
		}
	})

	if(msg == false){
		toastr.error("Please enter Options");
	}
	return msg;	
}
