$(function() {})

// function to add option
function setEventAddOptionsPair() {
	$('#match-add-pairs').off('click');
	$('#match-add-pairs').on('click', function() {
		genratedId = $("#match-options-div .option").length;
		if(genratedId < 50) {
			genratedId += 1;
			html = '<div class="options row"> <div class="col-md-12"> <div class="col-md-11 col-xs-10"> <div class="row"> <div class="col-md-6 col-xs-6 form-group"> <div contentEditable="true" class="option" id="match-option'+genratedId+'" data-column="1" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-6 col-xs-6 form-group"> <div contentEditable="true" class="option" id="match-option'+(genratedId+1)+'" data-column="2" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> </div> </div> <div class="col-md-1 col-xs-2"> <a class="match-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div></div>';
			$('#match-more-options-div').append(html);
			setEventDeleteMatchingOptions();
			CKEDITOR.inline( 'match-option'+genratedId);
			CKEDITOR.inline( 'match-option'+(genratedId+1));
		}
	});
}

function setEventAddMoreOptions() {
	$('#match-add-option').off('click');
	$('#match-add-option').on('click', function() {
		genratedId = $("#match-options-div .option").length;
		if(genratedId < 50) {
			genratedId += 1;
			html = '<div class="options row"> <div class="col-md-12"> <div class="col-md-11 col-xs-10"> <div class="row"> <div class="col-md-6 col-xs-6 form-group"></div> <div class="col-md-6 col-xs-6 form-group"> <div contentEditable="true" class="option" id="match-option'+(genratedId)+'" data-column="3" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> </div> </div> <div class="col-md-1 col-xs-2"> <a class="match-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div></div>';
			$('#match-options-div').append(html);
			setEventDeleteMatchingOptions();
			CKEDITOR.inline( 'match-option'+genratedId);
		}
	});
}

// function to delete option
function setEventDeleteMatchingOptions() {
	$('.match-deleteOption').off('click');
	$('.match-deleteOption').on('click', function() {
		remove = $("#match-options-div .option").length;
		if(remove>2) {
			$(this).parents().eq(1).remove();
			opt = $("#match-options-div .option");
			$.each(opt,function(x,o) {
				remove = x+1;
				$(o).attr('placeholder','Option '+remove);
			});
		}
	});
}

// this function fetch match option from page
function getMatchingOptions() {
	firstColumn = $('[data-column="1"]');
	secondColumn = $('[data-column="2"]');
	extraOptions = $('[data-column="3"]');
	value = [];
	$.each(firstColumn, function(i,option){
		choises = {};
		choises.option = CKEDITOR.instances[$(option).attr('id')].getData();
		choises.answer = 0;
		choises.number = 1;
		value.push(choises);
	});
	$.each(secondColumn, function(i,option){
		choises = {};
		choises.option = CKEDITOR.instances[$(option).attr('id')].getData();
		choises.answer = 1;
		choises.number = 2;
		value.push(choises);
	});
	$.each(extraOptions, function(i,option){
		choises = {};
		choises.option = CKEDITOR.instances[$(option).attr('id')].getData();
		choises.answer = 0;
		choises.number = 2;
		value.push(choises);
	});
	return value;
}

// function set editor in match div
function matchLoadEditor() {
	setEventDeleteMatchingOptions(); // function set events to delete on options
	setEventAddOptionsPair(); // function to set event add pair option
	setEventAddMoreOptions(); // function to set event single option

	questionEditor = CKEDITOR.inline('match-question-box');

	CKEDITOR.inline('match-option1');
	CKEDITOR.inline('match-option2');
	CKEDITOR.inline('match-option3');
	CKEDITOR.inline('match-option4');
	CKEDITOR.inline('match-option5');
	CKEDITOR.inline('match-option6');
	CKEDITOR.inline('match-option7');
	CKEDITOR.inline('match-option8');
}

// this function save match type question
function saveMatch(data) {
	if(validateMatch()){
		req = {};
		req.action = "addQuestion";
		req.questionData = questionEditor.getData();
		req.DescriptionData = "No Description.";
		if(hintEditor != undefined)
			req.DescriptionData = hintEditor.getData();
		req.parent_id = parent_id;
		req.questionType = 5;
		req.options = getMatchingOptions();
		req.class_id = $("#classes").find('option:selected').data('id');
		req.subject_ids = getSelectedSubjectId();
		req.unit_ids = getSelectedUnitId();
		req.chapter_ids = getSelectedChapterId();
		req.topic_ids = getSelectedTopicId();
		req.tag_ids = getSelectedTagId();
		req.difficultyLevel_ids = getSelectedDifficultyLevelId();
		req.skill_ids = getSelectedskillId();
		
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				resetPageValues();
				toastr.success(res.message,'Question Id: 00'+res.questionId);
			}
			else {
				toastr.error(res.message,'Error: Question Not Saved');
			}
		});
	}
}

function validateMatch(){
	if(questionEditor.getData().length <= 8){
		toastr.error("Please enter Multiple Choice Question.");
		return false;
	}
	
	option = getMatchingOptions();
	var msg = true;

	$.each(option, function(i,opt){
		if(opt.option.length <= 8){
			msg =false;
		}
	})

	if(msg == false){
		toastr.error("Please enter Options");
	}
	return msg;	
}

