var class_id;
var subject_ids;
var unit_ids;
var chapter_ids;
var topic_ids;
var tag_ids;
var difficultyLevel_ids;
var skill_ids;
var hintEditor;
var parent_id = 0;
var questionEditor;

function setEventOnFormSubmit() {
	// event fired when admin adds a question
	$('#form-add-question').off('submit');
	$('#form-add-question').on('submit', function(e) {
		e.preventDefault();
		if($("#classes").val() != 0 && $("#subjects").val() != null)
		{
			id = $('#question-types option:selected').data('id');
			switch(id) {
				case 1:
					saveSCQ();
					break;

				case 2:
					saveMAQ();
					break;

				case 3:
					saveTF();
					break;

				case 4:
					saveOrd();
					break;

				case 5:
					saveMatch();
					break;

				case 6:
					saveFb();
					break;

				case 7:
					saveFbDD(data);
					break;

				case 8:
					saveFbDnd();
					break;

				case 9:
					saveAr();
					break;

				case 10:
					saveDescriptive();
					break;

				case 11:
					savePassageBasedQuestion();
					break;

				case 12:
					saveMatrix();
					break;

				case 13:
					saveInteger();
					break;

				default:
					toastr.error('Please Select A question type.');
					break;
			}
		}
		else {
			toastr.error('Please Select Class and atleast one Subjects.');
		}
	});
}


// Event Fired When user changes Question Type
$('#question-types').on('change', function(e) {
	question_type = $('#question-types option:selected').data('id');
	// changeFirstLetterToUpper();
	html = getTheHtmlOfSelectedQuestionType(question_type);

	$("#question-container").html(html);
	loadEventOfSelectedQuestionType(question_type);
	parent_id = 0;
	showDescriptionBox(question_type);
	setEventDeleteOptions();
});

function loadEventOfSelectedQuestionType(type) {
	switch(type) {
		case 1:
			scqLoadEditor();
			break;

		case 2:
			maqLoadEditor();
			break;

		case 3:
			trueFalseLoadEditor();
			break;

		case 4:
			orderingLoadEditor();
			break;

		case 5:
			matchLoadEditor();
			break;

		case 6:
			fbLoadEditor();
			break;

		case 7:
			fbdDDLoadEditor();
			break;

		case 8:
			fbDndLoadEditor();
			break;

		case 9:
			// arLoadEditor();
			break;

		case 10:
			descriptiveLoadEditor();
			break;

		case 11:
			loadPassageBasedQuestion();
			break;

		case 12:
			matrixLoadEvents();
			break;

		case 13:
			integerLoadEditor();
			break;

		default:

			break;

	}
}


function getTheHtmlOfSelectedQuestionType(type) {
	switch(type) {
		// case to add SCQ
		case 1:
			html = '<div class="singleChoise"> <div class="form-group"> <label> Enter Question: </label> <div id="scq-question-box" name="scq-question-box" placeholder="Enter Question" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </div> <div class="row margin-top-10"> <div class="scq-options-div col-md-11"> <div class="row"> <div class="col-md-12 col-xs-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input type="radio" name="radioOption" data-answer="1"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option1" type="text" data-id="1" class="form-control option" placeholder="Option 1">--> <div id="scq-option1" contentEditable="true" data-id="1" class="option" placeholder="Option 1" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1 col-xs-2"> <a class="scq-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input type="radio" name="radioOption" data-answer="2"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <div id="scq-option2" contentEditable="true" class="option" data-id="2" placeholder="Option 2" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1 col-xs-2"> <a class="scq-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="ich/eck-list"> <label> <input type="radio" name="radioOption" data-answer="3"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option3" type="text" class="form-control option" data-id="3" placeholder="Option 3"> --> <div id="scq-option3" contentEditable="true" class="option" data-id="3" placeholder="Option 3" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1 col-xs-2"> <a class="scq-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input type="radio" name="radioOption" data-answer="3"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option4" type="text" class="form-control option" data-id="4" placeholder="Option 4"> --> <div id="scq-option4" contentEditable="true" class="option" data-id="4" placeholder="Option 4" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1 col-xs-2"> <a class="scq-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> </div> <div class="col-md-1"></div> <div class="col-md-12"> <a class="scq-addOption btn btn-circle btn-icon-only blue pull-right add-option"> <i class="fa fa-plus"></i> </a> </div> </div></div><div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;

		// case to generate html for MCQ Question Type
		case 2:
			html = '<div class="multiChoise"> <div class="form-group"> <label> Enter Question: </label> <div id="maq-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </div> <div class="row margin-top-10"> <div class="maq-options-div col-md-11"> <div class="row"> <div class="col-md-12 col-xs-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="checkbox-list"> <label> <input type="checkbox" data-answer="1"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option1" type="text" data-id="1" class="form-control option" placeholder="Option 1">--> <div id="maq-option1" contentEditable="true" data-id="1" class="option" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;" placeholder="Option 1"></div> </div> <div class="col-md-1 col-xs-2"> <a class="maq-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="checkbox-list"> <label> <input type="checkbox" data-answer="2"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option2" type="text" class="form-control option" data-id="2" placeholder="Option 2"> --> <div id="maq-option2" contentEditable="true" class="option" data-id="2" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;" placeholder="Option 2"></div> </div> <div class="col-md-1 col-xs-2"> <a class="maq-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="checkbox-list"> <label> <input type="checkbox" data-answer="3"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option3" type="text" class="form-control option" data-id="3" placeholder="Option 3"> --> <div id="maq-option3" contentEditable="true" class="option" data-id="3" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;" placeholder="Option 3"></div> </div> <div class="col-md-1 col-xs-2"> <a class="maq-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="checkbox-list"> <label> <input type="checkbox" data-answer="3"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option4" type="text" class="form-control option" data-id="4" placeholder="Option 4"> --> <div id="maq-option4" contentEditable="true" class="option" data-id="4" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;" placeholder="Option 4"></div> </div> <div class="col-md-1 col-xs-2"> <a class="maq-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> </div> <div class="col-md-1"></div> <div class="col-md-12"> <a class="maq-addOption btn btn-circle btn-icon-only blue pull-right add-option"> <i class="fa fa-plus"></i> </a> </div> </div> </div><div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;

		// case to generate html for T/F Question Type
		case 3:
			html = '<div id="true-false"> <div class="form-group"> <label> Enter Question: </label> <div id="tf-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </div> <div class="row margin-top-10"> <div id="tf-options-div" class="col-md-11"> <div class="row"> <div class="col-md-12 col-xs-12"> <div class="col-md-2 col-xs-4 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input id="true" type="radio" name="radioOption" data-answer="1"> True </label> </div> </div> </div> <div class="col-md-9 col-xs-6 form-group"> <!--<div>True</div>--> </div> <div class="col-md-1 col-xs-2"> <!--<a class="tf-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a>--> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-2 col-xs-4 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input id="false" type="radio" name="radioOption" data-answer="2"> False </label> </div> </div> </div> <div class="col-md-9 col-xs-6 form-group"> <!--<div >False</div>--> </div> <div class="col-md-1 col-xs-2"> <!--<a class="tf-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a>--> </div> </div> </div> </div> <div class="col-md-1"></div> <div class="col-md-12"> <!--<a id="tf-addOption" class="btn btn-circle btn-icon-only blue pull-right add-option"> <i class="fa fa-plus"></i> </a>--> </div> </div> </div><div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;

		// case  to generate html for Sequescing Question Type
		case 4:
			html = '<div id="ordering" class=""> <div class="form-group"> <label> Enter Question: </label> <div id="ord-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </div> <div class="row margin-top-10"> <div id="ord-options-div" class="col-md-11"> <div class="row"> <div class="col-md-12 col-xs-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label class="index">1</label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option1" type="text" data-id="1" class="form-control option" placeholder="Option 1">--> <div id="ord-option1" contentEditable="true" data-id="1" class="option" placeholder="Option 1" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1 col-xs-2"> <a class="ord-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label class="index">2</label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <div id="ord-option2" contentEditable="true" class="option" data-id="2" placeholder="Option 2" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1 col-xs-2"> <a class="ord-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="ich/eck-list"> <label class="index">3</label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option3" type="text" class="form-control option" data-id="3" placeholder="Option 3"> --> <div id="ord-option3" contentEditable="true" class="option" data-id="3" placeholder="Option 3" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1 col-xs-2"> <a class="ord-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label class="index">4</label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option4" type="text" class="form-control option" data-id="4" placeholder="Option 4"> --> <div id="ord-option4" contentEditable="true" class="option" data-id="4" placeholder="Option 4" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1 col-xs-2"> <a class="ord-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> </div> <div class="col-md-1"></div> <div class="col-md-12"> <a id="ord-addOption" class="btn btn-circle btn-icon-only blue pull-right add-option"> <i class="fa fa-plus"></i> </a> </div> </div> </div><div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;

		// case  to generate html for Matching Question Type
		//<div class="col-md-12"> <a id="match-add-option" class="btn btn-circle btn-icon-only blue pull-right add-option" title="Add Option"> <i class="fa fa-plus"></i> </a> </div>
		case 5:
			html = '<div id="matching" class=""> <div class="form-group"> <label> Enter Question: </label> <div id="match-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </div> <div class="row margin-top-10"> <div id="match-options-div" class="col-md-10"> <div class="options row"> <div class="col-md-12 col-xs-12"> <div class="col-md-11 col-xs-10"> <div class="row"> <div class="col-md-6 col-xs-6 form-group"> <div id="match-option1" contentEditable="true" data-id="1" data-column="1" class="option" placeholder="Option 1" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-6 col-xs-6 form-group"> <div id="match-option2" contentEditable="true" data-id="1" data-column="2" class="option" placeholder="Option 1" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> </div> </div> <div class="col-md-1 col-xs-2"> <a class="match-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="options row"> <div class="col-md-12"> <div class="col-md-11 col-xs-10"> <div class="row"> <div class="col-md-6 col-xs-6 form-group"> <div id="match-option3" contentEditable="true" class="option" data-id="2" data-column="1" placeholder="Option 2" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-6 col-xs-6 form-group"> <div id="match-option4" contentEditable="true" class="option" data-id="2" data-column="2" placeholder="Option 2" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> </div> </div> <div class="col-md-1 col-xs-2"> <a class="match-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="options row"> <div class="col-md-12"> <div class="col-md-11 col-xs-10"> <div class="row"> <div class="col-md-6 col-xs-6 form-group"> <div id="match-option5" contentEditable="true" class="option" data-id="3" data-column="1" placeholder="Option 3" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-6 col-xs-6 form-group"> <div id="match-option6" contentEditable="true" class="option" data-id="3" data-column="2" placeholder="Option 3" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> </div> </div> <div class="col-md-1 col-xs-2"> <a class="match-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="options row"> <div class="col-md-12"> <div class="col-md-11 col-xs-10"> <div class="row"> <div class="col-md-6 col-xs-6 form-group"> <div id="match-option7" contentEditable="true" class="option" data-id="4" data-column="1" placeholder="Option 4" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-6 col-xs-6 form-group"> <div id="match-option8" contentEditable="true" class="option" data-id="4" data-column="2" placeholder="Option 4" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> </div> </div> <div class="col-md-1 col-xs-2"> <a class="match-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div id="match-more-options-div"></div> </div> <div class="col-md-1"></div> <div class="col-md-12"> <div class="row"> <div class="col-md-11"><a id="match-add-pairs" class="btn btn-circle btn-icon-only blue pull-right add-option" title="Add Pair Options"><i class="fa fa-plus"></i></a></div> <div class="col-md-1"></div> </div> </div>  </div></div><div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;

		// case  to generate html for FB Question Type
		case 6:
			html = '<div id="fill-blanks" class=""> <div class="form-group"> <label> Enter Question: </label> <div id="fb-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> <button id="fb-addBlank"> Add Blank</button> </div> <div class="row margin-top-10"> <div id="fb-options-div" class="col-md-11"></div> <div class="col-md-1"></div> <div class="col-md-12"></div> </div> </div><div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;

		// case to generate html for F/B Drag and drop Question Type
		case 7:
			html = '<div id="fill-blanks-dd" class=""> <div class="form-group"> <label> Enter Question: </label> <div id="fb-dd-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> <button id="fb-addDropDown"> Add Drop-Down </button> </div> <div class="row margin-top-10"><div class="col-md-12"><label> Answers: </label><div id="fb-dd-options-div" class="row"></div></div> </div> </div><div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;


		// case  to generate html for F/B Drag-n-Drop Question Type
		case 8:
			html = '<div id="fill-blanks-dnd" class=""> <div class="form-group"> <label> Enter Question: </label> <div id="fb-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> <button id="fb-addBlank"> Add Blank</button><button id="add-option-fb-dnd"> Add Option</button> </div> <div class="row margin-top-10"> <div id="fb-options-div" class="col-md-11"></div> <div id="fb-dd-extra-options-div" class="row"></div><div class="col-md-1"></div> <div class="col-md-12"></div> </div> </div><div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;

		// case  to generate html for Assertion-Reason Question Type
		case 9:
			// html = '<div id="assertion-reason" class="hidden"> <div class="form-group"> <label> Enter Question: </label><div class="row"><div class="col-md-2"><label class="pull-right labelAssRes"> Asseration (A): </label></div><div class="col-md-10"><div id="assertion-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div><div class="row mar-top10"><div class="col-md-2"><label class="pull-right labelAssRes"> Reason (R): </label></div><div class="col-md-10"><div id="reason-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div> </div> <div class="row margin-top-10"> <div id="ar-options-div" class="col-md-11"> <div class="row"> <div class="col-md-12 col-xs-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input type="radio" name="ass-res-radio-option" data-answer="1"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <div id="ar-option1" contentEditable="true" data-id="1" class="option" placeholder="Option 1" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1 col-xs-2"> <a class="ar-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input type="radio" name="ass-res-radio-option" data-answer="2"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <div id="ar-option2" contentEditable="true" class="option" data-id="2" placeholder="Option 2" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1 col-xs-2"> <a class="ar-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input type="radio" name="ass-res-radio-option" data-answer="3"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <div id="ar-option3" contentEditable="true" class="option" data-id="3" placeholder="Option 3" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1 col-xs-2"> <a class="ar-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input type="radio" name="ass-res-radio-option" data-answer="3"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <div id="ar-option4" contentEditable="true" class="option" data-id="4" placeholder="Option 4" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1 col-xs-2"> <a class="ar-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> </div> <div class="col-md-1"></div> <div class="col-md-12"> <a id="ar-addOption" class="btn btn-circle btn-icon-only blue pull-right add-option"> <i class="fa fa-plus"></i> </a> </div> </div> </div><div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;

		// case  to generate html for Descriptive Question Type
		case 10:
			html = '<div id="descriptive" class=""> <div class="form-group"> <label> Enter Question: </label> <div id="descriptive-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </div> </div><div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;

		// case  to generate html for Passive Base Question Type
		case 11:
			html = '<div id="passivebase" class=""> <div class="form-group"> <label> Enter Passage: </label> <div id="passage-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </div> <div id="passage-child-question" class="hidden"> <div class="row margin-top-10"> <div class="col-md-1"></div> <div class="col-md-11"> <div id="passbase-questions-div" class="row"> <div class="passage-question col-md-12 col-xs-12"> <div class="col-md-8 form-group"> <label class="control-label"><span class="required">* </span>Select Question Type</label> <select class="passage-question-types form-control"> <option value="0" data-id="0"> Select Question Type</option> <option value="1" data-id="1"> Single Choice Questions </option> <option value="2" data-id="2"> Multiple Choice Questions </option> <option value="3" data-id="3"> True/False </option> <option value="6" data-id="6"> Fill In The Blanks </option>  <option value="7" data-id="7"> Fill In The Blanks With Drop-Down Options </option> <option value="7" data-id="7"> Fill In The Blanks With Drag and Drop Options </option>  </select> </div> <div class="col-md-3"></div> <div class="col-md-12 col-xs-12"> <div class="selected-passage-question col-md-12 col-xs-12" style="border:1px solid rgb(232, 227, 245);"> <!-- Question Type Area --> </div> </div> </div> </div> </div> </div> <div class="col-lg-12"><a class="btn_AddQuestion btn green pull-right" style="margin-top: 15px; /* color:#2C2CEB */"><i class="fa fa-plus"></i> Save And Add Question </a></div> </div> </div>';
			break;

		// case  to generate html for Matrix Matching Question Type
		case 12:
			html = '<div id="matrix" class=""><div class="form-group"> <label> Enter Question: </label> <div id="matrix-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </div> <div class="row margin-top-10"> <div class="col-md-1"></div> <div id="matrix-options-div" class="col-md-11 matrix-table-options"> <div class="margin-bottom-10 table-scrollable"><table id="matrix-table" class="table table-bordered table-striped table-hover"><thead><tr><th></th><th> <div id="" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </th><th> <div id="" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </th><th> <div id="" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </th><th> <div id="" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </th><th><a id="matrix-add-column" class="btn btn-circle btn-icon-only blue"><i class="fa fa-plus-circle"></i></a><!--<button class="btn green"><i class="fa fa-plus-circle"></i> Add column</button>--></th></tr></thead> <tfoot> <tr><td><a id="matrix-add-row" class="btn btn-circle btn-icon-only blue"><i class="fa fa-plus-circle"></i></a></td><td><a class="matrix-delete-column matrix-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a></td><td><a class="matrix-delete-column matrix-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a></td><td><a class="matrix-delete-column matrix-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a></td><td><a class="matrix-delete-column matrix-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a></td><td></td></tr> </tfoot> <tbody><tr> <td> <div id="" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </td> <td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><a class="matrix-delete-row matrix-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a></td></tr><tr> <td> <div id="" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </td> <td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><a class="matrix-delete-row matrix-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a></td></tr><tr> <td> <div id="" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </td> <td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><a class="matrix-delete-row matrix-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a></td></tr><tr> <td> <div id="" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </td> <td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><a class="matrix-delete-row matrix-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a></td></tr><tr> <td> <div id="" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </td> <td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><div class="checkbox-list"><label><input type="checkbox"></label></div></td><td><a class="matrix-delete-row matrix-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a></td></tr></tbody></table></div> </div> </div> </div> <div class="form-group"> <div class="AddHint"> <label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label> <div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </div> </div>';
			break;

		// case  to generate html for Integer Question Type
		case 13:
			html = '<div id="integer" class=""> <div class="form-group"> <label> Enter Question: </label> <div id="integer-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </div> </div> <div class="col-md-12 col-xs-12"> <div class="col-md-6"><lable>From: </lable><input id="from" type="text" class="form-control">  </div>  <div class="col-md-6"><lable>To: </lable> <input id="to" type="text" class="form-control">  </div> </div> <div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;

		default:
			html = '';
			break;
	}
	return html;
	setEventDeleteOptions();
}


function showDescriptionBox(questionType) {
	if(questionType == 0 || questionType == 11) {
		$('#description-container').html('');
	}
	else {
		// $('#description-container').html('<div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div>');
		hintEditor = CKEDITOR.inline('hint-box');

	}

}

// function eventAddHintRemoveHint() {
// 	$('.btn_AddHint').off('click')
// 	$('.btn_AddHint').on('click',function(e) {
// 		$('.AddHint').removeClass('hidden');
		// hintEditor = CKEDITOR.inline('hint-box');
// 		$('.btn_AddHint').addClass('hidden');
// 		// descriptionEditor.on('blur', function(e){
// 			// data = descriptionEditor.getData();
// 		// });
// 	});
// 	$('.btn_removeHint').off('click')
// 	$('.btn_removeHint').on('click',function(e) {
// 		$('.AddHint').addClass('hidden');
// 		$('.btn_AddHint').removeClass('hidden');
// 	})

// }

function resetPageValues(){
	if(!($("#keepSame").prop('checked'))) {
		$('#classes').val(0).change();
		$('#map_keyword').val('');
		$('#description').val('');
		$('input:checkbox, input:radio').prop('checked',false).parent().removeClass('checked');
		$("#tags").select2('val',null);
		$("#difficulty-levels").select2('val',null);
		$("#skills").select2('val',null);
	}

	$('[contenteditable="true"]').html('');;
	$('#question-container input:checkbox, input:radio').prop('checked',false).parent().removeClass('checked');
}


// function changeFirstLetterToUpper() {
// 	$('[contenteditable]').blur(function() {
// 		$(this).text($(this).text().charAt(0).toUpperCase() + $(this).text().slice(1));
// 	})
// }