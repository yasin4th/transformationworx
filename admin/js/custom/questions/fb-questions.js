$(function() {})

function setEventAddBlank() {
	// function to add option
	//alert("hi");
	var i = 0;
	$('#fb-addBlank').off('click')
	$('#fb-addBlank').on('click', function(e) {
		e.preventDefault();
		i++;

		html = '<div class="row">';
		html += '<div class="col-md-12">';
		html += '<div class="col-md-1 form-group radio-option">';
		html += '<div class="input-group">';
		html += '<div class="icheck-list">';
		html += '<label class="index"></label>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		html += '<div class="col-md-10 form-group">';
		html += '<input class="form-control option" data-id="input'+(i+1)+'" placeholder="Option"/>';
		
		//html += '<div class="col-md-1 col-xs-2">';
		//html += '<a class="fb-dd-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a>';
		//html += '</div>';


		html += '</div>';
		html += '</div>';
		html += '</div>';
		divText = fbQuestionEditor.getData();
		$('#fb-question-box').html(divText.slice(0,-5)+'<input class="fb-input" disabled="true" data-id="input'+(i+1)+'" size="8"></p>');
		$('#fb-options-div').append(html);
	});
}
	// CKEDITOR.inline( 'fb-option'+genratedId);
	// setEventDeleteFbOptions();
	// if(i < 50) {
	// 	i += 1;
	// 	$.each(opt,function(x,o) {
	// 		$(o).parents('.row').eq(0).find('.index').text(x+1);
	// 	});
	

// function to delete option
function setEventDeleteFbOptions() {

	$('#fb-question-box').keyup(function(e) {
	    e.preventDefault();
	    
	    blank = $('#fb-question-box input').length;
	    option = $("#fb-options-div .option").length;
	    
	    blanks = $('#fb-question-box input');
	    options = $("#fb-options-div .option");

	    if (blank != option) {
	    	var barr = [];
	    	$.each(blanks, function(i, blk) {
	    		barr.push($(blk).data('id'));

	    	})
	    	
	    	$.each(options, function(i, opt) {
	    		a = $(opt).data('id');
	    		if(barr.indexOf(a) == -1){
	    			$(opt).parent().parent().remove();
	    		}


	    	})
	    	
	        /*options = $("#fb-options-div .option")
	        $.each(options, function(i, opt) {
	            a = $(opt).data('id');
	            console.log(a);
		        if ($('[data-id="' + a + '"]').length != 2)
		            $(opt).parents('.row').eq(0).remove();
			})*/
		}
	});

}

// this function fetch fb option from page
function getOptionsFb() {
	options = $("#fb-options-div .option");
	value = [];
	$.each(options, function(i,option) {
		choises = {};
		//choises.option = $(option).html();
		choises.option = $(option).val();
		choises.answer = 1;
		value.push(choises);
	});
	option_values = value;
	return value;
}

// function set editor in fb div
function fbLoadEditor() {

	setEventAddBlank();
	setEventDeleteFbOptions(); // function set events to delete on options
	//fbQuestionEditor.destroy();
	fbQuestionEditor = CKEDITOR.inline('fb-question-box');
}

// this function save fb type question
function saveFb() {
	if(validateFB()){
		var req = getFB_data();
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				resetPageValues();
				toastr.success(res.message,'Question Id: 00'+res.questionId);
			}
			else {
				toastr.error(res.message,'Error: Question Not Saved');
			}
		});
	}
}

function getFB_data() {
	var req = {};
	req.action = "addQuestion";
	req.questionData = fbQuestionEditor.getData();
	req.DescriptionData = "No Description.";
	if(hintEditor != undefined)
		req.DescriptionData = hintEditor.getData();
	req.parent_id = parent_id;
	req.questionType = 6;
	req.options = getOptionsFb();
	req.class_id = $("#classes").find('option:selected').data('id');
	req.subject_ids = getSelectedSubjectId();
	req.unit_ids = getSelectedUnitId();
	req.chapter_ids = getSelectedChapterId();
	req.topic_ids = getSelectedTopicId();
	req.tag_ids = getSelectedTagId();
	req.difficultyLevel_ids = getSelectedDifficultyLevelId();
	req.skill_ids = getSelectedskillId();
	return req;
}

function validateFB(){
	options = $("#fb-options-div .option");
	if(fbQuestionEditor.getData().length <= 8){		
		toastr.error("Please enter Fb Question");
		return false;
	}
	
	option = getOptionsFb();
	var msg = true;

	$.each(option, function(i,opt){
		if(opt.option.length == 0){
			msg =false;
		}
	})
	$.each(options,function(i,option){
		console.log(option);
		if($(option).val()==""){
			toastr.error("Please enter Options");
			return;
		}
	});
	if($('#fb-question-box').find('.input-droppable').length==0){
		toastr.error("Please insert fill in the blank");
		return;
	}
	if(msg == false){
		toastr.error("Please enter Options");
	}
	return msg;	
}