// function set editor in true false div
function trueFalseLoadEditor() {

	tfquestionEditor = CKEDITOR.inline('tf-question-box');
}

// this function fetch true false option from page
function tfGetOptions() {
	options = ['true','false'];
	value = [];
	$.each(options, function(i,option){
		choises = {};
		choises.option = option;
		choises.answer = 0;
		if($("#"+option).prop("checked"))
			choises.answer = 1;
		value.push(choises);
	});
	option_values = value;
	return value;
}

// this function save TF type question
function saveTF(data) {
	req = getTF_Data();
	
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			resetPageValues();
			toastr.success(res.message,'Question Id: 00'+res.questionId)
		}
		else {
			toastr.error(res.message,'Error: Question Not Saved')
		}
	});
}

function getTF_Data() {
	var req = {};
	req.action = "addQuestion";
	req.questionData = tfquestionEditor.getData();
	req.DescriptionData = "No Description.";
	if(hintEditor != undefined)
		req.DescriptionData = hintEditor.getData();
	req.questionType = 3;
	req.parent_id = parent_id;
	req.options = tfGetOptions();
	req.class_id = $("#classes").find('option:selected').data('id');
	req.subject_ids = getSelectedSubjectId();
	req.unit_ids = getSelectedUnitId();
	req.chapter_ids = getSelectedChapterId();
	req.topic_ids = getSelectedTopicId();
	req.tag_ids = getSelectedTagId();
	req.difficultyLevel_ids = getSelectedDifficultyLevelId();
	req.skill_ids = getSelectedskillId();
	return req;
}