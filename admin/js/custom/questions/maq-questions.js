$(function() {})

// function to add option
function setEventAddMcqOption() {
	$('.maq-addOption').on('click', function() {
		var i = $(this).parents(".multiChoise").find(".option").length;
		if(i < 6) {
			i += 1;
			genratedId = $(this).parents(".multiChoise").find(".option").eq('-1').data('id')+1;
			html = '<div class="row"><div class="col-md-12"><div class="col-md-1 form-group radio-option"><div class="input-group"><div class="checkbox-list"><label><input type="checkbox" data-answer="3"></label></div></div></div><div class="col-md-10 form-group"><div contentEditable="true" class="option" name="maq-option'+i+'" data-id="'+genratedId+'" id="maq-option'+genratedId+'" placeholder="Option '+i+'" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div></div><div class="col-md-1"><a class="maq-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div></div></div>'
		
			$(this).parents(".multiChoise").find('.maq-options-div').append(html);
			maqSetEventDeleteOptions();
			CKEDITOR.inline( 'maq-option'+genratedId);
			Metronic.init();
		}
	});

}

// function to delete option
function maqSetEventDeleteOptions(){	
	$('.maq-deleteOption').off('click');
	$('.maq-deleteOption').on('click', function() {
		var remove = $(this).parents(".multiChoise").find(".option").length;
		if(remove>2) {
			$(this).parents().eq(1).remove();
			opt = $(this).parents(".multiChoise").find(".option");
			$.each(opt,function(x,o) {
				remove = x+1;
				$(o).attr('placeholder','Option '+remove);
			});
		}
	});
}

// this function fetch maq option from page
function maqGetOptions() {
	options = $(".maq-options-div .option");
	value = [];
	$.each(options, function(i,option){
		choises = {};
		// choises.option = $(option).html();
		choises.option = CKEDITOR.instances[$(option).attr('id')].getData();
		choises.answer = 0;
		if($(option).parents('.row').eq(0).find('input:checkbox').prop("checked"))
		choises.answer = 1;
		value.push(choises);
	});
	option_values = value;
	return value;
}

// function set editor in maq div
function maqLoadEditor() {
	setEventAddMcqOption();
	maqSetEventDeleteOptions(); // function set events to delete on options

	maqQuestionEditor = CKEDITOR.inline('maq-question-box');
	
	CKEDITOR.inline('maq-option1');
	CKEDITOR.inline('maq-option2');
	CKEDITOR.inline('maq-option3');
	CKEDITOR.inline('maq-option4');
}

// this function save maq type question
function saveMAQ(data) {
	if(validateMaq()){
		req = getMAQ_Data();
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				resetPageValues();
				toastr.success(res.message,'Question Id: 00'+res.questionId);
			}
			else {
				toastr.error(res.message,'Error: Question Not Saved');
			}
		});
	}
}

function getMAQ_Data() {
	var req = {};
	req.action = "addQuestion";
	req.questionData = maqQuestionEditor.getData();
	req.DescriptionData = "No Description.";
	if(hintEditor != undefined)
		req.DescriptionData = hintEditor.getData();
	req.parent_id = parent_id;
	req.questionType = 2;
	req.options = maqGetOptions();
	req.class_id = $("#classes").find('option:selected').data('id');
	req.subject_ids = getSelectedSubjectId();
	req.unit_ids = getSelectedUnitId();
	req.chapter_ids = getSelectedChapterId();
	req.topic_ids = getSelectedTopicId();
	req.tag_ids = getSelectedTagId();
	req.difficultyLevel_ids = getSelectedDifficultyLevelId();
	req.skill_ids = getSelectedskillId();
	req.parent_id = parent_id;
	return req;
}

function validateMaq(){
	if(maqQuestionEditor.getData().length <= 8){		
		toastr.error("Please enter Multiple Choice Question.");
		return false;
	}
	
	option = maqGetOptions();
	checkedOptions=$(".maq-options-div").find(".option").parents('.row').find('input:checkbox:checked'); //added by suresh

	var msg = true;

	$.each(option, function(i,opt){
		if(opt.option.length <= 8){
			msg =false;
		}
	})

	if(msg == false){
		toastr.error("Please enter Options");
	}
	if(checkedOptions.length == 0)
	{
		toastr.error("Please check answer");
		return;
	}
	return msg;	
}