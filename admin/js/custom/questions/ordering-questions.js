$(function() {



	
})

// function to add option

// function to delete option
function setEventAddOrderingOptions() {
	$('#ord-addOption').on('click', function() {
	var i = $("#ord-options-div .option").length;
	if(i < 6) {
		i += 1;
		genratedId = $("#ord-options-div .option").eq('-1').data('id')+1;
		html = '<div class="row"><div class="col-md-12"><div class="col-md-1 form-group radio-option"><div class="input-group"><div class="icheck-list"><label class="index">'+i+'</label></div></div></div><div class="col-md-10 form-group"><div contentEditable="true" class="option" name="ord-option'+i+'" data-id="'+genratedId+'" id="ord-option'+genratedId+'" placeholder="Option '+i+'" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div></div><div class="col-md-1"><a class="ord-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div></div></div>';
	
		$('#ord-options-div').append(html);
		setEventDeleteOrderingOptions();
		CKEDITOR.inline( 'ord-option'+genratedId);
		$.each(opt,function(x,o) {
			$(o).parents('.row').eq(0).find('.index').text(x+1);
		});
	}
});
}

function setEventDeleteOrderingOptions() {
	$(' .ord-deleteOption').off('click');
	$('.ord-deleteOption').on('click', function() {
		var remove = $("#ord-options-div .option").length;
		if(remove>2) {
			$(this).parents().eq(1).remove();
			opt = $("#ord-options-div .option");
			$.each(opt,function(x,o) {
				$(o).parents('.row').eq(0).find('.index').text(x+1);
			});
		}
	});
}

// this function fetch ord option from page
function getOptionsOrdering() {
	options = $("#ord-options-div .option");
	value = [];
	$.each(options, function(i,option){
		choises = {};
		//choises.option = $(option).html();
		choises.option = CKEDITOR.instances[$(option).attr('id')].getData();
		choises.answer = 1;
		/*if($(option).parents('.row').eq(0).find('input:radio').prop("checked"))
		choises.answer = 1;*/
		value.push(choises);
	});
	option_values = value;
	return value;
}

// function set editor in ord div
function orderingLoadEditor() {
	setEventAddOrderingOptions(); // function set events to add on options
	setEventDeleteOrderingOptions(); // function set events to delete on options
	//ordQuestionEditor.destroy();
	ordQuestionEditor = CKEDITOR.inline('ord-question-box');
	// questionEditor.on('blur', function(e){
		// data = questionEditor.getData();
	// });
	
	CKEDITOR.inline('ord-option1');
	CKEDITOR.inline('ord-option2');
	CKEDITOR.inline('ord-option3');
	CKEDITOR.inline('ord-option4');
}

// this function save ord type question
function saveOrd(data) {
	if(validateOrd()){
		var req = {};
		req.action = "addQuestion";
		req.questionData = ordQuestionEditor.getData();
		req.DescriptionData = "No Description.";
		if(hintEditor != undefined)
			req.DescriptionData = hintEditor.getData();
		req.parent_id = parent_id;
		req.questionType = 4;
		req.options = getOptionsOrdering();
		req.class_id = $("#classes").find('option:selected').data('id');
		req.subject_ids = getSelectedSubjectId();
		req.unit_ids = getSelectedUnitId();
		req.chapter_ids = getSelectedChapterId();
		req.topic_ids = getSelectedTopicId();
		req.tag_ids = getSelectedTagId();
		req.difficultyLevel_ids = getSelectedDifficultyLevelId();
		req.skill_ids = getSelectedskillId();
		
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				resetPageValues();
				toastr.success(res.message,'Question Id: 00'+res.questionId);
			}
			else {
				toastr.error(res.message,'Error: Question Not Saved');
			}
		});
	}
}


function validateOrd(){
	if(ordQuestionEditor.getData().length <= 8){		
		toastr.error("Please enter Sequencing Question.");
		return false;
	}
	
	option = getOptionsOrdering();
	var msg = true;

	$.each(option, function(i,opt){
		if(opt.option.length <= 8){
			msg =false;
		}
	})

	if(msg == false){
		toastr.error("Please enter Options");
	}
	return msg;	
}