$(function() {})

function setEventAddScqOptions(){
	// alert("ADD option for scq");
	// function to add option
	$('.scq-addOption').off('click');
	$('.scq-addOption').on('click', function() {
		var i = $(this).parents(".singleChoise").find(".option").length;
		if(i < 6) {
			i += 1;
			genratedId = $(this).parents(".singleChoise").find(".option").eq('-1').data('id')+1;
			html = '<div class="row"><div class="col-md-12 col-xs-12">';
			html += '<div class="col-md-1 col-xs-2 form-group radio-option">';
			html += '<div class="input-group"><div class="icheck-list"><label><input type="radio" name="radioOption" data-answer="'+i+'"></label></div></div></div>';
			html += '<div class="col-md-10 col-xs-8 form-group"><div contentEditable="true" class="option" name="scq-option'+i+'" data-id="'+genratedId+'" id="scq-option'+genratedId+'" placeholder="Option '+i+'" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div></div><div class="col-md-1 col-xs-2">';
			html += '<a class="scq-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a>';
			html += '</div></div></div>';

			$(this).parents('.singleChoise').find('.scq-options-div').append(html);
			setEventDeleteOptions();
			CKEDITOR.inline( 'scq-option'+genratedId);
			Metronic.init();
		}
	});
}

// function to delete option
function setEventDeleteOptions1() {
	//gone to fb-dnd-questions.js
}

// this function fetch scq option from page
function getOptions() {
	options = $(".scq-options-div").find(".option");
	value = [];
	$.each(options, function(i,option){
		choises = {};
		//choises.option = $(option).html();
		choises.option = CKEDITOR.instances[$(option).attr('id')].getData();
		choises.answer = 0;
		if($(option).parents('.row').eq(0).find('input:radio').prop("checked"))
		choises.answer = 1;
		value.push(choises);
	});
	return value;
}

// function set editor in scq div
function scqLoadEditor() {
	setEventAddScqOptions();
	setEventDeleteOptions(); // function set events to delete on options

	questionEditor = CKEDITOR.inline('scq-question-box');
    /*CKEDITOR.replace('scq-question-box', {
        'extraPlugins': 'imagecrop',
        'allowedContent': true,
        'toolbar': 'Custom',
        'toolbar_Custom': [{'name': 'insert', 'items': ['ImageCrop']}],

        // Setup file browser urls
        // (See CKEditor documentation http://docs.ckeditor.com/#!/guide/dev_file_browser_api)
        'filebrowserBrowseUrl': '/browser/browse.php',
        'filebrowserUploadUrl': '/uploader/upload.php',

        // Setup cropper options.
        // (See cropper.js documentation https://github.com/fengyuanchen/cropperjs)
        'cropperOption': {
            'aspectRatio': 1.8,
            'autoCropArea': 1,
            'background': false,
            'cropBoxResizable': true,
            'dragMode': 'move'
        },

        // Add js and css urls to cropper.js
        'cropperJsUrl': 'https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js',
        'cropperCssUrl': 'https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.css'
    });*/
	// questionEditor = CKEDITOR.replace( 'scq-question-box',
 //    {



 //    });

	CKEDITOR.inline('scq-option1');
	CKEDITOR.inline('scq-option2');
	CKEDITOR.inline('scq-option3');
	CKEDITOR.inline('scq-option4');
}

// this function save SCQ type question
function saveSCQ(data) {
	if(validateSCQ()){
		req = getSCQ_Data();
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				resetPageValues();
				toastr.success(res.message,'Question Id: 00'+res.questionId);
			}
			else {
				toastr.error(res.message,'Error: Question Not Saved');
			}
		});
	}
}

function getSCQ_Data() {
	var req = {};
	req.action = "addQuestion";
	req.questionData = questionEditor.getData();
	req.DescriptionData = "No Description.";
	if(hintEditor != undefined)
		req.DescriptionData = hintEditor.getData();
	req.questionType = 1;
	req.parent_id = parent_id;
	req.options = getOptions();
	req.class_id = $("#classes").find('option:selected').data('id');
	req.subject_ids = getSelectedSubjectId();
	req.unit_ids = getSelectedUnitId();
	req.chapter_ids = getSelectedChapterId();
	req.topic_ids = getSelectedTopicId();
	req.tag_ids = getSelectedTagId();
	req.difficultyLevel_ids = getSelectedDifficultyLevelId();
	req.skill_ids = getSelectedskillId();
	return req;
}

function validateSCQ(){
	if(questionEditor.getData().length <= 11){
		toastr.error("Please enter Scq Question");
		return false;
	}
	option = getOptions();
	checkedOption=$(".scq-options-div").find(".option").parents('.row').find('input:radio:checked'); //added by suresh
	var msg = true;
	$.each(option, function(i,opt){
		if(opt.option.length <= 8){
			msg =false;
		}
	});
	if(msg == false){
		toastr.error("Please enter Options");

	}
	if(checkedOption.length == 0)
	{
		toastr.error("Please check answer");
		return;
	}
	return msg;
}