$(function() {})

// function to add option
$('#ar-addOption').on('click', function() {
	var i = $("#ar-options-div .option").length;
	if(i < 6) {
		i += 1;
		genratedId = $("#ar-options-div .option").eq('-1').data('id')+1;
		html = '<div class="row"><div class="col-md-12"><div class="col-md-1 form-group radio-option"><div class="input-group"><div class="icheck-list"><label><input type="radio" name="ass-res-radio-option" data-answer="'+i+'"></label></div></div></div><div class="col-md-10 form-group"><div contentEditable="true" class="option" name="ar-option'+i+'" data-id="'+genratedId+'" id="ar-option'+genratedId+'" placeholder="Option '+i+'" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div></div><div class="col-md-1"><a class="ar-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div></div></div>'
	
		$('#ar-options-div').append(html);
		setEventDeleteOptionsAR();
		CKEDITOR.inline( 'ar-option'+genratedId);
		Metronic.init();
	}
});

// function to delete option
function setEventDeleteOptionsAR() {
	$(' .ar-deleteOption').off('click');
	$('.ar-deleteOption').on('click', function() {
		var remove = $("#ar-options-div .option").length;
		if(remove>2) {
			$(this).parents().eq(1).remove();
			opt = $("#ar-options-div .option");
			$.each(opt,function(x,o) {
				remove = x+1;
				$(o).attr('placeholder','Option '+remove);
			});
		}
	});
}

// this function fetch ar option from page
function getOptionsAR() {
	options = $("#ar-options-div .option");
	value = [];
	$.each(options, function(i,option){
		choises = {};
		//choises.option = $(option).html();
		choises.option = CKEDITOR.instances[$(option).attr('id')].getData();
		choises.answer = 0;
		if($(option).parents('.row').eq(0).find('input:radio').prop("checked"))
		choises.answer = 1;
		value.push(choises);
	});
	option_values = value;
	return value;
}

// function set editor in ar div
function arLoadEditor() {
	setEventDeleteOptionsAR(); // function set events to delete on options


	ass_questionEditor = CKEDITOR.inline('assertion-question-box');
	res_questionEditor = CKEDITOR.inline('reason-question-box');

	CKEDITOR.inline('ar-option1');
	CKEDITOR.inline('ar-option2');
	CKEDITOR.inline('ar-option3');
	CKEDITOR.inline('ar-option4');
}

// this function save ar type question
function saveAr(data) {
	var req = {};
	req.action = "addQuestion";
	req.ass_questionData = ass_questionEditor.getData();
	req.ress_questionData = res_questionEditor.getData();
	req.DescriptionData = "No Description.";
	if(hintEditor != undefined)
		req.DescriptionData = hintEditor.getData();
	req.parent_id = parent_id;
	req.questionType = 9;
	req.options = getOptionsAR();
	req.class_id = $("#classes").find('option:selected').data('id');
	req.subject_ids = getSelectedSubjectId();
	req.unit_ids = getSelectedUnitId();
	req.chapter_ids = getSelectedChapterId();
	req.topic_ids = getSelectedTopicId();
	req.tag_ids = getSelectedTagId();
	req.difficultyLevel_ids = getSelectedDifficultyLevelId();
	req.skill_ids = getSelectedskillId();
	
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			resetPageValues();
			toastr.success(res.message,'Question Id: 00'+res.questionId);
		}
		else {
			toastr.error(res.message,'Error: Question Not Saved');
		}
	});
}