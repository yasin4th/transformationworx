$(function() {})


// function set editor in descriptive div
function descriptiveLoadEditor() {
	  
	descriptiveQuestionEditor = CKEDITOR.inline('descriptive-question-box');
	
	// questionEditor.on('blur', function(e){
		// data = questionEditor.getData();
	// });
	
	// descriptiveDescriptionEditor = CKEDITOR.inline('descriptive-description-box');
	// descriptionEditor.on('blur', function(e){
		// data = descriptionEditor.getData();
	// });
}

// this function save descriptive type question
function saveDescriptive(data) {
	if(validator()){
		var req = {};
		req.action = "addQuestion";
		req.questionData = descriptiveQuestionEditor.getData();
		req.DescriptionData = "No Description.";
		if(hintEditor != undefined)
			req.DescriptionData = hintEditor.getData();
		req.parent_id = parent_id;
		req.questionType = 10;
		req.class_id = $("#classes").find('option:selected').data('id');
		req.subject_ids = getSelectedSubjectId();
		req.unit_ids = getSelectedUnitId();
		req.chapter_ids = getSelectedChapterId();
		req.topic_ids = getSelectedTopicId();
		req.tag_ids = getSelectedTagId();
		req.difficultyLevel_ids = getSelectedDifficultyLevelId();
		req.skill_ids = getSelectedskillId();
		
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				resetPageValues();
				toastr.success(res.message,'Question Id: 00'+res.questionId);
			}
			else {
				toastr.error(res.message,'Error: Question Not Saved');
			}
		});
		
	}
}
function validator(){

	if(descriptiveQuestionEditor.getData() =="")
	{
		toastr.error('Please insert question.');
		return false;
	}

	if(hintEditor != undefined && hintEditor.getData() =="" )
	{
		toastr.error('Please insert answer of question.');
		return false;
	}
	return true;
}