function matrixAddRow() {
	$('#matrix-add-row').off('click');
	$('#matrix-add-row').on('click', function(e) {
		e.preventDefault();
		if ($('#matrix-table tbody tr').length < 10) {

			html = '<tr> <td> <div id="" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></td> '
			for(x=2; x<$('#matrix-table thead tr:eq(0) th').length; x++) {
				html += '<td><div class="checkbox-list"><label><input type="checkbox"></label></div></td>'; 
			}
			html += '<td><a class="matrix-delete-row matrix-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a></td></tr>';
			$('#matrix-table tbody').append(html);
		};
	matrixDeleteRow();
	});
}

function matrixAddColumn() {
	$('#matrix-add-column').off('click');
	$('#matrix-add-column').on('click', function(e) {
		e.preventDefault();
		if($('#matrix-table thead tr:eq(0) th').length < 12) {
			$('#matrix-table thead tr:eq(0) th:eq(-1)').before('<th> <div id="" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </th>');
			$.each($('#matrix-table tbody tr'), function(i,tr) {
				$(tr).find('td:eq(-1)').before('<td> <div class="checkbox-list"> <label> <input type="checkbox"> </label> </div> </td>');
			});
			$('#matrix-table tfoot tr:eq(0) td:eq(-1)').before('<td><a class="matrix-delete-column matrix-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a></td>');
		}
	matrixDeleteColumn();
	});
}

function matrixDeleteRow() {
	$('.matrix-delete-row').off('click');
	$('.matrix-delete-row').on('click', function(e) {
		e.preventDefault();
		if ($('#matrix-table tbody tr').length > 2) {
			$(this).parents('tr').remove();
		}
	});
}

function matrixDeleteColumn() {
	$('.matrix-delete-column').off('click');
	$('.matrix-delete-column').on('click', function(e) {
		e.preventDefault();
		if($('#matrix-table thead tr:eq(0) th').length > 4) {
			index = $(this).parent()[0].cellIndex;
			$('#matrix-table thead tr:eq(0) th:eq('+index+')').remove();
			$.each($('#matrix-table tbody tr'), function(i,tr) {
				$(tr).find('td:eq('+index+')').remove();
			});
			$('#matrix-table tfoot tr:eq(0) td:eq('+index+')').remove();
		}
	});
}

// function load events of matrix question type
function matrixLoadEvents() {
	matrixAddRow();
	matrixAddColumn();
	matrixDeleteRow(); // function set event delete row
	matrixDeleteColumn(); // function set event delete column

	questionEditor = CKEDITOR.inline('matrix-question-box');
}


// this function save SCQ type question
function saveMatrix() {
	if(validateMatrix()){
		req = getMatrixData();
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				resetPageValues();
				toastr.success(res.message,'Question Id: 00'+res.questionId);
			}
			else {
				toastr.error(res.message,'Error: Question Not Saved');
			}
		});
	}
}

function getMatrixData() {
	var req = {};
	req.action = "addQuestion";
	req.questionData = questionEditor.getData();
	req.DescriptionData = "No Description.";
	if(hintEditor != undefined)
		req.DescriptionData = hintEditor.getData();
	req.questionType = 12;
	req.parent_id = parent_id;
	req.options = getMatrixOptions();
	req.class_id = $("#classes").find('option:selected').data('id');
	req.subject_ids = getSelectedSubjectId();
	req.unit_ids = getSelectedUnitId();
	req.chapter_ids = getSelectedChapterId();
	req.topic_ids = getSelectedTopicId();
	req.tag_ids = getSelectedTagId();
	req.difficultyLevel_ids = getSelectedDifficultyLevelId();
	req.skill_ids = getSelectedskillId();
	return req;
}

function getMatrixOptions() {
	options = [];
	
	thead = $('#matrix-table thead th')
	thead.splice(0,1);
	thead.splice(-1,1);
	$.each(thead, function(i, th) {
		var opt= {};
		opt.answer = 1;
		opt.option = $(th).find('[contenteditable]').html();
		options.push(opt);
	});

	var number = 2;
	$.each($('#matrix-table tbody tr'), function(i, tr) {
		values = $(tr).find(' td');
		values.splice(0,1);
		values.splice(-1,1);
		
		var opt= {};
		opt.answer = number;
		opt.option = $(tr).find('td:eq(0)').find('[contenteditable]').html();
		options.push(opt);
		$.each(values, function(x, td) {
			var opt= {};
			opt.answer = number;
			opt.option = 0;
			if ($(td).find('input:checkbox').prop('checked')) {
				opt.option = 1;
			};
			options.push(opt);
		})

		number++;
	})
	return options;
}

function validateMatrix(){
	if(questionEditor.getData().length <= 11){		
		toastr.error("Please enter Matrix Question.");
		return false;
	}	
	option = $('#matrix-table [contenteditable="true"]');
	var msg = true;

	$.each(option, function(i,opt){
		if($(opt).html().length == 0){
			
			msg =false;
		}			
	})

	if(msg == false){
		toastr.error("Please enter Options");
	}
	return msg;	
	
}