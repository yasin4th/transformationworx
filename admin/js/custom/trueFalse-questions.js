// function set editor in true false div
function trueFalseLoadEditor() {

	tfquestionEditor = CKEDITOR.inline('tf-question-box');
}

// this function fetch scq option from page
function tfGetOptions() {
	options = ['true','false'];
	value = [];
	$.each(options, function(i,option){
		choises = {};
		choises.option = option;
		choises.answer = 0;
		if($("#"+option).prop("checked"))
			choises.answer = 1;
		value.push(choises);
	});
	option_values = value;
	return value;
}

// this function save SCQ type question
function saveTF(data) {
	var req = {};
	req.action = "addQuestion";
	req.questionData = tfquestionEditor.getData();
	req.DescriptionData = hintEditor.getData();
	req.questionType = $('#question-types option:selected').attr('data-id');
	req.options = tfGetOptions();
	req.class_id = $("#classes").find('option:selected').data('id');
	req.subject_ids = getSelectedSubjectId();
	req.unit_ids = getSelectedUnitId();
	req.chapter_ids = getSelectedChapterId();
	req.topic_ids = getSelectedTopicId();
	req.tag_ids = getSelectedTagId();
	req.difficultyLevel_ids = getSelectedDifficultyLevelId();
	req.skill_ids = getSelectedskillId();
	
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(!($("#keepSame").prop('checked'))) {
			$('select').val(0);
			$('#classes').val("Select Class").change();
			$("#tags").select2('val',null);
			$("#difficulty-levels").select2('val',null);
			$("#skills").select2('val',null);
		}
		$('input:checkbox').parent().removeClass('checked');
		$('input:radio').parent().removeClass('checked');
		$(':input','#question-container').not(':button, :submit, :reset, :hidden').val('').removeAttr('selected').parent().removeClass('checked');
	
		$('#tf-question-box').html('');
		$('#tf-description-box').html('');
		console.log(res);
	});
}