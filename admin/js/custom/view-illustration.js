test_program = getUrlParameter('test_program');
subject = getUrlParameter('subject')
chapter = getUrlParameter('chapter');
$(function(){
	
	$(".test-program-name").attr('href', 'view-test-program.php?test_program='+test_program)
	$('.subject-navigation').attr('href','chapters-in-test-program.php?test_program='+test_program+'&subject='+subject);
	$('.chapter-navigation').attr('href','attach-paper.php?test_program='+test_program+'&subject='+subject+'&chapter='+chapter);

	fetchAttachedIllustrations();

});

function fetchAttachedIllustrations() {
	var req = {};
	req.action = 'fetchAttachedIllustrations';
	req.chapter = chapter;
	req.test_program = test_program;

	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		$(".test-program-name").text(res.test_program)
			fillIllustrationsTbody(res.data);
	});
}

function fillIllustrationsTbody(illustrations) {
	html = '';
	$.each(illustrations,function(x,illustration) {
		html += '<tr data-id="'+illustration.id+'"> <td> '+illustration.name+' </td> <td> <a href="../'+illustration.source+'" class="view-details">View File</a> </td> <td> <a class="delete-illustration" style="color: rgb(230, 12, 12);font-weight: bold; text-align:center;"><i class="fa fa-trash-o"></i></a> </td> </tr>';
	});
	$('#illustration-table tbody').html(html);	
	setEventDeleteIllustration();
}

function setEventDeleteIllustration() {
	$('.delete-illustration').off('click');
	$('.delete-illustration').on('click', function() {
		var req = {};
		req.action = 'deleteIllustration';
		req.id = $(this).parents('tr').data('id');
		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req)  
		}).done(function(res) {
			res = $.parseJSON(res);
			if (res.status == 1) {
				fetchAttachedIllustrations();
				toastr.success(res.message);
			} else{
				toastr.error(res.message);
			};
		});
	});
}