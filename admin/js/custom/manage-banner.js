$(function() {

	fetchBBTemplate();
	saveLogo();
	saveBanner();
	saveTemplate();
	downloadTemplate();
	$('[name=id]').val(getUrlParameter('institute'));
	$('#download-link').attr('href','../download-template.php?institute='+getUrlParameter('institute')+'&template=1');
	$('input[name=template]').on('change',function(e) {
		$('#download-link').attr('href','../download-template.php?institute='+getUrlParameter('institute')+'&template='+$('input:radio[name=template]:checked').val());
	})
})

fetchBBTemplate = function()
{
	
	
	var req = {};
	req.action = 'fetchBBTemplate';	
	req.institute = getUrlParameter('institute');
	

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		
		if(res.status == 1) {			
			html = '<div class="row form-group">';
			html += '<div class="col-md-2">Logo <a href="#logo" data-toggle="modal"><i class="fa fa-edit"></i></a></div>';
			html += '<div class="col-md-6"><img class="col-md-6" src="../'+res.logo+'"></div>';
			html += '</div>';
			html += '<div class="row form-group">';
			html += '<div class="col-md-2">Banner <a href="#banner" data-toggle="modal"><i class="fa fa-edit"></i></a></div>';
			html += '<div class="col-md-6"><img class="col-md-12" height="150px" src="../'+res.banner+'"></div>';
			html += '</div>';
			
			
			/*html += '<div class="row">';
			html += '<div class="col-md-3"><a href="#logo" data-toggle="modal">Change Logo</a></div>';
			html += '<div class="col-md-9"><a href="#banner" data-toggle="modal">Change Banner</a></div>';
			html += '</div>';*/
			$('#b2b-details').html(html);
			$('#website').val(res.website);


			CKEDITOR.config.customConfig = 'basic-toolbar-config.js';			
			CKEDITOR.instances['about'].setData( res.about );
			CKEDITOR.instances['contact'].setData( res.contact );
			CKEDITOR.instances['director'].setData( res.director );
			CKEDITOR.instances['features'].setData( res.features );
			CKEDITOR.instances['courses'].setData( res.courses );
			

			toastr.success(res.message);			
		}
		else {
			toastr.error(res.message);
		}
	});
}

saveLogo = function() {
	$('#upload-logo').off('submit');
	$('#upload-logo').on('submit',function(e){
		
		
		e.preventDefault();
		if(validateLogo())
		{
			$.ajax({
				'type'	:	'post',
				'url'	:	FormDataEndPoint,
				
				data: new FormData($('#upload-logo').get(0)),
		    	processData: false,
				contentType: false

			}).done(function(res) {
				
				res = $.parseJSON(res);
				
				if(res.status == 1) {					
					fetchBBTemplate();
					$('#logo').modal("hide");
				}
				else {
					toastr.error(res.message);
					
				}
			});
		}
	});
}

validateLogo = function()
{
	/*if($('#logo').val() == '')
	{
		toastr.info('Please select logo!');
		return false;
	}*/
	return true;
}

saveBanner = function() {
	$('#upload-banner').off('submit');
	$('#upload-banner').on('submit',function(e){
		
		
		e.preventDefault();
		if(validateBanner())
		{
			$.ajax({
				'type'	:	'post',
				'url'	:	FormDataEndPoint,
				data: new FormData($('#upload-banner').get(0)),
		    	processData: false,
				contentType: false

			}).done(function(res) {
				
				res = $.parseJSON(res);
				
				if(res.status == 1) {					
					fetchBBTemplate();
					$('#banner').modal("hide");
				}
				else {
					toastr.error(res.message);
					
				}
			});
		}
	});
}

validateBanner = function()
{
	/*if($('#banner').val() == '')
	{
		toastr.info('Please select banner!');
		return false;
	}*/
	return true;
}

saveTemplate = function() {
	$('#create-b2b').off('submit');
	$('#create-b2b').on('submit',function(e){
		
		
		e.preventDefault();
		if(validate())
		{
			var req = {};
			req.action = 'saveB2BTemplate'
			req.id = getUrlParameter('institute');
			req.features = $('#features').html();
			req.about = $('#about').html();
			req.director = $('#director').html();
			req.courses = $('#courses').html();
			req.contact = $('#contact').html();
			req.website = $('#website').val();
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				
				res = $.parseJSON(res);
				
				if(res.status == 1) {					
					fetchBBTemplate();
					$('#banner').modal("hide");
				}
				else {
					toastr.error(res.message);
					
				}
			});
		}
	});
}

validate = function() 
{
	return true;
}

downloadTemplate = function()
{
	
	/*$('#download').off('submit');
	$('#download').on('submit',function(e){
		
		
		e.preventDefault();
		if(validate())
		{
			var req = {};
			req.action = 'downloadTemplate'
			req.id = getUrlParameter('institute');
			req.template = $('input:radio[name=template]:checked').val() || 1;
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				
				res = $.parseJSON(res);
				
				if(res.status == 1) {
					//fetchBBTemplate();
					$('#download-modal').modal("hide");
				}
				else {
					toastr.error(res.message);
					
				}
			});
		}
	});*/
}