$(function() {
	fetchClasses();
	fetchDifficultyLevel();
	fetchTags();
	fetchSkills();
	fetchQuestionTypes();

	$("#filters").off('click');
	$("#filters").on('click', function() {
		$("#questions-filter-modal").modal("show");

		$("#subjects").select2({
			placeholder: "Subjects ( multiple subjects )"
		});
		$("#units").select2({
			placeholder: "Units ( multiple units )"
		});
		$("#chapters").select2({
			placeholder: "Chapters ( multiple Chapter )"
		});
		$("#topics").select2({
			placeholder: "Topics ( multiple Topic )"
		});
		$("#tags").select2({
			placeholder: "Tags ( multiple Tags )"
		});
		$("#difficulty-levels").select2({
			placeholder: "Difficulty-Levels ( multiple Difficulty Levels )"
		});
		$("#skills").select2({
			placeholder: "Skills ( multiple Skills )"
		});
	});


	$("#filter-questions").off("click");
	$("#filter-questions").on("click", function() {
		var req = getFilterData();
		sn = 1;
		$.ajax({
			"type": "post",
			"url": EndPoint,
			"data": JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status ==  1) {
				// questionsTable.fnClearTable();
				// questionsTable.fnDestroy();
				$('#total-questions').text(res.data.length);
				if (res.data.length) {
					createPagination(res.total_questions);
					html = fillQuestion(res.data);
					$('#questions-table tbody').html(html);

					$('#total-questions').text(res.total_questions);


				} else{
					$('#questions-table tbody').html('<tr><td colspan="10">No questions found</td></tr>');

				};


				setEventDeleteOfQuestion();
				mathsjax();
				Metronic.init();
				// questionsTable = $('#questions-table').dataTable();
				$("#questions-filter-modal").modal("hide");
			}

		});
	});
});

function getFilterData() {
	var req = {};
	req.action = "filterQuestions";
	req.limit = limit;

	if($("#question-id").val() == '' && $("#question-from-id").val() == '' && $("#question-to-id").val() == '') {
		// req.quality_check =	2;

		if($("#classes").val() != "0" && $("#classes").val() != undefined)
			req.classes = $("#classes").val();
		if(getSelectedSubjectId() != '')
			req.subjects = getSelectedSubjectId();
		if(getSelectedUnitId() != '')
			req.units = getSelectedUnitId();
		if(getSelectedChapterId() != '')
			req.chapters = getSelectedChapterId();
		if(getSelectedTopicId() != '')
			req.topics = getSelectedTopicId();
		if(getSelectedTagId() != '')
			req.tags = getSelectedTagId();
		if(getSelectedDifficultyLevelId() != '')
			req.difficultyLevel = getSelectedDifficultyLevelId();
		if(getSelectedskillId() != '')
			req.skills = getSelectedskillId();
		if($("#question-status").val() != '')
			req.quality_check = $("#question-status").val();
		if($("#question-types").val() != "0" && $("#question-types").val() != null)
			req.questionTypes = [$("#question-types").val()];
		if ($("#question-text").val() != '') {
			req.text = $("#question-text").val();
		};
	}
	else {
		req.question_id = $("#question-id").val();
		req.question_from_id = $("#question-from-id").val();
		req.question_to_id = $("#question-to-id").val();
	}
	return req;
}

function resetFilterForm() {
	$("input:text").val("");
	$("#questions-filter-modal select").val(0).change();
	$('#question-status').val("").change();
	$("#tags").select2('val',null);
	$("#difficulty-levels").select2('val',null);
	$("#skills").select2('val',null);
	//location.reload();
}

