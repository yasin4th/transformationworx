$(function(){
	/*** Create Test Paper Functions ***/
	$('#test-time').hide();
	setDateTimePicker();
	setEventOnTestPaperTypeChange();
	setEventWhenHidePaperDetails();

	/******		ck-editor configrations 	*******/
	CKEDITOR.config.customConfig = 'basic-toolbar-config.js';
	CKEDITOR.disableAutoInline = true;



	// event load subjects when admin select a class
	$('#test-paper-class').on('change', function(e) {
		$("#test-paper-subjects").select2('val',null);
		$("#test-paper-subjects").html('');
		fetchTestpaperSubjects();
	});

	// set event save test paper
	$('#save-test-paper').on('click', function() {
		createTestPaper();
	})

/*** Print Paper Functions ***/

})


/**********************************************************************************************************************************/
/**** Create Test Paper Functions **********/
/**********************************************************************************************************************************/

/*
* function sets event to show test paper details modal
*/
function setEventOnCreatePaperButton() {
	$('#create-test-paper').off('click')
	$('#create-test-paper').on('click', function(e) {
		e.preventDefault();
		if($('.select-question:checked').length > 0) {
			
			$('#test-paper-details').modal({backdrop: 'static'});
			$('#test-paper-details-modal').modal("show");		
			setTimeout(function(){ CKEDITOR.inline("test-paper-instructions"); }, 1000); // from this line ck editor toolbar will not disable
			fetchClasses();
			$("#test-paper-subjects").select2();
		}
		else {
			toastr.info("No Questions Selected.");
		}
	})
}

/*
* function sends ajax and create the paper
*/
function createTestPaper() {
	if(validate()){
		var req = getTestPaperDetails();
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				toastr.success(res.message);
				// window.location = 'untitle-question-paper.php?test_paper='+res.test_paper_id+'&status=1';
				window.location = 'test_paper.php';
			}
			else {
				toastr.info(res.message, 'Error');
				console.log(res.error);
			}
		});
		
	}
}

/*
* function get the data filled by admin
*/
function getTestPaperDetails() {
	var req = {};
	req.action = 'createQuestionPaper';
	req.question_ids = getSelectedQuestions();
	req.max_marks = $('#test-paper-max-marks').val();
	req.neg_marks = $('#test-paper-neg-marks').val();
	req.totalQuestion = req.question_ids.length;
	
	req.test_paper_name = $('#test-paper-name').val();
	req.test_paper_type = $('#test-paper-type').val();
	req.test_paper_class = $('#test-paper-class').val();
	req.test_paper_subject = $('#test-paper-subjects').val();
	req.test_paper_time = $('#test-paper-time').val();

	if(req.test_paper_type == '5')
	{
		req.test_paper_attempts = 1;
	}
	else
	{
		req.test_paper_attempts = $('#test-paper-attempts').val();
	}
	req.test_paper_cutoff = $('#test-paper-cutoff').val();
	req.test_paper_start = $('#test-paper-start').val();
	req.test_paper_end = $('#test-paper-end').val();
	req.test_paper_instructions = CKEDITOR.instances['test-paper-instructions'].getData();
	return req;
}

/*
* function destroy ck-editor from test paper instruction
*/
function setEventWhenHidePaperDetails() {
	$('#test-paper-details').off('hidden.bs.modal');
	$('#test-paper-details').on('hidden.bs.modal', function () {
    	CKEDITOR.instances['test-paper-instructions'].destroy();
	});
}

/*
* funnction to show shedule date picker when user select shedule test
*/
function setEventOnTestPaperTypeChange() {
	$('#test-paper-type').off('change');
	$('#test-paper-type').on('change', function(e) {
		e.preventDefault();
		
		if($('#test-paper-type').val() == 5) {
			$('#attempts').hide('slow');
			$('#marks-time-div').show('slow');
			$('#test-time').fadeIn();
		}
		else if($('#test-paper-type').val() == 6) {
			$('#marks-time-div').hide('slow');
			$('#test-time').fadeOut();
			$('#time-div').hide('slow');
		}
		else
		{
			$('#time-div').show('slow');
			$('#attempts').show('slow');
			$('#marks-time-div').show('slow');
			$('#test-time').fadeOut();
		}
	});
}

/*
* set date time picker in test paper shedule
*/
function setDateTimePicker() {

    $('#test-paper-start').datetimepicker({
    	format: "dd MM yyyy - hh:ii",
    	startDate: Date(),
    	showMeridian: true,
        autoclose: true,
        todayHighlight: true
    	// todayBtn: true
    });
    $('#test-paper-end').datetimepicker({
    	format: "dd MM yyyy - hh:ii",
    	startDate: Date(),
    	showMeridian: true,
        autoclose: true,
        todayHighlight: true
    	// todayBtn: true
    });


    $("#test-paper-start").datetimepicker().on('changeDate', function (e) {
		$('#test-paper-end').datetimepicker('setStartDate', $(this).val());
    });
     $("#test-paper-end").datetimepicker().on('changeDate', function (e) {
		$('#test-paper-start').datetimepicker('setEndDate', $(this).val());
    });
}

/*
* this function fetch test papers types
*/
function fetchTestPaperTypes() {
	var req = {};
	req.action = "fetchTestPaperTypes";
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
			fillPaperTypes(res.data);
		else
			alert("ERROR:"+res.message)
	});
}

/*
* fill test paper types
*/
function fillPaperTypes(types) {
	html = '<option value="0">Select Type</option>';
	$.each(types,function(x,type) {
		html += '<option value="'+type.id+'">'+type.type+'</option>';
	});
	$('#test-paper-type').html(html);
}






/**********************************************************************************************************************************/
/**** Print Paper Functions **********/
/**********************************************************************************************************************************/


/*
* function sets even to print questions
*/


function setEventPrintQuestions() {
	$('#print-questions').off('click');
	$("#print-questions").on("click", function () {
		$('iframe').remove();
        if($('.select-question:checked').length > 0) {
	        if($('#print-questions-modal input:radio:checked').length) {
        		var req = {};
				req.action = 'getDataOfQuestions';
				req.question_ids = getSelectedQuestions();
				$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1) {
						DataOfQuestions = res.data;
						if($('#print-questions-modal input:radio:checked').val() == 'with-answer') {
				    		section = genrateFrameBody(DataOfQuestions);
				    		$('#div-to-print').html(section);
				    		MathJax.Hub.Typeset();
				    		$('#print-preview-modal').modal('show');
						    // $('#div-to-print').print()
					}
					    else {
				    		section = genrateFrameBody(DataOfQuestions);
				    		$('#div-to-print').html(section);
				    		MathJax.Hub.Typeset();
				    		$('.solution').hide();
				    		$('.solution-explanation').hide();
				    		$('#print-preview-modal').modal('show');
				    		// $('#div-to-print').print();
					    }
					}
					else {
						toastr.error(res.message,'Error:');
					}
				});
	        }
	        else {
	        	toastr.info('Please Choose an Option...');
	        }
	    }
	    else {
	        	toastr.info('No Questions Selected.');
		}
    });
}

// function to create html of question without answer to pint
function genrateFrameBody(Questions) {
	var html = "";
	$.each(Questions, function(x,question) {
		 html += getQuestionHtml((x+1),question);
		// html += '<div class="question portlet box purple"> <div class="portlet-title"> <div class="caption"> Question '+(x+1)+' </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well"> '+question.question+' </div> <div class="row"> <div class="col-md-1 col-xs-2"></div> <div class="col-md-11 col-xs-10">';
		// html += '<div class="option row"> <div class="col-md-12 col-xs-12"> <ol class="upper-alpha">';
		// $.each(question.options,function(z,option) {
		// 	html += '<li><span>'+option.option+'</span></li>';
		// });
		// html += '</ol> </div> </div> ';
		// html += '</div> </div>';
		// // html += '<div class="row"> <div class="col-md-12 col-xs-12"> '+question.description+' </div> </div>';
		// html += '</div> </div></div>';
	});
	return html;
}

// function to create html of question with answer to pint
// function genrateFrameBodyForWithAnswer(Questions) {
// 	var html = "";
// 	$.each(Questions, function(x,question) {
// 		 html += getQuestionHtml((x+1),question);		
		// html += '<div class="question portlet box purple"> <div class="portlet-title"> <div class="caption"> Question '+(x+1)+' </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well"> '+question.question+' </div> <div class="row"> <div class="col-md-1 col-xs-2"></div> <div class="col-md-11 col-xs-10">';
		// html += '<div class="option row"> <div class="col-md-12 col-xs-12"> <ol class="upper-alpha">';
		// $.each(question.options,function(z,option) {
		// 	html += '<li><span>'+option.option+'</span></li>';
		// });
		// html += '</ol> </div> </div> ';
		// html += '</div> </div>';
		// html += '<div class="row"> <div class="col-md-12 col-xs-12"> '+question.description+' </div> </div>';
		// html += '</div> </div></div>';
// 	});
// 	return html;
// }



var parent=0;



/*
* function return question html by its type
*/
function getQuestionHtml (number,question) {
	html = '';
	switch(parseInt(question.type)) {
		
		case 1:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			if(parent == 0){
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			}else{
			html += ' <span style="float: left;">Q '+parent+'.'+(number)+'&nbsp; </span>';
			}
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			$.each(question.options,function(z,option) {
				if($('#print-questions-modal input:radio:checked').val() == 'with-answer') {
					if(option.answer == 1)
						html += '<span style="display: block;"><input type="radio" checked="checked" style="float: left;margin-right: 5px;"/ disabled> '+option.option+' </span> ';
					else
						html += '<span style="display: block;"><input type="radio" style="float: left;margin-right: 5px;"/ disabled> '+option.option+' </span> ';
				}else
					html += '<span style="display: block;"><input type="radio" style="float: left;margin-right: 5px;"/ disabled> '+option.option+' </span> ';
			});
			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;

		case 2:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			if(parent == 0){
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			}else{
			html += ' <span style="float: left;">Q '+parent+'.'+(number)+'&nbsp; </span>';
			}
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			$.each(question.options,function(z,option) {
				if($('#print-questions-modal input:radio:checked').val() == 'with-answer') {
					if(option.answer == 1)
						html += '<span style="display: block;"><input type="checkbox" checked style="float: left;margin-right: 5px;"/ disabled> '+option.option+' </span> ';
					else
						html += '<span style="display: block;"><input type="checkbox" style="float: left;margin-right: 5px;"/ disabled> '+option.option+' </span> ';
				}else
					html += '<span style="display: block;"><input type="checkbox" style="float: left;margin-right: 5px;"/ disabled> '+option.option+' </span> ';
			});
			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;


		case 3:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			if(parent == 0){
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			}else{
			html += ' <span style="float: left;">Q '+parent+'.'+(number)+'&nbsp; </span>';
			}
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			$.each(question.options,function(z,option) {				
					html += '<span style="display: block;"><input type="checkbox" style="float: left;margin-right: 5px;"/ disabled> '+option.option+' </span> ';
			});
			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;


		case 4:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			html += ' <span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			$.each(question.options,function(z,option) {
				html += '<span style="display: block;"><span style="float: left;margin-right: 5px;">(&nbsp;&nbsp; )</span> '+option.option+' </span> ';
			});
			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;


		case 5:

			/**
			*
			* @author Ravi Arya
			* @date 28-1-2016
			* @param params
			* @return HTML
			* @todo todo
			*
			**/

			html += '<div style="font-size: 13px; font-weight: bold;">';
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			html += '<span> '+question.question+' </span>';
			html += ' </div>';
			var opt1 = [], opt2 = [];
			var i=0,j=0;
			console.log(question.option);
			//html += '<div class="question portlet"> <div class="portlet-title"> <div class="caption"> Question '+(number+1)+' </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> </div> <div class="col-md-11 col-xs-10"> <div class="option row"><div class="col-md-12 col-xs-12"><div class="row text-center"> <div class="col-md-6"> <div class="col-md-11" > <div class="row" style="border: 1px solid #ccc; padding: 10px 10px 0px;">';
			$.each(question.options, function(z,option) {
				if(option.number == '1') {
					opt1[i]=option.option;
					
					i++;
					
				}
				if(option.number == '2') {
					opt2[j]=option.option;
					
					j++;
					
				}
			});
						
			html += '<table>';
			for(var x=0 ; x<i ; x++){
				html += '<tr>';
				html += '<td style="border:1px solid black;"  width="300">'+opt1[x]+'</td>';
				html += '<td width="100"></td>';
				html += '<td style="border:1px solid black;"  width="300">'+opt2[x]+'</td>';
				html += '</tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';				
			}
			if(j>i){
				for (var x = i; x < j; x++) {
					html += '<tr>';
					html += '<td width="300"></td>';
					html += '<td width="100"></td>';
					html += '<td style="border:1px solid black;"  width="300">'+opt2[x]+'</td>';
					html += '</tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';	
				};
			}

				html += '</table>';
				html += '</div>';

			
			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
						
			
		break;



		case 6:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			if(parent == 0){
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			}else{
			html += ' <span style="float: left;">Q '+parent+'.'+(number)+'&nbsp; </span>';
			}
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			// $.each(question.options,function(z,option) {
			// 	html += '<span style="display: block;"><span style="float: left;margin-right: 5px;">(&nbsp;&nbsp; )</span> '+option.option+' </span> ';
			// });
			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;



		case 7:
			$('body').append('<div id="virtual-div"></div>');
			$('#virtual-div').html(question.question);
			dropDowns = $('#virtual-div select');
			$.each(dropDowns,function(x,dropDown) {
				$(dropDown).before('<input disabled>').remove();
			});
			virtualQuestion = $('#virtual-div').html();
			$('#virtual-div').remove();

			html += '<div style="font-size: 13px; font-weight: bold;">';
			if(parent == 0){
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			}else{
			html += ' <span style="float: left;">Q '+parent+'.'+(number)+'&nbsp; </span>';
			}
			html += '<span> '+virtualQuestion+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			for (var x = 1; x <= question.options[question.options.length - 1].number; x++) {				
				html += '<div class="form-group"> <label class=""> Select the correct options for drop down '+ x +' </label>';
				html += ' <div class="">';

				$.each(question.options,function(z,option) {
					if($('#print-questions-modal input:radio:checked').val() == 'with-answer') {
						if(option.answer == 1)
							html += '<span style="display: block;margin-left: 10px;"><input type="radio" checked="checked" style="float: left;margin-right: 5px;"/ disabled> '+option.option+' </span> ';
						else
							html += '<span style="display: block;margin-left: 10px;"><input type="radio" style="float: left;margin-right: 5px;"/ disabled> '+option.option+' </span> ';
					}else
						html += '<span style="display: block;margin-left: 10px;"><input type="radio" style="float: left;margin-right: 5px;"/ disabled> '+option.option+' </span> ';
				});
				
				html += ' </div> ';				
				html += ' </div> ';
			};
			// $.each(question.options,function(z,option) {
			// 	html += '<span style="display: block;"><span style="float: left;margin-right: 5px;">(&nbsp;&nbsp; )</span> '+option.option+' </span> ';
			// });
			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;


		case 8:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			if(parent == 0){
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			}else{
			html += ' <span style="float: left;">Q '+parent+'.'+(number)+'&nbsp; </span>';
			}
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			$.each(question.options,function(z,option) {
				html += '<span style="display: block;"><span style="float: left;margin-right: 5px;">'+(z+1)+': </span> '+option.option+' </span> ';
			});
			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			html += '</div> </div> </div>';
		break;


		case 10:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;

		case 11:
			html += ' <div style="font-size: 13px; font-weight: bold;">';
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';			
			html += ' <span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			$.each(question.sub_questions,function(t,passageQuestion) {
				parent = number;
				html += '';
				html += getQuestionHtml(t+1,passageQuestion);
				html += '';
				parent = 0;
			});
			html += '</div>';

			html += '</div>';
			html += '</div>';
			break;


		case 12:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class=""><table class="table table-bordered table-striped table-hover text-center" style="max-width: 720px;"><thead><tr><th></th>';
			/*
			* creating table head
			*/
			$.each(question.options, function(z,option) {
				if(option.answer == 1) {
					html += '<th class="text-center"> <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;">'+ option.option +'</div> </th>';
				}
			});
			html += '</tr> </thead> ';

			/*
			* creating table body
			*/
			var row = 2;
			html += '<tbody>'
			$.each(question.options ,function(i,option) {
				if(option.answer == row) {
					html += '<tr> <td> <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"> '+option.option+' </div></td>';
					option.answer = 0;
					$.each(question.options ,function(x,opt) {
						if(opt.answer == row) {
							if($('#print-questions-modal input:radio:checked').val() == 'with-answer') {
								if(opt.option == 1)
									html += '<td><div class="checkbox-list"><label><input type="checkbox" disabled checked></label></div></td>';
								else
									html += '<td><div class="checkbox-list"><label><input type="checkbox" disabled></label></div></td>';

							}else{								
									html += '<td><div class="checkbox-list"><label><input type="checkbox" disabled></label></div></td>';								

							}
						
						}
					});				
					row++;
				}
			});
			html += '</tbody></table>';

			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;


		case 13:
			html += '<div style="font-size: 13px; font-weight: bold;">';
			html += ' <span style="float: left;">Q '+(number)+'.&nbsp; </span>';
			html += '<span> '+question.question+' </span>';
			html += ' </div>';

			html += '<div style="margin: 20px;">';
			
			html += '<div class="">';
			// $.each(question.options,function(z,option) {
			// 	if (option.number ==  1) {
			// 		from = option.option;
			// 	};
			// 	if (option.number ==  2) {
			// 		to = option.option;
			// 	};
			// });
			// html += '<span style="display: block;"><span style="float: left;margin-right: 5px;">From: </span> '+from+' </span> ';
			// html += '<span style="display: block;"><span style="float: left;margin-right: 5px;">To: </span> '+to+' </span> ';

			html += '</div>';

			html += '</div>';
			html += '</div>';

			html += '<div class="solution" style="border: dotted 1px;margin: 10px;padding: 5px;"><span style="font-size: 13px;font-weight: bold;text-decoration: underline;">Solution: </span><span style="font-size: 12px;"> '+question.description+' </span></div>';
			break;

		default:
		break;

	}
	return html;
}

function validate()
{

	if($('#test-paper-name').val() == "" ){
		toastr.error('Please enter name.');
		return false;
	}

	if($('#test-paper-type').val() == "" ){
		toastr.error('Please select type.');
		return false;
	}

	if($('#test-paper-class').val() == "" ){
		toastr.error('Please select class.');
		return false;
	}

	if($('#test-paper-subjects').val() == null ){
		toastr.error('Please select subject.');
		return false;
	}
    if($('#test-paper-type').val() != '6')
    {

        if($('#test-paper-type').val() != '5' && $('#test-paper-attempts').val() == ""){
            toastr.error('Please Enter Total Attempts.');
            return false;
        }
        if($('#test-paper-time').val() == "" || $('#test-paper-time').val() == "0"){
            toastr.error('Please Enter Time.');
            return false;
        }
        if($('#test-paper-max-marks').val() == "" || $('#test-paper-max-marks').val() <= "0"){
            toastr.error('Please enter maximum marks > 0');
            return false;
        }
        if($('#test-paper-neg-marks').val() != "" && $('#test-paper-neg-marks').val() < "0"){
            toastr.error('Please Enter negative marks > 0');
            return false;
        }   
    }
	return true;
}