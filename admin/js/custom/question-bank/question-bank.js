var limit = 0;
var page_no = 1;
var questionsTable;
var DataOfQuestions;
$(function() {
	fetchQuestions();
	setEventPrintQuestions();

});

function fetchQuestions() {
	var req = {};
	req.action = "fetchQuestions";

	req.limit = limit;
	if (Object.keys(getFilterData()).length > 2) {
		req = getFilterData();
	};

	$.ajax({
		type	:	'post',
		url	:	EndPoint,
		data	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		// console.log(res);
		html = fillQuestion(res.data);
		$('#questions-table tbody').html(html);
		createPagination(res.total_questions);
		setEventDeleteOfQuestion();
		setEventOnCheck();
		fetchTestPaperTypes();
		mathsjax();
		setEventCopyQuestion();
		setEventOnCreatePaperButton();
		setEventStatusOfMultipleQuestions();

		$('#total-questions').text(res.total_questions);
		$('#select-all').prop('checked', false).parent().removeClass('checked');
		Metronic.init();
		MathJax.Hub.Typeset();
		// setTimeout(function(){ questionsTable = $('#questions-table').dataTable();}, 5000);
	});
}

function fillQuestion(questions) {
	// console.log(questions);

	var html = '';
	var q = [];
	$.each(questions, function(x, question) {
		$('body').append('<div id="virtual-div"></div>');
		$('#virtual-div').html(question.question);
		dropDowns = $('#virtual-div select');
		$.each(dropDowns,function(x,dropDown) {
			$(dropDown).before('<input disabled>').remove();
		});
		virtualQuestion = $('#virtual-div').text();
		$('#virtual-div').remove();
		cleanText = question.question.replace(/<\/?[^>]+(>|$)/g, "");
		// html += '<tr data-q_id="'+question.id+'"><td><input type="checkbox" class="select-question"/></td><td>'+sn+'</td><td><div title="'+question.type+'">'+question.type+'</div></td><td>QU0'+(000+question.id)+'</td><td><div title="'+question.class+'">'+question.class+'</div></td><td><div title="'+question.subjects+'">'+question.subjects+'</div></td><td><div title="'+virtualQuestion+'">'+question.question+'</div></td><td><span class="label label-sm label-info">'+question.qc+'</span></td><td><div>'+question.modified+'</div></td><td><div><a href="question-details.php?question_id='+question.id+'" title="Edit"><i class="fa fa-pencil"></i></a> | <a  class="copy-question" title="Copy"><i class="fa fa-files-o"></i></a> | <a class="delete" style="color: rgb(230, 12, 12);font-weight: bold;" title="Delete"><i class="fa fa-trash-o"></i></a></div></td></tr>';
		html += '<tr data-q_id="'+question.id+'"><td><input type="checkbox" class="select-question"/></td><td>'+sn+'</td><td><div title="'+question.type+'">'+question.type+'</div></td><td>QU0'+(000+question.id)+'</td><td><div class="title">'+cleanText.slice(0,50)+'</div></td><td><span class="label label-sm label-info">'+question.qc+'</span></td><td><div>'+question.modified+'</div></td><td><div><a href="question-details.php?question_id='+question.id+'" title="Edit" target="_blank"><i class="fa fa-pencil"></i></a> | <a  class="copy-question" title="Copy"><i class="fa fa-files-o"></i></a> ';
		html += (role!=3)?'| <a class="delete" style="color: rgb(230, 12, 12);font-weight: bold;" title="Delete"><i class="fa fa-trash-o"></i></a>':'';
		html += '</div></td></tr>';
		sn++;
		q.push(question.id);
	});
	$.cookie('questions', q, { expires: 1 });
	return html;
}

function setEventOnCheck() {
	// set setting show multi actions when box is checked
	/*
	$('.select-question').off('change');
	$('.select-question').on('change', function(e) {
		if($('.select-question:checked').length > 0)
			$('#multiple-actions').show();
	})*/

	// set setting show multi actions when box is checked
	$('#select-all').off('change');
	$('#select-all').on('change', function(e) {
		if($('#select-all').prop('checked')) {
			$('.select-question').prop('checked',true).parent().addClass('checked');
		}
		else {
			$('.select-question').prop('checked',false).parent().removeClass('checked');
		}
	})
}


function setEventStatusOfMultipleQuestions() {
	$('#change-multiple-qc').off('change');
	$('#change-multiple-qc').on('change', function(e) {
		e.preventDefault();
		if($('.select-question:checked').length > 0) {
			var req = {};
			req.action = 'changeMultipleQC';
			req.quality_check = $('#change-multiple-qc').val();
			req.question_ids = getSelectedQuestions();

			$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 1){
					fetchQuestions();
					toastr.success(res.message);
				}
				else {
					toastr.error(res.message,'Error:');
				}
			});
		}
	});
	$('#delete-question').off('click');
	$('#delete-question').on('click', function(e) {
		e.preventDefault();
		if($('.select-question:checked').length > 0 && confirm("Are you sure you want to delete questions?")) {
			var req = {};
			req.action = 'deleteQuestions';
			req.question_ids = getSelectedQuestions();


			$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 1){
					fetchQuestions();
					toastr.success(res.message);
				}
				else {
					toastr.error(res.message,'Error:');
				}
			});
		}
		else{
			toastr.error('Please Select Paper:');
		}
	});
}

function getSelectedQuestions() {
	ids = [];
	$.each($('.select-question:checked'),function(i,checkbox){
		ids.push($(checkbox).parents('tr').data('q_id'));
	});
	return ids;
}

function setEventDeleteOfQuestion()
{
	$('.delete').off('click');
	$('.delete').on('click', function(e) {
		e.preventDefault();
		tr = $(this).parents('tr').eq(0);
		question_id = $(tr).data('q_id');
		if(confirm('Are you sure want to delete Question: QU00'+question_id)) {
			var req = {};
			req.action = 'deleteQuestion';
			req.question_id = question_id;
			$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 1){
					//fetchQuestions();
					window.location = 'question-bank.php';
					toastr.success(res.message);
				}
				else {
					toastr.error(res.message,'Error:');
				}
			});
		}
	});
}

/*
* function sets even to copy a question
*/
function setEventCopyQuestion()
{
	$('.copy-question').off('click');
	$('.copy-question').on('click', function() {
		question_id = $(this).parents('tr').eq(0).data('q_id');
		var req = {};
		req.action = 'copyQuestion';
		req.question_id = question_id;
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				window.location = 'question-details.php?question_id='+res.question_id;
			}
			else {
				toastr.error(res.message,'Error:');
			}
		});
	})
}


/*
* Pagination functions
*
*/
var sn = 1;
function createPagination(total_questions) {
	if (total_questions % 50 == 0) {
		pages = total_questions / 50;
	} else{
		pages = parseInt(total_questions / 50) + 1;
	};

	html = '';
	html += '<li><a class="page-number" value="1">&laquo; &laquo; </a></li>';
	html += '<li><a class="previous-page">&laquo; Prev </a></li>';
	// html += '<span class="page-numbers">';

	for (var i = 0; i < pages; i++) {
		html += '<li class=""><a class="page-numbers page-number" value="'+(i+1)+'">'+(i+1)+'</a></li>';
	};
	// html += '</span>';
	html += '<li><a class="next-page">Next &raquo;</a></li>';
	html += '<li><a class="page-number" value="'+pages+'">&raquo;&raquo;</a></li>';

	$('.question-table-pagination').html(html);

	$('.page-number').off('click');
	$(".page-number").on('click', function(e) {
		e.preventDefault();

		page_no = parseInt($(this).attr('value'));
		limit = parseInt(page_no - 1) * 50;

		sn = (limit == 0)?1:limit+1; // set serial number

		/* calling function to fetch data*/
		fetchQuestions();

	});

	// previous button functions
	$('.previous-page').off('click');
	$('.previous-page').on('click', function(e) {
		e.preventDefault();
		if(!($(this).parent().hasClass('disabled'))) {

			page_no = parseInt($('.question-table-pagination:eq(0)').find('.active a').attr('value'));

			$('.question-table-pagination:eq(0) li a[value="'+(page_no - 1)+'"]').click();
		}
	});

	// next button functions
	$('.next-page').off('click');
	$('.next-page').on('click', function(e) {
		e.preventDefault();
		if(!($(this).parent().hasClass('disabled'))) {

			page_no = parseInt($('.question-table-pagination:eq(0)').find('.active a').attr('value'));
			$('.question-table-pagination:eq(0) li a[value="'+(page_no + 1)+'"]').click();
		}
	});


	// disable previous button function
	if(page_no == 1) {
		$('.question-table-pagination li .previous-page').parent().addClass('disabled');
	}
	// disable next button function
	if(page_no == parseInt($('.question-table-pagination:eq(0) li').length - 4)) {
		$('.question-table-pagination li .next-page').parent().addClass('disabled');
	}

	// adding active class to show which page is active
	$('.question-table-pagination').find('.active').removeClass('active');
	$('.question-table-pagination li a[value="'+page_no+'"]').parent().addClass('active');
	$('.question-table-pagination li .page-numbers').parent().addClass('hidden');
	$('.question-table-pagination li .page-numbers[value="'+(page_no- 1)+'"], .page-numbers[value="'+page_no+'"], .page-numbers[value="'+(page_no+ 1)+'"]').parent().removeClass('hidden');

}