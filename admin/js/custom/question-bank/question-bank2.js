$(function() {
	fetchQuestionsForQBank();
});

function fetchQuestionsForQBank() {
	var req = {};
	req.action = "fetchQuestionsForQBank";
	$.ajax({
		type	:	'post',
		url	:	EndPoint,
		data	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		// console.log(res);
		fillQuestion(res.data);
		//$('#questions-table2 tbody').html(html);
		//fetchTestPaperTypes();
		
		$('#total-questions').text(res.total_questions);
		setEventDeleteOfQuestion();
		//$('#select-all').prop('checked', false).parent().removeClass('checked');
		Metronic.init();
		//MathJax.Hub.Typeset();
		// setTimeout(function(){ questionsTable = $('#questions-table').dataTable();}, 5000);
	});
}

function fillQuestion(questions) {
	var html = '';
	var sno = 1;
	$.each(questions, function(i, question) {
			html += '<tr data-q_id="'+question.id+'">'
			html += '<td>'+sno+'</td>'
			html += '<td>'+question.type+'</td>'
			html += '<td>QU0'+(000+question.id)+'</td>'
			html += '<td>'+question.class+'</td>'
			html += '<td>'+question.subjects+'</td>'

			html += '<td>'+question.question+'</td>'
			html += '<td><span class="label label-sm label-info">'+question.qc+'</span></td>'
			html += '<td>'+question.modified+'</td>'

			html += '<td class="action"> <div><a href="question-details.php?question_id='+question.id+'" title="Edit" target="_blank"><i class="fa fa-pencil"></i></a> | <a class="delete" style="color: rgb(230, 12, 12);font-weight: bold;" title="Delete"><i class="fa fa-trash-o"></i></a></div></td>';
			
			html += '</tr>'
			sno++;
	});
	
	$('#questions-table2 tbody').html(html);
	$('#questions-table2').dataTable({
	    "aoColumnDefs": [
	      { "iDataSort": 0, "aTargets": [2] }
	    ]
  });
}

function setEventDeleteOfQuestion()
{
	$('.delete').off('click');
	$('.delete').on('click', function(e) {
		e.preventDefault();
		tr = $(this).parents('tr').eq(0);
		question_id = $(tr).data('q_id');
		if(confirm('Are you sure want to delete Question: QU00'+question_id)) {
			var req = {};
			req.action = 'deleteQuestion';
			req.question_id = question_id;
			$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 1){
					//fetchQuestions();
					window.location = 'question-bank2.php';
					toastr.success(res.message);
				}
				else {
					toastr.error(res.message,'Error:');
				}
			});
		}
	});
}