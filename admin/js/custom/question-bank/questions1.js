// event load subjects when admin select a class
$('#classes').on('change', function(e) {
	$("#subjects").select2('val',null);
	$("#units").select2('val',null);
	$("#chapters").select2('val',null);
	$("#topics").select2('val',null);
	$("#subjects").html('');
	$("#units").html('');
	$("#chapters").html('');
	$("#topics").html('');
	fetchSubjects();
});

// event load units when admin select subjects
$('#subjects').select2().on('change', function(e) {
		$("#units").select2('val',null);
		$("#chapters").select2('val',null);
		$("#topics").select2('val',null);
		$("#units").html('');
		$("#chapters").html('');
		$("#topics").html('');
		fetchUnits();
});
	
// event load topics when admin select units
$('#units').select2().on('change', function(e) {
	$("#chapters").select2('val',null);
	$("#topics").select2('val',null);
	$("#chapters").html('');
	$("#topics").html('');
	fetchChapters();
});

// event load topics when admin select units
$('#chapters').select2().on('change', function(e) {
	$("#topics").select2('val',null);
	$("#topics").html('');
	fetchTopics();
});

// Event Fired When user changes Tags
$('#tags').select2().on('change', function(e) {
	tags = $("#tags").find('option:selected')
	id = [];
	$.each(tags, function(i,tag_id){
		id.push($(tag_id).data('id'));
	})
	tag_ids = id;
});

// Event Fired When user changes Difficulty-Level
$('#difficulty-levels').select2().on('change', function(e) {
	difficultyLevels = $("#difficulty-levels").find('option:selected')
	id = [];
	$.each(difficultyLevels, function(i,dl_id){
		id.push($(dl_id).data('id'));
	})
	difficultyLevel_ids = id;
});

// Event Fired When user changes Skills
$('#skills').select2().on('change', function(e) {
	skills = $("#skills").find('option:selected');
	id = [];
	$.each(skills, function(i,skill_id){
		id.push($(skill_id).data('id'));
	});
	skill_ids = id;
});

