var limit = 0;
var page_no = 1;
var filter = false;
$(function() {
	//_URL = window.URL || window.webkitURL;
	filterOrders();
	order = getUrlParameter("id");
	if(order){
		fetchOrderById(order);
	}
	else{

		fetchOrders();
		filterOrders();
		$('#dpd1').daterangepicker();
		$('#dpd2').daterangepicker();

		$('#dpd1').daterangepicker(
		{
		    locale: {
		      format: 'DD-MM-YYYY',
		      cancelLabel: 'Clear'
		    },
		    singleDatePicker: true,
	        showDropdowns: true,
	        startDate: '01-01-2016',
	    	endDate: '31-12-2017',
	    	autoUpdateInput: false
		});

		$('#dpd2').daterangepicker(
		{
		    locale: {
		      format: 'DD-MM-YYYY',
		      cancelLabel: 'Clear'
		    },
		    singleDatePicker: true,
	        showDropdowns: true,
		    startDate: '01-01-2016',
	    	endDate: '31-12-2017',
	    	autoUpdateInput: false
		});
	}
	

})

fetchOrders = function() {
	var req = {};
	req.action = "fetchOrders";
	//req.limit = limit;
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		//console.log(res);
		if (res.status == 1) {
			filter = false;
			fillDataInTable(res.data);
			createPagination(res.total_questions);
			$("#total-orders").html(res.total_questions);	
			
		} else{
			toastr.error(res.message, "Error:");
		};
	});
}

/**
* function to fill users Data In Table
*/
fillDataInTable = function(orders) {
	var html = '';
	$.each(orders, function(i, order) {
		html += '<tr data-id="'+order.id+'">';
		// var find = '-';
		// var re = new RegExp(find, 'g');
		// order_no = order.time.replace(re, '');
		
		
		html += '<td>' + order.id + '</td>';
		html += '<td data-oid="'+order.id+'">' + order.created + '</td>';
		html += '<td>' + order.first_name+' '+order.last_name+' ('+order.mobile+')</td>';
		html += '<td>' + order.name+'</td>';
		html += '<td>' + order.code+'</td>';
		
		_price = (order._price == null)?0:order._price;
		price  = (order.price == null) ?0:order.price;
		
		
		html += '<td>' + _price + '</td>';
		html += '<td>' + price + '</td>';
		html += '<td>' + order.status+ '</td>';

		
		html += '</tr>';
	});
	
	if (typeof oTable != "undefined") {
		oTable.fnDestroy();
	}

	$("#orders-table tbody").html(html);
	// $('#orders-table').DataTable();
	oTable = $('#orders-table').dataTable({

        "pageLength": 10,
       	"ordering": true,   
     
        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "columnDefs": [{ // set default column settings
            'orderable': false,
            'targets': 'no-sort'
        }, {
            "searchable": true,
            "targets": [0]
        },{
        	"targets": [0],
        	"data": [0],
        	"render": function ( data, type, full, meta ) {
        		if(type === 'display')
        		{
        			console.log(data);
        			
        			created = $('[data-oid='+data+']').html();

        			        			
        			created = moment(created, "YYYY-MM-DD HH:mm:ss").format("DDMMYY");
        		}
				return type === 'display' ? 'EXP/'+created+'/'+data : data;				
		    }
        }],

        "order": []
       /* "aoColumnDefs": [
	    {
			"aTargets": [ 0 ],
			"mDataProp": function ( source, type, val ) {
				console.log(source);
				console.log(type);
				console.log(val);

				if (type === 'set') {
					source[0] = val;
					// Store the computed display for speed
					// source.date_rendered = renderDate( val );
					return;
				}
				else if (type === 'display' || type === 'filter') 
				{
					return source.date_rendered;
				}
		        // 'sort' and 'type' both just use the raw data
		        return source[0];
	    	}
	    }]*/
    });
}


/*
*  Event to Reset form
*/
function resetFilterForm() {
	$("input:text").val("");
	
}

filterOrders = function() {
	$("#filters").off('click');
	$("#filters").on('click', function() {
		$("#order-filter-modal").modal("show");

	});
		

	$('#filter-form').off('submit');
	$('#filter-form').on('submit', function(e) {
		e.preventDefault();
		var req = {};
		
		req.action = "fetchOrders";
		//req.limit = limit;
		filter = true;

		if ($('#order-id').val() != "") {
			req.order_id = $('#order-id').val();
		};
		if ($('#name').val() != "") {
			req.name = $('#name').val();
		};
		if ($('#program-name').val() != "") {
			req.program_name = $('#program-name').val();
		};
		if ($('#dpd1').val() != "") {
			req.from = $('#dpd1').val();
		};
		if ($('#dpd2').val() != "") {
			req.to = $('#dpd2').val();
		};
		if ($('#price-from').val() != "") {
			req.price_from = $('#price-from').val();
		};
		if ($('#price-to').val() != "") {
			req.price_to = $('#price-to').val();
		};
		//alert(req);
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == 1)
			{	
				$("#total-orders").html(res.total_questions);	
				$("#order-filter-modal").modal("hide");
				fillDataInTable(res.data);
				//createPagination(res.total_questions);				
				
			}				
			else if(res.status == 0)
				toastr.error(res.message);
		});
		
	});
}


fetchOrderById = function(id) {

	var req = {};
	req.action = "fetchOrders";
	req.order_id = id;
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		//console.log(res);
		if (res.status == 1) {
			filter = false;
			fillDataInTable(res.data);
			//createPagination(res.total_questions);
			$("#total-orders").html(res.total_questions);	
			
		} else{
			toastr.error(res.message, "Error:");
		};
	});
}

/*
* Pagination functions
*
*/
var sn = 1;
function createPagination(total_questions) {
	if (total_questions % 50 == 0) {
		pages = total_questions / 50;
	} else{
		pages = parseInt(total_questions / 50) + 1;
	};

	html = '';
	html += '<li><a class="page-number" value="1">&laquo; &laquo; </a></li>';
	html += '<li><a class="previous-page">&laquo; Prev </a></li>';
	// html += '<span class="page-numbers">';

	for (var i = 0; i < pages; i++) {
		html += '<li class=""><a class="page-numbers page-number" value="'+(i+1)+'">'+(i+1)+'</a></li>';
	};
	// html += '</span>';
	html += '<li><a class="next-page">Next &raquo;</a></li>';
	html += '<li><a class="page-number" value="'+pages+'">&raquo;&raquo;</a></li>';

	$('.question-table-pagination').html(html);

	$('.page-number').off('click');
	$(".page-number").on('click', function(e) {
		e.preventDefault();

		page_no = parseInt($(this).attr('value'));
		limit = parseInt(page_no - 1) * 50;
		
		sn = (limit == 0)?1:limit+1; // set serial number
		
		/* calling function to fetch data*/
		if(filter){
			filterOrders();
		}else{
			fetchOrders();
		}
		

	});
	
	// previous button functions
	$('.previous-page').off('click');
	$('.previous-page').on('click', function(e) {
		e.preventDefault();
		if(!($(this).parent().hasClass('disabled'))) {

			page_no = parseInt($('.question-table-pagination:eq(0)').find('.active a').attr('value'));
			
			$('.question-table-pagination:eq(0) li a[value="'+(page_no - 1)+'"]').click();
		}
	});
	
	// next button functions
	$('.next-page').off('click');
	$('.next-page').on('click', function(e) {
		e.preventDefault();
		if(!($(this).parent().hasClass('disabled'))) {

			page_no = parseInt($('.question-table-pagination:eq(0)').find('.active a').attr('value'));
			$('.question-table-pagination:eq(0) li a[value="'+(page_no + 1)+'"]').click();
		}
	});
	

	// disable previous button function
	if(page_no == 1) {
		$('.question-table-pagination li .previous-page').parent().addClass('disabled');
	}
	// disable next button function
	if(page_no == parseInt($('.question-table-pagination:eq(0) li').length - 4)) {
		$('.question-table-pagination li .next-page').parent().addClass('disabled');
	}

	// adding active class to show which page is active
	$('.question-table-pagination').find('.active').removeClass('active');
	$('.question-table-pagination li a[value="'+page_no+'"]').parent().addClass('active');
	$('.question-table-pagination li .page-numbers').parent().addClass('hidden');
	$('.question-table-pagination li .page-numbers[value="'+(page_no- 1)+'"], .page-numbers[value="'+page_no+'"], .page-numbers[value="'+(page_no+ 1)+'"]').parent().removeClass('hidden');

}

var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()