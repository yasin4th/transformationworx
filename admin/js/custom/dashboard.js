$(function() {
	fetchUserDashboard();
	fetchReports();

})

fetchUserDashboard = function() {
	//alert("hi");
	req = {};
	req.action = 'fetchUserDashboard';
	
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		//console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1){
			$("#student-count").html(parseInt(res.total_students).commas());
			$("#active").html(parseInt(res.active).commas());
			$("#inactive").html(parseInt(res.inactive).commas());
			
			$("#question-count").html(parseInt(res.total_questions).commas());
			// $("#test-paper-count").html(res.total_test_paper);
			$("#order-count").html(parseInt(res.total_orders).commas());
			// $("#sale-count").html(res.total_orders);
			$("#revenue-count").html(parseInt(res.total_sale).commas());
			fillTopSelling(res.top_selling);
			fillNewStudents(res.new_students);
			fillLatestOrders(res.latest_orders);
			
		}
		else{
			//toastr.error('Error');
		}
	});	
}

fetchReports = function() {
	
	req = {};
	req.days = days;
	req.action = 'fetchReports';
	
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		//console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1){
			fillCharts(res.revenue,res.amount,res.days);
		}
		else{
			//toastr.error('Error');
		}
	});
	
}

// function to fill classes
function fillTopSelling(programs) {
	var html = '';

	$.each(programs, function(i,program) {
		
		html += '<tr>';
		html += '<td><a href="#">'+program.name+'</a></td>';
		html += '<td>'+parseInt(program.price).commas()+'</td>';
		html += '<td>'+parseInt(program.count).commas()+'</td>';
		html += '<td><a href="manage-order.php?id='+program.test_program_id+'" class="btn btn-default btn-xs"> <i class="fa fa-search"></i> View </a></td>';
		
	})
	$("#top-selling tbody").html(html);
}

// function to fill new students registered
function fillNewStudents(programs) {
	var html = '';

	$.each(programs, function(i,program) {
		
		html += '<tr>';
		html += '<td>STUDENT00'+program.id+'</td>'
		html += '<td><a href="#">'+program.first_name+' '+program.last_name+'</a></td>';
		html += '<td>'+program.registered_on+'</td>';		
		html += '<td><a href="edit-student.php?id='+program.id+'" class="btn btn-default btn-xs"> <i class="fa fa-search"></i> View </a></td>';
		
	})
	$("#new-students tbody").html(html);
}

function fillLatestOrders(programs) {
	var html = '';

	$.each(programs, function(i,program) {
		
		html += '<tr>';
		var find = '-';
		var re = new RegExp(find, 'g');
		order_no = program.time.replace(re, '');		
		html += '<td>'+order_no+'/' + program.id + '</td>';
		// html += '<td><a href="#">'+program.name+'</a></td>';
		html += '<td><a href="#">'+program.time+'</a></td>';
		html += '<td>'+parseInt(program.price).commas()+'</td>';
		// html += '<td>'+parseInt(program.price).commas()+'</td>';		
		html += '<td><a href="#" class="btn btn-default btn-xs"> <i class="fa fa-search"></i> View </a></td>';
		
	})
	$("#latest-orders tbody").html(html);
}

function fillCharts(revenue,amount,day) {
	var html = '';
	if(days == 0){
		$('.caption-helper').html("Monthly Status...");
		var x=[];
		var y=[];
		var x1=[];
		var y1=[];


		$.each(revenue.reverse(),function(i,rev){
			x.push(rev._month);
			y.push(parseInt(rev.count));
		});
		$.each(amount.reverse(),function(i,rev){
			x1.push(rev._month);
			y1.push(parseInt(rev.count));
		});
		
		
	}else if(days == 7 || days == 15){
		$('.caption-helper').html("Daily Status...");
		var x=[];
		var y=[];
		var x1=[];
		var y1=[];
		$.each(day.reverse(),function(j,d){
			flag = false;
			$.each(revenue.reverse(),function(i,rev){
				if(d.date == rev._realTime){
					x.push(rev._day+' '+rev._month);
					y.push(parseInt(rev.count));
					flag=true;
				}
			});
			$.each(amount.reverse(),function(i,rev){
				if(d.date == rev._realTime){
					x1.push(rev._day+' '+rev._month);
					y1.push(parseInt(rev.count));
					flag=true;
				}
			});
			if(flag == false){
				//x.push(rev._day+' '+rev._month);
				x.push(d.day);
				y.push(parseInt("0"));
				//x1.push(rev._day+' '+rev._month);
				x1.push(d.day);
				y1.push(parseInt("0"));
			}
		})
		
	}

	$('#chart1').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
       /* subtitle: {
            text: 'Source: WorldClimate.com'
        },*/
        xAxis: {
            categories: x
        },
        yAxis: {
            title: {
                text: 'Orders'
            },
            min: 0
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: 'Packages sold',
            data: y
        }]
    });

	


    $('#chart2').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
       /* subtitle: {
            text: 'Source: WorldClimate.com'
        },*/
        xAxis: {
            categories: x1
        },
        yAxis: {
            title: {
                text: 'Revenue'
            },
            min: 0
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: 'Revenue Earned',
            data: y1
        }]
    });
}
