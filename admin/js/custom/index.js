
$(function() {
	
});


function signIn(){
	if($('#username').val() != "" && $('#password').val() != "") {
		var req = {};
		req.email = $('#username').val();
		req.password = $('#password').val();
		req.action = 'adminLogin';
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				location.replace("dashboard.php");
			}
			else {
				$('.alert').html('<button class="close" data-close="alert"></button><span>'+res.message+'</span>');
				$('.alert').removeClass("display-hide");
				toastr.error(res.message,'Error:');
			}
		});
	}
}