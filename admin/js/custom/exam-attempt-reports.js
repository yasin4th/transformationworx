var limit = 0;
var page_no = 1;
var filter = false;


$(function() {
	fetchResultAndReport();
});

function fetchResultAndReport() {
	var req = {};
	req.action = 'fetchexamattemptreport';
	// req.limit=limit;
	$.ajax({
		type : "post", 
		url  : EndPoint,
		data : JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 1) {
			fillResultAndReport(res.data);	
			// createPagination(res.total_attempt);		
		} else {
			toastr.error(res.message);			
		};
	});

}

fillResultAndReport = function(students) {
	var html = '';
	
	$.each(students, function(i, student) {
		html += '<tr>';
		html += '<td> '+(i+1)+' </td>';
		html += '<td> '+student.first_name+'</td>';
		html += '<td> '+student.email+' </td>';
		html += '<td> '+student.start_time+' </td>';
		html += '<td> <a href="test-report.php?test='+student.test_paper_id+'"> '+student.name+'</a> </td>';
		html += '<td> '+student.total_questions+' </td>';
		html += '<td> '+student.attempted_questions+' </td>';
		html += '<td> '+student.marks_archived+' </td>';
		html += '</tr>';
	});
	if (typeof oTable != "undefined") {
		oTable.fnDestroy();
	}
	$('#examp-attempt-report-table tbody').html(html);
	oTable = $('#examp-attempt-report-table').dataTable({
		"fnDrawCallback": function( oSettings ) {

	   // or fnInfoCallback
	    }   
       
    });
}


/*
* Pagination functions
*
*/
var sn = 1;
function createPagination(total_questions) {
	if (total_questions % 50 == 0) {
		pages = total_questions / 50;
	} else{
		pages = parseInt(total_questions / 50) + 1;
	};

	html = '';
	html += '<li><a class="page-number" value="1">&laquo; &laquo; </a></li>';
	html += '<li><a class="previous-page">&laquo; Prev </a></li>';
	// html += '<span class="page-numbers">';

	for (var i = 0; i < pages; i++) {
		html += '<li class=""><a class="page-numbers page-number" value="'+(i+1)+'">'+(i+1)+'</a></li>';
	};
	// html += '</span>';
	html += '<li><a class="next-page">Next &raquo;</a></li>';
	html += '<li><a class="page-number" value="'+pages+'">&raquo;&raquo;</a></li>';

	$('.question-table-pagination').html(html);

	$('.page-number').off('click');
	$(".page-number").on('click', function(e) {
		e.preventDefault();

		page_no = parseInt($(this).attr('value'));
		limit = parseInt(page_no - 1) * 50;
		
		sn = (limit == 0)?1:limit+1; // set serial number
		
		/* calling function to fetch data*/
		
		fetchResultAndReport();
		
	});
	
	// previous button functions
	$('.previous-page').off('click');
	$('.previous-page').on('click', function(e) {
		e.preventDefault();
		if(!($(this).parent().hasClass('disabled'))) {

			page_no = parseInt($('.question-table-pagination:eq(0)').find('.active a').attr('value'));
			
			$('.question-table-pagination:eq(0) li a[value="'+(page_no - 1)+'"]').click();
		}
	});
	
	// next button functions
	$('.next-page').off('click');
	$('.next-page').on('click', function(e) {
		e.preventDefault();
		if(!($(this).parent().hasClass('disabled'))) {

			page_no = parseInt($('.question-table-pagination:eq(0)').find('.active a').attr('value'));
			$('.question-table-pagination:eq(0) li a[value="'+(page_no + 1)+'"]').click();
		}
	});
	

	// disable previous button function
	if(page_no == 1) {
		$('.question-table-pagination li .previous-page').parent().addClass('disabled');
	}
	// disable next button function
	if(page_no == parseInt($('.question-table-pagination:eq(0) li').length - 4)) {
		$('.question-table-pagination li .next-page').parent().addClass('disabled');
	}

	// adding active class to show which page is active
	$('.question-table-pagination').find('.active').removeClass('active');
	$('.question-table-pagination li a[value="'+page_no+'"]').parent().addClass('active');
	$('.question-table-pagination li .page-numbers').parent().addClass('hidden');
	$('.question-table-pagination li .page-numbers[value="'+(page_no- 1)+'"], .page-numbers[value="'+page_no+'"], .page-numbers[value="'+(page_no+ 1)+'"]').parent().removeClass('hidden');

}