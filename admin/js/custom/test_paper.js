currentDate = new Date();

$(function() {
	fetchTestPaper();
	setDateTimePicker();
	setEventOnTestPaperTypeChange();
	setEventSaveTestPaperDetails();
	fetchClasses();
	fetchTestpackage();
	setEventFilterTestPaper();
	fetchTestPaperTypes();
	mathsjax();
	setEventOnCheck();
	setEventOnAddtestpackage();

	CKEDITOR.config.customConfig = 'basic-toolbar-config.js';
	CKEDITOR.disableAutoInline = true;

	setEventWhenHidePaperDetails();
	$("#subjects").select2({placeholder: 'Select sebjects'})
	// event load subjects when admin select a class
	$('#test-paper-class').on('change', function(e) {
		$("#test-paper-subjects").select2('val',null);
		$("#test-paper-subjects").html('');
		var class_id = $(this).val();
		var target = $("#test-paper-subjects");
		fetchSubjects(class_id ,target);
	});
	
	$('#classes').on('change', function(e) {
		$("#subjects").select2('val',null);
		$("#subjects").html('');
		
		var class_id = $(this).val();
		var target = $("#subjects");
		fetchSubjects(class_id ,target);
	});
	$('#testpackage').select2({placeholder: 'Select Test Package'});
});
/*
* this function set event to show test package details in a model 
*/

	

/*
* this function set event to show test paper details in a model 
*/
function setViewDetailsEvent() {
	$('.view-details').off('click');
	$('.view-details').on('click', function(e) {
		e.preventDefault();
		
		var req = {};
		req.action = 'fetchTestPaperDetails';
		req.test_paper_id = $(this).parents('tr').eq(0).data('id');
		
		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);			

			$('#test-paper-details').modal({backdrop: 'static'});

			if(parseInt(res.data.type) == 5)
				$('#test-time').show();
			else
				$('#test-time').hide();

			$('#test-paper-subjects').select2('destroy')	
			$("#save-test-paper-modal").modal("show");
			$("#test-paper-instructions").attr('contenteditable',true);
		
			fillSubjects(res.subjectsList, $('#test-paper-subjects'));
			
			$('#test-paper-details').find('.name').val(res.data.name);
			$('#test-paper-id').val(res.data.id);
			$('#test-paper-name').val(res.data.name);
			$('#test-paper-type').val(res.data.type).change();
			$('#test-paper-class').val(res.data.class);
			$('#test-paper-subjects').val(res.data.subjects);
			$('#test-paper-time').val(res.data.time);
			$('#test-paper-attempts').val(res.data.total_attempts);			
			$('#test-paper-start').val(res.data.start_time);
			$('#test-paper-end').val(res.data.end_time);
			$('#test-paper-instructions').html(res.data.instructions);
			
			$('#test-paper-subjects').select2();
			setTimeout(function(){ CKEDITOR.inline("test-paper-instructions"); }, 1000); // from this line ck editor toolbar will not disable
			
		});
	});
}

function setEventWhenHidePaperDetails() {
	$('#test-paper-details').off('hidden.bs.modal');
	$('#test-paper-details').on('hidden.bs.modal', function () {
    	CKEDITOR.instances['test-paper-instructions'].destroy();
	});
}

/*
* this function fetch test papers to display
*/
function fetchTestPaper() {

	var req = {};
	req.action = "fetchTestPaper";
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		if(res.status == 1){
			fillTestPapers(res.data);
			setEventCopyTestPaper();
		}
		else if(res.status == 0)
			toastr.error(res.message);
	});
      setEventOnCreatePaperButton(); 
	
}

/*
* this funtion genrate html and display test paper
*/	
function fillTestPapers(testPaper) {
	var html = "";
	var pa = [];  // lakshman
	$.each(testPaper, function(index,paper) {
		html += '<tr data-id="'+paper.id+'" data-status="'+paper.status+'"><td><input type="checkbox" class="select-paper"/></td><td><div style="padding:5px;">'+paper.name+'</div></td><td><div style="padding:5px;">'+paper.type+'</div></td>';
		html += '<td><div style="padding:5px;">'+paper.questions+'</div></td>';
		html += '<td><div style="padding:5px;"> <a href="untitle-question-paper.php?test_paper='+paper.id+'&status='+paper.status+'">View/Edit Test Paper</a></div></td><td><div style="padding:5px;"> <a class="view-details" ="static">View/Edit Details</a></div></td><td> <div style="padding:5px; text-align:center;"><a  class="copy-test-paper" title="Copy"><i class="fa fa-files-o"></i></a> | <a class="delete" style="color: rgb(230, 12, 12);font-weight: bold; text-align:center;"><i class="fa fa-trash-o"></i></a></div> </td></tr>';
	    pa.push(paper.id);
	    $('#select-all').prop('checked', false).parent().removeClass('checked');
		Metronic.init();
		
	});
	if (typeof oTable != "undefined") {
		oTable.fnDestroy();
	}
	$('#test-paper-table tbody').html(html);

	setViewDetailsEvent();
	setEventDeleteTestPaper();
	oTable = $('#test-paper-table').dataTable();
    $.cookie('testPaper', pa, { expires: 1 });
	return html;
}



/*
* this function fetch types of test papers to display
*/
function fetchTestPaperTypes() {
	var req = {};
	req.action = "fetchTestPaperTypes";
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
			fillPaperTypes(res.data);
		else
			alert("ERROR:"+res.message)
	});
}

/*
* function sets event to copy a test paper
*/
function setEventCopyTestPaper()
{
	$('.copy-test-paper').off('click');
	$('.copy-test-paper').on('click', function() {
		test_paper_id = $(this).parents('tr').eq(0).data('id');
		var req = {};
		req.action = 'copyTestPaper';
		req.test_paper_id = test_paper_id;
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				toastr.success(res.message);
				fetchTestPaperWhenCopy();
			}
			else {
				toastr.error(res.message,'Error:');
			}
		});
	})
}


function fetchTestPaperWhenCopy() {
	var req = {};
	req.action = "fetchTestPaper";
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		if(res.status == 1){
			fillTestPapers(res.data);
			$('#test-paper-table tbody tr:eq(0) .view-details:eq(0)').click();
			setEventCopyTestPaper();
		}
		else if(res.status == 0)
			toastr.error(res.message);
	});
      setEventOnCreatePaperButton(); 
}


/*
* this function genrate html of test paper types and fill it
*/
function fillPaperTypes(types) {
	html = '<option value="0">Select Type</option>';
	$.each(types,function(x,type) {
		if(type.id != 1 && type.id !=3)
		html += '<option value="'+type.id+'">'+type.type+'</option>';
	});
	$('#test-paper-type').html(html);
	$('#paper-type').html(html);
}

/*
* funnction to show date picker when user select shedule test
*/
function setEventOnTestPaperTypeChange() {
	$('#test-paper-type').off('change');
	$('#test-paper-type').on('change', function(e) {
		e.preventDefault();
		if($('#test-paper-type').val() == 5) {
			$('#test-time').fadeIn();
		}
		else if($('#test-paper-type').val() == 6) {
			$('#test-time').fadeOut();
			$('#marks-time-div').hide('slow');
		}
		else{
			$('#marks-time-div').show('slow');
			$('#test-time').fadeOut();
		}
	});
}

/*
* this function set event to save test paper details
*/
function setEventSaveTestPaperDetails() {
	$('#save-test-paper').off('click');
	$('#save-test-paper').on('click', function(e){
		e.preventDefault();
		if(TestPaperDetailSaveValidator()){
			var req = {};
			req.action = "saveTestPaperDetails";
			req.id	=	$('#test-paper-id').val();
			req.test_paper_name = $('#test-paper-name').val();
			req.test_paper_type = $('#test-paper-type').val();
			req.test_paper_class = $('#test-paper-class').val();
			req.test_paper_subject = $('#test-paper-subjects').val();
			req.test_paper_time = $('#test-paper-time').val();
			req.test_paper_attempts = $('#test-paper-attempts').val();
			req.test_paper_start = $('#test-paper-start').val();
			req.test_paper_end = $('#test-paper-end').val();
			req.test_paper_instructions = CKEDITOR.instances['test-paper-instructions'].getData();
			
			$.ajax({
				type	: "post",
				url: EndPoint,
				data: JSON.stringify(req) 
				
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 1) {
					fetchTestPaper();
					toastr.success(res.message);
					$("#test-paper-details").modal("hide");
				}
				else {
					toastr.success(res.message);
				}

			});
			
		}
	});
}

function TestPaperDetailSaveValidator(){
	if($('#test-paper-name').val() == "" ){
		toastr.error('Please enter name.');
		return false;
	}

	if($('#test-paper-type').val() == "" ){
		toastr.error('Please select type.');
		return false;
	}

	if($('#test-paper-class').val() == "" ){
		toastr.error('Please select class.');
		return false;
	}

	if($('#test-paper-subjects').val() == null ){
		toastr.error('Please select subject.');
		return false;
	}
	if($('#test-paper-attempts').val() == ""){
		toastr.error('Please Enter Total Attempts.');
		return false;
	}
	if($('#test-paper-time').val() == "" || $('#test-paper-time').val() == "0"){
		toastr.error('Please Enter Time.');
		return false;
	}
	if($('#test-paper-max-marks').val() == "" || $('#test-paper-max-marks').val() == "0"){
		toastr.error('Please enter maximum marks.');
		return false;
	}
	if($('#test-paper-neg-marks').val() == "" || $('#test-paper-neg-marks').val() == "0"){
		toastr.error('Please Enter minimum marks.');
		return false;
	}
	return true;
}

/*
* set event to delete test paper
*/
function setEventDeleteTestPaper() {
	$('.delete').off('click');
	$('.delete').on('click', function(e){
		e.preventDefault();
		if(confirm("Are you sure want to delete this Paper?")) {
			var req = {};
			req.action = "deleteTestPaper";
			req.id	=	$(this).parents('tr').eq(0).data('id');
			$.ajax({
				type	: "post",
				url: EndPoint,
				data: JSON.stringify(req) 
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.success(res.message,'Test Paper');
					fetchTestPaper();
				}
			});
		}
	});
}


/*
* set date time picker
*/
function setDateTimePicker() {

    $('#test-paper-start').datetimepicker({
    	format: "dd MM yyyy - hh:ii",
    	startDate: currentDate,
    	showMeridian: true,
        autoclose: true,
        todayHighlight: true
    	// todayBtn: true
    });
    $('#test-paper-end').datetimepicker({
    	format: "dd MM yyyy - hh:ii",
    	startDate: currentDate,
    	showMeridian: true,
        autoclose: true,
        todayHighlight: true
    	// todayBtn: true
    });


    $("#test-paper-start").datetimepicker().on('changeDate', function (e) {
		$('#test-paper-end').datetimepicker('setStartDate', $(this).val());
    });
     $("#test-paper-end").datetimepicker().on('changeDate', function (e) {
		$('#test-paper-start').datetimepicker('setEndDate', $(this).val());
    });
}


// function to fetch classes
function fetchClasses() {
	req = {};
	req.action = 'fetchClasses';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillClasses(res.classes);
		}
		else{
			// toastr.error('Error');
		}
	});
	
}


// function to fill classes
function fillClasses(classes) {
	var html = "<option value='0'> Select Class </option>";
	$.each(classes,function( i,cls) {
		html += "<option value='"+cls.id+"' data-id="+cls.id+">" +cls.name+ "</option>";
	});
	$('#test-paper-class').html(html);
	$('#classes').html(html);
}

// function to fetch test package
function fetchTestpackage() {
	req = {};
	req.action = 'fetchTestpackage';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillTestpackage(res.testpackages);
		}
		else{
			// toastr.error('Error');
		}
	});
	
}

// function to fill test package
function fillTestpackage(testpackages) {
	var html = "";
	$.each(testpackages,function( i,pack) {
		html += "<option val='"+(i+1)+"' value='"+pack.id+"' data-id="+pack.id+">" +pack.name+ "</option>";
	});
	//$('#test-paper-class').html(html);
	$('#testpackage').html(html);
}


// function to fetch subjects
function fetchSubjects(class_id, target) {
	req = {};
	req.action = 'fetchSubjects';
	req.class_id = class_id;
	if(req.class_id) {
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				fillSubjects(res.subjects, target);
			}
			else{
				// toastr.error("Unknown class");
			}
		});
	}
}

// function to fill subjects
function fillSubjects(subjects, target) {
	var html = "";
	$.each(subjects,function( i,sub) {
		html += '<option val="'+(i+1)+'" value="'+sub.id+'" data-id="'+sub.id+'">'+sub.name+'</option>';
	});
	$(target).html(html);
}

function setEventFilterTestPaper() {
	$('#filter-form').off('submit');
	$('#filter-form').on('submit', function(e) {
		e.preventDefault();
		var req = {};

		
		req.action = "fetchTestPaper";
		if ($('#classes').val() != "0") {
			req.class = $('#classes').val();
		};

		if ($('#subjects').val() != null) {
			req.subjects = $('#subjects').val();
		};

		if ($('#paper-type').val() != "0") {
			req.type = $('#paper-type').val();
		};

		if ($('#test-paper').val() != "") {
			req.test_paper = $('#test-paper').val();
		};
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == 1)
				fillTestPapers(res.data);
			else if(res.status == 0)
				toastr.error(res.message);
		});
		
		$('#test-papers-filter-modal').modal('hide');
		
	});
}

function resetForm(){
	$('input,textarea').val('');
	$('select').val('0');
	fetchTestPaper();
	setDateTimePicker();
	setEventOnTestPaperTypeChange();
	setEventSaveTestPaperDetails();
	fetchClasses();
	setEventFilterTestPaper();
	fetchTestPaperTypes();
}

function setEventOnCheck() {
	$('#select-all').off('change');
	$('#select-all').on('change', function(e) {
		if($('#select-all').prop('checked')) {
			$('.select-paper').prop('checked',true).parent().addClass('checked');
		}
		else {
			$('.select-paper').prop('checked',false).parent().removeClass('checked');
		}
	})
}
function demo() {
	$('#delete-paper').off('click');
	$('#delete-question').on('click', function(e) {
		e.preventDefault();
		if($('.select-question:checked').length > 0 && confirm("Are you sure you want to delete questions?")) {
			var req = {};
			req.action = 'deleteQuestions';			
			req.question_ids = getSelectedPapers();
			
			
			$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 1){
					fetchQuestions();
					toastr.success(res.message);
				}
				else {
					toastr.error(res.message,'Error:');
				}
			});	
		}
		else{
			toastr.error('Please Select Paper:');
		}
	});
}	
function getSelectedPapers() {
	ids = [];
	$.each($('.select-paper:checked'),function(i,checkbox){
		ids.push($(checkbox).parents('tr').data('id'));
	});
	return ids;
}
function setEventOnCreatePaperButton() {
	$('#add-test-package-modal').off('click');
	$('#add-test-package-modal').on('click', function(e) {
		e.preventDefault();
		if($('.select-paper:checked').length > 0) {
			$('#addtestpackagemodal').modal("show");
		}
		else {
			toastr.info("No Paper Selected.");
		}
   	});
}
function setEventOnAddtestpackage() {
	$('#addTestPackage').off('click');
	$('#addTestPackage').on('click', function(e) {
		e.preventDefault();
		var testpackage = $('#testpackage').val();
		if(testpackage ==0){
			toastr.success("Please Select Atleast On Test Package");
		}
		else {
			addTestPackage();
		}
   	});
}

function addTestPackage(){
		var req = {};
		req.action = "saveTestPaperPackage";
		var testpapers = [];
		req.testpackage = $('#testpackage').val();
		req.parent_type = 1;
		checkboxes = $('#add-test-package-modal input:checkbox');
		if(req.testpackage ==0){
			toastr.success("Please Select Atleast On Test Package");
		}
		$.each(checkboxes, function(x, checkbox) {
			if($(checkbox).prop('checked'))
			{
				testpapers.push($(checkbox).parents('tr').data('id'));
			}
		});
		req.testpackagePapers = getSelectedPapers();
		$.ajax({
			type: "post",
			url: EndPoint,
			data:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1)
			{				
                toastr.success("Test Paper Add Successfully");
                $('#addtestpackagemodal').modal("hide");
			}
			else{
				toastr.error(res.message);
			}
		});
	
}
	
