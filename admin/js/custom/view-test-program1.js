_URL = window.URL || window.webkitURL;
UploadEndPoint = "../vendor/fileUploads.php";
test_program = getUrlParameter("test_program");
var batches;
$(function() {
	fetchBatches();
	fetchProgramDetails();
	fetchNumberOfAttachedTestPapers();
	setEventOfUpdateTestPrograme();
	setEventAddTestPaper();
	setEventShowTestProgramDetails();
	saveAttachedTestProgram();
	addStudyMaterial();
	setAjaxForm();
	showBatch();
	//setEvents();
	$('#test-program-class').on('change', function(e) {
		$("#test-program-subjects").select2('val',null);
		$("#test-program-subjects").html('');
		fetchSubjects();
		if($('#test-program-class').val() != 0){
			$("#batch").select2('val',null);
			fillBatches(batches,$('#test-program-class').val());
		}
		else
		{	$('#batch').select2('val',null);
		}
	});

	CKEDITOR.config.customConfig = 'basic-toolbar-config.js';

	CKEDITOR.disableAutoInline = true;
	
	
	$('.view-test').on('click', function(e){
		e.preventDefault();
		type = $(this).parents('.dashboard-stat').find('.add-test-paper').data('type');
		window.location = 'view-test-1.php?test_program='+test_program+'&type='+type+'&parent_id=0&parent_type=0';
	})
	
	$('#change-test-program-image').on('change', function(e){
		e.preventDefault();
		if($('#change-test-program-image')[0].files[0].size < 1048576) {
			var file, img;
		    if ((file = this.files[0]))
		    {
		        img = new Image();
		        img.onload = function () {
					
					$('#test-program-form').submit();

		           /* if(this.width <= 500 && this.height <= 500)
		            {
						$('#test-program-form').submit();
		            }
		            else
		            {
						toastr.error('Error: File Resolution Is Not  500 X 500 px.')
		            }*/
		        };
		        img.src = _URL.createObjectURL(file);
		    }
		}
		else
		{
			toastr.error('Error: File Size Larger Than 1 MB.')
		}
	})
	$('#program_id').val(test_program);
});

// function fetch Test Program Details Fromm DB
function fetchTestProgramDetails()
{
	var req = {};
	req.action = "fetchTestProgramDetails";
	req.test_program = test_program;

	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res)
	{
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			subjects = $.parseJSON('['+res.details.subjects+']');
			fillData(res.category,res.details.category);
			fillClasses(res.classesList);
			fillSubjects(res.subjectsList);
			//fillSubjectsToAddTest(res.subjectsList,subjects);
			
			$('.test-program').html(res.details.name);
			$('a.test-program').attr('href','view-test-program.php?test_program='+res.details.id);
			$('#test-program-name').val(res.details.name);
			$('#test-program-status').val(res.details.status);
			$('#test-program-type').val(res.details.type);
			if(res.details.type == 1 || res.details.type == 3 ){
				$('#test-program-type').change();
			}
			
			
			$('#test-program-class').val(res.details.class);
			$('#test-program-subjects').val(subjects).select2();
			$('#test-program-price').val(res.details.price);
			$("#cut-off").val(res.details.cut_off);
			$('#test-program-description').html(res.details.description);
			$("#test-program-image").attr('src', '../'+res.details.image);
			if($('#test-program-class').val() != 0){
				fillBatches(batches,$('#test-program-class').val());
			}

			if(res.program_batches.length != 0)
			{	

				$('#batch').val(res.program_batches).select2();
			}
			
		}
	});
}

function fillSubjectsToAddTest (subjectList,subjects) {
	html = '';
	$.each(subjectList, function(i,subject) {
		$.each(subjects, function(x,sub) {
			if(subject.id == sub) {
				html += '<div class="col-md-4 col-sm-6 col-xs-12"> <div class="form-group sub-box tooltips" data-container="body" data-placement="top" data-original-title="'+subject.name+'"> <div class="sub-name"> <a href="chapters-in-test-program.php?test_program='+test_program+'&subject='+subject.id+'" data-id="'+subject.id+'">'+subject.name+'</a> </div> </div> </div>';
			}
		})
				
	})
	$('#subjects-div').html(html);
	Metronic.init();
}


// function to fetch classes
function fetchClasses() {
	req = {};
	req.action = 'fetchClasses';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillClasses(res.classes);
		}
		else{
			toastr.error('Error');
		}
	});
	
}

// function to fill classes
function fillClasses(classes){
	var html = "<option value='0'> Select Class </option>";
	$.each(classes,function( i,cls) {
		html += "<option value='"+cls.id+"' data-id="+cls.id+">" +cls.name+ "</option>";
	});
	$('#test-program-classes').html(html);
}

function setEventOfUpdateTestPrograme() {
	$("#update_program").off('click');
	$("#update_program").on('click', function(e) {
		e.preventDefault();
		var req = {};
		req.id = test_program;
		req.action = "updateTestProgram";
		req.name = $('#test-program-name').val();
		req.status = $('#test-program-status').val();
		req.package_type = $('#test-program-type').val();
		req.class = $('#test-program-class').val();
		req.category = $('#test-program-category').val();
		req.subjects = $('#test-program-subjects').val();
		req.price = $('#test-program-price').val();
		req.cut_off = $("#cut-off").val();
		req.description = CKEDITOR.instances['test-program-description'].getData();

		if($('#test-program-type').val() == 2){
				req.package_type = $('#test-program-type').val();
		}
		else if($('#test-program-type').val() == 0){
			toastr.error("Please Select Package type.");
			return;
			
		}
		else{

			req.package_type = $('#test-program-type').val();
			if($('#batch').val() == 0){
				toastr.error("Please Select Batches.");
				return;
			}
			else{
				req.batch = $('#batch').select2('val');
			}
		}
		
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				toastr.success(res.message);
				fetchTestProgramDetails();
				$('#edit-test-program-modal').modal("hide");
				$('#test-program-form')[0].reset();
			}
			else{
				toastr.error('Error:'+res.message);
			}
		});
	});
}


function setEventShowTestProgramDetails() {
	$('#show-edit-test-program-modal').on('click', function(e){
		e.preventDefault();
		fetchNumberOfAttachedTestPapers();
		$('#edit-test-program-modal').modal("show");
		setTimeout(function(){ CKEDITOR.inline("test-program-description"); }, 1000);
	})
};

function setEventAddTestPaper() {
	$('.add-test-paper').on('click', function(e) {
		e.preventDefault();
		type = $(this).data('type');

		$('#test-paper-by-type-modal-table tbody').html('');
		$('#saveAttachedTestPaper').data('type',type)
		$('#test-paper-by-type-modal').modal("show");
		fetchTestPaperByType(type);
	})
}

function fetchTestPaperByType(type) {
	var req = {};
	req.action = "fetchTestPaperByType";
	req.type = type;
	req.class_id = $('#test-program-class').val();
	req.test_program = test_program;
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		fillTestPaperByType(res.test_papers);
	});

}

function fillTestPaperByType(paper) {
	html = "";
	$.each(paper, function(x, testPaper) {
		html += '<tr><td> '+testPaper.name+'</td><td><div class="checkbox-list"><label><input type="checkbox" data-id="'+testPaper.id+'"></label></div></td></tr>';
	});
	$('#test-paper-by-type-modal-table tbody').html(html);

}

function saveAttachedTestProgram() {
	$('#saveAttachedTestPaper').off('click');
	$('#saveAttachedTestPaper').on('click', function(e) {
		e.preventDefault();
		$('#saveAttachedTestPaper').attr('disabled',true);
		var req = {};
		req.action = "saveAttachedTestPaper";
		req.test_program = test_program;
		var papers = [];
		req.type = $(this).data('type');
		req.parent_id = 0;
		req.parent_type = 0;
		checkboxes = $('#test-paper-by-type-modal-table input:checkbox');
		$.each(checkboxes, function(x, checkbox) {
			if($(checkbox).prop('checked')) {
				papers.push($(checkbox).data('id'));
			}
		});
		req.testPapers = papers;
		$.ajax({
			type: "post",
			url: EndPoint,
			data:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1)
			{
				fetchNumberOfAttachedTestPapers();
				$('#test-paper-by-type-modal').modal("hide");
				$('#saveAttachedTestPaper').attr('disabled',false);
			}
		});
	});
}

function fetchNumberOfAttachedTestPapers() {
	var req = {};
	req.action = "fetchNumberOfAttachedTestPapers";
	req.test_program = test_program;
	req.parent_id = 0;
	req.parent_type = 0;
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		{			
			// console.log(res);
			if(res.data.diagnostic != undefined)
			{
				$('#diagnostic .count').text(res.data.diagnostic);			
			}
			if(res.data.fullSyllabus != undefined)
			{
				$('#full-syllabus-test .count').text(res.data.fullSyllabus);
			}
			if(res.data.scheduled != undefined)
			{
				$('#scheduled-test .count').text(res.data.scheduled);
			}	
		}
		else
		{
			toastr.error(res.message);
		}
	});
}

function fetchClasses()
{
	req = {};
	req.action = 'fetchClasses';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillClasses(res.classes);
		}
		else{
			toastr.error('Error');
		}
	});	
}

// function to fill classes
function fillClasses(classes)
{
	var html = "<option value='0'> Select Class </option>";
	$.each(classes,function( i,cls) {
		html += "<option value='"+cls.id+"' value='"+cls.id+"' data-id="+cls.id+">" +cls.name+ "</option>";
	});
	$('#test-program-class').html(html);
}

// function to fetch subjects
function fetchSubjects()
{
	req = {};
	req.action = 'fetchSubjects';
	req.class_id = $("#test-program-class").find('option:selected').data('id');
	if(req.class_id)
	{
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == 1)
			{
				fillSubjects(res.subjects);
			}
			else
			{
				$('#test-program-subjects').html('');
			}
		});
	}
}

// function to fill subjects
function fillSubjects(subjects)
{
	var html = "";
	$.each(subjects,function( i,sub) {
		html += '<option val="'+(i+1)+'" value="'+sub.id+'" data-id="'+sub.id+'">'+sub.name+'</option>';
	});
	$('#test-program-subjects').html(html);
}


function setAjaxForm()
{
	$('form#test-program-form').ajaxForm({
        beforeSend: function(arr, $form, data) {
            console.log('starting');
        },
		uploadProgress: function(event, position, total, percentComplete) {
			//Progress bar
			$('#percentage').show();
			$('#percentage').html(percentComplete + '% done.'); //update progressbar percent complete
        },
        success: function() {
            
        },
        complete: function(resp) {
        	$('#percentage').hide();
            res = $.parseJSON(resp.responseText);
            console.log(res);
            if(res.status == 1) {
				toastr.success('Image Updated.');
				$("#test-program-image").attr('src', '../'+res.image);
			}
			else
				toastr.error('An error occured. Please refresh the page and try again. Error Details: ' + res.message);
        },
		error: function() {
			console.log('Error');
        }
    });
}

function fetchProgramDetails()
{
	req = {};
	req.action = 'fetchProgramDetails';
	req.program_id = test_program;

	if(req.program_id)
	{
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == 1)
			{
				fillProgramTable(res.details);
			}
			else
			{
				
			}
		});
	}
}

fillProgramTable = function(subjects)
{
	var html = '';
	$.each(subjects, function(i,sub) {
		html += '<tr data-sub='+sub.subject_id+'>';
		html += '<td>' + sub.name + '</td>';


		// if(sub.chapter == 0)
		// {
		// 	html += '<td> <a href="#">' + sub.chapter + '</a></td>';			
		// }
		// else
		// {
		// 	html += '<td> <a href="view-test.php?test_program='+test_program+'&type=3&parent_id='+sub.subject_id+'&parent_type=1&subject='+sub.subject_id+'">' + sub.chapter + '</a></td>';
		// }


		if(sub.practice == 0)
		{
			html += '<td> <a href="#">' + sub.practice + '</a></td>';
		}
		else
		{
			html += '<td> <a href="view-test.php?test_program='+test_program+'&type=6&parent_id='+sub.subject_id+'&parent_type=1&subject='+sub.subject_id+'">' + sub.practice + '</a></td>';
		}

		// if(sub.diagnostic == 0)
		// {
		// 	html += '<td> <a href="#">' + sub.diagnostic + '</a></td>';
		// }
		// else
		// {
		// 	html += '<td> <a href="view-test.php?test_program='+test_program+'&type=1&parent_id='+sub.subject_id+'&parent_type=1&subject='+sub.subject_id+'">' + sub.diagnostic + '</a></td>';
		// }

		if(sub.mock == 0)
		{
			html += '<td> <a href="#">' + sub.mock + '</a></td>';
		}
		else
		{
			html += '<td> <a href="view-test.php?test_program='+test_program+'&type=4&parent_id='+sub.subject_id+'&parent_type=1&subject='+sub.subject_id+'">' + sub.mock + '</a></td>';
		}

		if(sub.scheduled == 0)
		{
			html += '<td> <a href="#">' + sub.scheduled + '</a></td>';
		}
		else
		{
			html += '<td> <a href="view-test.php?test_program='+test_program+'&type=5&parent_id='+sub.subject_id+'&parent_type=1&subject='+sub.subject_id+'">' + sub.scheduled + '</a></td>';
		}
		
		html += '<td> <a href="#qcr-illustration-modal" data-toggle="modal" id="show-study-material-modal">Add</a> | <a href="#qcr-illustration-listing-modal" data-toggle="modal" id="show-study-material-listing-modal">View</a> </td>';
		html += '<td> <a class="add-paper" href="" >Add</a> </td>';
		html += '</tr>';
	})
	$('#program-table tbody').html(html);
	clickAddStudyMaterial();
	listingStudyMaterial();
	setEvents();
}

setEvents = function()
{
	$('.add-paper').click(function(e) {
		e.preventDefault();
		sub = $(this).parent().parent().data('sub');
		$('#test-paper-modal').attr('data-subject',sub);
		$('#test-paper-modal').modal('show');
	})

	$('.delete-paper').click(function(e) {
		e.preventDefault();
		sub = $(this).parent().parent().data('sub');
		$('#test-paper-delete-modal').attr('data-subject',sub);
		$('#test-paper-delete-modal').modal('show');
	})
	
	$('#test-paper-type').on('change',function(e){
		e.preventDefault();
		if($(this).val() == 0)
		{
			toastr.info("Please select test type.");
			$('#test-paper-modal tbody').html('');
		}
		else
		{
			fetchPapers();
		}
	});

	$('#test-paper-delete-type').on('change',function(e){
		e.preventDefault();
		if($(this).val() == 0)
		{
			toastr.info("Please select test type.");
			$('#test-paper-delete-modal tbody').html('');
		}
		else
		{
			fetchDeletePapers();
		}
	});

	$('#saveAttachedPaper').off('click');
	$('#saveAttachedPaper').on('click', function(e) {
		e.preventDefault();
		$('#saveAttachedPaper').attr('disabled',true);
		var req = {};
		req.action = "saveAttachedTestPaper";
		req.test_program = test_program;
		var papers = [];
		req.type = $('#test-paper-type').val();
		req.parent_id = $('#test-paper-modal').attr('data-subject');
		req.parent_type = 1;
		checkboxes = $('#test-paper-modal table input:checkbox');
		$.each(checkboxes, function(x, checkbox) {
			if($(checkbox).prop('checked'))
			{
				papers.push($(checkbox).data('id'));
			}
		});
		req.testPapers = papers;
		$.ajax({
			type: "post",
			url: EndPoint,
			data:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1)
			{				
				$('#test-paper-modal').modal("hide");
				$('#saveAttachedPaper').attr('disabled',false);
				$('#testpapertable tbody').html('');
				fetchProgramDetails();
			}
		});
	});	
}

fetchPapers = function()
{
	req = {};
	req.action = 'fetchProgramPapers';
	req.test_program = test_program;
	//req.subject_id = $('#test-paper-modal').attr('data-subject');
	req.type = $('#test-paper-type').val();
	// if(req.test_program && req.subject_id)
	if(req.test_program)
	{
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == 1)
			{
				fillTableModal(res.test_papers);
			}
			else
			{
				$('#testpapertable tbody').html('');
			}
		});
	}
}

fillTableModal = function(papers)
{
	html = "";
	$.each(papers, function(x, testPaper) {
		html += '<tr><td> '+testPaper.name+'</td><td><div class="checkbox-list"><label><input type="checkbox" data-id="'+testPaper.id+'"></label></div></td></tr>';
	});
	$('#testpapertable tbody').html(html);
}


function fillData(data,category) {
	var html = '<option value="0"> Select Category </option>';
	$.each(data, function(i, difficulty){
		if(category == difficulty.id)
		{
			html += '<option value="' + difficulty.id + '" selected>' + difficulty.name + '</option>';
		}
		else
		{
			html += '<option value="' + difficulty.id + '" >' + difficulty.name + '</option>';
		}
	});
	$('#test-program-category').html(html);
		
}

function addStudyMaterial(){
	$('#test-program-illustration').off('submit');
	$('#test-program-illustration').on('submit', function(e){
		e.preventDefault();
		if($('#illustration-name').val() == "")
		{
			toastr.info("Please enter name of file.");
			return;
		}

	    if($('#illustration-file').val() != '') {
	      if ($('#illustration-file').val() != '' && $('#illustration-file')[0].files[0].size > 10240000) {
	        toastr.info("File is larger than 10 MB");
	        return false;
	      };
	      // console.log('qrc start');
	      $.ajax({
	        type  : 'post',
	        url : UploadEndPoint,
	        processData: false,
	        contentType: false,
	        data  : new FormData($('#test-program-illustration').get(0))
	      }).done(function(response){
	        response=$.parseJSON(response);
	        if (response.status == 1) {
	          // fetchComments();
	          toastr.success(response.message);
	          $('#test-program-illustration')[0].reset();
	          $('#qcr-illustration-modal').hide();
	        };
	      });
	    }
	    else {
	      toastr.warning("Please attach file.");
	    }

		
	});
	$('#illustration-file').on('change', function(e){
		$('#file-name').html('');
		if($('#illustration-file').val() != ''){
			if($('#illustration-file')[0].files[0].type == "application/pdf" || $('#illustration-file')[0].files[0].type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || $('#illustration-file')[0].files[0].type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
				$('#file-name').html($('#illustration-file')[0].files[0].name);
			}
			else{
				toastr.error("Please attach only pdf type of file.");
				$('#illustration-file').val('');
				return;
			}
		}
	});
}
function clickAddStudyMaterial(){
	$('#show-study-material-modal').off('click');
	$('#show-study-material-modal').on('click', function(){
		$("#chapter_id").val($(this).parents('tr').data('sub'));
		$('#test-program-illustration').find('#program_id').val(test_program);
	});
}
function listingStudyMaterial(){
	$('#show-study-material-listing-modal').off('click');
	$('#show-study-material-listing-modal').on('click', function(e){
		e.preventDefault();
		studyListing();
	});
}
function studyListing(){
	var req = {};
		req.action = "listingStudyMaterial";
		// req.subject_id = $(this).parents('tr').data('sub') ;
		req.program_id = test_program;
		$.ajax({
			type: "post",
			url: EndPoint,
			data:	JSON.stringify(req)
		}).done(function(res)
		{
			res = $.parseJSON(res);
			if(res.status == 1)
			{
				// console.log(res);
				fillStudyMaterialListing(res.study_material);
			}
		});
}
function fillStudyMaterialListing(data){
	var html = "<tr><td colspan ='4'>No Assignment Template</td></tr>";
	if(data.length == 0)
	{
		$('#listing-table tbody').html(html);
		return;
	}
		html="";
	$.each(data, function(i, study_material){
		html += "<tr data-id="+study_material.id+">";
		html += "<td>"+(i+1)+"</td>";
		html += "<td>"+study_material.subject_name+"</td>";
		html += "<td>"+study_material.name+"</td>";
		html += '<td> <a href="../'+study_material.source+'" target="_blank">View</a> | <a href="javascript:void(0)" class="delete-study-material-listing">Delete</a> </td>';
		html += "</tr>";
	});
	
	$('#listing-table tbody').html(html);
	deleteStudyMaterial();
}

function deleteStudyMaterial(){
	$('.delete-study-material-listing').off('click');
	$('.delete-study-material-listing').on('click', function(e){
		e.preventDefault();
		var req = {};
		req.action = "deleteStudyMaterial";
		req.study_material_id = $(this).parents('tr').data('id') ;
		console.log(req.study_material_id);
		$.ajax({
			type: "post",
			url: EndPoint,
			data:	JSON.stringify(req)
		}).done(function(res)
		{
			res = $.parseJSON(res);
			if(res.status == 1)
			{
				toastr.success(res.message);
				studyListing();
			}
		});
	});
}



function showBatch(){
	$('#test-program-type').off('change');
	$('#test-program-type').on('change',function(){
		if($('#test-program-type').val() == "1" || $('#test-program-type').val() == "3"){
			// html='<label class="col-md-3 control-label">Batch</label>\
			// 							<div class="col-md-9">\
			// 								<select id="batch" multiple="multiple" class="form-control" placeholder="Select Batches for the Test Package"></select>\
			// 							</div>';
			// $('#batch-frm-grp').html(html);
			$('#batch-frm-grp').removeClass('hidden');
			$('#batch').select2({placeholder: "Select Subjects"});
			if($('#test-program-type').val() == "1"){
			$('#price-frm-grp').addClass('hidden');
			}
			
			// $('#test-program-class').off('change');
			// $('#test-program-class').on('change',function(e){
			// 	e.preventDefault();
			// 	if($('#test-program-class').val() != 0){
			// 	fillBatches(batches,$('#test-program-class').val());
			// 	}
			// 	else
			// 	{	var html = "<option value='0'> Select Batch </option>";
			// 		$('#batch').html(html);
			// 	}
			// });

		
		}
		else{
			$('#batch-frm-grp').addClass('hidden');
			$('#price-frm-grp').removeClass('hidden');
		}
	});


}


fetchBatches = function(){
	req = {};
	req.action = 'fetchBatches';
	$.ajax({
		'type'	:	'post',
		'url'	:	'../vendor/api.php',
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			// fillBatches(res.batches);
			// console.log(res.batches);
			batches = res.batches;
			if($('#test-program-class').val() != 0){
				fillBatches(batches,$('#test-program-class').val());
				}
				fetchTestProgramDetails();
				// console.log(batches);
		}
		else{
			toastr.error('Error');
		}
	});
}

fillBatches = function(batches,cls){
	var html = "<option value='0'> Select Batch </option>";
	$.each(batches,function( i, batch) {
		if(batch.class_id == cls){
		html += "<option  value='"+batch.id+"' data-id="+batch.id+">" +batch.name+ "</option>";
		}
	});
	$('#batch').html(html);
}