// function validate(oTable, row) {
// 	name = $('#name').val();
// 	if(name.length > 1) {
// 		$('td:eq(0)', row).removeClass('has-error');
// 		id = $('#name').parents('tr').eq(0).attr('id');
// 		if(id == '' || id == undefined)
// 			addNew(oTable, row);
// 		else
// 			updateRow(oTable,row);
// 		return true;
// 	}
// 	else {
// 		$('td:eq(0)', row).addClass('has-error');
// 	}
// 		return false;
// }
	function validateAddBatch(){
		if($('#class').val() == 0)
		{
			toastr.error("Please select class.");
			return false;
		}
		if($('#batch').val() == "")
		{
			toastr.error("Please enter batch.");
			return false;
		}
		return true;
	}
	function addNew(oTable, row) {
		var req = {};
		req.action = 'addBatch';
		req.batch = name = $('#name').val();
		req.class = name = $('#desc').val();
		
		$.ajax({
			'type'	:	'post',
			'url'	:	'../vendor/api.php',
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				$(row).attr('id',res.id);
				saveRow(oTable, row);
			}
			else if(res.status == 0) {
				toastr.error(res.message);
			}
		});
	}

	function addBatch(){
		$('#add-btn').off('click');
		$('#add-btn').on('click',function(e){
			if($('#add-btn').html() == 'update'){
				var req = {};
				req.action = 'updateBatchRow';
				req.batch = name = $('#batch').val();
				req.class = name = $('#class').val();
				req.id= $('#add-batch-form').data('id');
				
				$.ajax({
					'type'	:	'post',
					'url'	:	'../vendor/api.php',
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1) {
						toastr.success(res.message);
						$('#add-batch-modal').modal('hide');
						$('#add-batch-form')[0].reset();
						$('#add-btn').html('Add');
						getBatchData();
					}
					else if(res.status == 0) {
						toastr.error(res.message);
					}
				});
				return;
			}
			e.preventDefault();
			if(validateAddBatch()){
				var req = {};
				req.action = 'addBatch';
				req.batch = name = $('#batch').val();
				req.class = name = $('#class').val();
				
				$.ajax({
					'type'	:	'post',
					'url'	:	'../vendor/api.php',
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1) {
						toastr.success(res.message);
						$('#add-batch-modal').modal('hide');
						$('#add-batch-form')[0].reset();
						getBatchData();
					}
					else if(res.status == 0) {
						toastr.error(res.message);
					}
				});
			}
		});

		
	}
	
	function fetchClasses() {
		req = {};
		req.action = 'fetchClasses';
		$.ajax({
			'type'	:	'post',
			'url'	:	'../vendor/api.php',
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fillClasses(res.classes);
			}
			else{
				toastr.error('Error');
			}
		});
	
	}

	function fillClasses(classes){
		var html = "<option value='0'> Select Class </option>";
		$.each(classes,function( i,cls) {
			html += "<option value='"+cls.id+"' data-id="+cls.id+">" +cls.name+ "</option>";
		});
		$('#class').html(html);
	}

	// function updateRow(oTable,row){

	// 	var req = {};
	// 	req.action = "updateTagRow";
	// 	req.id = $('#name').parents('tr').eq(0).attr('id');
	// 	req.tag = $('#name').val();
	// 	req.description = $('#desc').val();
		
	// 	$.ajax({
	// 		'type'	:	'post',
	// 		'url'	:	'../vendor/api.php',
	// 		'data'	:	JSON.stringify(req),
	// 	}).done(function(res){
	// 		res = $.parseJSON(res);
	// 		if(res.status == '1') {
	// 			saveRow(oTable, row);
	// 		}	
	// 		else {
	// 			toastr.error(res.message);
	// 		}
	// 	});	
	// }
	
	// function delRow(oTable,row){
	// 	var req = {};
	// 	req.action = "DeleteTagRow";
	// 	req.id = $(row).attr('id');

	// 	$.ajax({
	// 		'type'	:	'post',
	// 		'url'	:	'../vendor/api.php',
	// 		'data'	:	JSON.stringify(req)
	// 	}).done(function(res){
	// 		res = $.parseJSON(res);
	// 		if(res.status == 1) {
	// 			oTable.fnDeleteRow(row); // delete from html
	// 		}
			
	// 		else {
	// 			toastr.error(res.message);
	// 		}
			
	// 	});
	// }

	
	function getBatchData() {
		var req = {};
		// req.limit = 5;
		// req.offset = 0;
		req.action = "getBatchData";
		$.ajax({
			'type'	:	'post',
			'url'	:	'../vendor/api.php',
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			fillBatchData(res.data);
			
		});
	}
	function fillBatchData(data) {
		if(data.length == 0){
			var html = '<tr ><td colspan ="4"><span>No Batch Found</span</td></tr>';
			$('#sample_editable_1 tbody').html(html);
			return;
		}
		html ="";
		$.each(data, function(i, batch){ 
			html += '<tr id="' + batch.id + '" ><td id="row-class" data-id="'+batch.class_id+'">' + batch.class_name + '</td><td id="row-batch">' + batch.name + '</td><td><a class="edit"  href="javascript:;">Edit</a></td><td><a class="delete" href="javascript:;">Delete</a></td></tr>';		});
		$('#sample_editable_1 tbody').html(html);
			// TableEditable.init();
		deleteBatchRow();
		editBatchRow();
	}

	function deleteBatchRow() {
		$('.delete').off('click');
		$('.delete').on('click', function(e){
			e.preventDefault();
			var r = confirm("Are you sure to delete batch?");
			if(r== true){
				var req = {};
				req.action = "deleteBatchRow";
				req.id=$(this).parents('tr').attr('id');
				$.ajax({
					'type'	:	'post',
					'url'	:	'../vendor/api.php',
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					toastr.success(res.message);
					getBatchData();
					
				});
			}
			
		});
		
	}

	function editBatchRow() {
		var cls = "";
		var batch = "";
		$('.edit').off('click');
		$('.edit').on('click',function(e){
			e.preventDefault();
			cls = $(this).parents('tr').find('#row-class').data('id');
			batch = $(this).parents('tr').find('#row-batch').html();
			$('#class').val(cls);
			$('#batch').val(batch);
			$('#add-batch-form').attr('data-id',$(this).parents('tr').attr('id'));
			$('#add-btn').html('update');
			$('#add-batch-modal').modal('show');

		});
		
	}
	
$(function() {
	//  ();
	getBatchData();
	fetchClasses();


	addBatch();
	$('#add-batch-btn').off('click');
	$('#add-batch-btn').on('click',function(e){
		e.preventDefault();
		$('#add-batch-modal').modal('show');
	})
	
});