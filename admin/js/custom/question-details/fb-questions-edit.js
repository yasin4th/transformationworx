function fbLoadEditor() {
	setEventAddBlank();
	setEventDeleteFbOptions(); // function set events to delete on options
	
	fbQuestionEditor = CKEDITOR.inline('fb-question-box');
}

function setEventAddBlank() {
	// function to add option
	var i = 0;
	$('#fb-addBlank').off('click')
	$('#fb-addBlank').on('click', function(e) {
		e.preventDefault();
		if(parent_id != 0){
			i++;
			html = '<div class="row"> <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label class="index"></label> </div> </div> </div> <div class="col-md-10 form-group"> <input class=" form-control option" data-id="input'+(i+1)+'" placeholder="Option"/> </div>';
			html += '<div class="col-md-1"><a class="fb-deleteOption btn btn-circle btn-icon-only blue" data-id="input'+(i+1)+'"><i class="fa fa-trash-o"></i></a></div>';
			html += '</div></div>'
			
			divText = fbQuestionEditor.getData();
			$('#fb-question-box').html(divText.slice(0,-5)+'<input  class="input-droppable" disabled="true" data-id="input'+(i+1)+'" size="8"> </p>');
			$('#fb-options-div').append(html);
			//divText= "<input class=\"input-droppable\" disabled=\"true\" data-id=\"input"+(i+1)+"\" size=\"8\">";
			//console.log(divText);
			//fbQuestionEditor.insertHtml(divText.slice(0,-5)+divText='</p>','html');
			// var selection = e.editor.getSelection();
			// var range = selection.getRanges()[0];
			// var cursor_position = range.startOffset;
			setEventDeleteOptions();
		}else{
			i++;
			html = '<div class="row">';
			html += '<div class="col-md-12">';
			html += '<div class="col-md-1 form-group radio-option">';
			html += '<div class="input-group">';

			html += '<div class="icheck-list"> <label class="index"></label> </div>';

			html += '</div> </div>';
			html += '<div class="col-md-10 form-group"> <input class=" form-control option" data-id="input'+(i+1)+'" placeholder="Option"/> </div>';
			html += '<div class="col-md-1"><a class="fb-deleteOption btn btn-circle btn-icon-only blue" data-id="input'+(i+1)+'"><i class="fa fa-trash-o"></i></a></div>';
			html += '</div></div>'
			
			divText = questioneditor.getData();
			$('#question-box').html(divText.slice(0,-5)+'<input  class="input-droppable" disabled="true" data-id="input'+(i+1)+'" size="8"> </p>');
			$('#options').append(html);
			//divText= "<input class=\"input-droppable\" disabled=\"true\" data-id=\"input"+(i+1)+"\" size=\"8\">";
			//console.log(divText);
			//fbQuestionEditor.insertHtml(divText.slice(0,-5)+divText='</p>','html');
			// var selection = e.editor.getSelection();
			// var range = selection.getRanges()[0];
			// var cursor_position = range.startOffset;
			setEventDeleteOptions();
		}

	});
}
function getOptionsFb() {
	options = $("#fb-options-div .option");
	value = [];
	$.each(options, function(i,option) {
		choises = {};
		//choises.option = $(option).html();
		choises.option = $(option).val();
		choises.answer = 1;
		value.push(choises);
	});
	option_values = value;
	return value;
}

// this function save fb type question
function saveFb() {
	var req = getFB_data();
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			//resetPageValues();
			toastr.success(res.message,'Question Id: 00'+res.questionId);
		}
		else {
			toastr.error(res.message,'Error: Question Not Saved');
		}
	});
}

function getFB_data() {
	var req = {};
	req.action = "addQuestion";
	req.questionData = fbQuestionEditor.getData();
	req.DescriptionData = "No Description.";
	if(hintEditor != undefined)
		req.DescriptionData = hintEditor.getData();
	req.parent_id = parent_id;
	req.questionType = 6;
	req.options = getOptionsFb();
	req.class_id = $("#classes").find('option:selected').data('id');
	req.subject_ids = getSelectedSubjectId();
	req.unit_ids = getSelectedUnitId();
	req.chapter_ids = getSelectedChapterId();
	req.topic_ids = getSelectedTopicId();
	req.tag_ids = getSelectedTagId();
	req.difficultyLevel_ids = getSelectedDifficultyLevelId();
	req.skill_ids = getSelectedskillId();
	return req;
}