function fillOptions(type,options) {
	switch(type) {
		case '1':
			var html = '';
			$.each(options ,function(i,option) {
				html += '<div class="row">';
				html += '<div class="col-md-12">';
				html += '<div class="col-md-1 form-group radio-option">';
				html += '<div class="input-group">';
				html += '<div class="radio-list">';
				html += '<label>'
				if(option.answer == 1) {
					html += '<input type="radio" name="radio1" checked="checked">';
				}
				else {
					html += '<input type="radio" name="radio1">';
				}

				html += '</label></div></div></div>';

				html += '<div class="col-md-10 form-group">';
				html += '<div contentEditable="true" class="option" name="option" id="option'+i+'" data-id="'+option.id+'" style="border: 1px solid rgb(232, 227, 245); min-height: 30px;">'+option.option+'</div>';
				html += '</div>';

				html += '<div class="col-md-1">';
				html += '<a class="delete-option btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a>';
				html += '</div>';

				html += '</div></div>';
			});
			$('#options-box').append('<div class="col-md-12"> <a id="addOption" class="btn btn-circle btn-icon-only blue pull-right add-option"> <i class="fa fa-plus"></i> </a> </div>');
			return html;
			break;

		case '2':
			var html = '';
			$.each(options ,function(i,option) {
				html += '<div class="row"><div class="col-md-12"><div class="col-md-1 form-group radio-option"><div class="input-group"><div class="checkbox-list"><label>';

				if(option.answer == 1) {
					html += '<input type="checkbox" checked="checked">';
				}
				else {
					html += '<input type="checkbox" >';
				}

				html += '</label></div></div></div><div class="col-md-10 form-group"><div contentEditable="true" class="option" name="option" id="option'+i+'" data-id="'+option.id+'" style="border: 1px solid rgb(232, 227, 245); min-height: 30px;">'+option.option+'</div></div><div class="col-md-1"><a class="delete-option btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div></div></div>';

			});

			$('#options-box').append('<div class="col-md-12"> <a id="addOption" class="btn btn-circle btn-icon-only blue pull-right add-option"> <i class="fa fa-plus"></i> </a> </div>');
			return html;
			break;

		case '3':
			var html = '';
			$.each(options ,function(i,option) {
				html += '<div class="row"> <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="checkbox-list"> <label>';

				if(option.answer == 1) {
					html += '<input type="radio" name="radio1" checked="checked">';
				}
				else {
					html += '<input type="radio" name="radio1">';
				}

				html += '</label> </div> </div> </div> <div class="col-md-10 form-group"> <div class="option" name="option" data-id="'+option.id+'" style="border: 1px solid rgb(232, 227, 245); min-height: 30px;padding: 7px;width: 50%;">'+option.option+'</div> </div> <div class="col-md-1"></div> </div></div>';
			});

			return html;
			break;

		case '4':
			var html = '';
			$.each(options ,function(i,option) {
				html += '<div class="row"> <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label class="index"></label> </div> </div> </div> <div class="col-md-10 form-group"> <div contentEditable="true" class="option" name="ord-option'+i+'" data-id="'+option.id+'" id="option'+i+'" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;">'+option.option+'</div> </div> <div class="col-md-1"><a class="delete-option btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div> </div></div>';
			});

			$('#options-box').append('<div class="col-md-12"> <a id="addOption" class="btn btn-circle btn-icon-only blue pull-right add-option"> <i class="fa fa-plus"></i> </a> </div>');
			return html;
			break;

		case '5':
			var html = '';
			var firstCol = [];
			var secondCol = [];
			var extraOptions = [];

			$.each(options ,function(i,option) {
				if(option.number == "1") {
					firstCol.push(option);
				}
				else if(option.number == "2" && option.answer == "1"){
					secondCol.push(option);
				}
				else {
					extraOptions.push(option);
				}
			});

			$.each(secondCol ,function(i,option) {
				if(firstCol[i] != undefined) {

					html += '<div class="options row"> <div class="col-md-12"> <div class="col-md-11 col-xs-10"> <div class="row"> <div class="col-md-6 col-xs-6 form-group"> <div id="match-option'+firstCol[i].id+'" contentEditable="true" class="option" data-id="'+firstCol[i].id+'" data-column="1" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;">'+firstCol[i].option+'</div> </div> <div class="col-md-6 col-xs-6 form-group"> <div id="match-option'+option.id+'" contentEditable="true" class="option" data-id="'+option.id+'" data-column="2" placeholder="Option 3" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;">'+option.option+'</div> </div> </div> </div> <div class="col-md-1 col-xs-2"> <a class="match-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div></div>';
				}
				// else {
				// 	html += '<div class="options row"> <div class="col-md-12"> <div class="col-md-11 col-xs-10"> <div class="row"> <div class="col-md-6 col-xs-6 form-group"></div> <div class="col-md-6 col-xs-6 form-group"> <div contentEditable="true" class="option" id="match-option'+option.id+'" data-id="'+option.id+'" data-column="2" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;">'+option.option+'</div> </div> </div> </div> <div class="col-md-1 col-xs-2"> <a class="match-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div></div>';
				// }
			});

			$.each(extraOptions ,function(i,option) {
					html += '<div class="options row"> <div class="col-md-12"> <div class="col-md-11 col-xs-10"> <div class="row"> <div class="col-md-6 col-xs-6 form-group"></div> <div class="col-md-6 col-xs-6 form-group"> <div contentEditable="true" class="option" id="match-option'+option.id+'" data-id="'+option.id+'" data-column="3" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;">'+option.option+'</div> </div> </div> </div> <div class="col-md-1 col-xs-2"> <a class="match-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div></div>';
			});

			$("#options").after('<div class=""> <div class="col-md-12"> <div class="col-md-11 col-xs-10"> <div class="row"> <div class="col-md-6 col-xs-6 form-group">  </div> <div class="col-md-6 col-xs-6 form-group"> <a id="addPairOptions"> + Add Pair Options </a> </div> </div> </div> <div class="col-md-1 col-xs-2"> </div> </div></div>');

			$('#options-box').append('<div class="col-md-12"> <a id="addOption" class="btn btn-circle btn-icon-only blue pull-right add-option"> <i class="fa fa-plus"></i> </a> </div>');
			return html;
			break;

		case '6':
			var html = '';
			blanks = $('#question-box').find('input:text');
			$.each(options ,function(i,option) {
				if(option.answer == 1) {
					html += '<div class="row"> ';
					html += '<div class="col-md-12">';
					html += '<div class="col-md-1 form-group radio-option">';
					html += '<div class="input-group">';

					html += '<div class="icheck-list"> <label class="index"></label> </div>';

					html += '</div>';
					html += '</div>';

					html += '<div class="col-md-10 form-group"> <input type="text" class="option form-control" name="ord-option'+i+'" data-id="'+$(blanks).eq(i).data("id")+'" id="option'+i+'" value="'+option.option+' "/> </div>';

					html += '<div class="col-md-1"><a class="fb-deleteOption btn btn-circle btn-icon-only blue" data-id="'+$(blanks).eq(i).data("id")+'"><i class="fa fa-trash-o"></i></a></div>';

					html += '</div>';
					html += '</div>';
				}
			});

			//$('#options').append('<div class="col-md-12"> <a id="addOption" class="btn btn-circle btn-icon-only blue pull-right fb-deleteOption"> <i class="fa fa-trash-o"></i> </a> </div>');
			return html;
			break;

		case '7':
			var html = '';

			blanks = $('#question-box').find('select');
			//radioButtonName = blanks.length;
			for (var x = 1; x <= options[options.length - 1].number; x++) {
				radioButtonName = parseInt(Math.random() * 1000);

				html += '<div id="fb-dd'+ x +'" class="option" data-id="'+$(blanks).eq(x-1).data("id")+'"> <label class="mrg-left10"> Drop Down '+ x +' Options </label>';

				$.each(options ,function(i,option) {
					if (option.number == x) {
						html += '<div class="row value dd-option">';
						html += '<div class="col-md-11 col-xs-11">';
						html += '<div class="col-md-1 col-xs-2 form-group radio-option">';
						html += '<div class="input-group">';
						html += '<div class="icheck-list"><label>';
						//html += '<input type="radio" name="radioOptionDrop3" data-answer="1">';
						//html += ' <div class="col-md-12 dd-option"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> ';

						/*
						* checking that option's answer is truue or false
						*/
						if (option.answer == 1) {
							html += '<input type="radio" name="'+radioButtonName+'" checked data-answer="1">';
						} else{
							html += '<input type="radio" name="'+radioButtonName+'" >';
						}

						html += '</lable></div>';
						html += '</div></div>';
						html += '<div class="col-md-10 col-xs-8 form-group">'
						html += '<input name="ord-option'+i+'" data-id="'+option.id+'" id="option'+i+'" type="text" class="form-control" placeholder="Option" value="'+option.option+'">';
						html += '</div>';
						html += '<div class="col-md-1 col-xs-2"><a class="fb-dd-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div>';
						html += '</div><div class="col-md-1 col-xs-1"></div></div>';
					}
						//html += ' </div> </div> </div> <div class="options col-md-10 form-group"> <input class="form-control" name="ord-option'+i+'" data-id="'+option.id+'" id="option'+i+'" value="'+option.option+'"/> </div> <div class="col-md-1"><a class="fb-dd-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div> </div>';

				});

					html += '<div class="row">';
		        		html += '<div class="col-md-12">';
			        		html += '<div class="col-md-1 form-group radio-option">';
				        		html += '<div class="input-group">';
				        			html += '<div class="icheck-list"> </div>';
				        		html += '</div>';
			        		html += '</div>';
		        			html += '<div class="col-md-10 form-group"> <a class="fb-dd-addOption" data-name="'+radioButtonName+'"> + Add More Option </a> </div>';
		        		html += '</div>';
	    			html += '</div>';
	    			radioButtonName++;
				/*html += ' <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> </div> </div> </div> <div class="col-md-10 form-group"> <a class="fb-dd-addOption"> + Add More Option </a> </div> <div class="col-md-1"></div> </div>';
				html += ' </div> ';

				html += ' </div> ';*/
			};
			html += '</div>';

			//$('#options-box').append('<div class="col-md-12"> <a id="addOption" class="btn btn-circle btn-icon-only blue pull-right add-option"> <i class="fa fa-plus"></i> </a> </div>');
			//console.log(html);
			return html;
			break;



/*<div class="row">
	<div class="col-md-12">
		<div class="col-md-1 form-group radio-option">
			<div class="input-group">
				<div class="icheck-list">
					<label class="index"></label>
				</div>
			</div>
		</div>
		<div class="col-md-10 form-group"> <input class=" form-control option" data-id="input3" placeholder="Option"> </div>
		<div class="col-md-1"><a class="fb-deleteOption btn btn-circle btn-icon-only blue" data-id="input3"><i class="fa fa-trash-o"></i></a></div>
	</div>
</div>*/
		case '8':
			var html = '';
			html += '<div class="options">';
			blanks = $('#question-box').find('input:text');
			$.each(options ,function(i,option) {
				if(option.answer == 1) {
					html += '<div class="row">';
					html += '<div class="col-md-12">';
					html += '<div class="col-md-1 form-group radio-option">';
					html += '<div class="input-group">';
					html += '<div class="icheck-list">';
					html += '<label class="index"></label>';
					html += '</div> </div> </div>';
					html += '<div class="col-md-10 form-group">';
					html += '<input type="text" class="option form-control" name="ord-option'+counter+'" data-id="'+$(blanks).eq(i).data("id")+'" id="option'+counter+'" value="'+option.option+' "/>'
					html += '</div>';
					html += '<div class="col-md-1"><a class="fb-deleteOption btn btn-circle btn-icon-only blue" data-id="'+$(blanks).eq(i).data("id")+'"><i class="fa fa-trash-o"></i></a></div>';
					html += '</div></div>';
					counter++;
				}
			});
			html += '</div>';

			html += '<div id="extra-options">';
			$.each(options ,function(i,option) {
				if(option.answer == 0) {
					html += '<div class="row">';
					html += '<div class="col-md-12">';
					html += '<div class="col-md-1 form-group radio-option">';
					html += '<div class="input-group">';
					html += '<div class="icheck-list">';
					html += '<label class="index"></label>';
					html += '</div>';
					html += '</div></div>';
					html += '<div class="col-md-10 form-group">';
					html += '<input type="text" class="option form-control" name="ord-option'+counter+'" data-id="'+option.id+'" id="option'+counter+'" value="'+option.option+'" style="background-color: aliceblue;"/>'
					html += '</div>';
					html += '<div class="col-md-1"><a class="fb-dnd-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div>';
					html += '</div></div>';
					counter++;
				}
			});
			html += '</div>';

			//$('#options-box').append('<div class="col-md-12"> <a id="addOption" class="btn btn-circle btn-icon-only blue pull-right add-option"> <i class="fa fa-plus"></i> </a> </div>');
			return html;
			break;

		case '11':
			var req = {};
			req.action = 'getQuestionsOfPassage';
			req.id = getUrlParameter('question_id');
			$.ajax({
				type: "post",
				url: EndPoint,
				data: JSON.stringify(req)
			}).done(function(res){
				res = $.parseJSON(res);
				var html = '<a class="add-question" >Add Question</a>';
				html += ' <div id="passbase-questions-div" class="row" style="display:none;"> ';
				html += '<div class="passage-question col-md-12 col-xs-12"> ';

				html += ' <div class="col-md-8 form-group"> <label class="control-label"><span class="required">* </span>Select Question Type</label> <select class="passage-question-types form-control"> <option value="0" data-id="0"> Select Question Type</option> <option value="1" data-id="1"> Single Choice Questions </option> <option value="2" data-id="2"> Multiple Choice Questions </option> <option value="3" data-id="3"> True/False </option> <option value="6" data-id="6"> Fill In The Blanks </option>  <option value="7" data-id="7"> Fill In The Blanks With Drop-Down Options </option> <option value="8" data-id="8"> Fill In The Blanks With Drag and Drop Options </option>  </select> </div>';

				html += ' <div class="col-md-3"></div>';
				html += ' <div class="col-md-12 col-xs-12" style="border:1px solid rgb(232, 227, 245);">';
				html += ' <div class="selected-passage-question col-md-12 col-xs-12" > <!-- Question Type Area --></div> ';
				html += ' <div class="col-lg-12"><a class="btn_AddQuestion btn green pull-right" style="margin-top: 15px; /* color:#2C2CEB */"><i class="fa fa-plus"></i> Save And Add Question </a></div>';
				html += ' </div> ';
				html += ' </div> ';
				html += ' </div>';


				$.each(res.data,function(x,Question) {
					html += '';
					html += ' <div class="question-view">';
					html += ' <div class="form-group">';
					html += ' <label class="control-label">';
					html += ' <h4>Question '+(x+1)+'</h4> </label> <a class="pull-right delete-sub-question" data-id="'+Question.question.id+'"> <i class="fa fa-trash fa-2x" style="margin: 5px;"></i> </a>';
					html += ' <a href="question-details.php?question_id='+Question.question.id+'" target="_blank" class="pull-right"> <i  style="margin: 5px;" class="fa fa-pencil fa-2x"></i> </a>';
					html += ' <div class="col-md-12"> <div class="question-box" style="border: 1px solid rgb(232, 227, 245); min-height: 70px;">'+Question.question.question+'</div> <div class="options-box row margin-top-10"> <div class="options col-md-11">';
					//console.log(Question.question.type);

					html += getHtmlOfOptionsInPassage(Question.question.type,Question.options);
					html += '</div> <div class="col-md-1"></div> </div> </div> </div> <div class="form-group"> <label class="control-label"> <h4>Solution/Explanation</h4> </label> <div class="col-md-12 form-group"> <div class="description-box" style="border: 1px solid rgb(232, 227, 245); min-height: 70px;">'+Question.question.description+'</div> </div> </div></div>';

				});
				$("#questions-of-passage").html(html);
				// addPassageQuestion();
				$("#questions-of-passage input").attr('disabled',true);
				$("#questions-of-passage").removeClass('hide');
				$('#question-box').parents('.editable-question-view').find('label:eq(0)').html('<h4>Passage</h4>');
				$("#description-box").addClass('hidden');
				addPassageQuestion();
				setEventDeleteSubQuestion();
			});

			break;


		case '12':
			var html = '';

			/*
			* creating table head
			*/
			html += '<div class="row margin-top-10"> <div class="col-md-1"></div> <div id="matrix-options-div" class="col-md-11 matrix-table-options"> <div class="margin-bottom-10 table-scrollable">';
			html += '<table id="matrix-table" class="table table-bordered table-striped table-hover">';
			var columns = -1;
			html += '<thead><tr><th>  <div class="col-md-1"></div> </th>';
			$.each(options ,function(i,option) {
				if(option.answer == 1) {
					html += '<th > <div id="" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"> '+option.option+' </div> </th>';
					columns++;
				}
			});
			html += '<th><a id="matrix-add-column" class="btn btn-circle btn-icon-only blue"><i class="fa fa-plus-circle"></i></a><!--<button class="btn green"><i class="fa fa-plus-circle"></i> Add column</button>--></th></tr></thead>';

			/*
			* creating table foot
			*/
			html += ' <tfoot> <tr><td><a id="matrix-add-row" class="btn btn-circle btn-icon-only blue"><i class="fa fa-plus-circle"></i></a></td>';
			for(x = columns; x >= 0; x--) {
				html += '<td><a class="matrix-delete-column matrix-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a></td>';
			}
			html += '<td></td></tr> </tfoot>';

			/*
			* creating table body
			*/
			var row = 2;
			html += '<tbody>'
			$.each(options ,function(i,option) {
				if(option.answer == row) {
					html += '<tr> <td style="max-width: 100px;"> <div id="" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;">'+option.option+'</div></td>';
					option.answer = 0;
					$.each(options ,function(x,opt) {
						if(opt.answer == row) {
							if(opt.option == 1) {
								html += '<td><div class="checkbox-list"><label><input type="checkbox" checked="true"></label></div></td>';
							}
							else {
								html += '<td><div class="checkbox-list"><label><input type="checkbox"></label></div></td>';
							}
						}
					});
					html += '<td> <a class="matrix-delete-row matrix-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a></td> </tr>';
					row++;
				}
			});
			html += '</tbody></table></div> </div> </div>'

			return html;
			break;

		case '13':
			var from="";
			var to ="";
			$.each(options ,function(i,option) {

				if(option.number == 1) {
					from = option.option;
				}
				if(option.number == 2) {
					to = option.option;
				}
			});

			var html = '<div class="col-md-12 col-xs-12"> <div class="col-md-6"> <lable>From: </lable> <input id="from" type="number" class="form-control" value="'+from+'"> </div> <div class="col-md-6"> <lable>To: </lable> <input id="to" type="number" class="form-control" value="'+to+'"> </div> </div>';

			return html;
			break;

			default:
			break;
	}

}

function setEventOnClickSaveButton() {
	$('#edit-question-form').off('submit');
	$('#edit-question-form').on('submit',function(e){
		e.preventDefault();
		if(validate()){
			var req  = {};
			req.action = "updateQuestion";
			req.question =  CKEDITOR.instances['question-box'].getData();
			req.options =  getOptionsData();
			req.description =  descriptionEditor.getData();
			req.question_id	=	question_id;
			//New code
			req.class_id = $("#classes").find('option:selected').data('id');
			req.subject_ids = getSelectedSubjectId();
			req.unit_ids = getSelectedUnitId();
			req.chapter_ids = getSelectedChapterId();
			req.topic_ids = getSelectedTopicId();
			req.tag_ids = getSelectedTagId();
			req.difficultyLevel_ids = getSelectedDifficultyLevelId();
			req.skill_ids = getSelectedskillId();
			//End New code
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'contentType'	:	"application/json; charset=utf-8",
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
					// console.log(res);
				if(res.status == 1)
				{
					if($('#save-question').attr('data-href') != null){
						if($('#save-question').attr('data-href').search("undefined") < 0){
							toastr.success(res.message);
							setTimeout(window.location = $('#save-question').attr('data-href'),100);
						}
						else{
							toastr.error('Error: There is no more Questions available according to your filter');
							setTimeout(window.location = 'question-bank.php', 100);
						}
					}
					else{
						toastr.error('Error: There is no more Questions available according to your filter');
						setTimeout(window.location = 'question-bank.php', 100);
					}
				}
				else
				{
					toastr.error('Error: '+res.message);
				}
			});

		}
	});

}

function getOptionsData() {
	type = $('#question-box').attr('type');
	value = [];
	switch(parseInt(type)) {
		case 1:
			$.each($('#options .option'), function(i,option){
				choises = {};
				choises.option = CKEDITOR.instances[$(this).attr('id')].getData();
				choises.answer = 0;
				if($(this).parents('.row').eq(0).find('input').prop('checked'))
					choises.answer = 1;
				value.push(choises);
			});
			break;

		case 2:
			$.each($('#options .option'), function(i,option){
				choises = {};
				choises.option = CKEDITOR.instances[$(this).attr('id')].getData();
				choises.answer = 0;
				if($(this).parents('.row').eq(0).find('input').prop('checked'))
					choises.answer = 1;
				value.push(choises);
			});
			break;

		case 3:
			$.each($('#options .option'), function(i,option){
				choises = {};
				choises.option = $(this).text();
				choises.answer = 0;
				if($(this).parents('.row').eq(0).find('input').prop('checked'))
					choises.answer = 1;
				value.push(choises);
			});
			break;

		case 4:
			$.each($('#options .option'), function(i,option){
				choises = {};
				choises.option = CKEDITOR.instances[$(this).attr('id')].getData();
				choises.answer = 1;
				value.push(choises);
			});
			break;

		case 5:
			firstColumn = $('[data-column="1"]');
			secondColumn = $('[data-column="2"]');
			extraOptions = $('[data-column="3"]');
			value = [];
			$.each(firstColumn, function(i,option){
				choises = {};
				choises.option = CKEDITOR.instances[$(option).attr('id')].getData();
				choises.answer = 0;
				choises.number = 1;
				value.push(choises);
			});
			$.each(secondColumn, function(i,option){
				choises = {};
				choises.option = CKEDITOR.instances[$(option).attr('id')].getData();
				choises.answer = 1;
				choises.number = 2;
				value.push(choises);
			});
			$.each(extraOptions, function(i,option){
				choises = {};
				choises.option = CKEDITOR.instances[$(option).attr('id')].getData();
				choises.answer = 0;
				choises.number = 2;
				value.push(choises);
			});
			break;

		case 6:
			$.each($('#options input:text'), function(i,option){
				choises = {};
				choises.option = $(this).val();
				choises.answer = 1;
				value.push(choises);
			});
			break;

		case 7:
			$.each($("#options-box .option"), function(i,option) {
				$.each($(option).find('.dd-option'), function(x,val) {
					choises = {};
					//choises.option = $(option).html();
					choises.option = $(val).find('input:text').val();
					choises.number = i+1;
					choises.answer = 0;
					if($(val).find('input:radio').prop('checked'))
						choises.answer = 1;
					value.push(choises);
				});
			});
			break;

		case 8:
			$.each($('#options .options input:text'), function(i,option){
				choises = {};
				choises.option = $(this).val();
				choises.answer = 1;
				value.push(choises);
			});
			$.each($('#extra-options input:text'), function(i,option){
				choises = {};
				choises.option = $(this).val();
				choises.answer = 0;
				value.push(choises);
			});
			break;

		case 9:
			// question type deleted
			break;

		case 10:
			// these question contains no option
			break;

		case 11:
			// these question contains no option
			break;


		case 12:
			value = [];
			thead = $('#matrix-table thead th')
			thead.splice(0,1);
			thead.splice(-1,1);
			$.each(thead, function(i, th) {
				var opt= {};
				opt.answer = 1;
				opt.option = $(th).find('[contenteditable]').html();
				value.push(opt);
			});

			var number = 2;
			$.each($('#matrix-table tbody tr'), function(i, tr) {
				values = $(tr).find(' td');
				values.splice(0,1);
				values.splice(-1,1);

				var opt= {};
				opt.answer = number;
				opt.option = $(tr).find('td:eq(0)').find('[contenteditable]').html();
				value.push(opt);
				$.each(values, function(x, td) {
					var opt= {};
					opt.answer = number;
					opt.option = 0;
					if ($(td).find('input:checkbox').prop('checked')) {
						opt.option = 1;
					};
					value.push(opt);
				})

				number++;
			})
			break;

		case 13:
			var choises = {};
				choises.option = $('#from').val();
				choises.number = 1;
				choises.answer = 1;
			value.push(choises);

			var choises = {};
				choises.option = $('#to').val();
				choises.number = 2;
				choises.answer = 1;
			value.push(choises);
			break;

		default:
			toastr.error('');
			break;
	}

	return value;
}



function getHtmlOfOptionsInPassage(type,options) {
	radioButtonName = parseInt(Math.random() * 1000);
	switch(type) {
		case '1':
			var html = '';
			$.each(options ,function(i,option) {
				html += '<div class="row"><div class="col-md-12"><div class="col-md-1 form-group radio-option"><div class="input-group"><div class="radio-list"><label>'
				if(option.answer == 1) {
					html += '<input type="radio" name="radio'+radioButtonName+'" checked="checked">';
				}
				else {
					html += '<input type="radio" name="radio'+radioButtonName+'">';
				}

				html += '</label></div></div></div><div class="col-md-10 form-group"><div contentEditable="true" class="option" name="option" id="option'+i+'" data-id="'+option.id+'" style="border: 1px solid rgb(232, 227, 245); min-height: 30px;">'+option.option+'</div></div><div class="col-md-1"></div></div></div>';
			});
			return html;
			break;

		case '2':
			var html = '';
			$.each(options ,function(i,option) {
				html += '<div class="row"><div class="col-md-12"><div class="col-md-1 form-group radio-option"><div class="input-group"><div class="checkbox-list"><label>';

				if(option.answer == 1) {
					html += '<input type="checkbox" checked="checked">';
				}
				else {
					html += '<input type="checkbox" >';
				}

				html += '</label></div></div></div><div class="col-md-10 form-group"><div contentEditable="true" class="option" name="option" id="option'+i+'" data-id="'+option.id+'" style="border: 1px solid rgb(232, 227, 245); min-height: 30px;">'+option.option+'</div></div><div class="col-md-1"></div></div></div>';

			});

			return html;
			break;

		case '3':
			var html = '';
			$.each(options ,function(i,option) {
				html += '<div class="row"> <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="checkbox-list"> <label>';

				if(option.answer == 1) {
					html += '<input type="radio" name="radio'+radioButtonName+'" checked="checked">';
				}
				else {
					html += '<input type="radio" name="radio'+radioButtonName+'">';
				}

				html += '</label> </div> </div> </div> <div class="col-md-10 form-group"> <div class="option" name="option" data-id="'+option.id+'" style="border: 1px solid rgb(232, 227, 245); min-height: 30px;padding: 7px;width: 50%;">'+option.option+'</div> </div> <div class="col-md-1"></div> </div></div>';
			});

			return html;
			break;

		case '4':
			var html = '';
			$.each(options ,function(i,option) {
				html += '<div class="row"> <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label class="index">'+(i+1)+'</label> </div> </div> </div> <div class="col-md-10 form-group"> <div contentEditable="true" class="option" name="ord-option'+i+'" data-id="'+option.id+'" id="option'+i+'" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;">'+option.option+'</div> </div> <div class="col-md-1"></div> </div></div>';
			});

			return html;
			break;


		case '6':
			var html = '';

			$.each(options ,function(i,option) {
				if(option.answer == 1) {
					html += '<div class="row"> <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label class="index">'+(i+1)+'</label> </div> </div> </div> <div class="col-md-10 form-group"> <input contentEditable="true" type="text" class="option form-control" name="ord-option'+i+'" data-id="'+option.id+'" id="option'+i+'" value="'+option.option+' "/> </div> <div class="col-md-1"><a class="delete-option btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div> </div></div>';
				}
			});

			return html;
			break;

		case '7':
			var html = '';
			$.each(options ,function(i,option) {
				html += '<div class="row"> <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label class="index">'+(i+1)+'</label> </div> </div> </div> <div class="options col-md-10 form-group"> <input class="option form-control" name="ord-option'+i+'" data-id="'+option.id+'" id="option'+i+'" value="'+option.option+'"/> </div> <div class="col-md-1"><!--<a class="delete-option btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a>--></div> </div></div>';
			});

			return html;
			break;

		case '8':
			var html = '';
			html += '<div class="options">';
			$.each(options ,function(i,option) {
				if(option.answer == 1) {
					html += '<div class="row"> <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label class="index">'+(i+1)+'</label> </div> </div> </div> <div class="col-md-10 form-group"> <input type="text" class="option form-control" name="ord-option'+i+'" data-id="'+option.id+'" id="option'+i+'" value="'+option.option+' "/> </div> <div class="col-md-1"><!--<a class="delete-option btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a>--></div> </div></div>';
				}
			});
			html += '</div>';

			html += '<div id="extra-options">';
			$.each(options ,function(i,option) {
				if(option.answer == 0) {
					html += '<div class="row"> <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label class="index">'+(i+1)+'</label> </div> </div> </div> <div class="col-md-10 form-group"> <input type="text" class="option form-control" name="ord-option'+i+'" data-id="'+option.id+'" id="option'+i+'" value="'+option.option+' "/> </div> <div class="col-md-1"><!--<a class="delete-option btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a>--></div> </div></div>';
				}
			});
			html += '</div>';

			return html;
			break;
		case '13':

			html = '<div class="row">';
			// html += '<div class="options">';
			$.each(options ,function(i,option) {
				if(option.answer == 1 && option.number == 1)
				{
					html += ' <div class="col-md-4">  <div class="col-md-12 form-group"> From <input type="text" class="option form-control" name="ord-option'+i+'" data-id="'+option.id+'" id="option'+i+'" value="'+option.option+' "/> </div> </div>';
				}
				else
				{
					html += ' <div class="col-md-4">  <div class="col-md-12 form-group"> To <input type="text" class="option form-control" name="ord-option'+i+'" data-id="'+option.id+'" id="option'+i+'" value="'+option.option+' "/> </div> </div>';
				}
			});
			html += '</div>';
			return html;
			break;


		default:
			break;
	}

}

/*
* this function in it ck editor in options divs
*/
function initCkeditor() {
	$('#options [contenteditable]').uniqueId();
	$.each($('#options [contenteditable]'), function(i,option) {
		CKEDITOR.inline($(option).attr('id'));
	});

}

/*
* function load all events according to question type
*/
function loadFunctionsOfQuestion() {
	type = $('#question-box').attr('type');
	switch(parseInt(type)) {
		/*
		* function to load events of SCQ question type
		*/
		case 1:
			setEventAddScqOption();
			setEventDeleteOption1_4();
		break;

		/*
		* function to load events of MCQ question type
		*/
		case 2:
			setEventAddMcqOption();
			setEventDeleteOption1_4();
		break;

		/*
		* function to load events of TRUE/False question type
		*/
		case 3:
		// dont need
		break;

		/*
		* function to load events of Sequencing(Ordering) question type
		*/
		case 4:
			setEventAddSequencingOption();
			setEventDeleteOption1_4();
		break;

		/*
		* function to load events of Matching(MTF) question type
		*/
		case 5:
			setEventAddMtfOption();
			setEventAddMtfPairOptions();
			setEventDeleteMtfOptions();
		break;

		/*
		* function to load events of Fill in the blanks question type
		*/
		case 6:
			setEventFbAddBlank();
			setEventDeleteFbOptions();
		break;

		/*
		* function to load events of Fill in the blanks DROP DOWN question type
		*/
		case 7:
			setEventAddDropDown();
			setEventAddOptionsInDropDown();
			setEventKeyup();
			setEventDeleteFbDDOptions();
		break;

		/*
		* function to load events of Fill in the blanks DRAG AND DROP question type
		*/
		case 8:
			setEventDeleteFbDndOptions();
			setEventFbDndAddBlank();
		break;

		/*
		* function to load events of Assersion Reasion question type
		*/
		case 9:
		// dont need
		break;

		/*
		* function to load events of Descriptive question type
		*/
		case 10:
		// dont need
		break;

		/*
		* function to load events of Passage Based question type
		*/
		case 11:

		break;

		/*
		* function to load events of Matrix question type
		*/
		case 12:
		matrixDeleteRow();
		matrixAddRow();
		matrixAddColumn();
		matrixDeleteColumn();
		break;

		/*
		* function to load events of Integer question type
		*/
		case 13:
			// question type not created
		break;

		default:
		break;
	}
}


function validate(){
	bool = true
	var options = $('#options').find('.option');
	if($("#classes").val() == 0 ){
		toastr.error('please enter class.');
		bool = false;
	}
	if( $("#subjects").val() == null){
		toastr.error('please enter subject.');
		bool = false;
	}

	if( $('#question-box').text() == ""){
		toastr.error('Please Enter Question.');
		bool = false;
	}
	$.each(options,function(i,option){
		console.log($(option).attr('type'));
		console.log($(option).text());

		if($(option).attr('type') !== undefined){
			if($(option).val() == ""){
				toastr.error('Please fill answer1.');
				bool = false;
				return;
			}
		}
		else{
			// if($(option).text() == ""){
			// 	toastr.error('Please fill answer2.');
			// 	bool = false;
			// 	return;
			// }

		}
	});

	if($('#options input:checkbox').length != 0 || $('#options input:radio').length != 0 ){
		if($('#options input:checked').length == 0){
			toastr.error('Please select option.');
			bool = false;
		}

	}

	return bool;
}

$(function(){
	$('#save-question').off('click');
	$('#save-question').on('click', function(e) {
		e.preventDefault();
		$('#edit-question-form').submit();
	});
});