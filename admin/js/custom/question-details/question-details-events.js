// event load subjects when admin select a class
$('#classes').on('change', function(e) {
	$("#subjects").select2('val',null);
	$("#units").select2('val',null);
	$("#chapters").select2('val',null);
	$("#topics").select2('val',null);
	$("#subjects").html('');
	$("#units").html('');
	$("#chapters").html('');
	$("#topics").html('');
	fetchSubjects();
	//updateTaggedClass();
});

// event load units when admin select subjects
$('#subjects').select2().on('change', function(e) {
		$("#units").select2('val',null);
		$("#chapters").select2('val',null);
		$("#topics").select2('val',null);
		$("#units").html('');
		$("#chapters").html('');
		$("#topics").html('');
		fetchUnits();
		//updateTaggedSubject();
});
	
// event load topics when admin select units
$('#units').select2().on('change', function(e) {
	$("#chapters").select2('val',null);
	$("#topics").select2('val',null);
	$("#chapters").html('');
	$("#topics").html('');
	fetchChapters();
	//updateTaggedUnit();
});

// event load topics when admin select units
$('#chapters').select2().on('change', function(e) {
	$("#topics").select2('val',null);
	$("#topics").html('');
	fetchTopics();
	//updateTaggedChapter();
});

// event fired when when some one change topic drop-down
$('#topics').select2().on('change', function(e) {
	//updateTaggedTopic();
});

// Event Fired When user changes Tags
$('#tags').select2().on('change', function(e) {
	tags = $("#tags").find('option:selected')
	id = [];
	$.each(tags, function(i,tag_id){
		id.push($(tag_id).data('id'));
	})
	tag_ids = id;
	//updateTaggedTag();
});

// Event Fired When user changes Difficulty-Level
$('#difficulty-levels').select2().on('change', function(e) {
	difficultyLevels = $("#difficulty-levels").find('option:selected')
	id = [];
	$.each(difficultyLevels, function(i,dl_id){
		id.push($(dl_id).data('id'));
	})
	difficultyLevel_ids = id;
	//updateTaggedDifficulty();
});

// Event Fired When user changes Skills
$('#skills').select2().on('change', function(e) {
	skills = $("#skills").find('option:selected');
	id = [];
	$.each(skills, function(i,skill_id){
		id.push($(skill_id).data('id'));
	});
	skill_ids = id;
	//updateTaggedSkill();
});



/*
*	function to update
*
*/
function updateTaggedClass() {
	var req = {};
	req.action = "updateTaggedClass";
	req.question_id = question_id;
	 req.class_id = $("#classes").find('option:selected').data('id');
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		toastr.info(res.message);
	});

}

/*
*	function to update
*
*/
function updateTaggedSubject() {
	var req = {};
	req.action = "updateTaggedSubject";
	req.question_id = question_id;
	req.subject_ids = getSelectedSubjectId();
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		toastr.info(res.message,'Subject');
	});

}

/*
*	function to update
*
*/
function updateTaggedUnit() {
	var req = {};
	req.action = "updateTaggedUnit";
	req.question_id = question_id;
	req.unit_ids = getSelectedUnitId();
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		toastr.info(res.message,'Unit');
	});

}

/*
*	function to update
*
*/
function updateTaggedChapter() {
	var req = {};
	req.action = "updateTaggedChapter";
	req.question_id = question_id;
	req.chapter_ids = getSelectedChapterId();
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		toastr.info(res.message,'Chapter');
	});

}

/*
*	function to update
*
*/
function updateTaggedTopic() {
	var req = {};
	req.action = "updateTaggedTopic";
	req.question_id = question_id;
	req.topic_ids = getSelectedTopicId();
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		toastr.info(res.message,'Topic');
	});

}

/*
*	function to update tag
*
*/
function updateTaggedTag() {
	var req = {};
	req.action = "updateTaggedTag";
	req.question_id = question_id;
	req.tag_ids = getSelectedTagId();
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		toastr.info(res.message,'Tag');
	});

}

/*
*	function to update difficulty
*
*/
function updateTaggedDifficulty() {
	var req = {};
	req.action = "updateTaggedDifficulty";
	req.question_id = question_id;
	req.difficultyLevel_ids = getSelectedDifficultyLevelId();
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		toastr.info(res.message,'Difficulty-Level');
	});

}

/*
*	function to update skill
*
*/
function updateTaggedSkill() {
	var req = {};
	req.action = "updateTaggedSkill";
	req.question_id = question_id;
	req.skills = getSelectedskillId();
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		toastr.info(res.message,'Skill');
	});

}


/*
* function to delete options in SCQ, MCQ and Sequencing(Ordering) question types
*/
function setEventDeleteOption1_4() {
	$('.delete-option').off('click');
	$('.delete-option').on('click',function() {
		if($('.delete-option').length > 2) {
			$id = $(this).parents('.row').eq(0).find('.option').data('id');
			$(this).parents('.row').eq(0).remove();
		}
	});
}

/*
* function to add options in scq question type
*/
function setEventAddScqOption() {

	$('#addOption').off('click');
	$('#addOption').on('click', function() {
		//alert("hiscq");
		var i = $("#options").find(".option").length;
		if(i < 6) {
			//i += 1;
			html = '<div class="row">';
			html += '<div class="col-md-12">';

			html += '<div class="col-md-1 form-group radio-option">';
			html += '<div class="input-group">';
			html += '<div class="radio-list">';
			html += '<label><input type="radio" name="radio1" /></label>';
			html += '</div></div></div>';

			html += '<div class="col-md-10 form-group">';
			html += '<div contentEditable="true" class="option" name="option" id="option'+i+'" data-id="" style="border: 1px solid rgb(232, 227, 245); min-height: 30px;"></div>';
			html += '</div>';

			html += '<div class="col-md-1"><a class="delete-option btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div>';
			html += '</div></div>';
		
			$("#options").append(html);
			CKEDITOR.inline('option'+i);

			setEventDeleteOption1_4();
			Metronic.init();
		}
	});
}


/*
* function to add options in MCQ question type
*/
function setEventAddMcqOption() {
	$('#addOption').off('click');
	$('#addOption').on('click', function() {
		var i = $("#options").find(".option").length;
		if(i < 6) {
			//i += 1;
			html = '<div class="row"><div class="col-md-12"><div class="col-md-1 form-group radio-option"><div class="input-group"><div class="radio-list"><label><input type="checkbox" ></label></div></div></div><div class="col-md-10 form-group"><div contentEditable="true" class="option" name="option" id="option'+i+'" data-id="" style="border: 1px solid rgb(232, 227, 245); min-height: 30px;"></div></div><div class="col-md-1"><a class="delete-option btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div></div></div>'
		
			$("#options").append(html);
			CKEDITOR.inline('option'+i);
			setEventDeleteOption1_4();
			Metronic.init();
		}
	});
}

/*
* function to add options in Sequencing(Ordering) question type
*/
function setEventAddSequencingOption() {
	$('#addOption').off('click');
	$('#addOption').on('click', function() {
		var i = $("#options").find(".option").length;
		if(i < 6) {
			//i += 1;
			html = '<div class="row"> <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label class="index"></label> </div> </div> </div> <div class="col-md-10 form-group"> <div contentEditable="true" class="option" name="ord-option'+i+'" data-id="" id="option'+i+'" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1"><a class="delete-option btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div> </div></div>'
		
			$("#options").append(html);
			CKEDITOR.inline('option'+i);
			Metronic.init();
			setEventDeleteOption1_4();
		}
	});
}


/*
* function to delete options in Matching(MTF) question type
*/
function setEventDeleteMtfOptions() {
	$('.match-deleteOption').off('click');
	$('.match-deleteOption').on('click', function() {
		remove = $("#options .option").length;
		if(remove>3) {
			$(this).parents().eq(1).remove();
			opt = $("#match-options-div .option");
			$.each(opt,function(x,o) {
				remove = x+1;
				$(o).attr('placeholder','Option '+remove);
			});
		}
	});
}

/*
* function to add options in Matching(MTF) question type
*/
function setEventAddMtfOption() {
	$('#addOption').off('click');
	$('#addOption').on('click', function() {
		genratedId = $("#options .option").length;
		if(genratedId < 50) {
			//genratedId += 1;
			html = '<div class="options row"> <div class="col-md-12"> <div class="col-md-11 col-xs-10"> <div class="row"> <div class="col-md-6 col-xs-6 form-group"></div> <div class="col-md-6 col-xs-6 form-group"> <div contentEditable="true" class="option" id="match-option'+(genratedId)+'" data-column="3" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> </div> </div> <div class="col-md-1 col-xs-2"> <a class="match-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div></div>';
			$('#options').append(html);
			CKEDITOR.inline( 'match-option'+genratedId);
			setEventDeleteMtfOptions();
		}		
	});
}

/*
* function to add Pair options in Matching(MTF) question type
*/
function setEventAddMtfPairOptions() {
	$('#addPairOptions').off('click');
	$('#addPairOptions').on('click', function() {
		genratedId = $("#options .option").length;
		if(genratedId < 50) {
			//genratedId += 1;
			html = '<div class="options row"> <div class="col-md-12"> <div class="col-md-11 col-xs-10"> <div class="row"> <div class="col-md-6 col-xs-6 form-group"> <div contentEditable="true" class="option" id="match-option'+genratedId+'" data-column="1" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-6 col-xs-6 form-group"> <div contentEditable="true" class="option" id="match-option'+(genratedId+1)+'" data-column="2" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> </div> </div> <div class="col-md-1 col-xs-2"> <a class="match-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div></div>';
			$('#options').append(html);
			CKEDITOR.inline( 'match-option'+genratedId);
			CKEDITOR.inline( 'match-option'+(genratedId+1));
			setEventDeleteMtfOptions();
		}		
	});
}

// function to delete Fill In The Blanks option
function setEventDeleteFbOptions() {
	$('#question-box').keyup(function(e) {
	    e.preventDefault();
	    blank = $('#question-box input').length;
	    option = $("#options .option").length;
	    if (blank != option) {
	        options = $("#options .option")
	        $.each(options, function(i, opt) {
	            id = $(opt).data('id');
			        if ($('[data-id="' + id + '"]').length != 2)
			            $(opt).parents('.row').eq(0).remove();
			})
		}
	})
}


// function to add blanks in fill in the blanks
function setEventFbAddBlank() {
	var i = parseInt(Math.random() * 1000);
	$('#addOption').off('click')
	$('#addOption').on('click', function(e) {
		e.preventDefault();
		html = '<div class="row"> <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label class="index"></label> </div> </div> </div> <div class="col-md-10 form-group"> <input class=" form-control option" data-id="input'+(i+1)+'" placeholder="Option"/> </div> </div></div>'
		divText = questionEditor.getData();
		$('#question-box').html(divText.slice(0,-5)+'<input class="fb-input" disabled="true" data-id="input'+(i+1)+'" size="8"></p>');
		$('#options').append(html);
		i++;
	});
}


/*
* function to delete drop down's option of fill in the blanks drop down
*/
function setEventDeleteFbDDOptions() {
	$('.fb-dd-deleteOption').off('click');
	$('.fb-dd-deleteOption').on('click', function(e) {
		e.preventDefault();
		numberOfOptions = $(this).parents('.option').find('input:text').length;
		if(numberOfOptions > 2)
			$(this).parents('.dd-option').eq(0).remove();
	})
}


/*
* function to delete drop down option of fill in the blanks drop down
*/
function setEventKeyup() {
	$('#question-box').keyup(function(e) {
		alert("seteventkeyup qdevents")
	    e.preventDefault();
	    blank = $('#question-box select').length;
	    option = $("#options .option").length;
	    if (blank != option) {
	        options = $("#options .option");
	        $.each(options, function(i, opt) {
	            selector = $(opt).data('id');
			        if ($('[data-id="' +selector+ '"]').length != 2)
			            $(opt).remove();
			})
		}
	});
}


/*
* function to add option in drop down of fill in the blanks drop down
*/
function setEventAddOptionsInDropDown() {
	$('.add-option-in-drop-down').off('click');
	$('.add-option-in-drop-down').on('click', function(e) {
	    e.preventDefault();
	    numberOfOptions = $(this).parents('.option').find('input:text').length;
	    radioButtonName = $(this).parents('.option').find('input:radio').attr('name');
		if(numberOfOptions < 6) {
		    var html = '<div class="col-md-12 dd-option"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input type="radio" name="'+radioButtonName+'"> </label> </div> </div> </div> <div class="options col-md-10 form-group"> <input class="form-control" type="text" /> </div> <div class="col-md-1"><a class="fb-dd-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div> </div>';
		    $(this).parents('.option').find('.dd-option').eq(-1).after(html);
			setEventDeleteFbDDOptions();
			Metronic.init();
		}
	})
}


/*
* function to add Drop Down in fill in the blanks drop down
*/
function setEventAddDropDown() {
	// function to add option

	//alert('hi');
	numb = $('#question-box').find('select').length + 1;
	var i = parseInt(Math.random() * 1000);
	$('#addOption').off('click');
	$('#addOption').on('click', function(e) {
		e.preventDefault();
		i++;
		html = '<div id="fb-dd'+i+'" class="option" data-id="dd'+i+'"> <label class="mrg-left10"> Drop Down '+numb+' Options </label> <div class="row"> <div class="col-md-12 dd-option"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input type="radio" name="'+i+'"> </label> </div> </div> </div> <div class="options col-md-10 form-group"> <input class="form-control" type="text" /> </div> <div class="col-md-1"><a class="fb-dd-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div> </div> <div class="col-md-12 dd-option"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input type="radio" name="'+i+'"> </label> </div> </div> </div> <div class="options col-md-10 form-group"> <input class="form-control" type="text" /> </div> <div class="col-md-1"><a class="fb-dd-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div> </div> <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> </div> </div> </div> <div class="col-md-10 form-group"> <a class="add-option-in-drop-down"> + Add More Option </a> </div> <div class="col-md-1"></div> </div> </div></div>';
		divText = questionEditor.getData();
		$('#question-box').html(divText.slice(0,-5)+'<select disabled="true" data-id="dd'+i+'" style="width:10%"></select> </p>');
		$('#options').append(html);
		setEventAddOptionsInDropDown();
		setEventDeleteFbDDOptions(); // function set events to delete on options
		Metronic.init();
		numb++;
	});
}


// function to delete Fill In The Blanks option
function setEventDeleteFbDndOptions() {
	$('#question-box').keyup(function(e) {
	    e.preventDefault();
	    //console.log("seteventdeletefbdnd" );
	    
	   
	    blanks = $('#question-box input');
	    options = $('.options .option');
	   
	    if (blanks.length != options.length) {
	        var arr=[];
	        $.each(blanks, function(i, blank) {
	            a = $(blank).data('id');
	            arr.push(a);       
			});
			
			
			$.each(options, function(i, opt) {
	            a = $(opt).data('id');
	          
	            if(arr.indexOf(a)==-1){
	            	$(opt).parent().parent().remove();
	            }
			});
		}
	
	});
	$('.fb-deleteOption').off('click');
	$('.fb-deleteOption').on('click', function(e) {
		e.preventDefault();
		id = $(this).data('id');
		$('#question-box [data-id='+id+']').remove();
		$(this).parents('.options .row').remove(); 
		
	});

	$('.fb-dnd-deleteOption').off('click');
	$('.fb-dnd-deleteOption').on('click', function(e) {
		e.preventDefault();
		
		$(this).parents('#extra-options .row').remove(); 
		
	});

}


// function to add blanks in fill in the blanks
function setEventFbDndAddBlank() {
	var i = parseInt(Math.random() * 1000);
	$('#fb-addBlank').off('click')
	$('#fb-addBlank').on('click', function(e) {
		e.preventDefault();
		//alert('seteventfbdndaddblank');
		html = '<div class="row">';
		html += '<div class="col-md-12">';
		html += '<div class="col-md-1 form-group radio-option">';
		html += '<div class="input-group">';
		html += '<div class="icheck-list">';
		html += '<label class="index"></label>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		html += '<div class="col-md-10 form-group">';
		html += '<input class=" form-control option" data-id="input'+(i+1)+'" placeholder="Option"/>';
		html += '</div>';
		html += '<div class="col-md-1"><a class="fb-deleteOption btn btn-circle btn-icon-only blue" data-id="input'+(i+1)+'"><i class="fa fa-trash-o"></i></a></div>';
		html += '</div>';
		html += '</div>';
		divText = questioneditor.getData();
		$('#question-box').html(divText.slice(0,-5)+'<input class="fb-input" disabled="true" data-id="input'+(i+1)+'" size="8"></p>');
		$('#options .options').append(html);
		setEventDeleteFbDndOptions();
		i++;
	});
}

function setEventFbDndAddOption() {

		var i = 0;
	$('#add-option-fb-dnd').off('click')
	$('#add-option-fb-dnd').on('click', function(e) {
		e.preventDefault();
		i++;
		html = '<div class="row">';
		html += '<div class="col-md-12">';
		html += '<div class="col-md-1 form-group radio-option">';
		html += '<div class="input-group">';
		html += '<div class="icheck-list">';
		html += '<label class="index"></label>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		html += '<div class="col-md-10 form-group">';
		html += '<input class=" form-control option" data-id="input'+(i+1)+'" placeholder="Option" style="background-color: aliceblue;"/>';
		html += '</div>';
		html += '<div class="col-md-1"><a class="fb-dnd-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div>';
		html += '</div>';
		html += '</div>';
		
		// $('#fb-question-box').html(divText.slice(0,-5)+'<input disabled="true" data-id="input'+(i+1)+'" size="8" maxlength="0"></p>');
		$('#extra-options').append(html);
	});


	/*var i = parseInt(Math.random() * 1000);
	$('#fb-addBlank').off('click')
	$('#fb-addBlank').on('click', function(e) {
		e.preventDefault();
		html = '<div class="row"> <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label class="index"></label> </div> </div> </div> <div class="col-md-10 form-group"> <input class=" form-control option" data-id="input'+(i+1)+'" placeholder="Option"/> </div> </div></div>'
		divText = questioneditor.getData();
		$('#question-box').html(divText.slice(0,-5)+'<input class="fb-input" disabled="true" data-id="input'+(i+1)+'" size="8"></p>');
		$('#options .options').append(html);
		i++;
	});*/
}


/*
* function to delete row of Matrix question type
*/
function matrixDeleteRow() {
	$('.matrix-delete-row').off('click');
	$('.matrix-delete-row').on('click', function(e) {
		e.preventDefault();
		if ($('#matrix-table tbody tr').length > 2) {
			$(this).parents('tr').remove();
		}
	});
}


/*
* function add row of Matrix question type
*/
function matrixAddRow() {
	$('#matrix-add-row').off('click');
	$('#matrix-add-row').on('click', function(e) {
		e.preventDefault();
		if ($('#matrix-table tbody tr').length < 10) {

			html = '<tr> <td> <div id="" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"> </div></td> '
			for(x=2; x<$('#matrix-table thead tr:eq(0) th').length; x++) {
				html += '<td><div class="checkbox-list"><label><input type="checkbox"></label></div></td>'; 
			}
			html += '<td><a class="matrix-delete-row matrix-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a></td></tr>';
			$('#matrix-table tbody').append(html);
		};
		matrixDeleteRow();
	});
}


/*
* function add column of Matrix question type
*/
function matrixAddColumn() {
	$('#matrix-add-column').off('click');
	$('#matrix-add-column').on('click', function(e) {
		e.preventDefault();
		if($('#matrix-table thead tr:eq(0) th').length < 12) {
			$('#matrix-table thead tr:eq(0) th:eq(-1)').before('<th> <div id="" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"> </th>');
			$.each($('#matrix-table tbody tr'), function(i,tr) {
				$(tr).find('td:eq(-1)').before('<td> <div class="checkbox-list"> <label> <input type="checkbox"> </label> </div> </td>');
			});
			$('#matrix-table tfoot tr:eq(0) td:eq(-1)').before('<td><a class="matrix-delete-column matrix-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a></td>');
		}
		matrixDeleteColumn();
	});
}


/*
* function delete column of Matrix question type
*/
function matrixDeleteColumn() {
	$('.matrix-delete-column').off('click');
	$('.matrix-delete-column').on('click', function(e) {
		e.preventDefault();
		if($('#matrix-table thead tr:eq(0) th').length > 4) {
			index = $(this).parent()[0].cellIndex;
			$('#matrix-table thead tr:eq(0) th:eq('+index+')').remove();
			$.each($('#matrix-table tbody tr'), function(i,tr) {
				$(tr).find('td:eq('+index+')').remove();
			});
			$('#matrix-table tfoot tr:eq(0) td:eq('+index+')').remove();
		}
	});
}

function setEventDeleteSubQuestion() {
	$('.delete-sub-question').off('click');
	$('.delete-sub-question').on('click', function(e) {
		e.preventDefault();
		question_id = $(this).data('id');
		Question = $(this).parents('.question-view:eq(0)');
		if(confirm('Are you sure want to delete this Question.')) {
			var req = {};
			req.action = 'deleteQuestion';
			req.question_id = question_id;
			$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 1){
					Question.remove();
					toastr.success(res.message);
				}
				else {
					toastr.error(res.message,'Error:');
				}
			});
		}
	
	});
}


addPassageQuestion = function() {
	$('.add-question').off('click');
	$('.add-question').on('click', function(e) {
		e.preventDefault();
		$('#passbase-questions-div'	).show();
		setEventAppendQuestion();
		parent_id=question_id;
		setEventOnFormSubmit();
		//alert(parent_id);
	});
	// $('#question-types').on('change', function(e) {
	// question_type = $('#question-types option:selected').data('id');
	
	
	// html = getTheHtmlOfSelectedQuestionType(question_type);

	// $("#question-container").html(html);
	// loadEventOfSelectedQuestionType(question_type);
	// parent_id = 0;
	// showDescriptionBox(question_type);
	// setEventDeleteOptions();
}

function showDescriptionBox(questionType) {
	if(questionType == 0 || questionType == 11) {
		$('#description-container').html('');
	}
	else {
		// $('#description-container').html('<div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div>');
		hintEditor = CKEDITOR.inline('hint-box');
		
	}

}