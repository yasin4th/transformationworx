function fbDndLoadEditor() {
	setEventAddBlank();
	setEventDeleteFbOptions(); // function set events to delete on options
	
	//fbQuestionEditor.destroy();
	fbQuestionEditor = CKEDITOR.inline('fb-question-box');
	setEventAddOptionFbDnd();
	setEventDeleteFbOptions();
}


function setEventAddOptionFbDnd() {
	// function to add option
	var i = 0;
	$('#add-option-fb-dnd').off('click')
	$('#add-option-fb-dnd').on('click', function(e) {
		e.preventDefault();
		i++;
		html = '<div class="row"> <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label class="index"></label> </div> </div> </div> <div class="col-md-10 form-group"> <input class=" form-control option" data-id="input'+(i+1)+'" placeholder="Option" style="background-color: aliceblue;"/> </div> </div></div>'
		divText = fbQuestionEditor.getData();
		// $('#fb-question-box').html(divText.slice(0,-5)+'<input disabled="true" data-id="input'+(i+1)+'" size="8" maxlength="0"></p>');
		$('#fb-dd-extra-options-div').append(html);
	});
}
	
// function to delete option
function setEventDeleteFbOptions() {

	//type 6
	$('#fb-question-box').keyup(function(e) {
		
	    e.preventDefault();
	    blanks = $('#fb-question-box input');
	    options = $('#fb-options-div .option');
	   
	    if (blanks.length != options.length) {
	        var arr=[];
	        $.each(blanks, function(i, blank) {
	            a = $(blank).data('id');
	            arr.push(a);       
			});
			
			
			$.each(options, function(i, opt) {
	            a = $(opt).data('id');
	          
	            if(arr.indexOf(a)==-1){
	            	$(opt).parent().parent().remove();
	            }
			});
		}
	});
}
function getOptionsFbDnd() {
	options = $("#fb-options-div .option");
	value = [];
	$.each(options, function(i,option) {
		choises = {};
		//choises.option = $(option).html();
		choises.option = $(option).val();
		choises.answer = 1;
		value.push(choises);
	});
	
	options = $("#fb-dd-extra-options-div .option");
	$.each(options, function(i,option) {
		choises = {};
		choises.option = $(option).val();
		choises.answer = 0;
		value.push(choises);
	});
	return value;
}


// this function save fb type question
function saveFbDnd(data) {
	req = getFBDnd_data();
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			//resetPageValues();
			toastr.success(res.message,'Question Id: 00'+res.questionId);
		}
		else {
			toastr.error(res.message,'Error: Question Not Saved');
		}
	});
}

function getFBDnd_data() {
	var req = {};
	req.action = "addQuestion";
	req.questionData = fbQuestionEditor.getData();
	req.DescriptionData = "No Description.";
	if(hintEditor != undefined)
		req.DescriptionData = hintEditor.getData();
	req.parent_id = parent_id;
	req.questionType = 8;
	req.options = getOptionsFbDnd();
	req.class_id = $("#classes").find('option:selected').data('id');
	req.subject_ids = getSelectedSubjectId();
	req.unit_ids = getSelectedUnitId();
	req.chapter_ids = getSelectedChapterId();
	req.topic_ids = getSelectedTopicId();
	req.tag_ids = getSelectedTagId();
	req.difficultyLevel_ids = getSelectedDifficultyLevelId();
	req.skill_ids = getSelectedskillId();
	return req;
}