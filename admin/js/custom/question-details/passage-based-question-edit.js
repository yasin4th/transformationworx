
function setEventAppendQuestion() {
	$('.passage-question-types').off('change');
	$('.passage-question-types').on('change', function() {
		question_type = $(this).val();
		
		html = getTheHtmlOfSelectedQuestionType(parseInt(question_type));
		$(this).parents('.passage-question').find('.selected-passage-question').html(html);
		loadEventOfSelectedQuestionType(parseInt(question_type));
		showDescriptionBox(question_type);
	});
}

function getTheHtmlOfSelectedQuestionType(type) {
	switch(type) {
		// case to add SCQ
		case 1:
			html = '<div class="singleChoise"> <div class="form-group"> <label> Enter Question: </label> <div id="scq-question-box" placeholder="Enter Question" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </div> <div class="row margin-top-10"> <div class="scq-options-div col-md-11"> <div class="row"> <div class="col-md-12 col-xs-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input type="radio" name="radioOption" data-answer="1"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option1" type="text" data-id="1" class="form-control option" placeholder="Option 1">--> <div id="scq-option1" contentEditable="true" data-id="1" class="option" placeholder="Option 1" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1 col-xs-2"> <a class="scq-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input type="radio" name="radioOption" data-answer="2"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <div id="scq-option2" contentEditable="true" class="option" data-id="2" placeholder="Option 2" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1 col-xs-2"> <a class="scq-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="ich/eck-list"> <label> <input type="radio" name="radioOption" data-answer="3"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option3" type="text" class="form-control option" data-id="3" placeholder="Option 3"> --> <div id="scq-option3" contentEditable="true" class="option" data-id="3" placeholder="Option 3" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1 col-xs-2"> <a class="scq-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input type="radio" name="radioOption" data-answer="3"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option4" type="text" class="form-control option" data-id="4" placeholder="Option 4"> --> <div id="scq-option4" contentEditable="true" class="option" data-id="4" placeholder="Option 4" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;"></div> </div> <div class="col-md-1 col-xs-2"> <a class="scq-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> </div> <div class="col-md-1"></div> <div class="col-md-12"> <a class="scq-addOption btn btn-circle btn-icon-only blue pull-right add-option"> <i class="fa fa-plus"></i> </a> </div> </div></div><div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;
			
		// case to generate html for MCQ Question Type
		case 2:
			html = '<div class="multiChoise"> <div class="form-group"> <label> Enter Question: </label> <div id="maq-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </div> <div class="row margin-top-10"> <div class="maq-options-div col-md-11"> <div class="row"> <div class="col-md-12 col-xs-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="checkbox-list"> <label> <input type="checkbox" data-answer="1"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option1" type="text" data-id="1" class="form-control option" placeholder="Option 1">--> <div id="maq-option1" contentEditable="true" data-id="1" class="option" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;" placeholder="Option 1"></div> </div> <div class="col-md-1 col-xs-2"> <a class="maq-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="checkbox-list"> <label> <input type="checkbox" data-answer="2"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option2" type="text" class="form-control option" data-id="2" placeholder="Option 2"> --> <div id="maq-option2" contentEditable="true" class="option" data-id="2" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;" placeholder="Option 2"></div> </div> <div class="col-md-1 col-xs-2"> <a class="maq-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="checkbox-list"> <label> <input type="checkbox" data-answer="3"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option3" type="text" class="form-control option" data-id="3" placeholder="Option 3"> --> <div id="maq-option3" contentEditable="true" class="option" data-id="3" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;" placeholder="Option 3"></div> </div> <div class="col-md-1 col-xs-2"> <a class="maq-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-1 col-xs-2 form-group radio-option"> <div class="input-group"> <div class="checkbox-list"> <label> <input type="checkbox" data-answer="3"> </label> </div> </div> </div> <div class="col-md-10 col-xs-8 form-group"> <!-- <input id="option4" type="text" class="form-control option" data-id="4" placeholder="Option 4"> --> <div id="maq-option4" contentEditable="true" class="option" data-id="4" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 30px;" placeholder="Option 4"></div> </div> <div class="col-md-1 col-xs-2"> <a class="maq-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a> </div> </div> </div> </div> <div class="col-md-1"></div> <div class="col-md-12"> <a class="maq-addOption btn btn-circle btn-icon-only blue pull-right add-option"> <i class="fa fa-plus"></i> </a> </div> </div> </div><div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;
			
		// case to generate html for T/F Question Type
		case 3:
			html = '<div id="true-false"> <div class="form-group"> <label> Enter Question: </label> <div id="tf-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> </div> <div class="row margin-top-10"> <div id="tf-options-div" class="col-md-11"> <div class="row"> <div class="col-md-12 col-xs-12"> <div class="col-md-2 col-xs-4 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input id="true" type="radio" name="radioOption" data-answer="1"> True </label> </div> </div> </div> <div class="col-md-9 col-xs-6 form-group"> <!--<div>True</div>--> </div> <div class="col-md-1 col-xs-2"> <!--<a class="tf-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a>--> </div> </div> </div> <div class="row"> <div class="col-md-12"> <div class="col-md-2 col-xs-4 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> <label> <input id="false" type="radio" name="radioOption" data-answer="2"> False </label> </div> </div> </div> <div class="col-md-9 col-xs-6 form-group"> <!--<div >False</div>--> </div> <div class="col-md-1 col-xs-2"> <!--<a class="tf-deleteOption btn btn-circle btn-icon-only blue"> <i class="fa fa-trash-o"></i> </a>--> </div> </div> </div> </div> <div class="col-md-1"></div> <div class="col-md-12"> <!--<a id="tf-addOption" class="btn btn-circle btn-icon-only blue pull-right add-option"> <i class="fa fa-plus"></i> </a>--> </div> </div> </div><div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;
						
		// case  to generate html for Sequescing Question Type
				
						
		// case  to generate html for FB Question Type
		case 6:	
			html = '<div id="fill-blanks" class=""> <div class="form-group"> <label> Enter Question: </label> <div id="fb-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> <button id="fb-addBlank"> Add Blank</button> </div> <div class="row margin-top-10"> <div id="fb-options-div" class="col-md-11"></div> <div class="col-md-1"></div> <div class="col-md-12"></div> </div> </div><div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;		
			
		// case to generate html for F/B Drag and drop Question Type
		case 7:
			html = '<div id="fill-blanks-dd" class=""> <div class="form-group"> <label> Enter Question: </label> <div id="fb-dd-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> <button id="fb-addDropDown"> Add Drop-Down </button> </div> <div class="row margin-top-10"><div class="col-md-12"><label> Answers: </label><div id="fb-dd-options-div" class="row"></div></div> </div> </div><div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;		

						
		// case  to generate html for F/B Drag-n-Drop Question Type
		case 8:	
			html = '<div id="fill-blanks-dnd" class=""> <div class="form-group"> <label> Enter Question: </label> <div id="fb-question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div> <button id="fb-addBlank"> Add Blank</button><button id="add-option-fb-dnd"> Add Option</button> </div> <div class="row margin-top-10"> <div id="fb-options-div" class="col-md-11"></div> <div id="fb-dd-extra-options-div" class="row"></div><div class="col-md-1"></div> <div class="col-md-12"></div> </div> </div><div class="form-group"><div class="AddHint"><label class="btn_removeHint" style="margin-top: 15px;">Solution/Explanation : </label><div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div></div></div>';
			break;		
						
		// case  to generate html for Assertion-Reason Question Type
			

		default:
			html = '';
			break;		
	}
	return html;
	setEventDeleteOptions();
}

function loadEventOfSelectedQuestionType(type) {
	switch(type) {
		case 1:
			scqLoadEditor();
			break;
		
		case 2:
			maqLoadEditor();
			break;

		case 3:
			trueFalseLoadEditor();
			break;
		
		

		case 6:
			fbLoadEditor();
			break;
			
		case 7:
			fbdDDLoadEditor();
			break;

		case 8:
			fbDndLoadEditor();
			break;

		
		default:
			
			break;

	}
}
function setEventDeleteOptions() {
	$('.fb-deleteOption').off('click');
	$('.fb-deleteOption').on('click', function() {
		
		//var remove = $(this).parents(".fb-options-div").find(".option").length;		
		var id=$(this).attr('data-id');
		//console.log(id);
		$(this).parents().eq(1).remove();
		$('#fb-question-box input[data-id='+id+']').remove();
		$('#question-box input[data-id='+id+']').remove();

		/*opt = $(this).parents(".fb-options-div").find(".option");
		$.each(opt,function(x,o) {
			remove = x+1;
			$(o).attr('placeholder','Option '+remove);
		});*/
		
	});
	$('.scq-deleteOption').off('click');
	$('.scq-deleteOption').on('click', function() {
		//alert("clicked on delete");
		var remove = $(this).parents(".scq-options-div").find(".option").length;
		if(remove > 2) {
			$(this).parents().eq(1).remove();
			opt = $(this).parents(".scq-options-div").find(".option");
			$.each(opt,function(x,o) {
				remove = x+1;
				$(o).attr('placeholder','Option '+remove);
			});
		}
	});

}