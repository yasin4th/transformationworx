function fbdDDLoadEditor() {
	setEventAddDropDown();
	setEventKeyup();
	//fb-ddQuestionEditor.destroy();
	fbDDQuestionEditor = CKEDITOR.inline('fb-dd-question-box');
}


function setEventAddDropDown() {
	// function to add option
	var i = 0;
	$('#fb-addDropDown').off('click');
	$('#fb-addDropDown').on('click', function(e) {
		e.preventDefault();
		if(parent_id != 0){
			i++;
			// i =  $('#fb-dd-options-div .option').length + 1;
			html = '<div data-id="dd'+i+'" class="option col-md-12">';
			html += '<label class="mrg-left10"> Drop Down '+i+' Options </label>';
			html += '<div class="row value"><div class="col-md-11 col-xs-11"><div class="col-md-1 col-xs-2 form-group radio-option"><div class="input-group">';
			html += '<div class="icheck-list">';
			html += '<label><input type="radio" name="radioOptionDrop'+i+'" data-answer="1"></label>';
			html += '</div>';
			html += '</div></div>';
			html += '<div class="col-md-10 col-xs-8 form-group">';
			html += '<input id="option" type="text" data-id="1" class="form-control" placeholder="Option">';
			html += '</div>';
			html += '<div class="col-md-1 col-xs-2">';
			html += '<a class="fb-dd-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a>';
			html += '</div>';
			html += '</div>';
			html += '<div class="col-md-1 col-xs-1"></div>';
			html += '</div>';
			html += '<div class="row value"><div class="col-md-11 col-xs-11"><div class="col-md-1 col-xs-2 form-group radio-option"><div class="input-group">';
			html += '<div class="icheck-list">';
			html += '<label><input type="radio" name="radioOptionDrop'+i+'" data-answer="2"></label>';
			html += '</div>';
			html += '</div></div>';
			html += '<div class="col-md-10 col-xs-8 form-group">';
			html += '<input id="option2" type="text" data-id="2" class="form-control" placeholder="Option">';
			html += '</div>';
			html += '<div class="col-md-1 col-xs-2">';
			html += '<a class="fb-dd-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a>';
			html += '</div></div>';
			html += '<div class="col-md-1 col-xs-1"></div></div><div class="row"><div class="col-md-12">';
			html += '<a class="fb-dd-addOption btn btn-circle btn-icon-only blue pull-right add-option"><i class="fa fa-plus"></i></a>';
			html += '</div></div></div>';
			divText = fbDDQuestionEditor.getData();
			$('#fb-dd-question-box').html(divText.slice(0,-5)+'<select disabled="true" data-id="dd'+i+'" style="width:10%"></select> </p>');
			$('#fb-dd-options-div').append(html);
			setEventAddOptionsInDropDown();
			setEventDeleteFbDDOptions(); // function set events to delete on options
			setEventKeyup();
		}else{
			i = $("#question-box").find("select").length + 1;
			//find all select
			//$("#question-box").find("select");
			


			html = '<div id="fb-dd'+i+'" data-id="dd'+i+'" class="option">';
			html += '<label class="mrg-left10"> Drop Down '+i+' Options </label>';
			html += '<div class="row value dd-option"><div class="col-md-11 col-xs-11"><div class="col-md-1 col-xs-2 form-group radio-option"><div class="input-group">';
			html += '<div class="icheck-list">';
			html += '<label><input type="radio" name="radioOptionDrop'+i+'" data-answer="1"></label>';
			html += '</div>';
			html += '</div></div>';
			html += '<div class="col-md-10 col-xs-8 form-group">';
			html += '<input id="option" type="text" data-id="1" class="form-control" placeholder="Option">';
			html += '</div>';
			html += '<div class="col-md-1 col-xs-2">';
			html += '<a class="fb-dd-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a>';
			html += '</div>';
			html += '</div>';
			html += '<div class="col-md-1 col-xs-1"></div>';
			html += '</div>';
			html += '<div class="row value dd-option"><div class="col-md-11 col-xs-11"><div class="col-md-1 col-xs-2 form-group radio-option"><div class="input-group">';
			html += '<div class="icheck-list">';
			html += '<label><input type="radio" name="radioOptionDrop'+i+'" data-answer="2"></label>';
			html += '</div>';
			html += '</div></div>';
			html += '<div class="col-md-10 col-xs-8 form-group">';
			html += '<input id="option2" type="text" data-id="2" class="form-control" placeholder="Option">';
			html += '</div>';
			html += '<div class="col-md-1 col-xs-2">';
			html += '<a class="fb-dd-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a>';
			html += '</div></div>';
			html += '<div class="col-md-1 col-xs-1"></div></div><div class="row"><div class="col-md-12">';
			//html += '<a class="fb-dd-addOption btn btn-circle btn-icon-only blue pull-right add-option"><i class="fa fa-plus"></i></a>';
			html += '<div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> </div> </div> </div>'
			html += '<div class="col-md-10 form-group"> <a class="fb-dd-addOption"> + Add More Option </a> </div>';
			html += '</div>';
			html += '</div></div>';
			divText = questioneditor.getData();
			$('#question-box').html(divText.slice(0,-5)+'<select disabled="true" data-id="dd'+i+'" style="width:10%"></select> </p>');
			$('#options-box').append(html);
			setEventAddOptionsInDropDown();
			setEventDeleteFbDDOptions(); // function set events to delete on options
			setEventKeyup();
		}
		Metronic.init();
	});
}

function setEventDeleteFbDDOptions() {
	$('.fb-dd-deleteOption').off('click');
	$('.fb-dd-deleteOption').on('click', function(e) {
		e.preventDefault();
		numberOfOptions = $(this).parents('.option').find('input:text').length;
		if(numberOfOptions > 2)
			$(this).parents('.row').eq(0).remove();
	})
}

function setEventAddOptionsInDropDown() {
	
	$('.fb-dd-addOption').off('click');
	$('.fb-dd-addOption').on('click', function(e) {
		e.preventDefault();
		if(parent_id != 0){
		    numberOfOptions = $(this).parents('.option').find('input:text').length;
			if(numberOfOptions < 6) {
			    var html = '<div class="row value"><div class="col-md-11 col-xs-11"><div class="col-md-1 col-xs-2 form-group radio-option"><div class="input-group"><div class="icheck-list"><label><input type="radio" name="radioOptionDrop1" data-answer="1"></label></div></div></div><div class="col-md-10 col-xs-8 form-group"><input id="option" type="text" data-id="1" class="form-control" placeholder="Option"></div><div class="col-md-1 col-xs-2"><a class="fb-dd-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div></div><div class="col-md-1 col-xs-1"></div></div>';
			    $(html).insertBefore($(this).parents('.row').eq(0))
				setEventDeleteFbDDOptions();
			}
		}else{
			var radioButtonName = $(this).data('name');
			var html = '<div class="row value dd-option"><div class="col-md-11 col-xs-11"><div class="col-md-1 col-xs-2 form-group radio-option"><div class="input-group"><div class="icheck-list"><label><input type="radio" name="'+radioButtonName+'" data-answer="1"></label></div></div></div><div class="col-md-10 col-xs-8 form-group"><input id="option" type="text" data-id="1" class="form-control" placeholder="Option"></div><div class="col-md-1 col-xs-2"><a class="fb-dd-deleteOption btn btn-circle btn-icon-only blue"><i class="fa fa-trash-o"></i></a></div></div><div class="col-md-1 col-xs-1"></div></div>';
			$(html).insertBefore($(this).parents('.row').eq(0))
			setEventDeleteFbDDOptions();
		}
		Metronic.init();
	})

}
function setEventKeyup() {
	$('#fb-dd-question-box').keyup(function(e) {
		
	    e.preventDefault();
	    blanks = $('#fb-dd-question-box select');
	    options = $('#fb-dd-options-div div[data-id]');
	   	//console.log(options);
	   
	    if (blanks.length != options.length) {
	        var arr=[];
	        $.each(blanks, function(i, blank) {
	            a = $(blank).data('id');
	            arr.push(a);			        
			});
			
			
			$.each(options, function(i, opt) {
	            a = $(opt).data('id');
	          
	            if(arr.indexOf(a)==-1){
	            	$(opt).remove();
	            }
			});

		}
	});

	$('#question-box').keyup(function(e) {
		
	    e.preventDefault();
	    blanks = $('#question-box select');
	    options = $('#options-box div[data-id]');
	   	//console.log(options);
	   
	    if (blanks.length != options.length) {
	        var arr=[];
	        $.each(blanks, function(i, blank) {
	            a = $(blank).data('id');
	            arr.push(a);			        
			});
			
			
			$.each(options, function(i, opt) {
	            a = $(opt).data('id');
	          
	            if(arr.indexOf(a)==-1){
	            	$(opt).remove();
	            }
			});

		}
	});


	/*$('#fb-dd-question-box').keyup(function(e) {
	    e.preventDefault();
	    blank = $('#fb-dd-question-box select').length;
	    option = $("#fb-dd-options-div .option").length;
	    if (blank != option) {
	        options = $("#fb-dd-options-div .option");
	        $.each(options, function(i, opt) {
	            selector = $(opt).data('id');
			        if ($('[data-id="' +selector+ '"]').length != 2)
			            $(opt).remove();
			})
		}
	});*/
}

function getOptionsFbDD() {
	var value = [];
	$.each($("#fb-dd-options-div .option"), function(i,option) {
		$.each($(option).find('.value'), function(x,val) {
			choises = {};
			//choises.option = $(option).html();
			choises.option = $(val).find('input:text').val();
			choises.number = i+1;
			choises.answer = 0;
			if($(val).find('input:radio').prop('checked'))
				choises.answer = 1;
			value.push(choises);
		});
	});
	return value;
}

function saveFbDD() {
	var req = getFbDD_Data();	
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			//resetPageValues();
			toastr.success(res.message,'Question Id: 00'+res.questionId);
		}
		else {
			toastr.error(res.message,'Error: Question Not Saved');
		}
	});
}

function getFbDD_Data () {
	var req = {};
	req.action = "addQuestion";
	req.questionData = fbDDQuestionEditor.getData();
	req.DescriptionData = "No Description.";
	if(hintEditor != undefined)
		req.DescriptionData = hintEditor.getData();
	req.parent_id = parent_id;
	req.questionType = 7;
	req.options = getOptionsFbDD();
	req.class_id = $("#classes").find('option:selected').data('id');
	req.subject_ids = getSelectedSubjectId();
	req.unit_ids = getSelectedUnitId();
	req.chapter_ids = getSelectedChapterId();
	req.topic_ids = getSelectedTopicId();
	req.tag_ids = getSelectedTagId();
	req.difficultyLevel_ids = getSelectedDifficultyLevelId();
	req.skill_ids = getSelectedskillId();
	return req;
}