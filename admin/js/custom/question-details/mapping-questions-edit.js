$(function() {
fetchMapKeyword();
});

// function to fetch map keyword
function fetchMapKeyword() {
	req = {};
	req.action = 'fetchMapKeywords';
	$.ajax({
		'type'  :   'post',
		'url'   :   EndPoint,
		'data'  :   JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillMapKeyword(res.mapKeywords);
			//mapKeywords = res.mapKeywords;
		}
		else{
			toastr.error('Error');
		}
	});

}

// function to fill map keyword
function fillMapKeyword(keywords){
	var html = "<option value='0'> Select Keyword </option>";
	$.each(keywords,function( i,keyw) {
		html += "<option value="+keyw.id+" data-id="+keyw.id+">" +keyw.map_keyword+ "</option>";
	});
	$('#map_keyword').html(html);
}


// function to fetch all questions details using keyword
function autoFillUsingKeyword(key_id){
	$("#units").val('').change();
	$("#chapters").val('').change();
	if(key_id == 0 )
	{
		toastr.error("Please Select Mapping Keyword.");
		return;
	}
	var req = {};
	req.action = 'getQuestionDetailsUsingKey';
	req.key_id = key_id;
	$.ajax({
		'type'  :   'post',
		'url'   :   EndPoint,
		'data'  :   JSON.stringify(req)

	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillQuestionDetailsUsingKey(res);
		}
		else if(res.status == 2)
		{
			window.location="404-error.php";
		}
		else{
			toastr.error(res.message);
		}
	});
}

function fillQuestionDetailsUsingKey(data){
	if(data.fill.classes != undefined)
		fillClasses(data.fill.classes);
	if((data.fill.subjects) != undefined)
		fillSubjects(data.fill.subjects);
	if((data.fill.units) != undefined)
		fillUnits(data.fill.units);
	if((data.fill.chapters) != undefined)
		fillChapters(data.fill.chapters);
	if((data.fill.topics) != undefined)
		fillTopics(data.fill.topics);
	if(data.fill.difficulty != undefined){
		fillDifficultyLevel(data.fill.difficulty);
	//updateTaggedDifficulty();
	}
	if(data.fill.skills != undefined)
		fillSkills(data.fill.skills);
	if(data.fill.tags != undefined)
		fillTags(data.fill.tags);

	$("#classes").val(data.keywordData.class_id);
	setTaggedData(data.tagged);
	// updateTaggedClass();
	// updateTaggedSubject();
	// updateTaggedUnit();
	// updateTaggedChapter();
	// updateTaggedTopic();
	// updateTaggedDifficulty();
	// updateTaggedSkill();
	// updateTaggedTag();

	// $("#map_keyword").val(data.keywordData.map_keyword);
	//$("#description").val(data.keywordData.description);
}