$(function() {
    questionMappingFormSubmit();
    updateQuestionMappingFormSubmit();
})


function questionMappingFormSubmit() {
    // event fired when admin adds a mapping
   $('#form-add-mapping').off('submit');
    $('#form-add-mapping').on('submit', function(e) {
        e.preventDefault();
        var req = {};
        req.action = "addMapping";

        if($('#map_keyword').val() != '')
        {
            req.keyword = $('#map_keyword').val();
        }
        else
        {
            toastr.error("Enter Mapping Keyword Name");
            return;
        }
        if(typeof($("#classes").find('option:selected').data('id')) !="undefined")
        {
            req.class_id = $("#classes").find('option:selected').data('id');
        }
        else
        {
            toastr.error("Select Class Name");
            return;
        }
        if(getSelectedSubjectId().length != 0)
        {
            req.subject_ids = getSelectedSubjectId();
        }
        else
        {
            toastr.error("Select Subject Name");
            return;
        }

        req.description = $('#description').val();
        req.unit_ids = getSelectedUnitId();
        req.chapter_ids = getSelectedChapterId();
        req.topic_ids = getSelectedTopicId();
        req.tag_ids = getSelectedTagId();
        req.difficultyLevel_ids = getSelectedDifficultyLevelId();
        req.skill_ids = getSelectedskillId();

        $.ajax({
            'type'  :   'post',
            'url'   :   EndPoint,
            'data'  :   JSON.stringify(req)
        }).done(function(res) {
            res = $.parseJSON(res);
            if(res.status == 1) {
                resetPageValues();
                toastr.success(res.message,'Mapping Keyword added successfully');
                // window.setTimeout(history.back(), 5000);
                setTimeout(function() {location.replace('mapping.php')}, 1000);
                // location.replace('mapping.php');
            }
            else {
                toastr.error(res.message,'Error: Mapping Keyword Not Saved');
            }
        });

    });
}

function updateQuestionMappingFormSubmit() {
    // event fired when admin update mapping
   $('#form-edit-mapping').off('submit');
    $('#form-edit-mapping').on('submit', function(e) {
        e.preventDefault();
        
        var req = {};
        req.action = "updateMapping";

        if($('#map_keyword_on_edit').val() != '')
        {
            req.keyword = $('#map_keyword_on_edit').val();
        }
        else
        {
            toastr.error("Enter Mapping Keyword Name");
            return;
        }
        if(typeof($("#classes").find('option:selected').data('id')) !="undefined")
        {
            req.class_id = $("#classes").find('option:selected').data('id');
        }
        else
        {
            toastr.error("Select Class Name");
            return;
        }
        if(getSelectedSubjectId().length != 0)
        {
            req.subject_ids = getSelectedSubjectId();
        }
        else
        {
            toastr.error("Select Subject Name");
            return;
        }

        req.map_id = getUrlParameter('map_id');
        req.description = $('#description').val();
        req.unit_ids = getSelectedUnitId();
        req.chapter_ids = getSelectedChapterId();
        req.topic_ids = getSelectedTopicId();
        req.tag_ids = getSelectedTagId();
        req.difficultyLevel_ids = getSelectedDifficultyLevelId();
        req.skill_ids = getSelectedskillId();

        $.ajax({
            'type'  :   'post',
            'url'   :   EndPoint,
            'data'  :   JSON.stringify(req)
        }).done(function(res) {
            res = $.parseJSON(res);
            if(res.status == 1) {
                resetPageValues();
                toastr.success(res.message,'Mapping Keyword updated successfully');
                // window.setTimeout(history.back(), 5000);
                setTimeout(function() {location.replace('mapping.php')}, 1000);
                // location.replace('mapping.php');
            }
            else {
                toastr.error(res.message,'Error: Mapping Keyword Not Updated');
            }
        });

    });
}


//FETCH ALL MAPPING DATA
function mappingData() {
        var req = {};
        req.action = "getMappingsData";
        $.ajax({
            'type'  :   'post',
            'url'   :   '../vendor/api.php',
            'data'  :   JSON.stringify(req)
        }).done(function(res) {
            res = $.parseJSON(res);
            fillData(res.data);
            
        });
    }
    function fillData(data) {
        var html = '';
        $.each(data, function(i, mapping){
            html += '<tr id="' + mapping.id + '" ><td>' + mapping.map_keyword + '</td><td>' + mapping.description + '</td><td><a class="edit" href="edit-mapping.php?map_id='+mapping.id+'">Edit</a></td><td><a class="delete" href="javascript:;">Delete</a></td></tr>';
        });
        $('#sample_editable_1 tbody').html(html);
            TableEditable.init();
        
    }


    //SCRIPT FOR DELETE MAPPING AND DATA TABLES

function delRow(oTable,row){
        var req = {};
        req.action = "DeleteMappingRow";
        req.id = $(row).attr('id');

        $.ajax({
            'type'  :   'post',
            'url'   :   '../vendor/api.php',
            'data'  :   JSON.stringify(req)
        }).done(function(res){
            res = $.parseJSON(res);
            if(res.status == 1) {
                oTable.fnDeleteRow(row); // delete from html
            }
            
            else {
                toastr.error(res.message);
            }
            
        });
    }

function editRow(oTable, nRow) {
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);
    
    if(($('#sample_editable_1 input').length) == 0) {
        jqTds[0].innerHTML = '<input id="name" type="text" class="form-control input-small" value="' + aData[0] + '" maxlength="20">';
        jqTds[1].innerHTML = '<input id="desc" type="text" class="form-control input-small" value="' + aData[1] + '" maxlength="200">';
        // jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
        // jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[3] + '">';
        jqTds[2].innerHTML = '<a class="edit" href="">Save</a>';
        jqTds[3].innerHTML = '<a class="cancel" href="">Cancel</a>';
    }
    else
        $('#save-row').modal('show');
}

function saveRow(oTable, nRow) {
    nRow = $('#sample_editable_1 input').parents('tr').eq(0);
    var jqInputs = $('input', nRow);
    oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
    oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
    // oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
    // oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
    //oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 2, false);
    oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 3, false);
    oTable.fnDraw();
}

var row;
var TableEditable = function () {

    var handleTable = function () {

        
        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            // oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            // oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
            oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 2, false);
            oTable.fnDraw();
        }
        
        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }
            oTable.fnDraw();
        }
        
        var table = $('#sample_editable_1');

        var oTable = table.dataTable({

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "lengthMenu": [
                [5,10, 15, 20, -1],
                [5,10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "paging":   true,
            "ordering": true, 

            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "columnDefs": [{ // set default column settings
                'orderable': false,
                'targets': 'no-sort'
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });

        var nEditing = null;
        var nNew = false;

        table.on('click', '.delete', function (e) {
            e.preventDefault();
            $('#delete-row').modal('show');

            // if (confirm("Are you sure to delete this row ?") == false) {
                // return;
            // }
            // var nRow = $(this).parents('tr')[0];
            
            // delRow(oTable,nRow); // sends ajax to delete row
            
            row = $(this).parents('tr')[0];
        });
        
        
        // Functions For MODALS
        
        $('#delete-row .delete-row').on('click', function() {       
                delRow(oTable,row); // sends ajax to delete row
                $('#delete-row').modal('hide');
        });
        // End Modals Functions

        table.on('click', '.cancel', function (e) {
            e.preventDefault();
        if (nNew || !$(nEditing).attr('id')) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

    }
    return {

        //main function to initiate the module
        init: function () {
            handleTable();
        }

    };

}();