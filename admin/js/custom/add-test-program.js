var batches;
$(function(){
	fetchClasses();
	setEventSaveTestPaper();

	CKEDITOR.config.customConfig = 'basic-toolbar-config.js';
	// CKEDITOR.disableAutoInline = true;

	$('#test-program-subjects').select2({placeholder: "Select Subjects"});
	fetchBatches();
	// event load subjects when admin select a class
	$('#test-program-class').off('change');
	$('#test-program-class').on('change', function(e) {
		e.preventDefault();
		$("#test-program-subjects").select2('val',null);
		$("#test-program-subjects").html('');
		fetchSubjects();

		// console.log(batches);
		if($('#test-program-class').val() != 0){
		fillBatches(batches,$('#test-program-class').val());
		}
		else
		{
			$('#batch').select2('val',null);
			$('#batch').html('');
		}
	});
	categoryData();
	showBatch();
});

function setEventSaveTestPaper() {
	$('#form-add-test-program').on('submit', function(e) {
		e.preventDefault();
		var req = {};
		req.action = "addNewTestProgram";
		req.name = 'No Name';
		req.cut_off = 0;
		req.price = 0;
		req.description = "No Description.";
		req.class = 0;
		req.credits = $('#credits').val();
		req.subjects = [];

			if($("#test-program-name").val() != '')
			{
				req.name = $("#test-program-name").val();
			}
			else
			{
				toastr.error("Enter Program Name");
				return;
			}


			if($("#test-program-price").val() != '')
				req.price = $("#test-program-price").val();


			if($("#cut-off").val() != '')
				req.cut_off = $("#cut-off").val();


			if($('#test-program-class').val() != 0){
				req.class = $('#test-program-class option:selected').data('id');
			}
			else{
				toastr.error("Please Select Class.");
				return;
			}


			if($('#test-program-type').val() == 2){
				req.package_type = $('#test-program-type').val();
			} else if($('#test-program-type').val() == 0){
				toastr.error("Please Select Package type.");
				return;

			} else {
				req.package_type = $('#test-program-type').val();
				if($('#batch').val() == 0){
					toastr.error("Please Select Batches.");
					return;
				}
				else{
					req.batch = $('#batch').select2('val');
				}
			}


			if($('#test-program-category').val() != 0 )
			{
				req.category = $('#test-program-category').val();
			}
			else
			{
				toastr.error("Please Select Category.");
				return;
			}


			if($('#test-program-subjects').val()==null)
			{
				toastr.error("Enter Subject");
				return;
			}


			if($('#test-program-subjects').val().length != 0)
				req.subjects = $("#test-program-subjects").select2('val');


			if(req.description = CKEDITOR.instances['test-program-description'].getData() != '')
				req.description = CKEDITOR.instances['test-program-description'].getData();

			req.validity = $('#validity').val();

			// req.valid_from = moment($('input[name=from]').val(), "DD/MM/YYYY").format("YYYY-MM-DD");
			// req.valid_till = moment($('input[name=to]').val(), "DD/MM/YYYY").add('1','days').format("YYYY-MM-DD");

			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res){
				res = $.parseJSON(res);
				if(res.status == 1)
				{
					 toastr.success(res.message);
					 // setTimeout(window.location = 'view-test-program1.php?test_program='+res.id+'', 10000);
					 window.location = 'view-test-program1.php?test_program='+res.id;
				}
				if(res.status == 0){
					toastr.error(res.message);
				}
			});

	});
}

function fetchClasses() {
	req = {};
	req.action = 'fetchClasses';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillClasses(res.classes);
		}
		else{
			toastr.error('Error');
		}
	});

}

// function to fill classes
function fillClasses(classes){
	var html = "<option value='0'> Select Class </option>";
	$.each(classes,function( i,cls) {
		html += "<option value='"+cls.id+"' data-id="+cls.id+">" +cls.name+ "</option>";
	});
	$('#test-program-class').html(html);
}

// function to fetch subjects
function fetchSubjects() {
	req = {};
	req.action = 'fetchSubjects';
	req.class_id = $("#test-program-class").find('option:selected').data('id');
	if(req.class_id) {
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fillSubjects(res.subjects);
			}
			else{
				$('#test-program-subjects').html('');
				// toastr.error('No Subject Found In Selected Class.');
			}
		});
	}
}

// function to fill subjects
function fillSubjects(subjects) {
	var html = "";
	$.each(subjects,function( i,sub) {
		html += '<option  value="'+sub.id+'" data-id="'+sub.id+'">'+sub.name+'</option>';
	});
	$('#test-program-subjects').html(html);
}

function validate()
{

	return true;
}

function categoryData() {
	var req = {};
	// req.limit = 5;
	// req.offset = 0;
	req.action = "getCategoryData";
	$.ajax({
		'type'	:	'post',
		'url'	:	'../vendor/api.php',
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		fillData(res.data);
	});
}
function fillData(data) {
	var html = '<option value="0"> Select Category </option>';
	$.each(data, function(i, difficulty){
		html += '<option value="' + difficulty.id + '" >' + difficulty.name + '</option>';
	});
	$('#test-program-category').html(html);

}

function showBatch(){
	$('#test-program-type').off('change');
	$('#test-program-type').on('change',function(){
		if($('#test-program-type').val() == "1" || $('#test-program-type').val() == "3"){
			html='<label class="col-md-3 control-label">Batch</label>\
										<div class="col-md-6">\
											<select id="batch" multiple="multiple" class="form-control" placeholder="Select Batches for the Test Package"></select>\
										</div>';
			$('#batch-frm-grp').html(html);
			$('#batch-frm-grp').removeClass('hidden');
			$('#batch').select2({placeholder: "Select Subjects"});
			if($('#test-program-type').val() == "1"){
			$('#price-frm-grp').addClass('hidden');
			}

			// $('#test-program-class').off('change');
			// $('#test-program-class').on('change',function(e){
			// 	e.preventDefault();
			// 	if($('#test-program-class').val() != 0){
			// 	fillBatches(batches,$('#test-program-class').val());
			// 	}
			// 	else
			// 	{	var html = "<option value='0'> Select Batch </option>";
			// 		$('#batch').html(html);
			// 	}
			// });


		}
		else{
			$('#batch-frm-grp').html('');
			$('#batch-frm-grp').addClass('hidden');
			$('#price-frm-grp').removeClass('hidden');
		}
	});


}

fetchBatches = function(){
	req = {};
	req.action = 'fetchBatches';
	$.ajax({
		'type'	:	'post',
		'url'	:	'../vendor/api.php',
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			// fillBatches(res.batches);
			// console.log(res.batches);
			batches = res.batches;
			if($('#test-program-class').val() != 0){
				fillBatches(batches,$('#test-program-class').val());
				}
				// console.log(batches);
		}
		else{
			toastr.error('Error');
		}
	});
}

fillBatches = function(batches,cls){
	var html = "<option value='0'> Select Batch </option>";
	$.each(batches,function( i, batch) {
		if(batch.class_id == cls){
		html += "<option  value='"+batch.id+"' data-id="+batch.id+">" +batch.name+ "</option>";
		}
	});
	$('#batch').html(html);
}