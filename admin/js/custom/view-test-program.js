_URL = window.URL || window.webkitURL;
test_program = getUrlParameter("test_program");
$(function() {
	fetchTestProgramDetails();
	fetchNumberOfAttachedTestPapers();

	// fetchNumberOfAttachedTestPapers();
	setEventOfUpdateTestPrograme();
	setEventAddTestPaper();
	setEventShowTestProgramDetails();
	saveAttachedTestProgram();
	setAjaxForm();

	CKEDITOR.config.customConfig = 'basic-toolbar-config.js';	

	CKEDITOR.disableAutoInline = true;

	// event load subjects when admin select a class
	$('#test-program-class').on('change', function(e) {
		$("#test-program-subjects").select2('val',null);
		$("#test-program-subjects").html('');
		fetchSubjects();
	});
	
	$('.view-test').on('click', function(e){
		e.preventDefault();
		type = $(this).parents('.dashboard-stat').find('.add-test-paper').data('type');
		window.location = 'view-test-1.php?test_program='+test_program+'&type='+type+'&parent_id=0&parent_type=0';
	})
	
	$('#change-test-program-image').on('change', function(e){
		e.preventDefault();
		if($('#change-test-program-image')[0].files[0].size < 1048576) {
			var file, img;
		    if ((file = this.files[0])) {
		        img = new Image();
		        img.onload = function () {
		            if(this.width == 300 && this.height == 200) {
						$('#test-program-form').submit();
		            }
		            else {
						toastr.error('Error: File Resolution Is Not  300 X 200 px.')
		            }
		        };
		        img.src = _URL.createObjectURL(file);
		    }
		}
		else
		{
			toastr.error('Error: File Size Larger Than 1 MB.')
		}
	})
	$('#program_id').val(test_program);
});

// function fetch Test Program Details Fromm DB
function fetchTestProgramDetails() {
	var req = {};
	req.action = "fetchTestProgramDetails";
	req.test_program = test_program;

	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			subjects = $.parseJSON('['+res.details.subjects+']');
			fillClasses(res.classesList);
			fillSubjects(res.subjectsList);
			fillSubjectsToAddTest(res.subjectsList,subjects);
			
			$('.test-program').html(res.details.name);
			$('a.test-program').attr('href','view-test-program.php?test_program='+res.details.id);
			$('#test-program-name').val(res.details.name);
			$('#test-program-status').val(res.details.status);
			$('#test-program-class').val(res.details.class);
			$('#test-program-subjects').val(subjects).select2();
			$('#test-program-price').val(res.details.price);
			$("#cut-off").val(res.details.cut_off);
			$('#test-program-description').html(res.details.description);
			$("#test-program-image").attr('src', '../'+res.details.image);
		}

	});
}

function fillSubjectsToAddTest (subjectList,subjects) {
	html = '';
	$.each(subjectList, function(i,subject) {
		$.each(subjects, function(x,sub) {
			if(subject.id == sub) {
				html += '<div class="col-md-4 col-sm-6 col-xs-12"> <div class="form-group sub-box tooltips" data-container="body" data-placement="top" data-original-title="'+subject.name+'"> <div class="sub-name"> <a href="chapters-in-test-program.php?test_program='+test_program+'&subject='+subject.id+'" data-id="'+subject.id+'">'+subject.name+'</a> </div> </div> </div>';
			}
		})
				
	})
	$('#subjects-div').html(html);
	Metronic.init();
}


// function to fetch classes
function fetchClasses() {
	req = {};
	req.action = 'fetchClasses';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillClasses(res.classes);
		}
		else{
			toastr.error('Error');
		}
	});
	
}

// function to fill classes
function fillClasses(classes){
	var html = "<option value='0'> Select Class </option>";
	$.each(classes,function( i,cls) {
		html += "<option value='"+cls.id+"' data-id="+cls.id+">" +cls.name+ "</option>";
	});
	$('#test-program-classes').html(html);
}

function setEventOfUpdateTestPrograme() {
	$("#update_program").off('click');
	$("#update_program").on('click', function(e) {
		e.preventDefault();
		var req = {};
		req.id = test_program;
		req.action = "updateTestProgram";
		req.name = $('#test-program-name').val();
		req.status = $('#test-program-status').val();
		req.class = $('#test-program-class').val();
		req.subjects = $('#test-program-subjects').val();
		req.price = $('#test-program-price').val();
		req.cut_off = $("#cut-off").val();
		req.description = CKEDITOR.instances['test-program-description'].getData();
		
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fetchTestProgramDetails();
				$('#edit-test-program-modal').modal("hide");
			}
			else{
				toastr.error('Error:'+res.message);
			}
		});
	});
}


function setEventShowTestProgramDetails() {
	$('#show-edit-test-program-modal').on('click', function(e){
		e.preventDefault();
		fetchNumberOfAttachedTestPapers();
		$('#edit-test-program-modal').modal("show");
		setTimeout(function(){ CKEDITOR.inline("test-program-description"); }, 1000);
	})
};

function setEventAddTestPaper() {
	$('.add-test-paper').on('click', function(e) {
		e.preventDefault();
		type = $(this).data('type');

		$('#test-paper-by-type-modal-table tbody').html('');
		$('#saveAttachedTestPaper').data('type',type)
		$('#test-paper-by-type-modal').modal("show");
		fetchTestPaperByType(type);
	})
}

function fetchTestPaperByType(type) {
	var req = {};
	req.action = "fetchTestPaperByType";
	req.type = type;
	req.class_id = $('#test-program-class').val();
	req.test_program = test_program;
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		 fillTestPaperByType(res.test_papers);
	});

}

function fillTestPaperByType(paper) {
	html = "";
	$.each(paper, function(x, testPaper) {
		html += '<tr><td> '+testPaper.name+'</td><td><div class="checkbox-list"><label><input type="checkbox" data-id="'+testPaper.id+'"></label></div></td></tr>';
	});
	$('#test-paper-by-type-modal-table tbody').html(html);

}

function saveAttachedTestProgram() {
	$('#saveAttachedTestPaper').off('click');
	$('#saveAttachedTestPaper').on('click', function(e) {
		e.preventDefault();
		$('#saveAttachedTestPaper').attr('disabled',true);
		var req = {};
		req.action = "saveAttachedTestPaper";
		req.test_program = test_program;
		var papers = [];
		req.type = $(this).data('type');
		req.parent_id = 0;
		req.parent_type = 0;
		checkboxes = $('#test-paper-by-type-modal-table input:checkbox');
		$.each(checkboxes, function(x, checkbox) {
			if($(checkbox).prop('checked')) {
				papers.push($(checkbox).data('id'));
			}
		});
		req.testPapers = papers;
		$.ajax({
			type: "post",
			url: EndPoint,
			data:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1)
				fetchNumberOfAttachedTestPapers();
				$('#test-paper-by-type-modal').modal("hide");
				$('#saveAttachedTestPaper').attr('disabled',false);
		});

	});
}



function fetchNumberOfAttachedTestPapers() {
	var req = {};
	req.action = "fetchNumberOfAttachedTestPapers";
	req.test_program = test_program;
	req.parent_id = 0;
	req.parent_type = 0;
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			
			// console.log(res);
			if(res.data.diagnostic != undefined){
				$('#diagnostic .count').text(res.data.diagnostic);
			}
			if(res.data.fullSyllabus != undefined){
				$('#full-syllabus-test .count').text(res.data.fullSyllabus);
			}
			if(res.data.scheduled != undefined){
				$('#scheduled-test .count').text(res.data.scheduled);
			}
	
		}
		else {
			toastr.error(res.message);
		}
	});
}

function fetchClasses() {
	req = {};
	req.action = 'fetchClasses';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillClasses(res.classes);
		}
		else{
			toastr.error('Error');
		}
	});
	
}

// function to fill classes
function fillClasses(classes){
	var html = "<option value='0'> Select Class </option>";
	$.each(classes,function( i,cls) {
		html += "<option value='"+cls.id+"' value='"+cls.id+"' data-id="+cls.id+">" +cls.name+ "</option>";
	});
	$('#test-program-class').html(html);
}

// function to fetch subjects
function fetchSubjects() {
	req = {};
	req.action = 'fetchSubjects';
	req.class_id = $("#test-program-class").find('option:selected').data('id');
	if(req.class_id) {
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fillSubjects(res.subjects);
			}
			else{
				$('#test-program-subjects').html('');
				// toastr.error('No Subject Found In Selected Class.');
			}
		});
	}
}

// function to fill subjects
function fillSubjects(subjects) {
	var html = "";
	$.each(subjects,function( i,sub) {
		html += '<option val="'+(i+1)+'" value="'+sub.id+'" data-id="'+sub.id+'">'+sub.name+'</option>';
	});
	$('#test-program-subjects').html(html);
}


function setAjaxForm() {
	$('form#test-program-form').ajaxForm({
        beforeSend: function(arr, $form, data) {
            console.log('starting');
        },
		uploadProgress: function(event, position, total, percentComplete) {
			//Progress bar
			$('#percentage').show();
			$('#percentage').html(percentComplete + '% done.') //update progressbar percent complete
        },
        success: function() {
            
        },
        complete: function(resp) {
        	$('#percentage').hide();
            res = $.parseJSON(resp.responseText);
            console.log(res);
            if(res.status == 1) {
				toastr.success('Image Updated.');
				$("#test-program-image").attr('src', '../'+res.image);
			}
			else
				toastr.error('An error occured. Please refresh the page and try again. Error Details: ' + res.message);
        },
		error: function() {
			console.log('Error');
        }
    });
}