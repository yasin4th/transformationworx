var test_id = getUrlParameter("test");
$(function() {
	fetchTestReport();
})

function fetchTestReport() {
	var req = {};
	req.action = "fetchSelectedTestReport";
	req.test_paper = test_id;
	
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		fillTestReport(res.data);
	})
}

function fillTestReport(students) {
	var html = '';
	$.each(students, function(i,student) {
		html += '<tr>';
		html += '<td> '+(i+1)+' </td>';
		html += '<td><a href="student-reports.php?test='+test_id+'&student='+student.user_id+'"> '+student.first_name+' '+student.last_name+'</a></td>';
		html += '<td> '+student.email+' </td>';
		html += '<td> '+student.total_questions+' </td>';
		html += '<td> '+student.attemted_questions+' </td>';
		html += '<td> '+student.correct_questions+' </td>';
		html += '<td> '+student.marks_archived+' </td>';
		html += '<td> '+parseFloat(student.percennt_marks).toFixed(2)+' </td>';
		html += '<td> '+parseFloat(student.speed).toFixed(2)+' </td>';
		// html += '<td> '+parseFloat(student.strike).toFixed(2)+' </td>';
		// html += '<td> '+parseFloat(student.eas).toFixed(2)+' </td>';
		// html += '<td> '+student.+' </td>';
		// html += '<td> '+student.total_marks+' </td>';


		html += '</tr>';
	});
	$("#test-report-table tbody").html(html);
	oTable = $('#test-report-table').dataTable({

        // set the initial value
       	"pageLength": 10,
       	"ordering": true,
        
        //"ordering": false, 

        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "columnDefs": [{ // set default column settings
            'orderable': false,
            'targets': 'no-sort'
        }, {
            "searchable": true,
            "targets": [0]
        }],
        "order": [
            [0, "asc"]
        ] 
    });
}