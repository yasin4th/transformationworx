$(function() {
	fetchStudentAssignments();
});

/*
* this function fetch assignment to display
*/
function fetchStudentAssignments() {

	var req = {};
	req.action = "fetchStudentAssignments";
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		if(res.status == 1){
			fillStudentAssignments(res.up_assignments);
		}
		else if(res.status == 0)
			toastr.error(res.message);
	});
}

/*
* this funtion genrate html and display assignment
*/
function fillStudentAssignments(up_assignments) {
	var html = "";
	var sno = 1;
	$.each(up_assignments, function(index,up_assignment) {
		html += '<tr data-id="'+up_assignment.id+'" data-db_status="'+up_assignment.status+'">';
		html += '<td><div style="padding:5px;">'+sno+'</div></td>';
		html += '<td><div style="padding:5px;">'+up_assignment.first_name+' '+up_assignment.last_name+'</div></td>';
		html += '<td><div style="padding:5px;">'+up_assignment.email+'</div></td>';
		html += '<td><div style="padding:5px;">'+up_assignment.class_name+'</div></td>';
		html += '<td><div style="padding:5px;">'+up_assignment.doc_name+'</div></td>';
		html += '<td><div style="padding:5px;">';
		if (up_assignment.doc_name != 'Send By E-mail') {
			html += ' <a href="../assets/'+up_assignment.path+''+up_assignment.doc_name+'" download title="Download Assignment"><i class="fa fa-download"></i></a>';
		}
		html += '</div> </td>';

/*		if(up_assignment.status == 0) {
			html += `<td><div style="padding:5px;"><select name="pass_fail" onchange="change_status(this.value, this, ${up_assignment.test_program_id})" title="Change Status"><option value="0" ${(up_assignment.status == 0 ? "selected" : "")}>Select</option><option value="1" ${(up_assignment.status == 1 ? "selected" : "")}>Pass</option><option value="2" ${(up_assignment.status == 2 ? "selected" : "")}>Fail</option></select></div></td>`;
		}else if(up_assignment.status == 1) {
			html += '<td>Pass</td>';
		} else {
			html += '<td>Fail</td>';
		}
		html += '<td><div style="padding:5px;"> <a href="../assets/'+up_assignment.path+''+up_assignment.doc_name+'" download title="Download Assignment"><i class="fa fa-download"></i></a> </div> </td></tr>';
*/
		html += '<td><div style="padding:5px;"><select name="pass_fail"><option value="0" '+(up_assignment.status == 0 ? "selected" : "")+'>Select</option><option value="1" '+(up_assignment.status == 1 ? "selected" : "")+'>Pass</option><option value="2" '+(up_assignment.status == 2 ? "selected" : "")+'>Fail</option></select></div></td>';

		html += `<td><div style="padding:5px;"><button class="btn btn-primary btn-xs" onclick="change_status(this,${up_assignment.test_program_id},'${up_assignment.package_name}',${up_assignment.user_id})">Submit</button></div></td></tr>`;
		//html += '<td><div style="padding:5px;"> <a href="../assets/'+up_assignment.path+'/'+up_assignment.doc_name+'" download title="Download Assignment"><i class="fa fa-download"></i></a> | <a class="pass_fail" style="color: rgb(230, 12, 12);font-weight: bold; text-align:center;"><i class="fa fa-trash-o"></i></a></div> </td></tr>';
		sno++;
	});
	if (typeof oTable != "undefined") {
		oTable.fnDestroy();
	}
	$('#student-assignment-table tbody').html(html);

	oTable = $('#student-assignment-table').dataTable();
	//return html;
}


function change_status(id_this, program, package_name, user_id){
	var req = {};
	req.as_status = $(id_this).parents('tr:eq(0)').find('td:eq(6)').find('select[name=pass_fail]').val();
	req.db_status = $(id_this).parents('tr:eq(0)').data('db_status');
	req.as_id = $(id_this).parents('tr:eq(0)').data('id');
	req.program = program;
	req.user_id = user_id;
	req.package_name = package_name;
	req.action = 'changeAssignmentStatus';
if(req.db_status == 0 && req.as_status !=0 && confirm('Are you sure, You want to change Status')){
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			if(req.as_status == 1) {
				location.replace(`generate-certificate.php?id=${req.as_id}`);
				return;
			}
			location.reload();
			//fetchStudentAssignments()
		}
		else {
			toastr.error(res.message);
		}
	});
}else{
	toastr.error('You can not change status');
}

}
