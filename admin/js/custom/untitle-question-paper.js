test_paper_id = getUrlParameter("test_paper");
var list = {};
var qs = [];
var importedQs;
var currentDate = new Date();
var mode = '0';

$(function() {
	$('#test-time').hide();
	fetchQuestions();
	setDateTimePicker();
	fetchTestPaperTypes();
	setEventInsertQuestion();
	setEventOnTestPaperTypeChange();
	fetchClasses();
	mathsjax();
	CKEDITOR.config.customConfig = 'basic-toolbar-config.js';
	CKEDITOR.disableAutoInline = true;

	$('#test-paper-subject').select2({placehoder: 'Select Subject'});
	// event load subjects when admin select a class
	$('#test-paper-class').on('change', function(e) {
		// $("#subjects").select2('val',null);
		$("#subjects").html('');
		fetchSubjects();
	});

	// function shows the modal of save test paper. this fired when admin hit the save test paper button
	$("#show-save-test-paper-modal").on('click', function(e){
		e.preventDefault();
		status = getUrlParameter('status');
		if (status == 0) {

			// CKEDITOR.inline("test-paper-instructions");
			$("#save-test-paper-modal").modal("show");
			$("#test-paper-instructions").attr('contenteditable',true);
			// from this line ck editor toolbar will not disable
			setTimeout(function(){ CKEDITOR.inline("test-paper-instructions"); }, 1000);
		}
		else if (status == 1) {
			updateTestPaperQuestions();
		}
	});

	$("#print-paper").on("click", function () {
		if($('#print-paper-modal input:radio:checked').length) {
			if(getUrlParameter('status') == 1) {
				if($('#print-paper-modal input:radio:checked').val() == 'with-answer') {
					url = 'print-paper-with-answer.php?test_paper='+test_paper_id;
				}
				else {
					url = 'print-paper-without-ans.php?test_paper='+test_paper_id;
				}
				window.open(url, "popupWindow", "width=600, height=400, scrollbars=yes")
			}
			else {
				toastr.info('You can\'t print a Test-Paper in Draft mode. Please save it first.');
			}
		}
		else {
			toastr.info('Please Choose an Option...');
		}
	});

	$("#cancel").on('click', function(e){
		e.preventDefault();
		if(getUrlParameter('status') == '0') {
			if(confirm('If you cancel, This test paper will be saved in Draft mode. Click Ok to discard it?')) {
				var req = {};
				req.action = 'deleteTestPaper';
				req.id = test_paper_id;
				$.ajax({
					'type'	:	'post',
					'url'	:	EndPoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1){
						window.location = 'test_paper.php';
					}
					else{
						toastr.error(res.message,'Error');
					}
				});

			}
		}
		else {
				window.location = 'test_paper.php';
		}
	});

	// function trigred when admin hit the save test paper button in the model
	$('#save-test-paper').on('click', function(e){
		e.preventDefault();
		var req = {};
		req.action = "saveTestPaper";
		req.id	=	getUrlParameter("test_paper");
		req.test_paper_name = $('#test-paper-name').val();
		req.test_paper_type = $('#test-paper-type').val();
		req.test_paper_class = $('#test-paper-class').val();
		req.test_paper_subject = $('#test-paper-subject').val();
		req.test_paper_time = $('#test-paper-time').val();
		if(req.test_paper_type == '5')
		{
			req.test_paper_attempts = 1;
		}
		else
		{
			req.test_paper_attempts = $('#test-paper-attempts').val();
		}
		req.test_paper_cutoff = $('#test-paper-cutoff').val();
		req.test_paper_start = $('#test-paper-start').val();
		req.test_paper_end = $('#test-paper-end').val();
		req.test_paper_instructions = CKEDITOR.instances['test-paper-instructions'].getData();
		req.questions = retriveQuestionIds();
		// req.questionIds = retriveQuestionIds();
		$.ajax({
			type	: "post",
			url: EndPoint,
			data: JSON.stringify(req)

		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				$("#save-test-paper-modal").modal("hide");
				window.location = "test_paper.php";
			}
		});

	});
});

/*
* funnction to show date picker when user select shedule test
*/
function setEventOnTestPaperTypeChange() {
	$('#test-paper-type').off('change');
	$('#test-paper-type').on('change', function(e) {
		e.preventDefault();
		if($('#test-paper-type').val() == 5)
		{
			$('#attempts').hide('slow');
			$('#test-time').fadeIn();
		}
		else if($('#test-paper-type').val() == 6)
		{
			$('#test-time').fadeOut();
			$('#marks-time-div').hide('slow');
		}
		else
		{
			$('#attempts').hide('slow');
			$('#marks-time-div').show('slow');
			$('#test-time').fadeOut();
		}
	});
}

// this function fetch questions of test papers
function fetchQuestions() {
	if(getUrlParameter("test_paper") != '' && getUrlParameter("status") != '') {
		if(getUrlParameter("status") == 1) {
			fetchTestPaperDetails();
			fetchQuestionsOfTestPaper();
		}
		else {
			setQuestionsOfTestPaper();
		}
	}
}

function setQuestionsOfTestPaper() {
	var req	=	{};
	req.action = "setQuestionsOfTestPaper";
	req.testPaper = getUrlParameter("test_paper");
	$.ajax({
		'type' : 'post',
		'url'	:	EndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);

		if (res.status == 1) {
			html = creatPaper(res.data);
			$('#sections').html(html);

			setEventShowQuestionInformation();
			setEventImportQuestion();
			setEventNextQuestion();
			mathsjax();
			//MathJax.Hub.Typeset();
			Metronic.init();
		}
		else {
			toastr.error(res.message);
			setTimeout(function(){ history.back(); }, 1500);
		}
	});
}

function updateTestPaperQuestions() {
	var req = {};
	req.action = "updateTestPaperQuestions";
	req.test_paper_id	=	test_paper_id;
	req.questions = retriveQuestionIds();
	$.ajax({
		type	: "post",
		url: EndPoint,
		data: JSON.stringify(req)

	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			window.location = "test_paper.php";
		}
		else {
			toastr.error(res.message,"ERROR:")
		}

	});
}


// function to create html of test paper when question are not set
function creatPaper(sections) {
	html = '';
	$.each(sections, function(y,section) {
		question_number = 0;
		// html += '<div data-s_id="'+section.section.id+'" data-totalQuestion="'+section.section.total_question+'" class="section portlet box blue"> <div class="portlet-title"> <div class="caption"> <i class="fa fa-gift"></i> '+section.section.name+'</div> <div class="tools"> <a href="#import-section-modal" data-toggle="modal"><span class="white i-font-size"><i class="fa fa-cloud-download" style="margin-top:0px"></i></span></a> <a class="collapse"></a> </div> </div> <div class="portlet-body form"> <div id="questions" class="form-body">';
		html += '<div data-s_id="'+section.section.id+'" data-totalQuestion="'+section.section.total_question+'" class="section portlet box blue"> <div class="portlet-title"> <div class="caption"> <i class="fa fa-pencil"></i> '+section.section.name+'</div> <div class="tools"> <a class="collapse"></a> </div> </div> <div class="portlet-body form"> <div id="questions" class="form-body">';

		$.each(section.questionData, function(i,questionData) {
				$.each(questionData.questions, function(a,question) {
					if(list[questionData.lable.id] == undefined)
						list[questionData.lable.id] = [];
					list[questionData.lable.id].push(question.id);
					qs.push(question.id);

					question.section_id = questionData.lable.section_id;
					question.lable_id = questionData.lable.id;
					question.max_marks = questionData.lable.max_marks;
					question.neg_marks = questionData.lable.neg_marks;
					question.question_id = question.id;

					html += getQuestionHtml (question_number+1,question)
					question_number += 1;
				});
			});

		html += '</div> </div></div>';
	});
	return html;
}

// this function fetch saved questions of test papers
function fetchQuestionsOfTestPaper() {
	var req	=	{};
	req.action = "fetchQuestionsOfTestPaper";
	req.testPaper = getUrlParameter("test_paper");

	$.ajax({
		'type' : 'post',
		'url'	:	EndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);

		if(res.status == 1) {
			html = genratePaper(res.data);
			$('#sections').html(html);
			setEventShowQuestionInformation();
			setEventImportQuestion();
			setEventNextQuestion();
			mathsjax();
			//MathJax.Hub.Typeset();
			Metronic.init();
		}
		else {
			toastr.error(res.message,"ERROR:")
		}
	});
}


// function to create html of test paper when question are set
function genratePaper(sections) {
	var html = "";

	$.each(sections, function(i,section) {

		html += '<div data-s_id="'+section.section.id+'" data-totalQuestion="'+section.section.total_question+'" class="section portlet box blue"> <div class="portlet-title"> <div class="caption"> <i class="fa fa-pencil"></i> '+section.section.name+'</div> <div class="tools"> <a class="collapse"></a> </div> </div> <div class="portlet-body form"> <div id="questions" class="form-body">';

		$.each(section.section.questionData, function(x,question) {
			if(list[question.lable_id] == undefined)
				list[question.lable_id] = [];
			list[question.lable_id].push(question.question_id);
			html += getQuestionHtml (x+1,question);

		});
		html += '</div> </div></div>';
	});
	return html;
}

var parent=0;

function getQuestionHtml (number,question) {
	html = '';

	switch(parseInt(question.type)) {

		case 1:

			html += '<div data-q_id="'+ question.question_id +'" data-section_id="'+question.section_id+'" data-lable_id="'+question.lable_id+'" data-marks="'+question.max_marks+'" data-neg-marks="'+question.neg_marks+'" data-type="'+question.type+'" class="question portlet box purple"> <div class="portlet-title"> ';
			if(parent == 0){
				html += '<div class="caption"> Question '+number+' </div> ';
			}else{
				html += '<div class="caption"> Question '+parent+'.'+number+'</div>' ;
			}
			html += '<div class="tools">';

			if(mode=='0')
			{
				html += '<a class="nextQuestion"><span class="white i-font-size"><i class="fa fa-forward" style="margin-top:0px"></i></span></a> <a class="show-import-question-modal"><span class="white i-font-size"><i class="fa fa-cloud-download" style="margin-top:0px"></i></span></a>';
			}
			html += '<a href="" class="collapse"></a> </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> <a class="show-question-info-modal"><span class="i-circle"><i class="fa fa-info"></i></span></a> </div> <div class="col-md-11 col-xs-10"> <div class="option row"><div class="col-md-12 col-xs-12"><div class="row">';
			$.each(question.options,function(z,option) {
				html += '<div class="col-md-12"><div class="qp-opt-border form-group"><span class="pull-left mrg-right10">';

				html += '<input type="radio" name="option" disabled/>';

				html += '</span>';
				html += option.option.esc();

				html += '</div></div>';
			});
			html += '</div> </div></div> </div> </div>';
			html += '<div class="row">  <div class="col-md-12"> <label style="font-weight: 700;">Solution/Explanation</label> <div class="sol-exp-border">'+question.description+'</div> </div> </div>';
			html += '</div> </div> </div>';

		break;


		case 2:
			html += '<div data-q_id="'+ question.question_id +'" data-section_id="'+question.section_id+'" data-lable_id="'+question.lable_id+'" data-marks="'+question.max_marks+'" data-neg-marks="'+question.neg_marks+'" data-type="'+question.type+'" class="question portlet box purple"> <div class="portlet-title"> ';
			if(parent == 0){
				html += '<div class="caption"> Question '+number+' </div> ';
			}else{
				html += '<div class="caption"> Question '+parent+'.'+number+'</div>' ;
			}
			html += '<div class="tools">';
			if(mode=='0'){
				html += ' <a class="nextQuestion"><span class="white i-font-size"><i class="fa fa-forward" style="margin-top:0px"></i></span></a> <a class="show-import-question-modal"><span class="white i-font-size"><i class="fa fa-cloud-download" style="margin-top:0px"></i></span></a>';
			}
			html += ' <a href="" class="collapse"></a> </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> <a class="show-question-info-modal"><span class="i-circle"><i class="fa fa-info"></i></span></a> </div> <div class="col-md-11 col-xs-10"> <div class="option row"><div class="col-md-12 col-xs-12"><div class="row">';
			$.each(question.options,function(z,option) {
				html += '<div class="col-md-12"><div class="qp-opt-border form-group"><span class="pull-left mrg-right10">';
				html += '<input type="checkbox" name="option" disabled/>';
				html += '</span>'+option.option.esc()+'</div></div>';
			});
			html += '</div> </div></div> </div> </div>';
			html += '<div class="row">  <div class="col-md-12"> <label style="font-weight: 700;">Solution/Explanation</label> <div class="sol-exp-border">'+question.description+'</div> </div> </div>';
			html += '</div> </div> </div>';
		break;


		case 3:
			html += '<div data-q_id="'+ question.question_id +'" data-section_id="'+question.section_id+'" data-lable_id="'+question.lable_id+'" data-marks="'+question.max_marks+'" data-neg-marks="'+question.neg_marks+'" data-type="'+question.type+'" class="question portlet box purple"> <div class="portlet-title"> ';
			if(parent == 0){
				html += '<div class="caption"> Question '+number+' </div> ';
			}else{
				html += '<div class="caption"> Question '+parent+'.'+number+'</div>' ;
			}
			html += '<div class="tools">';
			if(mode=='0'){
				html += ' <a class="nextQuestion"><span class="white i-font-size"><i class="fa fa-forward" style="margin-top:0px"></i></span></a> <a class="show-import-question-modal"><span class="white i-font-size"><i class="fa fa-cloud-download" style="margin-top:0px"></i></span></a>';
			}
			html += ' <a href="" class="collapse"></a> </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> <a class="show-question-info-modal"><span class="i-circle"><i class="fa fa-info"></i></span></a> </div> <div class="col-md-11 col-xs-10"> <div class="option row"><div class="col-md-12 col-xs-12"><div class="row">';
			$.each(question.options,function(z,option) {
				html += '<div class="col-md-12"><div class="qp-opt-border form-group"><span class="pull-left mrg-right10"><input type="radio" name="option" disabled/></span>'+option.option+'</div></div>';
			});
			html += '</div> </div></div> </div> </div>';
			html += '<div class="row">  <div class="col-md-12"> <label style="font-weight: 700;">Solution/Explanation</label> <div class="sol-exp-border">'+question.description+'</div> </div> </div>';
			html += '</div> </div> </div>';
		break;


		case 4:
			html += '<div data-q_id="'+ question.question_id +'" data-section_id="'+question.section_id+'" data-lable_id="'+question.lable_id+'" data-marks="'+question.max_marks+'" data-neg-marks="'+question.neg_marks+'" data-type="'+question.type+'" class="question portlet box purple"> <div class="portlet-title"> <div class="caption"> Question '+number+' </div> <div class="tools">';
			if(mode=='0'){
				html += ' <a class="nextQuestion"><span class="white i-font-size"><i class="fa fa-forward" style="margin-top:0px"></i></span></a> <a class="show-import-question-modal"><span class="white i-font-size"><i class="fa fa-cloud-download" style="margin-top:0px"></i></span></a>';
			}
			html += ' <a href="" class="collapse"></a> </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> <a class="show-question-info-modal"><span class="i-circle"><i class="fa fa-info"></i></span></a> </div> <div class="col-md-11 col-xs-10"> <div class="option row"><div class="col-md-12 col-xs-12"><div class="row">';
			$.each(question.options,function(z,option) {
				html += '<div class="col-md-12"><div class="qp-opt-border form-group"><span class="pull-left mrg-right10">(&nbsp;&nbsp; )</span>'+option.option+'</div></div>';
			});
			html += '</div> </div></div> </div> </div>';
			html += '<div class="row">  <div class="col-md-12"> <label style="font-weight: 700;">Solution/Explanation</label> <div class="sol-exp-border">'+question.description+'</div> </div> </div>';
			html += '</div> </div> </div>';
		break;


		case 5:
			html += '<div data-q_id="'+ question.question_id +'" data-section_id="'+question.section_id+'" data-lable_id="'+question.lable_id+'" data-marks="'+question.max_marks+'" data-neg-marks="'+question.neg_marks+'" data-type="'+question.type+'" class="question portlet box purple"> <div class="portlet-title"> <div class="caption"> Question '+number+' </div> <div class="tools">';
			if(mode=='0'){
				html += ' <a class="nextQuestion"><span class="white i-font-size"><i class="fa fa-forward" style="margin-top:0px"></i></span></a> <a class="show-import-question-modal"><span class="white i-font-size"><i class="fa fa-cloud-download" style="margin-top:0px"></i></span></a>';
			}
			html += ' <a href="" class="collapse"></a> </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> <a class="show-question-info-modal"><span class="i-circle"><i class="fa fa-info"></i></span></a> </div> <div class="col-md-11 col-xs-10"> <div class="option row"><div class="col-md-12 col-xs-12"><div class="row text-center"> <div class="col-md-6"> <div class="col-md-11" > <div class="row" style="border: 1px solid #ccc; padding: 10px 10px 0px;">';
			$.each(question.options, function(z,option) {
				if(option.number == 1) {
					html += '<div class="col-md-12 mrg-bot10" style="border: 1px solid #ccc;">'+option.option+'</div>';
				}
			});
			html += '</div> </div> </div> <div class="col-md-6"> <div class="col-md-11" > <div class="row" style="border: 1px solid #ccc; padding: 10px 10px 0px">';
			$.each(question.options, function(z,option) {
				if(option.number == 2) {
					html += '<div class="col-md-12 mrg-bot10" style="border: 1px solid #ccc;">'+option.option+'</div>';
				}
			});
			html += '</div> </div> </div>';
			html += '</div> </div></div> </div> </div>';
			html += '<div class="row">  <div class="col-md-12"> <label style="font-weight: 700;">Solution/Explanation</label> <div class="sol-exp-border">'+question.description+'</div> </div> </div>';
			html += '</div> </div> </div>';
		break;



		case 6:
			html += '<div data-q_id="'+ question.question_id +'" data-section_id="'+question.section_id+'" data-lable_id="'+question.lable_id+'" data-marks="'+question.max_marks+'" data-neg-marks="'+question.neg_marks+'" data-type="'+question.type+'" class="question portlet box purple"> <div class="portlet-title"> ';
			if(parent == 0){
				html += '<div class="caption"> Question '+number+' </div> ';
			}else{
				html += '<div class="caption"> Question '+parent+'.'+number+'</div>' ;
			}
			html += '<div class="tools">';
			if(mode=='0'){
				html += ' <a class="nextQuestion"><span class="white i-font-size"><i class="fa fa-forward" style="margin-top:0px"></i></span></a> <a class="show-import-question-modal"><span class="white i-font-size"><i class="fa fa-cloud-download" style="margin-top:0px"></i></span></a>';
			}
			html += ' <a href="" class="collapse"></a> </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> <a class="show-question-info-modal"><span class="i-circle"><i class="fa fa-info"></i></span></a> </div> <div class="col-md-11 col-xs-10"> <div class="option row"><div class="col-md-12 col-xs-12"><div class="row">';
			html += '</div> </div></div> </div> </div>';
			html += '<div class="row">  <div class="col-md-12"> <label style="font-weight: 700;">Solution/Explanation</label> <div class="sol-exp-border">'+question.description+'</div> </div> </div>';
			html += '</div> </div> </div>';
		break;


		case 7:
			html += '<div data-q_id="'+ question.question_id +'" data-section_id="'+question.section_id+'" data-lable_id="'+question.lable_id+'" data-marks="'+question.max_marks+'" data-neg-marks="'+question.neg_marks+'" data-type="'+question.type+'" class="question portlet box purple"> <div class="portlet-title"> ';
			if(parent == 0){
				html += '<div class="caption"> Question '+number+' </div> ';
			}else{
				html += '<div class="caption"> Question '+parent+'.'+number+'</div>' ;
			}
			html += '<div class="tools">';
			if(mode=='0'){
				html += ' <a class="nextQuestion"><span class="white i-font-size"><i class="fa fa-forward" style="margin-top:0px"></i></span></a> <a class="show-import-question-modal"><span class="white i-font-size"><i class="fa fa-cloud-download" style="margin-top:0px"></i></span></a>';
			}
			html += ' <a href="" class="collapse"></a> </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> <a class="show-question-info-modal"><span class="i-circle"><i class="fa fa-info"></i></span></a> </div> <div class="col-md-11 col-xs-10"> <div class="option row"><div class="col-md-12 col-xs-12"><div class="row">';

			for (var x = 1; x <= question.options[question.options.length - 1].number; x++) {
				radioButtonName = parseInt(Math.random() * 1000);

				html += '<div id="fb-dd'+ x +'"> <label class="mrg-left10"> Drop Down '+ x +' Options </label>';
				html += ' <div class="row">';

				$.each(question.options ,function(i,option) {
					if (option.number == x) {

						html += ' <div class="col-md-12"> <div class="col-md-1 form-group radio-option"> <div class="input-group"> <div class="icheck-list"> ';

						html += '<label><input type="radio" name="'+radioButtonName+'" disabled></label>'

						html += ' </div> </div> </div> <div class="options col-md-10 form-group"> <div class="form-control">'+option.option+' </div> </div> <div class="col-md-1"></div> </div>';
					};

				});

				html += ' </div> ';
				html += ' </div> ';
			};

			html += '</div> </div></div> </div> </div>';
			html += '<div class="row">  <div class="col-md-12"> <label style="font-weight: 700;">Solution/Explanation</label> <div class="sol-exp-border">'+question.description+'</div> </div> </div>';
			html += '</div> </div> </div>';
		break;


		case 8:
			html += '<div data-q_id="'+ question.question_id +'" data-section_id="'+question.section_id+'" data-lable_id="'+question.lable_id+'" data-marks="'+question.max_marks+'" data-neg-marks="'+question.neg_marks+'" data-type="'+question.type+'" class="question portlet box purple"> <div class="portlet-title"> ';
			if(parent == 0){
				html += '<div class="caption"> Question '+number+' </div> ';
			}else{
				html += '<div class="caption"> Question '+parent+'.'+number+'</div>' ;
			}
			html += '<div class="tools">';
			if(mode=='0'){
				html += ' <a class="nextQuestion"><span class="white i-font-size"><i class="fa fa-forward" style="margin-top:0px"></i></span></a> <a class="show-import-question-modal"><span class="white i-font-size"><i class="fa fa-cloud-download" style="margin-top:0px"></i></span></a>';
			}
			html += ' <a href="" class="collapse"></a> </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> <a class="show-question-info-modal"><span class="i-circle"><i class="fa fa-info"></i></span></a> </div> <div class="col-md-11 col-xs-10"> <div class="option row"><div class="col-md-12 col-xs-12"><div class="row">';
			$.each(question.options,function(z,option) {
				html += '<div class="col-md-12"><div class="qp-opt-border form-group">'+option.option+'</div></div>';
			});
			html += '</div> </div></div> </div> </div>';
			html += '<div class="row">  <div class="col-md-12"> <label style="font-weight: 700;">Solution/Explanation</label> <div class="sol-exp-border">'+question.description+'</div> </div> </div>';
			html += '</div> </div> </div>';
		break;


		case 11:
			html += '<div data-q_id="'+ question.question_id +'" data-section_id="'+question.section_id+'" data-lable_id="'+question.lable_id+'" data-marks="'+question.max_marks+'" data-neg-marks="'+question.neg_marks+'" data-type="'+question.type+'" class="question portlet box purple"> <div class="portlet-title"> <div class="caption"> Question '+number+' </div> <div class="tools">';
			if(mode=='0'){
				html += ' <a class="nextQuestion"><span class="white i-font-size"><i class="fa fa-forward" style="margin-top:0px"></i></span></a> <a class="show-import-question-modal"><span class="white i-font-size"><i class="fa fa-cloud-download" style="margin-top:0px"></i></span></a>';
			}
			html += ' <a href="" class="collapse"></a> </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> <a class="show-question-info-modal"><span class="i-circle"><i class="fa fa-info"></i></span></a> </div> <div class="col-md-11 col-xs-10"> <div class="option row"><div class="col-md-12 col-xs-12"><ol class="upper-alpha">';

			html += '<ol> </div></div> </div> </div>';

			$.each(question.passageQuestions,function(z,passageQuestion) {
				parent = number;
				html += '<div class="row"> <div class="col-md-1"> </div>  <div class="col-md-11"> ';
				html += getQuestionHtmlForPassageQuestion(z+1,passageQuestion);
				html += '</div> </div>';
				parent = 0;
			});


			// html += '<div class="row">  <div class="col-md-12"> <label style="font-weight: 700;">Solution/Explanation</label> <div class="">'+question.description+'</div> </div> </div>';

			html += '</div> </div> </div>';
		break;

		case 12:

			html += '<div data-q_id="'+ question.question_id +'" data-section_id="'+question.section_id+'" data-lable_id="'+question.lable_id+'" data-marks="'+question.max_marks+'" data-neg-marks="'+question.neg_marks+'" data-type="'+question.type+'" class="question portlet box purple"> <div class="portlet-title"> <div class="caption"> Question '+number+' </div> <div class="tools">';
			if(mode=='0'){
				html += ' <a class="nextQuestion"><span class="white i-font-size"><i class="fa fa-forward" style="margin-top:0px"></i></span></a> <a class="show-import-question-modal"><span class="white i-font-size"><i class="fa fa-cloud-download" style="margin-top:0px"></i></span></a>';
			}
			html += ' <a href="" class="collapse"></a> </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> <a class="show-question-info-modal"><span class="i-circle"><i class="fa fa-info"></i></span></a> </div> <div id="matrix-options-div" class="col-md-11 matrix-table-options"> <div class="margin-bottom-10 table-scrollable"><table id="matrix-table" class="table table-bordered table-striped table-hover"><thead><tr><th></th>';

			/*
			* creating table head
			*/
			$.each(question.options, function(z,option) {
				if(option.answer == 1) {
					html += '<th> <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;">'+ option.option +'</div> </th>';
				}
			});
			html += '</tr> </thead> ';


			/*
			* creating table body
			*/
			var row = 2;
			html += '<tbody>'
			$.each(question.options ,function(i,option) {
				if(option.answer == row) {
					html += '<tr> <td> <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"> '+option.option+' </div></td>';
					option.answer = 0;
					$.each(question.options ,function(x,opt) {
						if(opt.answer == row) {
							if(opt.option == 1) {
								html += '<td><div class="checkbox-list"><label><input type="checkbox" checked="true" disabled></label></div></td>';
							}
							else {
								html += '<td><div class="checkbox-list"><label><input type="checkbox" disabled></label></div></td>';
							}
						}
					});
					row++;
				}
			});
			html += '</tbody></table></div> </div> </div>';

			html += '<div class="row">  <div class="col-md-12"> <label style="font-weight: 700;">Solution/Explanation</label> <div class="sol-exp-border">'+question.description+'</div> </div> </div>';
			html += '</div> </div> </div>';
		break;


		case 13:
			html += '<div data-q_id="'+ question.question_id +'" data-section_id="'+question.section_id+'" data-lable_id="'+question.lable_id+'" data-marks="'+question.max_marks+'" data-neg-marks="'+question.neg_marks+'" data-type="'+question.type+'" class="question portlet box purple"> <div class="portlet-title"> <div class="caption"> Question '+number+' </div> <div class="tools">';
			if(mode=='0'){
				html += ' <a class="nextQuestion"><span class="white i-font-size"><i class="fa fa-forward" style="margin-top:0px"></i></span></a> <a class="show-import-question-modal"><span class="white i-font-size"><i class="fa fa-cloud-download" style="margin-top:0px"></i></span></a>';
			}
			html += ' <a href="" class="collapse"></a> </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> <a class="show-question-info-modal"><span class="i-circle"><i class="fa fa-info"></i></span></a> </div> <div class="col-md-11 col-xs-10"> <div class="option row"><div class="col-md-12 col-xs-12"><div class="row">';
			$.each(question.options,function(z,option) {
				if (option.number ==  1) {
					from = option.option;
				};
				if (option.number ==  2) {
					to = option.option;
				};
			});
			html += '<div class="col-md-6"><div class="qp-opt-border form-group"><lable>From: </lable>'+from+'</div></div>';
			html += '<div class="col-md-6"><div class="qp-opt-border form-group"><lable>To: </lable>'+to+'</div></div>';
			html += '</div> </div></div> </div> </div>';
			html += '<div class="row">  <div class="col-md-12"> <label style="font-weight: 700;">Solution/Explanation</label> <div class="sol-exp-border">'+question.description+'</div> </div> </div>';
			html += '</div> </div> </div>';
		break;


		default:
		break;

	}
	return html;
}


function getQuestionHtmlForPassageQuestion(number,question) {
html = '';
	switch(parseInt(question.type)) {

		default:
			html += '<div data-q_id="'+ question.question_id +'" data-section_id="'+question.section_id+'" data-lable_id="'+question.lable_id+'" data-marks="'+question.max_marks+'" data-neg-marks="'+question.neg_marks+'" data-type="'+question.type+'" class="portlet box purple"> <div class="portlet-title">';
			if(parent == 0){
				html += '<div class="caption"> Question '+number+' </div> ';
			}else{
				html += '<div class="caption"> Question '+parent+'.'+number+'</div>' ;
			}
			html += '<div class="tools">';
			html += ' <a href="" class="collapse"></a> </div> </div> <div class="portlet-body form"> <div class="form-body"> <div class="well">'+question.question+'</div> <div class="row"> <div class="col-md-1 col-xs-2"> </div> <div class="col-md-11 col-xs-10"> <div class="option row"><div class="col-md-12 col-xs-12"><div class="row">';
			$.each(question.options,function(z,option) {
				html += '<div class="col-md-12"><div class="qp-opt-border form-group"><span class="pull-left mrg-right10"><input type="radio" name="option" disabled/></span>'+option.option+'</div></div>';
			});
			html += '</div> </div></div> </div> </div>';
			html += '<div class="row">  <div class="col-md-12"> <label style="font-weight: 700;">Solution/Explanation</label> <div class="sol-exp-border">'+question.description+'</div> </div> </div>';
			html += '</div> </div> </div>';
		break;
	}

	return html;

}

function retriveQuestionIds() {
	test_paper = test_paper_id;
	var questionData = [];
	questions = $('.question');

	$.each(questions, function(e,question){
		var data = {};
		data.test_paper = test_paper;
		data.question_id = $(question).data('q_id');
		data.max_marks = $(question).data('marks');
		data.neg_marks = $(question).data('neg-marks');
		data.lable_id = $(question).data('lable_id');
		data.section_id = $(question).data('section_id');
		questionData.push(data);
	});
	return questionData;
}

function fetchTestPaperDetails() {
	var req = {};
	req.action = "fetchTestPaperDetails";
	req.test_paper_id = test_paper_id;
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillPaperTypesDetails(res.data);
			mode = res.data.mode;
		}
		else {
			toastr.error(res.message,"ERROR:");
		}
	});
}

function fillPaperTypesDetails(data) {
	// console.log(data);
	$('#test-paper-details').modal({backdrop: 'static'});
		name = data.name;
		type = data.type;
		totalAttempts = data.total_attempts;
		startTime = data.start_time;
		endTime = data.end_time;
		instructions = data.instructions;
		status = data.status;
		paper_id = data.id;

		$('.name').text(name);

		if(startTime == 0)
			$('#test-paper-schedule').prop('checked', false);
		else if(parseInt(startTime) != 0)
			$('#test-paper-schedule').prop('checked', true);

		if(!$('#test-paper-schedule').prop('checked'))
			$('#test-time').hide();
		else
			$('#test-time').show();
		$('#test-paper-details').modal('show');
		$('#test-paper-details').find('.name').val(name);

		$('#test-paper-id').val(paper_id);
		$('#test-paper-name').val(name);
		$('#test-paper-type').val(type);
		$('#test-paper-attempts').val(totalAttempts);
		$('#test-paper-start').val(startTime);
		$('#test-paper-end').val(endTime);
		$('#test-paper-instructions').val(instructions);
}

function fetchTestPaperTypes() {
	var req = {};
	req.action = "fetchTestPaperTypes";
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
			fillPaperTypes(res.data);
		else
			toastr.error(res.message,"ERROR:");
	});
}

function fillPaperTypes(types) {
	html = '<option value="0">Select Type</option>';
	$.each(types,function(x,type) {
		html += '<option value="'+type.id+'">'+type.type+'</option>';
	});
	$('#test-paper-type').html(html).change();
}

function setDateTimePicker() {

	$('#test-paper-start').datetimepicker({
		format: "dd MM yyyy - hh:ii",
		startDate: currentDate,
		showMeridian: true,
		autoclose: true,
		todayHighlight: true
		// todayBtn: true
	});
	$('#test-paper-end').datetimepicker({
		format: "dd MM yyyy - hh:ii",
		startDate: currentDate,
		showMeridian: true,
		autoclose: true,
		todayHighlight: true
		// todayBtn: true
	});

	$("#test-paper-start").datetimepicker().on('changeDate', function (e) {
		$('#test-paper-end').datetimepicker('setStartDate', $(this).val());
	});
	 $("#test-paper-end").datetimepicker().on('changeDate', function (e) {
		$('#test-paper-start').datetimepicker('setEndDate', $(this).val());
	});
}


function setEventNextQuestion() {

	$('.nextQuestion').off('click');
	$('.nextQuestion').on('click',function(e) {
		e.preventDefault();
		var questionDiv  = $(this).parents('.question');
		caption = $(this).parents('.question').find('.caption').text();
		lableId =$(this).parents('.question').data("lable_id");

		qId =$(this).parents('.question').attr("data-q_id");

		var req = {};
		// req.count;
		req.action = "getNextQuestion";
		req.notIn = qs;

		req.lableId = lableId;
		$.ajax({
			type: "post",
			url: EndPoint,
			data:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				if(res.data.length > 0){
				var index = list[lableId].indexOf(qId.toString());
				if (index > -1) {
					list[lableId].splice(index, 1);
				}

				list[lableId].push(res.data[0].id);
				qs.push(res.data[0].id);

				$(questionDiv).attr("data-q_id",res.data[0].id.toString());
				$(questionDiv).find(".well").html(res.data[0].question);
				var optionHtml = '<div class="col-md-12 col-xs-12"><ol class="upper-alpha">';
				$.each(res.data[0].options, function(o,opt) {
					optionHtml += '<li><span>'+opt.option+'</span></li>';
				});
				optionHtml += '<ol></div>';
				$(questionDiv).find(".option").html(optionHtml);
				mathsjax();
				MathJax.Hub.Typeset();
				Metronic.init();
				}else{
					toastr.error("No Questions Found.","ERROR:");
				}
			}
			else
				toastr.error(res.message,"ERROR:");
		});
	});
}

function setEventImportQuestion() {
	$(".show-import-question-modal").off('click');
	$(".show-import-question-modal").on('click', function(e) {
		e.preventDefault();
		var questionDiv  = $(this).parents('.question');
		lableId = $(this).parents('.question').data("lable_id");
		questionToChange = $(this).parents('.question').data("q_id");
		type = $(this).parents('.question').data("type");
		var req = {};
		req.action = "importQuestion";
		req.notIn = qs;
		//console.log(list[lableId]);
		req.lableId = lableId;
		req.type = type;
		$.ajax({
			type: "post",
			url: EndPoint,
			data:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				importedQs = res.data;

				$('#import-question-modal .modal-body').html(createImportModalQuestionHtml(res.data));
				// Metronic.init();
				$('#import-question-modal').attr('data-change_id',questionToChange);
				$("#import-question-modal").attr('data-lable_id',lableId);
				mathsjax();
				$("#import-question-modal").modal("show");
			}
			else {
				toastr.error(res.message,"ERROR:");
				// console.log(res);
			}
		});
	});
}

function createImportModalQuestionHtml(questions) {
	var html = '';
	$.each(questions, function(x,qu) {
		//html += getQuestionHtml(x,qu);
		html += '<div class="row"> <div class="col-md-1 col-xs-2"> <div class="checkbox-list"> <label><strong class="caption">Qu.'+(x+1)+'</strong> <input class="insert" type="radio"> </label> </div> </div> <div class="col-md-11 col-xs-10"> <div data-q_id="'+qu.id+'" class="well"> '+qu.question+' </div> <div class="row"> <div class="col-md-1 col-xs-2"></div> <div class="col-md-11 col-xs-10"> <div class="option row"><div class="col-md-12 col-xs-12"> <ol class="upper-alpha">';
		$.each(qu.options, function(o,opt) {
					html += '<li><span>'+opt.option+'</span></li>';
				});
				html += '</ol> </div> </div> </div> </div> </div></div><hr/>';
	});
	return html;
}

function setEventInsertQuestion() {
	$('#insertQuestion').off('click');
	$('#insertQuestion').on('click', function() {
		questionToChange = $("#import-question-modal").attr('data-change_id');
		//console.log(questionToChange);

		lableId = $("#import-question-modal").attr('data-lable_id');
		q_id = $('#import-question-modal .insert:checked').parents('.row').find('.well').data('q_id');
		questonHtml = $('#import-question-modal .insert:checked').parents('.row').find('.well').html();
		optionHtml = $('#import-question-modal .insert:checked').parents('.row').find('.option').html();

		$('[data-q_id="'+questionToChange+'"]').find('.well').html(questonHtml);
		$('[data-q_id="'+questionToChange+'"]').find('.option').html(optionHtml);
		$('[data-q_id="'+questionToChange+'"]').attr('data-q_id',q_id);


		list[lableId].push(q_id.toString());
		qs.push(q_id.toString());
		var index = list[lableId].indexOf(questionToChange.toString());


		if (index > -1) {
			list[lableId].splice(index, 1);
		}
		//console.log(list[lableId]);


		$("#import-question-modal").modal("hide");
			MathJax.Hub.Typeset();
			Metronic.init();

		// $('[data-q_id="'+questionToChange+'"]').attr('data-q_id',q_id);
	});
}

function setEventShowQuestionInformation() {
	$(".show-question-info-modal").off("click");
	$(".show-question-info-modal").on("click", function(e) {
		e.preventDefault();
		var req	=	{};
		req.action = "fetchQuestionInformation";
		req.question_id = $(this).parents('.question').data('q_id');
		$.ajax({
			'type' : 'post',
			'url'	:	EndPoint,
			'data'	: JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				//console.log(res);
				fillQueestionInformation(res.tagged_data);
				$("#question-details-modal").modal("show");
			}
			else
				console.log(res.message);
		});
	});
}


function fillQueestionInformation(list) {
	if(list.length > 0) {
		$('#subject-li').text('');
		$('#unit-li').text('');
		$('#chapter-li').text('');
		$('#topic-li').text('');
		$('#tag-li').text('');
		$('#difficulty-li').text('');
		$('#skill-li').text('');
		$('#class-li').text(' '+list[0].class_name);
		$.each(list, function(i,listItem) {
			switch(listItem.tag_type) {
				case "1":
					if($('#subject-li').text() != '')
						$('#subject-li').append(',');
					$('#subject-li').append(' '+listItem.name);
					break;

				case "2":
					if($('#unit-li').text() != '')
						$('#unit-li').append(',');
					$('#unit-li').append(' '+listItem.name);
					break;

				case "3":
					if($('#chapter-li').text() != '')
						$('#chapter-li').append(',');
					$('#chapter-li').append(' '+listItem.name);
					break;

				case "4":
					if($('#topic-li').text() != '')
						$('#topic-li').append(',');
					$('#topic-li').append(' '+listItem.name);
					break;

				case "5":
					if($('#tag-li').text() != '')
						$('#tag-li').append(',');
					$('#tag-li').append(' '+listItem.name);
					break;

				case "6":
					if($('#difficulty-li').text() != '')
						$('#difficulty-li').append(',');
					$('#difficulty-li').append(' '+listItem.name);

					break;

				case "7":
					if($('#skill-li').text() != '')
						$('#skill-li').append(',');
					$('#skill-li').append(' '+listItem.name);

					break;

				default:
					break;
			}
		});
	}
}


// function to fetch classes
function fetchClasses() {
	req = {};
	req.action = 'fetchClasses';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillClasses(res.classes);
		}
		else{
			// toastr.error('Error');
		}
	});

}

// function to fill classes
function fillClasses(classes) {
	var html = "<option value='0'> Select Class </option>";
	$.each(classes,function( i,cls) {
		html += "<option value='"+cls.id+"'>" +cls.name+ "</option>";
	});
	$('#test-paper-class').html(html);
}

// function to fetch subjects
function fetchSubjects() {
	req = {};
	req.action = 'fetchSubjects';
	req.class_id = $("#test-paper-class").val();
	if(req.class_id) {
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fillSubjects(res.subjects);
			}
			else{
				$('#test-paper-subject').html('');
				// toastr.error('No Subject Found In Selected Class.');
			}
		});
	}
}

// function to fill subjects
function fillSubjects(subjects) {
	var html = "";
	$.each(subjects,function( i,sub) {
		html += '<option value="'+sub.id+'">'+sub.name+'</option>';
	});
	$('#test-paper-subject').html(html);
}