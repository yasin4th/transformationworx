
test_program = getUrlParameter("test_program");
subject = getUrlParameter("subject");
chapter = getUrlParameter("chapter");
$(function() {
	$('.test-program').attr('href','view-test-program.php?test_program='+test_program);
	$('.subject-navigation').attr('href','chapters-in-test-program.php?test_program='+test_program+'&subject='+subject);

	fetchTestProgramDetails();
	fetchNumberOfAttachedTestPapers();
	setEventOfUpdateTestPrograme();
	setEventAddTestPaper();
	setEventShowTestProgramDetails();
	saveAttachedTestProgram();
	setAjaxFormToUploadFile();

	CKEDITOR.config.customConfig = 'basic-toolbar-config.js';
	CKEDITOR.disableAutoInline = true;

	// event load subjects when admin select a class
	
	$('.view-test').on('click', function(e){
		e.preventDefault();
		type = $(this).parents('.dashboard-stat').find('.add-test-paper').data('type');
		window.location = 'view-test.php?test_program='+test_program+'&type='+type+'&parent_id='+getUrlParameter('chapter')+'&parent_type=2&subject='+subject;
	});
	
	$('#upload-illustration-file').on('click', function(e){
		e.preventDefault();
		$('#test-program-illustration').submit();
	});
	
	$('#view-illustration').on('click', function(e){
		e.preventDefault();
		window.location = 'view-illustration.php?test_program='+test_program+'&chapter='+chapter+'&subject='+subject;
	});
	
	$('#program_id').val(test_program);
	$('#chapter_id').val(chapter);


	$('#test-program-class').on('change', function(e) {
		$("#test-program-subjects").select2('val',null);
		$("#test-program-subjects").html('');
		fetchSubjects();
	});

});

// function fetch Test Program Details Fromm DB
function fetchTestProgramDetails() {
	var req = {};
	req.action = "fetchTestProgramDetails";
	req.test_program = test_program;

	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			subjects = $.parseJSON('['+res.details.subjects+']');
			fillClasses(res.classesList);			
			fillSubjects(res.subjectsList);			
			$('.test-program').html(res.details.name);
			setBreadcrumb(res.subjectsList,res.chapters);
			$('#test-program-name').val(res.details.name);
			$('#test-program-status').val(res.details.status);
			$('#test-program-class').val(res.details.class);
			$('#test-program-subjects').val(subjects).select2();
			$('#test-program-price').val(res.details.price);
			$('#test-program-description').html(res.details.description);
		}
		else {
			toastr.error(res.message,'Error');
		}
			
	});
}


// function to fetch classes
function fetchClasses() {
	req = {};
	req.action = 'fetchClasses';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillClasses(res.classes);
		}
		else{
			toastr.error(res.message,'Error');
		}
	});
	
}

// function to fill classes
function fillClasses(classes){
	var html = "<option value='0'> Select Class </option>";
	$.each(classes,function( i,cls) {
		html += '<option val="'+i+'" value="'+cls.id+'" data-id="'+cls.id+'">' +cls.name+ '</option>';
	});
	$('#test-program-class').html(html);
}

function setEventOfUpdateTestPrograme() {
	$("#update_program").off('click');
	$("#update_program").on('click', function(e) {
		e.preventDefault();
		var req = {};
		req.id = test_program;
		req.action = "updateTestProgram";
		req.name = $('#test-program-name').val();
		req.status = $('#test-program-status').val();
		req.class = $('#test-program-class').val();
		req.subjects = $('#test-program-subjects').val();
		req.price = $('#test-program-price').val();
		req.description = CKEDITOR.instances['test-program-description'].getData();
		
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fetchTestProgramDetails();
				$('#edit-test-program-modal').modal("hide");
			}
			else{
				toastr.error('Error:'+res.message);
			}
		});
	});
}


function setEventShowTestProgramDetails() {
	$('#show-edit-test-program-modal').on('click', function(e){
		e.preventDefault();
		$('#edit-test-program-modal').modal("show");
		setTimeout(function(){ CKEDITOR.inline("test-program-description"); }, 1000);
	})
};

function setEventAddTestPaper() {
	$('.add-test-paper').on('click', function(e) {
		e.preventDefault();
		type = $(this).data('type');

		$('#test-paper-by-type-modal-table tbody').html('');
		$('#saveAttachedTestPaper').data('type',type)
		$('#test-paper-by-type-modal').modal("show");
		fetchTestPaperByType(type);
	})
}

function fetchTestPaperByType(type) {
	var req = {};
	req.action = "fetchTestPaperByType";
	req.type = type;
	req.class_id = $('#test-program-class').val();
	req.subject_id = getUrlParameter('subject');
	req.test_program = test_program;
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillTestPaperByType(res.test_papers);
		}
		else {
			toastr.error(res.message,'Error');
		}
	});

}

function fillTestPaperByType(paper) {
	html = "";
	$.each(paper, function(x, testPaper) {
		html += '<tr><td> '+testPaper.name+'</td><td><div class="checkbox-list"><label><input type="checkbox" data-id="'+testPaper.id+'"></label></div></td></tr>';
	});
	$('#test-paper-by-type-modal-table tbody').html(html);
}

function saveAttachedTestProgram() {
	$('#saveAttachedTestPaper').off('click');
	$('#saveAttachedTestPaper').on('click', function(e) {
		e.preventDefault();
		$('#saveAttachedTestPaper').attr('disabled',true);
		var req = {};
		req.action = "saveAttachedTestPaper";
		req.test_program = test_program;
		var papers = [];
		req.type = $(this).data('type');
		req.parent_id = getUrlParameter('chapter');
		req.parent_type = 2;
		checkboxes = $('#test-paper-by-type-modal-table input:checkbox');
		$.each(checkboxes, function(x, checkbox) {
			if($(checkbox).prop('checked')) {
				papers.push($(checkbox).data('id'));
			}
		});
		req.testPapers = papers;
		$.ajax({
			type: "post",
			url: EndPoint,
			data:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				fetchNumberOfAttachedTestPapers();
				$('#test-paper-by-type-modal').modal("hide");
				$('#saveAttachedTestPaper').attr('disabled',false);
			}
			else {
				toastr.error(res.message,'Error');
			}
				
		});

	});
}


function fetchNumberOfAttachedTestPapers() {
	var req = {};
	req.action = "fetchNumberOfAttachedTestPapers";
	req.test_program = test_program;
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillCounts(res.data);1
		}
		else {
			toastr.error(res.message,'Error');
		}
	});
}

function fillCounts(data) {
	$.each(data, function(i,paper){
		switch(parseInt(paper.type)) {
			case 1:
				$('#diagnostic .number').text(paper.counts);
				break;

			case 2:
				$('#illustration .number').text(paper.counts);
				break;

			case 3:
				$('#chapter-test .number').text(paper.counts);
				break;

			case 4:
				$('#full-syllabus-test .number').text(paper.counts);
				break;

			case 5:
				$('#scheduled-test .number').text(paper.counts);
				break;

			case 6:
				$('#practice-test .number').text(paper.counts);
				break;

			default:
				break;
		}

	})
}



// function to fetch subjects
function fetchSubjects() {
	req = {};
	req.action = 'fetchSubjects';
	req.class_id = $("#test-program-class").find('option:selected').data('id');
	if(req.class_id) {
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fillSubjects(res.subjects);
			}
			else{
				$('#test-program-subjects').html('');
				// toastr.error('No Subject Found In Selected Class.');
			}
		});
	}
}

// function to fill subjects
function fillSubjects(subjects) {
	var html = "";
	$.each(subjects,function( i,sub) {
		html += '<option val="'+(i+1)+'" value="'+sub.id+'" data-id="'+sub.id+'">'+sub.name+'</option>';
	});
	$('#test-program-subjects').html(html);
}

function setAjaxFormToUploadFile() {
	$('form#test-program-illustration').ajaxForm( {
		  beforeSend: function(arr, $form, data) {
            console.log('starting');
        },
		uploadProgress: function(event, position, total, percentComplete) {
			//Progress bar
			$('#percentage').show();
			$('#percentage').html(percentComplete + '% done.') //update progressbar percent complete
        },
        success: function() {
            
        },
        complete: function(resp) {
        	$('#percentage').hide();
            res = $.parseJSON(resp.responseText);
            console.log(res);
            if(res.status == 1) {
				toastr.success(res.message);
			}
			else
				toastr.error('An error occured. Please refresh the page and try again. Error Details: ' + res.message);
        },
		error: function() {
			console.log('Error');
        }
    });
}



/*
* function to  show how number of papers attached
*/
function fetchNumberOfAttachedTestPapers() {
	var req = {};
	req.action = "fetchNumberOfAttachedTestPapers";
	req.test_program = test_program;
	req.parent_id = chapter;
	req.parent_type = 2;
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			
			// console.log(res);
			if(res.data.chapter != undefined){
				$('#chapter-test .count').text(res.data.chapter);
			}
			if(res.data.practice != undefined){
				$('#practice-test .count').text(res.data.practice);
			}
			if(res.data.illustration != undefined){
				$('#illustration .count').text(res.data.illustration);
			}
		}
		else {
			toastr.error(res.message);
		}
	});
}


setBreadcrumb = function(subjects,chapters) {
	console.log(subjects);
	$.each(subjects, function(i, sub){
		if(sub.id==subject){
			$('.subject-navigation').html(sub.name);
			$('a.chapter').attr('href','attach-paper.php?test_program='+test_program+'&subject='+subject+'&chapter='+chapter);
			//$('a.subject').attr('href','chapters-in-test-program.php?test_program='+test_program+'&subject='+subject);
			//$('.subject').text(sub.name);
			
		}
	});
	$.each(chapters, function(i, chap){
		if(chap.chapter_id==chapter){
			$('.chapter').html(chap.chapter_name);
			//$('a.chapter').attr('href','student-course-chapter-tests-types.php?program_id='+test_program+'&subject='+subject+'&chapter='+chapter);
			
			
		}
	});
}