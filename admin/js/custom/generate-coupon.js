$(function() {
	getTestprograms();
	downloadCoupons();
	setDateTimePicker();
	getInstituteDetails();
});
function getInstituteDetails() {
	var req = {};
	user_id = getUrlParameter('institute');
	req.action = "getInstituteDetails";
	req.user_id = user_id;

	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req),
	}).done(function(res) {
		res = $.parseJSON(res);
		console.log(res);
		if (res.status == 1) {
			$(".institute-name").text(res.users.name+' ('+res.users.email+')');
		} else{
			toastr.error(res.message, "Error:");
		};
	});
}

function getTestprograms() {
	var req = {};
	req.action = "getTestprograms";
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillTestprograms(res.data);
		}
		else if(res.status == 0) {
			toastr.error(res.message);
		}
	});
}

function fillTestprograms(programs) {
	html = '';
	$.each(programs, function(p,tr) {
		html += '<tr data-id="'+tr.id+'""><td>'+tr.name+'</td><td> '+tr.class+'</td><td>'+tr.test_papers+'</td><td> '+tr.price+'</td><td style="width:150px;"><a class="generate-coupons btn blue"> Generate Coupon </a></td></tr>';
	});

	$('#test-programs-table tbody').html(html);
	// $('#test-programs-table').DataTable();
	$('#test-programs-table').DataTable({
		"fnDrawCallback": function( oSettings ) {
	      showGenrateCouponModal(); // or fnInfoCallback
	    }
	});
	showGenrateCouponModal();
}

function showGenrateCouponModal() {
	$('.generate-coupons').off('click');
	$('.generate-coupons').on('click', function(e) {
		e.preventDefault();
		id = $(this).parents('tr:eq(0)').data('id');
		tp_name = $(this).parents('tr:eq(0)').find('td:eq(0)').text();
		$('#test-program-name').text(tp_name);
		$('#generate-coupon-modal').data('program-id', id);
		$('#generate-coupon-modal').modal('show');

	});
}

function downloadCoupons() {
	$('#create-coupons-form').off('submit');
	$('#create-coupons-form').on('submit', function(e) {
		e.preventDefault();

		var req = {};
		req.id = $('#generate-coupon-modal').data('program-id');
		req.action = "downloadCoupons";
		req.number_of_coupons = $('#number-of-coupons').val();
		req.prefix = $('#coupon-prefix').val();
		req.start_date = $('#start-date').data("datetimepicker").getDate().valueOf()/1000;
		req.end_date = $('#end-date').data("datetimepicker").getDate().valueOf()/1000;
		req.institute = getUrlParameter('institute');

		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			// console.log(res);

			uriContent = "data:application/octet-stream," +escape(res.coupon.toString());
   			window.open(uriContent, 'myDocument');
		});
	});
}


/*
* set date time picker
*/
function setDateTimePicker() {

    $('#start-date').datetimepicker({
    	format: "dd MM yyyy - hh:ii",
    	startDate: new Date,
    	showMeridian: true,
        autoclose: true,
        todayHighlight: true
    	// todayBtn: true
    });
    $('#end-date').datetimepicker({
    	format: "dd MM yyyy - hh:ii",
    	startDate: new Date,
    	showMeridian: true,
        autoclose: true,
        todayHighlight: true
    	// todayBtn: true
    });


    $("#start-date").datetimepicker().on('changeDate', function (e) {
		$('#end-date').datetimepicker('setStartDate', $(this).val());
    });
     $("#end-date").datetimepicker().on('changeDate', function (e) {
		$('#start-date').datetimepicker('setEndDate', $(this).val());
    });
}