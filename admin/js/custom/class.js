// var classes;
// var subjects;
// var units;
// var chapters;
// var topics;
var name;
var desc;

	function addClass() {
		$('#chapter-dialog').addClass('hidden');
		$('#unit-dialog').addClass('hidden');
		$('#subject-dialog').addClass('hidden');
		$('#topic-dialog').addClass('hidden');
		$('#class-dialog').attr('data-mode' , 'new');
		$('#class-dialog .caption').html('<i class="fa fa-plus"></i>Add Class');
		$('#class-dialog').removeClass('hidden');
	}

	function addSubject(id) {
		$('#chapter-dialog').addClass('hidden');
		$('#unit-dialog').addClass('hidden');
		$('#class-dialog').addClass('hidden');
		$('#topic-dialog').addClass('hidden');
		$('#subject-dialog').attr('data-p_id' , id);
		$('#subject-dialog').attr('data-mode' , 'new');
		$('#subject-dialog .caption').html('<i class="fa fa-plus"></i>Add Subject');
		$('#subject-dialog').removeClass('hidden');
	}

	function addUnit(id) {
		$('#chapter-dialog').addClass('hidden');
		$('#subject-dialog').addClass('hidden');
		$('#class-dialog').addClass('hidden');
		$('#topic-dialog').addClass('hidden');
		$('#unit-dialog').attr('data-p_id' , id);
		$('#unit-dialog').attr('data-mode' , 'new');
		$('#unit-dialog .caption').html('<i class="fa fa-plus"></i>Add Unit');
		$('#unit-dialog').removeClass('hidden');
	}

	function addChapter(id) {
		$('#unit-dialog').addClass('hidden');
		$('#subject-dialog').addClass('hidden');
		$('#class-dialog').addClass('hidden');
		$('#topic-dialog').addClass('hidden');
		$('#chapter-dialog').attr('data-p_id' , id);
		$('#chapter-dialog').attr('data-mode' , 'new');
		$('#chapter-dialog .caption').html('<i class="fa fa-plus"></i>Add Chapter');
		$('#chapter-dialog').removeClass('hidden');
	}
	
	function addTopic(id) {
		$('#chapter-dialog').addClass('hidden');
		$('#unit-dialog').addClass('hidden');
		$('#subject-dialog').addClass('hidden');
		$('#class-dialog').addClass('hidden');
		$('#topic-dialog').attr('data-p_id' , id);
		$('#topic-dialog').attr('data-mode' , 'new');
		$('#topic-dialog .caption').html('<i class="fa fa-plus"></i>Add Topic');
		$('#topic-dialog').removeClass('hidden');
	}
	
	
	function getClass(data) {
		$('#chapter-dialog').addClass('hidden');
		$('#unit-dialog').addClass('hidden');
		$('#subject-dialog').addClass('hidden');
		$('#topic-dialog').addClass('hidden');
		$('#class-dialog').attr('data-id' , data.id);
		$('#chapter-dialog').attr('data-p_id' , data.parent_id);
		$('#class-dialog').attr('data-mode' , 'edit');
		$('#class-dialog .caption').html('<i class="fa fa-pencil-square-o"></i>Edit Class');
		$('#class-dialog input').val(data.name);
		$('#class-dialog textarea').val(data.desc);
		$('#class-dialog').removeClass('hidden');
	}
	
	function getSubject(data) {
		$('#class-dialog').addClass('hidden');
		$('#unit-dialog').addClass('hidden');
		$('#chapter-dialog').addClass('hidden');
		$('#topic-dialog').addClass('hidden');
		$('#subject-dialog').attr('data-id' , data.id);
		$('#subject-dialog').attr('data-p_id' , data.parent_id);
		$('#subject-dialog').attr('data-mode' , 'edit');
		$('#subject-dialog .caption').html('<i class="fa fa-pencil-square-o"></i>Edit Subject');
		$('#subject-dialog input').val(data.name);
		$('#subject-dialog textarea').val(data.desc);
		$('#subject-dialog').removeClass('hidden');
	}
	
	function getUnit(data) {
		$('#class-dialog').addClass('hidden');
		$('#chapter-dialog').addClass('hidden');
		$('#subject-dialog').addClass('hidden');
		$('#topic-dialog').addClass('hidden');
		$('#unit-dialog').attr('data-id' , data.id);
		$('#unit-dialog').attr('data-p_id' , data.parent_id);
		$('#unit-dialog').attr('data-mode' , 'edit');
		$('#unit-dialog .caption').html('<i class="fa fa-pencil-square-o"></i>Edit Unit');
		$('#unit-dialog input').val(data.name);
		$('#unit-dialog textarea').val(data.desc);
		$('#unit-dialog').removeClass('hidden');
	}
	
	function getChapter(data) {
		$('#class-dialog').addClass('hidden');
		$('#unit-dialog').addClass('hidden');
		$('#subject-dialog').addClass('hidden');
		$('#topic-dialog').addClass('hidden');
		$('#chapter-dialog').attr('data-id' , data.id);
		$('#chapter-dialog').attr('data-p_id' , data.parent_id);
		$('#chapter-dialog').attr('data-mode' , 'edit');
		$('#chapter-dialog .caption').html('<i class="fa fa-pencil-square-o"></i>Edit Chapter');
		$('#chapter_name').val(data.name);
		$('#chapter_time').val(data.time);
		$('#chapter-dialog textarea').val(data.desc);
		$('#chapter-dialog').removeClass('hidden');
	}
	
	function getTopic(data) {
		$('#class-dialog').addClass('hidden');
		$('#subject-dialog').addClass('hidden');
		$('#unit-dialog').addClass('hidden');
		$('#chapter-dialog').addClass('hidden');
		$('#topic-dialog').attr('data-id' , data.id);
		$('#topic-dialog').attr('data-p_id' , data.parent_id);
		$('#topic-dialog').attr('data-mode' , 'edit');
		$('#topic-dialog .caption').html('<i class="fa fa-pencil-square-o"></i>Edit Topic');
		$('#topic-dialog input').val(data.name);
		$('#topic-dialog textarea').val(data.desc);
		$('#topic-dialog').removeClass('hidden');
	}
	
	
function TreeClickAction(data) {
	mode = $('#'+data.id).attr('data-mode');
	$('input, textarea').val('');
	if(mode == 'edit') {
		var req = {};
		
		req.id = $('#'+data.id).data('id');
		req.time = $('#'+data.id).data('time');
		req.name =  $('#'+data.id).find('a:eq(0)').text();
		req.desc = $('#'+data.id).attr('data-desc');
		
		action = $('#'+data.id).attr('data-action')
		if(action != 'getClass')
			req.parent_id = $('#'+data.parent).data('id');
		
		//console.log(id,action);
		window[action](req);
	}
	else if(mode == 'addNew') {
		action = $('#'+data.id).attr('data-action')
		if(action != "addClass") {
			parent_id = $('#'+data.parent).attr('data-id');
			// console.log(action,parent_id);
			window[action](parent_id);
		}
		else
			console.log(action);
			window[action]();
	}
}

function treeData() {
	var req = {};
	req.action = 'fetchTreeData';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		$('#tree_1').jstree('destroy');
		$('#tree_1').html(res.html);
		UITree.init();
		$('[data-action="addClass"] a')[0].click();
		 // classes	=	res.classes;
		 // subjects	=	res.subjects;
		 // units	=	res.units;
		 // chapters	=	res.chapters;
		 // topics	=	res.topics;
	});
}
$(function(){
	treeData();
	deleteEvents();
	$('.cancel').off('click');
	$('.cancel').on('click',function(){
		location.replace("dashboard.php");
	});
	$('#class-dialog form').on('submit', function(e) {
		e.preventDefault();
		var mode = $('#class-dialog').attr('data-mode');
		var req = {};
		req.name = $('#class_name').val();
		req.description = $('#class_description').val();
		if(mode == 'edit') {
			req.action = "updateClass";
			req.id = $('#class-dialog').attr('data-id');
		}
		else if(mode == 'new') {
			req.action = "addNewClass";
			$('.save').attr('disabled',true);
		}
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			$('input, textarea').val('');
			$('.save').attr('disabled',false)
			if(res.status == 1) {
				saveOpenNodes();
				treeData();
				setTimeout(function() {openNodes(1);}, 1500);
				// if(req.action == "updateClass") {
					// li = $('.jstree-clicked').parent('li').eq(0);
					// $('#tree_1').jstree('set_text', li, req.name);
					// li.attr('data-desc', req.description);
				// }
				// if(req.action == "addNewClass")
					// var position = 'before';
					// var li = $('#'+$('#tree_1').jstree('get_selected'));
					// var name = { "text" : req.name };
					// nodeId = $('#tree_1').jstree().create_node(li , name, position);
					// attributes = {
						// 'data-id'	:	res.id,
						// 'data-desc'	:	req.description,
						// 'data-mode'	:	'edit',
						// 'data-action'	:	'getClass'
					// };
					// $('#'+nodeId).attr(attributes);
					// console.log('ok');
					//row = "<li data-mode='edit' data-action='getSubject' data-id='" +res.id+ "' data-desc='" ++ "'><ul>";
			}
			if(res.status == 0) {
				toastr.error(res.message);
			}
		});
		
	});
	
	
	$('#subject-dialog form').on('submit', function(e){
		e.preventDefault();
		
		var mode = $('#subject-dialog').attr('data-mode');
		var req = {};
		req.name = $('#subject_name').val();
		req.p_id = $('#subject-dialog').attr('data-p_id');
		req.description = $('#subject_description').val();
		if(mode == 'edit') {
			req.action = "updateSubject";
			req.id = $('#subject-dialog').attr('data-id');
		}
		else if(mode == 'new') {
			req.action = "addNewSubject";
			$('.save').attr('disabled',true);
		}
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			$('input, textarea').val('');
			$('.save').attr('disabled',false);
			if(res.status == 1) {
				saveOpenNodes();
				treeData();
				setTimeout(function() {openNodes(2);}, 1500);
				// if(req.action == "updateSubject") {
					// li = $('.jstree-clicked').parent('li').eq(0);
					// $('#tree_1').jstree('set_text', li, req.name);
					// li.attr('data-desc', req.description);
				// }
				// if(req.action == "addNewSubject") {
					// var position = 'before';
					// var parent = $('#'+$('#tree_1').jstree('get_selected'));
					// var Node = { "text" : req.name };
					// nodeId = $('#tree_1').jstree().create_node(parent , Node, position);
					// attributes = {
						// 'data-id'	:	res.id,
						// 'data-desc'	:	req.description,
						// 'data-mode'	:	'edit',
						// 'data-action'	:	'getSubject'
					// };
					// $('#'+nodeId).attr(attributes);
					// console.log('ok');
					
					// $('#tree_1').jstree().create_node('#' ,  { "text" : "newly added" }, "inside", function(){alert("done");});
					// row = "<li data-mode='edit' data-action='getSubject' data-id='" +res.id+ "' data-desc='" ++ "'><ul>";
					
				// }
			}
			if(res.status == 0) {
				toastr.error(res.message);
			}
		});
	});
	
	
	$('#unit-dialog form').on('submit', function(e){
		e.preventDefault();
		var mode = $('#unit-dialog').attr('data-mode');
		var req = {};
		req.name = $('#unit_name').val();
		req.p_id = $('#unit-dialog').attr('data-p_id');
		req.description = $('#unit_description').val();
		if(mode == 'edit') {
			req.action = "updateUnit";
			req.id = $('#unit-dialog').attr('data-id');
		}
		else if(mode == 'new') {
			req.action = "addNewUnit";
			$('.save').attr('disabled',true);
		}
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			$('input, textarea').val('');
			$('.save').attr('disabled',false);
			if(res.status == 1) {
				saveOpenNodes();
				treeData();
				setTimeout(function() {openNodes(3);}, 1500);
				// if(req.action == "updateUnit") {
					// li = $('.jstree-clicked').parent('li').eq(0);
					// $('#tree_1').jstree('set_text', li, req.name);
					// li.attr('data-desc', req.description);
				// }
				// if(req.action == "addNewUnit")
					// var position = 'before';
					// var parent = $('#'+$('#tree_1').jstree('get_selected'));
					// var Node = { "text" : req.name };
					// nodeId = $('#tree_1').jstree().create_node(parent , Node, position);
					// attributes = {
						// 'data-desc'	:	req.description,
						// 'data-mode'	:	'edit',
						// 'data-action'	:	'getUnit'
					// };
					// $('#'+nodeId).attr(attributes);
					// console.log('ok');
					//row = "<li data-mode='edit' data-action='getSubject' data-id='" +res.id+ "' data-desc='" ++ "'><ul>";
			}
			if(res.status == 0) {
				toastr.error(res.message);
			}
		});
		
	});
	
	
	$('#chapter-dialog form').on('submit', function(e){
		e.preventDefault();
		var mode = $('#chapter-dialog').attr('data-mode');
		var req = {};
		req.name = $('#chapter_name').val();
		req.time = $('#chapter_time').val();
		req.description = $('#chapter_description').val();
		req.p_id = $('#chapter-dialog').attr('data-p_id');

		if(mode == 'edit') {
			req.action = "updateChapter";
			req.id = $('#chapter-dialog').attr('data-id');
		}
		else if(mode == 'new') {
			req.action = "addNewChapter";
			$('.save').attr('disabled',true);
		}
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			$('input, textarea').val('');
			$('.save').attr('disabled',false);
			if(res.status == 1) {
				saveOpenNodes();
				treeData();
				setTimeout(function() {openNodes(4);}, 1500);
				// if(req.action == "updateChapter") {
					// li = $('.jstree-clicked').parent('li').eq(0);
					// $('#tree_1').jstree('set_text', li, req.name);
					// li.attr('data-desc', req.description);
				// }
				// if(req.action == "addNewChapter")
					// var position = 'before';
					// var parent = $('#'+$('#tree_1').jstree('get_selected'));
					// var Node = { "text" : req.name };
					// nodeId = $('#tree_1').jstree().create_node(parent , Node, position);
					// attributes = {
						// 'data-desc'	:	req.description,
						// 'data-mode'	:	'edit',
						// 'data-action'	:	'getChapter'
					// };
					// $('#'+nodeId).attr(attributes);
					// console.log('ok');
					//row = "<li data-mode='edit' data-action='getSubject' data-id='" +res.id+ "' data-desc='" ++ "'><ul>";
			}
			if(res.status == 0) {
				toastr.error(res.message);
			}
		});
		
	});
	

	$('#topic-dialog form').on('submit', function(e){
		e.preventDefault();
		$('.save').attr('disabled',true);
		var mode = $('#topic-dialog').attr('data-mode');
		var req = {};
		req.name = $('#topic_name').val();
		req.p_id = $('#topic-dialog').attr('data-p_id');
		req.description = $('#topic_description').val();
		if(mode == 'edit') {
			req.action = "updateTopic";
			req.id = $('#topic-dialog').attr('data-id');
		}
		else if(mode == 'new') {
			req.action = "addNewTopic";
		}
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			$('input, textarea').val('');
			$('.save').attr('disabled',false);
			if(res.status == 1) {
				saveOpenNodes();
				treeData();
				setTimeout(function() {openNodes(5);}, 1500);
				// if(req.action == "updateTopic") {
					// li = $('.jstree-clicked').parent('li').eq(0);
					// $('#tree_1').jstree('set_text', li, req.name);
					// li.attr('data-desc', req.description);
				// }
				// if(req.action == "addNewTopic")
					// var position = 'before';
					// var parent = $('#'+$('#tree_1').jstree('get_selected'));
					// var Node = { "text" : req.name };
					// nodeId = $('#tree_1').jstree().create_node(parent , Node, position);
					// attributes = {
						// 'data-desc'	:	req.description,
						// 'data-mode'	:	'edit',
						// 'data-action'	:	'getTopic'
					// };
					// $('#'+nodeId).attr(attributes);
					// console.log('ok');
					//row = "<li data-mode='edit' data-action='getSubject' data-id='" +res.id+ "' data-desc='" ++ "'><ul>";
			}
			if(res.status == 0) {
				toastr.error(res.message);
			}
		});
	});
});


function deleteEvents() {		
	
	/*
	* Fuunction to delete class
	*/
	$('#class-dialog form .delete').off('click')
	$('#class-dialog form .delete').on('click',function(e) {
		e.preventDefault();
		mode = $('#class-dialog').attr('data-mode');
		if(mode == 'edit') {
			if(confirm('Are you sure want to delete this Class?')) {	
				var req = {};
				req.action = "deleteClass";
				req.id = $('#class-dialog').attr('data-id');
				$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
				}).done(function(res) {
					$('input:text').val('');
					$('textarea').val('');
					res = $.parseJSON(res);
					if (res.status == 1) {
						toastr.success(res.message);
						saveOpenNodes();
						treeData();
						setTimeout(function() {openNodes(1);}, 1500);
					} else{
						toastr.error(res.message);
					};
				});
			}
		}
	});
	
	/*
	* Fuunction to delete subject
	*/
	$('#subject-dialog form .delete').off('click')
	$('#subject-dialog form .delete').on('click',function(e) {
		e.preventDefault();
		mode = $('#subject-dialog').attr('data-mode');
		if(mode == 'edit') {
			if(confirm('Are you sure want to delete this Subject?')) {	
				var req = {};
				req.action = "deleteSubject";
				req.id = $('#subject-dialog').attr('data-id');
				$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
				}).done(function(res) {
					$('input:text').val('');
					$('textarea').val('');
					res = $.parseJSON(res);
					if (res.status == 1) {
						toastr.success(res.message);
						saveOpenNodes();
						treeData();
						setTimeout(function() {openNodes(2);}, 1500);
					} else{
						toastr.error(res.message);
					};
				});
			}
		}
	});
	
	/*
	* Fuunction to delete unit
	*/
	$('#unit-dialog form .delete').off('click')
	$('#unit-dialog form .delete').on('click',function(e) {
		e.preventDefault();
		mode = $('#unit-dialog').attr('data-mode');
		if(mode == 'edit') {
			if(confirm('Are you sure want to delete this Unit?')) {	
				var req = {};
				req.action = "deleteUnit";
				req.id = $('#unit-dialog').attr('data-id');
				$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
				}).done(function(res) {
					$('input:text').val('');
					$('textarea').val('');
					res = $.parseJSON(res);
					if (res.status == 1) {
						toastr.success(res.message);
						saveOpenNodes();
						treeData();
						setTimeout(function() {openNodes(3);}, 1500);
					} else{
						toastr.error(res.message);
					};
				});
			}
		}
	});
	
	/*
	* Fuunction to delete chapter
	*/
	$('#chapter-dialog form .delete').off('click')
	$('#chapter-dialog form .delete').on('click',function(e) {
		e.preventDefault();
		mode = $('#chapter-dialog').attr('data-mode');
		if(mode == 'edit') {
			if(confirm('Are you sure want to delete this Chapter?')) {
				var req = {};
				req.action = "deleteChapter";
				req.id = $('#chapter-dialog').attr('data-id');
				$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
				}).done(function(res) {
					$('input:text').val('');
					$('textarea').val('');
					res = $.parseJSON(res);
					if (res.status == 1) {
						toastr.success(res.message);
						saveOpenNodes();
						treeData();
						setTimeout(function() {openNodes(4);}, 1500);
					} else{
						toastr.error(res.message);
					};
				});
			}
		}
	});
	
	/*
	* Fuunction to delete topic
	*/
	$('#topic-dialog form .delete').off('click')
	$('#topic-dialog form .delete').on('click',function(e) {
		e.preventDefault();
		mode = $('#topic-dialog').attr('data-mode');
		if(mode == 'edit') {
			if(confirm('Are you sure want to delete this Topic?')) {
				var req = {};
				req.action = "deleteTopic";
				req.id = $('#topic-dialog').attr('data-id');
				$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
				}).done(function(res) {
					$('input:text').val('');
					$('textarea').val('');
					res = $.parseJSON(res);
					if (res.status == 1) {
						toastr.success(res.message);
						saveOpenNodes();
						treeData();
						setTimeout(function() {openNodes(5);}, 1500);
					} else{
						toastr.error(res.message);
					};
				});
			}
		}
	});
}


var class_id;
var subject_id;
var unit_id;
var chapter_id;
function saveOpenNodes() {
	parents = $('#tree_1').jstree().get_selected(true)[0].parents;
	l = parents.length;
	switch(l) {
		case 1:
			break;
		
		case 2:
			class_id = $('#'+parents[0]).data('id');
			break;
		
		case 3:
			class_id = $('#'+parents[1]).data('id');
			subject_id = $('#'+parents[0]).data('id');
			break;
		
		case 4:
			class_id = $('#'+parents[2]).data('id');
			subject_id = $('#'+parents[1]).data('id');
			unit_id = $('#'+parents[0]).data('id');
			break;
		
		case 5:
			class_id = $('#'+parents[3]).data('id');
			subject_id = $('#'+parents[2]).data('id');
			unit_id = $('#'+parents[1]).data('id');
			chapter_id = $('#'+parents[0]).data('id');
			break;

		default:
		break;
	}
}

function openNodes(l) {
	switch(l) {
		case 1:
			break;
		
		case 2:
			$('#tree_1').jstree('open_node', $('[data-action="getClass"][data-id="'+class_id+'"'));
			break;
		
		case 3:
			$('#tree_1').jstree('open_node', $('[data-action="getClass"][data-id="'+class_id+'"'));
			$('#tree_1').jstree('open_node', $('[data-action="getSubject"][data-id="'+subject_id+'"'));
			break;
		
		case 4:
			$('#tree_1').jstree('open_node', $('[data-action="getClass"][data-id="'+class_id+'"'));
			$('#tree_1').jstree('open_node', $('[data-action="getSubject"][data-id="'+subject_id+'"'));
			$('#tree_1').jstree('open_node', $('[data-action="getUnit"][data-id="'+unit_id+'"'));
			break;
		
		case 5:
			$('#tree_1').jstree('open_node', $('[data-action="getClass"][data-id="'+class_id+'"'));
			$('#tree_1').jstree('open_node', $('[data-action="getSubject"][data-id="'+subject_id+'"'));
			$('#tree_1').jstree('open_node', $('[data-action="getUnit"][data-id="'+unit_id+'"'));
			$('#tree_1').jstree('open_node', $('[data-action="getChapter"][data-id="'+chapter_id+'"'));
			break;

		default:
		break;
	}
}	
	