test_program = getUrlParameter("test_program");
subject = getUrlParameter("subject");
$(function() {
	$('.test-program').attr('href','view-test-program.php?test_program='+test_program);

	fetchClasses();
	fetchNumberOfAttachedTestPapers();
	fetchTestProgramDetails();
	setEventOfUpdateTestPrograme();
	
	setEventAddTestPaper();
	setEventShowTestProgramDetails();
	saveAttachedTestProgram();

	CKEDITOR.config.customConfig = 'basic-toolbar-config.js';
	CKEDITOR.disableAutoInline = true;

	// event load subjects when admin select a class
	$('#test-program-class').on('change', function(e) {
		$("#test-program-subjects").select2('val',null);
		$("#test-program-subjects").html('');
		fetchSubjects();
	});


	$('.view-test').on('click', function(e){
		e.preventDefault();
		type = $(this).parents('.dashboard-stat').find('.add-test-paper').data('type');
		window.location = 'view-test-2.php?test_program='+test_program+'&type='+type+'&parent_id='+subject+'&parent_type=1';
	})

});

// function fetch Test Program Details Fromm DB
function fetchTestProgramDetails() {
	var req = {};
	req.action = "fetchTestProgramDetails";
	req.test_program = test_program;

	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
			subjects = $.parseJSON('['+res.details.subjects+']');
			fillClasses(res.classesList);
			fillSubjects(res.subjectsList);
			fillChaptrsToAddTest();
			setBreadcrumb(res.subjectsList);
			$('.test-program').html(res.details.name);
			
			$('#test-program-name').val(res.details.name);
			$('#test-program-status').val(res.details.status);
			$('#test-program-class').val(res.details.class);
			$('#test-program-subjects').val(subjects).select2();
			$('#test-program-price').val(res.details.price);
			$('#test-program-description').html(res.details.description);			
	});
}

function fillChaptrsToAddTest () {
	req = {};
	req.action = 'fetchChaptersOfSubject';
	req.subject_id = [getUrlParameter('subject')];
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			html = '';
			$.each(res.chapters, function(i,chapter) {

				html += '<div class="col-md-4 col-sm-6 col-xs-12"> <div class="form-group sub-box tooltips" data-container="body" data-placement="top" data-original-title="'+chapter.name+'"> <div class="sub-name"> <a href="attach-paper.php?test_program='+test_program+'&subject='+getUrlParameter('subject')+'&chapter='+chapter.id+'" data-id="'+chapter.id+'">'+chapter.name+'</a> </div> </div> </div>';
			})
			$('#chapters-div').html(html);
			Metronic.init();
		}
		else{
			// toastr.error('Error Chapters not load');
		}
	});
}


// function to fetch classes
function fetchClasses() {
	req = {};
	req.action = 'fetchClasses';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillClasses(res.classes);
		}
		else{
			toastr.error('Error');
		}
	});
	
}

// function to fill classes
function fillClasses(classes){
	var html = "<option value='0'> Select Class </option>";
	$.each(classes,function( i,cls) {
		html += '<option value="'+cls.id+'" data-id="+cls.id+">' +cls.name+ '</option>';
	});
	$('#test-program-class').html(html);
}

function setEventOfUpdateTestPrograme() {
	$("#update_program").off('click');
	$("#update_program").on('click', function(e) {
		e.preventDefault();
		var req = {};
		req.id = test_program;
		req.action = "updateTestProgram";
		req.name = $('#test-program-name').val();
		req.status = $('#test-program-status').val();
		req.class = $('#test-program-class').val();
		req.subjects = $('#test-program-subjects').val();
		req.price = $('#test-program-price').val();
		req.description = CKEDITOR.instances['test-program-description'].getData();
		
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fetchTestProgramDetails();
				$('#edit-test-program-modal').modal("hide");
			}
			else{
				toastr.error('Error:'+res.message);
			}
		});
	});
}


function setEventShowTestProgramDetails() {
	$('#show-edit-test-program-modal').on('click', function(e){
		e.preventDefault();
		$('#edit-test-program-modal').modal("show");
		setTimeout(function(){ CKEDITOR.inline("test-program-description"); }, 1000);
	})
};

function setEventAddTestPaper() {
	$('.add-test-paper').on('click', function(e) {
		e.preventDefault();
		type = $(this).data('type');

		$('#test-paper-by-type-modal-table tbody').html('');
		$('#saveAttachedTestPaper').data('type',type);
		$('#test-paper-by-type-modal').modal("show");
		fetchTestPaperByType(type);
	})
}

function fetchTestPaperByType(type) {
	var req = {};
	req.action = "fetchTestPaperByType";
	req.type = type;
	req.class_id = $('#test-program-class').val();
	req.subject_id = getUrlParameter('subject');
	req.test_program = test_program;
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		 fillTestPaperByType(res.test_papers);
	});

}

function fillTestPaperByType(paper) {
	html = "";
	$.each(paper, function(x, testPaper) {
		html += '<tr><td> '+testPaper.name+'</td><td><div class="checkbox-list"><label><input type="checkbox" data-id="'+testPaper.id+'"></label></div></td></tr>';
	});
	$('#test-paper-by-type-modal-table tbody').html(html);

}

function saveAttachedTestProgram() {
	$('#saveAttachedTestPaper').off('click');
	$('#saveAttachedTestPaper').on('click', function(e) {
		e.preventDefault();
		$('#saveAttachedTestPaper').attr('disabled',true);
		var req = {};
		req.action = "saveAttachedTestPaper";
		req.test_program = test_program;
		var papers = [];
		req.type = $(this).data('type');
		req.parent_id = getUrlParameter('subject');
		req.parent_type = 1;
		checkboxes = $('#test-paper-by-type-modal-table input:checkbox');
		$.each(checkboxes, function(x, checkbox) {
			if($(checkbox).prop('checked')) {
				papers.push($(checkbox).data('id'));
			}
		});
		req.testPapers = papers;
		$.ajax({
			type: "post",
			url: EndPoint,
			data:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				fetchNumberOfAttachedTestPapers();
				$('#test-paper-by-type-modal').modal("hide");
				$('#saveAttachedTestPaper').attr('disabled',false);
			}
		});

	});
}

// function to fetch subjects
function fetchSubjects() {
	req = {};
	req.action = 'fetchSubjects';
	req.class_id = $("#test-program-class").find('option:selected').data('id');
	if(req.class_id) {
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fillSubjects(res.subjects);
			}
			else{
				$('#test-program-subjects').html('');
				// toastr.error('No Subject Found In Selected Class.');
			}
		});
	}
}

// function to fill subjects
function fillSubjects(subjects) {
	var html = "";
	$.each(subjects,function( i,sub) {
		html += '<option val="'+(i+1)+'" value="'+sub.id+'" data-id="'+sub.id+'">'+sub.name+'</option>';
	});
	$('#test-program-subjects').html(html);
}


/*
* function to  show how number of papers attached
*/
function fetchNumberOfAttachedTestPapers() {
	var req = {};
	req.action = "fetchNumberOfAttachedTestPapers";
	req.test_program = test_program;
	req.parent_id = subject;
	req.parent_type = 1;
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			
			// console.log(res);
			if(res.data.diagnostic != undefined){
				$('#diagnostic .count').text(res.data.diagnostic);
			}
			if(res.data.fullSyllabus != undefined){
				$('#full-syllabus-test .count').text(res.data.fullSyllabus);
			}
		}
		else {
			toastr.error(res.message);
		}
	});
}


setBreadcrumb = function(subjects) {
	console.log(subjects);
	$.each(subjects, function(i, sub){
		if(sub.id==subject){
			$('a.subject').html(sub.name);
			$('a.subject').attr('href','chapters-in-test-program.php?test_program='+test_program+'&subject='+subject);
			//$('.subject').text(sub.name);
			
		}
	});
}
