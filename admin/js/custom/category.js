function validate(oTable, row) {
	name = $('#name').val();
	regex = /^([a-zA-Z0-9 -])+$/;
	if(name.length > 1 && name.length <= 50 && regex.test(name)) {
		$('td:eq(0)', row).removeClass('has-error');
		id = $('#name').parents('tr').eq(0).attr('id');
		if(id == '' || id == undefined)
			addNew(oTable, row);
		else
			updateRow(oTable,row);
		return true;
	}
	else {
		$('td:eq(0)', row).addClass('has-error');
	}
		return false;
}
	
	function addNew(oTable, row) {
		var req = {};
		req.action = 'addCategory';
		req.difficulty = name = $('#name').val();
		req.description = name = $('#desc').val();
		
		$.ajax({
			'type'	:	'post',
			'url'	:	'../vendor/api.php',
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				$(row).attr('id',res.id);
				saveRow(oTable, row);
				toastr.success(res.message);
			}
			else if(res.status == 0) {
				toastr.error(res.message);
			}
		});
	}
	
	function updateRow(oTable,row){

		var req = {};
		req.action = "updateCategoryRow";
		req.id = $('#name').parents('tr').eq(0).attr('id');
		req.difficulty = $('#name').val();
		req.description = $('#desc').val();
		
		$.ajax({
			'type'	:	'post',
			'url'	:	'../vendor/api.php',
			'data'	:	JSON.stringify(req),
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == '1') {
				saveRow(oTable, row);
			}	
			else {
				toastr.error(res.message);
			}
		});	
	}
	
	function delRow(oTable,row){
		var req = {};
		req.action = "DeleteCategoryRow";
		req.id = $(row).attr('id');

		$.ajax({
			'type'	:	'post',
			'url'	:	'../vendor/api.php',
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == 1) {
				oTable.fnDeleteRow(row); // delete from html
				toastr.success(res.message);
			}
			
			else {
				toastr.error(res.message);
			}
			
		});
	}

	
	function difficultyData() {
		var req = {};
		// req.limit = 5;
		// req.offset = 0;
		req.action = "getCategoryData";
		$.ajax({
			'type'	:	'post',
			'url'	:	'../vendor/api.php',
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			fillData(res.data);
			
		});
	}
	function fillData(data) {
		var html = '';
		$.each(data, function(i, difficulty){
			html += '<tr id="' + difficulty.id + '" ><td>' + difficulty.name + '</td><td>' + difficulty.description + '</td><td><a class="edit" href="javascript:;">Edit</a></td><td><a class="delete" href="javascript:;">Delete</a></td></tr>';
		});
		$('#sample_editable_1 tbody').html(html);
			TableEditable.init();
		
	}
	
$(function() {
	difficultyData();	
});