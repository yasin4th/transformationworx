var limit = 0;
var page_no = 1;
var filter = false;
var programs, students;
$(function() {
	fetchStudents();
	fetchClasses();
	fetchPrograms();
	addStudent();

	
	$('select#programs').select2({placeholder: "Select Programs"});

	$('#batch').select2({placeholder: "Select Batch"});
	$('#add-student-btn').off('click');
	$('#add-student-btn').on('click',function(e){
		e.preventDefault();
		$('#registration-form').submit();
	});
	
	$('#class').on('change', function(e){
		e.preventDefault();
		if($('#class').val() != 0){
			$("#batch").select2('val',null);
			$("#batch").html('');
			fetchBatches($('#class').val());
		}
		else
		{	$('#batch').select2('val',null);
			var html = "<option value='0'> Select Batch </option>";
			$('#batch').html(html);
		}
	});

	$('input[name="from"]').datepicker();
	$('input[name="to"]').datepicker();
	$('input[name="from"]').datepicker(
	{
	    locale: {
	      format: 'DD-MM-YYYY',
	      cancelLabel: 'Clear'
	    },
	    singleDatePicker: true,
        showDropdowns: true,
        startDate: '01-01-2016',
    	endDate: '31-12-2017',
    	autoUpdateInput: false
	    
	});
	$('input[name="to"]').datepicker(
	{
	    locale: {
	      format: 'DD-MM-YYYY',
	      cancelLabel: 'Clear'

	    },
	    singleDatePicker: true,
        showDropdowns: true,
	    startDate: '01-01-2016',
    	endDate: '31-12-2017',
    	autoUpdateInput: false
	});
	$("#filters").off("click");
	$("#filters").on("click", function() {
		filterStudents();
		$('#students-filter-modal').modal('hide');
	});

})

fetchClasses = function() {
	req = {};
	req.action = 'fetchClasses';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			fillClasses(res.classes);
		}
		else{
			toastr.error('Error');
		}
	});
}

fetchPrograms = function() {
	var req = {};
	req.action = 'getTestprograms';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			programs = res.data;
		}
		else{
			toastr.error('Error');
		}
	});
				
}

fillClasses = function(classes){
	var html = "<option value='0'> Select Class </option>";
	$.each(classes,function( i,cls) {
		html += "<option value='"+cls.id+"' data-id="+cls.id+">" +cls.name+ "</option>";
	});
	$('#class').html(html);
 }

fetchBatches = function(class_id)
{
	req = {};
	req.action = 'fetchBatches';
	req.class = class_id;
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillBatches(res.batches);
		}
		else{
			toastr.error('Error');
		}
	});
}

fillBatches = function(batches)
{
	var html = "<option value='0'> Select Batch </option>";
	$.each(batches,function( i, batch) {
		html += "<option  value='"+batch.id+"' data-id="+batch.id+">" +batch.name+ "</option>";
	});
	$('#batch').html(html);
}

function validateEmail(sEmail) 
{
	var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(filter.test(sEmail))
	{
		return true;
	}
	else 
	{
		return false;
	}
}

addStudent = function(){
	$('#registration-form').on('submit', function(e) { 
		e.preventDefault();


		if(validate()) {
			var req = {};
			req.action = "register";
			req.firstname = $('#firstname').val();
			req.lastname = $('#lastname').val();
			req.email = $('#email').val();
			req.password = $('#mobile').val();
			req.mobile = $('#mobile').val();
			req.class = $('#class option:selected').text();
			req.batch = $('#batch').select2('val');
			req.type ="admin";
			
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);

				if(res.status == 1)
				{	$('#add-student-modal').modal('hide');
					$('#registration-form')[0].reset();
					toastr.success("Student Successfully added. ");
					$('#batch').html("<option value='0'> Select Batch </option>");
					fetchStudents();
				}
				else
				{
					// alert("Error:"+res.message);
					toastr.error(res.message);
				}
			});
		}
	});
}


validate = function() {
	// var pass = $('#password').val();
	// var confirm = $('#confirm-password').val();
	var regx = /^[0-9]{10}$/;

	if($('#firstname').val() != "" && $('#firstname').val().length<3){
		toastr.error("please enter alteast 3 characters in firstname.");
		return false;
	}
	if($('#firstname').val() == 0)
	{
		toastr.error("please enter firstname.");
		return false;
	}

	if($('#email').val() == 0)
	{	toastr.error("please enter email.");
		return false;
	}
	if(!validateEmail($('#email').val())){
			toastr.error('please enter valid email.');
			return false;
	}

	// if($('#mobile').val() == 0)
	// {	
	// 	toastr.error("please enter mobile no..");
	// 	return false;
	// }

	if($('#lastname').val() != "" && $('#lastname').val().length < 1) {
		toastr.error("please enter alteast 3 characters in lastname.");
		return false;
	}

	if($('#mobile').val().length && !regx.test($('#mobile').val())) {
		toastr.error("please enter valid mobile no.");
		return false;
	}

	if($('#class').val() == 0){
		toastr.error("please select class.");
		return false;
	}

	// if($('#batch').val() == 0){
	// 	toastr.error("please select batch.");
	// 	return false;
	// }
	
	// if($('#password').val() != "" && $('#password').val().length<6){
	// 	toastr.error("please enter atleast six characters long password");
	// 	return false;
	// }
	// if(confirm != pass){
	// 	toastr.error("Confirm password incorrect.");
	// 	return false;
	// }
	

	
	return true;
}

fetchStudents = function() {
	
	req = {};
	req.action = 'fetchStudents';
	req.role = '4';
	//req.limit = limit;
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		//console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			fillStudents(res.data);
			$('#total-students').html(res.total_questions);
			filter = false;
			// createPagination(res.total_questions);
			//$('#total-students').html(count(res.data));
			students = res.data;
		}
		else
		{
			toastr.error('Error');
		}
	});
	
}

// function to fill classes
function fillStudents(stud) {
	// console.log(new Date());
	html = '';
	count = 0;
	
	$.each(stud, function(i,student) {
		html += '<tr>';
		html += '<td data-order="'+student.id+'">STUDENT00'+student.id+'</td>'
		html += '<td>';
		html += student.first_name+' '+student.last_name;
		html += '</td><td>';
		html += student.email+'</td><td>'+student.mobile+'</td><td>'+student.class+'</td><td data-order="'+student.registered_on+'" data-search="'+student.registered_on1+'">'+student.registered_on1+'</td>';
		html += '<td>'+student.type+'</td>';
		// html += '<td>'+student.institute+'</td>';
		html += '<td>';
		html += '<a href="edit-student.php?id='+student.id+'"class="edit" title="Edit"><i class="fa fa-pencil"></i></a> | ';
		html += '<a href="view-report.php?id='+student.id+'"class="report-card" title="View Report Card"><i class="fa fa-eye"></i></a> | ';
		html += '<a href="#assign-program-modal" data-id="'+student.id+'" class="assign-program" title="Assign Program"><i class="fa fa-plus-circle"></i></a>';
		html += '</td>';
		html += '</tr>';
		count++ ;
	})
	if(count == 0){
		html += '<tr class="odd"><td valign="top" colspan="8" class="dataTables_empty">No students found</td></tr>';
	}
	if (typeof oTable != "undefined") {
		oTable.fnDestroy();
	}
	$('#students-table tbody').html(html);
	oTable = $('#students-table').dataTable({
		"fnDrawCallback": function( oSettings ) {
	      setEvent(); // or fnInfoCallback
	    }   
       
    });
	
	setEvent();

	// $('#total-students').html(count);
}

filterStudents = function(){
	filter = true;
	var req = {};
	req.action = 'fetchStudents';
	// req.limit = limit;
	filter = true;
	if($("input[name='from']").val()!='')
	{
		req.from = $("input[name='from']").val();
	}
	// else{
	// 	toastr.error("Please enter Date.");
	// }
	if($("input[name='to']").val()!='')
	{
		req.to = $("input[name='to']").val();
	}

	if ($('#text').val() != "") {
		req.name = $('#text').val();
	};
	if ($('#fliter-email').val() != "") {
		req.email = $('#filter-email').val().trim();
	};

	$.ajax({
		"type": "post",
		"url": EndPoint,
		"data": JSON.stringify(req) 
	}).done(function(res) {
		//console.log(res);
		res = $.parseJSON(res);

		if(res.status ==  1) {
			filter = true;
			fillStudents(res.data);
			students = res.data;
		}
		
	});	
}

setEvent = function()
{
	$('.assign-program').off('click');
	$('.assign-program').click(function(e){
		e.preventDefault();
		var req = {};
		req.userId = $(this).attr('data-id');		
		
		$('#assign-program-modal').attr('data-id', req.userId);
		req.action = 'displayPrograms';
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1)
			{
				var bought = [];
				$.each(res.data, function(i,d){
					bought.push(d.id);
				});
				fillPrograms(bought);
			}
			else
			{
				toastr.error('Error');
			}
		});
	})
}

fillPrograms = function(bought)
{

	html = '';
	stud_id = $('#assign-program-modal').attr('data-id');
	$.each(students, function(i,s){
		if(s.id == stud_id)
		{
			html += 'Name: <strong>'+s.first_name+'</strong><br>';
			html += 'Email: <strong>'+s.email+'</strong>';
		}
	})
	$('#student-details').html(html);

	html = '';
	$.each(programs, function(i,d){
		if(bought.indexOf(d.id) == -1)
		{
			html += '<option value="'+d.id+'">'+d.name+'</option>';
		}
	})

	$('select#programs').html(html);
	$('#assign-program-modal').modal('show');

	$('#assign-form').off('submit');
	$('#assign-form').on('submit', function(e){
		e.preventDefault();
		var req = {};
		req.userId = $('#assign-program-modal').attr('data-id');		
		req.programs = $('select#programs').val();
		req.action = 'assignPrograms';
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1)
			{
				toastr.success(res.message);
				$('#assign-program-modal').modal('hide');
			}
			else
			{
				toastr.error('Error');
			}
		});
	});
}

function resetFilterForm() {
	$("input:text").val("");
	/*$("#questions-filter-modal select").val(0).change();
	$('#question-status').val("").change();
	$("#tags").select2('val',null);
	$("#difficulty-levels").select2('val',null);
	$("#skills").select2('val',null);*/
}


/*
* Pagination functions
*
*/
var sn = 1;
function createPagination(total_questions) {
	if (total_questions % 50 == 0) {
		pages = total_questions / 50;
	} else{
		pages = parseInt(total_questions / 50) + 1;
	};

	html = '';
	html += '<li><a class="page-number" value="1">&laquo; &laquo; </a></li>';
	html += '<li><a class="previous-page">&laquo; Prev </a></li>';
	// html += '<span class="page-numbers">';

	for (var i = 0; i < pages; i++) {
		html += '<li class=""><a class="page-numbers page-number" value="'+(i+1)+'">'+(i+1)+'</a></li>';
	};
	// html += '</span>';
	html += '<li><a class="next-page">Next &raquo;</a></li>';
	html += '<li><a class="page-number" value="'+pages+'">&raquo;&raquo;</a></li>';

	$('.question-table-pagination').html(html);

	$('.page-number').off('click');
	$(".page-number").on('click', function(e) {
		e.preventDefault();

		page_no = parseInt($(this).attr('value'));
		limit = parseInt(page_no - 1) * 50;
		
		sn = (limit == 0)?1:limit+1; // set serial number
		
		/* calling function to fetch data*/

		if(filter){
			filterStudents();
		}else{
			fetchStudents();
		}
	});
	
	// previous button functions
	$('.previous-page').off('click');
	$('.previous-page').on('click', function(e) {
		e.preventDefault();
		if(!($(this).parent().hasClass('disabled'))) {

			page_no = parseInt($('.question-table-pagination:eq(0)').find('.active a').attr('value'));
			
			$('.question-table-pagination:eq(0) li a[value="'+(page_no - 1)+'"]').click();
		}
	});
	
	// next button functions
	$('.next-page').off('click');
	$('.next-page').on('click', function(e) {
		e.preventDefault();
		if(!($(this).parent().hasClass('disabled'))) {

			page_no = parseInt($('.question-table-pagination:eq(0)').find('.active a').attr('value'));
			$('.question-table-pagination:eq(0) li a[value="'+(page_no + 1)+'"]').click();
		}
	});
	

	// disable previous button function
	if(page_no == 1) {
		$('.question-table-pagination li .previous-page').parent().addClass('disabled');
	}
	// disable next button function
	if(page_no == parseInt($('.question-table-pagination:eq(0) li').length - 4)) {
		$('.question-table-pagination li .next-page').parent().addClass('disabled');
	}

	// adding active class to show which page is active
	$('.question-table-pagination').find('.active').removeClass('active');
	$('.question-table-pagination li a[value="'+page_no+'"]').parent().addClass('active');
	$('.question-table-pagination li .page-numbers').parent().addClass('hidden');
	$('.question-table-pagination li .page-numbers[value="'+(page_no- 1)+'"], .page-numbers[value="'+page_no+'"], .page-numbers[value="'+(page_no+ 1)+'"]').parent().removeClass('hidden');

}


var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()