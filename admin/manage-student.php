<?php include 'Access-API-sup.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php include 'html/general/headtags.php'; ?>
	<link href="assets/global/plugins/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/ media="all">

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square">
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php' ?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php' ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!--add student modal start -->
			<div class="modal fade" id="add-student-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Add Student</h4>
						</div>
						<div class="modal-body">
							 <!-- <div class="col-md-12"> -->
								<div class="row">
									<form id="registration-form" role="form">
											<div class="form-body">
												<div class="form-group ">
													<label for="firstname" class="col-lg-4 control-label margin-top-20">First Name <span class="require">*</span></label>
													<div class="col-sm-8 col-xs-6 margin-top-20">
														<input type="text" class="form-control" id="firstname">									
													</div>
												</div>
												<div class="form-group margin-top-20">
													<label for="lastname" class="col-lg-4 control-label margin-top-20">Last Name <!-- <span class="require">*</span> --></label>
													<div class="col-sm-8 col-xs-6 margin-top-20">
													<input type="text" class="form-control" id="lastname">
													</div>
												</div>
												<div class="form-group margin-top-20">
													<label for="class" class="col-lg-4 control-label margin-top-20">Class</label>
													<div class="col-sm-8 col-xs-6 margin-top-20">
													<select id="class" class="form-control "><option value="0">Select Class</option></select>
													</div>
												</div>
												<div class="form-group margin-top-20">
													<label for="class" class="col-lg-4 control-label margin-top-20">Batch</label>
													<div class="col-sm-8 col-xs-6 margin-top-20">
														<select id="batch" multiple="multiple" class="form-control" placeholder="Select Batches for the Test Package">
															<option value='0'> Select Batch </option>
														</select>
													</div>
												</div>
												<div class="form-group ">
													<label for="email" class="col-lg-4 control-label margin-top-20">Email <span class="require">*</span></label>
													<div class="col-sm-8 col-xs-6 margin-top-20">
													<input type="text" class="form-control" id="email">
													</div>
												</div>
												<div class="form-group">
													<label for="mobile" class="col-lg-4 control-label margin-top-20">Mobile No <!-- <span class="require">*</span> --></label>
													<div class="col-sm-8 col-xs-6">
													<input type="text" class="form-control margin-top-20" id="mobile">
													</div>
												</div>				
												
											</div>
										
					                </form>
								<!-- </div> -->
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue" id="add-student-btn">Add</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!--add student modal end -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Manage Students
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="manage-student.php">Manage Students</a>
					</li>
				</ul>
			</div>

			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-question-circle"></i> Students
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<div class="pull-left">
									Total Students:
									<lable id="total-students" class="">
										0
									</lable>
								</div>
							</div>
						</div>
						<div class="portlet-body">
							<!-- <form role="form">
								<div class="form-body"> -->
										<!-- <div class="col-md-8 col-xs-10">
											<div class="form-group">
											<div class="pull-left">
											 	<div class="col-sm-4">
			                                    	<div class="form-group">
					                                    <label>Date Range Filter</label> -->
					                                    <!-- <input type="text" class="form-control" placeholder="Order Date"> -->

					                                    <!-- <div class="input-group input-large" data-date="13/07/2013" data-date-format="dd/mm/yyyy">
					                                        <input type="text" class="form-control dpd1" name="from">
					                                        <span class="input-group-addon">To</span>
					                                        <input type="text" class="form-control dpd2" name="to"> 
					                                    </div>
					                                  	<div class="form-group">
															<label class="control-label">Student Name</label>
															<input type="text" class="form-control" id="text" placeholder="Student Name">													
														</div>
				                                  	</div>
				                                </div> -->
				                               
												<!-- <a href="questions.php" class="btn green"><i class="fa fa-plus-circle"></i> &nbsp; Add </a>
												<a href="import-question.php" class="btn green mrg-left5"><i class="fa fa-cloud-download"></i> &nbsp; Import</a>
												<a href="#print-questions-modal" data-toggle="modal" class="btn green mrg-left5"><i class="fa fa-print"></i> &nbsp; Print</a>
												<a id="create-test-paper" class="btn green mrg-left5"><i class="fa fa-file-text-o"></i> &nbsp;Create Test Paper </a> -->
											<!-- </div> -->
												<!-- <div>
													<lable class="btn green mrg-left5" > QC : 
														<select id="change-multiple-qc" style="color: black;">  
															<option value="0"> Pending </option> 
															<option value="1"> Done </option> 
														</select>
													</lable>
												</div> -->
											<!-- </div>
										</div> -->
										<!-- <div class="col-md-12">
											<div class="form-group">
												<a href="#students-filter-modal" data-toggle="modal" class="btn btn-circle btn-icon-only blue mrg5 tooltips" data-container="body" data-placement="top" data-original-title="Filter">
													<i class="fa fa-filter"></i>
												</a>
												<a onclick="resetFilterForm();fetchStudents();" class="btn btn-circle btn-icon-only green mrg5 tooltips" data-container="body" data-placement="top" data-original-title="Clear Filter">
													<i class="fa fa-refresh"></i>
												</a>
												<a href="#" class="btn btn-circle btn-icon-only green mrg5 tooltips" data-container="body" data-placement="top" data-original-title="Export to excel"><i class="fa fa-file-excel-o"></i></a> -->
												<!-- <i class="fa fa-cloud-download"></i> -->
											<!-- </div>
										</div>
									</div> -->
								<!-- </div>
							</form> -->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<a href="#students-filter-modal" data-toggle="modal" class="btn btn-circle btn-icon-only blue mrg5 tooltips" data-container="body" data-placement="top" data-original-title="Filter">
											<i class="fa fa-filter"></i>
										</a>
										<a onclick="resetFilterForm();fetchStudents();" class="btn btn-circle btn-icon-only green mrg5 tooltips" data-container="body" data-placement="top" data-original-title="Clear Filter">
											<i class="fa fa-refresh"></i>
										</a>
										<a href="#" id="excel-export" class="btn btn-circle btn-icon-only green mrg5 tooltips" onclick="tableToExcel('students-table', 'W3C Example Table')" data-container="body" data-placement="top" data-original-title="Export to excel"><i class="fa fa-file-excel-o"></i></a>
										<a href="#add-student-modal" class="btn green btn-icon-only btn-circle mrg5 tooltips" data-toggle="modal"  data-placement="top" data-original-title="Add Student">
										<i class="fa fa-plus-circle"></i></a>
										<!-- <i class="fa fa-cloud-download"></i> -->
									</div>
								</div>
								<!-- <div class="col-md-6">
									<ul class="question-table-pagination pagination pull-right"></ul>
								</div> -->
							</div>
							<div class="">
								<table id="students-table" class="tb-questionbank table table-striped table-hover table-bordered">
								<thead>
								<tr>
									<!-- <th>
										<div class="tooltips" data-container="body" data-placement="top" data-original-title="Select All" aria-describedby="tooltip735432">
										 <input type="checkbox" id="select-all">
										</div>
									</th> -->
									<th>
										Student ID
									</th>
									<th>
										Student Name
									</th>
									<th>
										E-mail
									</th>
									<th>
										Mobile
									</th>
									<th>
										Class
									</th>
									<th>
										Reg. Date
									</th>
									<th>
										Added By
									</th>
									<!-- <th>
										Institute
									</th> -->
									<th class="no-sort">
										Actions
									</th>									
								</tr>
								</thead>
								<tbody></tbody>
								<tfoot>
								</tfoot>
								</table>
							</div>
							<!-- <div class="row">
								<div class="col-md-12">
									<ul class="question-table-pagination pagination pull-right"></ul>
								</div>
							</div> -->
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php' ?>
<!-- END FOOTER -->
<!-- modal filter -->
<div class="modal fade" id="students-filter-modal" tabindex="-1" role="basic" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Students Filter</h4>
			</div>
			<form id="filter-form" role="form">	
				<div class="form-body">
					<div class="modal-body">
						 <div class="form-group">
							<div class="radio-list">
								<div class="row">
									<div class="col-sm-6">
                                    	<div class="form-group">
		                                    <label>Date Range Filter</label>
		                                    <!-- <input type="text" class="form-control" placeholder="Order Date"> -->

		                                    <div class="input-group" data-date="13/07/2013" data-date-format="dd/mm/yyyy">
		                                        <input type="text" class="form-control dpd1" name="from" data-date-format="dd/mm/yyyy">
		                                        <span class="input-group-addon">To</span>
		                                        <input type="text" class="form-control dpd2" name="to" data-date-format="dd/mm/yyyy"> 
		                                    </div>
	                                  	</div>
	                                </div>
									<div class="col-sm-6">
	                                  	<div class="form-group">
											<label class="control-label">Student Name</label>
											<input type="text" class="form-control" id="text" placeholder="Student Name">												
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-sm-6">
	                              	<div class="form-group">
										<label class="control-label">Email</label>
										<input type="text" class="form-control" id="filter-email" placeholder="Email">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" id="filters" class="btn green"><i class="fa fa-filter"></i> Filter</button>
						<button class="btn default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle"></i> &nbsp; Cancel</button>
					</div>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal filter -->

<!-- modal assign progran -->
<div class="modal fade" id="assign-program-modal" tabindex="-1" role="basic" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Assign Program</h4>
			</div>
			<form id="assign-form" role="form">
				<div class="form-body">
					<div class="modal-body">
						<div class="form-group">
							
							<div class="row">
								<div class="col-sm-12">
                                	<div class="form-group">
	                                    
	                                    <div id="student-details">
	                                    	
	                                    </div>                           
									</div>	                                    
                                </div>								
							</div>
							
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-sm-6">
	                              	<div class="form-group">
										<label class="control-label">Select Programs to assign</label>
										<select class="form-control" id="programs" multiple="multiple">

										</select>		
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" id="" class="btn green"><i class="fa fa-plus-circle"></i> Assign</button>
						<button class="btn default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle"></i> &nbsp; Cancel</button>
					</div>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal assign progran -->
</div>
<!-- /.modal filter -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<!-- <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> -->
<!-- <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> -->
<!-- <script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script> -->
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> -->
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<!-- <script src="assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script> -->
<script src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/dataTables.bootstrap.min.js"></script>



<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script type="text/javascript" src="js/common.js"></script>
<script src="js/custom/manage-student.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features 
   Index.init();   
   Index.initDashboardDaterange();  
   Tasks.initDashboardWidget();
});
</script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(7)').addClass('active open');
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>