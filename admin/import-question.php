<?php include 'Access-API-sup-sme-dtp.php'; ?>
<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<!-- <title>AIETS - Test Engine</title> -->
<?php include 'html/general/headtags.php' ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="page-header-fixed page-quick-sidebar-over-content page-style-square"> 
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php';?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php';?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Import Questions
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="question-bank.php">Questions Bank</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Import Questions</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cloud-download"></i> Import Questions
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" action="../vendor/fileUploads.php" method="post" enctype="multipart/form-data" id="zipform">
								<div class="form-body">
								<label>Please select the criteria to add questions</label>
									<div class="form-group">
										<label class="control-label"><span class="required">* </span>Class:</label>
										<div class="row">
											<div class="col-md-4 col-sm-4">
												<select id="classes" name="classes" class="form-control" required>
													<option> Select Class </option>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label"><span class="required">* </span>Subjects: ( multiple Subjects are allowed )</label>
										<select id="subjects" name="subjects[]" class="form-control" data-tags="true" multiple="multiple" required></select>
										<!--
											<input type="text" class="form-control" placeholder="Subject ( multiple Subject )">
										-->
									</div>
									<div class="form-group">
										<label class="control-label">Units: ( multiple Units are allowed )</label>
										<select id="units" name="units[]" class="form-control" multiple="multiple"></select>
										<!--
											<input type="text" class="form-control" placeholder="Units ( multiple units )">
										-->
									</div>
									<div class="form-group">
										<label class="control-label">Chapters: ( multiple Chapters are allowed )</label>
										<select id="chapters" name="chapters[]" class="form-control" multiple="multiple"></select>
										<!--
											<input type="text" class="form-control" placeholder="Chapters ( multiple Chapter )">
										-->
									</div>
									<div class="form-group">
										<label class="control-label">Topics: ( multiple Topics are allowed )</label>
										<select id="topics" name="topics[]" class="form-control" multiple="multiple"></select>
										<!--
											<input type="text" class="form-control" placeholder="Topics ( multiple Topic )">
										-->
									</div>
									<div class="row">
										<div class="col-md-6 form-group">
											<label class="control-label">Difficulty Level:</label>
											<select id="difficulty-levels" name="difficulty-levels[]" class="form-control" multiple="multiple"></select>
										<!--											
											<input type="text" class="form-control" placeholder="Difficulty Level ( multiple level )">
										-->
										</div>
										<div class="col-md-6 form-group">
											<label class="control-label">Skills:</label>
											<select id="skills" name="skills[]" class="form-control" multiple="multiple"></select>
											<!--
												<input type="text" class="form-control" placeholder="Skills ( multiple skilld )">
											-->
										</div>
									</div>
									<div class="form-group">
										<label class="control-label">Tags:</label>
										<select id="tags" name="tags[]" class="form-control" multiple="multiple"></select>
										<!--
											<input type="text" class="form-control" placeholder="Tags ( multiple tags )">
										-->
									</div><hr>
									<div class="form-group">
										<div class="fileinput fileinput-new" style="margin-top:10px;" data-provides="fileinput">
											<div>
												<span class="btn green btn-file" style="margin-top:10px;">
												<span class="fileinput-new">
												<i class="fa fa-cloud-download"></i> Import File</span>
												<span class="fileinput-exists">
												Change </span>
												<input id="questions_file" type="file" name="questions_file" required>
												</span>
												<a class="btn default fileinput-exists" data-dismiss="fileinput">
												Remove </a>
											</div>
										</div>
										<input id="action" type="hidden" name="action" value="action" />
										<div id="percentage" style="display: none;"></div>
									</div>
										<div>File Name: <span id="show-file-name" style="font-size: 15px;color: #35AA47;"></span> </div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-12">
											<button type="submit" class="btn green"><i class="fa fa-save"></i> &nbsp; Save Question </button>
											<a href="question-bank.php" class="btn default"> <i class="fa fa-times-circle"></i> &nbsp; Cancel</a>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>
			
			
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php'?>
<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script src="../assets/global/plugins/jquery.form.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/custom/fetchData.js"></script>
<script type="text/javascript" src="js/custom/import-question.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features 
   // Index.init();
   // Index.initDashboardDaterange();
  
});
</script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(3)').addClass('active open');
  });
  $(function () {
	  $('#program li').removeClass('active');
	  $('#program li:eq(2)').addClass('active');
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>