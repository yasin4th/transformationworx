<?php include 'Access-API-sup.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php include 'html/general/headtags.php' ?>
</head>
<!-- END HEAD -->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square">
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php' ?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php' ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			B2B Template
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="manage-institute.php">B2B Template</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-university"></i> B2B Template
							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-12">									
									<a href="#download-modal" data-toggle="modal" class="btn green mrg5 tooltips pull-right" data-container="body" data-placement="top" data-original-title="Download">
										<i class="fa fa-download"></i> Download
									</a>
								</div>								
							</div>
							<div id="b2b-details">

								
							</div>
							<div class="margin-top-10 margin-bottom-10" >
								<form role="form" id="create-b2b">
									
									<div class="row form-group">
										<div class="col-md-2">
											Website
										</div>
										<div class="col-md-6">
											<input type="text" class="form-control" id="website" name="website">
										</div>
									</div>
									<div class="row form-group">
										<div class="col-md-2">
											Features
										</div>
										<div class="col-md-6">
											<div id="features" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); min-height: 70px; position: relative;"></div>
										</div>
									</div>
									<div class="row form-group">
										<div class="col-md-2">
											About Us
										</div>
										<div class="col-md-6">
											<div id="about" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); min-height: 70px; position: relative;" ></div>
										</div>
									</div>
									<div class="row form-group">
										<div class="col-md-2">
											Directors Message
										</div>
										<div class="col-md-6">
											<div id="director" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); min-height: 70px; position: relative;" ></div>
										</div>
									</div>
									<div class="row form-group">
										<div class="col-md-2">
											Courses
										</div>
										<div class="col-md-6">
											<div id="courses" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); min-height: 70px; position: relative;"></div>
										</div>
									</div>
									<div class="row form-group">
										<div class="col-md-2">
											Contact Us
										</div>
										<div class="col-md-6">
											<div id="contact" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); min-height: 70px; position: relative;"></div>
										</div>
									</div>
									<div class="row form-group">
										<div class="col-md-2">
											
										</div>
										<div class="col-md-6">
											<button class="btn btn-success" type="submit"><i class="fa fa-save"></i> Save</button>
										</div>
									</div>
										
									
								</form>
								
									
							</div>
							
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>


			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php' ?>
<!-- END FOOTER -->
<!-- Start Add Institute Modal -->

<!-- End Edit Institute Modal -->
<!-- Start Institute Filter Modal -->
	<div class="modal fade" id="logo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" aria-hidden="true">
        <div class="modal-dialog">
          <form id="upload-logo" role="form">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Upload Logo</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Logo</label>
                        <input type="file" id="logo" name="logo"  class=" form-control" placeholder="Ex. Absorbents">
                    </div>
                  </div> 
                  <input name="action" value="b2blogo" style="display:none;">
                  <input name="id" value="0" style="display:none;">
                  
                </div>
                
              </div>
              <div class="modal-footer">
                <button class="btn btn-success" type="submit"><i class="fa fa-plus"></i> Upload</button>
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              </div>
            </div>
          </form>
        </div>
      </div>

      <div class="modal fade" id="banner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" aria-hidden="true">
        <div class="modal-dialog">
          <form id="upload-banner" role="form">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Upload Banner</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Banner</label>
                        <input type="file" id="banner" name="banner"  class=" form-control" placeholder="Ex. Absorbents">
                    </div>
                  </div> 
                  <input name="action" value="b2bbanner" style="display:none;">
                  <input name="id" value="" style="display:none;">
                  
                </div>
                
              </div>
              <div class="modal-footer">
                <button class="btn btn-success" type="submit"><i class="fa fa-plus"></i> Upload</button>
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              </div>
            </div>
          </form>
        </div>
      </div>

      <div class="modal fade" id="download-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" aria-hidden="true">
        <div class="modal-dialog">
          <form id="download" role="form">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Choose Template</h4>
              </div>
              <div class="modal-body">
				<div class="row">
				
					<div class="col-md-12">
					  <label>
					    <input type="radio" name="template" value="1" checked>
					    <img src="../assets/instituteimages/template/template1.PNG" height="140px">
					  </label>
					</div>
					<div class="col-md-12">
					  <label>
					    <input type="radio" name="template" value="2">
					    <img src="../assets/instituteimages/template/template2.PNG" height="140px">
					  </label>
					</div>
					<div class="col-md-12">
					  <label>
					    <input type="radio" name="template" value="3">
					    <img src="../assets/instituteimages/template/template3.PNG" height="140px">
					  </label>
					</div>

                </div>
                
              </div>
              <div class="modal-footer">
                <!-- <button class="btn btn-success" type="submit"><i class="fa fa-plus"></i> Download</button> -->
                <a href="" class="btn btn-success" id="download-link"><i class="fa fa-download"></i> Download</a>
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              </div>
            </div>
          </form>
        </div>
      </div>
<!-- End Institute Filter Modal -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/global/plugins/ckeditor/basic-toolbar-config.js?t=F0RD"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<script src="js/custom/manage-banner.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features
   Index.init();
});
</script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(15)').addClass('active open');
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>