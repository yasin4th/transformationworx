<?php include 'Access-API-sup-sme-dtp.php'; ?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.1
Version: 3.6
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- <link rel="stylesheet" type="text/css" href="./assets/global/plugins/mathquill/mathquill.css"> -->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<!-- <title>AIETS - Test Engine</title> -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/mathquill/mathquill.css">
<link rel="stylesheet" type="text/css" href="assets/global/plugins/ckeditor/skins/kama/autoring.css">
<?php include 'html/general/headtags.php' ?>
<style>
	.table>tbody>tr>td {
		padding: 0px;
	}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square"> 
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php'; ?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php'; ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Student Details
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="manage-student.php">Manage Student</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a onclick="location.reload();">Edit Student</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<!-- BEGIN PAGE CONTENT-->
			
			<div class="row">
			    <div class="col-md-12">
			        <!-- BEGIN SAMPLE FORM PORTLET-->
			        <div class="portlet box green ">
			            <div class="portlet-title">
			                <div class="question-id caption">
			                    Edit Student
			                </div>
			                <div class="tools">
			                    <a href="javascript:;" class="collapse">
			                    </a>
			                </div>
			            </div>
			            <div class="portlet-body form label-left">			                

	                        <div class="row">
	                        	<div class="col-md-6 col-sm-6">
									<form id="edit-form"  role="form">										
										<div class="form-body">
											<div class="form-group ">
												<label for="firstname" class="col-lg-4 control-label margin-top-20">First Name <span class="require">*</span></label>
												<div class="col-sm-8 col-xs-6 margin-top-20">
													<input type="text" class="form-control" id="firstname">															
											</div>
											<div class="form-group margin-top-20">
												<label for="lastname" class="col-lg-4 control-label margin-top-20">Last Name <!-- <span class="require">*</span> --></label>
												<div class="col-sm-8 col-xs-6 margin-top-20">
												<input type="text" class="form-control" id="lastname">
												</div>
											</div>
											<div class="form-group margin-top-20">
													<label for="class" class="col-lg-4 control-label margin-top-20">Class</label>
													<div class="col-sm-8 col-xs-6 margin-top-20">
													<select id="class" class="form-control "><option value="0">Select Class</option></select>
													</div>
												</div>
												<div class="form-group margin-top-20">
													<label for="class" class="col-lg-4 control-label margin-top-20">Batch</label>
													<div class="col-sm-8 col-xs-6 margin-top-20">
														<select id="batch" multiple="multiple" class="form-control" placeholder="Select Batches for the Test Package">
															<option value='0'> Select Batch </option>
														</select>
													</div>
												</div>
											<div class="form-group ">
												<label for="email" class="col-lg-4 control-label margin-top-20">Email <span class="require">*</span></label>
												<div class="col-sm-8 col-xs-6 margin-top-20">
												<input type="text" class="form-control" id="email">
												</div>
											</div>
											<div class="form-group">
												<label for="mobile" class="col-lg-4 control-label margin-top-20">Mobile No</label>
												<div class="col-sm-8 col-xs-6">
												<input type="text" class="form-control margin-top-20" id="mobile">
												</div>
											</div>				
											
											<div class="row">
												<div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
													<br>
													<div class="row">													
													<button id="register" type="submit" class="btn btn-primary">Submit</button>
													<button type="button" class="btn btn-default" id ="cancel-btn">Cancel</button>
													</div>
													<br><br>
													<div class="row">
							                		<button id="changepass" type="button" class="btn btn-primary">Reset Password</button>
													<a data-toggle="modal" href="#change-password-modal" id="change-password" class="btn btn-primary">Change Password</a>
													</div>
												</div>
											</div>
										</div>
									</form>
				                </div>
	                            
	                        </div>	                
			                                

			                           
			            </div>
			        </div>
			        <!-- END SAMPLE FORM PORTLET-->

			       <div class="modal fade" id="change-password-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" aria-hidden="true">
			        <div class="modal-dialog">
			          <form id="change-password-form" role="form" method="post">
			            <div class="modal-content">
			              <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                <h4 class="modal-title">Change Password</h4>
			              </div>
			              <div class="modal-body">
			                <div class="row">
			                  
			                  <div class="col-sm-8">
			                    <div class="form-group">
			                        <label>Password</label>
			                        <input type="password" id="new-password" name="new-password" required="required" class=" form-control" >
			                    </div>
			                  </div>
			                  <div class="col-sm-8">
			                    <div class="form-group">
			                        <label>Confirm Password</label>
			                        <input type="password" id="new-password1" name="new-password1" required="required" class=" form-control" >
			                    </div>
			                  </div>			                  
			                
			                </div>
			              </div>
			              <div class="modal-footer">
			                <button class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Submit</button>
			                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			              </div>
			            </div>
			          </form>
			        </div>
			      </div>



			    </div>
			</div>
			
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php';?>
<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- BEGIN PLUGINS USED BY X-EDITABLE -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.zh-CN.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/moment.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-editable/inputs-ext/address/address.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-editable/inputs-ext/wysihtml5/wysihtml5.js"></script>
<!-- END X-EDITABLE PLUGIN -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<!-- <script type="text/javascript" src="assets/global/plugins/mathquill/mathquill.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/ckeditor/ckeditor.js"></script> -->
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/form-editable.js"></script>
<script src="assets/admin/pages/scripts/components-editors.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/custom/edit-student.js"></script>
<!-- <script src="assets/global/plugins/mathquill//mathquill.min.js"></script> -->
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features 
   /*Index.init();   
   Index.initDashboardDaterange();
   Index.initJQVMAP(); // init index page's custom scripts
   Index.initCalendar(); // init index page's custom scripts
   Index.initCharts(); // init index page's custom scripts
   Index.initChat();
   Index.initMiniCharts();
   Tasks.initDashboardWidget();*/
   // FormEditable.init();
});
</script>
<script type="text/javascript">
 $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(7)').addClass('active open');
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>