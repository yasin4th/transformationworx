<?php include 'Access-API-sup.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<!-- <title>AIETS - Test Engine</title> -->
	<?php include 'html/general/headtags.php' ?>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-quick-sidebar-over-content page-style-square">
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php' ?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php' ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Manage Institute
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="manage-institute.php">Manage Institute</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-university"></i> Manage Institute
							</div>
						</div>
						<div class="portlet-body">


							<!-- <form id="create-institute-form">
								<div id="add-manage-institute">
									<div class="row">
										<div class="col-md-3 col-sm-4">
											<div class="form-group">
												<label class="control-label">Institute Name</label>
												<div class="row">
													<div class="col-sm-12">
														<input type="text" class="name form-control" placeholder="Institute Name">
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 col-sm-4">
											<div class="form-group">
												<label class="control-label">Email / User Name</label>
												<div class="row">
													<div class="col-sm-12">
														<input type="email" class="email form-control" placeholder="example@demo.com">
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 col-sm-4">
											<div class="form-group">
												<label class="control-label">Password</label>
												<div class="row">
													<div class="col-sm-12">
														<input type="password" class="password form-control" placeholder="Password">
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 col-sm-4">
											<div class="form-group">
												<label class="control-label">Phone/Mobile</label>
												<div class="row">
													<div class="col-md-12">
														<input type="text" class="phn-no form-control" placeholder="Phone">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-9">
											<div class="form-group">
												<label class="control-label">Details</label>
												<div class="row">
													<div class="col-md-12">
														<textarea  class="details form-control" rows="2" placeholder="Details"></textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<button type="submit" class="btn green pull-right mar-top45"><i class="fa fa-plus-circle"></i> &nbsp; Create </button>
										</div>
									</div>
								</div>
							</form> -->
							<!-- <form id="edit-institute-form" style="display: none;">
								<div id="edit-manage-institute">
									<div class="row">
										<div class="col-md-3 col-sm-4">
											<div class="form-group">
												<label class="control-label">Institute Name</label>
												<div class="row">
													<div class="col-sm-12">
														<input type="text" class="name form-control" placeholder="Institute Name">
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 col-sm-4">
											<div class="form-group">
												<label class="control-label">Email / User Name</label>
												<div class="row">
													<div class="col-sm-12">
														<input type="email" class="email form-control" placeholder="example@demo.com">
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 col-sm-4">
											<div class="form-group">
												<label class="control-label">Password</label>
												<div class="row">
													<div class="col-sm-12">
														<input type="password" class="password form-control" placeholder="Password">
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 col-sm-4">
											<div class="form-group">
												<label class="control-label">Phone/Mobile</label>
												<div class="row">
													<div class="col-md-12">
														<input type="text" class="phn-no form-control" placeholder="Phone">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-8">
											<div class="form-group">
												<label class="control-label">Details</label>
												<div class="row">
													<div class="col-md-12">
														<textarea  class="details form-control" rows="2" placeholder="Details"></textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<a class="cancel btn default pull-right mar-top45"><i class="fa fa-times-circle"></i> &nbsp; Cancel </a>
											<button type="submit" class="btn green pull-right mar-top45 mrg-right5"><i class="fa fa-plus-circle"></i> &nbsp; Save </button>
										</div>
									</div>
								</div>
							</form> -->
							<div class="row">
								<div class="col-md-6">
									<a id="filters" class="btn btn-circle btn-icon-only blue mrg5 tooltips" data-container="body" data-placement="top" data-original-title="Filter">
										<i class="fa fa-filter"></i>
									</a>
									<a onclick="Institute.Events.resetForm();Institute.Events.getTheInstitutes();" class="btn btn-circle btn-icon-only green  mrg5 tooltips" data-container="body" data-placement="top" data-original-title="Clear Filter">
										<i class="fa fa-refresh"></i>
									</a>
									<a href="#add-institute-modal" data-toggle="modal" class="btn btn-circle btn-icon-only green mrg5 tooltips" data-container="body" data-placement="top" data-original-title="Add Institute">
										<i class="fa fa-plus-circle"></i>
									</a>
								</div>
								<div class="col-md-6">
									<ul class="question-table-pagination pagination pull-right"></ul>
								</div>
							</div>
							<div class="margin-top-10 margin-bottom-10 table-scrollable">
								<table id="institute-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>
												Name
											</th>
											<th>
												Email
											</th>
											<th>
												Phone
											</th>
											<th>Credits</th>
											<th>
												Lab Status
											</th>
											<th>
												Status
											</th>
											<th style="width:230px;">
												Action
											</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
							<div class="row">
								<div class="col-md-12">
									<ul class="question-table-pagination pagination pull-right"></ul>
								</div>
							</div>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>


			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php' ?>
<!-- END FOOTER -->
<!-- Start Add Institute Modal -->
<div class="modal fade" id="add-institute-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add Institute</h4>
			</div>
			<form id="create-institute-form" role="form">
				<div id="add-manage-institute">
					<div class="modal-body">

						<div class="row">
						<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Institute Type</label>
									<div class="row">
										<div class="col-sm-12">
											<label class="radio-inline"><input type="radio" value="1" class="ins_lab_type" name="ins_lab_type" checked>With Lab</label>
											<label class="radio-inline"><input type="radio" value="0" class="ins_lab_type" name="ins_lab_type">Without Lab</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Institute Name</label>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="name form-control" placeholder="Institute Name">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Email / User Name</label>
									<div class="row">
										<div class="col-sm-12">
											<input type="email" class="email form-control" placeholder="example@demo.com">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Password</label>
									<div class="row">
										<div class="col-sm-12">
											<input type="password" class="password form-control" placeholder="Password">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Phone/Mobile</label>
									<div class="row">
										<div class="col-md-12">
											<input type="text" class="phn-no form-control" placeholder="Phone">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">Details</label>
									<div class="row">
										<div class="col-md-12">
											<textarea  class="details form-control" rows="2" placeholder="Details"></textarea>
										</div>
									</div>
								</div>
							</div>
							<!-- <div class="col-md-3">
								<button type="submit" class="btn green pull-right mar-top45"><i class="fa fa-plus-circle"></i> &nbsp; Create </button>
							</div> -->
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn green"><i class="fa fa-plus-circle"></i> &nbsp; Create </button>
							<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
						</div>
					</div>

				</div>
			</form>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>
<!-- End Add Institute Modal -->
<!-- Start Edit Institute Modal -->
<div class="modal fade" id="edit-institute-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit Institute</h4>
			</div>
			<form id="edit-institute-form" role="form">
				<div id="edit-manage-institute">
					<div class="modal-body">
						<div class="row">
						<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Institute Type</label>
									<div class="row">
										<div class="col-sm-12">
										<label class="radio-inline"><input type="radio" value="1" class="ins_lab_type" name="ins_lab_type" checked>With Lab</label>
										<label class="radio-inline"><input type="radio" value="0" class="ins_lab_type" name="ins_lab_type">Without Lab</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Institute Name</label>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" class="name form-control" placeholder="Institute Name">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Email / User Name</label>
									<div class="row">
										<div class="col-sm-12">
											<input type="email" class="email form-control" placeholder="example@demo.com">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label class="control-label">Password</label>
									<div class="row">
										<div class="col-sm-12">
											<input type="password" class="password form-control" placeholder="Password">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6">
								<div class="form-group">
									<label class="control-label">Phone/Mobile</label>
									<div class="row">
										<div class="col-md-12">
											<input type="text" class="phn-no form-control" placeholder="Phone">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">Details</label>
									<div class="row">
										<div class="col-md-12">
											<textarea  class="details form-control" rows="2" placeholder="Details"></textarea>
										</div>
									</div>
								</div>
							</div>
							<!-- <div class="col-md-4">
								<a class="cancel btn default pull-right mar-top45"><i class="fa fa-times-circle"></i> &nbsp; Cancel </a>
								<button type="submit" class="btn green pull-right mar-top45 mrg-right5"><i class="fa fa-plus-circle"></i> &nbsp; Save </button>
							</div> -->
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn green"><i class="fa fa-plus-circle"></i> &nbsp; Save</button>
							<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
						</div>
					</div>

				</div>
			</form>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>
<!-- End Edit Institute Modal -->
<!-- Start Institute Filter Modal -->
<div class="modal fade" id="institute-filter-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Institute Filter</h4>
			</div>
			<form role="form" id="filter-form">
				<div class="modal-body">

					<!-- <div class="row">
						<div class="col-md-12">
							<button onclick="Institute.Events.resetForm();" class="btn btn-default pull-right" > Reset </button>
						</div>
					</div> -->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Institute Name</label>
								<div class="row">
									<div class="col-md-12">
										<input type="text" class="form-control" id="institute" placeholder="Institute Name">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Email</label>
								<div class="row">
									<div class="col-md-12">
										<input type="text" class="form-control" id="email" placeholder="email">
									</div>
								</div>
							</div>
						</div>


					</div>

					<div class="modal-footer">
						<button id="filter-institute" type="submit" class="btn blue">Filter</button>
						<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</form>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>
<!-- End Institute Filter Modal -->
<!-- Start Generate Student Id Modal -->
<div class="modal fade" id="generatestudent-id-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Generate Student Id</h4>
			</div>

				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Select Program</label>
								<div class="row">
									<div class="col-md-12">
										<select class="form-control" id="programs">
											<option value="0">Select Program</option>

										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">No. of Student id</label>
								<div class="row">
									<div class="col-md-12">
										<input class="form-control" placeholder="No. of Student id" id="number">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<!-- <a href="download-template.php" class="btn blue" id="download">Create</a> -->
						<a href="download-template.php" class="btn blue disabled" id="download" ><i class="fa fa-download"></i> Download</a>
						<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
					</div>
				</div>

			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>
<!-- End Generate Student Id Modal -->

<!-- Start Assign Modal -->
<div class="modal fade" id="assign-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Assign Program</h4>
			</div>
			<form role="form" id="assign-program">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Select Program</label>
								<div class="row">
									<div class="col-md-12">
										<select class="form-control" id="allPrograms">
											<option value="0">Select Program</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button id="btn-assign-program" type="submit" class="btn blue">Assign</button>
						<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</form>

			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>
<!-- End Assign Modal -->

<!-- Start Assign Modal -->
<div class="modal fade" id="assign-credits-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Assign Credits</h4>
			</div>
			<form role="form" id="assign-credits-program">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Enter Credits</label>
								<div class="row">
									<div class="col-md-12">
										<input class="form-control" id="credits" type="text">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button id="btn-assign-credits-program" type="submit" class="btn blue">Assign</button>
						<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</form>

			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
</div>
<!-- End Assign Modal -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<script src="js/custom/manage-institute.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features
   Index.init();
});
</script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(18)').addClass('active open');
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>