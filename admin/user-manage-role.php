php include 'Access-API-sup.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<!-- <title>AIETS - Test Engine</title> -->
	<?php include 'html/general/headtags.php' ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="page-header-fixed page-quick-sidebar-over-content page-style-square"> 
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php' ?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php' ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			User Roles
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="user-manage-role.php">User Roles</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-file-text-o"></i> Manage Roles
							</div>
						</div>
						<div class="portlet-body">
							<form id="create-admin" enctype="multipart/form-data">
								<div id="add-admin">
									<div class="row">
										<div class="col-md-3 col-sm-4">
											<div class="form-group">
												<label class="control-label">Name</label>
												<div class="row">
													<div class="col-sm-12">
														<input type="text" class="name form-control" placeholder="Name">
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 col-sm-4">
											<div class="form-group">
												<label class="control-label">Email / User Name</label>
												<div class="row">
													<div class="col-sm-12">
														<input type="email" class="email form-control" placeholder="example@demo.com">
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 col-sm-4">
											<div class="form-group">
												<label class="control-label">Password</label>
												<div class="row">
													<div class="col-sm-12">
														<input type="password" class="password form-control" placeholder="Password">
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 col-sm-4">
											<div class="form-group">
												<label class="control-label">Role</label>
												<div class="row">
													<div class="col-md-12">
														<select class="user-role form-control">
															<option value="0">Select Role</option>
															<option value="1">Super Admin</option>
															<option value="2">SME</option>
															<option value="3">Data Entry (DTP)</option>
														</select>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-sm-12">
											<!-- <button type="button" class="btn default pull-right">Cancel</button> -->
											<button type="submit" class="btn green pull-right mrg-right5"><i class="fa fa-plus-circle"></i> &nbsp; Create </button>
										</div>
									</div>
								</div>
							</form>
							<form id="edit-admin-details" enctype="multipart/form-data">
								<div id="edit-admin" style="display: none;">
									<div class="row">
										<div class="col-md-3 col-sm-4">
											<div class="form-group">
												<label class="control-label">Name</label>
												<div class="row">
													<div class="col-sm-12">
														<input type="text" class="name form-control" placeholder="Name">
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 col-sm-4">
											<div class="form-group">
												<label class="control-label">Email / User Name</label>
												<div class="row">
													<div class="col-sm-12">
														<input type="text" class="email form-control" placeholder="E-mail">
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 col-sm-4">
											<div class="form-group">
												<label class="control-label">Password</label>
												<div class="row">
													<div class="col-sm-12">
														<input type="password" class="password form-control" placeholder="Password">
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 col-sm-4">
											<div class="form-group">
												<label class="control-label">Role</label>
												<div class="row">
													<div class="col-md-12">
														<select class="user-role form-control">
															<option value="0">Select Role</option>
															<option value="1">Super Admin</option>
															<option value="2">SME</option>
															<option value="3">Data Entry (DTP)</option>
														</select>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-sm-12">
											<button type="button" class="cancel btn default pull-right">Cancel</button>
											<button type="submit" class="btn green pull-right mrg-right5"><i class="fa fa-floppy-o"></i> &nbsp; Save </button>
										</div>
									</div>
								</div>
							</form>
							<div class="margin-top-10 margin-bottom-10 table-scrollable">
								<table id="users-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>
												Name
											</th>
											<th>
												Role
											</th>
											<th>
												Status
											</th>
											<th style="width:150px;">
												Action
											</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>
			
			
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php' ?>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="js/common.js" type="text/javascript"></script>
<script src="js/custom/user-manage-role.js" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features 
   Index.init(); 
});
</script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(17)').addClass('active open');
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>