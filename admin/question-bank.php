<?php include 'Access-API-sup-sme-dtp.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<!-- <title>AIETS - Test Engine</title> -->
<?php include 'html/general/headtags.php'; ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square">
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php';?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php';?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<!--<h3 class="page-title">
			Question Bank
			</h3>-->
			<div class="row">
				<div class="col-md-6">
					<h3 class="page-title">
					Question Bank
					</h3>
				</div>
				<div class="col-md-6"></div>
			</div>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="question-bank.php">Question Bank</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-question-circle fa-lg"></i> Question Filter
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<div class="pull-left">
									Total Quetions:
									<lable id="total-questions" class="">
										0
									</lable>
								</div>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form">
								<div class="form-body">
									<div class="row">
										<div class="col-md-10 col-xs-10">
											<div class="form-group">
											<div class="pull-left">
												<a href="questions.php" class="btn green"><i class="fa fa-plus-circle"></i> &nbsp; Add </a>
												<a href="import-question.php" class="btn green mrg-left5"><i class="fa fa-cloud-download"></i> &nbsp; Import</a>
												<a href="#print-questions-modal" data-toggle="modal" class="btn green mrg-left5"><i class="fa fa-print"></i> &nbsp; Print</a>

<?php
@session_start();
if($_SESSION['role'] != 3){
 ?>												<a id="create-test-paper" class="btn green mrg-left5"><i class="fa fa-file-text-o"></i> &nbsp;Create Test Paper </a>
												<a id="delete-question" class="btn green mrg-left5"><i class="fa fa-trash-o"></i> &nbsp;Delete </a>

											</div>
												<div>
													<lable class="btn green mrg-left5" > QC :
														<select id="change-multiple-qc" style="color: black;">
															<option value="0"> Pending </option>
															<option value="1"> Done </option>
														</select>
													</lable>
												</div>
											</div><?php }?>
										</div>
										<div class="col-md-2 col-xs-2">
											<div class="form-group">
												<!--<a onclick="resetFilterForm();" class="btn btn-circle btn-icon-only green pull-right tooltips" style="margin: 5px;" data-original-title="Refresh">-->
												<a href="question-bank.php" class="btn btn-circle btn-icon-only green pull-right tooltips" style="margin: 5px;" data-original-title="Refresh">
													<i class="fa fa-refresh"></i>
												</a>

												<a id="filters" class="btn btn-circle btn-icon-only blue pull-right tooltips" style="margin: 5px;" data-original-title="Filter">
													<i class="fa fa-filter"></i>
												</a>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<ul class="question-table-pagination pagination pull-right"></ul>
										</div>
									</div>
									<div class="table-scrollable">
										<table id="questions-table" class="tb-questionbank table table-striped table-hover table-bordered">
										<thead>
										<tr>
											<th>
												<div class="tooltips" data-container="body" data-placement="top" data-original-title="Select All" aria-describedby="tooltip735432">
												 <input type="checkbox" id="select-all">
												</div>
											</th>
											<th>
												Sr. No.
											</th>
											<th>
												Type
											</th>
											<th>
												Ques. Id
											</th>
											<!-- <th>
												Class
											</th>
											<th>
												Subjects
											</th> -->
											<th>
												Question
											</th>
											<th>
												QC
											</th>
											<th>
												Modified Date
											</th>
											<th>
												Action
											</th>
										</tr>
										</thead>
										<tbody></tbody>
										<tfoot>
										</tfoot>
										</table>
									</div>
									<div class="row">
										<div class="col-md-12">
											<ul class="question-table-pagination pagination pull-right"></ul>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>

			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php';?>
<!-- END FOOTER -->
<!-- .modal -->
	<div class="modal fade bs-modal-lg" id="questions-filter-modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Question Filter</h4>
				</div>
				<div class="modal-body">

					<div class="row">
						<div class="col-md-12">
							<button onclick="resetFilterForm()" class="btn btn-default pull-right" > Reset </button>
							<label>Please select the criteria to set question paper</label>
						</div>
					</div><br>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Class</label>
								<div class="row">
									<div class="col-md-12">
										<select id="classes" class="form-control">
											<option value="0">Select Class</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Select Subject</label>
								<div class="row">
									<div class="col-md-12">
										<select id="subjects" class="form-control" data-tags="true" multiple="multiple"></select>
										<!--
											<input type="text" class="form-control pull-right" placeholder="Subjects ( multiple subject )">
										-->
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Select Unit</label>
								<div class="row">
									<div class="col-md-12">
										<select id="units" class="form-control" multiple="multiple"></select>
										<!--
											<input type="text" class="form-control" placeholder="Unit">
										-->
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Select Chapter</label>
								<div class="row">
									<div class="col-md-12">
										<select id="chapters" class="form-control" multiple="multiple"></select>
										<!--
											<input type="text" class="form-control" placeholder="Chapter">
										-->
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Select Topics</label>
								<div class="row">
									<div class="col-md-12">
										<select id="topics" class="form-control" multiple="multiple"></select>
										<!--
											<input type="text" class="form-control pull-right" placeholder="Topic">
										-->
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Enter Difficulty Level</label>
								<div class="row">
									<div class="col-md-12">
										<select id="difficulty-levels" class="form-control" multiple="multiple"></select>
										<!--
											<input type="text" class="form-control pull-right"
											placeholder=" Difficulty Level">
										--->
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Enter skills</label>
								<div class="row">
									<div class="col-md-12">
										<select id="skills" class="form-control" multiple="multiple"></select>
										<!--
											<input type="text" class="form-control" placeholder="Skills ( multiple skills )">
										-->
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Enter Tags</label>
								<div class="row">
									<div class="col-md-12">
										<select id="tags" class="form-control" multiple="multiple"></select>
										<!--
											<input type="text" class="form-control" placeholder="Tags ( multiple tags )">
										-->
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Question Type</label>
								<div class="row">
									<div class="col-md-12">
										<select id="question-types" class="form-control pull-right"></select>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Question Id </label>
								<div class="row">
									<div class="col-md-12">
										<input id="question-id" type="text" class="form-control pull-right" placeholder="Question Id">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">QC</label>
								<div class="row">
									<div class="col-md-12">
										<select id="question-status" class="form-control pull-right">
											<option value="">Select QC</option>
											<option value="0">Pending</option>
											<option  value="1">Done</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Question Text:</label>
								<div class="row">
									<div class="col-md-12">
										<input type="text" class="form-control" id="question-text" plcaholder="Question Text">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Question Id (From) </label>
								<div class="row">
									<div class="col-md-12">
										<input id="question-from-id" type="text" class="form-control pull-right" placeholder="Question Id (From)">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Question Id (To)</label>
								<div class="row">
									<div class="col-md-12">
										<input id="question-to-id" type="text" class="form-control pull-right" placeholder="Question Id (To)">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button id="filter-questions" type="button" class="btn blue">Filter</button>
					<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>

<!-- /.modal -->
<!-- modal Print -->
<div class="modal fade" id="print-questions-modal" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Print Question Bank</h4>
			</div>
			<div class="modal-body">
				 <div class="form-group">
					<div class="radio-list">
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<label class="radio-inline">
								<input type="radio" name="print-question-radio" value="without-answer"> Print without answer </label>
							</div>
							<div class="col-md-6 col-sm-6">
								<label class="radio-inline">
								<input type="radio" name="print-question-radio" value="with-answer"> Print with answer </label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<button id="print-questions" type="button" class="btn blue"><i class="fa fa-print"></i> &nbsp; Print</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal Print -->

<!-- Modal -->

	<div id="test-paper-details-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title control-label"><strong>Test Paper</strong></h4>
				</div>
				<div class="modal-body">
					<div class="portlet-body form">
						<form class="form-horizontal" role="form">
							<div class="form-body">
								<div class="form-group">
									<label class="col-md-3 control-label">Name</label>
									<div class="col-md-9">
										<input id="test-paper-name" type="text" class="form-control" placeholder="Test Paper">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Type</label>
									<div class="col-md-9">
										<select id="test-paper-type" class="form-control">
											<option value="0">Select Type</option>

										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Class:</label>
									<div class="col-md-9">
										<select id="test-paper-class" class="form-control">
											<option value="0">Select Class</option>
										</select>
										<input id="test-paper-id" type="hidden" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Subject:</label>
									<div class="col-md-9">
										<select id="test-paper-subjects" multiple="multiple" class="form-control">
											<option value="0">Select Subject</option>
										</select>
									</div>
								</div>
								<div id="test-time" class="row">
									<div class="col-md-6">
										<div class="form-group">
											<div class="col-md-12">
												<label class="control-label">Start Date / Time</label>
											</div>
											<div class="col-md-12">
												<div class="input-group date">
													<input id="test-paper-start" type="text" class="form-control" placeholder="Start Date/Time">
								                    <span class="input-group-addon">
								                        <span class="glyphicon glyphicon-calendar"></span>
								                    </span>
								                </div>
							                </div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<div class="col-md-12">
												<label class="control-label">End Date / Time</label>
											</div>
											<div class="col-md-12">
												<div class="input-group date">
													<input id="test-paper-end" type="text" class="form-control" placeholder="End Date/Time">
													<span class="input-group-addon">
								                        <span class="glyphicon glyphicon-calendar"></span>
								                    </span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div id="time-div">
									<div class="form-group">
										<label class="col-md-3 control-label">Time(In Minutes):</label>
										<div class="col-md-9">
											<input id="test-paper-time" type="number" min="0" class="form-control" placeholder="Time ( In Minutes )" value="0">
										</div>
									</div>
								</div>
								<div id="marks-time-div">

									<div class="form-group" id="attempts">
										<label class="col-md-3 control-label">Total Attempts</label>
										<div class="col-md-9">
											<input id="test-paper-attempts" type="number" class="form-control" placeholder="No of Attempts" min="1">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Max Marks</label>
										<div class="col-md-9">
											<input id="test-paper-max-marks" type="number" class="form-control" placeholder="Max Marks" min="0">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Neg Marks</label>
										<div class="col-md-9">
											<input id="test-paper-neg-marks" type="number" class="form-control" placeholder="Neg Marks" min="0">
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-3 control-label">Cut-off Marks</label>
										<div class="col-md-9">
											<input id="test-paper-cutoff" type="number" min="0" class="form-control" placeholder="Cut-off Marks">
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-3 control-label">Instructions</label>
										<div class="col-md-9">
										<!-- <textarea id="test-paper-instructions" class="form-control" rows="3" style="margin: 0px -6.625px 0px 0px; width: 100%; height: 150px;" placeholder="Enter Instructions for the test paper"></textarea> -->
										<div id="test-paper-instructions" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); min-height: 70px; position: relative;" data-ng-model="myModel"><p> Please Fill Instructions... </p></div>

										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="modal-footer">
					<button id="save-test-paper" class="btn green pull-left"><i class="fa fa-save"></i> &nbsp; Save</button>
					<button class="btn default pull-left" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle"></i> &nbsp; Cancel</button>
				</div>
			</div>
		</div>
	</div>

	<div id="print-preview-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title control-label"><strong>Print Preview</strong></h4>
				</div>
				<div class="modal-body">
					<div class="portlet-body">

					<!--*****************************************-->
						<!-- This is for printing -->
					<!--*****************************************-->
				        <div id="div-to-print" style=" width: 96%; border: solid 1px; padding: 10px;"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button id="final-print" onclick="$('#div-to-print').print()" class="btn green pull-left"><i class="fa fa-print"></i> Print</button>
					<button class="btn default pull-left" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle"></i> &nbsp; Cancel</button>
				</div>
			</div>
		</div>
	</div>
<!-- /.modal -->
	<!-- BEGIN SAMPLE FORM PORTLET-->
    <!-- END SAMPLE FORM PORTLET-->
<!--*****************************************-->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jQuery.print.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>

<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/admin/pages/scripts/table-advanced.js" type="text/javascript"></script>

<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/custom/question-bank/fetchData.js"></script>
<script type="text/javascript" src="js/custom/question-bank/question-bank.js"></script>
<script type="text/javascript" src="js/custom/question-bank/questions1.js"></script>
<script type="text/javascript" src="js/custom/question-bank/question-bank-questions-filter.js"></script>
<script type="text/javascript" src="js/custom/question-bank/create-and-print-test-paper.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features
   /*
   Index.init();
   Index.initDashboardDaterange();
   Index.initJQVMAP(); // init index page's custom scripts
   Index.initCalendar(); // init index page's custom scripts
   Index.initCharts(); // init index page's custom scripts
   Index.initChat();
   Index.initMiniCharts();
   Tasks.initDashboardWidget();
   */
});
</script>
<script type="text/javascript">
var role = 1;
<?php
	@session_start();
	if($_SESSION['role'] == 3) {
?>
		var role = 3;
<?php
	}
?>
	function mathsjax() {
		var script = document.createElement("script");
		script.type = "text/javascript";
		script.src  = "http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML";
		document.getElementsByTagName("head")[0].appendChild(script);
	}
	$(function () {
		$('#sidebar li').removeClass('active open');
		$('#sidebar li:eq(3)').addClass('active open');
	});
	$(function () {
		$('#program li').removeClass('active');
		$('#program li:eq(2)').addClass('active');
	});
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
