<?php include 'Access-API-sup-sme.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<!-- <title>AIETS - Test Engine</title> -->
	<?php include 'html/general/headtags.php' ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square">
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php' ?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php' ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Test Package
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="test-program.php">Test Package</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-file-text-o"></i> Test Package
							</div>
						</div>
						<div class="portlet-body">

							<div class="row">
								<div class="col-md-6 form-group">
									<a href="#test-program-filter-modal" data-toggle="modal" class="btn btn-circle btn-icon-only blue mrg5 tooltips" data-container="body" data-placement="top" data-original-title="Filter">
										<i class="fa fa-filter"></i>
									</a>
									<a class="btn btn-circle btn-icon-only green mrg5 tooltips" onclick="resetForm();" data-container="body" data-placement="top" data-original-title="Clear Filter">
										<i class="fa fa-refresh"></i>
									</a>
									<a href="add-test-program.php" class="btn green btn-icon-only btn-circle mrg5 tooltips" data-container="body" data-placement="top" data-original-title="Add Package">
										<i class="fa fa-plus-circle"></i>
									</a>
								</div>
								<div class="col-md-6 form-group">
									<ul class="question-table-pagination pagination pull-right"></ul>
								</div>
							</div>

							<div class="margin-top-10 margin-bottom-10 table-scrollable">
								<table id="test-programs-table" class="table table-striped table-bordered table-hover">
								<thead>
								<tr>
									<th> Package Name </th>
									<th> Class </th>
									<th> Price (CAD) </th>
									<th> Test Paper </th>
									<th> Credits </th>
									<th> View </th>
									<th style="width: 82px;"> Action </th>
								</tr>
								</thead>
								<tbody></tbody>
								</table>
							</div>
							<div class="row">
								<div class="col-md-12">
									<ul class="question-table-pagination pagination pull-right"></ul>
								</div>
							</div>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>


			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php' ?>
<!-- END FOOTER -->
<!-- START Test Program FILTER MODAL -->
<div id="test-program-filter-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title control-label"><strong>Test Package Filter </strong></h4>
			</div>
			<form id="filter-form" role="form">
				<div class="form-body">
					<div class="modal-body">
						<div class="portlet-body form">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Class</label>
										<select id="classes" class="form-control">
											<option value="0">Select Class</option>
										</select>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Select Subject</label>
										<select id="subjects" class="form-control" data-tags="true" multiple="multiple"></select>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Package status</label>
										<select id="test-program-status" class="form-control">
											<option value="2"> Select Status </option>
											<option value="0"> Not For Sale </option>
											<option value="1"> For Sale </option>
										</select>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
									<label class="control-label">Package Name</label>
									<input type="text" class="form-control" id="text" placeholder="Package Name">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn green"><i class="fa fa-filter"></i>Filter</button>
						<button class="btn default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle"></i> &nbsp; Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- END Test Program FILTER MODAL -->
	<!-- Start generate coupon modal -->
	<div class="modal fade" id="generate-coupon-modal" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog">

			<form id="create-coupons-form" enctype="multipart/form-data">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Generate Coupon</h4>
					</div>
					<div class="modal-body">
						 <div class="row">
						 	<div class="col-md-12">
						 		<h4>Institute Name: <span class="institute-name"></span></h4>
						 		<h4>Test Program Name: <span id="test-program-name"></span></h4>
						 	</div>
						 	<div class="col-md-6 mar-top20">
						 		<div class="form-group">
									<label class="control-label">No. of Coupons</label>
									<div class="row">
										<div class="col-md-12">
											<input id="number-of-coupons" type="number" min="1" max="1000" class="form-control" placeholder="No. of Coupons">
										</div>
									</div>
								</div>
					 		</div>
						 	<div class="col-md-6 mar-top20">
						 		<div class="form-group">
									<label class="control-label">Prefix if any</label>
									<div class="row">
										<div class="col-md-12">
											<input id="coupon-prefix" type="text" class="form-control" placeholder="Prefix">
										</div>
									</div>
								</div>
					 		</div>
					 		<div class="col-md-6 mar-top20">
						 		<div class="form-group">
									<label class="control-label">Start Date</label>
									<div class="row">
										<div class="col-md-12">
											<input id="start-date" type="text" class="form-control" placeholder="Start Date">
										</div>
									</div>
								</div>
					 		</div>
					 		<div class="col-md-6 mar-top20">
						 		<div class="form-group">
									<label class="control-label">End Date</label>
									<div class="row">
										<div class="col-md-12">
											<input id="end-date" type="text" class="form-control" placeholder="End Date">
										</div>
									</div>
								</div>
					 		</div>
						</div>
					</div>
					<div class="modal-footer mar-top45">
						<button type="submit" class="btn blue"> <i class="fa fa-download"></i> Download as text</button>
						<button type="button" class="btn default" data-dismiss="modal"> Close</button>
					</div>
				</div>
			</form>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- end generate coupon modal -->

	<!-- Start generate coupon modal -->
	<div class="modal fade" id="generate-enroll-modal" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog">

			<form id="create-enroll-form" enctype="multipart/form-data">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Generate Enroll</h4>
					</div>
					<div class="modal-body">
						 <div class="row">
						 	<div class="col-md-12">
						 		<h4>Test Program Name: <span id="test-enroll-name"></span></h4>
						 	</div>

						 	<div class="col-md-6 mar-top20">
						 		<div class="form-group">
									<label class="control-label">Enrollment ID</label>
									<div class="row">
										<div class="col-md-12">
											<input id="enroll-code" type="text" class="form-control" placeholder="ID">
										</div>
									</div>
								</div>
					 		</div>
						</div>
					</div>
					<div class="modal-footer mar-top45">
						<button type="submit" class="btn blue"> Create</button>
						<button type="button" class="btn default" data-dismiss="modal"> Close</button>
					</div>
				</div>
			</form>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- end generate coupon modal -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-markdown/lib/markdown.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<script src="js/custom/test-program.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features
  /* Index.init();
   Index.initDashboardDaterange();
   Index.initJQVMAP(); // init index page's custom scripts
   Index.initCalendar(); // init index page's custom scripts
   Index.initCharts(); // init index page's custom scripts
   Index.initChat();
   Index.initMiniCharts();
   Tasks.initDashboardWidget();*/
});
</script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(3)').addClass('active open');
  });
  $(function () {
	  $('#program li').removeClass('active');
	  $('#program li:eq(0)').addClass('active');
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>