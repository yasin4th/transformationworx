<?php include 'Access-API-sup-sme.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<!-- <title>AIETS - Test Engine</title> -->
	<?php include 'html/general/headtags.php'; ?>
<!--  Begin Style  -->
<style>
	.required {
		color: #e02222;
		font-size: 12px;
	}
</style>
<!--  End Style  -->

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="page-header-fixed page-quick-sidebar-over-content page-style-square"> 
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php';?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php';?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Test Paper
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="test_paper.php">Test Paper</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-file-text-o"></i> Test Paper
							</div>
						</div>
						<div class="portlet-body">
							<div class="form-body">
								
								<div class="row">

									<div class="col-md-12">
										<a href="#test-papers-filter-modal" data-toggle="modal" class="btn btn-circle btn-icon-only blue mrg5 tooltips" data-container="body" data-placement="top" data-original-title="Filter">
											<i class="fa fa-filter"></i>
										</a>
										<a class="btn btn-circle btn-icon-only green mrg5 tooltips" onclick="resetForm();" data-container="body" data-placement="top" data-original-title="Clear Filter">
											<i class="fa fa-refresh"></i>
										</a>
										<a href="add-test-paper.php" type="button" class="btn green btn-icon-only blue mrg5 btn-circle" data-container="body" data-placement="top" data-original-title="Add Test Paper"><i class="fa fa-plus-circle"></i></a>
										<!-- <a id="add-test-package-modal" class="btn green mrg-left5"><i class="fa fa-plus-circle"></i> &nbsp; Add Test Package </a> -->
									</div>
									<div class="col-md-12">
										<div class="margin-top-10 margin-bottom-10 table-scrollable">
											<table id="test-paper-table" class="table tb-questionbank table-bordered table-striped table-hover">
											<thead>
											<tr>
											    <th>
													<div class="tooltips" data-container="body" data-placement="top" data-original-title="Select All" aria-describedby="tooltip735432">
												      <input type="checkbox" id="select-all">
												    </div>
												</th>
												<th>
													<div style="padding:5px;"> Name </div>
												</th>
												<th>
													<div style="padding:5px;"> Type </div>
												</th>
												<th>
													<div style="padding:5px;"> Questions </div>
												</th>
												<th>
													<div style="padding:5px;"> View Question </div>
												</th>
												<th>
													<div style="padding:5px;"> View Details </div>
												</th>
												<th style="width: 82px;">
													<div style="padding:5px;"> Action </div>
												</th>
											</tr>
											</thead>
											<tbody></tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>
			
			
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php';?>
<!-- END FOOTER -->

<!-- Modal -->

	<div id="test-paper-details" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title control-label"><strong>Test Paper</strong></h4>
				</div>
				<div class="modal-body">
					<div class="portlet-body form">
						<form class="form-horizontal" role="form">
							<div class="form-body">
								<div class="form-group">
									<label class="col-md-3 control-label">Name</label>
									<div class="col-md-9">
										<input id="test-paper-name" type="text" class="form-control" placeholder="Test Paper">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Type</label>
									<div class="col-md-9">
										<select id="test-paper-type" class="form-control">
											<option value="0">Select Type</option>

										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Class:</label>
									<div class="col-md-9">
										<select id="test-paper-class" class="form-control">
											<option value="0">Select Class</option>
										</select>
										<input id="test-paper-id" type="hidden" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Subject:</label>
									<div class="col-md-9">
										<select id="test-paper-subjects" multiple="multiple" class="form-control">
											<option value="0">Select Subject</option>
										</select>
									</div>
								</div>

								<!-- start -->
								<div id="test-time" class="row">
									<div class="col-md-6"> 
										<div class="form-group">
											<div class="col-md-12">
												<label class="control-label">Start Date / Time</label>
											</div>
											<div class="col-md-12">
												<div class="input-group date">
													<input id="test-paper-start" type="text" class="form-control" placeholder="Start Date/Time">
								                    <span class="input-group-addon">
								                        <span class="glyphicon glyphicon-calendar"></span>
								                    </span>
								                </div>
							                </div>
										</div>
									</div>
									<div class="col-md-6"> 
										<div class="form-group">
											<div class="col-md-12">
												<label class="control-label">End Date / Time</label>
											</div>
											<div class="col-md-12">
												<div class="input-group date">
													<input id="test-paper-end" type="text" class="form-control" placeholder="End Date/Time">
													<span class="input-group-addon">
								                        <span class="glyphicon glyphicon-calendar"></span>
								                    </span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- end -->


								<div id="marks-time-div">
									
									<div class="form-group">
										<label class="col-md-3 control-label">Time(In Minutes):</label>
										<div class="col-md-9">
											<input id="test-paper-time" type="number" min="0" class="form-control" placeholder="Time ( In Minutes )" value="0">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Total Attempts</label>
										<div class="col-md-9">
											<input id="test-paper-attempts" type="number" class="form-control" placeholder="No of Attempts">
										</div>
									</div>									
									
									<div class="form-group">
										<label class="col-md-3 control-label">Instructions</label>
										<div class="col-md-9">
										
										<div id="test-paper-instructions" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); min-height: 70px; position: relative;" data-ng-model="myModel"><p></p></div>
										
										</div>
									</div>
								</div>
									
							</div>
						</form>
					</div>
				</div>
				<div class="modal-footer">
					<button id="save-test-paper" class="btn green pull-left"><i class="fa fa-save"></i> &nbsp; Save</button>
					<button class="btn default pull-left" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle"></i> &nbsp; Cancel</button>
				</div>
			</div>
		</div>
	</div>
<!-- /.modal -->
<!-- modal filter -->
<div class="modal fade" id="test-papers-filter-modal" tabindex="-1" role="basic" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Test Paper Filter</h4>
			</div>
			<form id="filter-form" role="form">	
				<div class="form-body">
					<div class="modal-body">
						 <div class="form-group">
							<div class="radio-list">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">Class</label>
											<select id="classes" class="form-control">
												<option value="0">Select Class</option>
											</select>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">Select Subject</label>
											<select id="subjects" class="form-control" data-tags="true" multiple="multiple"></select>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">Test-paper Type</label>
											<select id="paper-type" class="form-control pull-right"></select>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">Test Paper</label>
											<input type="text" class="form-control" id="test-paper" placeholder="Test Paper">													
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn green"><i class="fa fa-filter"></i> Filter</button>
						<button class="btn default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle"></i> &nbsp; Cancel</button>
					</div>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal filter -->
<!-- add-test-package-model-->
<div class="modal fade" id="addtestpackagemodal" tabindex="-1" role="basic" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add Test Package</h4>
			</div>
			<form id="filter-form" role="form">	
				<div class="form-body">
					<div class="modal-body">
						 <div class="form-group">
							<div class="radio-list">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label">Test Package</label>

											<select id="testpackage" class="form-control"  multiple="multiple" data-tags="true">
												
											</select>
										</div>
									</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn green" id="addTestPackage"><i class="fa fa-save"></i> Save</button>
						<button class="btn default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle"></i> &nbsp; Cancel</button>
					</div>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.add-test-package-model-->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>

<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
<!-- <script src="assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/moment.min.js" type="text/javascript"></script> -->
<script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-markdown/lib/markdown.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<script src="js/custom/test_paper.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features 
   /*Index.init();   
   Index.initDashboardDaterange();
   Index.initJQVMAP(); // init index page's custom scripts
   Index.initCalendar(); // init index page's custom scripts
   Index.initCharts(); // init index page's custom scripts
   Index.initChat();
   Index.initMiniCharts();
   Tasks.initDashboardWidget();*/
});
</script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(3)').addClass('active open');
  });
  $(function () {
	  $('#program li').removeClass('active');
	  $('#program li:eq(1)').addClass('active');
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>