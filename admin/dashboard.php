<?php include 'Access-API-sup.php'; ?>
<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<!-- <title>AIETS - Test Engine</title> -->
	<?php include 'html/general/headtags.php' ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square"> 
<!-- BEGIN HEADER -->
	<?php include 'html/general/header.php' ?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php' ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                     <h4 class="modal-title">Modal title</h4>
                  </div>
                  <div class="modal-body">
                      Widget settings form goes here
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn blue">Save changes</button>
                     <button type="button" class="btn default" data-dismiss="modal">Close</button>
                  </div>
               </div>
               <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
         </div>
         <!-- /.modal -->
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN PAGE HEADER-->
         <h3 class="page-title">
         Dashboard
         </h3>
         <div class="page-bar">
            <ul class="page-breadcrumb">
               <li>
                  <i class="fa fa-home"></i>
                  <a href="dashboard.php">Dashboard</a>
               </li>
            </ul>
         </div>
         <!-- END PAGE HEADER-->
         <!-- BEGIN DASHBOARD STATS -->
         <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
               <div class="dashboard-stat blue-madison">
                  <div class="visual">

                     <i class="fa fa-user"></i>
                  </div>
                  <div class="more">
                     <a class="" href="manage-student.php">
                     View more
                     </a>
                     <!-- <span class="pull-right">961364</span> -->
                  </div>
               </div>
               <div class="overlay-dashboard-tiles">
                  <h3 class=""> Students</h3>
                  <h3 class="count" id="student-count">0<!-- 961364 --></h3>
                  <!-- <p class="">Active = <span id="active">0</span></p>
                  <p class="">Inactive = <span id="inactive">0</span></p> -->
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
               <div class="dashboard-stat red-intense">
                  <div class="visual">
                     <i class="fa fa-file-text-o"></i>
                  </div>
                  <div class="more">
                     <a class="" href="question-bank.php">
                     View more
                     </a>
                     <!-- <span class="pull-right">961364</span> -->
                  </div>
               </div>
               <div class="overlay-dashboard-tiles">
                  <h3 class="">Total Questions</h3>
                  <h3 class="count" id="question-count">0<!-- 961364 --></h3>
                  <!-- <p class="">Attempted Questions = <span>5611/961364</span></p> -->
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
               <div class="dashboard-stat green-haze">
                  <div class="visual">
                     <i class="fa fa-file-text-o"></i>
                  </div>
                  <div class="more">
                     <a class="" href="test_paper.php">
                     View more
                     </a>
                     <!-- <span class="pull-right">961364</span> -->
                  </div>
               </div>
               <div class="overlay-dashboard-tiles">
                  <h3 class="">Total Revenue</h3>
                  <h3 class="count" id="revenue-count">0</h3>
                  <!-- <p class="">Total Test Paper = <span> 5611 </span></p> -->
                  <!-- <p class="">Total Test Program = <span>5611</span></p> -->
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
               <div class="dashboard-stat purple-plum">
                  <div class="visual">
                     <i class="fa fa-shopping-cart"></i>
                  </div>
                  <div class="more">
                     <a class="" href="manage-order.php">
                     View more
                     </a>
                     <!-- <span class="pull-right">961364</span> -->
                  </div>
               </div>
               <div class="overlay-dashboard-tiles">
                  <h3 class="">Total Orders</h3>
                  <h3 class="count" id="order-count">0</h3>
               </div>
            </div>
         </div>
         <!-- END DASHBOARD STATS -->
         <div class="clearfix">
         </div>
         <div class="row">
            <div class="col-md-6">
               <!-- <div class="table-scrollable">
                  <table class="table table-bordered table-hover">
                  <thead>
                  <tr>
                     <th>
                        First Name
                     </th>
                     <th>
                        Last Name
                     </th>
                     <th>
                        Username
                     </th>
                     <th>
                         
                     </th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                     <td>
                        Mark
                     </td>
                     <td>
                        Otto
                     </td>
                     <td>
                        makr124
                     </td>
                     <td>
                        <a href="#" class="btn btn-default btn-xs"> <i class="fa fa-search"></i> View </a>
                     </td>
                  </tr>
                  
                  <tr>
                     <td>
                        Sandy
                     </td>
                     <td>
                        Lim
                     </td>
                     <td>
                        sanlim
                     </td>
                     <td>
                        <a href="#" class="btn btn-default btn-xs"> <i class="fa fa-search"></i> View </a>
                     </td>
                  </tr>
                  </tbody>
                  </table>
               </div> -->

               <div class="portlet box grey-cascade">
                  <div class="portlet-title">
                     <div class="caption">
                        <i class="fa fa-share-alt"></i> Overview <!-- <span class="caption-helper">report overview...</span> -->
                     </div>
                  </div>
                  <div class="portlet-body">
                     <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                           <li class="active">
                              <a href="#overview_1" data-toggle="tab">
                              Top Selling </a>
                           </li>
                           <li>
                              <a href="#overview_3" data-toggle="tab">
                              New Students </a>
                           </li>
                           <li>
                              <a href="#overview_4" class="dropdown-toggle" data-toggle="tab">
                              Orders
                              </a>                              
                           </li>
                        </ul>
                        <div class="tab-content">
                           <div class="tab-pane active" id="overview_1">
                              <div class="table-responsive">
                                 <table class="table table-striped table-hover table-bordered" id="top-selling">
                                 <thead>
                                 <tr>
                                    <th>
                                        Package Name
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                    <th>
                                        Sold
                                    </th>
                                    <th>
                                    </th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                 <!-- <tr>
                                    <td>
                                       <a href="#">
                                       SSC CGL 2016 </a>
                                    </td>
                                    <td>
                                        795 RS
                                    </td>
                                    <td>
                                        635 Rs
                                    </td>
                                    <td>
                                       <a href="#" class="btn btn-default btn-xs"> <i class="fa fa-search"></i> View </a>
                                    </td>
                                 </tr> -->
                                 </tbody>
                                 </table>
                              </div>
                           </div>
                           <div class="tab-pane" id="overview_2">
                              <div class="table-responsive">
                                 <table class="table table-striped table-hover table-bordered" id="top-viewed">
                                 <thead>
                                 <tr>
                                    <th>
                                        Package Name
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                    <th>
                                        Sold
                                    </th>
                                    <th>
                                    </th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                 
                                 </tbody>
                                 </table>
                              </div>
                           </div>
                           <div class="tab-pane" id="overview_3">
                              <div class="table-responsive">
                                 <table class="table table-striped table-hover table-bordered" id="new-students">
                                 <thead>
                                 <tr>
                                    <th>
                                       Id
                                    </th>
                                    <th>
                                        Students Name
                                    </th>
                                    <th>
                                        Registration Date
                                    </th>
                                    <th>
                                    </th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                 
                                 </tbody>
                                 </table>
                              </div>
                           </div>
                           <div class="tab-pane" id="overview_4">
                              <div class="table-responsive">
                                 <table class="table table-striped table-hover table-bordered" id="latest-orders">
                                 <thead>
                                 <tr>
                                    <th>
                                       Order No
                                    </th>
                                    <th>
                                       Date
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                    <!-- <th>
                                        Sold
                                    </th> -->
                                    <th>
                                    </th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                 
                                 </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="portlet box grey-cascade">
                  <div class="portlet-title">
                     <div class="caption">
                        Revenue <span class="caption-helper">Monthly Status...</span>
                     </div>
                     <div class="actions">
                        <div class="btn-group pull-right">
                           <a href="" class="btn grey-steel btn-circle btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                           Filter <span class="fa fa-angle-down">
                           </span>
                           </a>
                           <ul class="dropdown-menu pull-right">
                              <li>
                                 <a href="javascript:days=0;fetchReports();">
                                 Monthly 
                                 </a>
                              </li>
                              <li>
                                 <a href="javascript:days=7;fetchReports();">
                                 Last 7 days 
                                 </a>
                              </li>
                              <li class="active">
                                 <a href="javascript:days=15;fetchReports();">
                                 Last 15 days 
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="portlet-body">
                     <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                           <li class="active pull-right">
                              <a href="#revenue_1" data-toggle="tab">
                              Order </a>
                           </li>
                           <li class="pull-right">
                              <a href="#revenue_2" data-toggle="tab">
                              Amount </a>
                           </li>
                        </ul>

                        <div class="tab-content">
                           <div class="tab-pane active" id="revenue_1" >
                              <div id="chart1" style="width: 410px; height: 340px; margin: 0 auto">                                 
                              </div>

                           </div>
                           <div class="tab-pane" id="revenue_2" >
                           
                              <div id="chart2" style="width: 410px; height: 340px; margin: 0 auto">                                
                              </div>

                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <!-- BEGIN PORTLET-->
              
               <!-- END PORTLET-->
            </div>
         </div>
      </div>
   </div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php' ?>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>

<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<script src="assets/global/plugins/highcharts/highcharts.js"></script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/ecommerce-index.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<script src="js/custom/dashboard.js" type="text/javascript"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>
days = 0;
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features
});
</script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(2)').addClass('active open');
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>