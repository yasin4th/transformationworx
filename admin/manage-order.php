<?php include 'Access-API-sup.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>

	<?php include 'html/general/headtags.php' ?>
	<!-- <script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script> -->
	<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/ media="all">

	<!-- https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square"> 
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php' ?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php' ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Manage Orders
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="manage-order.php">Manage Orders</a>
					</li>
				</ul>
			</div>

			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-question-circle"></i> Orders
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<div class="pull-left">
									Total Orders:
									<lable id="total-orders" class="">
										0
									</lable>
								</div>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form">
								<div class="form-body">
									<!-- <div class="row">
										<div class="col-md-8 col-xs-10">
											
										</div>
										<div class="col-md-4 col-xs-2">
											<div class="form-group">
												<a onclick="resetFilterForm();fetchOrders();" class="btn btn-circle btn-icon-only green pull-right" style="margin: 5px;">
													<i class="fa fa-refresh"></i>
												</a>
											
												<a id="filters" class="btn btn-circle btn-icon-only blue pull-right" style="margin: 5px;">
													<i class="fa fa-filter"></i>
												</a>
												<a href="#" class="btn btn-circle btn-icon-only green pull-right tooltips" data-container="body" data-placement="top" data-original-title="Export to excel" style="margin: 5px;"><i class="fa fa-file-excel-o"></i></a>
											</div>
										</div>
									</div> -->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<a id="filters" class="btn btn-circle btn-icon-only blue mrg5 tooltips" data-container="body" data-placement="top" data-original-title="Filter">
													<i class="fa fa-filter"></i>
												</a>
												<a onclick="resetFilterForm();fetchOrders();" class="btn btn-circle btn-icon-only green mrg5 tooltips" data-container="body" data-placement="top" data-original-title="Clear Filter">
													<i class="fa fa-refresh"></i>
												</a>
												<a href="#" class="btn btn-circle btn-icon-only green mrg5 tooltips" onclick="tableToExcel('orders-table', 'W3C Example Table')" data-container="body" data-placement="top" data-original-title="Export to excel">
													<i class="fa fa-file-excel-o"></i>
												</a>
											</div>
										</div>
										<!-- <div class="col-md-6">
											<ul class="question-table-pagination pagination pull-right"></ul>
										</div> -->
									</div>
									<div class="">
										<table id="orders-table" class="table table-striped table-hover table-bordered">
										<thead>
										<tr>
											<!-- <th>
												<div class="tooltips" data-container="body" data-placement="top" data-original-title="Select All" aria-describedby="tooltip735432">
												 <input type="checkbox" id="select-all">
												</div>
											</th> -->
											<th>
												Order No
											</th>
											<th>
												Purchased On
											</th>
											<th>
												Student Name
											</th>
											<th>
												Package Name
											</th>
											<th>
												Discount Coupon
											</th>
											<th>
												List Price
											</th>
											<th>
												Purchased Price
											</th>
											<th>
												Status
											</th>
																			
										</tr>
										</thead>
										<tbody></tbody>
										<tfoot>
										</tfoot>
										</table>
									</div>
									<!-- <div class="row">
										<div class="col-md-12">
											<ul class="question-table-pagination pagination pull-right"></ul>
										</div>
									</div> -->
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php' ?>
<!-- END FOOTER -->
<!-- .modal -->
	<div class="modal fade bs-modal-lg" id="order-filter-modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="filter-form" role="form">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Order Filter</h4>
				</div>
				<div class="modal-body">
				
					<div class="row">
						<div class="col-md-12">
							<!-- <button onclick="resetFilterForm()" class="btn btn-default pull-right" > Reset </button> -->
							<label>Please select the criteria to filter Order.</label>
						</div>
					</div><br>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">Order Number</label>
								<div class="row">
									<div class="col-md-12">
										<input type="text" class="form-control" id="order-id">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">Purchased On</label>
								<div class="row">
									<div class="col-md-12">
										<div class="input-group" data-date="01/03/2016" data-date-format="dd/mm/yyyy">
						                    <input type="text" class="form-control dpd1" id="dpd1" name="from">
						                    <span class="input-group-addon">To</span>
						                    <input type="text" class="form-control dpd2" id="dpd2" name="to"> 
					                  	</div>
									</div>
								</div>
							</div>
						</div>	
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">Student Name</label>
								<div class="row">
									<div class="col-md-12">
										<input type="text" id="name" class="form-control" placeholder="Student Name">
									</div>
								</div>
							</div>
						</div>				
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">Package Name</label>
								<div class="row">
									<div class="col-md-12">
										<input type="text" id="program-name" class="form-control" placeholder="Package Name">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Purchased Price Range</label>
								<div class="row">
									<div class="col-md-12">
										
										<div class="input-group">
											<input type="text" id="price-from" class="form-control" placeholder="From">
											<span class="input-group-addon">To</span>
											<input type="text" id="price-to" class="form-control" placeholder="To">
										</div>
									</div>
								</div>
								
							</div>

						</div>

					</div>
					
					
				</div>
				<div class="modal-footer">
					<button id="filter-orders" type="submit" class="btn blue">Filter</button>
					<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
				</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>

<!-- /.modal -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->


<script src="assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script type="text/javascript" src="js/common.js"></script>
<script src="js/custom/manage-order.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features  
});
</script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(19)').addClass('active');
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>