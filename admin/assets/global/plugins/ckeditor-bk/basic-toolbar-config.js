CKEDITOR.editorConfig = function( config ) {
	config.skin = 'kama';
     config.extraPlugins = "confighelper";
    // config.width = 700;
    config.height = 700;
	config.placeholder = ' ';
	config.toolbar = [{
            name: "document",
            groups: ["document"],
            items: ["NewPage"]
        }, {
            name: "clipboard",
            groups: ["clipboard", "undo"],
            items: ["Cut", "Copy", "Paste", "PasteText", "PasteFromWord", "-", "Undo", "Redo"]
        }, {
            name: "editing",
            groups: ["find", "selection"],
            items: ["Find", "Replace", "-", "SelectAll"]
        },{
            name: "colors", 
            items : ['TextColor','BGColor'] }, "/",
            {
            name: "basicstyles",
            groups: ["basicstyles"],
            items: ["Bold", "Italic", "Underline", "Strike", "Subscript", "Superscript"]
        }, {
            name: "paragraph",
            groups: ["list", "align"],
            items: ["NumberedList", "BulletedList", "-", "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock"]
        }];
};