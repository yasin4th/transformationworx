/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

'use strict';

CKEDITOR.dialog.add( 'mathjax', function( editor ) {

	var equationArea, equationSource, lang = editor.lang.mathjax, uniqueNo = new Date().getTime();	
	return {
		title: lang.title,
		minWidth: 552,
		minHeight: 300,
		contents: [
			{
				id: 'info',
				elements: [
					{
						id: 'equationArea',
						type: 'html',
						html: ' <div class="etitBtnOver">'+
						      '    <div class="editInpBtnRt">'+
						      '       <input class="editGrayBtn" type="button" id="sourceBtn'+uniqueNo+'" value="Latex Source" status="editor"/>' +
						      '       <input class="editGrayBtn" type="button" id="editorBtn'+uniqueNo+'" style="display:none;" value="Editor" status="editor"/>' +
						      '     </div>' +
						      '  </div>'+
						      '<div id="eqnEditorErrorSpan'+uniqueNo+'" style="display:none;" class="mathEditorErrorMsg" >Latex source couldn\'t render inside formula editor.</div>'+
						      '  <div id="mathQuillSource'+uniqueNo+'" style="display:none;"><textarea class="sourceTextarea" id="mathQuillSourceInput'+uniqueNo+'"></textarea></div>' +
							  '  <span id="mathQuill'+uniqueNo+'" class="mathEqualarea mathquill-editable" autofocus></span>' +							  			  
							  '',						
						onLoad: function( widget ) {
							var that = this;							
							equationArea = $('#mathQuill'+uniqueNo).mathquill('editable');							
							equationSource = $('#mathQuillSourceInput'+uniqueNo);
							
							// display source
							$("#sourceBtn"+uniqueNo).on('click', function() {								
								// set Latex code into text area
								
								var latexText = equationArea.mathquill('latex');
								if(latexText != ""){
									$("#mathQuillSourceInput"+uniqueNo).val(latexText);
								}
								equationArea.mathquill('latex', '\\(\\)' );
								$("#sourceBtn"+uniqueNo).hide();
								$("#editorBtn"+uniqueNo).show();
								$("#mathQuillSource"+uniqueNo).show();
								$("#mathQuill"+uniqueNo).hide();
								$("#editorButtons"+uniqueNo).hide();
							});															
							
							//Display Wyswig 
							$("#editorBtn"+uniqueNo).on('click', function() {
								
								//Check if latex can be displayed
								
								var validLatexForEditor = false;
								if(equationSource.val()!=""){
									equationArea.mathquill('latex', equationSource.val() );
									if(equationArea.mathquill('latex')!=''){										
										validLatexForEditor = true;
									}
								}else {
									validLatexForEditor = true;
								}
								
								if(validLatexForEditor) {
									$('#eqnEditorErrorSpan'+uniqueNo).hide();
									$("#sourceBtn"+uniqueNo).show();
									$("#editorBtn"+uniqueNo).hide();
									$("#mathQuillSource"+uniqueNo).hide();
									$("#mathQuill"+uniqueNo).show();
									$("#editorButtons"+uniqueNo).show();
									
									if(equationSource.val()!=""){
										equationArea.mathquill('latex', equationSource.val() );	
									} 
								} else {
									
									$("#sourceBtn"+uniqueNo).trigger("click");
									$('#eqnEditorErrorSpan'+uniqueNo).show();
								}
							});
						},
						onHide: function(widget) {
							equationSource.val("");
							$("#editorBtn"+uniqueNo).trigger("click");
						},
						setup: function( widget ) {
							var strLatex = CKEDITOR.plugins.mathjax.trim(widget.data.math);
							
							if(strLatex !== '') {								
								equationArea.mathquill('latex', strLatex);								
								if(equationArea.mathquill('latex') === '') { //latex can't be displayed in editor								
									equationSource.val(strLatex);
									$("#sourceBtn"+uniqueNo).trigger("click");
								} 
							}
							else {
								equationArea.mathquill('latex', '\\(\\)' );								
							}
							
							setTimeout( function() {
								equationArea.find("textarea").focus();
							}, 100);
						},
					},
					{
						id: 'scriptArea',
						type: 'html',
						html :'<script type="javascript">'+
							  ' function showMathQuillSource(idUniqueNumber) {'+
							  '$("")' +
							  '}' +
							  '</script>'
					},
					{
						id: 'inputBox',
						type: 'html',
						html: ' <div class="editBoxBdr" id="editorButtons'+uniqueNo+'"> '+
							  ' <ul class="nav nav-tabs" role="tablist">'+
						  	  '   <li class="active" title="Common Mathematical Expression"><a href="#default'+uniqueNo+'" role="tab" data-toggle="tab"><span class="rootxIcon"></span></a></li>' +
						      '   <li title="All Operators and Relations"><a href="#operators'+uniqueNo+'" role="tab" data-toggle="tab"><span class="semiEuqalIcon"></span></a></li>' +
						      '   <li title="All Letters and Symbols"><a href="#letters'+uniqueNo+'" role="tab" data-toggle="tab"><span class="betaIcon"></span></a></a></li>' + 
						      '   <li title="Miscellaneous"><a href="#misc'+uniqueNo+'" role="tab" data-toggle="tab"><span class="sumIcon"></span></a></a></li>' +
						      '   <li title="Keypad"><a href="#keypad'+uniqueNo+'" role="tab" data-toggle="tab"><span class="keypadIcon"></span></a></li>' +						      
						      ' </ul>'+
						      '<!-- Tab panes --> '+
						      '<div class="editValMain tab-content">'+
						      '  <div class="tab-pane active" id="default'+uniqueNo+'"> <!-- 1st Tab -->'+						      
						      '       <div class="calcValLft" id="'+uniqueNo+'pad11"><table class="table editorTbl"> <!-- 1.1 Symbol Tab -->'+
						      '		  	<tr>'+
						      '        		<td> <span action-value="=" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="Euqal">=</span></td>'+
						      '        		<td> <span action-value="\\pm" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="Plus Minus">\\pm</span></td>'+						      
						      '        		<td> <span action-value="lt" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Less than">\\lt</span></td>'+
						      '        		<td> <span action-value="gt" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Greater than">\\gt</span></td>'+
						      '        		<td> <span action-value="(" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Small Bracket">()</span></td>'+
						      '        		<td> <span action-value="x^2" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="x square">x^{2}</span></td>'+
							  '			</tr>'+						      
						      '			<tr>'+
						      '        		<td><span action-value="{}^{}" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="SuperScript">a^{n}</span></td>'+
						      '        		<td><span action-value="_" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Subscript">a_{n}</span></td>'+
						      '        		<td> <span action-value="|" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Absolute">|a|</span></td> '+
						      '        		<td> <span action-value="/" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Fraction">\\frac{a}{b}</span></td>'+
						      '        		<td><span action-value="/" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Mixed fraction">n\\frac{a}{b}</span></td>'+
						      '        		<td><span action-value="{}:{}" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="Ratio Expression">a:b</span></td>'+
						      '			</tr>'+						      
						      '			<tr>'+
						      '        		<td><span action-value=":" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="Ratio">:</span></td>'+
						      '        		<td><span action-value="%" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="Percentage">%</span></td>'+						      
						      '        		<td><span action-value="\\infin" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="Infinity">\\infin</span></td>'+
						      '        		<td><span action-value="sqrt" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Square Root">\\sqrt{a}</span></td>'+
						      '        		<td><span action-value="\\sqrt[n]{}" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="Complex Square Root">\\sqrt[n]{a}</span></td>'+
						      '        		<td><span action-value="sin" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Sin">\\sin</span></td>'+
						      '			</tr>'+						      
						      '			<tr>'+
						      '        		<td><span action-value="cos" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Cos">\\cos</span></td>'+
						      '        		<td><span action-value="sec" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Sec">\\sec</span></td>'+					
						      '        		<td><span action-value="^\\circ" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="Degree">^\\circ</span></td>'+
						      '        		<td><span action-value="pi" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Pi">\\pi</span></td>'+
						      '        		<td><span action-value="angle" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Angle">\\angle</span></td>'+
						      '        		<td><span action-value="\\bigtriangleup" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="Triangle">\\bigtriangleup</span></td>'+
						      '			</tr>'+					      
						      '			<tr>'+
						      '        		<td><span action-value="int" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Integral">\\int</span></td>'+
						      '        		<td><span action-value="\\binom{}{}" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="Binomial">\\binom{a}{n}</span></td>'+
						      '        		<td><span action-value="\\int_0^{\\infin}" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="Integral limit 0 to infinity">\\int_0^{\\infin}</span></td>'+
						      '        		<td><span action-value="x\\mathrm{d}x" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="x dx">x\\mathrm{d}x</span></td>'+
						      '        		<td><span action-value="\\lim" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="Limit">\\lim</span></td>'+
						      '        		<td><span action-value="11" action-type="nextTabPad" class="colSpan tab-pan-navigate'+uniqueNo+'" title="Next Symbols Set"><strong>More</strong></span></td>'+	
						      '			</tr>'+					      
						      '</table></div>'+	
						      '       <div class="calcValLft" id="'+uniqueNo+'pad12" style="display:none;"><table class="table editorTbl"> <!-- 1.2 Symbol Pad Tab -->'+
						      '		  	<tr>'+
						      '        		<td> <span action-value="12" action-type="previousTabPad" class="colSpan tab-pan-navigate'+uniqueNo+'" title="Previous Symbols Set"><strong>Prev</strong></span></td>'+
						      '        		<td> <span action-value="sum" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Summation">\\sum</span></td>'+
						      '        		<td> <span action-value="prod" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Product">\\prod</span></td>'+
						      '        		<td><span action-value="coprod" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Co product">\\coprod</span></td>'+
						      '        		<td><span action-value="bigcup" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Union">\\bigcup</span></td>'+
						      '        		<td><span action-value="bigcap" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Intersect">\\bigcap</span></td>'+
							  '			</tr>'+						      
						      '			<tr>'+
						      '        		<td> <span action-value="neg" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Negation">\\neg</span></td> '+
						      '        		<td><span action-value="!" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Factorial & Logical Negation">!</span></td>'+
						      '        		<td> <span action-value="propto" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Proportionality">\\propto </span></td>'+						      
						      '        		<td><span action-value="approx" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Approximation">\\approx</span></td>'+
						      '        		<td><span action-value="ne" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Inequality">\\ne</span></td>'+
						      '        		<td><span action-value="cong" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Congruence">\\cong</span></td>'+
						      '			</tr>'+						      
						      '			<tr>'+
						      '        		<td><span action-value="therefor" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Therefor">\\therefor</span></td>'+
						      '        		<td><span action-value="because" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Because">\\because</span></td>'+
						      '        		<td><span action-value="leq" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Less than equal to">\\leq </span></td>'+
						      '        		<td><span action-value="geq" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Greater than equal to">\\geq</span></td>'+
						      '        		<td><span action-value="alpha" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Alpha">\\alpha</span></td>'+
						      '        		<td><span action-value="beta" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Beta">\\beta</span></td>'+
						      '			</tr>'+						      
						      '			<tr>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+	
						      '			</tr>'+					      
						      '			<tr>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+	
						      '			</tr>'+					      
						      '</table></div>'+		
						      '    <div class="calcValRt"><table class="table editorTbl">'+
						      '      <tr>'+
						      '        <td> <span action-value="+" action-type="write" class="colSpan colSpanRed mathquill-read'+uniqueNo+'" title="Plus">+</span></td>'+
						      '        <td>'+
						      '          <span action-value="7" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Seven">7</span>'+
						      '        </td>'+
						      '        <td> <span action-value="8" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Eight">8</span></td>'+
						      '        <td><span action-value="9" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Nine">9</span></td>'+						      
						      '      </tr>'+
						      '       <tr>'+
						      '        <td> <span action-value="-" action-type="write" class="colSpan colSpanRed mathquill-read'+uniqueNo+'" title="Minus">-</span></td>'+
						      '         <td>'+
						      '          <span action-value="4" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Four">4</span>'+
						      '        </td>'+
						      '        <td> <span action-value="5" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Five">5</span></td>'+
						      '        <td><span action-value="6" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Six">6</span></td>'+						      
						      '      </tr> '+
						      '      <tr>'+
						      '        <td>  <span action-value="×" action-type="write" class="colSpan colSpanRed mathquill-read'+uniqueNo+'" title="Multiply">×</span></td>'+ 
						      '         <td>'+
						      '          <span action-value="1" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="One">1</span>'+
						      '        </td>'+
						      '        <td> <span action-value="2" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Two">2</span></td>'+
						      '        <td><span action-value="3" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Three">3</span></td>'+						      
						      '      </tr>'+
						      '      <tr>'+
						      '        <td> <span action-value="÷" action-type="write" class="colSpan colSpanRed mathquill-read'+uniqueNo+'" title="Division">÷</span></td>'+
						      '        <td>'+
						      '          <span action-value="0" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Zero">0</span>'+
						      '        </td>'+
						      '        <td> <span action-value="38" action-type="keystroke" class="colSpan colSpanYlo keyupIcon mathquill-read'+uniqueNo+'" title="Up"></span></td>'+
						      '        <td><span action-value="." action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="dot/decimal">.</span> </td>'+						      
						      '      </tr>'+
						      '      <tr>'+
						      '        <td> <span action-value="8" action-type="keystroke" class="colSpan colSpanRed keyCrossArrow mathquill-read'+uniqueNo+'" title="Backspace"></span></td>'+
						      '        <td>'+
						      '          <span action-value="37" action-type="keystroke" class="colSpan colSpanYlo keyleftIcon mathquill-read'+uniqueNo+'" title="left"></span>'+
						      '        </td>'+
						      '        <td> <span action-value="40" action-type="keystroke" class="colSpan colSpanYlo keydownIcon mathquill-read'+uniqueNo+'" title="Down"></span></td>'+
						      '        <td><span action-value="39" action-type="keystroke" class="colSpan colSpanYlo keyrightIcon mathquill-read'+uniqueNo+'" title="Right"></span> </td>'+
						      '      </tr>    '+
						      '    </table></div>     '+
						      ''+
				  		      '    </div>'+
						      '    <div id="operators'+uniqueNo+'" class="tab-pane"> <!-- 2nd Tab -->'+						      
						      '       <div class="calcValLft" id="'+uniqueNo+'pad21"><table class="table editorTbl">'+
						      '		  	<tr>'+
						      '        		<td> <span action-value="mp" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Minus Plus">\\mp</span></td>'+
						      '        		<td> <span action-value="ast" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Asterik">\\ast</span></td>'+
						      '        		<td> <span action-value="circ" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Circle">\\circ</span></td>'+
						      '        		<td><span action-value="bigcirc" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Big circle">\\bigcirc</span></td>'+
						      '        		<td><span action-value="bullet" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Bullet">\\bullet</span></td>'+
						      '        		<td><span action-value="cdot" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Circular dot">\\cdot</span></td>'+
							  '			</tr>'+						      
						      '			<tr>'+
						      '        		<td> <span action-value="cap" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Union">\\cap</span></td>'+
						      '        		<td> <span action-value="cup" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Intersect">\\cup</span></td>'+
						      '        		<td><span action-value="uplus" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\uplus</span></td>'+
						      '        		<td><span action-value="sqcap" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\sqcap</span></td>'+
						      '        		<td><span action-value="sqcup" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\sqcup</span></td>'+
						      '        		<td><span action-value="vee" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\vee</span></td>'+
						      '			</tr>'+						      
						      '			<tr>'+
						      '        		<td><span action-value="wedge" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\wedge</span></td>'+
						      '        		<td><span action-value="setminus" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\setminus</span></td>'+
						      '        		<td><span action-value="wr" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\wr</span></td>'+
						      '        		<td><span action-value="diamond" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\diamond</span></td>'+
						      '        		<td><span action-value="bigtriangleup" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\bigtriangleup</span></td>'+
						      '        		<td><span action-value="bigtriangledown" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\bigtriangledown</span></td>'+
						      '			</tr>'+						      
						      '			<tr>'+
						      '        		<td><span action-value="triangleleft" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\triangleleft</span></td>'+
						      '        		<td><span action-value="triangleright" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\triangleright</span></td>'+
						      '        		<td><span action-value="oplus" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\oplus</span></td>'+
						      '        		<td><span action-value="ominus" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\ominus</span></td>'+
						      '        		<td><span action-value="otimes" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\otimes</span></td>'+
						      '        		<td><span action-value="oslash" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\oslash</span></td>'+	
						      '			</tr>'+					      
						      '			<tr>'+
						      '        		<td><span action-value="odot" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\odot</span></td>'+
						      '        		<td><span action-value="dagger" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\dagger</span></td>'+
						      '        		<td><span action-value="ddagger" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\ddagger</span></td>'+
						      '        		<td><span action-value="amalg" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\amalg</span></td>'+
						      '        		<td><span action-value="|" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="">|</span></td>'+
						      '        		<td><span action-value="21" action-type="nextTabPad" class="colSpan tab-pan-navigate'+uniqueNo+'" title="Next Symbols Set"><strong>More</strong></span></td>'+	
						      '			</tr>'+					      
						      '</table></div>'+
						      '       <div class="calcValLft" id="'+uniqueNo+'pad22" style="display:none;"><table class="table editorTbl">'+
						      '		  	<tr>'+
						      '        		<td> <span action-value="22" action-type="previousTabPad" class="colSpan tab-pan-navigate'+uniqueNo+'" title="Previous Symbols Set"><strong>Prev</strong></span></td>'+
						      '        		<td> <span action-value="leq" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\leq</span></td>'+
						      '        		<td> <span action-value="geq" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\geq</span></td>'+
						      '        		<td><span action-value="equiv" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\equiv</span></td>'+
						      '        		<td><span action-value="models" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\models</span></td>'+
						      '        		<td><span action-value="prec" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\prec</span></td>'+
							  '			</tr>'+						      
						      '			<tr>'+
						      '        		<td> <span action-value="succ" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\succ</span></td>'+
						      '        		<td> <span action-value="sim" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\sim</span></td>'+
						      '        		<td><span action-value="perp" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\perp</span></td>'+
						      '        		<td><span action-value="preceq" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\preceq</span></td>'+
						      '        		<td><span action-value="succeq" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\succeq</span></td>'+
						      '        		<td><span action-value="simeq" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\mid</span></td>'+
						      '			</tr>'+						      
						      '			<tr>'+
						      '        		<td><span action-value="ll" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\ll</span></td>'+
						      '        		<td><span action-value="gg" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\gg</span></td>'+
						      '        		<td><span action-value="asymp" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\asymp</span></td>'+
						      '        		<td><span action-value="parallel" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\parallel</span></td>'+
						      '        		<td><span action-value="subset" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\subset</span></td>'+
						      '        		<td><span action-value="supset" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\supset</span></td>'+
						      '			</tr>'+						      
						      '			<tr>'+
						      '        		<td><span action-value="bowtie" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\bowtie</span></td>'+
						      '        		<td><span action-value="subseteq" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\subseteq</span></td>'+
						      '        		<td><span action-value="superseteq" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\superseteq</span></td>'+
						      '        		<td><span action-value="cong" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\cong</span></td>'+
						      '        		<td><span action-value="neg" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\neg</span></td>'+
						      '        		<td><span action-value="infty" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Infinity">\\infty</span></td>'+	
						      '			</tr>'+					      
						      '			<tr>'+
						      '        		<td><span action-value="sqsubseteq" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\sqsubseteq</span></td>'+
						      '        		<td><span action-value="sqsupseteq" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\sqsupseteq</span></td>'+
						      '        		<td><span action-value="doteq" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\doteq</span></td>'+
						      '        		<td><span action-value=";" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="Semicolon">;</span></td>'+
						      '        		<td><span action-value="in" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\in</span></td>'+
						      '        		<td><span action-value="22" action-type="nextTabPad" class="colSpan tab-pan-navigate'+uniqueNo+'" title="Next Symbols Set"><strong>More</strong></span></td>'+	
						      '			</tr>'+					      
						      '</table></div>'+						      
						      '       <div class="calcValLft" id="'+uniqueNo+'pad23" style="display:none;"><table class="table editorTbl">'+
						      '		  	<tr>'+
						      '        		<td> <span action-value="23" action-type="previousTabPad" class="colSpan tab-pan-navigate'+uniqueNo+'" title="Previous Symbols Set"><strong>Prev</strong></span></td>'+
						      '        		<td> <span action-value="ni" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\ni</span></td>'+
						      '        		<td> <span action-value="vdash" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\vdash</span></td>'+
						      '        		<td><span action-value="dashv" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\dashv</span></td>'+
						      '        		<td><span action-value="ldots" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\ldots</span></td>'+
						      '        		<td><span action-value="cdots" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\cdots</span></td>'+
							  '			</tr>'+						      
						      '			<tr>'+
						      '        		<td> <span action-value="vdots" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\vdots</span></td>'+
						      '        		<td> <span action-value="ddots" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\ddots</span></td>'+
						      '        		<td><span action-value="aleph" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\aleph</span></td>'+
						      '        		<td><span action-value="prime" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\prime</span></td>'+
						      '        		<td><span action-value="emptyset" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\emptyset</span></td>'+
						      '        		<td><span action-value="nabla" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\nabla</span></td>'+
						      '			</tr>'+						      
						      '			<tr>'+
						      '        		<td><span action-value="surd" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\surd</span></td>'+
						      '        		<td><span action-value="flat" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\flat</span></td>'+
						      '        		<td><span action-value="ell" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\ell</span></td>'+
						      '        		<td><span action-value="top" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\top</span></td>'+
						      '        		<td><span action-value="bot" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\bot</span></td>'+
						      '        		<td><span action-value="natural" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\natural</span></td>'+
						      '			</tr>'+						      
						      '			<tr>'+
						      '        		<td><span action-value="wp" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\wp</span></td>'+
						      '        		<td><span action-value="sharp" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\sharp</span></td>'+
						      '        		<td><span action-value="Im" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\Im</span></td>'+
						      '        		<td><span action-value="Re" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\Re</span></td>'+
						      '        		<td><span action-value="partial" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\neg</span></td>'+
						      '        		<td><span action-value="heartsuit" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\heartsuit</span></td>'+	
						      '			</tr>'+					      
						      '			<tr>'+
						      '        		<td><span action-value="clubsuit" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\sqsubseteq</span></td>'+
						      '        		<td><span action-value="spadesuit" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\sqsupseteq</span></td>'+
						      '        		<td><span action-value="forall" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\forall</span></td>'+
						      '        		<td><span action-value="exists" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\exists</span></td>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+		
						      '			</tr>'+					      
						      '</table></div>'+
						      '    <div class="calcValRt"><table class="table editorTbl">'+
						      '      <tr>'+
						      '        <td> <span action-value="+" action-type="write" class="colSpan colSpanRed mathquill-read'+uniqueNo+'" title="Plus">+</span></td>'+
						      '        <td>'+
						      '          <span action-value="7" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Seven">7</span>'+
						      '        </td>'+
						      '        <td> <span action-value="8" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Eight">8</span></td>'+
						      '        <td><span action-value="9" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Nine">9</span></td>'+						      
						      '      </tr>'+
						      '       <tr>'+
						      '        <td> <span action-value="-" action-type="write" class="colSpan colSpanRed mathquill-read'+uniqueNo+'" title="Minus">-</span></td>'+
						      '         <td>'+
						      '          <span action-value="4" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Four">4</span>'+
						      '        </td>'+
						      '        <td> <span action-value="5" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Five">5</span></td>'+
						      '        <td><span action-value="6" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Six">6</span></td>'+						      
						      '      </tr> '+
						      '      <tr>'+
						      '        <td>  <span action-value="×" action-type="write" class="colSpan colSpanRed mathquill-read'+uniqueNo+'" title="Multiply">×</span></td>'+ 
						      '         <td>'+
						      '          <span action-value="1" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="One">1</span>'+
						      '        </td>'+
						      '        <td> <span action-value="2" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Two">2</span></td>'+
						      '        <td><span action-value="3" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Three">3</span></td>'+						      
						      '      </tr>'+
						      '      <tr>'+
						      '        <td> <span action-value="÷" action-type="write" class="colSpan colSpanRed mathquill-read'+uniqueNo+'" title="Division">÷</span></td>'+
						      '        <td>'+
						      '          <span action-value="0" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Zero">0</span>'+
						      '        </td>'+
						      '        <td> <span action-value="38" action-type="keystroke" class="colSpan colSpanYlo keyupIcon  mathquill-read'+uniqueNo+'" title="Up"></span></td>'+
						      '        <td><span action-value="." action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="dot/decimal">.</span> </td>'+						      
						      '      </tr>'+
						      '      <tr>'+
						      '        <td> <span action-value="8" action-type="keystroke" class="colSpan colSpanRed keyCrossArrow mathquill-read'+uniqueNo+'" title="Backspace"></span></td>'+
						      '        <td>'+
						      '          <span action-value="37" action-type="keystroke" class="colSpan colSpanYlo keyleftIcon  mathquill-read'+uniqueNo+'" title="left"></span>'+
						      '        </td>'+
						      '        <td> <span action-value="40" action-type="keystroke" class="colSpan colSpanYlo keydownIcon mathquill-read'+uniqueNo+'" title="Down"></span></td>'+
						      '        <td><span action-value="39" action-type="keystroke" class="colSpan colSpanYlo keyrightIcon  mathquill-read'+uniqueNo+'" title="Right"></span> </td>'+
						      '      </tr>    '+
						      '    </table></div>     '+
						      ''+
						      '   </div>'+						          
						      '   <div id="letters'+uniqueNo+'" class="tab-pane">'+						      
							  '       <div class="calcValLft"><table class="table editorTbl">'+
					      '		  	<tr>'+
						      '        		<td> <span action-value="alpha" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Alpha">\\alpha</span></td>'+
						      '        		<td> <span action-value="beta" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Beta">\\beta</span></td>'+
						      '        		<td> <span action-value="gamma" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Gamma">\\gamma</span></td>'+
						      '        		<td><span action-value="theta" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Theta">\\theta</span></td>'+
						      '        		<td><span action-value="delta" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Delta">\\delta</span></td>'+
						      '        		<td><span action-value="zeta" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Zeta">\\zeta</span></td>'+
							  '			</tr>'+						      
						      '			<tr>'+
						      '        		<td> <span action-value="gamma" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Gamma">\\gamma </span></td> '+
						      '        		<td> <span action-value="lambda" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Lambda">\\lambda</span></td>'+
						      '        		<td><span action-value="eta" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Eta">\\eta</span></td>'+
						      '        		<td><span action-value="epsilon" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Epsilon">\\epsilon</span></td>'+
						      '        		<td><span action-value="varepsilon" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Varepsilon">\\varepsilon</span></td>'+
						      '        		<td><span action-value="vartheta" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Vartheta">\\vartheta</span></td>'+
						      '			</tr>'+						      
						      '			<tr>'+
						      '        		<td><span action-value="sigma" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Sigma">\\sigma</span></td>'+
						      '        		<td><span action-value="varsigma" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Varsigma">\\varsigma</span></td>'+
						      '        		<td><span action-value="omega" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Omega">\\omega</span></td>'+
						      '        		<td><span action-value="chi" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Chi">\\chi</span></td>'+
						      '        		<td><span action-value="psi" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Psi">\\psi</span></td>'+
						      '        		<td><span action-value="phi" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Phi">\\phi</span></td>'+
						      '			</tr>'+						      
						      '			<tr>'+
						      '        		<td><span action-value="varphi" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Varphi">\\varphi</span></td>'+
						      '        		<td><span action-value="o" action-type="write" class="colSpan mathquill-read'+uniqueNo+'" title="o">o</span></td>'+
						      '        		<td><span action-value="pi" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Pi">\\pi</span></td>'+
						      '        		<td><span action-value="varpi" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Varpi">\\varpi</span></td>'+
						      '        		<td><span action-value="rho" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Rho">\\rho</span></td>'+
						      '        		<td><span action-value="varrho" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Varrho">\\varrho</span></td>'+	
						      '			</tr>'+					      
						      '			<tr>'+
						      '        		<td><span action-value="tau" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Tau">\\tau</span></td>'+
						      '        		<td><span action-value="upsilon" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Upsilion">\\upsilon</span></td>'+
						      '        		<td><span action-value="kappa" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Kappa">\\kappa</span></td>'+
						      '        		<td><span action-value="mu" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Mu">\\mu</span></td>'+
						      '        		<td><span action-value="nu" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Nu">\\nu</span></td>'+
						      '        		<td><span action-value="xi" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Xi">\\xi</span></td>'+	
						      '			</tr>'+					      
						      '</table></div>'+						      
						      '    <div class="calcValRt"><table class="table editorTbl">'+
						      '      <tr>'+
						      '        <td> <span action-value="+" action-type="write" class="colSpan colSpanRed mathquill-read'+uniqueNo+'" title="Plus">+</span></td>'+
						      '        <td>'+
						      '          <span action-value="7" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Seven">7</span>'+
						      '        </td>'+
						      '        <td> <span action-value="8" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Eight">8</span></td>'+
						      '        <td><span action-value="9" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Nine">9</span></td>'+						      
						      '      </tr>'+
						      '       <tr>'+
						      '        <td> <span action-value="-" action-type="write" class="colSpan colSpanRed mathquill-read'+uniqueNo+'" title="Minus">-</span></td>'+
						      '         <td>'+
						      '          <span action-value="4" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Four">4</span>'+
						      '        </td>'+
						      '        <td> <span action-value="5" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Five">5</span></td>'+
						      '        <td><span action-value="6" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Six">6</span></td>'+						      
						      '      </tr> '+
						      '      <tr>'+
						      '        <td>  <span action-value="×" action-type="write" class="colSpan colSpanRed mathquill-read'+uniqueNo+'" title="Multiply">×</span></td>'+ 
						      '         <td>'+
						      '          <span action-value="1" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="One">1</span>'+
						      '        </td>'+
						      '        <td> <span action-value="2" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Two">2</span></td>'+
						      '        <td><span action-value="3" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Three">3</span></td>'+						      
						      '      </tr>'+
						      '      <tr>'+
						      '        <td> <span action-value="÷" action-type="write" class="colSpan colSpanRed mathquill-read'+uniqueNo+'" title="Division">÷</span></td>'+
						      '        <td>'+
						      '          <span action-value="0" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Zero">0</span>'+
						      '        </td>'+
						      '        <td> <span action-value="38" action-type="keystroke" class="colSpan colSpanYlo keyupIcon  mathquill-read'+uniqueNo+'" title="Up"></span></td>'+
						      '        <td><span action-value="." action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="dot/decimal">.</span> </td>'+						      
						      '      </tr>'+
						      '      <tr>'+
						      '        <td> <span action-value="8" action-type="keystroke" class="colSpan colSpanRed keyCrossArrow mathquill-read'+uniqueNo+'" title="Backspace"></span></td>'+
						      '        <td>'+
						      '          <span action-value="37" action-type="keystroke" class="colSpan colSpanYlo keyleftIcon  mathquill-read'+uniqueNo+'" title="left"></span>'+
						      '        </td>'+
						      '        <td> <span action-value="40" action-type="keystroke" class="colSpan colSpanYlo keydownIcon  mathquill-read'+uniqueNo+'" title="Down"></span></td>'+
						      '        <td><span action-value="39" action-type="keystroke" class="colSpan colSpanYlo keyrightIcon  mathquill-read'+uniqueNo+'" title="Right"></span> </td>'+
						      '      </tr>    '+
						      '    </table></div>     '+
						      ''+
						      '   </div>'+
						      '   <div id="misc'+uniqueNo+'" class="tab-pane">'+						      
							  '       <div class="calcValLft"><table class="table editorTbl">'+
					      '		  	<tr>'+
						      '        		<td> <span action-value="sin" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="sin">\\sin</span></td>'+
						      '        		<td> <span action-value="arcsin" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="arcsin">\\arcsin</span></td>'+
						      '        		<td> <span action-value="sinh" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="sinh">\\sinh</span></td>'+
						      '        		<td><span action-value="cos" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="cos">\\cos</span></td>'+
						      '        		<td><span action-value="arccos" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="arccos">\\arccos</span></td>'+
						      '        		<td><span action-value="cosh" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="cosh">\\cosh</span></td>'+
							  '			</tr>'+						      
						      '			<tr>'+
						      '        		<td> <span action-value="tan" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="tan">\\tan</span></td> '+
						      '        		<td> <span action-value="arctan" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="arctan">\\arctan</span></td>'+
						      '        		<td><span action-value="tanh" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="tanh">\\tanh</span></td>'+
						      '        		<td><span action-value="csc" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="csc">\\csc</span></td>'+
						      '        		<td><span action-value="csch" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="csch">\\csch</span></td>'+
						      '        		<td><span action-value="sec" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="sec">\\sec</span></td>'+

						      '			</tr>'+						      
						      '			<tr>'+						      	
						      '        		<td><span action-value="sech" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="sech">\\sech</span></td>'+	
						      '        		<td><span action-value="cot" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="cot">\\cot</span></td>'+
						      '        		<td><span action-value="coth" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="coth">\\coth</span></td>'+
						      '        		<td><span action-value="deg" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="degree">\\deg</span></td>'+	
						      '        		<td><span action-value="det" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="det">\\det</span></td>'+
						      '        		<td><span action-value="dim" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="">\\dim</span></td>'+						      
						      '			</tr>'+						      
						      '			<tr>'+
						      '        		<td><span action-value="log" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="log">\\log</span></td>'+
						      '        		<td><span action-value="gcd" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="gcd">\\gcd</span></td>'+
						      '        		<td><span action-value="lg" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="lg">\\lg</span></td>'+
						      '        		<td><span action-value="max" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="max">\\max</span></td>'+
						      '        		<td><span action-value="min" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="min">\\min</span></td>'+
						      '        		<td><span action-value="[" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="Big bracket">[</span></td>'+	
						      '			</tr>'+					      
						      '			<tr>'+
						      '        		<td><span action-value="langle" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="langle">langle</span></td>'+
						      '        		<td><span action-value="lfloor" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="lfloor">\\lfloor</span></td>'+
						      '        		<td><span action-value="rfloor" action-type="cmd" class="colSpan mathquill-read'+uniqueNo+'" title="rfloor">\\rfloor</span></td>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+
						      '        		<td><span action-value="" action-type="" class="colSpan" title=""></span></td>'+	
						      '			</tr>'+					      
						      '</table></div>'+						      
						      '    <div class="calcValRt"><table class="table editorTbl">'+
						      '      <tr>'+
						      '        <td> <span action-value="+" action-type="write" class="colSpan colSpanRed mathquill-read'+uniqueNo+'" title="Plus">+</span></td>'+
						      '        <td>'+
						      '          <span action-value="7" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Seven">7</span>'+
						      '        </td>'+
						      '        <td> <span action-value="8" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Eight">8</span></td>'+
						      '        <td><span action-value="9" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Nine">9</span></td>'+						      
						      '      </tr>'+
						      '       <tr>'+
						      '        <td> <span action-value="-" action-type="write" class="colSpan colSpanRed mathquill-read'+uniqueNo+'" title="Minus">-</span></td>'+
						      '         <td>'+
						      '          <span action-value="4" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Four">4</span>'+
						      '        </td>'+
						      '        <td> <span action-value="5" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Five">5</span></td>'+
						      '        <td><span action-value="6" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Six">6</span></td>'+						      
						      '      </tr> '+
						      '      <tr>'+
						      '        <td>  <span action-value="×" action-type="write" class="colSpan colSpanRed mathquill-read'+uniqueNo+'" title="Multiply">×</span></td>'+ 
						      '         <td>'+
						      '          <span action-value="1" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="One">1</span>'+
						      '        </td>'+
						      '        <td> <span action-value="2" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Two">2</span></td>'+
						      '        <td><span action-value="3" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Three">3</span></td>'+						      
						      '      </tr>'+
						      '      <tr>'+
						      '        <td> <span action-value="÷" action-type="write" class="colSpan colSpanRed mathquill-read'+uniqueNo+'" title="Division">÷</span></td>'+
						      '        <td>'+
						      '          <span action-value="0" action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="Zero">0</span>'+
						      '        </td>'+
						      '        <td> <span action-value="38" action-type="keystroke" class="colSpan colSpanYlo keyupIcon  mathquill-read'+uniqueNo+'" title="Up"></span></td>'+
						      '        <td><span action-value="." action-type="write" class="colSpan colSpanNum mathquill-read'+uniqueNo+'" title="dot/decimal">.</span> </td>'+						      
						      '      </tr>'+
						      '      <tr>'+
						      '        <td> <span action-value="8" action-type="keystroke" class="colSpan colSpanRed keyCrossArrow mathquill-read'+uniqueNo+'" title="Backspace"></span></td>'+
						      '        <td>'+
						      '          <span action-value="37" action-type="keystroke" class="colSpan colSpanYlo keyleftIcon  mathquill-read'+uniqueNo+'" title="left"></span>'+
						      '        </td>'+
						      '        <td> <span action-value="40" action-type="keystroke" class="colSpan colSpanYlo keydownIcon  mathquill-read'+uniqueNo+'" title="Down"></span></td>'+
						      '        <td><span action-value="39" action-type="keystroke" class="colSpan colSpanYlo keyrightIcon  mathquill-read'+uniqueNo+'" title="Right"></span> </td>'+
						      '      </tr>    '+
						      '    </table></div>     '+
						      ''+
						      '   </div>'+
						      '   <div id="keypad'+uniqueNo+'" action-value="_" action-type="cmd" class="keypadBg tab-pane">'+
						      '       <div class="keyOver">'+
						      '           <span action-value="1" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="One">1</span>'+
						      '           <span action-value="2" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="Two">2</span>'+
						      '           <span action-value="3" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="Three">3</span>'+
						      '           <span action-value="4" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="Four">4</span>'+
						      '           <span action-value="5" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="Five">5</span>'+
						      '           <span action-value="6" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="Six">6</span>'+
						      '           <span action-value="7" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="Seven">7</span>'+
						      '           <span action-value="8" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="Eight">8</span>'+
						      '           <span action-value="9" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="Nine">9</span>'+
						      '           <span action-value="0" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="Zero">0</span>'+
						      '           <span action-value="8" action-type="keystroke" class="keyNumSpan keyCrossArrow mathquill-read'+uniqueNo+'" title="Backspace"></span>'+
						      '       </div>'+
						      '       <div class="keyOver alphaColFirst">'+
						      '           <span action-value="q" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="q">q</span>'+
						      '           <span action-value="w" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="w">w</span>'+
						      '           <span action-value="e" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="e">e</span>'+
						      '           <span action-value="r" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="r">r</span>'+
						      '           <span action-value="t" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="t">t</span>'+
						      '           <span action-value="y" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="y">y</span>'+
						      '           <span action-value="u" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="u">u</span>'+
						      '           <span action-value="i" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="i">i</span>'+
						      '           <span action-value="o" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="o">o</span>'+
						      '           <span action-value="p" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="p">p</span>'+
						      '       </div>'+
						      '       <div class="keyOver alphaColMdl">'+
						      '           <span action-value="a" action-type="write" class="keyNumSpan keySpanMdl mathquill-read'+uniqueNo+'" title="a">a</span>'+
						      '           <span action-value="s" action-type="write" class="keyNumSpan keySpanMdl mathquill-read'+uniqueNo+'" title="s">s</span>'+
						      '           <span action-value="d" action-type="write" class="keyNumSpan keySpanMdl mathquill-read'+uniqueNo+'" title="d">d</span>'+
						      '           <span action-value="f" action-type="write" class="keyNumSpan keySpanMdl mathquill-read'+uniqueNo+'" title="f">f</span>'+
						      '           <span action-value="g" action-type="write" class="keyNumSpan keySpanMdl mathquill-read'+uniqueNo+'" title="g">g</span>'+
						      '           <span action-value="h" action-type="write" class="keyNumSpan keySpanMdl mathquill-read'+uniqueNo+'" title="h">h</span>'+
						      '           <span action-value="i" action-type="write" class="keyNumSpan keySpanMdl mathquill-read'+uniqueNo+'" title="i">i</span>'+
						      '           <span action-value="k" action-type="write" class="keyNumSpan keySpanMdl mathquill-read'+uniqueNo+'" title="k">k</span>'+
						      '           <span action-value="l" action-type="write" class="keyNumSpan keySpanMdl mathquill-read'+uniqueNo+'" title="l">l</span>'+
						      '           <span action-value=":" action-type="write" class="keyNumSpan keySpanMdl mathquill-read'+uniqueNo+'" title="Colon">:</span>'+
						      '       </div>'+
						      '       <div class="keyOver alphaColFirst">'+
						      '           <span action-value="z" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="z">z</span>'+
						      '           <span action-value="x" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="x">x</span>'+
						      '           <span action-value="c" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="c">c</span>'+
						      '           <span action-value="v" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="v">v</span>'+
						      '           <span action-value="b" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="b">b</span>'+
						      '           <span action-value="n" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="n">n</span>'+
						      '           <span action-value="m" action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="m">m</span>'+
						      '           <span action-value="," action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="Comma">,</span>'+
						      '           <span action-value="." action-type="write" class="keyNumSpan mathquill-read'+uniqueNo+'" title="dot/decimal">.</span>'+
						      '           <span action-value="38" action-type="keystroke" class="keyNumSpan keySpanArrow keyupIcon mathquill-read'+uniqueNo+'" title="Up"></span>'+
						      '       </div>'+
						      '       <div class="keyOver spaceBar">'+
						      '           <span action-value="32" action-type="keystroke" class="keyNumSpan keySpacebar mathquill-read'+uniqueNo+'" title="Space Bar">Space Bar</span>'+
						      '           <span action-value="37" action-type="keystroke" class="keyNumSpan keySpanArrow keyleftIcon mathquill-read'+uniqueNo+'" title="Left"></span>'+
						      '           <span action-value="40" action-type="keystroke" class="keyNumSpan keySpanArrow keydownIcon mathquill-read'+uniqueNo+'" title="Down"></span>'+
						      '           <span action-value="39" action-type="keystroke" class="keyNumSpan keySpanArrow keyrightIcon mathquill-read'+uniqueNo+'" title="Right"></span>'+
						      '       <div>'+
						      '   </div>  '+
						      ' </div> ' +
						      '</div>',
						 
						
						//html: '<div><span class="mathquill-read'+uniqueNo+''+uniqueNo+'" command="x^{2}">x^2</span><span command="\\frac{}{}" class="mathquill-read'+uniqueNo+''+uniqueNo+'">\\frac{a}{b}</span></div>',

						onLoad: function( widget ) {
							var that = this;
							var inputElementContainer = CKEDITOR.document.getById( this.domId );
							
							$(inputElementContainer.$).find(".mathquill-read"+uniqueNo).on('click', function() {							
								
								var opr = $(this).attr("action-type");
								var val = $(this).attr("action-value");								
								   if(opr === "keystroke") {      
								     $('#mathQuill'+uniqueNo).focus();
								     var e = jQuery.Event("keydown");
								     e.which = parseInt(val); // # Some key code value
								     $("#mathQuill"+uniqueNo).trigger(e);								      
								   } else {      
								      $('#mathQuill'+uniqueNo).mathquill(opr, val);
								      $('#mathQuill'+uniqueNo).focus();
								   }							
							
							});							
							$(".mathquill-read"+uniqueNo).mathquill();		
							$("#sqrt").mathquill();
							
							$(inputElementContainer.$).find(".tab-pan-navigate"+uniqueNo).on('click', function() {
								var opr = $(this).attr("action-type");
								var val = $(this).attr("action-value");
								if (opr === "nextTabPad") {									   
									$('#'+uniqueNo+'pad'+(parseInt(val))).hide();
									$('#'+uniqueNo+'pad'+(parseInt(val)+1)).show();									   
								} else {									   
									$('#'+uniqueNo+'pad'+(parseInt(val))).hide();
									$('#'+uniqueNo+'pad'+(parseInt(val)-1)).show();
								}
							});							
						},

						commit: function( widget ) {
							// Add \( and \) to make TeX be parsed by MathJax by default.				
							var latex ="";
							
							var equationAreaLatex = equationArea.mathquill('latex');
							if(CKEDITOR.plugins.mathjax.trim(equationAreaLatex) != "") {
								latex = equationAreaLatex;
								
							} else if(CKEDITOR.plugins.mathjax.trim(equationSource.val()) !="") {
								latex = equationSource.val();
							}		
							
							widget.setData( 'math', '\\(' + latex+ '\\)' );
						}
					}
				]
			}
		]
	};
} );