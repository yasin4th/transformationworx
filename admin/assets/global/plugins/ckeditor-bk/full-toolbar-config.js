/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.skin = 'kama';
    config.extraPlugins = "mathjax,menubutton,menu,image,imagecrop";

    // config.startupFocus = true;
    // config.filebrowserBrowseUrl = '/admin/content/filemanager.aspx?path=Userfiles/File&editor=FCK';
    config.filebrowserImageUploadUrl = 'assets/global/plugins/ckeditor/filemanager/connectors/php/upload.php',
    config.font_names = 'Kruti/"Kruti Dev";' + config.font_names;
    config.allowedContent = true;
    config.toolbar = 'MyFullToolBar'
    config.toolbar_MyFullToolBar = [{
            name: "document",
            groups: ["document"],
            items: ["NewPage"]
        }, {
            name: "clipboard",
            groups: ["clipboard", "undo"],
            items: ["Cut", "Copy", "Paste", "PasteText", "PasteFromWord", "-", "Undo", "Redo"]
        }, {
            name: "editing",
            groups: ["find", "selection"],
            items: ["Find", "Replace", "-", "SelectAll","Font","FontSize"]
        }, "/", {
            name: "basicstyles",
            groups: ["basicstyles"],
            items: ["Bold", "Italic", "Underline", "Strike", "Subscript", "Superscript"]
        }, {
            name: "paragraph",
            groups: ["list", "align"],
            items: ["NumberedList", "BulletedList", "-", "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock"]
        }, {
            name: "links",
            items: ["Link", "Unlink"]
        }, {
            name: "colors",
            items : ['TextColor','BGColor'] }
        , {
            name: "insert",
            items: ["Image", "SpecialChar", "Mathjax","Fibdropdown", "Table"]
        },
        // { name: 'forms', items : ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'] },
        {
            name: "button1",
            items: ["htmlbuttons"]
        },
        {
            name: "insert",
            items: ['ImageCrop']
        }];
            // Add js and css urls to cropper.js
        config.cropperJsUrl = "https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js";
        config.cropperCssUrl = "https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.css"

        // Setup cropper options. (See cropper.js documentation https://github.com/fengyuanchen/cropperjs)
        config.cropperOption = {
            "aspectRatio": 1.8,
            "autoCropArea": 1,
            "background": false,
            "cropBoxResizable": false,
            "dragMode": "move"
        };
         // config.filebrowserImageUploadUrl = 'filemanager/connectors/asp/upload.asp?Type=Image';




};
