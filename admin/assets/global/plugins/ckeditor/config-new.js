/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.plugins.addExternal(
	'imagecrop',
	'/admin/assets/global/plugins/ckeditor/plugins/imagecrop/',
	'plugin.js'
);
CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config
	config.extraPlugins = 'mathjax,imagecrop';
	config.allowedContent = true;
	config.skin = 'kama';

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'Mathjax' ] },
		{ name: 'styles' }
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';

	config.cropperOption = {
		// "aspectRatio": 1.8,
		"autoCropArea": 0,
		"background": false,
		"cropBoxResizable": true,
		"dragMode": "crop"
	};
	// Add js and css urls to cropper.js
	config.cropperJsUrl = "https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js";
	config.cropperCssUrl = "https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.css";
	config.filebrowserBrowseUrl = '/browser/browse.php';
	config.filebrowserUploadUrl = '/uploader/upload.php';
};

	/*CKEDITOR.editorConfig = function( config ) {
		// config.extraPlugins = 'imagecrop';
		config.allowedContent = true;
		config.toolbar = [
			{ name: 'links' },
			{ name: 'insert' },
			{ name: 'forms' },
			{ name: 'tools' },
			// { name: 'insert', 'items': ['ImageCrop']}
		];
		config.toolbarGroups = [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
			{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
			{ name: 'links' },
			{ name: 'insert' },
			{ name: 'forms' },
			{ name: 'tools' },
			{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
			{ name: 'others' },
			'/',
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
			{ name: 'styles' },
			{ name: 'colors' },
			{ name: 'about' }
		];

		// Setup file browser urls (See CKEditor documentation http://docs.ckeditor.com/#!/guide/dev_file_browser_api)
		config.filebrowserBrowseUrl = '/browser/browse.php';
		config.filebrowserUploadUrl = '/uploader/upload.php';

		// Setup cropper options. (See cropper.js documentation https://github.com/fengyuanchen/cropperjs)
		config.cropperOption = {
			"aspectRatio": 1.8,
			"autoCropArea": 1,
			"background": false,
			"cropBoxResizable": false,
			"dragMode": "move"
		};

		// Add js and css urls to cropper.js
		config.cropperJsUrl = "https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js";
		config.cropperCssUrl = "https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.css"
	};*/