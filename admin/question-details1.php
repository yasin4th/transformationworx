<?php include 'Access-API-sup-sme-dtp.php'; ?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.1
Version: 3.6
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- <link rel="stylesheet" type="text/css" href="./assets/global/plugins/mathquill/mathquill.css"> -->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<!-- <title>AIETS - Test Engine</title> -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/mathquill/mathquill.css">
<link rel="stylesheet" type="text/css" href="assets/global/plugins/ckeditor/skins/kama/autoring.css">
<?php include 'html/general/headtags.php' ?>
<style>
	.table>tbody>tr>td {
		padding: 0px;
	}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square"> 
<!-- BEGIN HEADER -->
<?php include 'html/general/header.php'; ?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include 'html/general/sidebar.php'; ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Question Details
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Dashboard</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="question-bank.php">Question Bank</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Question Details</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<!-- BEGIN PAGE CONTENT-->
			
			<div class="row">
			    <div class="col-md-12">
			        <!-- BEGIN SAMPLE FORM PORTLET-->
			        <div class="portlet box green ">
			            <div class="portlet-title">
			                <div class="question-id caption">
			                    QU001
			                </div>
			                <div class="tools">
			                    <a href="javascript:;" class="collapse">
			                    </a>
			                </div>
			            </div>
			            <div class="portlet-body form label-left">
			                <form role="form">
			                    <div class="form-body">
			                        <div class="row">
			                            <div class="col-md-10">

			                                <div class="row">
			                                    <div class="col-md-6 col-sm-6">
			                                        <div class="row" style="padding: 1px 0px;">
			                                            <div class="col-sm-4 col-xs-6">
			                                                <div class="ques-detail-filter-label">
			                                                    <strong>Class</strong>
			                                                </div>
			                                            </div>
			                                            <div class="col-sm-8 col-xs-6">
			                                                <div class="form-group">
			                                                    <select id="classes" class="" style="height: 34px; width: 100%;">
			                                                        <option value="0">Select Class</option>
			                                                    </select>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <div class="row">
			                                            <div class="col-sm-4 col-xs-6">
			                                                <div class="ques-detail-filter-label">
			                                                    <strong>Subject</strong>
			                                                </div>
			                                            </div>
			                                            <div class="col-sm-8 col-xs-6">
			                                                <div class="form-group">
			                                                    <select id="subjects" class="form-control" multiple="multiple" data-tags="true"></select>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <div class="row">
			                                            <div class="col-sm-4 col-xs-6">
			                                                <div class="ques-detail-filter-label">
			                                                    <strong>Chapter</strong>
			                                                </div>
			                                            </div>
			                                            <div class="col-sm-8 col-xs-6">
			                                                <div class="form-group">
			                                                    <select id="chapters" class="form-control" multiple="multiple" data-tags="true"></select>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <div class="row">
			                                            <div class="col-sm-4 col-xs-6">
			                                                <div class="ques-detail-filter-label">
			                                                    <strong>Topic</strong>
			                                                </div>
			                                            </div>
			                                            <div class="col-sm-8 col-xs-6">
			                                                <div class="form-group">
			                                                    <select id="topics" class="form-control" multiple="multiple" data-tags="true"></select>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-md-6 col-sm-6">
			                                        <div class="row">
			                                            <div class="col-sm-4 col-xs-6">
			                                                <div class="ques-detail-filter-label">
			                                                    <strong> Difficulty Level</strong>
			                                                </div>
			                                            </div>
			                                            <div class="col-sm-8 col-xs-6">
			                                                <div class="form-group">
			                                                    <select id="difficulty-levels" class="form-control" multiple="multiple" data-tags="true"></select>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <div class="row">
			                                            <div class="col-sm-4 col-xs-6">
			                                                <div class="ques-detail-filter-label">
			                                                    <strong>Unit</strong>
			                                                </div>
			                                            </div>
			                                            <div class="col-sm-8 col-xs-6">
			                                                <div class="form-group">
			                                                    <select id="units" class="form-control" multiple="multiple" data-tags="true"></select>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <div class="row">
			                                            <div class="col-sm-4 col-xs-6">
			                                                <div class="ques-detail-filter-label">
			                                                    <strong>Tag</strong>
			                                                </div>
			                                            </div>
			                                            <div class="col-sm-8 col-xs-6">
			                                                <div class="form-group">
			                                                    <select id="tags" class="form-control" multiple="multiple" data-tags="true"></select>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <div class="row">
			                                            <div class="col-sm-4 col-xs-6">
			                                                <div class="ques-detail-filter-label">
			                                                    <strong>Skills</strong>
			                                                </div>
			                                            </div>
			                                            <div class="col-sm-8 col-xs-6">
			                                                <div class="form-group">
			                                                    <select id="skills" class="form-control" multiple="multiple" data-tags="true"></select>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>

			                                <div class="editable-question-view">

			                                    <div class="form-group">
			                                        <label class="control-label">
			                                            <h4>Question</h4>
			                                        </label>
			                                        <div class="col-md-12">
			                                            <div id="question-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); min-height: 70px;"></div>
			                                            <div id="buttons" style="display: none;"><button id="fb-addBlank" > Add Blank</button></div>
			                                            <div id="buttonsdd" style="display: none;"><button id="fb-addDropDown"> Add Drop-Down </button></div>
			                                            <div id="buttonsdnd" style="display: none;"><button id="fb-addDropDown"> Add Drop-Down </button><button id="add-option-fb-dnd"  style="display: none;"> Add Option</button></div>
			                                            <div id="options-box" class="hidden row margin-top-10">
		                                                	<div class="row">
			                                                	<div class="col-md-12">
			                                                		<label> Answers: </label>
			                                                	</div>
				                                                <div id="options" class="col-md-11"></div>
				                                                <!-- <div class="col-md-1"></div> -->
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>

			                                    <div class="form-group" id="solution">
			                                        <label class="control-label ">
			                                            <h4>Solution/Explanation</h4>
			                                        </label>
			                                        <div class="col-md-12 form-group">
			                                            <div id="description-box" contentEditable="true" style="border: 1px solid rgb(232, 227, 245); min-height: 70px;"></div>
			                                        </div>
			                                    </div>

			                                    <div id="questions-of-passage" class="form-group hide">
												    <label class="control-label">
												        <h4>Questions Of Passage</h4>
												    </label>
												    
												    <div class="col-md-12">
												        <div class="col-md-12">
												            <div id="passage-questions" class="row">											                
												                
												                
												                
												            </div>
												        </div>
												    </div>
												</div>

			                                    

			                                	</div>
			                                	<div class="col-md-12 form-group">
			                                		<a id="save-question" class="pull-right btn green mrg-left5"><i class="fa fa-save"></i> &nbsp; Save</a>
			                                		<a href="question-bank.php" class="pull-right btn green mrg-left5"><i class="fa fa-times"></i> &nbsp; Cancel</a>
			                                	</div>
			                                </div>
			                                <div class="col-md-2" style="border-left:1px dashed #999;">
			                                    <div class="well">
			                                        <h4>QC Status</h4>
			                                        <select id="question-status" class="label label-sm label-success">
			                                            <option value="0">Pending</option>
			                                            <option value="1">Done</option>
			                                        </select>
			                                    </div>
			                                </div>

			                            </div>

			                        </div>

			                    </div>
			                </form>
			            </div>
			        </div>




			    </div>
			</div>
			
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include 'html/general/footer.php'?>
<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- BEGIN PLUGINS USED BY X-EDITABLE -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.zh-CN.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/moment.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery.mockjax.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-editable/inputs-ext/address/address.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-editable/inputs-ext/wysihtml5/wysihtml5.js"></script>
<!-- END X-EDITABLE PLUGIN -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script type="text/javascript" src="assets/global/plugins/mathquill/mathquill.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/ckeditor/ckeditor.js"></script>
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/form-editable.js"></script>
<script src="assets/admin/pages/scripts/components-editors.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/custom/question-details1/question-details.js"></script>
/*<script type="text/javascript" src="js/custom/question-details/question-details.js"></script>
<script type="text/javascript" src="js/custom/question-details/get-data-question-details.js"></script>
<script type="text/javascript" src="js/custom/question-details/question-details-events.js"></script>
<script type="text/javascript" src="js/custom/question-details/options-genrator.js"></script>
<script type="text/javascript" src="js/custom/question-details/passage-based-question-edit.js"></script>
<script type="text/javascript" src="js/custom/question-details/scq-questions-edit.js"></script>
<script type="text/javascript" src="js/custom/question-details/maq-questions-edit.js"></script>
<script type="text/javascript" src="js/custom/question-details/trueFalse-questions-edit.js"></script>
<script type="text/javascript" src="js/custom/question-details/fb-questions-edit.js"></script>
<script type="text/javascript" src="js/custom/question-details/fb-dd-questions-edit.js"></script>
<script type="text/javascript" src="js/custom/question-details/fb-dnd-questions-edit.js"></script>*/

// <script src="assets/global/plugins/mathquill//mathquill.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features 
   Index.init();   
   Index.initDashboardDaterange();
   Index.initJQVMAP(); // init index page's custom scripts
   Index.initCalendar(); // init index page's custom scripts
   Index.initCharts(); // init index page's custom scripts
   Index.initChat();
   Index.initMiniCharts();
   Tasks.initDashboardWidget();
   // FormEditable.init();
});
</script>
<script type="text/javascript">
  $(function () {
	  $('#sidebar li').removeClass('active open');
	  $('#sidebar li:eq(3)').addClass('active open');
  });
  $(function () {
	  $('#program li').removeClass('active');
	  $('#program li:eq(2)').addClass('active');
  });
  
    $(".addFile").on('click', function(e){
		e.preventDefault();
        $("#FileUpload").removeClass("hidden");
    });
	$(".HideUpload-btn").on('click', function(e){
		e.preventDefault();
        $("#FileUpload").addClass("hidden");
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>