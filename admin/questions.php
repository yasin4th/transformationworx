<?php include 'Access-API-sup-sme-dtp.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
	<head>
		<!-- <title>AIETS - Test Engine</title> -->
		<link rel="stylesheet" type="text/css" href="assets/global/plugins/mathquill/mathquill.css">
		<link rel="stylesheet" type="text/css" href="assets/global/plugins/ckeditor/skins/kama/autoring.css">
		<?php include 'html/general/headtags.php'; ?>
		<style>
			.table>tbody>tr>td {
				padding: 0px;
			}

		</style>
	</head>
	<!-- END HEAD -->
	<body class="page-header-fixed page-quick-sidebar-over-content page-style-square">
		<!-- BEGIN HEADER -->
		<?php include 'html/general/header.php';?>
			<!-- END HEADER -->
			<div class="clearfix">
			</div>
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN SIDEBAR -->
				<?php include 'html/general/sidebar.php';?>
					<!-- END SIDEBAR -->
					<!-- BEGIN CONTENT -->
					<div class="page-content-wrapper">
						<div class="page-content">
							<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
							<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Modal title</h4>
										</div>
										<div class="modal-body">
											Widget settings form goes here
										</div>
										<div class="modal-footer">
											<button type="button" class="btn blue">Save changes</button>
											<button type="button" class="btn default" data-dismiss="modal">Close</button>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<!-- /.modal -->
							<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
							<!-- BEGIN PAGE HEADER-->
							<h3 class="page-title">	Question </h3>
							<div class="page-bar">
								<ul class="page-breadcrumb">
									<li>
										<i class="fa fa-home"></i>
										<a href="dashboard.php">Dashboard</a>
										<i class="fa fa-angle-right"></i>
									</li>
									<li>
										<a href="question-bank.php">Question Bank</a>
										<i class="fa fa-angle-right"></i>
									</li>
									<li>
										<a href="questions.php">Add Question</a>
									</li>
								</ul>
							</div>
							<!-- END PAGE HEADER-->
							<div class="clearfix">
							</div>
							<!-- BEGIN PAGE CONTENT-->
							<div class="row">
								<div class="col-md-12">
									<!-- BEGIN SAMPLE FORM PORTLET-->
									<div class="portlet box green ">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-question-circle"></i> Add Question
											</div>
										</div>
										<div class="portlet-body form">
											<form id="form-add-question" role="form">
												<div class="form-body">
													<div class="row">
														<div class="col-md-6 form-group">
															<label class="control-label">Mapping Keyword</label>
															<select id="map_keyword" class="form-control" onchange="autoFillUsingKeyword(this.value)">
																<option> Select Keyword </option>
															</select>
														</div>
													</div>
													<hr/>
													<label>Please select the criteria to add question</label>
													<div class="row">
														<div class="col-md-6 form-group">
															<label class="control-label"><span class="required">* </span>Class:</label>
															<select id="classes" class="form-control">
																<option> Select Class </option>
															</select>
														</div>
														<div class="col-md-6 form-group">
															<label class="control-label"><span class="required">* </span>Enter Subject: ( multiple subjects are allowed )</label>
															<select id="subjects" class="form-control" data-tags="true" multiple="multiple"></select>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6 form-group">
															<label class="control-label">Enter Units: ( multiple units are allowed )</label>
															<select id="units" class="form-control" multiple="multiple"></select>
														</div>
														<div class="col-md-6 form-group">
															<label class="control-label">Enter Chapters: ( multiple chapters are allowed )</label>
															<select id="chapters" class="form-control" multiple="multiple"></select>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6 form-group">
															<label class="control-label">Enter Topics: ( multiple topics are allowed )</label>
															<select id="topics" class="form-control" multiple="multiple"></select>
														</div>
														<div class="col-md-6 form-group">
															<label class="control-label">Enter Difficulty Level:</label>
															<select id="difficulty-levels" class="form-control" multiple="multiple"></select>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6 form-group">
															<label class="control-label">Enter Skills:</label>
															<select id="skills" class="form-control" multiple="multiple"></select>
														</div>
														<div class="col-md-6 form-group">
															<label class="control-label">Enter Tags: ( multiple topics are allowed )</label>
															<select id="tags" class="form-control" multiple="multiple"></select>
														</div>
													</div>
													<hr/>
													<div class="row">
														<div class="col-md-6 form-group">
															<label class="control-label"><span class="required">* </span>Select Question Type:</label>
															<select id="question-types" class="form-control">
																<option>Single Choice Question</option>
																<option>Multiple Choice Question</option>
															</select>
														</div>
													</div>
													<div id="question-container"></div>
													<div id="description-container" class="form-group"></div>
													<!-- <div class="hidden hint form-group">
														<label class="btn_AddHint" style="margin-top: 15px; color:#2C2CEB">Add Hint: </label>
														<div class="hidden AddHint">
															<label class='btn_removeHint' style="margin-top: 15px; color:#EB2C2C">Remove hint: </label>
															<div id="hint-box" contenteditable="true" style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"></div>
														</div>
													</div> -->
													<div class="form-group">
														<div class="checkbox-list">
															<label>
																<input id="keepSame" type="checkbox"> Keep same setting for next question</label>
														</div>
													</div>
												</div>
												<div class="form-actions">
													<div class="row">
														<div class="col-md-12">
															<button id="saveQuestion" type="submit" class="btn green"><i class="fa fa-save"></i> &nbsp; Save Question </button>
															<a href="question-bank.php" class="btn default"> <i class="fa fa-times-circle"></i> &nbsp; Cancel</a>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
									<!-- END SAMPLE FORM PORTLET-->
								</div>
							</div>

							<!-- END PAGE CONTENT-->
						</div>
					</div>
					<!-- END CONTENT -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN FOOTER -->
			<?php include 'html/general/footer.php'; ?>
				<!-- END FOOTER -->
				<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
				<!-- BEGIN CORE PLUGINS -->
				<!--[if lt IE 9]>
					<script src="assets/global/plugins/respond.min.js"></script>
					<script src="assets/global/plugins/excanvas.min.js"></script>
				<![endif]-->
				<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
				<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
				<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
				<script src="assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
				<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
				<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
				<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
				<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
				<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
				<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
				<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
				<!-- END CORE PLUGINS -->
				<!-- BEGIN PAGE LEVEL PLUGINS -->
				<script src="assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
				<!-- <script src="assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
				<script src="assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
				<script src="assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script> -->
				<script src="assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
				<script src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
				<script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
				<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
				<!-- <script src="assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script> -->
				<!-- <script src="assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script> -->
				<script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
				<!-- END PAGE LEVEL PLUGINS -->
				<!-- BEGIN PAGE LEVEL PLUGINS -->
				<script type="text/javascript" src="assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
				<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
				<!-- <script type="text/javascript" src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script> -->
				<!-- <script type="text/javascript" src="assets/global/plugins/bootstrap-markdown/lib/markdown.js"></script> -->
				<!-- <script type="text/javascript" src="assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script> -->
				<script type="text/javascript" src="assets/global/plugins/mathquill/mathquill.min.js"></script>
    <!-- <script src="http://cdn.ckeditor.com/4.6.0/standard/ckeditor.js"></script> -->

				<script type="text/javascript" src="assets/global/plugins/ckeditor/ckeditor.js"></script>
				<!-- END PAGE LEVEL PLUGINS -->
				<!-- BEGIN PAGE LEVEL SCRIPTS -->
				<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
				<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
				<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
				<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
				<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
				<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
				<script src="assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
				<script src="assets/admin/pages/scripts/components-dropdowns.js"></script>
				<script type="text/javascript" src="js/common.js"></script>
				<script type="text/javascript" src="js/custom/questions/fetchData.js"></script>
				<script type="text/javascript" src="js/custom/questions/question.js"></script>
				<script type="text/javascript" src="js/custom/questions/questions1.js"></script>
				<script type="text/javascript" src="js/custom/questions/questions2.js"></script>
				<script type="text/javascript" src="js/custom/questions/scq-questions.js"></script>
				<script type="text/javascript" src="js/custom/questions/maq-questions.js"></script>
				<script type="text/javascript" src="js/custom/questions/trueFalse-questions.js"></script>
				<script type="text/javascript" src="js/custom/questions/ordering-questions.js"></script>
				<script type="text/javascript" src="js/custom/questions/descriptive-questions.js"></script>
				<script type="text/javascript" src="js/custom/questions/fb-questions.js"></script>
				<script type="text/javascript" src="js/custom/questions/fb-dnd-questions.js"></script>
				<script type="text/javascript" src="js/custom/questions/fb-dd-questions.js"></script>
				<script type="text/javascript" src="js/custom/questions/matching-questions.js"></script>
				<script type="text/javascript" src="js/custom/questions/assertion-reason-questions.js"></script>
				<script type="text/javascript" src="js/custom/questions/passage-based-question.js"></script>
				<script type="text/javascript" src="js/custom/questions/matrix-question.js"></script>
				<script type="text/javascript" src="js/custom/questions/integer-question.js"></script>
				<!-- END PAGE LEVEL SCRIPTS -->
				<script>
					jQuery(document).ready(function() {
						Metronic.init(); // init metronic core componets
						Layout.init(); // init layout
						QuickSidebar.init(); // init quick sidebar
						Demo.init(); // init demo features
						Index.init();
						Index.initDashboardDaterange();
						Index.initJQVMAP(); // init index page's custom scripts
						Index.initCalendar(); // init index page's custom scripts
						Index.initCharts(); // init index page's custom scripts
						Index.initChat();
						Index.initMiniCharts();
						Tasks.initDashboardWidget();
						//ComponentsDropdowns.init(); disabled by me
					});
				</script>
				<script type="text/javascript">
					$(function() {
						$('#sidebar li').removeClass('active open');
						$('#sidebar li:eq(3)').addClass('active open');
					});
					$(function() {
						$('#program li').removeClass('active');
						$('#program li:eq(2)').addClass('active');

						$('#map_keyword').select2({
							allowClear: true,
				  		});
					});

				</script>
				<!-- END JAVASCRIPTS -->
	</body>
	<!-- END BODY -->

</html>