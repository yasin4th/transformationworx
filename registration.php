<<?php
    require_once 'config.inc.php';
    require_once 'config.inc.test.php';
	@session_start(); if(isset($_SESSION['role'])) { header('location: my-program');}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <div style="padding-top: 100px;"></div>
    <?php// include('html/navigation.php'); ?>
    <?php include HEADER; ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?=URI1?>">Home</a></li>
            <li class="active">Registration</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          <!--<div class="sidebar col-md-3 col-sm-3">
            <ul class="list-group margin-bottom-25 sidebar-menu">
              <li class="list-group-item clearfix"><a href="#"><i class="fa fa-angle-right"></i> Login/Register</a></li>
              <li class="list-group-item clearfix"><a href="#"><i class="fa fa-angle-right"></i> Restore Password</a></li>
              <li class="list-group-item clearfix"><a href="#"><i class="fa fa-angle-right"></i> My account</a></li>
              <li class="list-group-item clearfix"><a href="#"><i class="fa fa-angle-right"></i> Address book</a></li>
              <li class="list-group-item clearfix"><a href="#"><i class="fa fa-angle-right"></i> Wish list</a></li>
              <li class="list-group-item clearfix"><a href="#"><i class="fa fa-angle-right"></i> Returns</a></li>
              <li class="list-group-item clearfix"><a href="#"><i class="fa fa-angle-right"></i> Newsletter</a></li>
            </ul>
          </div>-->
          <!-- END SIDEBAR -->
          	<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
	          <!-- BEGIN CONTENT -->
	          <div class="register-div">
	          <br>
	            <h1 class="text-center">Registration</h1>
	            <div class="content-form-page">
	              <div class="row">
	                <div class="col-md-12 col-sm-12">
						<form id="registration-form" class="form-horizontal" role="form">
							<fieldset>
							<!-- <legend>Your personal details</legend> -->
							<div class="form-group">
								<label for="firstname" class="col-lg-4 control-label">First Name <span class="require">*</span></label>
								<div class="col-lg-8">
								<input type="text" class="form-control" id="firstname" placeholder="First Name" required="required">
								</div>
							</div>
							<div class="form-group">
								<label for="lastname" class="col-lg-4 control-label">Last Name <!-- <span class="require">*</span> --></label>
								<div class="col-lg-8">
								<input type="text" class="form-control" id="lastname" placeholder="Last Name">
								</div>
							</div>
							<!-- <div class="form-group">
								<label for="class" class="col-lg-4 control-label">Class</label>
								<div class="col-lg-8">
								<input type="text" class="form-control" id="class">
								</div>
							</div> -->
							<div class="form-group">
								<label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
								<div class="col-lg-8">
								<input type="text" class="form-control" id="email" placeholder="Email" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
								</div>
							</div>
							<div class="form-group">
								<label for="mobile" class="col-lg-4 control-label">Mobile Number</label>
								<div class="col-lg-8">
								<input type="text" class="form-control" id="mobile" placeholder="Mobile Number" required="required">
								</div>
							</div>
							</fieldset>
							<fieldset>
							<!-- <legend>Your password</legend> -->
							<div class="form-group">
								<label for="password" class="col-lg-4 control-label">Password <span class="require">*</span></label>
								<div class="col-lg-8">
								<input type="password" class="form-control" id="password" placeholder="Password" required="required">
								</div>
							</div>
							<div class="form-group">
								<label for="confirm-password" class="col-lg-4 control-label">Confirm Password <span class="require">*</span></label>
								<div class="col-lg-8">
								<input type="password" class="form-control" id="confirm-password" placeholder="Confirm Password" required="required">
								</div>
							</div>
							</fieldset>
							<!--<fieldset>
							<legend>Newsletter</legend>
							<div class="checkbox form-group">
								<label>
								<div class="col-lg-4 col-sm-4">Singup for Newsletter</div>
								<div class="col-lg-8 col-sm-8">
									<input type="checkbox">
								</div>
								</label>
							</div>
							</fieldset>-->


							<div class="row">
								<div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
									<button id="register" type="submit" class="btn btn-primary">Submit</button>
									<button type="button" id="reset" class="btn btn-default">Reset</button>
								</div>
							</div>

							<div class="form-group">
								<label for="confirm-password" class="col-lg-4 control-label"></label>
								<div class="col-lg-8">
								Have an account? <a href="login"><strong>click here</strong></a>.
								</div>
							</div>
						</form>
	                </div>
					<!-- <div class="col-md-3 col-sm-3">
	                  <div class="form-info">
	                    <h2><em>Login </em> Using:</h2>
	                     <ul class="social-icons">
							<li><a href="#" data-original-title="facebook" class="facebook" title="facebook"></a></li>
							<li><a href="#" data-original-title="Google Plus" class="googleplus" title="Google Plus"></a></li>
							<li><a href="#" data-original-title="Linkedin" class="linkedin" title="LinkedIn"></a></li>
						</ul>
	                  </div>
	                </div> -->
	                <!-- <div class="col-md-3 col-sm-3 pull-right">
	                  <div class="form-info">
	                    <h2><em>Important</em> Information</h2>
	                    <p>Lorem ipsum dolor ut sit ame dolore  adipiscing elit, sed sit nonumy nibh sed euismod ut laoreet dolore magna aliquarm erat sit volutpat. Nostrud exerci tation ullamcorper suscipit lobortis nisl aliquip  commodo quat.</p>

	                    <p>Duis autem vel eum iriure at dolor vulputate velit esse vel molestie at dolore.</p>

	                    <button type="button" class="btn btn-default">More details</button>
	                  </div>
	                </div> -->
	              </div>
	            </div>
	          </div>
	          <!-- END CONTENT -->
          	</div>
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include FOOTER; ?>
    <!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
	<script src="assets/js/custom/studentRegistration.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>