<?php include 'Access-API.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
	<?php include('html/navigation.php'); ?>
	<!-- Navigation END -->

	<div class="main">
	  <div class="container">
		<ul class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li><a href="#">Student</a></li>
			<li><a href="my-program.php">My Program</a></li>
			<li><a href="student-course-details.php">Subject</a></li>
			<li><a href="student-course-chapters.php">Chapter</a></li>
			<li><a href="view-course-details.php">Test Types</a></li>
			<li class="active">Add Test Paper</li>
		</ul>
		<!-- BEGIN SIDEBAR & CONTENT -->
		<div class="row margin-bottom-40">
		  <!-- BEGIN SIDEBAR -->
		  <div class="sidebar col-md-2 col-sm-3">
			<?php include('html/student/sidebar.php'); ?>
		  </div>
		  <!-- END SIDEBAR -->

		  <!-- BEGIN CONTENT -->
		  <div class="col-md-10 col-sm-9">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2>Add Test Paper</h2>
				</div>
				<div class="col-md-6 col-sm-6"></div>
			</div>

			<!-- BEGIN DASHBOARD STATS -->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="row">
					<div class="section">
						<div id="section1" class="portlet light bg-inverse mrg-bot10">
							<div class="portlet-title" style="border: none; margin: 0px;">
								<div class="caption">
									<i class="fa fa-puzzle-piece"></i> Section 1
								</div>
								<div class="tools">
									<a class="tooltips delete" data-container="body" data-placement="top" data-original-title="Delete Section" aria-describedby="tooltip735432"><i class="fa fa-trash-o"></i></a>
									<a class="collapse">
									</a>
								</div>
							</div>
							<div class="portlet-body form">
								<form role="form">
									<div class="form-body">
										<label>Please select the criteria to set question paper</label>
										<div class="row">
											<div class="col-md-3 col-xs-6">
												<div class="form-group">
													<label class="control-label"><span class="required">* </span>Section Name</label>
													<div class="row">
														<div class="col-md-12">
															<input type="text" class="form-control pull-right" placeholder="Enter Section Name">
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-7 col-xs-2"></div>
											<div class="col-md-2 col-xs-4">
												<div class="form-group">
													<label class="control-label">Total-Questions&nbsp;</label>
													<input  type="text" class="form-control pull-right" placeholder="Total Questions">
												</div>
											</div>
										</div>
										<div class="">
											<div class="col-md-12 green-border mrg-bot10 form-body ">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-3">
															<div class="form-group">
																<div class="row">
																	<div class="col-md-12">
																		<select class="form-control">
																			<option value="0">Select Tags</option>
																		</select>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-3">
														</div>
														<div class="col-md-2 col-md-offset-4">
															<a class="tooltips delete pull-right mrg-left10" data-container="body" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
															<a class="tooltips add-color pull-right" data-container="body" data-placement="top" data-original-title="Add"><i class="fa fa-plus-circle"></i></a>
														</div>
													</div>
													<div class="col-md-12 blue-border form-body">
														<div class="questions col-md-11">
															<div class="question form-group">
																<div class="row">
																	<div class="col-md-11">
																		<div class="row">
																			<div class="row">
																				<div class="col-md-12">
																					<div class="col-md-2 col-xs-2">
																						<span class="required">* </span>
																						<select class="question-types form-control pull-right width-select-type">
																							<option value="0">Select Question Type</option>

																						</select>
																					</div>
																					<div class="col-md-2 col-xs-2">
																					<span class="required">* </span>
																					<input type="text" class="count form-control inline pull-right width-select-type" placeholder="Count" />
																					</div>
																					<div class="col-md-2 col-xs-2">
																						<select class="difficulty-levels form-control" placeholder="Select Class">
																							<option value="0">Difficulty Level</option>
																						</select>
																					</div>
																					<div class="col-md-2 col-xs-2">
																						<select class="skills form-control" placeholder="Select Class">
																							<option value="0">Skill</option>
																						</select>
																					</div>
																					<div class="col-md-2 col-xs-1">
																						<input type="text" class="max-mark form-control" placeholder="Max Marks">
																					</div>
																					<div class="col-md-2 col-xs-1">
																						<input type="text" class="neg-mark form-control" placeholder="Neg Marks">
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col-md-1">
																		<a class="tooltips delete pull-right mrg-left10" data-container="body" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-1 col-xs-2">
															<a class="tooltips add-color pull-right" data-container="body" data-placement="top" data-original-title="Add"><i class="fa fa-plus-circle"></i></a>
														</div>
													</div>
												</div>
											</div>

										</div>
										<div class="row">
											<div class="col-md-6" style="margin-top:10px;">
											</div>
											<div class="col-md-6"></div>
										</div>
									</div>
								</form>
							</div>
						</div>

					</div>
					</div>
				</div>
					<!-- END SAMPLE FORM PORTLET-->
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6" style="margin-top:10px;">
							<div class="form-group">
								<div class="checkbox-list">
									<label><input type="checkbox"> Group some question type together</label>
								</div>
								<button class="btn btn-primary"><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Section &nbsp;&nbsp;&nbsp;</button>
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
				</div>
			</div>
			<!-- END DASHBOARD STATS -->

		  </div>
		  <!-- END CONTENT -->
		</div>
		<!-- END SIDEBAR & CONTENT -->
	  </div>
	</div>

	<!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
	<!-- END FOOTER -->

	<!-- START PAGE LEVEL JAVASCRIPTS -->
	<?php include('html/js-files.php'); ?>
	<?php include('html/student/js-files.php'); ?>


	<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>