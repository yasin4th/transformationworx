<?php
    require_once 'config.inc.test.php';
?>
<?php include 'Access-API.php'; ?>
<?php include 'Access-Institute-API.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li><a href="#">Institute</a></li>
            <li class="active">Report Card</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          <div class="sidebar col-md-3 col-sm-3">
            <?php include('html/student/sidebar.php'); ?>
          </div>
          <!-- END SIDEBAR -->

          <!-- BEGIN CONTENT -->
          <div class="col-md-9 col-sm-9">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2>Institute Create Report</h2>
				</div>
				<div class="col-md-6 col-sm-6"></div>
			</div>

			<div class="row mrg-bot20">
				<div class="col-md-12 margin-bottom-40">

					<!-- TABS -->
			        <div class="tab-style-1">
			            <ul class="nav nav-tabs">
			              <li class="active"><a href="#test-performance-tab" data-toggle="tab">Test Performance Report</a></li>
			              <li><a href="#class-performance-tab" data-toggle="tab">Class Performance Report</a></li>
			            </ul>
			            <div class="tab-content">
				            <div class="tab-pane row fade in active" id="test-performance-tab">
				                <div class="col-md-12">
				                	<form id="test-performance-report-form" method="post">
						                <div class="row">
						                	<div class="col-md-4">
												<label>Test Program</label>
												<select id="select-program-test-performance" class="form-control form-group">
													<option value="0">Select Test Program</option>
												</select>
											</div>
											<div class="col-md-4">
												<label>Test Paper</label>
												<select id="select-test-paper" class="form-control form-group">
													<option value="0">Select Test Paper</option>
												</select>
											</div>
											<div class="col-md-4">
												<button type="submit" class="btn green pull-right create-report">Create</button>
											</div>
						                </div>
					                </form>
				                </div>
				                <div class="col-md-12">
				                	<div class="portlet">
										<div class="portlet-title">
											<div class="caption">
												<h4 class="table-tilte">Section Wise Report</h4>
											</div>
											<div class="tools">
												<a href="javascript:;" class="collapse"></a>
											</div>
										</div>
										<div class="portlet-body">
											<div class="col-md-12">
								                <div class="margin-top-10 margin-bottom-10 table-scrollable">
													<table id="section-wise-report-table" class="table table-striped table-bordered table-hover">
														<thead>
															<tr>
																<th>
																	Test Name: <span class="test-name"></span>
																</th>
																<th>
																	Average Marks
																</th>
																<th>
																	High Marks
																</th>
																<th>
																	Lowest Marks
																</th>
															</tr>
														</thead>
														<tbody></tbody>
													</table>
												</div>
							                </div>
						                </div>
					                </div>
				                </div>
				                <div class="col-md-12">
				                	<div class="portlet">
										<div class="portlet-title">
											<div class="caption">
												<h4 class="table-tilte">Question Wise Report:</h4>
											</div>
											<div class="tools">
												<a href="javascript:;" class="collapse"></a>
											</div>
										</div>
										<div class="portlet-body">
											<div class="col-md-12">
								                <div class="margin-top-10 margin-bottom-10 table-scrollable">
													<table id="question-wise-report-table" class="table table-striped table-bordered table-hover">
														<thead>
															<tr>
																<th>
																	Question Number
																</th>

																<th>
																	Attempted
																</th>
																<th>
																	Correct
																</th>
																<th>
																	Incorrect
																</th>
																<th>
																	Not Attempted
																</th>
																<!-- <th>
																	Avg Time Correct
																</th>
																<th>
																	Avg Time Incorrect
																</th> -->
															</tr>
														</thead>
														<tbody></tbody>
													</table>
												</div>
							                </div>
						                </div>
					                </div>
				                </div>
				                <div class="col-md-12">
				                	<div class="portlet">
										<div class="portlet-title">
											<div class="caption">
												<h4 class="table-tilte">Skill Wise Report on the above test:</h4>
											</div>
											<div class="tools">
												<a href="javascript:;" class="collapse"></a>
											</div>
										</div>
										<div class="portlet-body">
											<div class="col-md-12">
								                <div class="margin-top-10 margin-bottom-10 table-scrollable">
													<table id="skill-wise-report-table" class="table table-striped table-bordered table-hover">
														<thead>
															<tr>
																<th>
																	Skill
																</th>
																<th>
																	# of Question
																</th>
																<th>
																	Max Marks
																</th>
																<th>
																	highest Marks
																</th>
																<th>
																	lowest Marks
																</th>
																<!-- <th>
																	Average
																</th> -->
																<th>
																	Mean
																</th>
															</tr>
														</thead>
														<tbody></tbody>
													</table>
												</div>
							                </div>
						                </div>
					                </div>
				                </div>
			                </div>
				            <div class="tab-pane row fade" id="class-performance-tab">
				                <!-- <div class="col-md-12">
				                	<form id="class-performance-reprt-form" method="post">
						                <div class="row">
						                	<div class="col-md-4">
												<label>Test Program</label>
												<select id="select-program-class-performance" class="form-control form-group">
													<option value="0">Select Test Program</option>
													<option value="1">Test Program 1</option>
													<option value="2">Test Program 2</option>
													<option value="3">Test Program 3</option>
												</select>
											</div>
											<div class="col-md-4"></div>
											<div class="col-md-4">
												<button type="submit" class="btn green pull-right create-report">Create</button>
											</div>
						                </div>
					                </form>
				                </div> -->
				                <div class="col-md-12">
				                	<div class="portlet">
										<div class="portlet-title">
											<div class="caption">
												<h4 class="table-tilte">Student Performance Wise Report</h4>
											</div>
											<div class="tools">
												<a href="javascript:;" class="collapse"></a>
											</div>
										</div>
										<div class="portlet-body">
											<div class="col-md-12">
								                <div class="margin-top-10 margin-bottom-10 table-scrollable">
													<table id="class-perfomance-report-table" class="table table-striped table-bordered table-hover">
														<thead></thead>
														<tbody></tbody>
													</table>
												</div>
							                </div>
						                </div>
					                </div>
				                </div>
				            </div>
			            </div>
			        </div>
			        <!-- END TABS -->

				</div>



			</div>

			<!-- END PAGE CONTENT-->
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
    <!-- END FOOTER -->

	<!-- /modal -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
	<?php include('html/student/js-files.php'); ?>


	<script src="assets/global/plugins/jquery.form.js"></script>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="admin/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="admin/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="admin/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="admin/assets/global/plugins/highcharts/highcharts.js" type="text/javascript"></script>
<script src="admin/assets/global/plugins/highcharts/modules/exporting.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->

<script src="assets/js/custom/institute/institute-report-card.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
   // Index.initCharts(); // init index page's custom scripts
});
</script>
<script>
</script>


    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>