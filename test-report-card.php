<?php include 'Access-API.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li><a href="#">student</a></li>
            <li class="active">Report Card</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          <div class="sidebar col-md-2 col-sm-3">
            <?php include('html/student/sidebar.php'); ?>
          </div>
          <!-- END SIDEBAR -->

          <!-- BEGIN CONTENT -->
          <div class="col-md-10 col-sm-9">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2>Create Report</h2>
				</div>
				<div class="col-md-6 col-sm-6"></div>
			</div>

			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="actions pull-right">
						<div class="btn-group btn-group-devided" data-toggle="buttons">
							<label class="btn btn-transparent green-haze btn-circle btn-sm active">
							<input type="radio" name="options" class="toggle" id="option1">Speed</label>
							<label class="btn btn-transparent green-haze btn-circle btn-sm">
							<input type="radio" name="options" class="toggle" id="option2">Strike Rate</label>
							<label class="btn btn-transparent green-haze btn-circle btn-sm">
							<input type="radio" name="options" class="toggle" id="option2">EAS</label>
							<label class="btn btn-transparent green-haze btn-circle btn-sm">
							<input type="radio" name="options" class="toggle" id="option2">Net Score</label>
							<label class="btn btn-transparent green-haze btn-circle btn-sm">
							<input type="radio" name="options" class="toggle" id="option2">Success Gap</label>
							<label class="btn btn-transparent green-haze btn-circle btn-sm">
							<input type="radio" name="options" class="toggle" id="option2">Toppers Gap</label>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 mrg-bot20">
					<div id="site_activities_content" class="display-none">
						<div id="site_activities" style="height: 400px;">
						</div>
					</div>
					<h4 class="text-center mrg-top10">Graphical Analysis of Part Test</h4>
				</div>

				<div class="col-md-12 mrg-top20">
					<div class="margin-top-10 margin-bottom-10 table-scrollable">
						<table class="table table-striped table-bordered table-hover">
							<thead>
							<tr>
								<th>
									TEST Code
								</th>
								<th>
									No. Of Q
								</th>
								<th>
									Qu. Attempted
								</th>
								<th>
									Correct Qu.
								</th>
								<th>
									Net Score
								</th>
								<th>
									Per Marks
								</th>
								<th>
									Speed
								</th>
								<th>
									Strike Rate
								</th>
								<th>
									EAS
								</th>
							</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<a href="my-test-history.php">T-1001</a>
									</td>
									<td>
										100
									</td>
									<td>
										55
									</td>
									<td>
										40
									</td>
									<td>
										50
									</td>
									<td>
										65
									</td>
									<td>
										1.22
									</td>
									<td>
										5
									</td>
									<td>
										25
									</td>
								</tr>
								<tr>
									<td>
										<a href="my-test-history.php">T-1001</a>
									</td>
									<td>
										100
									</td>
									<td>
										55
									</td>
									<td>
										40
									</td>
									<td>
										50
									</td>
									<td>
										65
									</td>
									<td>
										1.22
									</td>
									<td>
										5
									</td>
									<td>
										25
									</td>
								</tr>
								<tr>
									<td>
										<a href="my-test-history.php">T-1001</a>
									</td>
									<td>
										100
									</td>
									<td>
										55
									</td>
									<td>
										40
									</td>
									<td>
										50
									</td>
									<td>
										65
									</td>
									<td>
										1.22
									</td>
									<td>
										5
									</td>
									<td>
										25
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<!-- END PAGE CONTENT-->
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
    <!-- END FOOTER -->
	
	<!-- /modal -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
	<?php include('html/student/js-files.php'); ?>
	

	<script src="assets/global/plugins/jquery.form.js"></script>
	<script src="assets/js/custom/studentProfile.js" type="text/javascript"></script>

    <!-- END PAGE LEVEL JAVASCRIPTS -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="admin/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="admin/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="admin/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->

<script src="admin/assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   Index.initCharts(); // init index page's custom scripts
});
</script>

</body>
<!-- END BODY -->
</html>