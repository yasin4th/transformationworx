<div id="wrapper-container" class="wrapper-container">
<div class="content-pusher ">
	<header id="masthead" class="site-header affix bg-custom-sticky sticky-header header_overlay header_v1">
	<div id="toolbar" class="toolbar">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<aside id="text-2" class="widget widget_text">
						<div class="textwidget">
							<div class="thim-have-any-question">
							</div>

							</div>
						</aside>
						<aside id="login-menu-2" class="widget widget_login-menu">
							<div class="thim-widget-login-menu thim-widget-login-menu-base">
								<div class="thim-link-login">
									<?php
										@session_start();
										if(!isset($_SESSION['role']))
										{
									?>
									<a class="login" href="login">Login</a>
									<!-- <a class="register" href="register">Register</a> -->
									<?php
										}else{
									?>
									<ul class="list-unstyled list-inline pull-right">
									<?php include 'html/student/navigation-ul.php';?>
									</ul>
									<?php } ?>
								</div>
							</div>
						</aside>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="navigation col-sm-12">
					<div class="tm-table">
						<div class="width-logo sm-logo">
							<?php
							if(isset($_SESSION['logo']))
							{
								$website = $_SESSION['website'];
								echo "<a href='$website'><img src='assets/frontend/layout/img/logos/TRANSFORMATIONWORX.png'></a>";
							}
							else
							{
							?>
							<!-- <a href="<?=URI1?>" title="transformationworx" rel="home" class="sticky-logo">
								<span class="title">TransformationWorx</span>
							</a> -->
							<a href='https://www.transformationworx.com/'><img src='assets/frontend/layout/img/logos/TRANSFORMATIONWORX.png'></a>
							<?php
							}
							?>

						</div>
						<nav class="width-navigation table-cell table-right">
							<ul class="nav navbar-nav menu-main-menu">

							<?php
							// dont show all pregrams page
							@session_start();
							if(!isset($_SESSION['role']))
							{
							?>

								<li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard">
									<a href="./" data-toggle="modal"><span title="Exam Portal Home" data-hover="Exam Portal Home">Exam Portal Home</span></a>
								</li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard">
									<a href="#get-certificate" data-toggle="modal"><span title="Verify Certificate" data-hover="Verify Certificate">Verify Certificate</span></a>
								</li>
							<?php } ?>

							 <?php if(isset($_SESSION['role']))
								{
									if($_SESSION['role'] == 4)
									{ ?>
									<li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="my-program"><span data-hover="My Packages">My Exams</span></a></li>

									<li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="my-certification"><span data-hover="My Certificates">My Certificates</span></a></li>

									<li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="profile"><span data-hover="Events">My Profile</span></a></li>

									</li>
									<li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><a href="exams"><span data-hover="Exams">Exams</span></a></li>

									</li>
							<?php }} ?>
							</ul>
						</nav>
						<div class="menu-mobile-effect navbar-toggle" data-effect="mobile-effect">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</div>
					</div>

				</div>
			</div>
		</div>
	</header>

	<nav class="mobile-menu-container mobile-effect">
		<ul class="nav navbar-nav">
			<?php if(isset($_SESSION['role']))
			{
				if($_SESSION['role'] == 4)
				{ ?>
				<li><a href="my-program"><span data-hover="My Packages">My Exams</span></a></li>
				<li><a href="my-certification"><span data-hover="My Certification">My Certification</span></a></li>
				<li><a href="profile"><span data-hover="My Profile">My Profile</span></a></li>
				<li><a href="exam"><span data-hover="Exams">Exams</span></a></li>
				<!-- <li><a href="report"><span data-hover="Events">Report Card</span></a></li> -->
			 <?php }} ?>
		</ul>
	</nav>
	<div class="padd-t130" style=""></div>

	<div class="modal fade" id="get-certificate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="gettransscript">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Verify Certificate</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<div class="row">
								<label class="col-md-2 col-sm-3">Please enter License No.</label>
								<div class="col-md-10 col-sm-9">
									<input type="text" class="form-control" name="phrase" placeholder="Enter License No.">
								</div>
							</div>
						</div>
						<table>
							<tbody></tbody>
						</table>
					</div>
					<div class="modal-footer">
						<a href="#" target="_blank" class="btn blue hide" id="downloadTranscript">Download</a>
						<button type="submit" class="btn blue">Verify</button>
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
