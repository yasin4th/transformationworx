<div id="loader" style="width: 100%; height: 100%; position: fixed; top: 0px; left: 0px; z-index: 10050; display: none; background: rgba(0, 0, 0, 0.309804);">
        <img src="admin/assets/global/images/ajax-loader.gif" style="position: fixed;
        top: 50%;
        left: 50%;">
</div>

<footer id="colophon" class="site-footer has-footer-bottom">
	<div class="footer">
		<div class="container">
			<div class="row">
				<aside id="siteorigin-panels-builder-9" class="widget widget_siteorigin-panels-builder footer_widget">
					<div id="pl-w56d90164c1d7f">
						<style scoped>
							#pgc-w56d90164c1d7f-0-0,
							#pgc-w56d90164c1d7f-0-4 {
								width: 33.325%;
							}

							#pgc-w56d90164c1d7f-0-1,
							#pgc-w56d90164c1d7f-0-2 {
								/*width: 25.0125%;*/
								width: 16.675%;
							}

							#pg-w56d90164c1d7f-0 .panel-grid-cell {
								float: left
							}

							#pl-w56d90164c1d7f .panel-grid-cell .so-panel {
								margin-bottom: 20px
							}

							#pl-w56d90164c1d7f .panel-grid-cell .so-panel:last-child {
								margin-bottom: 0px
							}

							#pg-w56d90164c1d7f-0 {
								margin-left: -15px;
								margin-right: -15px
							}

							#pg-w56d90164c1d7f-0 .panel-grid-cell {
								padding-left: 15px;
								padding-right: 15px
							}

							@media (max-width:767px) {
								#pg-w56d90164c1d7f-0 .panel-grid-cell {
									float: none;
									width: auto
								}
								#pgc-w56d90164c1d7f-0-0,
								#pgc-w56d90164c1d7f-0-1,
								#pgc-w56d90164c1d7f-0-2,
								#pgc-w56d90164c1d7f-0-3 {
									margin-bottom: 30px
								}
								#pl-w56d90164c1d7f .panel-grid {
									margin-left: 0;
									margin-right: 0
								}
								#pl-w56d90164c1d7f .panel-grid-cell {
									padding: 0
								}
							}
						</style>
						<div class="panel-grid" id="pg-w56d90164c1d7f-0">
							<div class="panel-grid-cell" id="pgc-w56d90164c1d7f-0-0">
								<div class="so-panel widget widget_black-studio-tinymce widget_black_studio_tinymce panel-first-child" id="panel-w56d90164c1d7f-0-0-0" data-index="0">
									<h3 class="widget-title">About TransformationWorx</h3>
									<div class="textwidget">
										<p class="text-justify">At TransformationWorx, we provide CIO Certified Digital Transformation Professional designation through interactive workshops led by Industry leaders.</p>
										<div class="thim-footer-location">
											<!-- <p><img class="alignnone size-full wp-image-10" src="//educationwp.thimpress.com/wp-content/uploads/2015/11/logo.png" alt="logo-eduma-the-best-lms-wordpress-theme" width="145" height="40" /></p> -->
										</div>
									</div>
								</div>
							</div>
							<div class="panel-grid-cell" id="pgc-w56d90164c1d7f-0-1">
								<div class="so-panel widget widget_nav_menu panel-first-child panel-last-child" id="panel-w56d90164c1d7f-0-1-0" data-index="2">
									<h3 class="widget-title">Quick Links</h3>
									<div class=" megaWrapper">
										<ul id="menu-company" class="menu">
											<li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><i class="fa fa-angle-right"></i> <a href="<?=URI1?>"><span data-hover="Home">Home</span></a></li>
											
											<li class="menu-item menu-item-type-custom menu-item-object-custom drop_to_right standard"><i class="fa fa-angle-right"></i> <a href="login"><span data-hover="Events">Login</span></a></li>
											<!-- <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><i class="fa fa-angle-right"></i> <a href="register"><span data-hover="Gallery">Register</span></a></li> -->
											<li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><i class="fa fa-angle-right"></i> <a target="_blank" href="http://transformationworx.dltlabs.ca/Exam%20Taking%20Instructions.pdf"><span data-hover="Gallery">Exam Instructions</span></a></li>
											<!-- <li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><i class="fa fa-angle-right"></i> <a href="features"><span data-hover="Become a Teacher">Features</span></a></li>
											<li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><i class="fa fa-angle-right"></i> <a href="contact-us"><span data-hover="Contact">Contact</span></a></li> -->
											
										</ul>
									</div>
								</div>
							</div>
							<div class="panel-grid-cell" id="pgc-w56d90164c1d7f-0-2">
								<div class="so-panel widget widget_nav_menu panel-first-child panel-last-child" id="panel-w56d90164c1d7f-0-2-0" data-index="3">
									<h3 class="widget-title">&nbsp;</h3>
									<div class=" megaWrapper">
										<ul id="menu-links" class="menu">
											<!-- <li class="menu-item menu-item-type-custom menu-item-object-custom drop_to_right standard"><i class="fa fa-angle-right"></i> <a href="login"><span data-hover="Events">Login</span></a></li>
											<li class="menu-item menu-item-type-post_type menu-item-object-page drop_to_right standard"><i class="fa fa-angle-right"></i> <a href="register"><span data-hover="Gallery">Register</span></a></li> -->
											<!-- <li class="menu-item menu-item-type-custom menu-item-object-custom drop_to_right standard"><i class="fa fa-angle-right"></i> <a href="faq"><span data-hover="Documentation">FAQ's</span></a></li>
											<li class="menu-item menu-item-type-custom menu-item-object-custom drop_to_right standard"><i class="fa fa-angle-right"></i> <a href="privacy-policy"><span data-hover="Forums">Privacy Policy</span></a></li>
											<li class="menu-item menu-item-type-custom menu-item-object-custom drop_to_right standard"><i class="fa fa-angle-right"></i> <a href="terms-and-condition"><span data-hover="Language Packs">Terms & Condition</span></a></li> -->
										</ul>
									</div>
								</div>
							</div>
							<div class="panel-grid-cell" id="pgc-w56d90164c1d7f-0-4">
								<div class="so-panel widget widget_nav_menu panel-first-child panel-last-child" id="panel-w56d90164c1d7f-0-4-0" data-index="5">
									<h3 class="widget-title">Contact US</h3>
									<div class="footermap">
										<!-- <p class="info"><i class="fa fa-phone"></i> (+91) 8800865479</p> -->
										<p class="info"><i class="fa fa-envelope"></i> <a href="mailto:dkhan@redmobileco.com"><span class="__cf_email__">dkhan@redmobileco.com</span></a></p>

										<p ><i class="fa fa-phone "></i>  416-909-6270 (9am-5pm, weekdays) </p>
										<p ><i class="fa fa-chrome "></i>  <a href="http://transformationworx.com/" target="_blank">transformationworx.com</a></p>
									</div>

								</div>
								<!-- <div class="so-panel widget widget_social panel-last-child" id="panel-w56d90164c1d7f-0-0-1" data-index="1">
									<div class="thim-widget-social thim-widget-social-base">
										<div class="thim-social">
											<ul class="social_link">
												<li><a class="facebook hasTooltip" href="#" target="_self"><i class="fa fa-facebook"></i></a></li>
												<li><a class="twitter hasTooltip" href="#" target="_self"><i class="fa fa-twitter"></i></a></li>
												<li><a class="google-plus hasTooltip" href="#" target="_self"><i class="fa fa-google-plus"></i></a></li>
												<li><a class="pinterest hasTooltip" href="#" target="_self"><i class="fa fa-pinterest"></i></a></li>
											</ul>
										</div>
									</div>
								</div> -->
							</div>
						</div>
					</div>
				</aside>
			</div>
		</div>
	</div>

</footer>

<footer class="copyright">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<p>2017 &copy TransformationWorx. All Rights Reserved.</p>
			</div>
			<div class="col-md-6 col-sm-6">
				<!-- <a href="http://4thpointer.com/" target="_blank" class="logo-footer"><img src="assets/frontend/layout/img/logos/4thpointer-footer-logo.png" class="img-responsive footer-logo" alt="" title="4thPointer Services"></a> -->
				<a style="color: #fff;" href="http://dltlabs.ca" target="_blank" class="logo-footer">DLT Labs</a>
			</div>
		</div>
	</div>
</footer>

<script type="text/javascript">
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-80978781-1', 'auto');
	ga('send', 'pageview');
</script>

	<!-- /.modal -->
	<div class="modal fade bs-modal-sm" id="redeem-coupon-modal" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-sm">
		<div class="modal-content">
		  <form id="redeem-coupon-form">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			  <h4 class="modal-title">Apply Your Coupon</h4>
			</div>
			<div class="modal-body">
			   <div class="form-group">
				  <label>Coupon Code</label>
				  <input type="text" class="coupon-code form-control" placeholder="Enter Coupon Code">
				</div>
			</div>
			<div class="modal-footer">
			  <button type="submit" class="btn green">Apply</button>
			  <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
			</div>
		  </form>
		</div>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
