<?php
  require_once 'config.inc.test.php';

  @session_start();
  if(isset($_SESSION['website']))
  {
	header('Location: my-program');
  }
  if(isset($_SESSION['role']) && $_SESSION['role'] != '4')
  {
	@session_destroy();
  }
?>
<!DOCTYPE html>
<html >
<head>
	<base href="<?=URI;?>" />

	<?php @include 'html/head-tag.php';?>
	<link href="assets/frontend/onepage/css/style.css" rel="stylesheet">
	<link href="assets/frontend/onepage/css/style-responsive.css" rel="stylesheet">
	<link href="blade/main/custom.css" rel="stylesheet">
	<link href="blade/main/owl.carousel.css" rel="stylesheet">
	<link href="blade/main/owl.theme.css" rel="stylesheet">
	<link href="blade/main/settings.css" rel="stylesheet">
	<link href="blade/main/style-revolution-slider.css" rel="stylesheet">
	<script type="text/javascript">
	if (window.location.hash && window.location.hash == '#_=_') {
		window.location.hash = '';
	}
	</script>

</head>

<body class="">
	<!-- Navigation START -->
	<?php include HEADER; ?>
	<!-- Navigation END -->
	<!-- carousel slider start -->
	<!-- END SLIDER -->
	<!-- BEGIN SLIDER -->
	<div class="page-slider">
		<div class="fullwidthbanner-container revolution-slider">
			<div class="fullwidthabnner">
				<ul id="revolutionul">
					<!-- THE NEW SLIDE -->
					<li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay	="9400" data-thumb="blade/main/revolutionslider/thumbs/thumb2.jpg">
						<!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
						<img src="blade/main/images/slide-2.jpg" class="img-responsive" alt="image">

						<div class="caption lft slide_title_white slide_item_left"
							data-x="30"
							data-y="90"
							data-speed="400"
							data-start="1500"
							data-easing="easeOutExpo">
							<span class="slide_title_white_bold">Blockchain-Based Online Assessment </span><br>to test your skills
						</div>
						<!-- <a class="caption lft btn green slide_btn slide_item_left mrgl-50 mrgt-50 bg-color" href="contact-us"
							data-x="100"
							data-y="315"
							data-speed="400"
							data-start="1500"
							data-easing="easeOutExpo">
							Enquiry Now
						</a> -->
					</li>
					<li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay=	"9400" data-thumb="blade/main/revolutionslider/thumbs/thumb2.jpg">
						<img src="blade/main/images/slider-img-4.jpg" class="img-responsive" alt="image">
						<div class="caption lfr slide_title_white revo-font"
							data-x="400"
							data-y="80"
							data-speed="400"
							data-start="2000"
							data-easing="easeOutExpo">
							Your Certificate will be<br> Stored on Ethereum Blockchain
						</div>
						<!-- <a class="caption lfr btn yellow slide_btn bg-color" href="contact-us"
							data-x="700"
							data-y="280"
							data-speed="400"
							data-start="3500"
							data-easing="easeOutExpo">
							Enquiry Now
						</a> -->
					</li>
				</ul>
				<div class="tp-bannertimer tp-bottom"></div>
			</div>
		</div>
	</div>
	<!-- END SLIDER -->


	<div id="main">

		<div class="institute-benifits">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="content text-center mrg-bottom40">
							<h2 class="title">Become a <strong>CIO Certified Professional</strong></h2>
							<h4>TransformationWorx is an online assessment portal which aims at helping professionals transcend geographical boundaries and time constraints in the task of constant evaluation of their performance.</h4>
						</div>
					</div>
					<div class="col-md-12 col-sm-12">
						<div id="myCarousel" class="carousel slide carousel-fade mrgt-20 mrg-bot80" data-ride="carousel">
							<div class="carousel-inner inner-2" role="listbox">
								<div class="item active">
									<div class="col-md-10 col-md-offset-1 col-sm-12 text-center">
										<h2>CIO Certified Professional</h2>
										<p>This international professional development certification establishes an industry minimum standard and "common body of knowledge" for those professionals who wish to support business and digital transformation and an enterprise-grade implementation of blockchain.</p>
									</div>
								</div>
								<div class="item">
									<div class="col-md-10 col-md-offset-1 col-sm-12 text-center">
										<h2>Executive Certification in Digital Transformation </h2>
										<p><strong>The Executive Stream:</strong> This program will offer a series of half-day workshops on current emerging topics of interest in Business Digital Transformation tailored to C-Suite Executives.</p>
									</div>
								</div>
								<div class="item">
									<div class="col-md-10 col-md-offset-1 col-sm-12 text-center">
										<h2>CIO Certified Blockchain Professional</h2>
										<p><strong>The Business Stream:</strong> This stream is for middle-management, product managers, sales and marketing, finance, legal and regulatory teams within in organizations. The Foundational Certification requires the completion of Day 1 and Day 2, successful completion of a small capstone project and an exam.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<?php include FOOTER; ?>


	<!-- <div class="request-demo-div">
		<a href="demo.php" class="btn btn-success" id="demo-request">Request For Demo</a>
	</div> -->

	<?php include('html/js-files.php'); ?>
	<script src="admin/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>

	<script type='text/javascript' src='assets/js/common.js'></script>
	<script type='text/javascript' src='assets/js/custom/index1.js'></script>
	<script src="assets/frontend/onepage/scripts/jquery.nav.js" type="text/javascript"></script>
	<script src="blade/main/owl.carousel.js" type="text/javascript"></script>
	<script src="blade/main/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
	<script src="blade/main/jquery.themepunch.tools.min.js" type="text/javascript"></script>
	<script src="blade/main/revo-slider-init.js" type="text/javascript"></script>
	<!-- <script src="blade/main/script.js" type="text/javascript"></script> -->
	<script>
	$(document).ready(function() {
/*	  $("#owl-demo").owlCarousel({
		autoPlay: 3000,
		items : 4,
		itemsDesktop : [1199,3],
		itemsDesktopSmall : [979,3]
	  });
*/
	});
	</script>

</body>

</html>
