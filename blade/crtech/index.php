<?php
  require_once 'config.inc.test.php';

  @session_start();
  if(isset($_SESSION['website']))
  {
	header('Location: my-program');
  }
  if(isset($_SESSION['role']) && $_SESSION['role'] != '4')
  {
	@session_destroy();
  }
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<base href="<?=URI;?>" />

	<?php @include 'html/head-tag.php';?>

	<!-- Loading Bootstrap -->
	<link href="blade/crtech/css/style.css" rel="stylesheet">
	<link href="blade/crtech/css/bootstrap.css" rel="stylesheet">
	<link href="blade/crtech/css/responsive.css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="blade/crtech/js/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="blade/crtech/js/fancybox/jquery.fancybox.css" media="screen" />

	<link rel="stylesheet" href="blade/crtech/css/skins/default.css" data-name="skins">
	<link rel="stylesheet" type="text/css" href="blade/crtech/css/switcher.css" media="screen" />


	<!-- Loading Flat UI -->
	<link href="blade/crtech/css/flat-ui.css" rel="stylesheet">

	<link rel="shortcut icon" href="blade/crtech/images/favicon.ico">

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->
	</head>

<body>
	<div id="preloader">
		<div id="status"></div>
	</div>

	<?php include HEADER;?>
	<!-- <nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="#">WebSiteName</a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav">
				<li class="active"><a href="#">Home</a></li>
				<li><a href="#">Page 1</a></li>
				<li><a href="#">Page 2</a></li>
				<li><a href="#">Page 3</a></li>
				</ul>
			</div>
		</div>
	</nav> -->



	<!-- <div class="container login-container">
		<div class="row">
			<div class="span5 offset7">
				<div class="login-form-div">
					<h6 class="text-center"><span>Sign In</span></h6>
					<form id="login-form" name="login-form" method="post"  action="http://exampointer.com/vendor/b2b-login.php" method="post" enctype="multipart/form-data">
						<div class="row">
							<input type="hidden" name="action" id="action" value="login">
							<input type="hidden" name="referrer" id="referrer" value="http://geniusvisionacademy.com/">
						</div>
						<div class="row">
							<p class="span5">
								<input id="email" name="email" type="text" tabindex="2" value="" placeholder="User Id">
							</p>
						</div>
						<div class="row">
							<p class="span5">
								<input id="password" name="password" type="password" tabindex="1" value="" placeholder="Password">
							</p>
						</div>
						<div class="row">
							<p class="span5">
								<button type="submit" class="btn btn-embossed btn-large btn-primary">Submit</button>
							</p>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div> -->


	<!--Slider-->
	<section class="revSlider">
		<!-- START REVOLUTION SLIDER 2.3.91 -->
			<div class="fullwidthbanner-container">
					<div class="fullwidthbanner">
						<ul>
							<li data-transition="randomrotate" data-slotamount="6" data-thumb="img/revslider/transparent.png">
								<img class="tp-caption lfl" src="blade/crtech/img/revslider/banner-1.jpg" alt="tiga">
								<div class="banner-overlay"></div>
								<div class="caption large_black_text sft tp-caption start"  data-x="0" data-y="80" data-speed="700" data-start="1200" data-easing="easeOutBack"></div>

								<div class="caption very_large_black_text sft tp-caption start"  data-x="0" data-y="120" data-speed="700" data-start="1800" data-easing="easeOutBack">Maximize Learning Outcomes
								</div>

								<div class="caption medium_black_text text sft tp-caption start"  data-x="0" data-y="180" data-speed="700" data-start="2300" data-easing="easeOutBack"> Test after test, You know Where you stand <br>What next to do to succeed
								</div>

								<!-- <div class="caption medium_black_text text sft tp-caption start"  data-x="0" data-y="180" data-speed="700" data-start="2300" data-easing="easeOutBack"> Test after test, You know Where you stand <br>What next to do to succeed
								</div>
								<div data-easing="easeOutExpo" data-start="2800" data-speed="900" data-y="240" data-x="0" class="caption lfl small_black_text tp-caption start" >
									An innovative platform extend the e-learning capabilities of any<br> educational institution by providing them a real time analysis on<br> their institution health
								</div> -->
							</li>
							<li data-transition="randomrotate" data-slotamount="6" data-thumb="blade/crtech/img/revslider/transparent.png">
								<img class="tp-caption lfl" src="blade/crtech/img/revslider/banner-2.jpg" alt="tiga">
								<div class="banner-overlay"></div>
								<div class="caption large_black_text sft tp-caption start"  data-x="0" data-y="80" data-speed="700" data-start="1200" data-easing="easeOutBack"></div>

								<div class="caption very_large_black_text sft tp-caption start"  data-x="0" data-y="120" data-speed="700" data-start="1800" data-easing="easeOutBack">Maximize Learning Outcomes
								</div>

								<div class="caption medium_black_text text sft tp-caption start"  data-x="0" data-y="180" data-speed="700" data-start="2300" data-easing="easeOutBack"> Test after test, You know Where you stand <br>What next to do to succeed
								</div>
								<div data-easing="easeOutExpo" data-start="2800" data-speed="900" data-y="240" data-x="0" class="caption lfl small_black_text tp-caption start" >
									An innovative platform extend the e-learning capabilities of any<br> educational institution by providing them a real time analysis on<br> their institution health
								</div>
							</li>
						</ul>

					</div>
				</div>
			<!-- END REVOLUTION SLIDER -->
	</section>

	<!--About-->
	<section id="about" class="about">
		<div class="container">
			<div class="row">
				<div class="span12">
					<div class="titleHead">
						<h3>About Us</h3>
					</div>
					<!--DIVIDER-->
					<div class="divider">
						<div class="divLine"></div>
						<div class="divImg">
							<i class="icon fui-user"></i>
						</div>
						<div class="divLine"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="span12">
					<div class="row">
						<div class="span4">
							<div class="about-feature-box">
								<div class="feature-icon">
									<img src="blade/crtech/img/feature-icon/expert.png" alt="">
								</div>
								<div class="feature-text">
									<h6>Coaching - by qualified Prob. Officers and experts</h6>
								</div>
							</div>
						</div>
						<div class="span4">
							<div class="about-feature-box">
								<div class="feature-icon">
									<img src="blade/crtech/img/feature-icon/experience-icon.png" alt="">
								</div>
								<div class="feature-text">
									<h6>Experience of 20+ years (Since 1991)</h6>
								</div>
							</div>
						</div>
						<div class="span4">
							<div class="about-feature-box">
								<div class="feature-icon">
									<img src="blade/crtech/img/feature-icon/study-materials-icon.png" alt="">
								</div>
								<div class="feature-text">
									<h6>Exclusive study materials</h6>
								</div>
							</div>
						</div>
					<div class="row">
					</div>
						<div class="span4">
							<div class="about-feature-box">
								<div class="feature-icon">
									<img src="blade/crtech/img/feature-icon/audio.png" alt="">
								</div>
								<div class="feature-text">
									<h6>Audio-Visual Classes</h6>
								</div>
							</div>
						</div>
						<div class="span4">
							<div class="about-feature-box">
								<div class="feature-icon">
									<img src="blade/crtech/img/feature-icon/handling-exam.png" alt="">
								</div>
								<div class="feature-text">
									<h6>Exclusive technique of handling exams with ease</h6>
								</div>
							</div>
						</div>
						<div class="span4">
							<div class="about-feature-box">
								<div class="feature-icon">
									<img src="blade/crtech/img/feature-icon/interview-gd.jpg" alt="">
								</div>
								<div class="feature-text">
									<h6>Through support for <b>GD</b> and Interview preparation</h6>
								</div>
							</div>
						</div>
					<div class="row">
					</div>
						<div class="span4">
							<div class="about-feature-box">
								<div class="feature-icon">
									<img src="blade/crtech/img/feature-icon/test.png" alt="">
								</div>
								<div class="feature-text">
									<h6>Weekly tests to assess the comprehensive level of the students</h6>
								</div>
							</div>
						</div>
						<div class="span4">
							<div class="about-feature-box">
								<div class="feature-icon">
									<img src="blade/crtech/img/feature-icon/successful.jpg" alt="">
								</div>
								<div class="feature-text">
									<h6>Interactive session with the successful students</h6>
								</div>
							</div>
						</div>
						<div class="span4">
							<div class="about-feature-box">
								<div class="feature-icon">
									<img src="blade/crtech/img/feature-icon/facility-icon.png" alt="">
								</div>
								<div class="feature-text">
									<h6>ONLINE Test facility</h6>
								</div>
							</div>
						</div>
						<!-- <div class="span6">
							<ul>
								<li>Coaching - by qualified Prob. Officers and experts</li>
								<li>Experience of 20+ years (Since 1991)</li>
								<li>Exclusive study materials</li>
								<li>Audio-Visual Classes</li>
								<li>Exclusive technique of handling exams with ease</li>
							</ul>
						</div>
						<div class="span6">
							<ul>
								<li>Through support for <b>GD</b> and Interview preparation</li>
								<li>Weekly tests to assess the comprehensive level of the students</li>
								<li>Personality development classes (Positive-Traits)</li>
								<li>Interactive session with the successful students</li>
								<li>ONLINE Test facility</li>
							</ul>
						</div> -->
					</div>

					<div class="clearfix"></div>
				</div>
			</div> <!--./row-->
			<!--DIVIDER-->
			<div class="row">
				<div class="divider">
					<div class="divLine"></div>
					<div class="divImg">
						<i class="icon fui-user"></i>
					</div>
					<div class="divLine"></div>
				</div>
			</div>

			<div class="row">
				<div class="span4 servContent">
					<div class="ico">
						<img src="blade/crtech/images/icons/Graph@2x.png" alt="ma">
					</div>
					<div class="servDetil">
					<h4>Classroom Sessions</h4>
					<p>Classroom sessions that focus on getting you geared up and easy with all your concepts to face your examinations.</p>
					</div>
				</div>

				<div class="span4 servContent">
					<div class="ico">
						<img src="blade/crtech/images/icons/Pensils@2x.png" alt="">
					</div>
					<div class="servDetil">
					<h4>Doubt Cell</h4>
					<p>Our dedicated doubt clearance cell helps student to clarify their queries from our renowned faculty team.</p>
					</div>
				</div>

				<div class="span4 servContent">
					<div class="ico">
						<img src="blade/crtech/images/icons/iMac@2x.png" alt="">
					</div>
					<div class="servDetil">
					<h4>Online Assessments</h4>
					<p>Best In Class Online Assessment Portal to ensure your preparation is effective and efficient</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!--Intro-->
	<section id="exams" class="intro grey">
		<div class="container">
			<div class="row">
				<div class="span12">
					<div class="titleHead">
						<h3>Exams</h3>
					</div>

					<!--DIVIDER-->
					<div class="divider">
						<div class="divLine"></div>
						<div class="divImg">
							<i class="icon fui-gear"></i>
						</div>
						<div class="divLine"></div>
					</div>
				</div>

				<div class="span2">
				  <div class="tile" title="SBI">
					<img class="tile-image" alt="SBI" src="blade/crtech/images/sbi-logo.png">
					<h3 class="tile-title">SBI</h3>
				  </div>
				</div>

				<div class="span2">
				  <div class="tile" title="SSC">
					<img class="tile-image" alt="SSC" src="blade/crtech/images/ssc-logo.png">
					<h3 class="tile-title">SSC</h3>
				  </div>
				</div>

				<div class="span2">
				  <div class="tile tile-hot" title="RRB">
					<img class="tile-image" alt="RRB" src="blade/crtech/images/rrb-logo.png">
					<h3 class="tile-title">RRB</h3>
				  </div>
				</div>
				<div class="span2">
				  <div class="tile" title="IBPS">
					<img class="tile-image" alt="IBPS" src="blade/crtech/images/ibps-logo.png">
					<h3 class="tile-title">IBPS</h3>
				  </div>
				</div>

				<div class="span2">
				  <div class="tile" title="UPSC">
					<img class="tile-image" alt="UPSC" src="blade/crtech/images/upsc-logo.png">
					<h3 class="tile-title">UPSC</h3>
				  </div>
				</div>

				<div class="span2">
				  <div class="tile tile-hot" title="NDA">
					<img class="tile-image" alt="NDA" src="blade/crtech/images/nda-logo.png">
					<h3 class="tile-title">NDA</h3>
				  </div>
				</div>
			</div>
		</div>
	</section>

	<!--Contact-->
	<section id="contact" class="contact">
		<div class="container">
			<!-- <div class="row">
				<div class="span12">
					<div class="titleHead">
						<h3>Contact Us</h3>
						<p>We have team of specialists always ready to assist you for any enrollment queries that you may.<br> If you need product information or need clarifcation on any career, exam or service,<br> you can call our centralized Enrollment Help Desk on</p>
					</div>
					<div class="divider">
						<div class="divLine"></div>
						<div class="divImg">
							<i class="icon fui-mail"></i>
						</div>
						<div class="divLine"></div>
					</div>
				</div>
			</div> -->


			<!-- Contact Form -->
			<div class="row">

				<div class="span12">
					<div id="map"></div>
				</div>

				<div class="contact_info span12">
					<div class="row">
						<address class="adr span12 text-center">
							<span class="road">
								<em class="icon-map-marker"></em>
								CR_TECH COMPUTERS
								RAILWAY ROAD NEAR STATE BANK OF PATIALA
								KHATAULI (UP) 251201
							</span>
							<br/>
							<span class="phone mrgl-25"><em class="icon-phone"></em> +(91) 9358187115</span>
							<br/>
							<span class="mail">
								<em class="icon fui-mail"></em>
								<a href="mailto:geniusvisionacademy@gmail.com" >geniusvisionacademy@gmail.com</a>
							</span>
							<br/>
						</address>
					</div>
					<!-- End Contact Form -->
				</div>

			</div>


		</div>
	</section>

	<?php include FOOTER; ?>

	<!-- log in pop up -->

	<div class="modal fade login-modal" id="myModal" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center">Sign In</h4>
				</div>
				<div class="modal-body">
					<div class="login-form-div">
					<!-- <h6 class="text-center"><span>Sign In</span></h6> -->
					<form id="login-form" name="login-form" method="post"  action="http://exampointer.com/vendor/b2b-login.php" method="post" enctype="multipart/form-data">
						<div class="col-md-10 col-sm-12">
							<input id="email" name="email" type="text" tabindex="2" value="" placeholder="User Id">
						</div>
						<div class="col-md-10 col-sm-12">
							<input id="password" name="password" type="password" tabindex="1" value="" placeholder="Password">
						</div>
						<div class="col-md-10 col-sm-12">
							<button type="submit" class="btn btn-embossed btn-large btn-primary">Submit</button>
						</div>
					</form>
				</div>
				</div>
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div> -->
			</div>
		</div>
	</div>

	<!-- Load JS here for greater good =============================-->
	<!-- // <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
	<script type="text/javascript" src="blade/crtech/js/jquery.js"></script>
	<script type="text/javascript" src="blade/crtech/js/bootstrap.min.js"></script>
	<!-- // <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&language=en"></script> -->
	<!-- // <script type="text/javascript" src="js/jquery.cookie.js"></script> -->
	<!-- // <script type="text/javascript" src="js/styleswitch.js"></script> -->
	<script type="text/javascript" src="blade/crtech/js/jquery.nav.js"></script>
	<!-- // <script type="text/javascript" src="js/jquery.sticky.js"></script> -->
	<script type="text/javascript" src="blade/crtech/js/jquery.parallax-1.1.3.js"></script>
	<script type="text/javascript" src="blade/crtech/js/jquery.lavalamp-1.4.js"></script>
	<script type="text/javascript" src="blade/crtech/js/jquery.scrollTo.js"></script>

	<!-- // <script type="text/javascript" src="js/rs-plugin/js/jquery.themepunch.plugins.min.js"></script> -->
	<script type="text/javascript" src="blade/crtech/js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="blade/crtech/js/jquery.carouFredSel-6.1.0-packed.js"></script>
	<!-- // <script type="text/javascript" src="js/jquery.bxslider.min.js"></script> -->
	<!-- // <script type="text/javascript" src="js/jquery.colorbox.js"></script>     -->
	<script type="text/javascript" src="blade/crtech/js/jquery.isotope.min.js"></script>

	<script type="text/javascript" src="blade/crtech/js/main.js" charset="utf-8"></script>

</body>
</html>
