
	<header id ="top" class="mTop">
		<div class="headContent">
			<div class="container-fluid">
				<div class="row">
					<div class="span6">
						<div class="brand">
							<a href="http://geniusvisionacademy.com/"><img src="blade/crtech/img/logo2.png" class="img-responsive" alt="Logo"><span style="font-size: 24px; color: #34495e; font-family: ariel;"> <b>CR-Tech Computers</b></span></a>
						</div>
					</div>
					<div class="col-md-6 pull-right">
						<div class="menu" id="steak">
							<nav>
								<ul class="navMenu inline-list" id="nav">
									<li class="current"><a href="#top">Home</a></li>
									<li><a href="#about">About</a></li>
									<li><a href="" class="login" data-toggle="modal" data-target="#myModal">Log in</a></li>
								</ul>
								<div class="clearfix"></div>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>