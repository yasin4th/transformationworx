	<!--Footer-->
	<footer class="footer grey">
		<div class="container-fluid">
			<div class="row">
				<div class="span6">
					<div class="copyright">
						<p>&copy; 2017. All rights Reserved. </p>
					</div>
				</div>

				<div class="span6 pull-right">
					<div class="copyright text-right mrgt-3">
						<a href="https://www.facebook.com/geniusvisionacademy/">
							<i class="icon-facebook"></i>
						</a>
						<a href="mailto:geniusvisionacademy@gmail.com">
							<i class="icon-google-plus"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</footer>

