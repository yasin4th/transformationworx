<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Effort Education</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Loading materilize -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="blade/main/css/style.css" rel="stylesheet">
	<link href="blade/main/css/materialize.min.css" rel="stylesheet">
	<link href="blade/main/css/materialdesignicons.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="images/favicon.ico">
</head>

<body>
	<!-- Preloader -->
<!-- <div id="preloader">
	<div id="status"></div>
</div> -->
<?php
	include HEADER;
?>

<!-- <div class="container login-container">
		<div class="row">
			<div class="span5 offset7">
				<div class="login-form-div">
					<h6 class="text-center"><span>Sign In</span></h6>

					<form id="login-form" name="login-form" method="post"  action="http://exampointer.com/vendor/b2b-login.php" method="post" enctype="multipart/form-data">
						<div class="row">
							<input type="hidden" name="action" id="action" value="login">
							<input type="hidden" name="referrer" id="referrer" value="http://efforteducation.exampointer.com/">
						</div>
						<div class="row">
							<p class="span5">
								<input id="email" name="email" type="text" tabindex="2" value="" placeholder="User Id">
							</p>
						</div>
						<div class="row">
							<p class="span5">
								<input id="password" name="password" type="password" tabindex="1" value="" placeholder="Password">
							</p>
						</div>
						<div class="row">
							<p class="span5">
								<button type="submit" class="btn btn-embossed btn-large btn-primary">Submit</button>
							</p>
						</div>
					</form>
				</div>
			</div>
		</div>
</div> -->


<!--Slider-->
<section>
	<div class="carousel carousel-slider center" data-indicators="true">
		<!-- <div class="carousel-fixed-item center">
			<a class="btn waves-effect white grey-text darken-text-2">button</a>
		</div> -->
		<div class="carousel-item white-text" href="#one!">
			<img src="blade/main/images/slider_1.jpg" class="responsive-img" height="">
		</div>
		<div class="carousel-item white-text" href="#two!">
			<img src="blade/main/images/slider_2.jpg" class="responsive-img">
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col s12 center packages">
				<h4 class="sec_heding"> MOST POPULAR <span class="orange-text">PACKAGES</span></h4>
				<p class="sec_des">Our Test Papers packages are designed by expert panel and our Test Platform interface is alike real time examination system.<br>
				Exam aspirants can boost his preparation and get indepth performance report.</p>
			</div>
		</div>

		<div class="row">
			<div class="col s3">
				<div class="card center white-text hoverable">
				<div class="card-image program">
					<img src="blade/main/images/programs/ibps-1.jpg" class="responsive-img">
				</div>
					<div class="card-content">
						<p><a href="#"> SSC CGL Tier-2 Mock test</a></p>
					</div>
					<div class="card-action charges">
						<a href="#" class="left">Rs 499</a>
					</div>
				</div>
			</div>

			<div class="col s3">
				<div class="card center white-text hoverable">
				<div class="card-image program">
					<img src="blade/main/images/programs/ssc-cgl-1.jpg" class="responsive-img">
				</div>
					<div class="card-content">
						<p><a href="#"> SSC CGL Tier-2 Mock test</a></p>
					</div>
					<div class="card-action charges">
						<a href="#" class="left">Rs 499</a>
					</div>
				</div>
			</div>

			<div class="col s3">
				<div class="card center white-text hoverable">
					<div class="card-image program">
						<img src="blade/main/images/programs/ssc-1.jpg" class="responsive-img height">
					</div>
					<div class="card-content">
						<p><a href="#">  SSC CGL/CHSL/CPO-SI Mock Test</a></p>
					</div>
					<div class="card-action charges">
						<a href="#" class="left">Rs 499</a>
					</div>
				</div>
			</div>

			<div class="col s3">
				<div class="card center white-text hoverable">
					<div class="card-image program">
						<img src="blade/main/images/programs/sbi-1.jpg" class="responsive-img">
					</div>
					<div class="card-content">
						<p><a href="#"> SBI PO PT Mock Test 2016</a></p>
					</div>
					<div class="card-action charges">
						<a href="#" class="left">Rs 499</a>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s3">
				<div class="card center white-text hoverable">
					<div class="card-image program">
						<img src="blade/main/images/programs/ssc-cgl-2.jpg" class="responsive-img">
					</div>
					<div class="card-content">
						<p><a href="#"> SSC CGL Mock Test 2016</a></p>
					</div>
					<div class="card-action charges">
						<a href="#" class="left">Rs 499</a>
					</div>
				</div>
			</div>

			<div class="col s3">
				<div class="card center white-text hoverable">
					<div class="card-image program">
						<img src="blade/main/images/programs/sbi-po.jpg" class="responsive-img">
					</div>
					<div class="card-content">
						<p><a href="#"> SBI PO Main Mock Test 2016</a></p>
					</div>
					<div class="card-action charges">
						 <a href="#" class="left">Rs 499</a>
					</div>
				</div>
			</div>

			<div class="col s3">
				<div class="card center white-text hoverable">
					<div class="card-image program">
						<img src="blade/main/images/programs/sbi-clerk.jpg" class="responsive-img">
					</div>
					<div class="card-content">
						<p><a href="#">  SBI Clerk Mock Test 2016</a></p>
					</div>
					<div class="card-action charges">
						<a href="#" class="left">Rs 499</a>
					</div>
				</div>
			</div>

			<div class="col s3">
				<div class="card center white-text hoverable">
					<div class="card-image program">
						<img src="blade/main/images/programs/upsi.jpg" class="responsive-img">
					</div>
					<div class="card-content">
						<p><a href="#"> UPSI Test Package</a></p>
					</div>
					<div class="card-action charges">
						<a href="#" class="left">Rs 299</a>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s3">
				<div class="card center white-text hoverable">
					<div class="card-image program">
						<img src="blade/main/images/programs/ibps-clerk.jpg" class="responsive-img">
					</div>
					<div class="card-content">
						<p><a href="#"> SSC CGL Mock Test 2016</a></p>
					</div>
					<div class="card-action charges">
						<a href="#" class="left">Rs 499</a>
					</div>
				</div>
			</div>

			<div class="col s3">
				<div class="card center white-text hoverable">
					<div class="card-image program">
						<img src="blade/main/images/programs/combo-ibps.jpg" class="responsive-img">
					</div>
					<div class="card-content">
						<p><a href="#"> SBI PO Main Mock Test 2016</a></p>
					</div>
					<div class="card-action charges">
						<a href="#" class="left">Rs 499</a>
					</div>
				</div>
			</div>

			<div class="col s3">
				<div class="card center white-text hoverable">
					<div class="card-image program">
						<img src="blade/main/images/programs/combo-ssc-cgl.jpg" class="responsive-img">
					</div>
					<div class="card-content">
						<p><a href="#">  SBI Clerk Mock Test 2016</a></p>
					</div>
					<div class="card-action charges">
						<a href="#" class="left">Rs 499</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 center">
				<a  href="" class="waves-effect waves-light btn orange">View All Exams</a>
			</div>
		</div>
	</div>
</section>
<section style="background-image:url('blade/main/images/slider_1.jpg')">
	<div class="container">
		<div class="row">
			<div class="col s12 center ssc-cgl">
				<h4 class="sec_heding"><span class="orange-text">SSC CGL</span> TIER-1 2016</h4>
				<p class="sec_des white-text">ExamPointer is a web based interactive, independent and intelligent examination platform for students.<br>
				This platform covers wide range of institutions and boards.</p>
			</div>
		</div>
		<div class="row">
			<div class="col m4 offset-m1">
				<img src="blade/main/images/sec_ssc-cgl.png" class="responsive-img hoverable">
			</div>
			<div class="col m6 offset-m1 hoverable white-text">
				<ul>
					<li><i class="mdi mdi-check small orange-text"></i> Test papers questions are designed by expert teachers.</li>
					<li><i class="mdi mdi-check small orange-text"></i> Interface of the Test Platform would be similar to new pattern</li>
					<li><i class="mdi mdi-check small orange-text"></i> Test Papers have similar sectional distribution like in live paper</li>
					<li><i class="mdi mdi-check small orange-text"></i> Give your test as per your suitable time and place.</li>
					<li><i class="mdi mdi-check small orange-text"></i> Detailed report would help to check preparation levels and find the weak points.</li>
					<li><i class="mdi mdi-check small orange-text"></i>  Compare yourself with all aspiring students.</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col s12 center white-text"> <h5>Enrolled today and get <span class="orange-text font_30">Flat 25%</span>; discount use coupon code <span class="orange-text font_30">SSC1-25</span></h5></div>
		</div>

		<div class="row">
			<div class="col s12 center">
				<a href="" class="waves-effect waves-light btn orange center">View Details</a>
			</div>
			<br>
			<br>
			<br>
		</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col s12 center">
				<h4 class="sec_heding">OUR <span class="orange-text">BENEFITS</span></h4>
				<p class="sec_des">ExamPointer is a web based interactive, independent and intelligent examination platform for students.<br>
				This platform covers wide range of institutions and boards.</p>
			</div>
		</div>
		<div class="row">
			<div class="col s6 hoverable">
				<div class="panel-group" id="accordion1">
	              <div class="panel panel-default">
	                <div class="panel-heading">
	                  <h5 class="panel-title">
	                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_1">Content Quality</a>
	                  </h5>
	                </div>
	                <div id="accordion1_1" class="panel-collapse collapse in">
	                  <div class="panel-body">
	                    <p>Created and reviewed by Experts and Top Faculty across the country, Find just the right content for any exam vetted by educators.</p>
	                  </div>
	                </div>
	              </div>
	              <div class="panel panel-default">
	                <div class="panel-heading">
	                  <h5 class="panel-title">
	                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_2">Designed by experts</a>
	                  </h5>
	                </div>
	                <div id="accordion1_2" class="panel-collapse collapse">
	                  <div class="panel-body">
	                    <p>The ExamPointer test series have been designed by India’s top experts in the respective domains while keeping the real examination pattern in mind. </p>
	                  </div>
	                </div>
	              </div>
	              <div class="panel panel-default">
	                <div class="panel-heading">
	                  <h5 class="panel-title">
	                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_3">Rich Library</a>
	                  </h5>
	                </div>
	                <div id="accordion1_3" class="panel-collapse collapse">
	                  <div class="panel-body">
	                    <p>We have compiled an exhaustive set of mock tests and practice exams to help students with their exam preparation. </p>
	                  </div>
	                </div>
	              </div>
	              <div class="panel panel-default">
	                <div class="panel-heading">
	                  <h5 class="panel-title">
	                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_4">Customize Yourself</a>
	                  </h5>
	                </div>
	                <div id="accordion1_4" class="panel-collapse collapse">
	                  <div class="panel-body">
	                    <p>Plan for EVERYONE, Customized your plan according to your need, select the best which suites you best</p>
	                  </div>
	                </div>
	              </div>
	              <div class="panel panel-default">
	                <div class="panel-heading">
	                  <h5 class="panel-title">
	                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_5">Practice Exams</a>
	                  </h5>
	                </div>
	                <div id="accordion1_5" class="panel-collapse collapse">
	                  <div class="panel-body">
	                    <p>Score more by practice more. Our unique designed practice module help you to gain knowledge even when you are giving exams.</p>
	                  </div>
	                </div>
	              </div>
	              <div class="panel panel-default">
	                <div class="panel-heading">
	                  <h5 class="panel-title">
	                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_6">Time - Bound</a>
	                  </h5>
	                </div>
	                <div id="accordion1_6" class="panel-collapse collapse">
	                  <div class="panel-body">
	                    <p>All the tests are time-bound and adhere to rules &amp; syllabus that are applicable for the respective exams.</p>
	                  </div>
	                </div>
	              </div>
				</div>
			</div>
			<div class="col s5 offset-s1">
				<img src="blade/main/images/desktop.png" class="responsive-img">
			</div>
		</div>
	</div>
	<div class="student-analytics">
		<div class="container">
			<div class="row center white-text">
				<div class="col s4 hoverable mrg-pad15">
					<h4>11,258+</h4>
					<p>STUDENTS LOVE US</p>
				</div>
				<div class="col s4 hoverable mrg-pad15">
					<h4>66,072+</h4>
					<p>QUESTIONS ATTEMPTED</p>
				</div>
				<div class="col s4 hoverable mrg-pad15">
					<h4>8,526+</h4>
					<p>TESTS ATTEMPTED</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container">
		<div class="row center">
			<div class="col s12 center">
				<h4 class="sec_heding"><span class="orange-text">STUDENTS</span> TESTIMONIALS</h4>
				<p class="sec_des">Loved by Students across INDIA</p>
			</div>
		</div>
	</div>
</section>



	<!--Footer-->
<?php
	include FOOTER;
?>

	<script type="text/javascript" src="blade/main/js/jquery.js"></script>
	<script type="text/javascript" src="blade/main/js/materialize.min.js"></script>
	<script type="text/javascript">
		$('.carousel.carousel-slider').carousel({full_width: true});

		$(document).ready(function(){
		$('.carousel').carousel({dist:0});
		window.setInterval(function(){$('.carousel').carousel('next')},3000)
	});

	</script>



</body>
</html>
