<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Effort Education</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Loading materilize -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="blade/main/css/style.css" rel="stylesheet">
	<link href="blade/main/css/materialize.min.css" rel="stylesheet">
	<link href="blade/main/css/materialdesignicons.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="images/favicon.ico">
	<link href="blade/main/css/font-awesome.css" rel="stylesheet">

</head>

<body>
	<!-- Preloader -->
<!-- <div id="preloader">
	<div id="status"></div>
</div> -->
<?php
	include HEADER;
?>

<!-- <div class="container login-container">
		<div class="row">
			<div class="span5 offset7">
				<div class="login-form-div">
					<h6 class="text-center"><span>Sign In</span></h6>

					<form id="login-form" name="login-form" method="post"  action="http://exampointer.com/vendor/b2b-login.php" method="post" enctype="multipart/form-data">
						<div class="row">
							<input type="hidden" name="action" id="action" value="login">
							<input type="hidden" name="referrer" id="referrer" value="http://efforteducation.exampointer.com/">
						</div>
						<div class="row">
							<p class="span5">
								<input id="email" name="email" type="text" tabindex="2" value="" placeholder="User Id">
							</p>
						</div>
						<div class="row">
							<p class="span5">
								<input id="password" name="password" type="password" tabindex="1" value="" placeholder="Password">
							</p>
						</div>
						<div class="row">
							<p class="span5">
								<button type="submit" class="btn btn-embossed btn-large btn-primary">Submit</button>
							</p>
						</div>
					</form>
				</div>
			</div>
		</div>
</div> -->


<!--Slider-->
<!-- <section>
	<div class="carousel carousel-slider center" data-indicators="true">
		<div class="carousel-item white-text" href="#two!">
			<img src="blade/main/images/13.jpg" class="responsive-img">
			 <div class="overlay">
				 <h2> </h2>
				 </div>

		</div>
		<div class="carousel-item white-text" href="#three!">
			<img src="blade/main/images/banner6.jpg" class="responsive-img">
			 <div class="overlay">
				 <h2> </h2>
				 </div>

		</div>
		<div class="carousel-item white-text" href="#four!">
			<img src="blade/main/images/3.jpg" class="responsive-img">
			 <div class="overlay">
				 <h2> </h2>
				 </div>
		</div>
	</div>
</section> -->
<section>
	<div class="carousel carousel-slider center" data-indicators="true">
		<!-- <div class="carousel-fixed-item center">
			<a class="btn waves-effect white grey-text darken-text-2">button</a>
		</div> -->
		<div class="carousel-item white-text" href="#">
		<img src="blade/main/images/13.jpg" class="responsive-img">
			<h2>First Panel</h2>
			<p class="white-text">This is your first panel</p>
		</div>
		<div class="carousel-item amber white-text" href="#two!">
		<img src="blade/main/images/banner6.jpg" class="responsive-img">
			<h2>Second Panel</h2>
			<p class="white-text">This is your second panel</p>
		</div>
		<div class="carousel-item amber white-text" href="#two!">
		<img src="blade/main/images/3.jpg" class="responsive-img">
			<h2>Second Panel</h2>
			<p class="white-text">This is your second panel</p>
		</div>
	</div>
</section>
<section style="background-color:#f1eeee;">
	<div class="container">
		<div class="row" id="features">
			<div class="col s12 center packages">
				<h4 class="sec_heding"> MOST POPULAR <span class="orange-text">PACKAGES</span></h4>
				<p class="sec_des">Our Test Papers packages are designed by expert panel and our Test Platform interface is alike real time examination system.<br>
				Exam aspirants can boost his preparation and get indepth performance report.</p>
			</div>
		</div>
		<div class="row">
			<div class="col s3">
				<div style="background-image:url('blade/main/images/testseries1.png'); background-size: 100% 100%; background-repeat:no-repeat; height: 320px; width: 320px;">
					<div class="logo" >
						<img src="blade/main/images/ibp-logo.png">
					</div>
					<h6 style="margin: 35px;">SBI PO Main Mock Test 2016</h6>
				</div>
			</div>
			<div class="col s3">
				<div style="background-image:url('blade/main/images/testseries1.png'); background-size: 100% 100%; background-repeat:no-repeat; height: 320px; width: 320px;">
					<div class="logo" >
						<img src="blade/main/images/ibp-logo.png">
					</div>
					<h6 style="margin: 35px;">SBI PO Main Mock Test 2016</h6>
				</div>
			</div>
			<div class="col s3">
				<div style="background-image:url('blade/main/images/testseries1.png'); background-size: 100% 100%; background-repeat:no-repeat; height: 320px; width: 320px;">
					<div class="logo" >
						<img src="blade/main/images/ibp-logo.png">
					</div>
					<h6 style="margin: 35px;">SBI PO Main Mock Test 2016</h6>
				</div>
			</div>
			<div class="col s3">
				<div style="background-image:url('blade/main/images/testseries1.png'); background-size: 100% 100%; background-repeat:no-repeat; height: 320px; width: 320px;">
					<div class="logo" >
						<img src="blade/main/images/ibp-logo.png">
					</div>
					<h6 style="margin: 35px;">SBI PO Main Mock Test 2016</h6>
				</div>
			</div>
		</div>
			<div class="row">
				<div class="col s3">
					<div style="background-image:url('blade/main/images/testseries1.png'); background-size: 100% 100%; background-repeat:no-repeat; height: 320px; width: 320px;">
						<div class="logo" >
							<img src="blade/main/images/ibp-logo.png">
						</div>
						<h6 style="margin: 35px;">SBI PO Main Mock Test 2016</h6>
					</div>
				</div>
				<div class="col s3">
					<div style="background-image:url('blade/main/images/testseries1.png'); background-size: 100% 100%; background-repeat:no-repeat; height: 320px; width: 320px;">
						<div class="logo" >
							<img src="blade/main/images/ibp-logo.png">
						</div>
						<h6 style="margin: 35px;">SBI PO Main Mock Test 2016</h6>
					</div>
				</div>
				<div class="col s3">
					<div style="background-image:url('blade/main/images/testseries1.png'); background-size: 100% 100%; background-repeat:no-repeat; height: 320px; width: 320px;">
						<div class="logo" >
							<img src="blade/main/images/ibp-logo.png">
						</div>
						<h6 style="margin: 35px;">SBI PO Main Mock Test 2016</h6>
					</div>
				</div>
				<div class="col s3">
					<div style="background-image:url('blade/main/images/testseries1.png'); background-size: 100% 100%; background-repeat:no-repeat; height: 320px; width: 320px;">
						<div class="logo" >
							<img src="blade/main/images/ibp-logo.png">
						</div>
						<h6 style="margin: 35px;">SBI PO Main Mock Test 2016</h6>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col s3">
					<div style="background-image:url('blade/main/images/testseries1.png'); background-size: 100% 100%; background-repeat:no-repeat; height: 320px; width: 320px;">
						<div class="logo" >
							<img src="blade/main/images/ibp-logo.png">
						</div>
						<h6 style="margin: 35px;">SBI PO Main Mock Test 2016</h6>
					</div>
				</div>
				<div class="col s3">
					<div style="background-image:url('blade/main/images/testseries1.png'); background-size: 100% 100%; background-repeat:no-repeat; height: 320px; width: 320px;">
						<div class="logo" >
							<img src="blade/main/images/ibp-logo.png">
						</div>
						<h6 style="margin: 35px;">SBI PO Main Mock Test 2016</h6>
					</div>
				</div>
				<div class="col s3">
					<div style="background-image:url('blade/main/images/testseries1.png'); background-size: 100% 100%; background-repeat:no-repeat; height: 320px; width: 320px;">
						<div class="logo" >
							<img src="blade/main/images/ibp-logo.png">
						</div>
						<h6 style="margin: 35px;">SBI PO Main Mock Test 2016</h6>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col s12 center">
					<a  href="" class="waves-effect waves-light btn orange">View All Exams</a>
				</div>
			</div>
		</div>
</section>
<section style="background-image:url('blade/main/images/bannner1.jpg'); background-size: 100% 100%; background-repeat:no-repeat; background-attachment: fixed;">
	<div class="container">
		<div class="row">
			<div class="col s12 center ssc-cgl">
				<h3 class="sec_heding orange-text">SSC CGL<span style="color: #d0cfd0">TIER-1 2016</span> </h3>
				<p class="sec_des white-text">ExamPointer is a web based interactive, independent and intelligent examination platform for students.<br>
				This platform covers wide range of institutions and boards.</p>
			</div>
		</div>
		<div class="row">
			<div class="col m4 offset-m1">
				<img src="blade/main/images/sec_ssc-cgl.png" class="responsive-img hoverable">
			</div>
			<div class="col m7 hoverable white-text">
				<ul style="font-size:17px; ">
					<li><i class="mdi mdi-check small orange-text"></i> Test papers questions are designed by expert teachers.</li>
					<li><i class="mdi mdi-check small orange-text"></i> Interface of the Test Platform would be similar to new pattern</li>
					<li><i class="mdi mdi-check small orange-text"></i> Test Papers have similar sectional distribution like in live paper</li>
					<li><i class="mdi mdi-check small orange-text"></i> Give your test as per your suitable time and place.</li>
					<li><i class="mdi mdi-check small orange-text"></i> Detailed report would help to check preparation levels and find the weak points.</li>
					<li><i class="mdi mdi-check small orange-text"></i>  Compare yourself with all aspiring students.</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col s12 center white-text"> <h5>Enrolled today and get <span class="orange-text 	font_30">Flat 25%</span>; discount use coupon code <span class="orange-text font_30">		SSC1-25</span></h5>
			</div>
		</div>
		<div class="row">
			<div class="col s12 center">
				<a href="" class="waves-effect waves-light btn orange center">View Details</a>
			</div>
			<br>
			<br>
			<br>
		</div>
	</div>
</section>


<section id="institute" style=" background-color:#f1eeee;">
	<div class="container">
		<div class="row">
			<br>
			<br>
			<div class="col s6">
				<div class="feat-images-slide js-feat-images-slide" >
					<div class="head-bar">
					  <span></span>
						<span></span>
						<span></span>
					</div>
					<div id="slidebox3" style="position: relative;"></div>
				</div>
			</div>
			<div class="col s5" style="padding-left:120px; padding-top:90px;">
				<div class="row div-content">
					<div>
						<h3>What is lorem ipsum?</h3>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
						</p>
					</div>
					<div><a class="waves-effect waves-light btn">button</a></div>
				</div>
				<div class="row div-content">
					<div>
						<h3>What is lorem ipsum?</h3>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
						</p>
					</div>
					<div><a class="waves-effect waves-light btn">button</a></div>
				</div>
				<div class="row div-content">
					<div>
						<h3>What is lorem ipsum?</h3>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
						</p>
					</div>
					<div><a class="waves-effect waves-light btn">button</a></div>
				</div>
				<div class="row div-content">
					<div>
						<h3>What is lorem ipsum?</h3>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
						</p>
					</div>
					<div><a class="waves-effect waves-light btn">button</a></div>
				</div>
				<div class="row div-content">
					<div>
						<h3>What is lorem ipsum?</h3>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
						</p>
					</div>
					<div><a class="waves-effect waves-light btn">button</a></div>
				</div>
			</div>
		</div>
	</div>
</section>

	<!--Footer-->
<?php
	include FOOTER;
?>

	<script type="text/javascript" src="blade/main/js/jquery.min.js"></script>
	<script type="text/javascript" src="blade/main/js/materialize.min.js"></script>
	<!-- <script type="text/javascript" src="blade/main/js/plugins.min.js"></script> -->
	<!-- <script type="text/javascript" src="blade/main/js/jquery.leanModal.min.js"></script> -->
	<script src="blade/main/js/slidebox.js"></script>
	<script type="text/javascript">

		$(document).ready(function(){
			// $('.modal-trigger').leanModal();
			$('.div-content').hide();
			$('.carousel.carousel-slider').carousel({full_width: true});
			$('.carousel').carousel({dist:0});
			window.setInterval(function(){$('.carousel').carousel('next')},5000)
			$(".modal-trigger").leanModal({ top : 200, overlay : 0.4, closeButton: ".modal_close" });

			// $('#slidebox3').slide({
			// 	arrImg:['https://unsplash.it/800/500?image=1024','https://unsplash.it/800/500?image=1022','https://unsplash.it/800/500?image=1020','https://unsplash.it/800/500?image=1012','https://unsplash.it/800/500?image=1011'],
			// 	timeOut: {
			// 		auto: true,
			// 		interval: 3000
			// 	},
			// 	mode: 'VERTICAL_MODE',
			// 		isNav: true,
			// });
		$(".button-collapse").sideNav();
		// Initialize collapsible (uncomment the line below if you use the dropdown variation)
		//$('.collapsible').collapsible();
		});

	</script>
</body>
</html>
