
<!-- <header id ="top" class="mTop">

		<div class="headContent">
			<div class="container">
				<div class="row">
					<div class="span4">
						<div class="brand">
							<a href="http://efforteducation.exampointer.com/"><img src="img/logo.jpg" alt="Logo"></a>
						</div>
					</div>
					<div class="span8">
						<div class="menu" id="steak">
							<nav>
								<ul class="navMenu inline-list" id="nav">
									<li class="current"><a href="#top">Home</a></li>
									<li><a href="#about">About</a></li>
									<li><a href="#contact">Contact</a></li>
								</ul>
								<div class="clearfix"></div>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
</header> -->
<div class="navbar-fixed">
	<nav>
		<div class="nav-wrapper">
			<a href="#" class="brand-logo">
				<img src="blade/main/images/logo-1.png" alt="Logo">
			</a>
			<ul id="nav-mobile" class="right hide-on-med-and-down">
				<li class="active"><a href="index.php">Exams</a></li>
				<li><a href="features1.php">Features</a></li>
				<li><a href="Institute-Benifits.php">Institute-Benifits</a></li>
				<li><a href="team.php">Team</a></li>
				<li><a href="contact_us.php">Contact</a></li>
				<li>
					<a class="modal-trigger waves-effect waves-light" href="#modal1">Log In/Sign Up</a>
					<!-- Modal Structure -->
					<div id="modal1" class="modal">
						<div class="modal-content">
							<h4 class="black">Modal Header</h4>
							<p class="black">A bunch of text</p>
						</div>
						<div class="modal-footer">
							<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
						</div>
					</div>
				</li>
				<!--
				<li><a class='dropdown-button' href='#' data-activates='dropdown1'>User<i class="material-icons right">perm_identity</i></a>
				<!Dropdown Structure -->
					<!-- <ul id='dropdown1' class='dropdown-content'>
						<li><a href="#!">one</a></li>
						<li><a href="#!">two</a></li>
						<li class="divider"></li>
						<li><a href="#!">three</a></li>
					</ul>
				</li> -->
			</ul>
			<a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
			<ul id="slide-out" class="side-nav">
				<li class="active"><a href="index.php" class="collection-item">Exams</a></li>
				<li><a href="features1.php" class="collection-item">Features</a></li>
				<li><a href="Institute-Benifits.php" class="collection-item">Institute-Benifits</a></li>
				<li><a href="team.php" class="collection-item">Team</a></li>
				<li><a href="contact_us.php" class="collection-item">Contact</a></li>
			</ul>
		</div>
	</nav>
</div>
