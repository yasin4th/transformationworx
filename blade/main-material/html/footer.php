<!-- <footer class="footer grey">
	<div class="container">
		<div class="row">
			<div class="span6">
				<div class="copyright">
					<p>&copy; 2016. All rights Reserved. </p>
				</div>
			</div>

			<div class="span6">
				<div class="copyright text-right">
					<p>Powered By - <a href="http://exampointer.com">ExamPointer</a></p>
				</div>
			</div>
		</div>
	</div>
</footer> -->

<footer class="page-footer">
	<div class="container">
		<div class="row">
			<div class="col s12 m3 l4">
				<h5 class="white-text">ABOUT EXAMPOINTER</h5>
				<p class="grey-text text-lighten-4">ExamPointer is the online place for conducting and practicing online exams. We provide a marketplace for the institutes, couching’s, individual teachers to spread his practice papers and a well organize web platform for the students to practice of online exams in real time environment.</p>
			</div>
			<div class="col s12 m3 l2">
				<h5 class="white-text">QUICK LINKS</h5>
				<ul>
					<li><a class="grey-text text-lighten-3" href="#!">Home</a></li>
					<li><a class="grey-text text-lighten-3" href="#!">Features</a></li>
					<li><a class="grey-text text-lighten-3" href="#!">Exams</a></li>
					<li><a class="grey-text text-lighten-3" href="#!">Contact</a></li>
				</ul>
			</div>
			<div class="col s12 m3 l2">
				<h5 class="white-text">OTHER LINKS</h5>
				<ul>
					<li><a class="grey-text text-lighten-3" href="#!">Home</a></li>
					<li><a class="grey-text text-lighten-3" href="#!">Features</a></li>
					<li><a class="grey-text text-lighten-3" href="#!">Exams</a></li>
					<li><a class="grey-text text-lighten-3" href="#!">Contact</a></li>
				</ul>
			</div>
			<div class="col s12 m3 l4">
				<h5 class="white-text">FIND US</h5>
				<p class="grey-text text-lighten-4">C-55, 2nd Floor, Sector 2, Noida, India, 201301</p>
				<p class="grey-text text-lighten-4 mrg-top"> <i class="material-icons tiny">call</i> (+91) 888 271 5488</p>
				<p class="grey-text text-lighten-4 mrg-top"> <i class="material-icons tiny">mail</i> support@exampointer.com</p>
				<div>
					 <a href="#"class=""><i class="mdi mdi-facebook-box small orange-text"></i></a>
					 <a href="#"class=""><i class="mdi mdi-twitter-box small orange-text"></i></a>
					 <a href="#"class=""><i class="mdi mdi-google-plus-box small orange-text"></i></a>
					 <a href="#"class=""><i class="mdi mdi-pinterest-box small orange-text"></i></a>
				</div>
			</div>
		</div>

	</div>
	<div class="footer-copyright">
		<div class="container">
			© 2017 Copyright Text
			<a class="grey-text text-lighten-4 right" href="#!">
				<img src="blade/main/images/4thpointer-footer-logo.png">
			</a>
		</div>
	</div>
</footer>
