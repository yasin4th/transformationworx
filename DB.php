<?php

include __DIR__.'/vendor/Zend/Loader/AutoloaderFactory.php';
Zend\Loader\AutoloaderFactory::factory(array(
		'Zend\Loader\StandardAutoloader' => array(
				'autoregister_zf' => true,
				'db' => 'Zend\Db\Sql'
		)
));

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Adapter\Adapter;

$config = array(
	'driver' => 'Pdo_Mysql',
	'database' => 'exampointer_01032017',
	// 'database' => 'exampointer',
	'username' => 'root',
	'password' => 'GDiV74Yoez',
	// 'password' => 'ravi',
	'hostname' => 'localhost',
);
$adapter = new Adapter($config);

class DB
	{
		public $adapter;

		public function __construct() {
			$config = array(
				'driver' => 'Pdo_Mysql',
				'database' => 'exampointer_01032017',
				// 'database' => 'exampointer',
				'username' => 'root',
				'password' => 'GDiV74Yoez',
				// 'password' => 'ravi',
				'hostname' => 'localhost',
			);
			$this->adapter = new Adapter($config);
		}

		public function _insert($table, $data)
		{
			$result = 0;
			try
			{
				$adapter = $this->adapter;

				$sql = new Sql($adapter);

				$query = new TableGateway($table, $adapter, null, new HydratingResultSet());
				$query->insert($data);

				return intval($query->getLastInsertValue());

			}
			catch(Exception $e)
			{
				return $e->getMessage();
			}
		}
		public function _iou($table, $check, $data)
		{
			$result = 0;
			try
			{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();

				$select->from($table);
				foreach ($check as $key => $value)
				{
					$select->where->equalTo($key,$value);
				}

				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$count = $run->toArray();

				if(count($count))
				{

					$result = $count[0]['id'];
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table($table);
					$update->set($data);
					$update->where(array(
						'id' => $result
					));

					$selectString = $sql->getSqlStringForSqlObject($update);
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

				}
				else
				{
					$sql = new Sql($adapter);
					$query = new TableGateway($table, $adapter, null, new HydratingResultSet());
					$query->insert($data);
					$result = $query->getLastInsertValue();
				}

				return $result;
			}catch(Exception $e) {

				return $result;
			}
		}

		public function _minsert($table, array $data)
		{
			if (count($data))
			{
				$columns = (array)current($data);
				$columns = array_keys($columns);
				$columnsCount = count($columns);

				for ($i=0; $i < count($columns); $i++) {
					$columns[$i] = "`$columns[$i]`";
				}

				$columns = "(" . implode(',', $columns) . ")";
				$placeholder = array_fill(0, $columnsCount, '?');
				$placeholder = "(" . implode(',', $placeholder) . ")";
				$placeholder = implode(',', array_fill(0, count($data), $placeholder));


				$statement = "INSERT INTO $table $columns VALUES ";
				foreach ($data as $row)
				{
					$statement .= "(";
					foreach ($row as $key => $value)
					{
						$eval = htmlspecialchars($value,ENT_QUOTES);
						$statement .= "'$eval',";
						// $values[] = $value;
					}
					$statement = rtrim($statement, ',') . '),';
				}
				$statement = rtrim($statement, ',');
				// echo $statement;

				$adapter = DB::$adapter;
				$sql = new Sql($adapter);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify


				// array_filter($columns, function ($index, &$item) use ($platform) {
				//     $item = $platform->quoteIdentifier($item);
				// });
				// $columns = "(" . implode(',', $columns) . ")";

				// $placeholder = array_fill(0, $columnsCount, '?');
				// $placeholder = "(" . implode(',', $placeholder) . ")";
				// $placeholder = implode(',', array_fill(0, count($data), $placeholder));

				// $values = array();
				// foreach ($data as $row) {
				//     foreach ($row as $key => $value) {
				//         $values[] = $value;
				//     }
				// }



				// $table = $this->db->platform->quoteIdentifier($table);
				// $q = "INSERT INTO $table $columns VALUES $placeholder";
				// $this->db->query($q)->execute($values);
			}
		}
	}



	function dd($var) {
		echo "<pre>";
		print_r($var);
		echo "</pre>";
		die();
	}
	function pp($var) {
		echo "<pre>";
		print_r($var);
		echo "</pre>";
	}
	function getPassage($key, $passages) {
		foreach ($passages as $p) {
			// dd($p);
			if($p['key'] == $key) {
				return ($p['passage']=='')?0:$p['id'];
			}
		}
	}

	function getElementsByClass(&$parentNode, $tagName, $className) {
		$nodes = array();

		$childNodeList = $parentNode->getElementsByTagName($tagName);
		for ($i = 0; $i < $childNodeList->length; $i++) {
			$temp = $childNodeList->item($i);
			if (stripos($temp->getAttribute('class'), $className) !== false) {
				$nodes[]=$temp;
			}
		}

		return $nodes;
	}

	function getLabels($section, $section_id, $class, $test_paper, $db) {

		$labels = [];
		$all = [];
		foreach ($section as $key => $q) {
			if(!in_array($q['content']['EN']['passage_id'], $all)) {

				$ps_id = $q['content']['EN']['passage_id'];
				$count = 0;
				foreach ($section as $jey => $value) {
					if($value['content']['EN']['passage_id'] == $ps_id ) {
						$count ++;
					}
				}

				$mp = ($ps_id != 0) ? (intval($q['mp']) * $count):$q['mp'];

				array_push($labels, [
					'section_id'	=>$section_id,
					'old_label_id'	=>$ps_id,
					'count'			=>$count,
					'mp'			=>$mp,
					'mn'			=>$q['mn']]
				);
				array_push($all, $ps_id);
			}
		}

		if(count($labels)) {

			foreach ($labels as $key => $l) {
				$mp = ($l['old_label_id'] != 0)? (intval($l['mp'])*$l['count']):$l['mp'];
				$tpl = $db->_insert('test_paper_labels', [
					'section_id' => $section_id,
					'class_id' => $class,
					'subject_id' => 0,
					'question_type' => ($l['old_label_id']==0)?1:11,
					'number_of_question' => $l['count'],
					'max_marks' => $l['mp'],
					'neg_marks' => $l['mn']
				]);
				$labels[$key]['new_label_id'] = $tpl;
			}
		}

		return $labels;
	}

	function getLabel($labels, $id) {
		foreach ($labels as $key => $value) {
			if($value['old_label_id'] == $id) {
				return $value['new_label_id'];
			}
		}
	}

	function getLabelPlus($labels, $id) {
		foreach ($labels as $key => $value) {
			if($value['old_label_id'] == $id) {
				return $value['mp'];
			}
		}
	}
	function getLabelMinus($labels, $id) {
		foreach ($labels as $key => $value) {
			if($value['old_label_id'] == $id) {
				return $value['mn'];
			}
		}
	}
?>
