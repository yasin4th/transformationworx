<!DOCTYPE html>
<html>
<head>
	<title>ssc test</title>
	<link rel="stylesheet" type="text/css" href="assets/frontend/layout/css/launchsscstyle.css">
	<link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<div id="mainssc">
		<nav class="ssctest_nav">
		<div class="launch_logo">
			<img src="assets/global/img/exampointer-logo-colored-small.png" alt="Education" >
		</div>
		</nav>
		<header>
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-md-4">
						<p>SSC_MOCK PAPER-193 (A)</p>
				</div>
				<div class="col-sm-4 col-md-3">
				   <div class="dropdown">
					  <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">English
					  <span class="caret"></span></button>
					  <ul class="dropdown-menu">
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
					  </ul>
					</div>
				 </div>
				<div class="col-sm-4 col-md-5">
						<p class="timer">TIMER : <span>00:00</span></p>
				</div>
			</div>
		</div>
		</header>
		 <section>
		  <div class="container">
			<div class="row margt_20">
				<div class="col-sm-12 col-md-7 ques_box">
						<div class="col-sm-12 col-md-12 ques_box_nav">
							<h4>Text Size
							<a href="#"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
							<a href="#"><i class="fa fa-search-minus" aria-hidden="true"></i></a>
							<p>Quantative Aptitude</p>
							</h4>
						</div>
						<div class="col-sm-12 col-md-12 ques_box_content scroller" style="overflow-y:scroll;">
							<div class="col-sm-12 col-md-12 margt_20">
							   <p>(Mark : 2) (Negative Mark/s - 0.5) </p>
							</div>
							<div class="col-sm-12 col-md-12" id="que">
							   <h5 style="color: black;">Q26.</h5>
							</div>
							 <div class="col-sm-12 col-md-12" id="que">
							   <p style="color: black;">A trader buys goods at 20% discount on marked price. If he wants to make a profit of 25% after allowing a discount of 20%, by what percent should his marked price be greater than the original marked price? </p>
							</div>
							<div class="col-sm-12 col-md-12">
							   <p><form>
									<label class="radio">
									  <input type="radio" name="optradio">15%
									</label>
									<label class="radio">
									   <input type="radio" name="optradio">30%
									</label>
									<label class="radio">
									  <input type="radio" name="optradio">40%
									</label>
									<label class="radio">
									  <input type="radio" name="optradio">80%
									</label>
							  </form></p>
							</div>

						</div>

				</div>
				<div class="col-sm-12 col-md-5 id_box">
				   <div class="">
					<div class="col-sm-12 col-md-12 id_box_nav">
						<div class="col-sm-3 col-md-4 margt_20">
						   <div class="img_box">
							<img src="blade/main/images/rohit.jpg" class="img-responsive">
							</div>
						</div>
						<div class="col-sm-9 col-md-8">
							<div class="row">
								<div class="col-sm-12 col-md-12 margt_20">
								  <p style="font-weight: bold; margin-top: 6px;">Login ID :</p>
								  <p>rohitpratapsingh007@gmail.com</p>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 col-md-12 margt_20">
								   <p style="font-weight: bold;">Candidate Name :</p>
								  <p>ROHIT PRATAP SINGH</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-md-12 id_box_content scroller" style="overflow-y: scroll;">
                        <div class="col-sm-12 col-md-12">
							<div class="row">
								<div class="col-sm-12 col-md-12 margt_20">
								  <p style="font-weight: bold;">Number of Questions </p>
								</div>
							</div>
								<div class="row">
									<div class="col-sm-12 col-md-12 qbox">

									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									  <div class="qtag">
									     <a href="#">Q1</a>
									  </div>
									</div>
							    </div>

							<div class="row">
								<div class="col-sm-12 col-md-12 margt_20">
                                  <p style="font-weight: bold;">Legends</p>
								</div>
						    </div>
							<div class="row lbox">
								<div class="col-sm-12 col-md-7">
								      <ul>
								     	<li style="font-weight: bold;"><span class="green"></span>Attempted</li>
								     	<li style="font-weight: bold;"><span class="white"></span>Tagged</li>
								     	<li style="font-weight: bold;"><span class="blue"></span>Attempted & Tagged</li>
								     </ul>
								</div>
								<div class="col-sm-12 col-md-5">
								      <ul>
								     	<li style="font-weight: bold;"><span class="yellow"></span>Unattempted</li>
								     	<li style="font-weight: bold;"><span class="violet"></span>Unanswered</li>

								     </ul>
								</div>
						    </div>
					</div>
				  </div>
				</div>
			</div>
		  </div>
		 </section>

		<footer class="margt_20">
		 <div class="container" >
		 <div class="row" id="botn">
			<div class="col-sm-3 col-md-2">
				<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Previous Question
				</button>
			</div>
			<div class="col-sm-3 col-md-2">
				<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Next Question
				</button>
			</div>

			<div class="col-sm-1 col-md-1">
				 <button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown">Tag
				</button>
			</div>
			<div class="col-sm-1 col-md-1">
				 <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown">Erase
				</button>
			</div>
			<div class="col-sm-1 col-md-2">
				 <button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown">Previous section
				</button>
			</div>
			<div class="col-sm-1 col-md-2">
				 <button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown">Next section
				</button>
			</div>
			<div class="col-sm-2 col-md-2">
				  <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">Sublime Text
				</button>
			</div>
		  </div>
		  </div>
		</footer>
	</div>
</body>
</html>



