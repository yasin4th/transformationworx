<?php
	include 'config.inc.test.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Effort Education</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Loading materilize -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="blade/main/css/style.css" rel="stylesheet">
	<link href="blade/main/css/materialize.min.css" rel="stylesheet">
	<link href="blade/main/css/materialdesignicons.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="images/favicon.ico">
	<link href="blade/main/fa/css/font-awesome.css" rel="stylesheet">
</head>

<body>

	<!-- Preloader -->
<!-- <div id="preloader">
	<div id="status"></div>
</div> -->
<?php
	include HEADER;
?>

		<div class="main">
			<div class="container">
				<div class="row contact_us">
					<!-- BEGIN CONTENT -->
					<div class="col s12">
					 <h2 class="text-center">Contact <strong class="blue-text">us</strong></h2>
						<div class="content-page">
							<div class="row ">
								<!--<div class="col-md-12">
									<div id="map" class="gmaps margin-bottom-40" style="height:400px;"></div>
								</div>-->


								<div class="col s12 m12 l6 sidebar2">
							<!-- <h2>Our Contacts</h2>   -->
									<div class="wpb_wrapper">

									 <ul class="contact-info">
											<li>
											 <a class="btn-floating btn-large waves-effect waves-light "><i class="material-icons">home</i></a>
											 <address><a href="addressto:">D-44, First Floor, Sector 6, Noida, India, 201301</a></address>
											</li>
											<li>
											<a class="btn-floating btn-large waves-effect waves-light "><i class="material-icons">mail</i></a>
											<address><a href="mailto:">support@exampointer.com</a></address>
											</li>
											<li>
											<a class="btn-floating btn-large waves-effect waves-light "><i class="material-icons">phone</i></a>
											<address><a href="#">+91-8882715488, 8750115488</a></address>
											</li>
											<li>
											<a class="btn-floating btn-large waves-effect waves-light "><i class="material-icons">language</i></a>
											<address><a href="#">www.exampointer.com</a></address>
											</li>
											</ul>
									</div>
									<!-- <div> -->
										 <!--  <ul class="social-icons margin-bottom-40">
												<li><a href="http://www.facebook.com/4thPointer" target="_blank" data-original-title="facebook" class="facebook"></a></li>
												<li><a href="https://twitter.com/#!/4thPointer" target="_blank" data-original-title="twitter" class="twitter"></a></li>
												<li><a href="https://www.linkedin.com/company/4thpointer-services?trk=tyah&trkInfo=clickedVertical%3Acompany%2Cidx%3A2-1-2%2CtarId%3A1431552111347%2Ctas%3A4thpointer" target="_blank" data-original-title="linkedin" class="linkedin"></a></li>
											</ul> -->

									<!-- </div> -->
								</div>


								<div class="col s12 m12 l6 contact-matter">
												<div class="row">
													<form class="col s12 center">
														<div class="row">
															<div class="input-field col s8 m10 l6">
																<i class="material-icons prefix">account_circle</i>
																<input id="icon_prefix" type="text" class="validate">
																<label for="icon_prefix">First Name</label>
															</div>
															<div class="input-field col s8 m10 l6">
																<i class="material-icons prefix">mail</i>
																<input id="icon_telephone" type="tel" class="validate">
																<label for="icon_telephone">E-mail</label>
															</div>
														 </div>
															<div class="row">
															 <div class="input-field col s8 m10 l6">
																	<i class="material-icons prefix">subject</i>
																<input id="icon_prefix" type="text" class="validate">
																<label for="icon_prefix">Subject</label>
															</div>
															<div class="input-field col s8 m10 l6">
																	 <i class="material-icons prefix">phone</i>
																<input id="icon_telephone" type="tel" class="validate">
																<label for="icon_telephone">Telephone</label>
															</div>
															</div>
															<div class="row">
																<div class="input-field col s8 m10 l12">
																	<i class="material-icons prefix">mode_edit</i>
																	<textarea id="icon_prefix2" class="materialize-textarea"></textarea>
																	<label for="icon_prefix2">Message</label>
																</div>
															</div>

													<div class="row">
													 <div class="file-field col s8 m10 l12">
														<div class="btn orange">
															<span>File</span>
															<input type="file" multiple>
														</div>
														<div class="file-path-wrapper">
															<input class="file-path validate" type="text" placeholder="Upload one or more files">
														</div>
													</div>
												 </div>
													</form>
													<div class="row botn center">
													<a class="waves-effect waves-light btn grey">Submit</a>
													<a class="waves-effect waves-light btn orange">Cancel</a>
													</div>
												</div>
										 </div>
						</div>
					</div>
					<!-- END CONTENT -->
				</div>
			</div>
			<!-- <div class="container-fluid">
				<div class="row">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3505.2238374785306!2d77.20602399999999!3d28.532991!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce202d3d62fb3%3A0x829f3a2562ca785f!2sAIETS!5e0!3m2!1sen!2sin!4v1429957439769" width="100%" height="450" frameborder="0" style="border:0"></iframe>
				</div>
			</div> -->
		</div>



	<!--Footer-->
<?php
	include FOOTER;
?>

<script type="text/javascript" src="blade/main/js/jquery.js"></script>
<script type="text/javascript" src="blade/main/js/materialize.min.js"></script>




</body>
<!-- END BODY -->
</html>