<?php
//zeroinfy
require('../../config.php');
require_once('../../course/lib.php');
require_once('../classicpay/classes/forms/paymentinit.php');
require_login();

$id = required_param('id',PARAM_INT); // Course id.

$course = get_course($id);
$user_billing_info = $DB->get_record('user_billing_address',array('userid'=>$USER->id));

$teacher = $DB->get_record('user',array('id'=>$course->userid));
$teacher_name = fullname($teacher);

// Merchant key here as provided by Payu
// $MERCHANT_KEY = "OygoFs";
$MERCHANT_KEY = "oQtvor";

// Merchant Salt as provided by Payu
// $SALT = "BV1QBwCv";
$SALT = "FGtjI9vl";

// End point - change to https://secure.payu.in for LIVE mode https://test.payu.in
$PAYU_BASE_URL = "https://secure.payu.in";


$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);

// // Hash Sequence

$data['userid'] = $USER->id;
$data['address1'] = $user_billing_info->address1;
$data['address2'] = $user_billing_info->address1;
$data['city'] = $user_billing_info->city;
$data['state'] = $user_billing_info->state;
$data['zipcode'] = $user_billing_info->zipcode;
$data['phone'] = $user_billing_info->phone;

if(empty($user_billing_info)){
$sql= $DB->insert_record('user_billing_address',$data);
}

else{
$data['id'] = $user_billing_info->id;
$DB->update_record('user_billing_address',$data);
}

/*********************************************/

if(isset($_SESSION['discount']) && isset($_SESSION['discountamt']) && isset($_SESSION['amount']))
{
$hash_string = $MERCHANT_KEY."|".$txnid."|".$_SESSION['amount']."|".$course->fullname.' instructed by '.$teacher_name."|".$USER->firstname.' '.$USER->lastname."|".$USER->email."|".
			$course->id."|".$_SESSION['discount']."|".$course->price."|".$_SESSION['discountamt']."|||||||".$SALT;
}
else
{
$hash_string = $MERCHANT_KEY."|".$txnid."|".$course->price."|".$course->fullname.' instructed by '.$teacher_name."|".$USER->firstname.' '.$USER->lastname."|".$USER->email."|".
				$course->id."|0|".$course->price."|0|||||||".$SALT;
}

$hash = strtolower(hash('sha512', $hash_string));
$action = $PAYU_BASE_URL . '/_payment';


echo $OUTPUT->header(); 

?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
  	</head>

  <body>
    <h4 class='title'>Order Details</h4>

	<div id='payu_course_info'>
		<table>
			<tr>
			<td class='hd' colspan='2'>Course Information</td>
			</tr>
			<tr></tr>

			<tr>
			<td><b>Course Name</b></td>
			<td><?php echo ": ".$course->fullname ?></td>
			</tr>

			<tr>
			<td><b>Instructor Name</b></td>
			<td><?php echo ": ".$teacher_name ?></td>
			</tr>

			<tr>
			<td><b>Course Fee</b></td>
			<td id='enrol-classicpay-basecost'><?php echo ": Rs. ".$course->price ?></td>
			</tr>

			<tr>
			<td colspan='2'>
				<p id="enrol-classicpay-coupondiscount"></p>
			</td>
			</tr>

			<tr>
			<td colspan='2'>
			<?php 
				$PAGE->requires->js('/enrol/classicpay/js/coupon.js');
			?>

			<input type='text' name='coupon' id='coupon' placeholder='Enter Discount Code'>
			<input type='hidden' name='courseid' id='courseid' value='<?php echo $course->id ?>'>
			<input type='button' id='btncheckcoupon' value='Apply Code'>
			
			</td>
			</tr>
		</table>
	</div>



<div id ='pay_u_form' >    

	<form id="idform" action="<?php echo $action ?>" method="post">

	  <input type="hidden" name="firstname" value="<?php echo $USER->firstname.' '.$USER->lastname ?>" />
      <input type="hidden" name="email" value="<?php echo $USER->email ?>" />
	  <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
	  <input type="hidden" name="udf1" value="<?php echo $course->id ?>" />
      <input type="hidden" name="udf3" value="<?php echo $course->price ?>" />
      <input type="hidden" name="udf2" id='discount' value="<?php echo (isset($_SESSION['discount'])) ? $_SESSION['discount'] : 0 ?>"/>
      <input type="hidden" name="udf4" id='discountamt' value="<?php echo (isset($_SESSION['discountamt'])) ? $_SESSION['discountamt'] : 0?>" />
      <input type="hidden" name="ctype"  id="ctype" value="<?php echo $course->ctype ?>" />
      <input type="hidden" id='amount' name="amount" value="<?php echo (isset($_SESSION['amount'])) ? $_SESSION['amount'] : $course->price ?>" />
      <input type="hidden" name="productinfo" value="<?php echo $course->fullname.' instructed by '.$teacher_name ?>" />
      <input type="hidden" name="surl" value="<?php echo $CFG->wwwroot.'/enrol/payumoney/success.php'; ?>" />
      <input type="hidden" name="furl" value="<?php echo $CFG->wwwroot.'/enrol/payumoney/failure.php'; ?>" />

	 <table>
			<tr>
			<td colspan='2' class='hd'>Personal Information</td>
			</tr>

	     	<tr>
			<?php if($formError) { ?>
	     		 <tr style="color:red"><td colspan='2'>Please fill all mandatory fields.</td></tr>
	   		<?php } ?>
			<td style='width:40%;'></td>
        	</tr>

	        <tr>
	          <td>Name: </td>
	          <td><input name="firstname" readonly id="firstname" value="<?php echo $USER->firstname.' '.$USER->lastname ?>" required/></td>
	        </tr>

	        <tr>
	          <td>Email: </td>
	          <td><input name="email" readonly id="email" value="<?php echo $USER->email; ?>" required/></td>
	        </tr>

			<tr>
	          <td>Address1: </td>
	          <td><input id="address1" name="address1" value="<?php echo $user_billing_info->address1 ?>" required/></td>
			</tr>

			<tr>
	          <td>Address2: </td>
	          <td><input name="address2" value="<?php echo $user_billing_info->address2 ?>" required/></td>
	        </tr>

	        <tr>
	          <td>City: </td>
	          <td><input name="city"  value="<?php echo $user_billing_info->city ?>" required/></td>
			</tr>

			<tr>
			<td>Zipcode: </td>
	          <td><input name="zipcode" value="<?php echo $user_billing_info->zipcode ?>" required/></td>
	        </tr>

			<tr>
	          <td>State: </td>
	          <td><input name="state" value="<?php echo $user_billing_info->state ?>" required/></td>
	        </tr>

			<tr>
			<td>Phone: </td>
	          <td><input name="phone" value="<?php echo $user_billing_info->phone ?>" required/></td>
			</tr>
		<?php 
    // if($course->ctype == 3){ ?>
			<tr>
			<td>Payment Option: </td>
	          <td>

				<input type="radio" id="r1" name="payment_option" class="payment_radios" value="1" checked> Pay Now by PayUMoney
				<br>
		      	<input type="radio" id="r3" name="payment_option" class="payment_radios" value="3"> Pay Now by Razorpay
		      	<br>
				<input type="radio" id="r2" name="payment_option" class="payment_radios" value="2"> Cash On Delivery </td>
			</tr>
		<?php 
  // } ?>
			<tr>
				<td colspan='2'>
				<span style='color:red'> *All the above fields are mandatory</span>
				</td>
		        <tr>
		          <td colspan="4"><input type="hidden" name="service_provider" value="payu_paisa" size="64" /></td>
		        </tr>
		    </tr>

	        <tr>
	          <?php //if(!$hash) { 
			  ?>
	            <td colspan="3"><input type="submit" id="pay_now" value="Pay Now" /></td>
	          <?php //} ?>
	        </tr>

      </table>

    </form>

      <table>
      		<tr>
	        	<td colspan="3">
					<form action="RazorpaySuccess.php" method="POST">
					<!-- Note that the amount is in paise = 50 INR -->
					<script
					    src="https://checkout.razorpay.com/v1/checkout.js"
					    data-key="rzp_test_C8O2kWUMb82OiO"
					    data-amount="<?php echo (isset($_SESSION['amount'])) ? $_SESSION['amount']*100 : $course->price*100 ?>"
					    data-buttontext="Pay with Razorpay"
					    data-name="Zeroinfy"
					    data-description="<?php echo $course->fullname.' instructed by '.$teacher_name; ?>"
					    data-image="https://your-awesome-site.com/your_logo.jpg"
					    data-prefill.name="<?php echo $USER->firstname.' '.$USER->lastname; ?>"
					    data-prefill.email="<?php echo $USER->email; ?>"
					    data-prefill.contact="<?php echo $user_billing_info->phone; ?>"
					    data-notes.status = "success"
					    data-notes.udf1 = "<?php echo $course->id; ?>"
					    data-notes.udf2 = "<?php echo (isset($_SESSION['discount'])) ? $_SESSION['discount'] : 0?>"					    
					    data-notes.udf3 = "<?php echo $course->price; ?>"
					    data-notes.udf4 = "<?php echo (isset($_SESSION['discountamt'])) ? $_SESSION['discountamt'] : 0?>"		
					    data-theme.color="#F37254"
					></script>
					<input type="hidden" value="Hidden Element" name="hidden">
					</form>
				</td>
	        </tr>
      </table>

</div>

</body>
</html>
<?php echo $OUTPUT->footer(); ?>

     <script>
      $(document).ready(function(){
      	$(".razorpay-payment-button").hide();

      	$(".payment_radios").on("click",function(){

      		if($("#r3").is(':checked'))
	      	{
	      		$(".razorpay-payment-button").show();
	      		$("#pay_now").hide();
	      	}
	      	else if($("#r2").is(':checked'))
	      	{
	      		$("#idform").attr("action","<?php echo $CFG->wwwroot.'/enrol/payumoney/CODsuccess.php' ?>");
	      		$(".razorpay-payment-button").hide();
	      		$("#pay_now").show();
	      	}
	      	else
	      	{
	      		$("#idform").attr("action","<?php echo $PAYU_BASE_URL . '/_payment' ?>");
	      		$(".razorpay-payment-button").hide();	
	      		$("#pay_now").show();
	      	}

      	});

      	//Handling the discount html or error in the coupon code handling
      	var discounthtml = "<?php echo (isset($_SESSION['discounthtml'])) ? $_SESSION['discounthtml'] : ''?>";
      	var discounterror = "<?php echo (isset($_SESSION['discounterror'])) ? $_SESSION['discounterror'] : ''?>";
      	
      	if(discounthtml)
      	{
	      	$('#enrol-classicpay-coupondiscount').html(discounthtml);
	      	$('#enrol-classicpay-coupondiscount').removeClass('error');
	      	$('#enrol-classicpay-basecost').addClass('enrol-classicpay-strike');
      	}
      	else if(discounterror)
      	{
      		 $('#enrol-classicpay-coupondiscount').html(discounterror);
      		 $('#enrol-classicpay-coupondiscount').addClass('error');
      		 $('#enrol-classicpay-basecost').removeClass('enrol-classicpay-strike');
      	}

      });

      
      </script>

<?php 
unset($_SESSION['discount']);  
unset($_SESSION['discountamt']);  
unset($_SESSION['amount']); 
unset($_SESSION['discounthtml']);
unset($_SESSION['discounterror']);
?>
