<?php include 'Access-API.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li><a href="#">Student</a></li>
            <li class="active">Question Bank</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          <div class="sidebar col-md-2 col-sm-3">
            <?php include('html/student/sidebar.php'); ?>
          </div>
          <!-- END SIDEBAR -->

          <!-- BEGIN CONTENT -->
          <div class="col-md-10 col-sm-9">
			<div class="row">
				<div class="col-md-6 col-sm-6">
        <h1>View Question</h1>
        </div>
        <div class="col-md-6 col-sm-6"></div>
      </div>
      <div class="row margin-bottom-20">
        
        <div class="col-md-10 form-group"> 
          <span style="font-size: 22px;">Question:</span>
          <div id="question" class="col-md-12">
            Question will Place HEre
          </div>
        </div>
      
        <div class="col-md-10 form-group"> 
          <span style="font-size: 22px;">Options:</span>
          <div class="col-md-12">
            <ol id="option">
              <li>
                sdsdfsd
              </li>
              <li>
                sdsdfsd
              </li>
              <li>
                sdsdfsd
              </li>
              <li>
                sdsdfsd
              </li>
            </ol>
          </div>
        </div>
      

        <div class="col-md-10 form-group"> 
          <span style="font-size: 22px;">Solution:</span>
          <div id="solution" class="col-md-12">
            Solution will place here....
          </div>
        </div>

      </div>
			<div class="row">
				
		  </div>
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
    <!-- END FOOTER -->
	
	<!-- /modal -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
	 <?php include('html/student/js-files.php'); ?>

    <script src="assets/js/custom/view-question.js" type="text/javascript"></script>

    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>