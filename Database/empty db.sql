-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Feb 06, 2016 at 12:37 PM
-- Server version: 5.5.45-37.4-log
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `evnxt_aietsStaging`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers_of_question_attempts`
--

CREATE TABLE IF NOT EXISTS `answers_of_question_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_attempt_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

CREATE TABLE IF NOT EXISTS `chapters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(150) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` varchar(25) NOT NULL,
  `created` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE IF NOT EXISTS `classes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(110) NOT NULL,
  `test_program_id` int(11) NOT NULL,
  `institute_id` int(11) NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL DEFAULT '0',
  `expiry_date` int(25) NOT NULL,
  `created` int(25) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `difficulty`
--

CREATE TABLE IF NOT EXISTS `difficulty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `difficulty` varchar(200) NOT NULL,
  `description` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option` text NOT NULL,
  `answer` tinyint(1) NOT NULL DEFAULT '0',
  `number` int(11) NOT NULL DEFAULT '0',
  `question_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `planner`
--

CREATE TABLE IF NOT EXISTS `planner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(2) NOT NULL,
  `subject_id` int(2) NOT NULL,
  `startDate` varchar(1000) NOT NULL,
  `targetDate` varchar(1000) NOT NULL,
  `offDays` varchar(1000) NOT NULL,
  `schoolHolidays` varchar(1000) NOT NULL,
  `dailyOrAlternate` varchar(1000) NOT NULL,
  `schoolDaysSlotsFrom` varchar(1000) NOT NULL,
  `schoolDaysSlotsTo` varchar(1000) NOT NULL,
  `schoolHolidaysSlotsFrom` varchar(1000) NOT NULL,
  `schoolHolidaysSlotsTo` varchar(1000) NOT NULL,
  `rectimes` varchar(1000) NOT NULL,
  `chapters` varchar(2000) NOT NULL,
  `alloted` varchar(1000) NOT NULL,
  `lc` varchar(1000) NOT NULL,
  `pq` varchar(1000) NOT NULL,
  `tc` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `class` varchar(55) NOT NULL,
  `registered_on` int(11) NOT NULL,
  `interests` varchar(500) NOT NULL,
  `occupation` varchar(100) NOT NULL,
  `about` varchar(500) NOT NULL,
  `website_url` varchar(500) NOT NULL,
  `address` text NOT NULL,
  `image` varchar(150) NOT NULL DEFAULT 'ProfilePictures/default.png',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `qcr_illustration`
--

CREATE TABLE IF NOT EXISTS `qcr_illustration` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `test_program_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `source` varchar(500) NOT NULL,
  `created` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL DEFAULT '0',
  `question` text NOT NULL,
  `description` text NOT NULL,
  `type` int(11) NOT NULL,
  `quality_check` tinyint(1) NOT NULL DEFAULT '0',
  `ideal_time` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `question_attempt`
--

CREATE TABLE IF NOT EXISTS `question_attempt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attempt_id` int(11) NOT NULL DEFAULT '0',
  `question_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL DEFAULT '0',
  `question_type` int(11) NOT NULL DEFAULT '0',
  `question_status` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  `answer` int(11) NOT NULL DEFAULT '0',
  `correct` int(11) NOT NULL DEFAULT '0',
  `marks` int(11) NOT NULL DEFAULT '0',
  `neg_marks` int(11) NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `question_details`
--
CREATE TABLE IF NOT EXISTS `question_details` (
`question_id` int(11)
,`data` text
,`description` text
,`type` int(11)
,`quality_check` tinyint(1)
,`parent_id` int(11)
,`modified_at` varchar(17)
,`deleted` int(11)
,`class_name` varchar(150)
,`subject_name` text
,`unit_name` text
,`chapter_name` text
,`topic_name` text
,`tag_name` text
,`difficulty_name` text
,`skill_name` text
);
-- --------------------------------------------------------

--
-- Table structure for table `question_skills`
--

CREATE TABLE IF NOT EXISTS `question_skills` (
  `question_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question_types`
--

CREATE TABLE IF NOT EXISTS `question_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_type` varchar(50) NOT NULL,
  `short_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `question_types`
--

INSERT INTO `question_types` (`id`, `question_type`, `short_name`) VALUES
(1, 'Single Choice Questions', 'scq'),
(2, 'Multiple Choice Questions', 'mcq'),
(4, 'Sequencing Type Questions', 'secquencing'),
(5, 'Matching (Drag n Drop)', 'matching'),
(6, 'Fill IN THE Blanks', 'fb'),
(7, 'Fill in the Blanks (Drop Down)', 'fb (dd)'),
(8, 'Fill in the Blanks (Drag n Drop)', 'fb (dnd)'),
(10, 'Descriptive', 'descriptive'),
(11, 'Passage', 'passage'),
(12, 'Matrix-Multiple Matching', 'matrix'),
(13, 'Integer Answer Questions', 'integer');

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE IF NOT EXISTS `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT 'untitled',
  `total_question` int(11) NOT NULL DEFAULT '0',
  `cut_off_marks` int(11) NOT NULL DEFAULT '0',
  `test_paper` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill` varchar(200) NOT NULL,
  `description` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `student_program`
--

CREATE TABLE IF NOT EXISTS `student_program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(5) NOT NULL,
  `test_program_id` int(5) NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `student_question_bank`
--

CREATE TABLE IF NOT EXISTS `student_question_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `revision` int(11) NOT NULL DEFAULT '0',
  `important` int(11) NOT NULL DEFAULT '0',
  `unable_to_solve` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(150) NOT NULL,
  `class_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` text NOT NULL,
  `description` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tagsofquestion`
--

CREATE TABLE IF NOT EXISTS `tagsofquestion` (
  `question_id` int(11) NOT NULL,
  `tagged_id` int(11) NOT NULL,
  `tag_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags_of_test_paper_label`
--

CREATE TABLE IF NOT EXISTS `tags_of_test_paper_label` (
  `test_paper_label_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tag_type`
--

CREATE TABLE IF NOT EXISTS `tag_type` (
  `tag_code` int(11) NOT NULL AUTO_INCREMENT,
  `tag_data` varchar(50) NOT NULL,
  PRIMARY KEY (`tag_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tag_type`
--

INSERT INTO `tag_type` (`tag_code`, `tag_data`) VALUES
(1, 'subject'),
(2, 'unit'),
(3, 'chapter'),
(4, 'topic'),
(5, 'tag'),
(6, 'difficyulty-level'),
(7, 'skill');

-- --------------------------------------------------------

--
-- Table structure for table `test_paper`
--

CREATE TABLE IF NOT EXISTS `test_paper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `type` int(50) NOT NULL DEFAULT '0',
  `class` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  `total_attempts` int(11) NOT NULL DEFAULT '0',
  `start_time` varchar(200) NOT NULL DEFAULT '0',
  `end_time` varchar(200) NOT NULL DEFAULT '0',
  `Instructions` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `mode` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test_papers_of_test_program`
--

CREATE TABLE IF NOT EXISTS `test_papers_of_test_program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_paper_id` int(11) NOT NULL,
  `test_program_id` int(11) NOT NULL,
  `test_paper_type_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `parent_type` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test_paper_attempts`
--

CREATE TABLE IF NOT EXISTS `test_paper_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_paper_id` int(11) NOT NULL,
  `marks_archived` int(11) NOT NULL DEFAULT '0',
  `total_marks` int(11) NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL,
  `start_time` int(11) NOT NULL DEFAULT '0',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `optimizer_id` int(11) NOT NULL DEFAULT '0',
  `complete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test_paper_labels`
--

CREATE TABLE IF NOT EXISTS `test_paper_labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL DEFAULT '0',
  `chapter_id` int(11) NOT NULL DEFAULT '0',
  `topic_id` int(11) NOT NULL DEFAULT '0',
  `question_type` int(11) NOT NULL,
  `number_of_question` int(11) NOT NULL DEFAULT '1',
  `difficulty_level` int(11) NOT NULL DEFAULT '0',
  `skill_id` int(11) NOT NULL DEFAULT '0',
  `max_marks` int(11) NOT NULL DEFAULT '1',
  `neg_marks` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test_paper_questions`
--

CREATE TABLE IF NOT EXISTS `test_paper_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_paper_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `lable_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `max_marks` int(11) NOT NULL DEFAULT '1',
  `neg_marks` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test_paper_subjects`
--

CREATE TABLE IF NOT EXISTS `test_paper_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_paper_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test_paper_type`
--

CREATE TABLE IF NOT EXISTS `test_paper_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `test_paper_type`
--

INSERT INTO `test_paper_type` (`id`, `type`) VALUES
(1, 'Diagnostic Test'),
(3, 'Chapter Test'),
(4, 'Full Syllabus Test'),
(5, 'Scheduled Test'),
(6, 'Practice Test');

-- --------------------------------------------------------

--
-- Table structure for table `test_program`
--

CREATE TABLE IF NOT EXISTS `test_program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `class` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `cut_off` int(11) NOT NULL DEFAULT '60',
  `description` text NOT NULL,
  `image` varchar(500) NOT NULL DEFAULT 'assets/frontend/pages/img/test_program/default.jpg',
  `created` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test_program_subjects`
--

CREATE TABLE IF NOT EXISTS `test_program_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_program_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `time_takers_report`
--
CREATE TABLE IF NOT EXISTS `time_takers_report` (
`attempt_id` int(11)
,`question_id` int(11)
,`question_type` int(11)
,`question_status` int(11)
,`time` int(11)
,`answer` int(11)
,`correct` int(11)
,`marks` int(11)
,`neg_marks` int(11)
,`student_id` int(11)
,`ideal_time` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE IF NOT EXISTS `topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(150) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE IF NOT EXISTS `units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(150) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` int(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(150) NOT NULL,
  `password` varchar(32) NOT NULL,
  `registered_on` int(11) NOT NULL DEFAULT '0',
  `last_login` varchar(50) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `role` int(2) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `registered_on`, `last_login`, `banned`, `role`, `deleted`) VALUES
(1, 'Admin', '', 'admin', '21232f297a57a5a743894a0e4a801fc3', 0, '1454762170', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `role`) VALUES
(1, 'Super Admin'),
(2, 'SME'),
(3, 'DTP'),
(4, 'Student');

-- --------------------------------------------------------

--
-- Structure for view `question_details`
--
DROP TABLE IF EXISTS `question_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`evnxt`@`localhost` SQL SECURITY DEFINER VIEW `question_details` AS select `questions`.`id` AS `question_id`,`questions`.`question` AS `data`,`questions`.`description` AS `description`,`questions`.`type` AS `type`,`questions`.`quality_check` AS `quality_check`,`questions`.`parent_id` AS `parent_id`,date_format(from_unixtime(`questions`.`created`),'%d-%m-%y %h:%i %p') AS `modified_at`,`questions`.`delete` AS `deleted`,`classes`.`name` AS `class_name`,group_concat(if((`tagsofquestion`.`tag_type` = 1),`subjects`.`name`,' ') separator ',') AS `subject_name`,group_concat(if((`tagsofquestion`.`tag_type` = 2),`units`.`name`,' ') separator ',') AS `unit_name`,group_concat(if((`tagsofquestion`.`tag_type` = 3),`chapters`.`name`,' ') separator ',') AS `chapter_name`,group_concat(if((`tagsofquestion`.`tag_type` = 4),`topics`.`name`,' ') separator ',') AS `topic_name`,group_concat(if((`tagsofquestion`.`tag_type` = 5),`tags`.`tag`,' ') separator ',') AS `tag_name`,group_concat(if((`tagsofquestion`.`tag_type` = 6),`difficulty`.`difficulty`,' ') separator ',') AS `difficulty_name`,group_concat(if((`tagsofquestion`.`tag_type` = 7),`skills`.`skill`,' ') separator ',') AS `skill_name` from (((((((((`questions` left join `tagsofquestion` on((`tagsofquestion`.`question_id` = `questions`.`id`))) left join `classes` on((`classes`.`id` = `questions`.`class_id`))) left join `subjects` on((`subjects`.`id` = `tagsofquestion`.`tagged_id`))) left join `topics` on((`topics`.`id` = `tagsofquestion`.`tagged_id`))) left join `chapters` on((`chapters`.`id` = `tagsofquestion`.`tagged_id`))) left join `units` on((`units`.`id` = `tagsofquestion`.`tagged_id`))) left join `tags` on((`tags`.`id` = `tagsofquestion`.`tagged_id`))) left join `difficulty` on((`difficulty`.`id` = `tagsofquestion`.`tagged_id`))) left join `skills` on((`skills`.`id` = `tagsofquestion`.`tagged_id`))) group by `questions`.`id`;

-- --------------------------------------------------------

--
-- Structure for view `time_takers_report`
--
DROP TABLE IF EXISTS `time_takers_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`evnxt`@`localhost` SQL SECURITY DEFINER VIEW `time_takers_report` AS select `question_attempt`.`attempt_id` AS `attempt_id`,`question_attempt`.`question_id` AS `question_id`,`question_attempt`.`question_type` AS `question_type`,`question_attempt`.`question_status` AS `question_status`,`question_attempt`.`time` AS `time`,`question_attempt`.`answer` AS `answer`,`question_attempt`.`correct` AS `correct`,`question_attempt`.`marks` AS `marks`,`question_attempt`.`neg_marks` AS `neg_marks`,`question_attempt`.`student_id` AS `student_id`,`questions`.`ideal_time` AS `ideal_time` from (`question_attempt` left join `questions` on((`question_attempt`.`question_id` = `questions`.`id`)));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
