

DROP TABLE IF EXISTS `answers_of_question_attempts`;
CREATE TABLE IF NOT EXISTS `answers_of_question_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_attempt_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_id` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;


DROP TABLE IF EXISTS `chapters`;
CREATE TABLE IF NOT EXISTS `chapters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(150) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` varchar(25) NOT NULL,
  `created` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
CREATE TABLE IF NOT EXISTS `classes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(110) DEFAULT NULL,
  `test_program_id` int(11) NOT NULL,
  `institute_id` int(11) NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL DEFAULT '0',
  `expiry_date` int(25) DEFAULT NULL,
  `created` int(25) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `difficulty`
--

DROP TABLE IF EXISTS `difficulty`;
CREATE TABLE IF NOT EXISTS `difficulty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `difficulty` varchar(200) NOT NULL,
  `description` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

DROP TABLE IF EXISTS `discount`;
CREATE TABLE IF NOT EXISTS `discount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL DEFAULT 'percent',
  `value` int(11) NOT NULL,
  `min_price` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `expiry` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `description` text NOT NULL,
  `created` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `institute`
--

DROP TABLE IF EXISTS `institute`;
CREATE TABLE IF NOT EXISTS `institute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `institute` int(11) NOT NULL,
  `website` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL DEFAULT 'assets/instituteimages/logo/default.png',
  `banner` varchar(255) NOT NULL DEFAULT 'assets/instituteimages/banners/default.jpg',
  `features` text NOT NULL,
  `about` text NOT NULL,
  `director` text NOT NULL,
  `courses` text NOT NULL,
  `contact` text NOT NULL,
  `last_active` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
CREATE TABLE IF NOT EXISTS `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option` text NOT NULL,
  `answer` tinyint(1) NOT NULL DEFAULT '0',
  `number` int(11) NOT NULL DEFAULT '0',
  `question_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `planner`
--

DROP TABLE IF EXISTS `planner`;
CREATE TABLE IF NOT EXISTS `planner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `startDate` varchar(1000) NOT NULL,
  `targetDate` varchar(1000) NOT NULL,
  `offDays` varchar(1000) NOT NULL,
  `schoolHolidays` varchar(1000) NOT NULL,
  `dailyOrAlternate` varchar(1000) NOT NULL,
  `schoolDaysSlotsFrom` varchar(1000) NOT NULL,
  `schoolDaysSlotsTo` varchar(1000) NOT NULL,
  `schoolHolidaysSlotsFrom` varchar(1000) NOT NULL,
  `schoolHolidaysSlotsTo` varchar(1000) NOT NULL,
  `rectimes` varchar(1000) NOT NULL,
  `chapters` varchar(2000) NOT NULL,
  `alloted` varchar(1000) NOT NULL,
  `lc` varchar(1000) NOT NULL,
  `pq` varchar(1000) NOT NULL,
  `tr` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `class` varchar(55) NOT NULL,
  `registered_on` int(11) NOT NULL,
  `interests` varchar(500) NOT NULL,
  `occupation` varchar(100) NOT NULL,
  `about` varchar(500) NOT NULL,
  `website_url` varchar(500) NOT NULL,
  `address` text NOT NULL,
  `image` varchar(150) NOT NULL DEFAULT 'profilePictures/default.png',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `qcr_illustration`
--

DROP TABLE IF EXISTS `qcr_illustration`;
CREATE TABLE IF NOT EXISTS `qcr_illustration` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `test_program_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `source` varchar(500) NOT NULL,
  `created` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL DEFAULT '0',
  `question` text NOT NULL,
  `description` text NOT NULL,
  `type` int(11) NOT NULL,
  `quality_check` tinyint(1) NOT NULL DEFAULT '0',
  `ideal_time` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `question_attempt`
--

DROP TABLE IF EXISTS `question_attempt`;
CREATE TABLE IF NOT EXISTS `question_attempt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attempt_id` int(11) NOT NULL DEFAULT '0',
  `question_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL DEFAULT '0',
  `question_type` int(11) NOT NULL DEFAULT '0',
  `question_status` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  `answer` int(11) NOT NULL DEFAULT '0',
  `correct` int(11) NOT NULL DEFAULT '0',
  `marks` int(11) NOT NULL DEFAULT '0',
  `neg_marks` int(11) NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `question_skills`
--

DROP TABLE IF EXISTS `question_skills`;
CREATE TABLE IF NOT EXISTS `question_skills` (
  `question_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question_types`
--

DROP TABLE IF EXISTS `question_types`;
CREATE TABLE IF NOT EXISTS `question_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_type` varchar(50) NOT NULL,
  `short_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
CREATE TABLE IF NOT EXISTS `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT 'untitled',
  `total_question` int(11) NOT NULL DEFAULT '0',
  `cut_off_marks` int(11) NOT NULL DEFAULT '0',
  `test_paper` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

DROP TABLE IF EXISTS `skills`;
CREATE TABLE IF NOT EXISTS `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill` varchar(200) NOT NULL,
  `description` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `student_program`
--

DROP TABLE IF EXISTS `student_program`;
CREATE TABLE IF NOT EXISTS `student_program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(5) DEFAULT '0',
  `test_program_id` int(5) DEFAULT '0',
  `time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `student_question_bank`
--

DROP TABLE IF EXISTS `student_question_bank`;
CREATE TABLE IF NOT EXISTS `student_question_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `revision` int(11) NOT NULL DEFAULT '0',
  `important` int(11) NOT NULL DEFAULT '0',
  `unable_to_solve` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
CREATE TABLE IF NOT EXISTS `subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(150) NOT NULL,
  `class_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` text NOT NULL,
  `description` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tagsofquestion`
--

DROP TABLE IF EXISTS `tagsofquestion`;
CREATE TABLE IF NOT EXISTS `tagsofquestion` (
  `question_id` int(11) NOT NULL,
  `tagged_id` int(11) NOT NULL,
  `tag_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags_of_test_paper_label`
--

DROP TABLE IF EXISTS `tags_of_test_paper_label`;
CREATE TABLE IF NOT EXISTS `tags_of_test_paper_label` (
  `test_paper_label_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tag_type`
--

DROP TABLE IF EXISTS `tag_type`;
CREATE TABLE IF NOT EXISTS `tag_type` (
  `tag_code` int(11) NOT NULL AUTO_INCREMENT,
  `tag_data` varchar(50) NOT NULL,
  PRIMARY KEY (`tag_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test_paper`
--

DROP TABLE IF EXISTS `test_paper`;
CREATE TABLE IF NOT EXISTS `test_paper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `type` int(50) NOT NULL DEFAULT '0',
  `class` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  `total_attempts` int(11) NOT NULL DEFAULT '0',
  `start_time` varchar(200) NOT NULL DEFAULT '0',
  `end_time` varchar(200) NOT NULL DEFAULT '0',
  `Instructions` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `mode` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test_papers_of_test_program`
--

DROP TABLE IF EXISTS `test_papers_of_test_program`;
CREATE TABLE IF NOT EXISTS `test_papers_of_test_program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_paper_id` int(11) NOT NULL,
  `test_program_id` int(11) NOT NULL,
  `test_paper_type_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `parent_type` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test_paper_attempts`
--

DROP TABLE IF EXISTS `test_paper_attempts`;
CREATE TABLE IF NOT EXISTS `test_paper_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `test_paper_id` int(11) NOT NULL,
  `marks_archived` int(11) NOT NULL DEFAULT '0',
  `total_marks` int(11) NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL,
  `start_time` int(11) NOT NULL DEFAULT '0',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `optimizer_id` int(11) NOT NULL DEFAULT '0',
  `complete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test_paper_labels`
--

DROP TABLE IF EXISTS `test_paper_labels`;
CREATE TABLE IF NOT EXISTS `test_paper_labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL DEFAULT '0',
  `chapter_id` int(11) NOT NULL DEFAULT '0',
  `topic_id` int(11) NOT NULL DEFAULT '0',
  `question_type` int(11) NOT NULL,
  `number_of_question` int(11) NOT NULL DEFAULT '1',
  `difficulty_level` int(11) NOT NULL DEFAULT '0',
  `skill_id` int(11) NOT NULL DEFAULT '0',
  `max_marks` int(11) NOT NULL DEFAULT '1',
  `neg_marks` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test_paper_questions`
--

DROP TABLE IF EXISTS `test_paper_questions`;
CREATE TABLE IF NOT EXISTS `test_paper_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_paper_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `lable_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `max_marks` int(11) NOT NULL DEFAULT '1',
  `neg_marks` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1  ;

-- --------------------------------------------------------

--
-- Table structure for table `test_paper_subjects`
--

DROP TABLE IF EXISTS `test_paper_subjects`;
CREATE TABLE IF NOT EXISTS `test_paper_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_paper_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test_paper_type`
--

DROP TABLE IF EXISTS `test_paper_type`;
CREATE TABLE IF NOT EXISTS `test_paper_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1  ;

-- --------------------------------------------------------

--
-- Table structure for table `test_program`
--

DROP TABLE IF EXISTS `test_program`;
CREATE TABLE IF NOT EXISTS `test_program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `class` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `cut_off` int(11) NOT NULL DEFAULT '60',
  `description` text NOT NULL,
  `image` varchar(500) NOT NULL DEFAULT 'assets/frontend/pages/img/test_program/default.jpg',
  `created` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1  ;

-- --------------------------------------------------------

--
-- Table structure for table `test_program_subjects`
--

DROP TABLE IF EXISTS `test_program_subjects`;
CREATE TABLE IF NOT EXISTS `test_program_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_program_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1  ;

-- --------------------------------------------------------


DROP TABLE IF EXISTS `topics`;
CREATE TABLE IF NOT EXISTS `topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(150) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `units`;
CREATE TABLE IF NOT EXISTS `units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(150) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` int(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT '',
  `email` varchar(150) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `registered_on` int(11) NOT NULL DEFAULT '0',
  `last_login` varchar(50) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `role` int(2) DEFAULT NULL,
  `mode` varchar(10) NOT NULL DEFAULT 'self',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_reset_codes`
--

DROP TABLE IF EXISTS `user_reset_codes`;
CREATE TABLE IF NOT EXISTS `user_reset_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `reset_code` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- --------------------------------------------------------

--
-- Structure for view `attempt_reports`
--
DROP TABLE IF EXISTS `attempt_reports`;

CREATE  VIEW `attempt_reports` AS select `attempt`.`student_id` AS `student_id`,`attempt`.`id` AS `attempt_id`,`attempt`.`program_id` AS `program_id`,`test_paper`.`id` AS `test_paper_id`,`test_paper`.`name` AS `name`,`test_paper`.`type` AS `test_paper_type`,`attempt`.`total_marks` AS `total_marks`,`attempt`.`marks_archived` AS `marks_archived`,`test_paper`.`time` AS `total_time`,(`attempt`.`end_time` - `attempt`.`start_time`) AS `time_taken`,count(`question_attempt`.`id`) AS `total_questions`,sum(if((`question_attempt`.`question_status` > 1),1,0)) AS `attempted_questions`,sum(if((`question_attempt`.`question_status` < 2),1,0)) AS `unattempted`,sum(`question_attempt`.`correct`) AS `correct_questions`,`attempt`.`complete` AS `complete` from (((`test_papers_of_test_program` `papers_of_program` join `test_paper_attempts` `attempt` on(((`attempt`.`program_id` = `papers_of_program`.`test_program_id`) and (`attempt`.`test_paper_id` = `papers_of_program`.`test_paper_id`)))) left join `test_paper` on((`test_paper`.`id` = `attempt`.`test_paper_id`))) left join `question_attempt` on((`question_attempt`.`attempt_id` = `attempt`.`id`))) group by `attempt`.`id`;

-- --------------------------------------------------------

--
-- Structure for view `question_details`
--
DROP TABLE IF EXISTS `question_details`;

CREATE VIEW `question_details` AS select `questions`.`id` AS `question_id`,`questions`.`question` AS `data`,`questions`.`description` AS `description`,`questions`.`type` AS `type`,`questions`.`quality_check` AS `quality_check`,`questions`.`parent_id` AS `parent_id`,date_format(from_unixtime(`questions`.`created`),'%d-%m-%y %h:%i %p') AS `modified_at`,`questions`.`delete` AS `deleted`,`classes`.`name` AS `class_name`,group_concat(if((`tagsofquestion`.`tag_type` = 1),`subjects`.`name`,' ') separator ',') AS `subject_name`,group_concat(if((`tagsofquestion`.`tag_type` = 2),`units`.`name`,' ') separator ',') AS `unit_name`,group_concat(if((`tagsofquestion`.`tag_type` = 3),`chapters`.`name`,' ') separator ',') AS `chapter_name`,group_concat(if((`tagsofquestion`.`tag_type` = 4),`topics`.`name`,' ') separator ',') AS `topic_name`,group_concat(if((`tagsofquestion`.`tag_type` = 5),`tags`.`tag`,' ') separator ',') AS `tag_name`,group_concat(if((`tagsofquestion`.`tag_type` = 6),`difficulty`.`difficulty`,' ') separator ',') AS `difficulty_name`,group_concat(if((`tagsofquestion`.`tag_type` = 7),`skills`.`skill`,' ') separator ',') AS `skill_name` from (((((((((`questions` left join `tagsofquestion` on((`tagsofquestion`.`question_id` = `questions`.`id`))) left join `classes` on((`classes`.`id` = `questions`.`class_id`))) left join `subjects` on((`subjects`.`id` = `tagsofquestion`.`tagged_id`))) left join `topics` on((`topics`.`id` = `tagsofquestion`.`tagged_id`))) left join `chapters` on((`chapters`.`id` = `tagsofquestion`.`tagged_id`))) left join `units` on((`units`.`id` = `tagsofquestion`.`tagged_id`))) left join `tags` on((`tags`.`id` = `tagsofquestion`.`tagged_id`))) left join `difficulty` on((`difficulty`.`id` = `tagsofquestion`.`tagged_id`))) left join `skills` on((`skills`.`id` = `tagsofquestion`.`tagged_id`))) group by `questions`.`id`;

-- --------------------------------------------------------

--
-- Structure for view `section_wise_report`
--
DROP TABLE IF EXISTS `section_wise_report`;

CREATE  VIEW `section_wise_report` AS select `coupons`.`institute_id` AS `institute_id`,`test_paper_attempts`.`student_id` AS `student_id`,`test_paper_attempts`.`id` AS `attempt_id`,`test_paper_attempts`.`program_id` AS `program_id`,`test_paper_attempts`.`test_paper_id` AS `test_paper_id`,`section`.`id` AS `section_id`,`section`.`name` AS `section_name`,sum(if((`question_attempt`.`question_status` > 1),1,0)) AS `questions_attempted`,sum(if(((`question_attempt`.`correct` = 1) and (`question_attempt`.`question_status` > 1)),1,0)) AS `questions_correct`,count(`question_attempt`.`id`) AS `total_questions`,sum(`question_attempt`.`time`) AS `time_taken`,(sum(if((`question_attempt`.`correct` = 1),`question_attempt`.`marks`,0)) - sum(if(((`question_attempt`.`correct` = 0) and (`question_attempt`.`question_status` > 1)),`question_attempt`.`neg_marks`,0))) AS `marks_get`,sum(`question_attempt`.`marks`) AS `total_markks`,`section`.`cut_off_marks` AS `cut_off_marks` from (((`coupons` left join `test_paper_attempts` on((`test_paper_attempts`.`student_id` = `coupons`.`student_id`))) left join `question_attempt` on((`question_attempt`.`attempt_id` = `test_paper_attempts`.`id`))) left join `section` on((`section`.`id` = `question_attempt`.`section_id`))) where ((`coupons`.`student_id` <> 0) and (`question_attempt`.`section_id` <> 0)) group by `question_attempt`.`attempt_id`,`question_attempt`.`section_id`;

-- --------------------------------------------------------

--
-- Structure for view `skill_wise_report`
--
DROP TABLE IF EXISTS `skill_wise_report`;

CREATE  VIEW `skill_wise_report` AS select `coupons`.`institute_id` AS `institute_id`,`test_paper_attempts`.`student_id` AS `student_id`,`test_paper_attempts`.`id` AS `attempt_id`,`test_paper_attempts`.`program_id` AS `program_id`,`test_paper_attempts`.`test_paper_id` AS `test_paper_id`,`skills`.`id` AS `skill_id`,ifnull(`skills`.`skill`,'No Skill') AS `skill`,count(`question_attempt`.`id`) AS `total_question`,sum(if(((`question_attempt`.`question_status` > 1) and (`question_attempt`.`correct` = 1)),1,0)) AS `questions_correct`,sum(if((`question_attempt`.`question_status` > 1),1,0)) AS `questions_attempted`,sum(if((`question_attempt`.`correct` = 1),`question_attempt`.`marks`,0)) AS `marks_get`,sum(`question_attempt`.`marks`) AS `total_marks` from ((`tagsofquestion` left join ((`coupons` left join `test_paper_attempts` on((`test_paper_attempts`.`student_id` = `coupons`.`student_id`))) left join `question_attempt` on((`question_attempt`.`attempt_id` = `test_paper_attempts`.`id`))) on(((`tagsofquestion`.`question_id` = `question_attempt`.`question_id`) and (`tagsofquestion`.`tag_type` = 7)))) left join `skills` on((`tagsofquestion`.`tagged_id` = `skills`.`id`))) where (`coupons`.`student_id` <> 0) group by `question_attempt`.`attempt_id`,`tagsofquestion`.`tagged_id`;

-- --------------------------------------------------------

--
-- Structure for view `time_takers_report`
--
DROP TABLE IF EXISTS `time_takers_report`;

CREATE VIEW `time_takers_report` AS select `question_attempt`.`attempt_id` AS `attempt_id`,`question_attempt`.`question_id` AS `question_id`,`question_attempt`.`question_type` AS `question_type`,`question_attempt`.`question_status` AS `question_status`,`question_attempt`.`time` AS `time`,`question_attempt`.`answer` AS `answer`,`question_attempt`.`correct` AS `correct`,`question_attempt`.`marks` AS `marks`,`question_attempt`.`neg_marks` AS `neg_marks`,`question_attempt`.`student_id` AS `student_id`,`questions`.`ideal_time` AS `ideal_time` from (`question_attempt` left join `questions` on((`question_attempt`.`question_id` = `questions`.`id`)));






--Data

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `registered_on`, `last_login`, `banned`, `role`, `deleted`) VALUES
(1, 'Admin', '', 'admin', '21232f297a57a5a743894a0e4a801fc3', 0, '1455082349', 0, 1, 0);

INSERT INTO `user_roles` (`id`, `role`) VALUES
(1, 'Super Admin'),
(2, 'SME'),
(3, 'DTP'),
(4, 'Student');


INSERT INTO `test_paper_type` (`id`, `type`) VALUES
(1, 'Diagnostic Test'),
(3, 'Chapter Test'),
(4, 'Full Syllabus Test'),
(5, 'Scheduled Test'),
(6, 'Practice Test');


INSERT INTO `tag_type` (`tag_code`, `tag_data`) VALUES
(1, 'subject'),
(2, 'unit'),
(3, 'chapter'),
(4, 'topic'),
(5, 'tag'),
(6, 'difficyulty-level'),
(7, 'skill');

INSERT INTO `question_types` (`id`, `question_type`, `short_name`) VALUES
(1, 'Single Choice Questions', 'scq'),
(2, 'Multiple Choice Questions', 'mcq'),
(4, 'Sequencing Type Questions', 'secquencing'),
(5, 'Matching (Drag n Drop)', 'matching'),
(6, 'Fill IN THE Blanks', 'fb'),
(7, 'Fill in the Blanks (Drop Down)', 'fb (dd)'),
(8, 'Fill in the Blanks (Drag n Drop)', 'fb (dnd)'),
(10, 'Descriptive', 'descriptive'),
(11, 'Passage', 'passage'),
(12, 'Matrix-Multiple Matching', 'matrix'),
(13, 'Integer Answer Questions', 'integer');