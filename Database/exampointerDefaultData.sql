INSERT INTO `question_types` (`id`, `question_type`, `short_name`) VALUES
(1, 'Single Choice Questions', 'scq'),
(2, 'Multiple Choice Questions', 'mcq'),
(4, 'Sequencing Type Questions', 'secquencing'),
(5, 'Matching (Drag n Drop)', 'matching'),
(6, 'Fill IN THE Blanks', 'fb'),
(7, 'Fill in the Blanks (Drop Down)', 'fb (dd)'),
(8, 'Fill in the Blanks (Drag n Drop)', 'fb (dnd)'),
(10, 'Descriptive', 'descriptive'),
(11, 'Passage', 'passage'),
(12, 'Matrix-Multiple Matching', 'matrix'),
(13, 'Integer Answer Questions', 'integer');

INSERT INTO `tag_type` (`tag_code`, `tag_data`) VALUES
(1, 'subject'),
(2, 'unit'),
(3, 'chapter'),
(4, 'topic'),
(5, 'tag'),
(6, 'difficyulty-level'),
(7, 'skill');

INSERT INTO `test_paper_type` (`id`, `type`) VALUES
(1, 'Diagnostic Test'),
(3, 'Chapter Test'),
(4, 'Full Syllabus Test'),
(5, 'Scheduled Test'),
(6, 'Practice Test');

INSERT INTO `user_roles` (`id`, `role`) VALUES
(1, 'Super Admin'),
(2, 'SME'),
(3, 'DTP'),
(4, 'Student');
