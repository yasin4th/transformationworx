DROP TABLE IF EXISTS `answers_of_question_attempts`;
CREATE TABLE `answers_of_question_attempts` (
  `id` int(11) NOT NULL,
  `question_attempt_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_id` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `batch`
--

DROP TABLE IF EXISTS `batch`;
CREATE TABLE `batch` (
  `id` int(20) NOT NULL,
  `class_id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created` int(50) NOT NULL,
  `deleted` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `batch_program`
--

DROP TABLE IF EXISTS `batch_program`;
CREATE TABLE `batch_program` (
  `id` int(11) NOT NULL,
  `program` int(11) NOT NULL,
  `batch` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `modified` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `batch_users`
--

DROP TABLE IF EXISTS `batch_users`;
CREATE TABLE `batch_users` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `batch` int(11) NOT NULL,
  `created` int(20) NOT NULL,
  `modified` int(20) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 -> not deleted 1 -> deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

DROP TABLE IF EXISTS `chapters`;
CREATE TABLE `chapters` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` varchar(150) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` varchar(25) NOT NULL,
  `created` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `code` varchar(110) DEFAULT NULL,
  `test_program_id` int(11) NOT NULL,
  `institute_id` int(11) NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL DEFAULT '0',
  `expiry_date` int(25) DEFAULT NULL,
  `created` int(25) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `difficulty`
--

DROP TABLE IF EXISTS `difficulty`;
CREATE TABLE `difficulty` (
  `id` int(11) NOT NULL,
  `difficulty` varchar(200) NOT NULL,
  `description` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `delete` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

DROP TABLE IF EXISTS `discount`;
CREATE TABLE `discount` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL DEFAULT 'percent',
  `value` int(11) NOT NULL,
  `min_price` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `maxuses` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `expiry` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `institute`
--

DROP TABLE IF EXISTS `institute`;
CREATE TABLE `institute` (
  `id` int(11) NOT NULL,
  `institute` int(11) NOT NULL,
  `website` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL DEFAULT 'assets/instituteimages/logo/default.png',
  `banner` varchar(255) NOT NULL DEFAULT 'assets/instituteimages/banners/default.jpg',
  `features` text NOT NULL,
  `about` text NOT NULL,
  `director` text NOT NULL,
  `courses` text NOT NULL,
  `contact` text NOT NULL,
  `last_active` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `institute_program`
--

DROP TABLE IF EXISTS `institute_program`;
CREATE TABLE `institute_program` (
  `id` bigint(20) NOT NULL,
  `institute` bigint(20) NOT NULL,
  `program` int(11) NOT NULL,
  `lab` text CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `option` text COLLATE utf8_bin NOT NULL,
  `answer` tinyint(1) NOT NULL DEFAULT '0',
  `number` int(11) NOT NULL DEFAULT '0',
  `question_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `program` int(11) NOT NULL,
  `order_id` varchar(200) NOT NULL,
  `price` varchar(20) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `txnid` varchar(13) NOT NULL DEFAULT '0',
  `mihpayid` varchar(25) NOT NULL COMMENT 'payu param',
  `mode` varchar(25) NOT NULL COMMENT 'payu param',
  `bank_ref_num` varchar(25) NOT NULL COMMENT 'payu param',
  `payuMoneyId` varchar(30) NOT NULL COMMENT 'payu param',
  `discount_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `planner`
--

DROP TABLE IF EXISTS `planner`;
CREATE TABLE `planner` (
  `id` int(11) NOT NULL,
  `user_id` int(2) NOT NULL,
  `subject_id` int(2) NOT NULL,
  `startDate` varchar(1000) NOT NULL,
  `targetDate` varchar(1000) NOT NULL,
  `offDays` varchar(1000) NOT NULL,
  `schoolHolidays` varchar(1000) NOT NULL,
  `dailyOrAlternate` varchar(1000) NOT NULL,
  `schoolDaysSlotsFrom` varchar(1000) NOT NULL,
  `schoolDaysSlotsTo` varchar(1000) NOT NULL,
  `schoolHolidaysSlotsFrom` varchar(1000) NOT NULL,
  `schoolHolidaysSlotsTo` varchar(1000) NOT NULL,
  `rectimes` varchar(1000) NOT NULL,
  `chapters` varchar(2000) NOT NULL,
  `alloted` varchar(1000) NOT NULL,
  `lc` varchar(1000) NOT NULL,
  `pq` varchar(1000) NOT NULL,
  `tr` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `class` varchar(55) NOT NULL,
  `registered_on` int(11) NOT NULL,
  `interests` varchar(500) NOT NULL,
  `occupation` varchar(100) NOT NULL,
  `about` varchar(500) NOT NULL,
  `website_url` varchar(500) NOT NULL,
  `address` text NOT NULL,
  `image` varchar(150) NOT NULL DEFAULT 'ProfilePictures/default.png'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qcr_illustration`
--

DROP TABLE IF EXISTS `qcr_illustration`;
CREATE TABLE `qcr_illustration` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `test_program_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `source` varchar(500) NOT NULL,
  `created` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `class_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `question` text COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `type` tinyint(2) UNSIGNED NOT NULL,
  `quality_check` tinyint(1) NOT NULL DEFAULT '0',
  `ideal_time` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `delete` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `question_attempt`
--

DROP TABLE IF EXISTS `question_attempt`;
CREATE TABLE `question_attempt` (
  `id` int(11) UNSIGNED NOT NULL,
  `attempt_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `question_id` int(11) UNSIGNED NOT NULL,
  `section_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `question_type` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `question_status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `time` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `answer` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `correct` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `marks` int(5) UNSIGNED NOT NULL DEFAULT '0',
  `neg_marks` float(5,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `student_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question_details`
--

DROP TABLE IF EXISTS `question_details`;
CREATE TABLE `question_details` (
  `question_id` int(11) DEFAULT NULL,
  `data` text,
  `description` text,
  `type` int(11) DEFAULT NULL,
  `quality_check` tinyint(1) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `modified_at` varchar(17) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `class_name` varchar(150) DEFAULT NULL,
  `subject_name` longtext,
  `unit_name` longtext,
  `chapter_name` longtext,
  `topic_name` longtext,
  `tag_name` longtext,
  `difficulty_name` longtext,
  `skill_name` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question_skills`
--

DROP TABLE IF EXISTS `question_skills`;
CREATE TABLE `question_skills` (
  `question_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question_types`
--

DROP TABLE IF EXISTS `question_types`;
CREATE TABLE `question_types` (
  `id` int(11) NOT NULL,
  `question_type` varchar(50) NOT NULL,
  `short_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT 'untitled',
  `total_question` int(11) NOT NULL DEFAULT '0',
  `cut_off_marks` int(11) NOT NULL DEFAULT '0',
  `test_paper` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

DROP TABLE IF EXISTS `skills`;
CREATE TABLE `skills` (
  `id` int(11) NOT NULL,
  `skill` varchar(200) NOT NULL,
  `description` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `delete` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_program`
--

DROP TABLE IF EXISTS `student_program`;
CREATE TABLE `student_program` (
  `id` int(11) NOT NULL,
  `user_id` int(5) NOT NULL,
  `test_program_id` int(5) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `allowed_total_paper` int(11) NOT NULL,
  `allowed_total_lab` int(11) NOT NULL,
  `valid_till` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `time` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_question_bank`
--

DROP TABLE IF EXISTS `student_question_bank`;
CREATE TABLE `student_question_bank` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `revision` int(11) NOT NULL DEFAULT '0',
  `important` int(11) NOT NULL DEFAULT '0',
  `unable_to_solve` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` varchar(150) NOT NULL,
  `class_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `tag` text NOT NULL,
  `description` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `delete` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tagsofquestion`
--

DROP TABLE IF EXISTS `tagsofquestion`;
CREATE TABLE `tagsofquestion` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `tagged_id` int(11) NOT NULL,
  `tag_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags_of_test_paper_label`
--

DROP TABLE IF EXISTS `tags_of_test_paper_label`;
CREATE TABLE `tags_of_test_paper_label` (
  `test_paper_label_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tag_type`
--

DROP TABLE IF EXISTS `tag_type`;
CREATE TABLE `tag_type` (
  `tag_code` int(11) NOT NULL,
  `tag_data` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tag_type`
--

INSERT INTO `tag_type` (`tag_code`, `tag_data`) VALUES
(1, 'subject'),
(2, 'unit'),
(3, 'chapter'),
(4, 'topic'),
(5, 'tag'),
(6, 'difficyulty-level'),
(7, 'skill');

-- --------------------------------------------------------

--
-- Table structure for table `test_paper`
--

DROP TABLE IF EXISTS `test_paper`;
CREATE TABLE `test_paper` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `type` int(50) NOT NULL DEFAULT '0',
  `class` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  `total_attempts` int(11) NOT NULL DEFAULT '0',
  `start_time` varchar(200) NOT NULL DEFAULT '0',
  `end_time` varchar(200) NOT NULL DEFAULT '0',
  `Instructions` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `mode` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `test_papers_of_test_program`
--

DROP TABLE IF EXISTS `test_papers_of_test_program`;
CREATE TABLE `test_papers_of_test_program` (
  `id` int(11) NOT NULL,
  `test_paper_id` int(11) NOT NULL,
  `test_program_id` int(11) NOT NULL,
  `test_paper_type_id` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `parent_type` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `test_paper_attempts`
--

DROP TABLE IF EXISTS `test_paper_attempts`;
CREATE TABLE `test_paper_attempts` (
  `id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `test_paper_id` int(11) NOT NULL,
  `marks_archived` float(11,2) NOT NULL DEFAULT '0.00',
  `total_marks` int(11) NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL,
  `start_time` int(11) NOT NULL DEFAULT '0',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `optimizer_id` int(11) NOT NULL DEFAULT '0',
  `complete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `test_paper_labels`
--

DROP TABLE IF EXISTS `test_paper_labels`;
CREATE TABLE `test_paper_labels` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL DEFAULT '0',
  `chapter_id` int(11) NOT NULL DEFAULT '0',
  `topic_id` int(11) NOT NULL DEFAULT '0',
  `question_type` int(11) NOT NULL,
  `number_of_question` int(11) NOT NULL DEFAULT '1',
  `difficulty_level` int(11) NOT NULL DEFAULT '0',
  `skill_id` int(11) NOT NULL DEFAULT '0',
  `max_marks` int(11) NOT NULL DEFAULT '1',
  `neg_marks` float(11,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `test_paper_questions`
--

DROP TABLE IF EXISTS `test_paper_questions`;
CREATE TABLE `test_paper_questions` (
  `id` int(11) NOT NULL,
  `test_paper_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `lable_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `max_marks` int(11) NOT NULL DEFAULT '1',
  `neg_marks` float(11,2) NOT NULL DEFAULT '0.00',
  `created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `test_paper_subjects`
--

DROP TABLE IF EXISTS `test_paper_subjects`;
CREATE TABLE `test_paper_subjects` (
  `id` int(11) NOT NULL,
  `test_paper_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `test_paper_type`
--

DROP TABLE IF EXISTS `test_paper_type`;
CREATE TABLE `test_paper_type` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_paper_type`
--

INSERT INTO `test_paper_type` (`id`, `type`) VALUES
(1, 'Diagnostic Test'),
(3, 'Chapter Test'),
(4, 'Full Syllabus Test'),
(5, 'Scheduled Test'),
(6, 'Practice Test');

-- --------------------------------------------------------

--
-- Table structure for table `test_program`
--

DROP TABLE IF EXISTS `test_program`;
CREATE TABLE `test_program` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `class` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `type` varchar(20) NOT NULL COMMENT 'private=1,public=2',
  `price` int(11) NOT NULL,
  `cut_off` int(11) NOT NULL DEFAULT '60',
  `validity` tinyint(4) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'validity in months',
  `valid_from` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `valid_till` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text NOT NULL,
  `image` varchar(500) NOT NULL DEFAULT 'assets/frontend/pages/img/test_program/default.jpg',
  `created` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `test_program_subjects`
--

DROP TABLE IF EXISTS `test_program_subjects`;
CREATE TABLE `test_program_subjects` (
  `id` int(11) NOT NULL,
  `test_program_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

DROP TABLE IF EXISTS `topics`;
CREATE TABLE `topics` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` varchar(150) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `units`;
CREATE TABLE `units` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` varchar(150) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` int(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `profile_image` varchar(200) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `fb_id` bigint(20) NOT NULL,
  `registered_on` int(11) NOT NULL DEFAULT '0',
  `last_login` varchar(50) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `role` tinyint(2) UNSIGNED NOT NULL DEFAULT '4',
  `mode` varchar(10) NOT NULL DEFAULT 'self',
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `profile_image`, `password`, `fb_id`, `registered_on`, `last_login`, `banned`, `role`, `mode`, `deleted`) VALUES
(4, 'test', 'test', 'test@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 0, 1472884641, '1479531091', 0, 5, 'self', 1),
(96, 'ghgh', 'dk', 'test1@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 0, 1479294986, '1482752808', 0, 5, 'self', 0),
(139, 'lll', '', 'lkk@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 0, 1480333438, '1482749717', 0, 1, 'self', 0),
(146, 'lakshman', 'kashyap', NULL, '', NULL, 0, 0, '0', 0, 4, 'self', 0),
(147, 'lakshmaasdfsdf', 'kashmaasdfsdfds', 'demo4@gmail.com', '', '26916c3a6922be8bf8c50e766dbfc056', 0, 1480401316, '0', 0, 4, 'self', 0),
(148, 'demosix890 dfg', 'deds ', 'demosix@gmail.com', '', 'e807f1fcf82d132f9bb018ca6738a19f', 0, 1480403196, '0', 0, 4, 'self', 1),
(149, 'demosix090', 'demosix', 'demosevean@gmail.com', '', '51f00056d82bf62f5d41f40654e67080', 0, 1480403227, '0', 0, 4, 'self', 0),
(150, 'demoten', 'fsdf', 'demoten@gamil.com', '', 'e807f1fcf82d132f9bb018ca6738a19f', 0, 1480405121, '0', 0, 4, 'self', 0),
(151, 'deepak', 'singh', 'deepak@gmail.com', '', 'e09c80c42fda55f9d992e59ca6b3307d', 0, 1480407705, '1482753799', 0, 4, 'self', 0),
(152, 'Lakshman', 'Kashyap', 'demo1@gmail.com', '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 1480594211, '0', 0, 4, 'self', 0),
(153, 'pawan', '', 'pwn@gmail.com', '', 'e807f1fcf82d132f9bb018ca6738a19f', 0, 1480594263, '0', 0, 4, 'self', 0),
(154, 'nelson', '', 'nel@gmaill.com', '', 'e807f1fcf82d132f9bb018ca6738a19f', 0, 1480594366, '0', 0, 4, 'self', 0),
(161, 'ravi', '', 'ravi@gmail.com', '', '18c3b18baf23112ba9b46b7f2a2c923c', 0, 1480595098, '0', 0, 4, 'self', 0),
(162, 'ravii', '', 'ravi2@gmail.com', '', '18c3b18baf23112ba9b46b7f2a2c923c', 0, 1480595223, '0', 0, 4, 'self', 0),
(163, 'raviii', '', 'ravi21@gmail.com', '', '18c3b18baf23112ba9b46b7f2a2c923c', 0, 1480595264, '0', 0, 4, 'self', 0),
(164, 'soi', '', 'so@gmail.com', '', '18c3b18baf23112ba9b46b7f2a2c923c', 0, 1480595291, '0', 0, 4, 'self', 0),
(165, 'soo', '', 'soo@gmail.com', '', '994eaa726ecf1153aec57e4425dc00d9', 0, 1480595324, '0', 0, 4, 'self', 0),
(166, 'hji', '', 'hj@gmail.com', '', '6555565280542494f1634735554d6139', 0, 1480595406, '0', 0, 4, 'self', 0),
(167, 'push', '', 'push@gmail.com', '', '6890ad2b0c2974dd52e062de03ef84b1', 0, 1480595585, '0', 0, 4, 'self', 0),
(168, 'lara', '', 'lara@gmail.com', '', '0f7e730cec50556a3eca722b093be08d', 0, 1480595839, '0', 0, 4, 'self', 0),
(169, 'java', '', 'java@gmail.com', '', '65f7db421b51d5eb4f7d15cd6d40f44c', 0, 1480596758, '0', 0, 4, 'self', 0),
(170, 'lakshman', 'kashyap', 'kkl@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 0, 1480597986, '1482322755', 0, 6, 'self', 0),
(171, 'tdemo', NULL, 'tdemo@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 0, 0, '0', 0, 5, 'self', 0),
(172, 'tdmeo', NULL, 'd@gmail.com', '', 'e807f1fcf82d132f9bb018ca6738a19f', 0, 0, '0', 0, 5, 'self', 0),
(173, 'ttttttttt', NULL, 't@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 0, 0, '0', 0, 5, 'self', 0),
(174, 'instittue', NULL, 'ins@gmail.com', '', 'd8e423a9d5eb97da9e2d58cd57b92808', 0, 0, '0', 0, 5, 'self', 0),
(175, 'ddddd', NULL, 'dd@gmail.com', '', '25f9e794323b453885f5181f1b624d0b', 0, 0, '0', 0, 5, 'self', 0),
(176, 'deepak', 'thakur', 'deepakthakur13@gmail.com', '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 1482130683, '1482222831', 0, 6, 'self', 0),
(177, 'ravishahoo', '', 'ravishahoo@gmail.com', '', 'ee0bc7485e9714d6a8476f0436553c8d', 0, 1482131837, '0', 0, 4, 'self', 0),
(178, 'ravi', '', 'ravis@gmail.com', '', 'e807f1fcf82d132f9bb018ca6738a19f', 0, 1482131870, '0', 0, 4, 'self', 0),
(179, 'ravi', 'ravi', 'ravi121@gmail.cm', '', '25f9e794323b453885f5181f1b624d0b', 0, 1482132036, '0', 0, 4, 'self', 0),
(180, 'ravi', '', 'ravi101@gmail.com', '', 'fcea920f7412b5da7be0cf42b8c93759', 0, 1482132077, '0', 0, 4, 'self', 0),
(181, 'ravi', 'shahoo', 'ra@gmail.com', '', 'e807f1fcf82d132f9bb018ca6738a19f', 0, 1482132405, '0', 0, 6, 'self', 0),
(182, 'pawan', 'thakur', 'pa@gmail.com', '', '18c3b18baf23112ba9b46b7f2a2c923c', 0, 1482132560, '0', 0, 6, 'self', 0),
(183, 'pa', '', 'paw@gmailc.om', '', '4e674e4eebe7721a791c7c4cafa5746b', 0, 1482132598, '0', 0, 4, 'self', 0),
(185, 'anuj', '', 'anuj@gmail.cm', '', 'e10adc3949ba59abbe56e057f20f883e', 0, 1482134252, '0', 0, 4, 'self', 0),
(186, 'anuj', '', 'anuj21@gmail.com', '', 'fcea920f7412b5da7be0cf42b8c93759', 0, 1482134306, '1482134393', 0, 6, 'self', 0),
(187, 'dd', '', 'ddd@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 0, 1482134547, '0', 0, 4, 'self', 0),
(188, 'dddddd', 'dddddd', 'ddd11@gmail.com', '', 'fcea920f7412b5da7be0cf42b8c93759', 0, 1482134624, '1482134684', 0, 6, 'self', 0),
(189, 'jjj', 'jjj', 'jjj@gmail.com', '', 'fcea920f7412b5da7be0cf42b8c93759', 0, 1482134637, '1482135493', 0, 6, 'self', 0),
(190, 'HI', '1', 'hi@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 0, 1482135586, '0', 0, 4, 'self', 0),
(191, 'hii', '', 'hii@gmail.com', '', '664fae06a748e656511d55b59fc6f85e', 0, 1482135692, '0', 0, 6, 'self', 0),
(192, 'llllqq', '', 'll@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 0, 1482135722, '1482135984', 0, 6, 'self', 0),
(193, 'pointer', NULL, 'pointer@gmail.com', '', 'e09c80c42fda55f9d992e59ca6b3307d', 0, 0, '1482136162', 0, 5, 'self', 0),
(194, 'deepak', 'singh', 'deepak.4thpointer@gmail.com', '', '498b5924adc469aa7b660f457e0fc7e5', 0, 1482136245, '1482136488', 0, 4, 'self', 0),
(195, 'ravi', 'ravi', 'rav@gmial.com', '', 'e10adc3949ba59abbe56e057f20f883e', 0, 1482136315, '1482137132', 0, 6, 'self', 1),
(196, 'ravi', 'ravi', 'rav@gamil.com', '', 'e0ec043b3f9e198ec09041687e4d4e8d', 0, 1482137156, '0', 0, 6, 'self', 1),
(197, 'anu', 'anuj', 'an@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 0, 1482137300, '1482137316', 0, 6, 'self', 0),
(211, 'lak', '', 'klkl@hotmail.com', '', 'e807f1fcf82d132f9bb018ca6738a19f', 0, 1482750590, '1482752878', 0, 4, 'self', 0),
(212, 'rav', '', 'k@ymail.com', '', 'e807f1fcf82d132f9bb018ca6738a19f', 0, 1482752957, '1482753350', 0, 4, 'self', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_reset_codes`
--

DROP TABLE IF EXISTS `user_reset_codes`;
CREATE TABLE `user_reset_codes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reset_code` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL,
  `role` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers_of_question_attempts`
--
ALTER TABLE `answers_of_question_attempts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_attempt_id` (`question_attempt_id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `option_id` (`option_id`);

--
-- Indexes for table `batch`
--
ALTER TABLE `batch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batch_program`
--
ALTER TABLE `batch_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batch_users`
--
ALTER TABLE `batch_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chapters`
--
ALTER TABLE `chapters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `difficulty`
--
ALTER TABLE `difficulty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `institute`
--
ALTER TABLE `institute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institute_program`
--
ALTER TABLE `institute_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `answer` (`answer`),
  ADD KEY `number` (`number`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `planner`
--
ALTER TABLE `planner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `type` (`type`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `delete` (`delete`),
  ADD KEY `created` (`created`);

--
-- Indexes for table `question_attempt`
--
ALTER TABLE `question_attempt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `attempt_id` (`attempt_id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `question_types`
--
ALTER TABLE `question_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `test_paper` (`test_paper`),
  ADD KEY `deleted` (`deleted`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_program`
--
ALTER TABLE `student_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_question_bank`
--
ALTER TABLE `student_question_bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tagsofquestion`
--
ALTER TABLE `tagsofquestion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `tagged_id` (`tagged_id`),
  ADD KEY `tag_type` (`tag_type`);

--
-- Indexes for table `tag_type`
--
ALTER TABLE `tag_type`
  ADD PRIMARY KEY (`tag_code`);

--
-- Indexes for table `test_paper`
--
ALTER TABLE `test_paper`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `type` (`type`),
  ADD KEY `time` (`time`),
  ADD KEY `start_time` (`start_time`),
  ADD KEY `end_time` (`end_time`),
  ADD KEY `status` (`status`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `deleted` (`deleted`);

--
-- Indexes for table `test_papers_of_test_program`
--
ALTER TABLE `test_papers_of_test_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_paper_attempts`
--
ALTER TABLE `test_paper_attempts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `program_id` (`program_id`),
  ADD KEY `test_paper_id` (`test_paper_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `complete` (`complete`),
  ADD KEY `start_time` (`start_time`),
  ADD KEY `end_time` (`end_time`),
  ADD KEY `marks_archived` (`marks_archived`),
  ADD KEY `total_marks` (`total_marks`);

--
-- Indexes for table `test_paper_labels`
--
ALTER TABLE `test_paper_labels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_paper_questions`
--
ALTER TABLE `test_paper_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_paper_subjects`
--
ALTER TABLE `test_paper_subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_paper_type`
--
ALTER TABLE `test_paper_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_program`
--
ALTER TABLE `test_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_program_subjects`
--
ALTER TABLE `test_program_subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`email`);

--
-- Indexes for table `user_reset_codes`
--
ALTER TABLE `user_reset_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers_of_question_attempts`
--
ALTER TABLE `answers_of_question_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `batch`
--
ALTER TABLE `batch`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `batch_program`
--
ALTER TABLE `batch_program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `batch_users`
--
ALTER TABLE `batch_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `chapters`
--
ALTER TABLE `chapters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=450;
--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1394;
--
-- AUTO_INCREMENT for table `difficulty`
--
ALTER TABLE `difficulty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `discount`
--
ALTER TABLE `discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10004;
--
-- AUTO_INCREMENT for table `institute`
--
ALTER TABLE `institute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `institute_program`
--
ALTER TABLE `institute_program`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18609;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `planner`
--
ALTER TABLE `planner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=377;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5399;
--
-- AUTO_INCREMENT for table `question_attempt`
--
ALTER TABLE `question_attempt`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=430;
--
-- AUTO_INCREMENT for table `question_types`
--
ALTER TABLE `question_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;
--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `student_program`
--
ALTER TABLE `student_program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=765;
--
-- AUTO_INCREMENT for table `student_question_bank`
--
ALTER TABLE `student_question_bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tagsofquestion`
--
ALTER TABLE `tagsofquestion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1723;
--
-- AUTO_INCREMENT for table `tag_type`
--
ALTER TABLE `tag_type`
  MODIFY `tag_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `test_paper`
--
ALTER TABLE `test_paper`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT for table `test_papers_of_test_program`
--
ALTER TABLE `test_papers_of_test_program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `test_paper_attempts`
--
ALTER TABLE `test_paper_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `test_paper_labels`
--
ALTER TABLE `test_paper_labels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=312;
--
-- AUTO_INCREMENT for table `test_paper_questions`
--
ALTER TABLE `test_paper_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3611;
--
-- AUTO_INCREMENT for table `test_paper_subjects`
--
ALTER TABLE `test_paper_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `test_paper_type`
--
ALTER TABLE `test_paper_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `test_program`
--
ALTER TABLE `test_program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `test_program_subjects`
--
ALTER TABLE `test_program_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=213;
--
-- AUTO_INCREMENT for table `user_reset_codes`
--
ALTER TABLE `user_reset_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
