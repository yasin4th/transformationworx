/ALTER TABLE `test_paper_attempts` ADD `inlab` INT(1) NOT NULL DEFAULT '0' AFTER `student_id`;

ALTER TABLE `certificates` ADD `license_no` VARCHAR(255) NULL DEFAULT NULL AFTER `hash`;

ALTER TABLE `test_program` DROP `description`;
ALTER TABLE `test_program` ADD `validity` DATE NULL DEFAULT NULL AFTER `cut_off`;