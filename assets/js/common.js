var EndPoint = 'vendor/api.php';

$(function() {
	loadHeaderDetails();

	setEventReedmeCoupon();
	/*
	* function to show ajax loader
	*
	*/
	$(document).ajaxStart(function() {
	    $('#loader').show();
	});

	/*
	* function to hide ajax loader
	*
	*/
	$(document).ajaxComplete(function() {
	    $('#loader').hide();
	});

});

function getUrlParameter(sParam) {
	sParam = sParam.toLowerCase();
	var sPageURL = window.location.search.substring(1).toLowerCase();
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++)
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam)
		{
			return sParameterName[1];
		}
	}
}

function setError(where, what) {
	var parent = $(where).parents('div:eq(0)');
	parent.addClass('has-error').append('<span class="help-block">' + what + '</span>');
}

function unsetError(where) {
	var parent = $(where).parents('div:eq(0)');
	parent.removeClass('has-error').find('.help-block').remove();
}

function mathsjax() {
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src  = "http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML";
	document.getElementsByTagName("head")[0].appendChild(script);
}

function setEventReedmeCoupon() {
	$('#redeem-coupon-form').off('submit')
	$('#redeem-coupon-form').on('submit', function(e) {
		e.preventDefault();
		code = $('#redeem-coupon-form .coupon-code').val();
		var req = {};
		req.action = "redeemCoupon"
		req.code = code;
		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);

			if (res.status == 1) {
				$('#redeem-coupon-modal').modal('hide');
				toastr.success(res.message);
			}
			else {
				toastr.error(res.message);
			};
		});
	});
}


function loadHeaderDetails() {
	var req = {};
	req.action = 'fetchUserDetails';
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 1) {
			$('.user-image').attr('src', res.user.image);
			if(res.user.ins_lab_type == '0')
			{
				//$('div.sidebar ul.sidebar-menu li:last').remove();
				$('#add-student-form div#ins_lab_type').hide();
				$('#assign-program-form #ap_ins_lab_type').hide();
				$('#edit-student-program #ep_on_ms').hide();
				$('#view-program #program-table').closest('table').find('thead tr th:eq(3)').hide();
				/*$('#view-program #program-table').closest('table').find('tbody tr').each(function() {
        			this.hideChild(this.cells[3]);
    			});*/
				$('#view-program #program-table').closest('table').find('tbody tr td:eq(3)').hide();
			}
			else{
				$('#li-for-lab-type').css('display','block');
			}
			if(res.user.ins_lab_type == '0' && window.location.href.search("manage-assistants.php") > 10)
			{
				location.replace('manage-students.php');
			}
			if( res.user != null && res.user.first_name != null)
			{
				$('.user-name').text(res.user.first_name);
			}

		} else if(res.status == 0){
			toastr.error(res.message);
		};
	})
}

String.prototype.caps = function() {
	if (this != null) {
    	return this.charAt(0).toUpperCase() + this.slice(1);
	}
	else
	{
		return this;
	}
}

String.prototype.dash = function()
{
    return this.replace(/[\s-_//,]+/gi,' ').split(" ").join("-").toLowerCase();
}

String.prototype.esc = function() {
	return this.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"').replace(/&amp;/g, '&');
}

