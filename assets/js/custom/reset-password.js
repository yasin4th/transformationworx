$(function() {

	$('#reset-password-form').off('submit');
	$('#reset-password-form').on('submit', function(e) {
		e.preventDefault();
		new_password = $('#new-password').val();
		confirm_password = $('#confirm-password').val();
		
		if (new_password != '' && confirm_password != '') {
			if (new_password == confirm_password) {

				var req = {};
				req.password = new_password;
				req.code = getUrlParameter('token');
				req.action = 'resetPassword';
				$.ajax({
					'type'	:	'post',
					'url'	:	EndPoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					
					if(res.status == 1)
					{
						toastr.success(res.message);
						setTimeout(function() {document.location = "login.php";}, 1000);
					}
					else 
					{
						toastr.error(res.message,'Error:');
					}

				});
			}else {
				toastr.error('Please enter same password both time.','Error:');
			};

		}else {
			toastr.error("Please fill password 2 times.",'Error:');
		};
	});

});