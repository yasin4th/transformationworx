$(function() {
	$('#reset').on('click', function(e){
		e.preventDefault();
		$('.register-div').find('input:text').val('');
		 $('#email').val('');
		 $('.register-div').find('input:password').val('');
	});
	
	$('#institute-registration-form').on('submit', function(e) { 
		e.preventDefault();
		
		if(validate()){
			var req = {};
			req.action = "institute_register";
			req.name = $('#institutename').val();
			req.email = $('#email').val();
			req.password = $('#password').val();
			req.phone_no = $('#mobile').val();
			req.details = '';
			req.ins_lab_type = 0;

			$.ajax({
				'type'	:	'post',
				'url'	:	'vendor/api.php',
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				console.log(res);
				if(res.status == 1)
				{
					location.replace("institute-profile.php");
				}
				else
				{
					alert("Error:"+res.message);
					toastr.error(res.message);
				}
			});
		}
	});
});

validate = function() {
	var pass = $('#password').val();
	var confirm = $('#confirm-password').val();
	var regx = /^[0-9]*$/;

	if($('#institutename').val() != "" && $('#institutename').val().length<3){
		toastr.error("please enter alteast 3 characters in institute name.");
		return false;
	}

	if(!regx.test($('#mobile').val()))
	{
		toastr.error("please enter valid mobile no.");
		return false;
	}

	if($('#mobile').val() != "" && $('#mobile').val().length<10){
		toastr.error("please enter valid mobile no.");
		return false;
	}

	if($('#mobile').val() != "" && $('#mobile').val().length>11){
		toastr.error("please enter valid mobile no.");
		return false;
	}

	if($('#password').val() != "" && $('#password').val().length<6){
		toastr.error("please enter atleast six characters long password");
		return false;
	}
	if(confirm != pass){
		toastr.error("Confirm password incorrect.");
		return false;
	}

	return true;
}