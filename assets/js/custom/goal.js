var program_id;
var gaugeOptions = {

        chart: {
            type: 'solidgauge'
        },

        title: null,

        pane: {
            center: ['50%', '85%'],
            size: '140%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        tooltip: {
            enabled: false
        },

        // the value axis
        yAxis: {
            stops: [
                [0.1, '#55BF3B'], // green
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#DF5353'] // red
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -70
            },
            labels: {
                y: 16
            }
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };
$(function () {
	program_id = getUrlParameter('program_id');
    getProgramDetails();
	Meter.getData();
	$('.go-to-course').attr('href', 'student-course-details.php?program_id='+program_id)	

    // The speed gauge
    $('#meter1').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 200,
            title: {
                text: ''
            }
        },

        credits: {
            enabled: false
        },

        series: [{
            name: 'Points',
            data: [0],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                       '<span style="font-size:12px;color:silver">Points</span></div>'
            },
            tooltip: {
                valueSuffix: ' Points'
            }
        }]

    }));
    $('#meter2').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 200,
            title: {
                text: ''
            }
        },

        credits: {
            enabled: false
        },

        series: [{
            name: 'Points',
            data: [0],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                       '<span style="font-size:12px;color:silver">Points</span></div>'
            },
            tooltip: {
                valueSuffix: ' Points'
            }
        }]

    }));
     $('#meter3').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 200,
            title: {
                text: ''
            }
        },

        credits: {
            enabled: false
        },

        series: [{
            name: 'Points',
            data: [0],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                       '<span style="font-size:12px;color:silver">Points</span></div>'
            },
            tooltip: {
                valueSuffix: ' Points'
            }
        }]

    }));
   
})

var Meter = {};

Meter.getData = function () {
	var req = {};
	req.action = "fetchRewardData";
	req.program_id = program_id;
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req) 
	}).done(function(res) {
		res = $.parseJSON(res);		
		if (res.status == 1) {
            $('#cutoff-flag').css('margin-left',res.cut_off+'%');
            questions = parseInt((res.total_attempted_questions * 100) / res.total_questions);
            test = parseInt((res.total_attempted_tests * 100) / res.total_tests);
            concept = parseInt((res.total_correct_questions * 100) / res.total_attempted_questions);
			if (res.total_attempted_questions == 0) { concept = 0;};
            points = parseInt((res.archived_reward_points * 100) / res.total_reward_points);			

			// Meter.createQuestionMeter(parseInt(res.total_attempted_tests), parseInt(res.total_tests));
			// Meter.createTestMeter(parseInt(res.total_attempted_questions), parseInt(res.total_questions));
			// Meter.createConceptMeter(parseInt(res.total_correct_questions), parseInt(res.total_attempted_questions));
			Meter.createQuestionMeter(questions);
			Meter.createTestMeter(test);
			Meter.createConceptMeter(concept);
			$('#progress-meter').text(points+'%').css('width', points+'%');
		} else{
			toastr.error(res.message);
		};

	});
}


Meter.createQuestionMeter = function (attempted) {
	$('#meter1').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: ''
            }
        },

        credits: {
            enabled: false
        },

        series: [{
            name: 'Points',
            data: [attempted],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}%</span><br/>' +
                       '<span style="font-size:12px;color:silver">Question Meter</span></div>'
            },
            tooltip: {
                valueSuffix: ' Points'
            }
        }]

    }));
}

Meter.createTestMeter = function(attempted) {
	 $('#meter2').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: ''
            }
        },

        credits: {
            enabled: false
        },

        series: [{
            name: 'Points',
            data: [attempted],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}%</span><br/>' +
                       '<span style="font-size:12px;color:silver">Test Meter</span></div>'
            },
            tooltip: {
                valueSuffix: ' Points'
            }
        }]

    }));
}

Meter.createConceptMeter = function (correct) {
	$('#meter3').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: ''
            }
        },

        credits: {
            enabled: false
        },

        series: [{
            name: 'Points',
            data: [correct],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}%</span><br/>' +
                       '<span style="font-size:12px;color:silver">Concept Meter</span></div>'
            },
            tooltip: {
                valueSuffix: ' Points'
            }
        }]

    }));
}

function getProgramDetails() {
    var req = {};
    req.test_program = program_id;
    req.action = 'getProgramDetails';

    $.ajax({
        type: "post",
        url: EndPoint,
        data: JSON.stringify(req) 
    }).done(function(res) {
        res = $.parseJSON(res);        
        if(res.status == 1) {
            $('.program-name').html(res.details.name);
            $('.program-name').attr('href','student-course-details.php?program_id='+program_id);

            //genrateSubjects(res.subjectList)
        }
        else {
            toastr.error(res.message);
        }
    });

}