test_program = getUrlParameter('program_id');
subject = getUrlParameter('subject');
chapter = getUrlParameter('chapter');

$(function() {
	// this is for BCT(BreadCrumb Trail)
	$('.back-to-subjects').attr('href', 'student-course-details.php?program_id='+test_program)
	$('.back-to-chapters').attr('href', 'student-course-chapters.php?program_id='+test_program+'&subject='+subject)
	fetchTestProgramDetails();
	getProgramDetails();
	setEventViewTest();
	fetchNumberOfAttachedTestPapers();

	$('#view-illustration').on('click', function (e) {
		e.preventDefault();
		window.location = 'view-illustration.php?test_program='+test_program+'&chapter='+chapter+'&subject='+subject;
	})
});


function getProgramDetails() {
	var	req = {};
	req.test_program = test_program;
	req.action = 'getProgramDetails';

	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req) 
	}).done(function(res) {
		res = $.parseJSON(res);
		console.log(res);
		if(res.status == 1) {
			//setBreadcrumb(res.subjectList);
			$('a.program-name').html(res.details.name);
			$('a.program-name').attr('href','student-course-details.php?program_id='+test_program);
		}
		else {
			toastr.error(res.message);
		}
	});

}

// function fetch Test Program Details Fromm DB
function fetchTestProgramDetails() {
	var req = {};
	req.action = "fetchTestProgramDetails";
	req.test_program = test_program;

	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			setBreadcrumb(res.details,res.subjectsList,res.chapters);
			
			// $('.test-program').text(res.details.name);
			// $('a.test-program').attr('href','view-test-program.php?test_program='+res.details.id);
			// $('#test-program-name').val(res.details.name);
			// $('#test-program-status').val(res.details.status);
			// $('#test-program-class').val(res.details.class);
			// $('#test-program-subjects').val(subjects).select2();
			// $('#test-program-price').val(res.details.price);
			// $("#cut-off").val(res.details.cut_off);
			// $('#test-program-description').html(res.details.description);
			// $("#test-program-image").attr('src', '../'+res.details.image);
		}

	});
}



function setEventViewTest() {
	$('.view-test').off('click');
	$('.view-test').on('click', function(e) {
		e.preventDefault();
		type = $(this).parents('.dashboard-stat').data('category');
		window.location = 'view-course-details.php?program_id='+test_program+'&type='+type+'&parent_id='+chapter+'&parent_type=2&subject='+subject;
	})
}



/*
* function to  show how number of papers attached
*/
function fetchNumberOfAttachedTestPapers() {
	var req = {};
	req.action = "fetchNumberOfAttachedTestPapers";
	req.test_program = test_program;
	req.parent_id = chapter;
	req.parent_type = 2;
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			
			// console.log(res);
			if(res.data.chapter != undefined){
				$('#chapter-test .count').text(res.data.chapter);
			}
			if(res.data.practice != undefined){
				$('#practice-test .count').text(res.data.practice);
			}
			if(res.data.illustration != undefined){
				$('#illustration .count').text(res.data.illustration);
			}
		}
		else {
			toastr.error(res.message);
		}
	});
}


setBreadcrumb = function(details,subjects,chapters) {
	$.each(subjects, function(i, sub){
		if(sub.id==subject){
			$('.subject').html(sub.name);
			$('a.subject').attr('href','student-course-chapters.php?program_id='+test_program+'&subject='+subject);
			$('a.program-name').attr('href','student-course-details.php?program_id='+test_program);	
			
		}
	});

	$.each(chapters, function(i, chap){
		if(chap.chapter_id==chapter){
			$('.chapter').html(chap.chapter_name);
			$('.program-name').html(chap.chapter_name);
			$('a.chapter').attr('href','student-course-chapter-tests-types.php?program_id='+test_program+'&subject='+subject+'&chapter='+chapter);
			
		}
	});

	


}