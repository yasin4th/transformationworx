$(function() {
	
	/*$('#firstname').blur(function() {
		unsetError($(this));		
		if ($('#firstname').val() == '') {
			setError($(this), 'First name required.');
			return;
		}
	});

	$('#email').blur(function() {
		unsetError($(this));
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!regex.test($(this).val())) {
			setError($(this), 'Please enter your correct E-mail.');
			return;
		}
	});
	
	$('#password').blur(function() {
		unsetError($(this));
			$('#confirm-password').blur();
		if ($('#password').val() == '') {
			setError($(this), 'Password required.');
			return;
		}
		if ($('#password').val() < 6) {
			setError($(this), 'Password atleast six characters long.');
			return;
		}
	});
	
	$('#confirm-password').blur(function() {
		unsetError($(this));
		if ($('#password').val() != $('#confirm-password').val()) {
			setError($(this), 'Password not matched.');
			return;
		}
		unsetError($(this));
		if ($('#confirm-password').val() == '') {
			setError($(this), 'Please enter password two times.');
			return;
		}
	});*/
	$('#reset').on('click', function(e){
		e.preventDefault();
		$('.register-div').find('input:text').val('');
		 $('#email').val('');
		 $('.register-div').find('input:password').val('');
	});
	
	$('#registration-form').on('submit', function(e) { 
		e.preventDefault();
		
		if(validate()){
			var req = {};
			req.action = "register";
			req.firstname = $('#firstname').val();
			req.lastname = $('#lastname').val();
			req.email = $('#email').val();
			req.password = $('#password').val();
			req.mobile = $('#mobile').val();
			req.class = '';//$('#class').val();
			
			$.ajax({
				'type'	:	'post',
				'url'	:	'vendor/api.php',
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				// console.log(res);
				if(res.status == 1)
				{
					location.replace("student-profile.php");
				}
				else
				{
					alert("Error:"+res.message);
					toastr.error(res.message);
				}
			});
		}
	});
});

validate = function() {
	var pass = $('#password').val();
	var confirm = $('#confirm-password').val();
	var regx = /^[0-9]*$/;

	if($('#firstname').val() != "" && $('#firstname').val().length<3){
		toastr.error("please enter alteast 3 characters in firstname.");
		return false;
	}

	if($('#lastname').val() != "" && $('#lastname').val().length<3){
		toastr.error("please enter alteast 3 characters in lastname.");
		return false;
	}

	if(!regx.test($('#mobile').val()))
	{
		toastr.error("please enter valid mobile no.");
		return false;
	}

	if($('#mobile').val() != "" && $('#mobile').val().length<10){
		toastr.error("please enter valid mobile no.");
		return false;
	}

	if($('#mobile').val() != "" && $('#mobile').val().length>11){
		toastr.error("please enter valid mobile no.");
		return false;
	}

	
	if($('#password').val() != "" && $('#password').val().length<6){
		toastr.error("please enter atleast six characters long password");
		return false;
	}
	if(confirm != pass){
		toastr.error("Confirm password incorrect.");
		return false;
	}
	

	
	return true;
}