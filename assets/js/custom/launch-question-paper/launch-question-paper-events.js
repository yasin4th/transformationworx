/*
 * when user click start test this function launchs test-paper
 */
function startTest() {
	if (test_paper_type != '6') {
		startTimer(duration, display);
	}
	setEventSectionChange();
	setEventSaveAndNext();
	setEventMarkAndReviewNext();
	setEventClearResponse();
	setEvengGetQuestionByNumbering();
	setEventShowAllQuestionsModal();
	setEventSubmit();

	initSpecialPlugin();
	Layout.init();
	//resumetest();
	$("#sections .section").eq(0).click();
	$('#go-to-test-history').attr('href', "my-test-history.php?attempt=" + attempt_id);
	// Metronic.init();
}


function setEventSectionChange() {
	$('.section').off("click");
	$('.section').on("click", function(e) {
		e.preventDefault();

		/*
		 * first we will hidden the divisions which are opened
		 */
		divToClose = $("#sections .section.blue").data("s_id");

		$('#sections [data-s_id=' + divToClose + ']').addClass("btn-info");
		$('#sections [data-s_id=' + divToClose + ']').removeClass("blue");

		questions = $("#questions").find('[data-s_id=' + divToClose + ']').addClass('hidden').find('.question');
		$.each(questions, function(i, question) {
			$(question).addClass('hidden').removeClass('active');
		})

		$("#question-numbering").find('[data-s_id=' + divToClose + ']').addClass('hidden');


		/*
		 * now we will show the section's divisions which is clicked
		 */
		divToOpen = $(this).data("s_id");

		$('#sections [data-s_id=' + divToOpen + ']').removeClass("btn-info");
		$('#sections [data-s_id=' + divToOpen + ']').addClass("blue");

		$("#questions").find('[data-s_id=' + divToOpen + ']').removeClass('hidden').find('.question').eq(0).removeClass('hidden').addClass('active');

		$("#question-numbering").find('[data-s_id=' + divToOpen + ']').removeClass('hidden');
		$("#question-numbering").find('[data-s_id=' + divToOpen + '] a').eq(0).click();
		// $("#question-numbering").find('[data-s_id='+divToOpen+'] a').eq(0).removeClass("btn-default").addClass("red");

	});
}

function setEvengGetQuestionByNumbering() {
	$('.numbering').off('click');
	$('.numbering').on('click', function(e) {
		e.preventDefault();
		index = $(this).data('index');
		s_id = $(this).parent('div').data('s_id');
		//console.log(s_id);

		// hiding the active question
		$('#questions').find('[data-s_id=' + s_id + ']').find('.active').addClass('hidden').removeClass('active');

		// checking if this question is not saved(green) or marked(blue) then show not answered(red)
		if (!($(this).hasClass("blue")) && !($(this).hasClass("green"))) {
			QU[index].status = 1;
			$("#clear-response").attr("disabled", true);
			$(this).addClass("red").removeClass("btn-default");

			// sending ajaxto save question status
			var req = {};
			req.action = "saveQuestionResponse";
			req.row = QU[index];
			req.attempt_id = attempt_id;
			req.testPaper = paper;

			$.ajax({
				'type': 'post',
				'url': EndPoint,
				'data': JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if (res.status == 1) {
					//console.log(res)

					// toastr.info('Status Saved',"Question:");	
				} else
					toastr.error(res.message, "ERROR:")
			});
		} else {
			$("#clear-response").attr("disabled", false);
		}

		// showing the selected question 
		$("#questions").find('[data-index=' + index + ']').addClass('active').removeClass('hidden');
		$("#questions").find('input.input-droppable').attr('size','16').attr('style','height:26px;');
		$("#questions .fb-question").find('input.input-droppable').attr('type','text').removeAttr('disabled');
		/*
		// if this question is last question of this section then disable some buttons
		index++;
		if($("#question-numbering").find('[data-index='+index+']').length == 0)
			$("#save-next").attr('disabled',true);
		else
		*/
	})
}

function setEventSaveAndNext() {
	$("#save-next").off('click');
	$("#save-next").on('click', function(e) {
		e.preventDefault();
		// var req = {};
		// req.action = "saveOption";
		// req.question_id = $("#questions").find('.active').data('q_id');

		// req.answer = $("#questions").find('.active').find('input:checked');

		s_id = $("#questions").find('.active').data("section_id");
		index = $("#questions").find('.active').data("index");

		if (getAnswer().length != 0) {
		//alert("saveNext");

			QU[index].status = 3;
			QU[index].answers = getAnswer();

			var req = {};
			req.action = "saveQuestionResponse";
			req.row = QU[index];
			req.attempt_id = attempt_id;
			req.testPaper = paper;

			saveQuestionResponse(req); // calling funtion to send ajax

			$("#question-numbering").find('[data-index=' + index + ']').addClass('green').removeClass('blue').removeClass('red');


			if ($('#question-numbering [data-s_id=' + s_id + ']').find('[data-index=' + (index + 1) + ']').length > 0) {
				$("#questions").find('.active').addClass('hidden').removeClass('active');
				// hiding the active question
				$("#question-numbering").find('[data-index=' + (index + 1) + ']').click();
				// /console.log($('#questions').find('.active input'));
				// hiding the active question

			} else {
				toastr.info('This is last question Of this section.', 'Please open next section.');
			}
		};

	})
}

function setEventMarkAndReviewNext() {
	$("#mark-review-next").off('click');
	$("#mark-review-next").on('click', function(e) {
		e.preventDefault();
		var req = {};
		req.action = "markOption";
		req.question_id = $("#questions").find('.active').data('q_id');

		s_id = $("#questions").find('.active').data("section_id");
		index = $("#questions").find('.active').data("index");
		// $.ajax({})


		QU[index].status = 2;

		QU[index].answers = getAnswer();

		var req = {};
		req.action = "saveQuestionResponse";
		req.row = QU[index];
		req.attempt_id = attempt_id;
		req.testPaper = paper;
		saveQuestionResponse(req); // calling funtion to send ajax


		$("#question-numbering").find('[data-index=' + index + ']').addClass('blue').removeClass('green').removeClass('red');
		if ($('#question-numbering [data-s_id=' + s_id + ']').find('[data-index=' + (index + 1) + ']').length > 0) {
			$("#questions").find('.active').addClass('hidden').removeClass('active');
			// hiding the active question
			$("#question-numbering").find('[data-index=' + (index + 1) + ']').click();
			// hiding the active question
		} else {
			toastr.info('This is last question Of this section.', 'Please open next section.');
		}

	})
}

function setEventClearResponse() {
	$("#clear-response").off('click');
	$("#clear-response").on('click', function(e) {
		e.preventDefault();
		var req = {};
		req.action = "clearResponseOfThisQuestion";
		req.question_id = $("#questions").find('.active').data('q_id');

		s_id = $("#questions").find('.active').data("section_id");
		index = $("#questions").find('.active').data("index");


		QU[index].status = 1;
		QU[index].answers = [];

		var req = {};
		req.action = "saveQuestionResponse";
		req.attempt_id = attempt_id;
		req.row = QU[index];
		req.testPaper = paper;
		saveQuestionResponse(req); // calling funtion to send ajax

		$("#question-numbering").find('[data-index=' + index + ']').addClass('red').removeClass('green').removeClass('blue');

		$("#questions").find('.active').find('input').prop('checked', false);
	})
}


/*
 * this function sends ajax to update a question's data
 */
function saveQuestionResponse(req) {
	$.ajax({
		'type': 'post',
		'url': EndPoint,
		'data': JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 1) {
			toastr.info('Status Saved', "Question:");
		} else
			toastr.error(res.message, "ERROR:");
	});
}

/*
 * function fetch the answer from the html of active question
 */
function getAnswer() {
	index = $("#questions").find('.active').data('index');
	answers = [];
	switch (parseInt(QU[index].type)) {
		/*
		 * case for SCQ question
		 */
		case 1:
			$.each($("#questions").find('.active').find('input:checked'), function(i, answer) {
				answers.push($(answer).parents('.option').data('id'));
			})
			break;

			/*
			 * case for MCQ question
			 */
		case 2:
			$.each($("#questions").find('.active').find('input:checked'), function(i, answer) {
				answers.push($(answer).parents('.option').data('id'));
			})
			break;

			/*
			 * case for  question
			 */
		case 3:

			break;

			/*
			 * case for Sequencing question
			 */
		case 4:
			$.each($("#questions").find('.active').find('li'), function(i, answer) {
				answers.push($(answer).data('id'));
			})
			break;

			/*
			 * case for MATCHING question
			 */
		case 5:
			$.each($("#questions").find('.active').find('.answers .option'), function(i, answer) {
				answers.push($(answer).data('id'));
			});
			break;

			/*
			 * case for Fill in the blanks question
			 */
		case 6:
			$.each($("#questions").find('.active').find('input:text'), function(i, answer) {
				answers.push($(answer).val());
			});
			break;

			/*
			 * case for Fill in the blanks question DROP DOWN
			 */
		case 7:
			$.each($("#questions").find('.active').find('select'), function(i, answer) {
				answers.push($(answer).val());
			});
			break;

			/*
			 * case for Fill in the blanks question Drag and Drop
			 */
		case 8:
			$.each($("#questions").find('.active').find('input:text'), function(i, answer) {
				answers.push($(answer).val());
			});
			break;

			/*
			 * case for passage question
			 */
		case 11:
			$.each($("#questions").find('.active').find('input:checked'), function(i, answer) {
				answers.push($(answer).parents('.option').data('id'));
			})
			break;

			/*
			 * case for Matrix question
			 */
		case 12:
			$.each($("#questions").find('.active').find('input:checked'), function(i, answer) {
				answers.push($(answer).parents('.option').data('id'));
			})

			break;

			/*
			 * case for Integer question
			 */
		case 13:
			$.each($("#questions").find('.active').find('input:text'), function(i, answer) {
				answers.push($(answer).val());
			});
			break;


		default:

			break;
	}
	return answers;
}

/*
 * function to set Event of Show All Questions of selected section
 */
function setEventShowAllQuestionsModal() {
	$('#show-all-questions-modal').off("click");
	$('#show-all-questions-modal').on("click", function(e) {
		e.preventDefault();

		$('#all-questions-modal .section-questions').addClass("hidden");

		/*
		 * getting the selected section
		 */
		s_id = $("#sections .section.blue").data("s_id");

		$('#all-questions-modal [data-s_id=' + s_id + ']').removeClass('hidden');

		$('#all-questions-modal').modal('show');
	});
}

/*
 * function to set submit event
 */
function setEventSubmit() {
	$('#submit').off('click');
	$('#submit').on('click', function(e) {
		e.preventDefault();

		$('#submit').attr('disabled', true);
		$('.container-fluid').addClass('hidden')
		$("#result-modal").modal({
			backdrop: 'static'
		}, 'show');
		$('#result-progressbar-div').show('slow');

		var req = {};
		req.rows = QU;
		req.action = "submitPaper";
		req.attempt_id = attempt_id;
		req.testPaper = paper;
		$.ajax({
			'type': 'post',
			'url': EndPoint,
			'data': JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if (res.status == 1) {
				$('.optmizer').hide();
				toastr.success("Your Paper Successfully Submited.", "Saved");
				clearInterval(clock);
				$('#result-progressbar-div').hide('slow');
				if (res.result_status.status == 1) {
					if (res.result_status.test_type != 0) {
						$('.optmizer').show()
					};
					$('#result-marks-div').show('slow');
					$('#marks-archived').text(res.result_status.marks_archived);
					$('#total-marks').text(res.result_status.total_marks);
					$('#go-to-test-history').attr('disabled', false);
					$("#create-test-optmizer").attr('disabled', false);
					setEventCreateOptmizer();

				} else {
					toastr.error(res.message, "ERROR:");
				};
			} else {
				toastr.error(res.message, "ERROR:");
			}
		});
	});
}

function setEventCreateOptmizer() {
	$("#create-test-optmizer").off('click');
	$("#create-test-optmizer").on('click', function(e) {
		e.preventDefault();
		createOptmizerTest();
	});
}

/*
 * create optimzer test of wrong and unattempted questions
 */
function createOptmizerTest() {
	var req = {};
	req.action = "createOptmizerTest";
	req.attempt_id = attempt_id;
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 1) {
			window.location = "launch-question-paper.php?paper=" + res.test_paper_id;
		} else {
			console.log(res);
			toastr.error(res.message, "Error:")
		};
	});
}

/*
 * function to init spcial functionn i.e. (SHORTABLE, DRAGABLE, DROPABLE.... )
 */
function initSpecialPlugin() {
	if ($('.sequencing-list').length)
		$('.sequencing-list').sortable();
	if ($('.input-droppable').length) {
		$('.input-droppable').droppable({
			drop: function(event, ui) {
				console.log(ui.draggable.text());
			}
		});
	}
	if ($('.draggable').length) {
		$('.draggable').draggable({
			revert: "invalid"
		});
	}
	if ($('.fb-input').length) {
		$('.fb-input').attr('disabled', false);
		$('.fb-input').attr('maxlength', 50);
	}
}


/*
 * CountDown Timer
 */
var duration = 60; // sec
display = $('#countdown-timer');

function startTimer(duration, display) {
	var timer = duration,
	minutes, seconds;
	clock = setInterval(function() {
		hours = parseInt(timer / (60 * 60), 10);
		minutes = parseInt((timer - (hours * 60 * 60)) / 60, 10);
		seconds = parseInt(timer % 60, 10);

		hours = hours < 10 ? "0" + hours : hours;
		minutes = minutes < 10 ? "0" + minutes : minutes;
		seconds = seconds < 10 ? "0" + seconds : seconds;

		display.text("Time left : "+hours + ":" + minutes + ":" + seconds);


		//QU[$("#questions").find('.active').data("index")].time++; //this line to save  the student taken on a question

		/*
		 * This Event Fires when time Over
		 */
		 if (--timer < 0) {
		 	toastr.info('Paper Submited', 'Time Over');
		 	$('#submit').click();
			// clearInterval(clock);
		}
	}, 1000);
}