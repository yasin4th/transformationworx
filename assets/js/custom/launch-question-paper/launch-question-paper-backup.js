var QU;
var paper;
var attempt_id;
$(function() {
	paper = getUrlParameter("paper");
	launchPaper();
});

/*
* this function send test-paper-id and get the data to create test-paper
*/
function launchPaper() {
	var req = {};
	req.action = "launchPaper";
	req.testPaper = paper;
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			console.log(res)
			attempt_id = res.attempt_id;
			QU = res.Q;
			$('.test-paper-name').text(res.test_paper_details['name']);
			duration = res.test_paper_details['time'] * 60;
			genrateQuestionPaper(res.data);
		}
		else
			toastr.error(res.message,"ERROR:")
	});
}


/*
* this function genrate html of test paper
* 
*/
function genrateQuestionPaper(sections) {
	questionIndex = 0;
	var section_html = "";
	var questions_html = "";
	var question_numbering_html = "";
	var allQuestionsHtml = "";
	

	$.each(sections, function(i,section) {
		section_html += '<button data-s_id="'+section.section.id+'" data-totalQuestion="'+section.section.total_question+'" type="button" class="section btn btn-info" style="margin: 5px;">'+section.section.name+'</button>';

		questions_html += '<div data-s_id="'+section.section.id+'" class="hidden">';
		question_numbering_html += '<div data-s_id="'+section.section.id+'" class="hidden">';
		allQuestionsHtml += '<div data-s_id="'+section.section.id+'" class="hidden section-questions"><table class="table table-striped table-hover table-bordered"><thead><tr><th><i class="fa fa-caret-down"></i> Sr No </th><th><i class="fa fa-caret-down"></i> Question </th></tr></thead><tbody>';

		$.each(section.section.questionData, function(x,question) {
			questionIndex++;
			question_numbering_html += '<a data-index="'+questionIndex+'" class="numbering deleteOption btn btn-circle btn-icon-only btn-default" style="margin: 3px;">'+(x+1)+'</a>';
			// QU[questionIndex] = {};
			// QU[questionIndex].status = 0;
			// QU[questionIndex].answers = [];
			// // QU[question.question_id].type = question.type;
			// QU[questionIndex].question_id = question.question_id;
			// QU[questionIndex].time = 0;

			questions_html += '<div data-index="'+questionIndex+'" data-q_id="'+question.question_id+'" data-section_id="'+question.section_id+'" class="question hidden"><div class="from-group"><label><strong>Question No. '+(x+1)+'</strong></label><br></div>';
			
			allQuestionsHtml += '<tr><td>'+(x+1)+'</td><td> '+question.question+' </td></tr>'; 

			questions_html += getQuestionHtmlByType(question);
			// questions_html += '<div class="col-md-12"><div class="row border-top-bbb"><div class="col-md-12 border-right-bbb mrg15"><h5><strong>'+question.question+'</strong></h5>';
			// questions_html += '<div class="radio-list">';
			
			// $.each(question.options,function(z,option) {

			// 	questions_html += '<div data-id='+option.id+' class="option col-md-12 col-sm-12"> <label style="display:block"> <div class="pull-left"> <input type="radio" name="'+radioButtonName+''+x+'">&nbsp;</div> <div class="pull-left">'+option.option+' </div> </label></div>';
			// });
			// questions_html += '</div>';
			// questions_html += '</div> </div> </div>';
			
			questions_html += '</div>';
		}); 
		allQuestionsHtml += '</tbody></table></div>';
		questions_html += '</div>';
		question_numbering_html += '</div>';
	});
	$("#question-numbering").html(question_numbering_html);
	$("#questions").html(questions_html);
	$("#sections").html(section_html);
	$("#all-questions-modal .modal-body").html(allQuestionsHtml);
	mathsjax();
	startTest();
}

function getQuestionHtmlByType(question) {
	html = ''
	switch(parseInt(question.type)) {
		case 1:
			radioButtonName = parseInt(Math.random() * 1000);
			html += '<div class="col-md-12"><div class="row"><div class="col-md-12 mrg15 mrg-top10"><h5><strong>'+question.question+'</strong></h5>';
			html += '<div class="radio-list">';
			$.each(question.options,function(z,option) {
				html += '<div data-id='+option.id+' class="option col-md-12 col-sm-12"> <label style="display:block"> <div class="pull-left"> <input type="radio" name="'+radioButtonName+'">&nbsp;</div> <div class="pull-left">'+option.option+' </div> </label></div>';
			});
			html += '</div>';
			html += '</div> </div> </div>';
			break;

		case 2:
			html += '<div class="col-md-12"><div class="row"><div class="col-md-12 mrg15 mrg-top10"><h5><strong>'+question.question+'</strong></h5>';
			html += '<div class="radio-list">';
			$.each(question.options,function(z,option) {
				html += '<div data-id='+option.id+' class="option col-md-12 col-sm-12"> <label style="display:block"> <div class="pull-left"> <input type="checkbox">&nbsp;</div> <div class="pull-left">'+option.option+' </div> </label></div>';
			});
			html += '</div>';
			html += '</div> </div> </div>';
			break;
		
		case 3:
			radioButtonName = parseInt(Math.random() * 1000);
			html += '<div class="col-md-12"><div class="row"><div class="col-md-12 mrg15 mrg-top10"><h5><strong>'+question.question+'</strong></h5>';
			html += '<div class="radio-list">';
			$.each(question.options,function(z,option) {
				html += '<div data-id='+option.id+' class="option col-md-12 col-sm-12"> <label style="display:block"> <div class="pull-left"> <input type="radio" name="'+radioButtonName+'">&nbsp;</div> <div class="pull-left">'+option.option+' </div> </label></div>';
			});
			html += '</div>';
			html += '</div> </div> </div>';
			break;

		case 4:
			html += '<div class="col-md-12"><div class="row"><div class="col-md-12 mrg15 mrg-top10"><h5><strong>'+question.question+'</strong></h5>';
			html += '<ul class="sequencing-list">';
			$.each(question.options,function(z,option) {
				html += '<li data-id='+option.id+' class="option mrg-bot5"><div class="row"><div class="btn btn-default col-md-4"> <label draggable="true" style="display:block"> <div class="pull-left">'+option.option+' </div> </label></div></div></li>';
			});
			html += '</ul>';
			html += '</div> </div> </div>';
			break;

		case 5:
			html += '<div class="col-md-12"><div class="row"><div class="col-md-12 mrg15 mrg-top10"><h5><strong>'+question.question+'</strong></h5>';
			html += '<div class="row">';
			html += '<div class="col-md-6"> <div class="col-md-11" > <div class="row" style="border: 1px solid #ccc; padding: 10px 10px 0px;">';
			$.each(question.options,function(z,option) {
				if(option.number == 1) {
					html += '<div data-id='+option.id+' class="col-md-12 mrg-bot10" style="border: 1px solid #ccc;">'+option.option+'</div>';
				}
			});
			html += '</div> </div> </div>'
			html += '<div class="col-md-6"> <div class="col-md-11" > <div class="row answers sequencing-list" style="border: 1px solid #ccc; padding: 10px 10px 0px;">';
			$.each(question.options,function(z,option) {
				if(option.number == 2) {
					html += '<div data-id='+option.id+' class="col-md-12 mrg-bot10 option" style="border: 1px solid #ccc;">'+option.option+'</div>';
				}
			});
			html += '</div> </div> </div>';
			html += '</div>';
			html += '</div> </div> </div>';
			break;

		case 6:
			html += '<div class="col-md-12"><div class="row"><div class="col-md-12 mrg15 mrg-top10"><h5><strong>'+question.question+'</strong></h5>';
			html += '<div class="Fb-list">';
			html += '</div>';
			html += '</div> </div> </div>';
			break;

		case 7:
			$('body').append('<div id="virtual-div"></div>');
			$('#virtual-div').html(question.question);
			dropDowns = $('#virtual-div select');
			$.each(dropDowns,function(x,dropDown) {

				$(dropDown).attr('disabled', false);
				$.each(question.options,function(z,option) {
					$(dropDown).append('<option value='+option.id+'> '+option.option+'</option>');
				});
			});
			virtualQuestion = $('#virtual-div').html();
			$('#virtual-div').remove();

			html += '<div class="col-md-12"><div class="row"><div class="col-md-12 mrg15 mrg-top10"><h5><strong>'+virtualQuestion+'</strong></h5>';
			html += '<div class="Fb-list input-droppable">';
			html += '</div>';
			html += '</div> </div> </div>';
			break;

		case 8:
			html += '<div class="col-md-12"><div class="row"><div class="col-md-12 mrg15 mrg-top10"><h5><strong>'+question.question+'</strong></h5>';
			html += '<div class="Fb-list input-droppable">';
			$.each(question.options,function(z,option) {
				html += '<span data-id='+option.id+' class="option mrg-bot5"> <div class="mrg-bot10"><span class="draggable border-ccc">'+option.option+' </span></div> <!--<div class="row"><div class="draggable btn btn-default col-md-4"> <label draggable="true" style="display:block"> <div class="pull-left"><span class="">'+option.option+' </span></div> </label></div></div>--></span>';
			});
			html += '</div>';
			html += '</div> </div> </div>';
			break;

		case 9:
			break;

		case 10:
			break;

		case 11:
				html = '<div class="col-md-12"> <div class="row border-top-bbb">';
				/*
				* adding passage
				*/
				html += '<div class="col-md-6 border-right-bbb mrg15"> <div class="row"> <div class="scroller QuestionDetailsHeight" data-always-visible="1" data-rail-visible="0"> <div class="col-md-12 mrg-top10">'+question.passage;
				html += '</div> </div> </div> </div>';
				
				/*
				* adding questtion of passage
				*/
				html += '<div class="col-md-6">';
					question.type  = question.child_type;
					html +=  getQuestionHtmlByType(question);;
				html += '</div>';

				html += '</div> </div>';			
			break;

		case 12:
			html += '<div class="col-md-12"><div class="row"><div class="col-md-12 mrg15 mrg-top10"><h5><strong>'+question.question+'</strong></h5>';

			html += '<div id="matrix-options-div" class="col-md-11 matrix-table-options"> <div class="margin-bottom-10 table-scrollable"><table id="matrix-table" class="table table-bordered table-striped table-hover"><thead><tr><th></th>';

			/*
			* creating table head
			*/
			$.each(question.options, function(z,option) {
				if(option.answer == 1) {
					html += '<th> <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;">'+ option.option +'</div> </th>';
				}
			});
			html += '</tr> </thead> ';
			
			/*
			* creating table body
			*/
			var row = 2;
			html += '<tbody>'
			$.each(question.options ,function(i,option) {
				if(option.answer == row) {
					html += '<tr> <td> <div style="border: 1px solid rgb(232, 227, 245); width: 100%; min-height: 50px;"> '+option.option+' </div></td>';
					option.answer = 0;
					$.each(question.options ,function(x,opt) {
						if(opt.answer == row) {
							html += '<td><div class="checkbox-list"><label><input type="checkbox"></label></div></td>';
						}
					});				
					row++;
				}
			});
			html += '</tbody></table></div> </div>';
			html += '</div> </div> </div>';

			break;

		case 13:
			break;

		default:
			break;
	}
	return html;
}



function resumetest() {
	$.each(QU, function(i,Question) {
		if(QU[i].status > 0)
			if(QU[i].status == 1) {
				$('.numbering[data-index="'+i+'"]').addClass("red").removeClass("btn-default");
			}
			else if(QU[i].status == 2) {
				$('.numbering[data-index="'+i+'"]').addClass("blue").removeClass("btn-default");
			}			
			else if(QU[i].status == 3) {
				$('.numbering[data-index="'+i+'"]').addClass("green").removeClass("btn-default");
			}
			
			resumeWithSavedAnswers(i);

			// console.log(QU[i].status);
	});
}

function resumeWithSavedAnswers(i) {
	switch(QU[i].type) {
		case '1':
			$.each(QU[i].answers, function(x,answer){
				$('#questions').find('[data-index="'+i+'"]').find('.option[data-id="'+answer+'"]').find('input').attr('checked', true);
			})
		break;
		
		case '2':
			$.each(QU[i].answers, function(x,answer){
				$('#questions').find('[data-index="'+i+'"]').find('.option[data-id="'+answer+'"]').find('input').attr('checked', true);	
			})
		break;
		
		case '4':
			if(QU[i].answers.length) {
				html = '';
				$.each(QU[i].answers, function(x,answer){
					html += $('#questions').find('[data-index="'+i+'"]').find('.sequencing-list li[data-id="'+answer+'"]').get(0).outerHTML;	
				})
				$('#questions').find('[data-index="'+i+'"]').find('.sequencing-list').html(html);
			}
		break;
		
		default:
		break;
	}

}