var program,paper,paper_type,paper_duration,paper_attempt,manual=true;
var obQuestions = [];

$(function(){

	$(document).on("contextmenu",function(e){
        return false;
    });
    
    paper = 18;
	program = 1;
    fetchQuestionPaper();
})

fetchQuestionPaper = function(){
	var req = {};
	req.action = "fetchQuestionPaper";
	req.demo = 1;
	$.ajax({
		'type': 'post',
		'url': EndPoint,
		'data': JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 1) {
			paper_type = res.test_paper_details[0].type;
			$('#test_paper_instructions').html(res.test_paper_details[0].Instructions);
			$('.test-paper-name').html(res.test_paper_details[0].name);
			if(paper_type == 5 || paper_type == 6 || paper_type == 0)
			{
				$('#create-test-optmizer').remove();
				$('.optmizer').remove();
			}
			if(paper_type == 6)
			{
				$('#submit').html("Close Test");
			}

			paper_attempt = res.paper_attempt_id;
			$('#go-to-test-history').attr('href', "my-test-history.php?attempt=" + res.paper_attempt_id);

			paper_duration = res.test_paper_details[0].time * 60;
			storeQuestions(res.questions, paper_type);
			displaySections(res.sections);
		}
		else if(res.status == 2) // not logged in
		{
			//window.location = "login.php";
		}
		else // no access
		{
			//window.location = "my-program";
		}
	});
}

storeQuestions = function(question,type) {
	$.each(question, function(i,q){
		obQuestions.push(new Questions(q.section_id,q.lable_id,q.question_id,q.question,q.type,q.time,q.answers,q.parent_id,q.parent_question,q.marks,q.neg_marks,q.qstatus,q.options,q.description,false,q.question_attempt_id));
	})
	displayNumbers();
}

displayNumbers = function() {
	html = '';
	$.each(obQuestions, function(i,q){
		html += '<a data-index="'+i+'" class="numbering deleteOption btn btn-circle btn-icon-only btn-default" style="padding: 7px 0; margin: 3px;">'+(i+1)+'</a>';
	})
	$('#question-numbering').html(html);
	Questions.prototype.setColor();

	$('.numbering').click(function(e){
		$.each(obQuestions, function(i,question){
			this.active = false;
		});
		obQuestions[$(this).data('index')].displayQuestion(paper_type);
		obQuestions[$(this).data('index')].activateOptions();
		obQuestions[$(this).data('index')].setAnswers();
		obQuestions[$(this).data('index')].active= true;

		section_id = obQuestions[$(this).data('index')].section_id;

		$('button.section').addClass('btn-info');
		$('button.section').removeClass('blue');
		//$('button.section')
		$.each($('button.section'),function(i,section) {
			if($(this).data('s_id') == section_id)
			{
				$('button.section').eq(i).addClass('blue');
			}
		})

		//$('button data-s_id='+obQuestions[$(this).data('index')].section_id).;

		if(obQuestions[$(this).data('index')].qstatus=='0'){
			//obQuestions[$(this).data('index')].qstatus= '1';
		}		
		Questions.prototype.setColor();
	})
}

displaySections = function(sections) {
	html = '';
	$.each(sections, function(i,s){
		html += '<button data-s_id="'+s.id+'" type="button" class="section btn btn-info" style="margin: 5px;">'+s.name+'</button>';
	})
	$('#sections').html(html);

	$('button.section').click(function(e){
		$('button.section').addClass('btn-info');
		$('button.section').removeClass('blue');
		$(this).addClass('blue');
		Questions.prototype.jumpSection($(this).data('s_id'));
	});
	//first time click
	$('button.section').eq(0).click();
	paperEvents();
}

paperEvents = function() {
	$('#btn-review').click(function(e){
		index = Questions.prototype.getActive();
		obQuestions[index].storeAnswers();
		if(obQuestions[index].answers.length > 0)
		{
			obQuestions[index].qstatus = '2';			
		}
		else
		{
			obQuestions[index].qstatus = '1';			
		}
		Questions.prototype.setColor();
		if(paper_type != '6'){
			Questions.prototype.saveQuestion();
		}
		$('.numbering').eq(index+1).click();
	});

	$('#save').click(function(e){
		index = Questions.prototype.getActive();
		obQuestions[index].storeAnswers();
		if(obQuestions[index].answers.length > 0)
		{
			obQuestions[index].qstatus = '3';
		}
		else
		{
			obQuestions[index].qstatus = '0';
		}
		Questions.prototype.setColor();
		if(paper_type != '6'){
			Questions.prototype.saveQuestion();
		}
		if(index+1 >= obQuestions.length){
			alert('You have reached end of the question paper.');
		}else{
			$('.numbering').eq(index+1).click();
		}
	});

	$('#submit').click(function(e){
		e.preventDefault();
		if(manual){
			if(confirm("Are you sure you want to submit"))
			{
				$.removeCookie('paper');
				$.removeCookie('program');
				if(paper_type == '6')
				{
					window.location = "test-papers.php?program_id="+program;
				}
				else
				{
					$('#submit').attr('disabled', true);
					$('.container-fluid').addClass('hidden');
					$("#result-modal").modal({
						backdrop: 'static'
					}, 'show');

					$('#result-progressbar-div').show('slow');

					index = Questions.prototype.getActive();
					obQuestions[index].storeAnswers();
					
					if(obQuestions[index].answers.length > 0)
					{
						obQuestions[index].qstatus = '3';
					}
					else
					{
						obQuestions[index].qstatus = '0';
					}

					Questions.prototype.setColor();
					if(paper_type != '6'){
						Questions.prototype.saveQuestionPaper();
					}
				}
			}
		}
		else
		{
			if(paper_type == '6')
			{
					window.location = "test-papers.php?program_id="+program;
			}else{

				$('#submit').attr('disabled', true);
				$('.container-fluid').addClass('hidden');
				$("#result-modal").modal({
					backdrop: 'static'
				}, 'show');

				$('#result-progressbar-div').show('slow');

				index = Questions.prototype.getActive();
				obQuestions[index].storeAnswers();
				obQuestions[index].qstatus = '3';
				Questions.prototype.setColor();
				if(paper_type != '6')
				{
					Questions.prototype.saveQuestionPaper();
				}
			}
		}
	});

	$('#clear').click(function(e){
		index = Questions.prototype.getActive();
		if(obQuestions[index].type == '8'){
			$('.input-droppable').droppable('destroy');
		}
		obQuestions[index].qstatus = '0';
		obQuestions[index].answers = [];
		$('.numbering').eq(index).click();

	});

	Questions.prototype.displayAllQuestion();
	/*
	 * CountDown Timer
	 */
	var duration = 60; // sec
	if(paper_type != '6'){
		display = $('#countdown-timer');
		startTimer(paper_duration,display);
	}

	function startTimer(duration, display) {
		var timer = duration,
		minutes, seconds;
		clock = setInterval(function() {
			hours = parseInt(timer / (60 * 60), 10);
			minutes = parseInt((timer - (hours * 60 * 60)) / 60, 10);
			seconds = parseInt(timer % 60, 10);

			hours = hours < 10 ? "0" + hours : hours;
			minutes = minutes < 10 ? "0" + minutes : minutes;
			seconds = seconds < 10 ? "0" + seconds : seconds;

			display.text("Time left : "+hours + ":" + minutes + ":" + seconds);

			Questions.prototype.updateTime();

			 if (--timer < 0) {
			 	//toastr.info('Paper Submited', 'Time Over');
			 	manual = false;
			 	$('#submit').click();
				clearInterval(clock);
			}
		}, 1000);
	}
}
function setEventCreateOptmizer() {
	$("#create-test-optmizer").off('click');
	$("#create-test-optmizer").on('click', function(e) {
		e.preventDefault();
		createOptmizerTest();
	});
}

/*
 * create optimzer test of wrong and unattempted questions
 */
function createOptmizerTest() {
	var req = {};
	req.action = "createOptmizerTest";
	req.attempt_id = paper_attempt;
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 1) {
			window.location = "launch.php?program="+program+"&paper=" + res.test_paper_id;
		} else {
			console.log(res);
		};
	});
}