$(function() {
	
	fetchStudentQuestions();
	filter();
})


function fetchStudentQuestions() {
	var req = {};
	req.action = "fetchStudentQuestions";
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req) 
	}).done(function(res) {
		res = $.parseJSON(res);
		 //console.log(res);
		if (res.status == 1) {
			fillQuestions(res.questions);
		}
		else {
			toastr.error(res.message);
		}
	});
}

function fillQuestions(questions) {
	var html = '';
	$.each(questions, function(i, question) {
		html += '<tr data-id="'+question.id+'" data-revision="'+question.revision+'" data-important="'+question.important+'" data-unsolved="'+question.unable_to_solve+'">';
		html += '<td>'+(i+1)+'</td>';
		html += '<td>'+question.type+'</td>';
		// html += '<td>'+question.class+'</td>';
		// html += '<td>'+question.subjects+'</td>';
		html += '<td>'+question.question+'</td>';
		html += '<td><a href="view-question.php?question_id='+question.id+'" title="View"><i class="fa fa-eye"></i></a> <a class="delete" style="color: rgb(230, 12, 12);font-weight: bold;" title="Delete"><i class="fa fa-trash-o"></i></a></td>';
		html += '</tr>';
	})
	$('#questions-table tbody').html(html);
	setEventDeleteQuestion();
}

function setEventDeleteQuestion() {
	$('#questions-table tbody .delete').off('click');
	$('#questions-table tbody .delete').on('click', function(e) {
		e.preventDefault();
		if (confirm('Please confirm if you want delete this question.')) {
			var req = {};
			req.action = "deleteQuestionFromMyQuestionBank";
			req.question_id = $(this).parents('tr:eq(0)').data('id');
			
			$.ajax({
				type: "post",
				url: EndPoint,
				data: JSON.stringify(req) 
			}).done(function(res) {
				res = $.parseJSON(res);
				console.log(res);
				if (res.status == 1) {
					// $(this).parents('tr:eq(0)').remove();

					fetchStudentQuestions();	
					toastr.success(res.message);
				}
				else{
					
				};
			});
		};

	});
}

function filter() {
	$('#filter').off('change');
	$('#filter').on('change', function(e) {
		filter = $('#filter').val();
		$('#questions-table tbody tr').addClass('hidden');
		
		switch(filter) {
			case "1":
				$('#questions-table tbody tr[data-revision="1"]').removeClass('hidden');
				break;

			case "2":
				$('#questions-table tbody tr[data-important="1"]').removeClass('hidden');
				break;

			case "3":
				$('#questions-table tbody tr[data-unsolved="1"]').removeClass('hidden');
				break;

			default:
				$('#questions-table tbody tr').removeClass('hidden');
				break;
		}
	});
}