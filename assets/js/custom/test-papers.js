test_program = getUrlParameter('program_id');
var assignments = {}

$(function(){
	// this is for BCT(BreadCrumb Trail)
	// $('.back-to-subjects').attr('href', 'student-course-details.php?program_id='+test_program);
	// $('.test-program').attr('href', 'student-course-details.php?program_id='+test_program);
	uploadStudentAssignment();
	displayTestProgram();
	displayTestPapers();
	fetchTestProgramDetails();
	studyListing();
});


function fetchTestProgramDetails() {
	var req = {};
	req.action = "fetchTestProgramDetails";
	req.test_program = test_program;

	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			$('.test-program').html(res.details.name);
			displayTestProgram();
		}
		else {
			toastr.error(res.message,'Error');
		}

	});
}
var cutoff = 0;
function displayTestPapers() {

	req = {};
	req.action = 'testPaperDetails';
	req.program_id = getUrlParameter('program_id');
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)

	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 1) {
			displayAssignments(res.assignments_details.up_assignments);
			assignments = res.assignments_details.up_assignments;
			let attempts = 0;
			res.data.map((i) => attempts += parseInt(i.attempted));
			// if(res.assignment == 0 && attempts > 0) {
			// 	$('#no-cert').html('<strong>Your certification is still not complete. Please upload the project in order to claim your certificate.</strong>')
			// }
			cutoff = res.cutoff;
			// if (res.institute == 1 && res.institute_lab_type == 1) {
			// 	$('#countprogram').removeClass('hide');
			// 			var labCount = (res.remainingAttempts < 0) ? 0 : res.remainingAttempts;
			// 			var totalCount = (res.allowed_total_paper < 0) ? 0 : res.allowed_total_paper;
			// 	$('#labcount').html(labCount);
			// 	$('#totalCount').html(totalCount);
			// 	$('#expiry').html(moment(res.expiry, "YYYY-MM-DD HH:mm:ss").subtract(1,'days').format("YYYY-MM-DD"));

			// 	var marq = [];
			// 	if(res.isExpired) {
			// 		marq.push('Your Package has been expired.');

			// 	} else {
			// 		$('marquee').addClass('hide');
			// 	}

			// 	if(totalCount == 0) {
			// 		marq.push('Allowed Total Attempts finished.');
			// 	}

			// 	if(labCount == 0) {
			// 		marq.push('Allowed Lab Attempts finished');
			// 	}

			// 	if(marq.length > 0) {
			// 		$('marquee').html(marq.join(' | '));
			// 		$('marquee').removeClass('hide');
			// 	}
			// }
			displayData(res.data, res.attemptStatus, res.institute, res.isExpired);
			showMessage(res);
		}
		if (res.status == 2)
		{
			window.location='404-error.php';
		}
	});
}

function displayData(papers, attemptStatus, institute, isExpired) {
	na = 'NA';
	html = '';
	var count = 0;
	$.each(papers, function(i,paper){


		/*************************************
		 * 0 -> Not in Resume State (Fresh Start)
		 * 	 -> {rem_attempt}
		 * 1 -> Resume State
		 *
		 *************************************/
		href = 'question-paper.php?program_id='+test_program+'&test_paper='+paper.id;
		start_time = (paper.start_time == '0' || paper.start_time == '1') ? na : paper.start_time;
		end_time = (paper.end_time == '0') ? na : paper.end_time;
		total_attempts = (paper.type == '6') ?  na : paper.total_attempts;
		remAttempts = parseInt(paper.total_attempts) - parseInt(paper.attempted);
		remAttempts = (remAttempts < 0) ?  '0' : remAttempts;
		remAttempts = (paper.type == '6') ?  na : remAttempts;
		action= '<a href='+href+'>Start</a>';


		// action = (remAttempts == 0 && paper.status == '0')? 'Start Test' : action;
		// action = (remAttempts > 0 && paper.status == '0')? action :action ;
		// action = ( paper.status == '1')? '<a href='+href+'>Resume Test</a>' :action ;

		var report = 'NA';
		if(paper.status == '0') {
			report = (paper.attempt_first_marks == undefined)?'NA':((paper.attempt_first_marks >= cutoff)? 'Pass' : 'Fail');
			if(remAttempts <= 0) {
				action = 'Attempted';
			}
			else {
				action= '<a href='+href+'>Start</a>';
			}
		}
		else {
			action= '<a href='+href+'>Resume</a>';
		}

		//action = (remAttempts == 0 && paper.type != '6')? 'Start Test':action;
		//action = (paper.type == '5' && paper.status == '0')? 'Start Test':action;
		result_href = (paper.type == '6')?'#':'my-test-history.php?attempt='+paper.attempt_first;

		result = '<a href='+result_href+'>Result</a>';
		result = (paper.type == '6') ? 'Result' : result;
		result = (paper.attempt_first == undefined)?'Result':result;

		switch(paper.type) {
			case '1':
				type = 'Diagnostic Exam';
				break;

			case '3':
				type = 'Chapter Exam';
				break;

			case '4':
				type = 'Official Exam';
				break;

			case '5':
				type = 'Scheduled Exam';
				total_attempts = na;
				remAttempts = na;
				if(paper.flag == false) {
					action = 'Start Exam';
				}
				break;

			case '6':
				type = 'Practice Exam';
				report = 'NA';
				break;

			default:
				type='';
				break;
		}
		/**
		 * not practice test and not allowed to attempt
		 * remove links
		 */
		action = (paper.type != '6' && !attemptStatus) ? "Start Exam" : action;
		// action = (institute && isExpired) ? "Start Test" : action;
		action = (isExpired) ? "Start Exam" : action;
		action = (paper.attempt_first_marks >= cutoff || moment(parseInt(paper.attempt_time * 1000) + (1000 * 60 * 60 * 24 * 5)).isAfter() ) ? "NA" : action;

		html += '<tr>';
		html += '<td>'+paper.name+'</td>';
		html += '<td>'+type+'</td>';
		html += '<td>'+paper.questions+'</td>';
		html += '<td>'+start_time+'</td>';
		html += '<td>'+end_time+'</td>';
		html += '<td>'+((paper.type == 6)? 'NA' : cutoff)+'</td>';
		// html += '<td>'+total_attempts+'</td>';
		// html += '<td>'+remAttempts+'</td>';
		html += '<td>'+action+'</td>';
		html += '<td>'+((paper.attempt_first_marks)?paper.attempt_first_marks:'NA')+'</td>';
		html += '<td>'+report+'</td>';
		// html += '<td>'+result+'</td>';
		//html += '<td><a href="leaderboard.php?paper='+paper.id+'" title="View Leaderboard">View</a></td>';
		html += '</tr>';
		count++;
		let percent = (paper.attempt_first_marks / paper.total_marks) * 100;
	})
	$('#sample_editable_1 tbody').html(html);
	if(papers.length == 0)
	{
		$('#sample_editable_1 tfoot').html('<th colspan="8" class="text-center">	Welcome. Please register and pay for the exam	</th>');
	}
	else
	{
		$('#sample_editable_1 tfoot').html('');
	}
}

/*
function setEventStartTest() {
	$('.startTest').off('click');
	$('.startTest').on('click',function(e){
		e.preventDefault();

		var type = $(this).attr('type');
		var paper = $(this).attr('paper');

		if(type == '5'){

			var program_id = getUrlVars()["program_id"];
			var category_id = getUrlVars()["category_id"];
			var html = "";


			req = {};
			req.action = 'displayTestPapers';
			req.parent_id = getUrlParameter('parent_id');
			req.parent_type = getUrlParameter('parent_type');
			req.program_id = getUrlParameter('program_id');
			req.type = getUrlParameter('type');
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)

			}).done(function(res) {
				res = $.parseJSON(res);

				if(res.status == 1)
				{
					$(res.data).each(function(i)
					{
						if(paper == res.data[i]['id'] && res.data[i]['status'] == 1){

							window.location = "question-paper.php?program_id="+test_program+"&test_paper="+res.data[i]['id'];

						}else if(paper == res.data[i]['id']){
							alert("Scheduled Test will start on "+res.data[i]['start_time']+" to "+res.data[i]['end_time']+". Try again later.");
						}
					});
				}
			});
		}
	});
}

*/
function studyListing() {
	var req = {};
		req.action = "listingStudyMaterial";
		// req.subject_id = $(this).parents('tr').data('sub') ;
		req.program_id = test_program;
		$.ajax({
			type: "post",
			url: EndPoint,
			data:	JSON.stringify(req)
		}).done(function(res)
		{
			res = $.parseJSON(res);
			if(res.status == 1)
			{
				// console.log(res);
				fillStudyMaterialListing(res.study_material);
			}
		});
}


function fillStudyMaterialListing(data){
	var html = "<tr><td colspan ='4'>No Capstone Project Template</td></tr>";
	if(data.length == 0)
	{
		$('#listing-table tbody').html(html);
		return;
	}
		$('#ref_study_material').show();
		html="";
	$.each(data, function(i, study_material) {
		html += "<tr data-id="+study_material.id+">";
		html += "<td>"+study_material.subject_name+"</td>";
		html += "<td>"+study_material.name+"</td>";
		html += '<td> <a href="'+study_material.source+'" download><i class="fa fa-download"></i></a></td>';
		html += "</tr>";
	});

	$('#listing-table tbody').html(html);

}

function displayTestProgram()
{

	req = {};
	req.action = 'testProgramDetails';
	req.program_id = getUrlParameter('program_id');
	$.ajax({
		'type'  :   'post',
		'url'   :   EndPoint,
		'data'  :   JSON.stringify(req)

	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			// $('#countprogram span:nth-child(1)').text(res.student_program['0']['total_paper']);
			// $('#countprogram span:nth-child(2)').text(res.student_program['0']['total_lab']);
		}
		if(res.status == 2)
		{
			window.location='404-error.php';
		}
	});
}

displayAssignments = function (upassignments) {
	if(upassignments.length > 0) {
		$('#assignments').removeClass('hide');
	} else {
		$('#assignments').addClass('hide');
	}
	var html = "";
	var sno = 1;
	$.each(upassignments, function(index,up_assignment) {
		html += '<tr data-id="'+up_assignment.id+'" data-db_status="'+up_assignment.status+'">';
		html += '<td><div style="padding:5px;">'+sno+'</div></td>';
		// html += '<td><div style="padding:5px;">'+up_assignment.first_name+' '+up_assignment.last_name+'</div></td>';
		// html += '<td><div style="padding:5px;">'+up_assignment.email+'</div></td>';
		// html += '<td><div style="padding:5px;">'+up_assignment.class_name+'</div></td>';
		html += '<td><div style="padding:5px;">'+up_assignment.doc_name+'</div></td>';

		html += '<td><div style="padding:5px;">';
		if (up_assignment.doc_name != 'Send By E-mail') {
			html += '<a href="../assets/'+up_assignment.path+''+up_assignment.doc_name+'" download title="Download Capstone Project"><i class="fa fa-download"></i></a>';
		}
		html += '</div> </td>';

		switch(up_assignment.status){
			case '1':
				status = 'Pass'
				break;
			case '2':
				status = 'Fail'
				break;
			default:
				status = 'Pending'
				break;
		}

		html += '<td><div style="padding:5px;">'+status+'</div></td>';

		html += (status != 'Pass') ? `<td><div style="padding:5px;"><button class="btn btn-primary btn-xs" onclick="change_status(this,${up_assignment.id})">Delete</button></div></td></tr>`: '<td></td></tr>';
		//html += '<td><div style="padding:5px;"> <a href="../assets/'+up_assignment.path+'/'+up_assignment.doc_name+'" download title="Download Assignment"><i class="fa fa-download"></i></a> | <a class="pass_fail" style="color: rgb(230, 12, 12);font-weight: bold; text-align:center;"><i class="fa fa-trash-o"></i></a></div> </td></tr>';
		sno++;
	});

	$('#student-assignment-table tbody').html(html);


}

function sendByEmail() {
	if(checkAssignment() && confirm('Are you sure you want to submit by email?')){
		var req = {};
		req.action = 'sendAssigmentByEmail';
		req.test_program_id = test_program;
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				setTimeout(function(){ location.reload(); }, 500);
				toastr.success(res.message)
			}
			else {
				toastr.error(res.message);
			}
		});
	}
}

function uploadStudentAssignment() {
	$('#assignmentUploadForm').off('submit');
	$('#assignmentUploadForm').on('submit', function(e) {
		e.preventDefault();
		var formData = new FormData($(this).get(0));
		//if(updateProfileImageValidation(formData)){
		formData.append('test_program_id', test_program);
		if(checkAssignment() && confirm('Make sure you are uploading the right file. You will not get second chance to upload')){
		$.ajax({
		'type'      :  'post',
		'url'    :  'vendor/fileUploads.php',
		'processData'  :   false,
		'contentType'  :   false,
		'data'      :   formData,
		}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
		toastr.success("Capstone Project uploaded Successfully");
		setTimeout(function(){ location.reload(); }, 500);
		}
		else if(res.status == 0) {
		toastr.error("ERROR: Capstone Project Already Exist.");
		}
		});
		//}
	}
});
}


function change_status(ob, assignment_id){
	var req = {};
	req.id = $(ob).parents('tr:eq(0)').data('id');
	req.action = 'deleteAssignment';

	if(confirm('Are you sure, You want to delete Capstone Project')){
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				toastr.success(res.message)
				displayTestPapers()
			}
			else {
				toastr.error(res.message);
			}
		});
	}else{
		toastr.error('You can not change status');
	}
}


checkAssignment = function() {
	count = 0
	assignments.map((a) => {count = (a.status == '0')?count+1:count;})
	if(count > 0){
		toastr.error('Please delete Capstone Project then upload.')
		return false;
	}
	return true;
}

function showMessage(data) {
	var now = moment();
	var expiry_date = moment(data.expiry);
	var days = parseInt(expiry_date.diff(now) / (1000 * 60 * 60 * 24)) + 1;
	if (!data.attempts || (data.attempts.length == 0 && data.assignments_details.up_assignments.length == 0)) {
		var message = "You have "+days+" days to complete the exam and submit your capstone.";
	}else if (data.attempts && data.attempts.length > 0 && (data.attempts[data.attempts.length - 1].marks_archived >= data.cutoff) && data.assignments_details.up_assignments.length == 0) {
		var message = "You have successfully completed the test. You have "+days+" days to submit your capstone.";
	}else if (data.attempts && data.attempts.length == 0 && data.assignments_details.up_assignments.length > 0) {
		var message = "You have submitted your capstone project. You have "+days+" days to complete your exam.";
	}else if (data.attempts && data.attempts.length > 0 && data.assignments_details.up_assignments.length > 0 && (data.attempts[data.attempts.length - 1].marks_archived >= data.cutoff) && data.assignments_details.up_assignments[0].status == 0) {
		var message = "Congratulations on passing the exam. Please wait as we grade your capstone project";
	}else if (data.attempts && data.attempts.length > 0 && data.assignments_details.up_assignments.length > 0 && (data.attempts[data.attempts.length - 1].marks_archived >= data.cutoff) && data.assignments_details.up_assignments[0].status == 1) {
		var message = "Congratulations on passing both the exam and the capstone project!";
	// }else if (data.attempts.length > 0 && data.assignments_details.up_assignments.length > 0 &&(data.attempts[data.attempts.length - 1].marks_archived < data.cutoff || data.assignments_details.up_assignments[0].status == 1)) {
	} else if (data.attempts && data.attempts.length) {
		var last_attempted_before = moment(((data.attempts[data.attempts.length - 1].start_time * 1000) + (1000 * 60 * 60 * 24 * 6))).format('DD-MMM-YYYY');
		var message = "You have one more attempt to complete the exam or submit the capstone available from "+last_attempted_before+" to "+expiry_date.format('DD-MMM-YYYY')+".";
	}
	$('#fail-cutoff').text(message).removeClass('hide');
}