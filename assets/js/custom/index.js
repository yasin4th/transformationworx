var Programs, Papers;

$(function() {

	getTestprogramsAvailableForSave();
	getTestprogramsAvailableForSavePopular();
});

function getTestprogramsAvailableForSave() {
	var req = {};
	//req.action = "getTestprogramsAvailableForSave";
	req.action = "filterPrograms";
	req.option = "2";
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		Programs = res.data;
		Papers = res.papers;
		if (res.status == 1) {
			fillTestprograms(res.data);
		} else if(res.status == 0)
			toastr.error(res.message);
	});
}
function getTestprogramsAvailableForSavePopular() {
	var req = {};
	req.action = "filterPrograms";
	req.option = "1";
	req.checkExpiry = "1";

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		Programs = res.data;
		Papers = res.papers;
		if(res.status == 1){
			fillTestprogramsPopular(res.data);
		}else if(res.status == 0)
			toastr.error(res.message);
	});
}

function fillTestprograms(programs) {
	//console.log(programs);
	html = '';
	$.each(programs, function(p,program) {
		html += '<div class="recent-work-item">';
		html += ' <a class="test-program" data-class="'+program.class+'" data-id="'+program.id+'" data-price="'+program.price+'" href="#program-details-modal">';
		html += ' <em> <img src="'+program.image+'" alt="Amazing Project" class="img-responsive"> <div class="overlay"></div> </em> ';
		html += ' <span class="recent-work-description">';
		html += ' <strong>'+program.name+'</strong>';
		html += ' <b>Course Description</b>';
		html += ' </span>';
		html += ' </a>';
		html += ' </div>';
	});
	//$('#latest-programs').html(html);
    //Layout.initOWL();
    setEventOnTestProgramClick();
  	setEventBuyPrograme();
}

function fillTestprogramsPopular(programs) {
	///console.log(programs);
	html = '';
	$.each(programs, function(p,program) {
		html += '<div class="recent-work-item">';
		html += ' <a class="test-program" data-class="'+program.class+'" data-id="'+program.id+'" data-price="'+program.price+'" href="#program-details-modal">';
		html += ' <em> <img src="'+program.image+'" alt="Amazing Project" class="img-responsive"> <div class="overlay"></div> </em> ';
		html += ' <span class="recent-work-description">';
		html += ' <strong>'+program.name+'</strong>';
		// html += ' <b>Course Description</b>';
		html += ' </span>';
		html += ' </a>';
		html += ' </div>';
	});
	$('#popular-programs').html(html);
    Layout.initOWL();
    setEventOnTestProgramClick();
  	setEventBuyPrograme();
}

function setEventOnTestProgramClick() {
	$('.test-program').off('click');
	$('.test-program').on('click',function(e){
		e.preventDefault();
		var req = {};
		req.id = $(this).data('id');

		price = 0;
		image = '';
		description = '';

		$.each(Programs, function(p,program) {
			if(req.id == program.id) {
				name = program.name;
				price = 'CAD '+program.price;
				description = program.description;
				image = program.image;
			}
		});

		var count = 0;
		html = '<table class="table table-striped table-hover table-bordered"><thead><tr><th>Test Topic</th><th>No. of Tests</th></tr></thead><tbody>';
		$.each(Papers, function(p,paper) {
			if(req.id == paper.test_program_id && paper.test_paper_type_id == 1) {

				html += '<tr>';
				html += '<td>'+paper.test_paper_type+'</td>';
				html += '<td>'+paper.count+'</td>';
				html += '</tr>';

				count++;
			}
		});
		$.each(Papers, function(p,paper) {
			if(req.id == paper.test_program_id && paper.test_paper_type_id == 6) {

				html += '<tr>';
				html += '<td>'+paper.test_paper_type+'</td>';
				html += '<td>'+paper.count+'</td>';
				html += '</tr>';

				count++;
			}
		});
		$.each(Papers, function(p,paper) {
			if(req.id == paper.test_program_id && paper.test_paper_type_id == 3) {

				html += '<tr>';
				html += '<td>'+paper.test_paper_type+'</td>';
				html += '<td>'+paper.count+'</td>';
				html += '</tr>';

				count++;
			}
		});
		$.each(Papers, function(p,paper) {
			if(req.id == paper.test_program_id && paper.test_paper_type_id == 4) {

				html += '<tr>';
				html += '<td>'+paper.test_paper_type+'</td>';
				html += '<td>'+paper.count+'</td>';
				html += '</tr>';

				count++;
			}
		});
		$.each(Papers, function(p,paper) {
			if(req.id == paper.test_program_id && paper.test_paper_type_id == 5) {

				html += '<tr>';
				html += '<td>'+paper.test_paper_type+'</td>';
				html += '<td>'+paper.count+'</td>';
				html += '</tr>';

				count++;
			}
		});
		html += '</tbody></table>';

		$('#program-details-modal .program-name').html(name);
		$('#program-details-modal .image').attr('src',image);
		$('#program-details-modal .price').html(price);
		$('#program-details-modal .description').html(description);

		if (count>0) {
			$('#program-details-modal #modal-table').html(html);
		} else {
			$('#program-details-modal #modal-table').html('No Tests Available in this Program...');
		}
		$('#program-details-modal').attr('data-program-id',req.id);
		$('#program-details-modal').modal("show");
	})
}

function setEventBuyPrograme() {

	$('.buy-program').off('click');
	$('.buy-program').on('click', function(e) {
		e.preventDefault();
		buyTestProgram();
	})
}

function buyTestProgram() {
	var req = {};
	req.action = "buyTestProgram";
	req.program_id = $('#program-details-modal').attr('data-program-id');


	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		if(res.status == 1) {
			toastr.success(res.message);
			$('#program-details-modal').modal("hide");
		}
		else if(res.status == 2) {
			toastr.warning(res.message);
			window.location = "login.php";
		}
		else if(res.status == 0)
			toastr.error(res.message);
			//console.log(res.data);
	});
}