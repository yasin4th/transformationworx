$(function() {

displayPrograms();
getTestprograms();

});

function getTestprograms() {
	var req = {};
	req.action = "getTestprograms";
	$.ajax({
		'type'	:	'post',
		'url'	:	'../test_engine/vendor/api.php',
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		if(res.status == 1)
			fillTestprograms(res.data);
		else if(res.status == 0)
			alert(res.message);
	});
}

function fillTestprograms(programs) {
	console.log(programs);
	html = '';
	$.each(programs, function(p,program) {
		html += '<div class="recent-work-item"> <em> <img src="assets/frontend/pages/img/works/course1.jpg" alt="Amazing Project" class="img-responsive"> <a href="#"><i class="fa fa-link"></i></a> <a href="assets/frontend/pages/img/works/course1.jpg" class="fancybox-button" title="'+program.name+'" data-rel="fancybox-button"><i class="fa fa-search"></i></a> </em> <a class="test-program recent-work-description" data-class="'+program.class+'" data-id="'+program.id+'" data-price="'+program.price+'"> <strong class="name">'+program.name+'</strong> <b>Course Description</b> </a></div>';
	});
	$('#all-programs').html(html);
    Layout.initOWL();
    setEventOnTestProgramClick();
  	setEventBuyPrograme();
}

function setEventOnTestProgramClick() {
	$('.test-program').off('click');
	$('.test-program').on('click',function(e){
		e.preventDefault();
		name = $(this).find('.name').text();
		cls = $(this).data('class');
		id = $(this).data('id');
		description = $(this).data('description');
		price = $(this).data('price');
		
		$('#test-program-details-modal .modal-title').text(name);
		$('#test-program-details-modal .cls').text(cls);
		$('#test-program-details-modal .price').text(price);
		$('#test-program-details-modal .description').text(description);
		$('#test-program-details-modal').attr('data-program-id',id);
		$('#test-program-details-modal').modal("show");
	})
}

function setEventBuyPrograme() {
	$('.buy-program').off('click');
	$('.buy-program').on('click', function(e) {
		e.preventDefault();
		buyTestProgram();
	})
}

function buyTestProgram() {
	var req = {};
	req.action = "buyTestProgram";
	req.user_id = 2;
	req.program_id = $('#test-program-details-modal').attr('data-program-id');
	$.ajax({
		'type'	:	'post',
		'url'	:	'../test_engine/vendor/api.php',
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		if(res.status == 1) {
			$('#test-program-details-modal').modal("hide");
		}
		else if(res.status == 0)
			alert(res.message);
			console.log(res.data);
	});
}


function displayPrograms()
{
	var req = {};
	req.action = "displayPrograms";

	$.ajax({
		'type'	:	'post',
		'url'	:	'../test_engine/vendor/api.php',
		'data'	:	JSON.stringify(req)

	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			fillPrograms(res.data);
		}

	});
}

function fillPrograms(programs)
{
	var html = "";

	$(programs).each(function(i,program){

	html += '<div class="col-md-4 col-xs-4"><div class="course"><em>'+
	'<img src="assets/frontend/pages/img/works/course1.jpg" alt="Amazing Project" class="img-responsive">'+
	'</em><a class="recent-work-description" href="#" onclick="urlDisplayProgramDetails(this)" id="'+program.id+'"><strong>'+program.name+'</strong>'+
	'<b>'+program.description+'</b></a></div></div>';

	});

	$("#courseDescriptions").append(html);


}

function urlDisplayProgramDetails(program)
{
	location.replace("student-course-details.php?program_id="+program.id);
}

function urlDisplayTestPapers(data)
{
	// var program_id = data.id.split(" ")[1].substring(8);
	// var category_id = data.id.split(" ")[0].substring(9);
	// location.replace("view-course-details.php?program_id="+program_id+"&category_id="+category_id);
	var program_id = getUrlParameter('program_id');
	var category_id = $(data).data('type');
	window.location = "view-course-details.php?program_id="+program_id+"&category_id="+category_id;
}

function displayProgramDetails()
{
	var program_id = getUrlVars()["program_id"];
	var i =0;

	var req = {};
	req.action = "displayProgramDetails";
	req.program_id = program_id;

	$.ajax({
		'type'	:	'post',
		'url'	:	'../test_engine/vendor/api.php',
		'data'	:	JSON.stringify(req)

	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			$(res.data).each(function(i)
			{
				$(".row").find("#"+ res.data[i]['test_paper_type_id'] +" .number").html(res.data[i]['details_count']);
				$(".row").find("#"+ res.data[i]['test_paper_type_id'] +" a").attr("id", $(".row").find("#"+ res.data[i]['test_paper_type_id'] +" a").attr("id") +  " program-"+getUrlVars()["program_id"]);
			});
		} 

	});
}

function displayTestPapers()
{

	var program_id = getUrlVars()["program_id"];
	var category_id = getUrlVars()["category_id"];
	var html = "";
	var i =0;

	var req = {};
	req.action = "displayTestPapers";
	req.program_id = program_id;
	req.category_id = category_id;

	$.ajax({
		'type'	:	'post',
		'url'	:	'../test_engine/vendor/api.php',
		'data'	:	JSON.stringify(req)

	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			$(res.data).each(function(i)
			{
				html += '<tr><td>'+res.data[i]['name']+'</td><td>'+res.data[i]['start_time']+'</td><td>'+res.data[i]['end_time']+'</td>'+
				'<td>'+res.data[i]['total_attempts']+'</td><td></td><td><a href="#">Start Exam</a></td><td><a href="#">See Results</a></td></tr>';
			});
		} 

		$("#sample_editable_1").append(html);

	});
}

function getUrlVars() 
{
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	vars[key] = value;
	});
	return vars;
}
