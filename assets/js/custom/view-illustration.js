test_program = getUrlParameter('test_program');
subject = getUrlParameter('subject');
chapter = getUrlParameter('chapter');
$(function(){
	// this is for BCT(BreadCrumb Trail)
	$('.back-to-subjects').attr('href', 'student-course-details.php?program_id='+test_program)
	$('.back-to-chapters').attr('href', 'student-course-chapters.php?program_id='+test_program+'&subject='+subject)
	$('.back-selected-chapter').attr('href', 'student-course-chapter-tests-types.php?program_id='+test_program+'&subject='+subject+'&chapter='+chapter)

	fetchAttachedIllustrations();

});

function fetchAttachedIllustrations() {
	var req = {};
	req.action = 'fetchAttachedIllustrations';
	req.chapter = chapter;
	req.test_program = test_program;

	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		console.log(res);
		if(res.status == 1)
			fillIllustrationsTbody(res.data);
	});
}

function fillIllustrationsTbody(illustrations) {
	html = '';
	$.each(illustrations,function(x,illustration) {
		filename = illustration.source.split('/').pop();
		link = 'download.php?filename='+filename;
		html += '<tr data-id="'+illustration.id+'"> <td> '+illustration.name+' </td> <td> <a href="'+link+'" class="view-details">View File</a> </td> </tr>';
	});
	$('#illustration-table tbody').html(html);	
}