var redirect = getUrlParameter('redirect');
$(function() {

	$('#login-form').on('submit', function(e) {
		e.preventDefault();
		signIn();
	});
})
function signIn()
{
	if($('#email').val() != "" && $('#password').val() != "")
	{
		$('#email').parents('div:eq(0)').removeClass('has-error').find('.help-block').remove();
		$('#password').parents('div:eq(0)').removeClass('has-error').find('.help-block').remove();

		var req = {};
		req.email = $('#email').val();
		req.password = $('#password').val();
		req.action = 'login';

		$.ajax({
			'type'	:	'post',
			'url'	:	'vendor/api.php',
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1)
			{
				if(redirect != undefined)
				{
					document.location = redirect;
				}
				else
				{
					location.replace("dashboard.php");
				}
			}
			else if(res.status == 0)
			{
				$('.alert').html('ERROR: Invalid Credentials');
				$('.alert').addClass('alert-danger');
				$('.alert').show();
				toastr.error(res.message);
			}
		});
	}
	else
	{
		$('#email').parents('div:eq(0)').removeClass('has-error').find('.help-block').remove();
		$('#password').parents('div:eq(0)').removeClass('has-error').find('.help-block').remove();
		$('.alert').html('Please fill user-name and passwords.');
		$('.alert').addClass('alert-danger');
		if ($('#email').val() == '') {
			$('#email').parents('div:eq(0)').addClass('has-error').append('<span class="help-block"> Please fill E-mail.</span>');
		}
		if($('#password').val() == "")	{
			$('#password').parents('div:eq(0)').addClass('has-error').append('<span class="help-block"> Please fill Password.</span>');
		}
	}
}