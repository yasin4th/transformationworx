var Programs, Papers, x=2, limit=20;
// var limit = 0;
var page_no = 1;
$(function() {
	
	fetchClasses();
	filterPrograms();
	categoryData();
	getTestprogramsAvailableForSale();
	//setEventOnTestProgramClick();
	setEventBuyPrograme();
	setEvents();
	if(getUrlParameter('q')){
		search();
	}
})
$('#classes').on('change', function(e) {
		
	//fetchSubjects();
});

function getTestprogramsAvailableForSale() {
	var req = {};
	req.action = "getTestprogramsAvailableForSave";
	

	$.ajax({
		type: 'post',
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		Programs = res.data; //latest
		Papers = res.papers;
		if(res.status == 1)
			fillTestprograms(res.data);
		else if(res.status == 0){
			toastr.error(res.message);
		}
	});
}


function fillTestprograms(programs) 
{
	html = '';
	$.each(programs, function(p,program) {
		// //html += '<div class="col-md-3 col-md-3 col-xs-6 form-group"> <div class="course"> <a data-id="'+program.id+'" title="Click to Buy." href="#"> <em> <img src="'+program.image+'" alt="Amazing Project" class="img-responsive"></em> <span class="recent-work-description"> <strong> '+program.name+' </strong> <b> Price: '+program.price+' <i>Class: '+program.class+' </i></b> </span> </a> </div></div>';
		// html += '<div class="col-md-3 product-border padd15 form-group program" style="height:500px;">';
		// html += '<div class="row">';
		// html += '<div class="col-md-12 mrg-bot10">';
		// html += '<a class="recent-work-description test-program" data-id="'+program.id+'">';
		// html += '<img src="'+program.image+'" alt="Amazing Project" class="img-responsive">';
		// html += '</a>';
		// html += '</div>';
		// html += '<div class="col-md-12">';
		// html += '<a class="recent-work-description test-program" data-id="'+program.id+'">';
		// html += '<h3>' + program.name + '</h3>';
		// html += '<p>';
		// html += program.description.substr(0,100);
		// if(program.description.length > 100)
		// {
		// 	html += '...';
		// }		
		// html += '</p>';
		// html += '</a>';
		// html += '<div class="row">';
		// html += '<div class="col-sm-12">';
		// // html += '<button class="btn btn-primary"><i class="fa fa-inr"></i> '+program.price+'</button>';
		// // html += '<span class="badge badge-roundless badge-danger price"><i class="fa fa-inr"></i> '+program.price+'</span>';
		// html += '<span class="price"><i class="fa fa-inr"></i> '+program.price+'</span>';

		// // html += '<a class="btn grey-cascade pull-right test-program" data-id="'+program.id+'">View Details</a>';
		// html += '</div>';
		// html += '</div>';
		// html += '</div>';
		// html += '</div>';
		// html += '</div>';

		
		html += '<div class="col-md-4">';
		html += '<a class="test-program" data-id="'+program.id+'" data-price="'+program.price+'" href="exams/'+program.name.dash()+'_'+program.id+'">';
		html += '<div class="course-item">';
		html += '<div class="course-thumbnail">';
        html += '<!--<a class="test-program" data-id="'+program.id+'" data-price="'+program.price+'" href="#program-details-modal">--><img src="'+program.image+'" alt="Introduction LearnPress &#8211; LMS plugin" title="course-4" ><!--</a>--><!--<a class="course-readmore test-program" href="#">Buy Now</a>--></div>';
		html += '<div class="thim-course-content">';
		/*html += '<div class="course-author" itemscope itemtype="http://schema.org/Person">';
        html += '<img alt="" src="http://educationwp.thimpress.com/wp-content/uploads/gravatar/37e6600c81e3765b84dd2eaa751da782-s40.jpg" class="avatar avatar-40" width="40" height="40" />';
		html += '<div class="author-contain">';
		html += '<label itemprop="jobTitle">Teacher</label>';
		html += '<div class="value" itemprop="name">';
		html += '<a href="http://educationwp.thimpress.com/profile/keny">';
		html += 'Keny White ';
		html += '</a>';
		html += '</div>';
		html += '</div>';
		html += '</div>';*/
		html += '<h2 class="course-title">';
		html += '<span class="test-program" data-id="'+program.id+'" data-price="'+program.price+'" href="#program-details-modal"> ' + program.name + '</span>';
		html += '</h2>';
		html += '<div class="course-meta">';
		// html += '<div class="course-comments-count">';
		// html += '<div class="value"><i class="fa fa-comment"></i>4</div>';
		// html += '</div>';
		html += '<div class="course-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';
		html += '<div class="value free-course" itemprop="price" content="Free">';
		html += 'CAD ';
		html += program.price+'</div>';
		html += '<meta itemprop="priceCurrency" content="&#36;" />';
		html += '</div>';
		// html += '<div class="course-students pull-right">';
		// html += '<label>Students</label>';
		// html += '<div class="value"><i class="fa fa-group"></i> 313K Enrolled </div>';
		// html += '</div>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		html += '</a>';
		html += '</div>';

	});
	if(programs.length == 0) 
	{
		$('#programs-container').html("<div class='col-md-4'><h4>No Programs Found.</h4></div>");
	}
	else
	{
		$('#programs-container').html(html);
	}
	$(".program").hide();
	size = $(".program").size();
	$('.program:lt('+limit+')').show();
	$('#loadMore').click(function () {
        x= (x+limit <= size) ? x+limit : size;
        $('.program:lt('+x+')').show('slow');
    });	
}


function setEventOnTestProgramClick() {
	
	$('.test-program').on('click',function(e){
		e.preventDefault();
		var req = {};
		req.id = $(this).data('id');
		$('#program-details-modal').attr('data-program-id',$(this).data('id'));
		
		$.each(Programs, function(p,program) {
			if(req.id == program.id) {
				name=program.name;
				price = 'CAD '+program.price;
				description = program.description;
				image = program.image;
				$('#program-details-modal').attr('data-price',program.price);
			}
		});
		
		var count = 0;
		html = '<table class="table table-striped table-hover table-bordered"><thead><tr><th>Test Topic</th><th>No. of Tests</th></tr></thead><tbody>';
		$.each(Papers, function(p,paper) {
			if(req.id == paper.test_program_id && paper.test_paper_type_id == 1) {
				
				html += '<tr>';
				html += '<td>'+paper.test_paper_type+'</td>';
				html += '<td>'+paper.count+'</td>';
				html += '</tr>';
				
				count++;
			}
		});
		$.each(Papers, function(p,paper) {
			if(req.id == paper.test_program_id && paper.test_paper_type_id == 6) {
				
				html += '<tr>';
				html += '<td>'+paper.test_paper_type+'</td>';
				html += '<td>'+paper.count+'</td>';
				html += '</tr>';
				
				count++;
			}
		});
		$.each(Papers, function(p,paper) {
			if(req.id == paper.test_program_id && paper.test_paper_type_id == 3) {
				
				html += '<tr>';
				html += '<td>'+paper.test_paper_type+'</td>';
				html += '<td>'+paper.count+'</td>';
				html += '</tr>';
				
				count++;
			}
		});
		$.each(Papers, function(p,paper) {
			if(req.id == paper.test_program_id && paper.test_paper_type_id == 4) {
				
				html += '<tr>';
				html += '<td>'+paper.test_paper_type+'</td>';
				html += '<td>'+paper.count+'</td>';
				html += '</tr>';
				
				count++;
			}
		});
		$.each(Papers, function(p,paper) {
			if(req.id == paper.test_program_id && paper.test_paper_type_id == 5) {
				
				html += '<tr>';
				html += '<td>'+paper.test_paper_type+'</td>';
				html += '<td>'+paper.count+'</td>';
				html += '</tr>';
				
				count++;
			}
		});
		html += '</tbody></table>';
		

		$('#program-details-modal .program-name').html(name);
		$('#program-details-modal .image').attr('src',image);
		
		$('#program-details-modal .price').html(price);
		$('#program-details-modal .description').html(description);

		if(count>0){
			$('#program-details-modal #modal-table').html(html);
		}else{
			$('#program-details-modal #modal-table').html('No Tests Available in this Program...');
		}
		
		//$('#program-details-modal').attr('data-price',price);
		$('#program-details-modal').modal("show");
	})
}


function setEventBuyPrograme() {
	$('.buy-program').off('click');
	$('.buy-program').on('click', function(e) {
		e.preventDefault();
		buyTestProgram();
	})
}

function buyTestProgram() {
	var req = {};
	req.action = "buyTestProgram";
	req.program_id = $('#program-details-modal').attr('data-program-id');
	req.price = $('#program-details-modal').data('price');	

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		if(res.status == 1) 
		{
			toastr.success(res.message);
			$('#program-details-modal').modal("hide");
		}
		else if(res.status == 2) {
			toastr.warning(res.message);
			setTimeout(window.location = "login.php",3000);
			// window.location = "login.php";
		}
		else if(res.status == 0)
			toastr.error(res.message);			
	});
}




/**
*
* @author Ravi Arya
* @date 1-2-2016
* @param params
* @return fetched lasses and subjects
* @todo todo
*
**/

function fetchClasses() {
	req = {};
	req.action = 'fetchClasses';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			fillClasses(res.classes);
		}
		else{
			// toastr.error('Error');
		}
	});
	
}

// function to fill classes
function fillClasses(classes) {
	var html = "<option data-id=''> Select Class </option>";
	$.each(classes,function( i,cls) {
		html += "<option value='"+cls.id+"' value='"+cls.id+"' data-id="+cls.id+">" +cls.name+ "</option>";
	});
	$('#classes').html(html);
	$('#classes').off('change');
	$('#classes').on('change', function(e){
		e.preventDefault();
		fetchSubjects();
	});
	//$('#test-paper-class').html(html);
}

// function to fetch subjects
function fetchSubjects() {
	req = {};
	req.action = 'fetchSubjects';
	req.class_id = $("#classes").find('option:selected').data('id');
	if(req.class_id) {
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fillSubjects(res.subjects);
			}
			else{
				$('#subjects').html('');
				// toastr.error('No Subject Found In Selected Class.');
			}
		});
	}
}

// function to fill subjects
function fillSubjects(subjects) {
	var html = "<option data-id=''> Select Subject </option>";
	$.each(subjects,function( i,sub) {
		html += '<option val="'+(i+1)+'" value="'+sub.id+'" data-id="'+sub.id+'">'+sub.name+'</option>';
	});
	$('#subjects').html(html);
	//$('#test-paper-subjects').html(html);
}
search = function(){
	var req = {};
		req.action = 'filterPrograms';
		req.limit = limit;
		category = getCategoryIds();
	
		if(category.length != 0)
		{
			req.category = getCategoryIds();
		}

		if($('#keyword').val().trim() != '')
		{		
			req.keyword = $('#keyword').val().trim();
		}
		if(getUrlParameter('q')){
			if(getUrlParameter('q').trim() != '')
		{
			req.keyword = getUrlParameter('q').trim();
		}
		}

		if($('#keyword').val().trim() != '' &&  getUrlParameter('q'))
		{		
			req.keyword = $('#keyword').val().trim();
		}
		
		
				
		if($("#classes").find('option:selected').data('id') != '')
		{
			req.class_id = $("#classes").find('option:selected').data('id');
		}
		if($("#subjects").find('option:selected').data('id') != '')
		{
			req.subject_id = $("#subjects").find('option:selected').data('id');
		}
		
		if($('#popular').prop('checked')){
			req.option = $('#popular').val();
		}else if($('#latest').prop('checked')){
			req.option =$('#latest').val();
		}else{
			req.option = "0";
		}
				
		$.ajax({
			"type": "post",
			"url": EndPoint,
			"data": JSON.stringify(req) 
		}).done(function(res) {
			//console.log(res);
			res = $.parseJSON(res);

			if(res.status ==  1) {
				fillTestprograms(res.data);
			}
			
		});
}
filterPrograms =function(){
	$('#filter').off('submit');
	$('#filter').on('submit', function(e) {
		e.preventDefault();
		search();
		
		
	});
	
}



/*
* Pagination functions
*
*/
var sn = 1;
function createPagination(total_questions) {
	if (total_questions % 50 == 0) {
		pages = total_questions / 50;
	} else{
		pages = parseInt(total_questions / 50) + 1;
	};

	html = '';
	html += '<li><a class="page-number" value="1">&laquo; &laquo; </a></li>';
	html += '<li><a class="previous-page">&laquo; Prev </a></li>';
	// html += '<span class="page-numbers">';

	for (var i = 0; i < pages; i++) {
		html += '<li class=""><a class="page-numbers page-number" value="'+(i+1)+'">'+(i+1)+'</a></li>';
	};
	// html += '</span>';
	html += '<li><a class="next-page">Next &raquo;</a></li>';
	html += '<li><a class="page-number" value="'+pages+'">&raquo;&raquo;</a></li>';

	$('.question-table-pagination').html(html);

	$('.page-number').off('click');
	$(".page-number").on('click', function(e) {
		e.preventDefault();

		page_no = parseInt($(this).attr('value'));
		limit = parseInt(page_no - 1) * 50;
		
		sn = (limit == 0)?1:limit+1; // set serial number
		
		/* calling function to fetch data*/

			filterPrograms();
		
	});
	
	// previous button functions
	$('.previous-page').off('click');
	$('.previous-page').on('click', function(e) {
		e.preventDefault();
		if(!($(this).parent().hasClass('disabled'))) {

			page_no = parseInt($('.question-table-pagination:eq(0)').find('.active a').attr('value'));
			
			$('.question-table-pagination:eq(0) li a[value="'+(page_no - 1)+'"]').click();
		}
	});
	
	// next button functions
	$('.next-page').off('click');
	$('.next-page').on('click', function(e) {
		e.preventDefault();
		if(!($(this).parent().hasClass('disabled'))) {

			page_no = parseInt($('.question-table-pagination:eq(0)').find('.active a').attr('value'));
			$('.question-table-pagination:eq(0) li a[value="'+(page_no + 1)+'"]').click();
		}
	});
	

	// disable previous button function
	if(page_no == 1) {
		$('.question-table-pagination li .previous-page').parent().addClass('disabled');
	}
	// disable next button function
	if(page_no == parseInt($('.question-table-pagination:eq(0) li').length - 4)) {
		$('.question-table-pagination li .next-page').parent().addClass('disabled');
	}

	// adding active class to show which page is active
	$('.question-table-pagination').find('.active').removeClass('active');
	$('.question-table-pagination li a[value="'+page_no+'"]').parent().addClass('active');
	$('.question-table-pagination li .page-numbers').parent().addClass('hidden');
	$('.question-table-pagination li .page-numbers[value="'+(page_no- 1)+'"], .page-numbers[value="'+page_no+'"], .page-numbers[value="'+(page_no+ 1)+'"]').parent().removeClass('hidden');

}


setEvents = function() {
	$('#btn-promo').on('click', function() {
		promo = $('#promo').val();
		if(promo.length > 0)
		{
			var id = $('#program-details-modal').attr('data-program-id');
			//console.log(id);
			
			price = 0;
			$.each(Programs, function(p,program) {
				if(id == program.id) {
					price = program.price;
				}
			});
			
			var req = {};

			req.action = "applyDiscount";
			req.promo = promo;
			req.price = price;
			$.ajax({
				type: 'post',
				url: EndPoint,
				data: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);	
				if(res.status == 1)
				{
					$('#program-details-modal').attr('data-price',res.amount);
					price = 'CAD <s>'+price+'</s> CAD '+res.amount;
					$('#program-details-modal .price').html(price);
				}
				else
				{
					toastr.error(res.message);
				}
			});
		}
	})

	$('#select-all').off('change');
	$('#select-all').on('change', function(e) {
		if($('#select-all').prop('checked')) {
			$('.category').prop('checked',true);
			$('#filter').submit();
		}
		else {
			$('.category').prop('checked',false);
			$('#filter').submit();
		}
	})

	$('.category').on('change', function(e) {
		$('#filter').submit();
	})




}

function categoryData() {
	var req = {};
	// req.limit = 5;
	// req.offset = 0;
	req.action = "getCategoryData";
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		fillData(res.data);		
	});
}

function fillData(data)
{
	html =  '<li>';
	html += '<div class="checkbox-list">';
	html += '<label><input type="checkbox" value="0" id="select-all"> All Categories </label>';
	html += '</div>';
	html += '</li>';
	$.each(data, function(i, difficulty) {
		
		html += '<li>';
		html += '<div class="checkbox-list">';
		html += '<label><input type="checkbox" value="'+difficulty.id+'" class="category"> '+difficulty.name+' </label>';
		html += '</div>';
		html += '</li>';
	});
	$('#category-filter').html(html);
	setEvents();
}

function getCategoryIds()
{
	ids=[];
	$.each($('.category:checked'),function(i,checkbox){
		ids.push($(this).val());
	});
	return ids;
}