/**
	* @author Pushpam Matah
	* @date 20-06-2015
**/

/* This function fetches the classes that are available for adding new subjects */
var index, counter;
function fetchPlanClasses() {
	var req = {};
	req.action = "fetchPlanClasses";
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		fillClasses(res.data);

		$('#classes').on('change', function(e) {
			$("#subjects").select2('val',null); // Fills the classes in the select box
			fetchPlanSubjects();
		});
	});
}

/* This function fills the classes in the dropdown */
function fillClasses(classes) {
	var html = "<option val='0'> Select Class </option>";
	$.each(classes, function(i, tag){
		html += '<option id="class_' +tag.id + '" value="' + tag.name + '">' + tag.name + '</option>';
	});
	$('#classes').html(html);
}

/* This function fetches the subjects of the respective classes on change event */
function fetchPlanSubjects() {

	req = {};
	req.action = 'fetchPlanSubjects';
	req.class_id =  $("#classes").find('option:selected').attr("id").substring(6);
	if(req.class_id) {
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1){
				fillPlanSubjects(res.data);
			}
			else{
				$('#subjects').html('');
				alert('No Subject Found In Selected Class.');
			}
		});
	}
}

/* This function fills subjects in the dropdown */
function fillPlanSubjects(subjects) {
	var html = "<option val='0'> Select Subject </option>";
	$.each(subjects,function( i,sub) {
		html += '<option id="subject_' +sub.id + '" value="' + sub.name + '">' + sub.name + '</option>';
	});
	$('#subjects').html(html);
}


/* This function helps to find the recommended hours and minutes according to the dates provided and hours required
for them */
function findingHours(editValue) {

	$("#loading-image").show();
	req = {};
	req.action = 'findingHours';
	req.startDate =  $("#startDate").val();
	req.targetDate = $("#targetDate").val();
	req.offDay = $("#offDay").val();
	req.schoolHoliday = $("#schoolHoliday").val();
	req.dailyOrAlternate = $("input[name=optionsRadios]:checked").val() || 'daily';
	req.editValue = editValue;

	req.subject_id = findSubjectId(editValue);

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)

	}).done(function(res) {

		res = $.parseJSON(res);
		if(res.status == 1){

			$("#recommendedTime").html(res.recommendedHours + "H " + res.recommendedMinutes + "M");
			$("#myButtonContinue").attr("onclick","calculateTimeSlots("+editValue+")");

			//The logic below is used to handle the back and continue button within plan modal
            var total = $('#form_wizard_1').find('li').length;
            var current = 2;
            var index = 1;

  			findCounter(current,index);
            $("#loading-image").hide();

		}
		else
		{
			toastr.error(res.data);
		}

	});

}

/* Helps to find the subject id on the basis of the editvalue  */
function findSubjectId(editValue) {

	if(editValue == 0)
		subject_id = $("#subjects").find('option:selected').attr("id").substring(8);
	else
		subject_id = $("#subjects").closest("div").attr("id");

	return subject_id;
}


/* This function fetches the chapters and their recommended times */
function fetchPlanChaptersTime(editValue) {
	var req = {};
	req.action = "fetchPlanChaptersTime";

	req.subject_id = findSubjectId(editValue);

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		fillPlanChaptersTime(res.data);
	});
}


/* This function fills the chapters and their recommended times in the allotment hours table */
function fillPlanChaptersTime(chapters) {
	var html = "";
	$.each(chapters, function(i, tag){
		html += '<tr><td>' +tag.name +'</td>' + '<td>' +tag.time +'</td>' +
		'<td><input type="text" class="form-control input-sm alloted" id="alloted_'+i+'" onclick="calculateTimeDivision()"></td>' +
		'<td><input type="text" class="form-control input-sm lc" id="lc_'+i+'" onclick="calculateNewTimeDivision()"></td>' +
		'<td><input type="text" class="form-control input-sm pq" id="pq_'+i+'" onclick="calculateNewTimeDivision()"></td>' +
		'<td><input type="text" class="form-control input-sm tr" id="tr_'+i+'" onclick="calculateNewTimeDivision()"></td>' +'</tr>';
	});

	$('#chapterRecommendedTime tbody').html(html);

}

/* This function appends divisions of time for the school day study slots
If there is only one from and to division, the minus image will not show up */
function addSchoolDaysSlots() {

	var descendantChild = $("#schoolDaysSlots > .row").length;

	$("#schoolDaysSlotsSubtract").show();

	var html = '<div class="row" id="schoolDaysSlotsRow_'+descendantChild+'"><div class="col-md-10"><div class="col-md-6"><div class="form-group">'+
	'<label class="control-label">From</label><div class="row"><div class="col-md-12"><div class="input-group">'+
	'<input type="text" class="form-control timepicker timepicker-no-seconds" data-minute-step="30" id="schoolDaysFrom_'+descendantChild+'"><span class="input-group-btn">'+
	'<button class="btn default" type="button"><i class="fa fa-clock-o"></i></button></span></div></div></div></div></div>'+
	'<div class="col-md-6"><div class="form-group"><label class="control-label">To</label><div class="row"><div class="col-md-12">'+
	'<div class="input-group"><input type="text" class="form-control timepicker timepicker-no-seconds" data-minute-step="30" id="schoolDaysTo_'+descendantChild+
	'"><span class="input-group-btn"><button class="btn default" type="button"><i class="fa fa-clock-o"></i></button></span>'+
	'</div></div></div></div></div></div></div>';

	$("#schoolDaysSlots").append(html);

	$("#schoolDaysFrom_"+descendantChild).timepicker();
	$("#schoolDaysTo_"+descendantChild).timepicker();

}

/* This function appends divisions of time for the school holiday study slots
If there is only one from and to division, the minus image will not show up */

function addSchoolHolidaysSlots() {

	var descendantChild = $("#schoolHolidaysSlots > .row").length;

	$("#schoolHolidaysSlotsSubtract").show();

	var html = '<div class="row" id="schoolHolidaysSlotsRow_'+descendantChild+'"><div class="col-md-10"><div class="col-md-6"><div class="form-group">'+
	'<label class="control-label">From</label><div class="row"><div class="col-md-12"><div class="input-group">'+
	'<input type="text" class="form-control timepicker timepicker-no-seconds" data-minute-step="30" id="schoolHolidaysFrom_'+descendantChild+'"><span class="input-group-btn">'+
	'<button class="btn default" type="button"><i class="fa fa-clock-o"></i></button></span></div></div></div></div></div>'+
	'<div class="col-md-6"><div class="form-group"><label class="control-label">To</label><div class="row"><div class="col-md-12">'+
	'<div class="input-group"><input type="text" class="form-control timepicker timepicker-no-seconds" data-minute-step="30" id="schoolHolidaysTo_'+descendantChild+
	'"><span class="input-group-btn"><button class="btn default" type="button"><i class="fa fa-clock-o"></i></button></span>'
	'</div></div></div></div></div></div></div>';

	$("#schoolHolidaysSlots").append(html);

	$("#schoolHolidaysFrom_"+descendantChild).timepicker();
	$("#schoolHolidaysTo_"+descendantChild).timepicker();

}

/* This function deletes divisions of time for the school day study slots */

function deleteSchoolDaysSlots() {

	var descendantChild = $("#schoolDaysSlots > .row").length-1;
	$(".row#schoolDaysSlotsRow_"+descendantChild).remove();

	if(descendantChild == 1)
		$("#schoolDaysSlotsSubtract").hide();

}

/* This function deletes divisions of time for the school holiday study slots */

function deleteSchoolHolidaysSlots() {

	var descendantChild = $("#schoolHolidaysSlots > .row").length-1;
	$(".row#schoolHolidaysSlotsRow_"+descendantChild).remove();

	if(descendantChild == 1)
		$("#schoolHolidaysSlotsSubtract").hide();
}

/* This function pushes from and to times into arrays for further calculation and checks time clashes also */

function calculateTimeSlots(editValue) {

	$("#loading-image").show();
	var i = 0 ;
	var result = {};
	var schoolDaysFrom = new Array();
	var schoolDaysTo = new Array();
	var schoolHolidaysFrom = new Array();
	var schoolHolidaysTo = new Array();

	/* Values of from and to times are pushed into arrays for school days */

	$("#schoolDaysSlots > .row").each(function(i)
	{

		schoolDaysFrom.push($("#schoolDaysSlots > .row").find("input#schoolDaysFrom_"+i).val());
		schoolDaysTo.push($("#schoolDaysSlots > .row").find("input#schoolDaysTo_"+i).val());

	});


	/* Values of from and to times are pushed into arrays for school holidays */

	$("#schoolHolidaysSlots > .row").each(function(i)
	{

		schoolHolidaysFrom.push($("#schoolHolidaysSlots > .row").find("input#schoolHolidaysFrom_"+i).val());
		schoolHolidaysTo.push($("#schoolHolidaysSlots > .row").find("input#schoolHolidaysTo_"+i).val());

	});

	result.schoolDaysFrom = schoolDaysFrom;
	result.schoolDaysTo = schoolDaysTo;
	result.schoolHolidaysFrom = schoolHolidaysFrom;
	result.schoolHolidaysTo = schoolHolidaysTo;
	result.startDate = $("#startDate").val();
	result.targetDate = $("#targetDate").val();
	result.offDay = $("#offDay").val();
	result.schoolHoliday = $("#schoolHoliday").val();

	result.action = "checkTimeClashes"; // Time clashes are found out for the planned subjects of the user, if any
	result.editValue = editValue;
	result.dailyOrAlternate = $("input[name=optionsRadios]:checked").val() || 'daily';
	result.subject_id = findSubjectId(editValue);

		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(result)
		}).done(function(res) {
			res = $.parseJSON(res);

			if(res.status == 1)
			{
				findAvailableHours();
				var current = 3;
            	var index = 2;

  				findCounter(current,index);
			}
			else if(res.status == 0)
			{
				toastr.error(res.message);

			}
			$("#loading-image").hide();

		});

	/* JSON data is returned containing all the arrays  */
	return JSON.stringify(result);
}

function checkSubjectClashes()
{
		var req = {};
		req.action = "checkSubjectClashes";
		req.subject_id = $("#subjects").find('option:selected').attr("id").substring(8);

		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);

		});
}

/* This function calculates the values of LC, PQ and TR on the basis of time alloted for them
Involves automatic calculation of LC, PQ and TR */

function calculateTimeDivision()
{

$(".form-control.input-sm.alloted").on("blur",function(){

 var allotedTimes = $(this).val();
 var lc = ".form-control.input-sm.lc#lc_" + $(this).attr("id").substring(8);
 var pq = ".form-control.input-sm.pq#pq_" + $(this).attr("id").substring(8);
 var tr = ".form-control.input-sm.tr#tr_" + $(this).attr("id").substring(8);

 if(allotedTimes == "")
 {
	 $(lc).val("");
	 $(pq).val("");
	 $(tr).val("");
 }
	 else
	 {
	 	var a = Math.round(0.5*allotedTimes);
	 	var b = Math.round(0.3*allotedTimes);
	 	var c = allotedTimes - (a + b);

		$(lc).val(a);
		$(pq).val(b);
		$(tr).val(c);
	 }
});


}

/* This function automatically calculates the total of LC, PQ and TR if the user does not want to use
the recommended times for the same */

function calculateNewTimeDivision()
{

	$(".form-control.input-sm.lc, .form-control.input-sm.pq, .form-control.input-sm.tr").on("blur",function(){

	 var lc = $(".form-control.input-sm.lc#lc_" + $(this).attr("id").substring(3)).val();
	 var pq = $(".form-control.input-sm.pq#pq_" + $(this).attr("id").substring(3)).val();
	 var tr = $(".form-control.input-sm.tr#tr_" + $(this).attr("id").substring(3)).val();

	 if(lc=="")
	 	lc = 0;
	 if(pq=="")
	 	pq = 0;
	 if(tr=="")
	 	tr = 0;

	 var sum = parseInt(lc)+parseInt(pq)+parseInt(tr);

	  $(".form-control.input-sm.alloted#alloted_" + $(this).attr("id").substring(3)).val(sum);

});


}

/* This function has all the details and helps to populate the data on the calendar */

function populateCalendar(editValue)
{
	//console.log('populate calendar');

	$("#loading-image").show();
	timeSlots = $.parseJSON(calculateTimeSlots(editValue));
	var chapters = new Array();
	var alloted = new Array();
	var rectimes = new Array();
	var lc = new Array();
	var pq = new Array();
	var tr = new Array();

	result = {};
	req = {};
	req.action = 'populateCalendar';
	req.startDate =  $("#startDate").val();
	req.targetDate = $("#targetDate").val();

	req.offDay = $("#offDay").val();

	req.schoolHoliday = $("#schoolHoliday").val();
	req.dailyOrAlternate = $("input[name=optionsRadios]:checked").val() || 'daily';
	req.schoolDaysFrom = timeSlots.schoolDaysFrom;
	req.schoolDaysTo = timeSlots.schoolDaysTo;
	req.schoolHolidaysFrom = timeSlots.schoolHolidaysFrom;
	req.schoolHolidaysTo = timeSlots.schoolHolidaysTo;


	for(i=0;i<$("#chapterRecommendedTime > tbody > tr").find("td:first").length;i++)
	{
		lc.push($("#chapterRecommendedTime > tbody > tr").find("input.lc").eq(i).val());
		pq.push($("#chapterRecommendedTime > tbody > tr").find("input.pq").eq(i).val());
		tr.push($("#chapterRecommendedTime > tbody > tr").find("input.tr").eq(i).val());
		search = $("#chapterRecommendedTime > tbody > tr").find("td:first").eq(i).text();
		var find = ',';
		var re = new RegExp(find, 'g');
		search = search.replace(re, ' ');
	 	search = search.replace(",", " ");
		chapters.push(search);
		rectimes.push($("#chapterRecommendedTime > tbody > tr").find("td:first").eq(i).next().text());
		alloted.push($("#chapterRecommendedTime > tbody > tr").find("td:first").eq(i).next().next().find("input").val())
	}
	req.alloted = alloted;
	req.chapters = chapters;
	req.rectimes = rectimes;
	req.lc = lc;
	req.pq = pq;
	req.tr = tr;

	req.subject_id = findSubjectId(editValue);

		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)

		}).done(function(res) {

			res = $.parseJSON(res);
			location.reload();
			/*if(res.status == 1 || res.status == 2 || res.status == 3)
			{
				//toastr.error(res.data);
				location.reload();
				//calendarFunction(res.planner);
			}
			else if(res.status == 4)
			{
				location.reload();
				//calendarFunction(res.planner);
			}*/

		});

	$("#loading-image").hide();
}

/* This function is used to generate the calendar */
function calendarFunction1(res)
{

	var newstart = new Array();
	var newend = new Array();
	var colors = new Array();

	$(res.start).each(function(i)
	{
		newstart[i] = new Array();
		newend[i] = new Array();
		colors[i] = new Array();

		$(res.start[i]).each(function(j)
		{
			newstart[i].push(res.start[i][j].split("-").reverse().join("-"));
			newend[i].push(res.end[i][j].split("-").reverse().join("-"));
			colors[i].push(res.color[i][j]);
		});
	});

	var i = 0;
	var events_new = [];

	 $(res.start).each(function(i) /* Start times for all the planned subjects */
	   {
	   	$(res.start[i]).each(function(j)  /* Start times for all the planned subjects (iterated again) */
	  	{
	  		$(res.time_from[i][j]).each(function(k) /* Handles multiple time slots  */
	  		{
	  			if(res.subject[i][j] == "Its your offday !!") /* Ensures that if there is off Day, rest data is not shown which would eventually be undefined */
	  			{
			  		events_new.push({

				       	title : res.subject[i][j],
				       	start : newstart[i][j] + "T" + res.time_from[i][j][k],
				       	end : newend[i][j] + "T" + res.time_to[i][j][k],
				       	color : colors[i][j]

			       	});
	  			}
	  			else
	  			{
			   		events_new.push({

				       	title : res.subject[i][j] + "| "+ res.timer_text[i][j] +" : " + res.chapter[i][j],
				       	//title : res.subject[i][j] + "\n"+ res.timer_text[i][j] +" : " + res.chapter[i][j],
				       	start : newstart[i][j] + "T" + res.time_from[i][j][k],
				       	end : newend[i][j] + "T" + res.time_to[i][j][k],
				       	color : colors[i][j],
				       	displayEventEnd : true,
				       	allDay : false
			       	});

			   	}
	       });
	   	});

   	});

	$("#calendar").fullCalendar('destroy');
	$('#calendar').fullCalendar({

        header: {
      		right: 'today prev,next,month,agendaWeek,agendaDay' /* buttons for switching between views */
    	},

   		events : events_new


	});


}
/* This function fetches the names of the subjects that are already planned */

function fetchPlannedSubjects() {
		var req = {};

		req.action = "fetchPlannedSubjects";
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1)
			{
				fillPlannedSubjects(res.data);
			}
		});
	}

/* This function fills the names of the subjects that are already planned */

function fillPlannedSubjects(plannedSubjects) {


	var html = "";
	$.each(plannedSubjects, function(i, subject_id){
		html += '<div class="btn-group-justified mrg-bot5">';
		html += '<a href="#large" onclick="editPlannedSubjects('+subject_id.subject_id+')" class="btn btn-info" data-toggle="modal" data-backdrop="static" style="width: 100%;">'+subject_id.name+'</a>';
		html += '<a onclick="deleteSubject('+subject_id.subject_id+')" class="btn" style="">';
		html += '<i class="fa fa-times pull-right"></i>';
		html += '</a>';
		//html += '<a href="#" class="m-btn red icn-only"><i class="icon-remove icon-white"></i></a>';
		html += '</div>' ;



	});
	$("#subjectPlanned").html(html);

}

/* This function is used for filling values by default from the database so that user can edit it */

function editPlannedSubjects(subject_id)
{
	$("#classChanger").find(".col-md-12").html('<input type="text" class="form-control" readonly id="classes" name="classes">');
	$("#subjectChanger").find(".col-md-12").html('<input type="text" class="form-control" readonly id="subjects" name="subjects">');
	$("#myButtonContinue").attr("onclick","findingHours(1)");
	$("#submitButton").attr("onclick","updatePlannedSubjects()");
	$("#myButtonBack").attr("onclick","handleBack(1)");

	var req = {};
	req.subject_id = subject_id;
	req.action = "editPlannedSubjects";
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			$("#startDate").val(res.data[0]['startDate']);
			$("#targetDate").val(res.data[0]['targetDate']);
			$("#offDay").val(res.data[0]['offDays']);
			$("#schoolHoliday").val(res.data[0]['schoolHolidays']);
			$("#" + res.data[0]['dailyOrAlternate']).prop("checked",true);

			$("#subjects").closest("div").attr("id",res.subjectId); /* addition of the id to subjects div */
			$('#classes').val(res.className);
			$('#subjects').val(res.subjectName);


		/*  Populating the chapter names with their respective times so that editing view is shown with all data */

		$.each(res.chapterName, function(i, tag){
			html += '<tr><td>' +res.chapterName[i] +'</td>' + '<td>' +res.chapterTime[i] +'</td>' +
			'<td><input type="text" class="form-control input-sm alloted" id="alloted_'+i+'" onclick="calculateTimeDivision()" value="'+res.data[0]['alloted'].split(',')[i]+'"></td>' +
			'<td><input type="text" class="form-control input-sm lc" id="lc_'+i+'" value="'+res.data[0]['lc'].split(',')[i]+'"></td>' +
			'<td><input type="text" class="form-control input-sm pq" id="pq_'+i+'" value="'+res.data[0]['pq'].split(',')[i]+'"></td>' +
			'<td><input type="text" class="form-control input-sm tr" id="tr_'+i+'" value="'+res.data[0]['tr'].split(',')[i]+'"></td>' +'</tr>';
		});

		$('#chapterRecommendedTime tbody').html(html);


		/* Logic implemented below creates the required time slots for the purpose of editing
		including when to show the "subtract button" for both school days and holidays */

		for(i=0;i<res.data[0]['schoolDaysSlotsFrom'].split(',').length;i++)
		{
			if(i==0)
			{
				$("#schoolDaysFrom_"+i).timepicker();
				$("#schoolDaysTo_"+i).timepicker();
			}
			else
			{
				$("#schoolDaysSlotsSubtract").show();

				var html = '<div class="row" id="schoolDaysSlotsRow_'+i+'"><div class="col-md-10"><div class="col-md-6"><div class="form-group">'+
				'<label class="control-label">From</label><div class="row"><div class="col-md-12"><div class="input-group">'+
				'<input type="text" class="form-control timepicker timepicker-no-seconds" id="schoolDaysFrom_'+i+'"><span class="input-group-btn">'+
				'<button class="btn default" type="button"><i class="fa fa-clock-o"></i></button></span></div></div></div></div></div>'+
				'<div class="col-md-6"><div class="form-group"><label class="control-label">To</label><div class="row"><div class="col-md-12">'+
				'<div class="input-group"><input type="text" class="form-control timepicker timepicker-no-seconds" id="schoolDaysTo_'+i+
				'"><span class="input-group-btn"><button class="btn default" type="button"><i class="fa fa-clock-o"></i></button></span>'+
				'</div></div></div></div></div></div></div>';

				$("#schoolDaysSlots").append(html);
				$("#schoolDaysFrom_"+i).timepicker();
				$("#schoolDaysTo_"+i).timepicker();
			}

		}

		/* Since time comes in series, the from is filled first and then to so odd values are used to
		fill from times while even ones are used to fill to times hence the logic  */

		i=0; j=0; k=0;
		$("#schoolDaysSlots > .row").find("input").each(function(i)
		{
			if(i%2 == 0)
			{
				$(this).val(res.data[0]['schoolDaysSlotsFrom'].split(',')[j]);
				j++;
			}
			else
			{
				$(this).val(res.data[0]['schoolDaysSlotsTo'].split(',')[k]);
				k++;
			}


		});

		for(i=0;i<res.data[0]['schoolHolidaysSlotsFrom'].split(',').length;i++)
		{
			if(i==0)
			{
				$("#schoolHolidaysFrom_"+i).timepicker();
				$("#schoolHolidaysTo_"+i).timepicker();
			}
			else
			{
				$("#schoolHolidaysSlotsSubtract").show();

				var html = '<div class="row" id="schoolHolidaysSlotsRow_'+i+'"><div class="col-md-10"><div class="col-md-6"><div class="form-group">'+
				'<label class="control-label">From</label><div class="row"><div class="col-md-12"><div class="input-group">'+
				'<input type="text" class="form-control timepicker timepicker-no-seconds" id="schoolHolidaysFrom_'+i+'"><span class="input-group-btn">'+
				'<button class="btn default" type="button"><i class="fa fa-clock-o"></i></button></span></div></div></div></div></div>'+
				'<div class="col-md-6"><div class="form-group"><label class="control-label">To</label><div class="row"><div class="col-md-12">'+
				'<div class="input-group"><input type="text" class="form-control timepicker timepicker-no-seconds" id="schoolHolidaysTo_'+i+
				'"><span class="input-group-btn"><button class="btn default" type="button"><i class="fa fa-clock-o"></i></button></span>'+
				'</div></div></div></div></div></div></div>';

				$("#schoolHolidaysSlots").append(html);
				$("#schoolHolidaysFrom_"+i).timepicker();
				$("#schoolHolidaysTo_"+i).timepicker();
			}
		}

		i=0; j=0; k=0;
		$("#schoolHolidaysSlots > .row").find("input").each(function(i)
		{
			if(i%2 == 0)
			{
				$(this).val(res.data[0]['schoolHolidaysSlotsFrom'].split(',')[j]);
				j++;
			}
			else
			{
				$(this).val(res.data[0]['schoolHolidaysSlotsTo'].split(',')[k]);
				k++;
			}


		});

		}
	});
}

function deleteSubject(subject_id)
{
	if(confirm("Are you sure you want to delete?"))
	{
		var req = {};
		req.subject_id = subject_id;
		req.action = "deletePlannedSubject";
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1)
			{
				toastr.success(res.message);
				setTimeout(function() {document.location = "study-planner.php";}, 1000);
			}
			else
			{
				toastr.error(res.message);
			}
		});
	}
}

/* This function clears the data if any for addition of the new subject */

function changeHeaders()
{
	console.log('changeHeaders');

	$("#classChanger").find(".col-md-12").html('<select class="form-control" id="classes"></select>');
	$("#subjectChanger").find(".col-md-12").html('<select class="form-control" id="subjects"></select>');
	$("#myButtonContinue").attr("onclick","findingHours(0);fetchPlanChaptersTime(0);");
	$("#submitButton").attr("onclick","populateCalendar(0)");
	$("#myButtonBack").attr("onclick","handleBack(0)");
	fetchPlanClasses();
	$("#startDate").val("");
	$("#targetDate").val("");
	$("#offDay").val("");
	$("#schoolHoliday").val("");

	// $("#schoolDaysSlots").html('<div class="row" id="schoolHolidaysSlotsRow_0"><div class="col-md-10"><div class="col-md-6"><div class="form-group">'+
	// 	'<label class="control-label">From</label><div class="row"><div class="col-md-12"><div class="input-group">'+
	// 	'<input type="text" class="form-control timepicker timepicker-no-seconds" id="schoolHolidaysFrom_0"><span class="input-group-btn">'+
	// 	'<button class="btn default" type="button"><i class="fa fa-clock-o"></i></button></span></div></div></div></div></div>'+
	// 	'<div class="col-md-6"><div class="form-group"><label class="control-label">To</label><div class="row"><div class="col-md-12">'+
	// 	'<div class="input-group"><input type="text" class="form-control timepicker timepicker-no-seconds" id="schoolHolidaysTo_0'+
	// 	'"><span class="input-group-btn"><button class="btn default" type="button"><i class="fa fa-clock-o"></i></button></span>'+
	// 	'</div></div></div></div></div><div class="col-md-2"><div class="form-group"><a class="btn btn-circle btn-icon-only blue" href="#" onclick="addSchoolHolidaysSlots()" style="margin: 25px 0px 0px;">'+
	// 	'<i class="fa fa-plus"></i></a></div></div></div></div>');

	// $("#schoolHolidaysSlots").html('<div class="row" id="schoolHolidaysSlotsRow_0"><div class="col-md-10"><div class="col-md-6"><div class="form-group">'+
	// 	'<label class="control-label">From</label><div class="row"><div class="col-md-12"><div class="input-group">'+
	// 	'<input type="text" class="form-control timepicker timepicker-no-seconds" id="schoolHolidaysFrom_0"><span class="input-group-btn">'+
	// 	'<button class="btn default" type="button"><i class="fa fa-clock-o"></i></button></span></div></div></div></div></div>'+
	// 	'<div class="col-md-6"><div class="form-group"><label class="control-label">To</label><div class="row"><div class="col-md-12">'+
	// 	'<div class="input-group"><input type="text" class="form-control timepicker timepicker-no-seconds" id="schoolHolidaysTo_0'+
	// 	'"><span class="input-group-btn"><button class="btn default" type="button"><i class="fa fa-clock-o"></i></button></span>'+
	// 	'</div></div></div></div></div></div></div>');

	$("#schoolDaysFrom_0").timepicker();
	$("#schoolDaysTo_0").timepicker();
	// $("#schoolHolidaysFrom_0").timepicker();
	// $("#schoolHolidaysTo_0").timepicker();
}

/* This function updates the details for the calendar in the database */

function updatePlannedSubjects()
{

	timeSlots = $.parseJSON(calculateTimeSlots(1));
	var chapters = new Array();
	var alloted = new Array();
	var rectimes = new Array();
	var lc = new Array();
	var pq = new Array();
	var tr = new Array();

	result = {};
	req = {};
	req.action = 'updatePlannedSubjects';
	req.startDate =  $("#startDate").val();
	req.targetDate = $("#targetDate").val();
	req.offDay = $("#offDay").val();
	req.schoolHoliday = $("#schoolHoliday").val();
	req.dailyOrAlternate = $("input[name=optionsRadios]:checked").val() || 'daily';
	req.schoolDaysFrom = timeSlots.schoolDaysFrom;
	req.schoolDaysTo = timeSlots.schoolDaysTo;
	req.schoolHolidaysFrom = timeSlots.schoolHolidaysFrom;
	req.schoolHolidaysTo = timeSlots.schoolHolidaysTo;


	for(i=0;i<$("#chapterRecommendedTime > tbody > tr").find("td:first").length;i++)
		{
			lc.push($("#chapterRecommendedTime > tbody > tr").find("input.lc").eq(i).val());
			pq.push($("#chapterRecommendedTime > tbody > tr").find("input.pq").eq(i).val());
			tr.push($("#chapterRecommendedTime > tbody > tr").find("input.tr").eq(i).val());
			search = $("#chapterRecommendedTime > tbody > tr").find("td:first").eq(i).text();
			var find = ',';
			var re = new RegExp(find, 'g');
			search = search.replace(re, ' ');
		 	search = search.replace(",", " ");
			chapters.push(search);

			//chapters.push($("#chapterRecommendedTime > tbody > tr").find("td:first").eq(i).text());
			rectimes.push($("#chapterRecommendedTime > tbody > tr").find("td:first").eq(i).next().text());
			alloted.push($("#chapterRecommendedTime > tbody > tr").find("td:first").eq(i).next().next().find("input").val())
		}
	req.alloted = alloted;
	req.chapters = chapters;
	req.rectimes = rectimes;
	req.lc = lc;
	req.pq = pq;
	req.tr = tr;

	req.subject_id = $("#subjects").closest("div").attr("id");


	$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)

		}).done(function(res) {

			location.reload();
			//res = $.parseJSON(res);

		});


}

/* This function is used for the purpose of proper functioning of back, continue and submit buttons */

function findCounter(current,index)
{
	/* Modified logic of form-wizard js so that proper back and forward functionality provided in modal  */

	var total = 3;

	var li_list = $('#form_wizard_1').find('li');

        for (var i = 0; i < index; i++) {
            jQuery(li_list[i]).addClass("done");
            jQuery(li_list[i+1]).addClass("active");
        }

       $('#form_wizard_1').find("#tab"+parseInt(index)).removeClass("active");
	   $('#form_wizard_1').find("#tab"+parseInt(current)).addClass("active");

        if (current == 1) {
            $('#form_wizard_1').find('.button-previous').hide();
        } else {
            $('#form_wizard_1').find('.button-previous').show();
        }

        if (current >= total) {
            $('#form_wizard_1').find('.button-next').hide();
            $('#form_wizard_1').find('.button-submit').show();

        } else {
            $('#form_wizard_1').find('.button-next').show();
            $('#form_wizard_1').find('.button-submit').hide();
        }


}

/* This function handles the functionality of the back button to be implemented properly */

function handleBack(editValue)
{
	var back = $("#form_wizard_1").find("li.active").last().find("a").attr("href").substring(4);
	var backer = parseInt(back);
	if(backer == 2)
	{
		$("#form_wizard_1").find(".button-previous").hide();
		$('#myButtonContinue').attr("onclick","findingHours("+editValue+")");
	}
	$("#form_wizard_1").find("li.active:eq("+(backer-1)+")").removeClass("active");
	$("#form_wizard_1").find("li.active:eq("+(backer-2)+")").removeClass("done");
	$("#tab"+(backer)).removeClass("active");
	$("#tab"+(backer-1)).addClass("active");
	$('#form_wizard_1').find('.button-submit').hide();
	$('#form_wizard_1').find('.button-next').show();

}

$(function() {
	$("#loading-image").hide();
	fetchPlannedSubjects();
	calendar();

            // $('#form_wizard_1').bootstrapWizard({

            //      onTabClick: function (tab, navigation, index, clickedIndex) {
            //         return false;
            //     }

            // });

			$('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function () {
                //alert('Finished! Hope you like it :)');
                var index = 3;
                var counter = 1;
                findCounter(index,counter);
                $('#large').hide();
                //location.reload();
            }).hide();


	$("#schoolDaysSlotsSubtract").hide();
	$("#schoolHolidaysSlotsSubtract").hide();
});

/* This function populates the calendar when the user logs in for the planned subjects */

function calendar()
{
		//console.log('calendar');

		var req = {};
		req.action = "calendar";

		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)

		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1)
			{
				calendarFunction(res.planner);
			}
			else if(res.status == 0)
			{
				//toastr.error(res.planner);
			}

		});
}


function findAvailableHours(){
	console.log('findAvailableHours');

	var req = {};
	req.action = "findAvailableHours";
	req.subject_id = findSubjectId(1);
	req.editvalue = 1;
	var schoolDaysFrom = new Array();
	var schoolDaysTo = new Array();
	var schoolHolidaysFrom = new Array();
	var schoolHolidaysTo = new Array();

	/* Values of from and to times are pushed into arrays for school days */

	$("#schoolDaysSlots > .row").each(function(i)
	{

		schoolDaysFrom.push($("#schoolDaysSlots > .row").find("input#schoolDaysFrom_"+i).val());
		schoolDaysTo.push($("#schoolDaysSlots > .row").find("input#schoolDaysTo_"+i).val());

	});


	/* Values of from and to times are pushed into arrays for school holidays */

	$("#schoolHolidaysSlots > .row").each(function(i)
	{

		schoolHolidaysFrom.push($("#schoolHolidaysSlots > .row").find("input#schoolHolidaysFrom_"+i).val());
		schoolHolidaysTo.push($("#schoolHolidaysSlots > .row").find("input#schoolHolidaysTo_"+i).val());

	});

	req.schoolDaysFrom = schoolDaysFrom;
	req.schoolDaysTo = schoolDaysTo;
	req.schoolHolidaysFrom = schoolHolidaysFrom;
	req.schoolHolidaysTo = schoolHolidaysTo;
	req.startDate = $("#startDate").val();
	req.targetDate = $("#targetDate").val();
	req.offDay = $("#offDay").val();
	req.schoolHoliday = $("#schoolHoliday").val();
	req.dailyOrAlternate = $("input[name=optionsRadios]:checked").val() || 'daily';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)

	}).done(function(res) {
		res = $.parseJSON(res);
		console.log(res);
		if(res.status == 1)
		{
			$('#totalAvailableHours').html(res.hours+'H '+res.minutes+'M');
		}
		else if(res.status == 0)
		{
		}
	});
}

function calendarFunction(res){
	//alert("planner");
	var events_new = [];

	$.each(res,function(i,plan){
		if(plan.type != 'off')
		{
			events_new.push({

		       	title : plan.type+ " : " +plan.chap,
		       	start : plan.day + "T" + plan.start,
		       	end : plan.day + "T" + plan.end,
		       	color : plan.color

		       	//allDay : false*/
	    	});
		}
		else
		{
			events_new.push({
		       	title : plan.chap,
		       	start : plan.day + "T" + plan.start,
		       	end : plan.day + "T" + plan.end,
		       	color : plan.color,
		       	displayEventEnd : true,
		       	allDay : true
	    	});
		}
	})


	$("#calendar").fullCalendar('destroy');
	$('#calendar').fullCalendar({

            header: {

            	right: 'today prev,next month,agendaWeek,agendaDay'
        		//right: 'today prev,next,month,agendaWeek,agendaDay' /* buttons for switching between views */
   			},
    		events : events_new

	});
	//console.log(events_new);
}