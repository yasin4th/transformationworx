var paper;
$(function() {
	paper = getUrlParameter('paper');
	fetchOptmizerReport();
});

function fetchOptmizerReport() {
	var req = {};
	req.action = 'fetchOptmizerReport';
	req.paper_id = paper;
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req) 
	}).done(function(res){
		res = $.parseJSON(res);
		if (res.status == 1) {
			// console.log(res);
			fillOptmizerReport(res);
		} else{
			toastr.error(res.message);
		}; 

	});
}

function fillOptmizerReport(report) {
	html = '<tr>';
	html += '<td>'+ report.unattempted_result.attempted +'</td>';
	html += '<td>'+ report.unattempted_result.correct +'</td>';
	html += '<td>'+ report.unattempted_result.wrong +'</td>';
	html += '</tr>';
	
	$('table#un-attempted-report-table tbody').html(html);
	
	html = '<tr>';
	html += '<td>'+ report.wrong_questoions_result.attempted +'</td>';
	html += '<td>'+ report.wrong_questoions_result.correct +'</td>';
	html += '<td>'+ report.wrong_questoions_result.wrong +'</td>';
	html += '</tr>';
	
	$('table#worng-report-table tbody').html(html);
}