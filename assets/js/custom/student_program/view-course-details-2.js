test_program = getUrlParameter('program_id');
subject = getUrlParameter('parent_id');
$(function(){
	// this is for BCT(BreadCrumb Trail)
	$('.back-to-subjects').attr('href', 'student-course-details.php?program_id='+test_program);
	$('.back-to-chapters').attr('href', 'student-course-chapters.php?program_id='+test_program+'&subject='+subject);

displayTestPapers();

});

function displayTestPapers()
{

	var program_id = getUrlVars()["program_id"];
	var category_id = getUrlVars()["category_id"];
	var html = "";
	var i =0;

	/*var req = {};
	req.action = "displayTestPapers";
	req.program_id = program_id;
	req.category_id = category_id;*/

	req = {};
	req.action = 'displayTestPapers';
	req.parent_id = getUrlParameter('parent_id');
	req.parent_type = getUrlParameter('parent_type');
	req.program_id = getUrlParameter('program_id');
	req.type = getUrlParameter('type');
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)

	}).done(function(res) {
		res = $.parseJSON(res);
		console.log(res);
		if(res.status == 1)
		{
			$(res.data).each(function(i)
			{
				html += '<tr> <td>'+res.data[i]['name']+'</td> <td>'+res.data[i]['start_time']+'</td> <td>'+res.data[i]['end_time']+'</td> <td>' +res.data[i]['total_attempts']+' </td> <td> '+(res.data[i]['total_attempts'] - res.data[i]['attempted'])+' </td> <td> <a href="question-paper.php?test_paper='+res.data[i]['id']+'">';
				if(res.data[i]['status']) {
					html += 'Resume Exam';
				}
				else {
					html += 'Start Exam';
				}

				html += '</a></td><td><a href="">See Results</a></td></tr>';
			});
		}

		$("#sample_editable_1").append(html);

	});
}


function getUrlVars()
{
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	vars[key] = value;
	});
	return vars;
}



