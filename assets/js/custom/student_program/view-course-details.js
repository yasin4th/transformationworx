test_program = getUrlParameter('program_id');
subject = getUrlParameter('subject');
chapter = getUrlParameter('parent_id');
$(function(){
	// this is for BCT(BreadCrumb Trail)
	$('.back-to-subjects').attr('href', 'student-course-details.php?program_id='+test_program);
	$('.back-to-chapters').attr('href', 'student-course-chapters.php?program_id='+test_program+'&subject='+subject);
	$('.back-selected-chapter').attr('href', 'student-course-chapter-tests-types.php?program_id='+test_program+'&subject='+subject+'&chapter='+chapter)
	fetchTestProgramDetails();
	displayTestPapers();
});

function fetchTestProgramDetails() {
	var req = {};
	req.action = "fetchTestProgramDetails";
	req.test_program = test_program;

	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			
			$('.test-program').html(res.details.name);
			setBreadcrumb(res.subjectsList,res.chapters);
			// $('#test-program-name').val(res.details.name);
			// $('#test-program-status').val(res.details.status);
			// $('#test-program-class').val(res.details.class);
			// $('#test-program-subjects').val(res.subjects).select2();
			// $('#test-program-price').val(res.details.price);
			// $('#test-program-description').html(res.details.description);
		}
		else {
			toastr.error(res.message,'Error');
		}
			
	});
}

function displayTestPapers()
{

	var program_id = getUrlVars()["program_id"];
	var category_id = getUrlVars()["category_id"];
	var html = "";
	var i =0;

	/*var req = {};
	req.action = "displayTestPapers";
	req.program_id = program_id;
	req.category_id = category_id;*/

	req = {};
	req.action = 'displayTestPapers';
	req.parent_id = getUrlParameter('parent_id');
	req.parent_type = getUrlParameter('parent_type');
	req.program_id = getUrlParameter('program_id');
	req.type = getUrlParameter('type');
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)

	}).done(function(res) {
		res = $.parseJSON(res);
		
		if(res.status == 1)
		{
			$(res.data).each(function(i) {
				href= 'question-paper.php?program_id='+test_program+'&test_paper='+res.data[i]['id'];
				remaining_attempts = res.data[i]['total_attempts'] - res.data[i]['attempted'];
				if(remaining_attempts < 0){
					remaining_attempts = 0;
					href = '';
				}
				total_attempts = res.data[i]['total_attempts'];
				if(res.data[i]['type']=="6"){
					total_attempts ="N/A";
					remaining_attempts="N/A";
					href= 'question-paper.php?program_id='+test_program+'&test_paper='+res.data[i]['id'];
				}

				html += '<tr> <td>'+res.data[i]['name']+'</td>';
				// html += '<td>'+res.data[i]['start_time']+'</td> <td>'+res.data[i]['end_time']+'</td>';
				html += ' <td>' +total_attempts+' </td> <td> '+remaining_attempts+' </td> <td>';
				if(res.data[i]['type']=="6"){
					html += '<a href="'+href+'"">Start Exam</a>';
				}else{
					if(remaining_attempts > 0 ){
						html += ' <a href="'+href+'">';
						if(res.data[i]['status'] == 2) {
							html += 'Start Exam';
						}
						else if(res.data[i]['status'] == 1){
							html += 'Resume Exam';
						}else{
							html += 'Start Exam';
						}
						html += '</a>';
					}else{
						html += 'Start Exam';
					}
				}


				html += '</td>';


				if(res.data[i]['type']=="6"){
					html += '<td>See Results</td></tr>';
				}else{
					if(res.data[i]['attempted'] > 0 ){
						result_href = 'my-test-history.php?attempt='+res.data[i]['attempt_first'];
						html += '<td><a href="'+result_href+'">See Results</a></td></tr>';
					}else{
						result_href = '';
						html += '<td>See Results</td></tr>';
					}
				}
				//html += '<td><a href="'+result_href+'">See Results</a></td></tr>';
			});
		} 

		$("#sample_editable_1").append(html);

	});
}

function getUrlVars() 
{
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	vars[key] = value;
	});
	return vars;
}

setBreadcrumb = function(subjects,chapters) {
	
	$.each(subjects, function(i, sub){
		if(sub.id==subject){
			$('.subject').html(sub.name);
			$('a.subject').attr('href','chapters-in-test-program.php?test_program='+test_program+'&subject='+subject);
			//$('.subject').text(sub.name);
			
		}
	});
	$.each(chapters, function(i, chap){
		if(chap.chapter_id==chapter){
			$('.chapter').html(chap.chapter_name);
			$('a.chapter').attr('href','student-course-chapter-tests-types.php?program_id='+test_program+'&subject='+subject+'&chapter='+chapter);			
		}
	});
}