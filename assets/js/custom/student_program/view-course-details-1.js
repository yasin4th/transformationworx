test_program = getUrlParameter('program_id');
subject = getUrlParameter('subject');
$(function(){
	// this is for BCT(BreadCrumb Trail)
	//$('.back-to-subjects').attr('href', 'student-course-details.php?program_id='+test_program);
	$('.test-program').attr('href', 'student-course-details.php?program_id='+test_program);

	displayTestPapers();
	fetchTestProgramDetails()
});


function fetchTestProgramDetails() {
	var req = {};
	req.action = "fetchTestProgramDetails";
	req.test_program = test_program;

	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			
			$('.test-program').html(res.details.name);			

			//setBreadcrumb(res.subjectsList);
			// $('#test-program-name').val(res.details.name);
			// $('#test-program-status').val(res.details.status);
			// $('#test-program-class').val(res.details.class);
			// $('#test-program-subjects').val(res.subjects).select2();
			// $('#test-program-price').val(res.details.price);
			// $('#test-program-description').html(res.details.description);
		}
		else {
			toastr.error(res.message,'Error');
		}
			
	});
}

function displayTestPapers()
{

	var program_id = getUrlVars()["program_id"];
	var category_id = getUrlVars()["category_id"];
	var html = "";
	var i =0;

	/*var req = {};
	req.action = "displayTestPapers";
	req.program_id = program_id;
	req.category_id = category_id;*/

	req = {};
	req.action = 'displayTestPapers';
	req.parent_id = getUrlParameter('parent_id');
	req.parent_type = getUrlParameter('parent_type');
	req.program_id = getUrlParameter('program_id');
	req.type = getUrlParameter('type');
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)

	}).done(function(res) {
		res = $.parseJSON(res);
		
		if(res.status == 1)
		{
			$(res.data).each(function(i)
			{
				href= 'question-paper.php?program_id='+test_program+'&test_paper='+res.data[i]['id'];
				remaining_attempts = res.data[i]['total_attempts'] - res.data[i]['attempted'];
				total_attempts = res.data[i]['total_attempts'];
				

				if(res.data[i]['type']=='5'){
					setEventStartTest();
				}

				if(res.data[i]['type'] != '5'){
					start_time = 'N/A';
					end_time = 'N/A';
				}else{
					start_time = res.data[i]['start_time'];
					end_time = res.data[i]['end_time'];
				}
				
				if(remaining_attempts < 0){
					remaining_attempts = 0;
					href = '#';
				}

				if(res.data[i]['type']=="6"){
					total_attempts ="N/A";
					remaining_attempts="N/A";
					href= 'question-paper.php?program_id='+test_program+'&test_paper='+res.data[i]['id'];		
				}
				if(res.data[i]['status'] == 0){
					href = '#';
				}

				html += '<tr> <td>'+res.data[i]['name']+'</td>  <td>'+start_time+'</td> <td>'+end_time+'</td>';
				// html += '<td>'+res.data[i]['start_time']+'</td> <td>'+res.data[i]['end_time']+'</td>';
				html += ' <td>' +total_attempts+' </td>';
				html += ' <td> '+remaining_attempts+' </td>';
				html += ' <td> <a href="'+href+'" class="startTest" type="'+res.data[i]["type"]+'" paper="'+res.data[i]["id"]+'">';
				if(res.data[i]['type']=="6"){
					html += 'Start Exam';
				}else if(res.data[i]['status'] == 2) {
					html += 'Start Exam';
				}
				else if(res.data[i]['status'] == 1) {
					html += 'Resume Exam';
				}else {
					html += 'Start Exam';
				}

				html += '</a></td>';


				if(res.data[i]['attempted'] > 0){
					result_href = 'my-test-history.php?attempt='+res.data[i]['attempt_first'];
					html += '<td><a href="'+result_href+'">See Results</a></td></tr>';
				}else{
					result_href = '';
					html += '<td>See Results</td></tr>';
				}


				// html += '<tr> <td>'+res.data[i]['name']+'</td> <td>'+res.data[i]['start_time']+'</td> <td>'+res.data[i]['end_time']+'</td> <td>' +res.data[i]['total_attempts']+' </td> <td> '+(res.data[i]['total_attempts'] - res.data[i]['attempted'])+' </td> <td> <a href="question-paper.php?test_paper='+res.data[i]['id']+'">';
				// remaining_attempts = res.data[i]['total_attempts'] - res.data[i]['attempted'];
				// if(remaining_attempts < 0){
				// 	remaining_attempts = 0;
					
				// }
				// total_attempts = res.data[i]['total_attempts'];

				// if(res.data[i]['status']) {
				// 	html += 'Resume Exam';
				// }
				// else {
				// 	html += 'Start Exam';
				// }

				// html += '</a></td>';
				// if( res.data[i]['attempted'] > 0){
				// 	html += '<td><a href="my-test-history.php?attempt='+res.data[i]['attempt_first']+'">See Results</a></td>';
				// }else{
				// 	html += '<td>See Results</td>';
				// }
				// html += '</tr>';
			});
		}

		$("#sample_editable_1").append(html);
		


	});
}


function getUrlVars() 
{
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	vars[key] = value;
	});
	return vars;
}

function setEventStartTest() {
	console.log("set");
	
	$('.startTest').off('click');
	$('.startTest').on('click',function(e){
		e.preventDefault();
		
		var type = $(this).attr('type');
		var paper = $(this).attr('paper');

		if(type == '5'){
		
			var program_id = getUrlVars()["program_id"];
			var category_id = getUrlVars()["category_id"];
			var html = "";
					

			req = {};
			req.action = 'displayTestPapers';
			req.parent_id = getUrlParameter('parent_id');
			req.parent_type = getUrlParameter('parent_type');
			req.program_id = getUrlParameter('program_id');
			req.type = getUrlParameter('type');
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)

			}).done(function(res) {
				res = $.parseJSON(res);
				
				if(res.status == 1)
				{
					$(res.data).each(function(i)
					{
						if(paper == res.data[i]['id'] && res.data[i]['status'] == 1){
							console.log(paper);
							
							//window.location = "question-paper.php?test_paper="+res.data[i]['id']+"&program_id="+test_program;

						}else if(paper == res.data[i]['id']){
							alert("Scheduled Test will start on "+res.data[i]['start_time']+" to "+res.data[i]['end_time']+". Try again later.");
						}
					});
				}


			});
		}else{
			$('.startTest').unbind('click');
		}
	});

}


