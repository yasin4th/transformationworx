$(function() {


});
displayPrograms();

function displayPrograms()
{
	var req = {};
	req.action = "displayPrograms";

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)

	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			fillPrograms(res.data);
		}
	});
}

function fillPrograms(programs) {
	// var html = "<div class='col-md-12'>No exams enrolled.</div>";
	var html = `<div class="col-md-6 col-md-offset-3">
					<p>Welcome. Please register and pay for the exam.</p>
					<h2>
						<a href="exams">Let's Enroll.</a>
					</h2>
				</div>`;
	if(programs.length != 0){
		html = '<div class="">';
		$(programs).each(function(i,program){

			html += '<div class="col-md-3 col-sm-6 col-xs-12">';
			html += '<div class="form-group">';
			html += '<div class="course"><a href="#" onclick="urlDisplayProgramDetails(this)" id="'+program.id+'"><em>'+
			'<img src="'+program.image+'" alt="Amazing Project" class="img-responsive" >'+
			'</em><span class="recent-work-description"><strong>'+program.name+'</strong>'+
			'<!--<b>'+program.description+'</b>--></span></a></div></div></div>';

			// html += '<div class="col-md-9 col-xs-9">'+
			// '<div class="table-scrollable" style="margin: 0 !important;">'+
			// '<table class="table table-striped table-hover table-bordered">'+
			// '	<thead>'+
			// '		<tr>'+
			// '			<th>Subject</th>'+
			// '			<th>Chapter Test</th>'+
			// '			<th>Practice Test</th>'+
			// '			<th>Mock Test</th>'+
			// '			<th>Diagnostic Test</th>'+
			// '			<th>Schedule Test</th>'+
			// '		</tr>'+
			// '	</thead>'+
			// '	<tbody>';
			// $.each(program.subjects, function(j,subject) {
			// 	html += '<tr>';
			// 	html += '<td>';
			// 	html += subject.name;
			// 	html += '</td>';
			// 	html += '<td>';
			// 	html += subject.chapter;
			// 	html += '</td>';
			// 	html += '<td>';
			// 	html += subject.practice;
			// 	html += '</td>';
			// 	html += '<td>';
			// 	html += subject.mock;
			// 	html += '</td>';
			// 	html += '<td>';
			// 	html += subject.diagnostic;
			// 	html += '</td>';
			// 	html += '<td>';
			// 	html += subject.scheduled;
			// 	html += '</td>';

			// 	html += '</tr>';
			// })
			// html += '</tbody>'+
			// 		'</table></div></div>';

		});
		html += '</div>';
		
	}



	$("#courseDescriptions").append(html);


}

function urlDisplayProgramDetails(program)
{	
	// location.replace("student-course-details.php?program_id="+program.id);
	location.replace("test-papers.php?program_id="+program.id);
	//location.replace("test-details.php?program_id="+program.id);
}
