$(function(){

displayProgramDetails();

	$('.more').on('click', function(e) {
		e.preventDefault();
		

		location.replace("view-course-details.php?program_id="+getUrlParameter("program_id")+"&category_id="+category_id);
	})
});

function displayProgramDetails()
{
	var program_id = getUrlParameter("program_id");
	var i =0;

	var req = {};
	req.action = "displayProgramDetails";
	req.program_id = program_id;

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)

	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			$(res.data).each(function(i)
			{
				$(".row").find("#"+ res.data[i]['test_paper_type_id'] +" .number").html(res.data[i]['details_count']);
				$(".row").find("#"+ res.data[i]['test_paper_type_id'] +" a").attr("id", "category-" + res.data[i]['test_paper_type_id'] +  " program-"+getUrlParameter("program_id"));
			});
		}

	});
}



function urlDisplayTestPapers(data)
{
	var program_id = data.id.split(" ")[1].substring(8);
	var category_id = data.id.split(" ")[0].substring(9);
	location.replace("view-course-details.php?program_id="+program_id+"&category_id="+category_id);
}