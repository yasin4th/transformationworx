$(function() {

	$('#forget-password-form').off('submit');
	$('#forget-password-form').on('submit', function(e) {
		e.preventDefault();
		email = $('#email').val();
		if (email.length > 5) {
			var req = {};
			req.email = email;
			req.action = 'forgotPassword';
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				// console.log(res);
				if(res.status == 1) 
				{
					toastr.success(res.message)
				}
				else 
				{
					toastr.error(res.message,'Error:');
				}
			});
		} else {
			toastr.error("Invalid Email ID.",'Error:');
		};
	});
})