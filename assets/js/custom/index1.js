var Programs, Papers;

$(function() {
	//getTestprogramsAvailableForSave();
	getTestprogramsAvailableForSavePopular();

	$('[data-toggle=dropdown]').on('click',function(e){
		parent = $(this).parent();
		parent.addClass('open');
		return false;
	});
	$('#demo-request').on('click',function(e){
		e.preventDefault();

        var req = {};
		req.action = "buyTestProgram";
		req.program_id = 9;

		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == 1)
			{
				toastr.success("Demo Package is added in your my package.");
				setTimeout(window.location = "my-program",1000);
			}
			else if(res.status == 2)
			{
				toastr.warning("To request Demo please login.");
				window.location = "login";
			}
			else if(res.status == 0)
			{
				toastr.error("Demo already requested.");
			}

		});


	});


});

function getTestprogramsAvailableForSavePopular() {
	var req = {};
	req.action = "filterPrograms";
	req.option = "1";
	req.checkExpiry = "1";
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		Programs = res.data;
		Papers = res.papers;
		if(res.status == 1){
			fillTestprogramsPopular(res.data);
		}else if(res.status == 0)
		{
			//toastr.error(res.message);
		}
	});
}



function fillTestprogramsPopular(programs) {
	//console.log(programs);
	html = '';
	$.each(programs, function(p,program) {
		
		html += '<div class="item test-program" data-id="'+program.id+'">';
		html += '<a href="exams/'+program.name.dash()+'_'+program.id+'" class="fxdc mh290-force-xs"><span class="course-thumb"><img src="'+program.image+'" alt="'+program.name+'"></span><span class="card-info fx fxdc"><span class="title">'+program.name+'</span><span class="card-details fxac dib-lg"><span class="price">CAD '+program.price+'</span></span></span></a>';
		html += '</div>';

/*
		html += '<li class="test-program" data-id="'+program.id+'" class="fn-force-lg m10-force-lg">';
		html += '<a href="exams/'+program.name.dash()+'_'+program.id+'" class="fxdc mh290-force-xs">';
		html += '<span class="course-thumb">';
		html += '<img src="'+program.image+'" alt="'+program.name+'">';
		html += '</span>';

		html += '<span class="card-info fx fxdc">';
		html += '<span class="title">';
		html += program.name;
		html += '</span>';


		html += '<span class="price">';
		html += '<i class="fa fa-inr"></i> ';
		html += program.price;

		html += '</span>';
		html += '</span>';
		html += '</span>';
		html += '</a>';
		html += '</li> ';*/
	});
	$('#owl-demo').html(html);

    // setEventOnTestProgramClick();
	$("#owl-demo").owlCarousel({
                autoPlay: 3000,
                items : 4,
                itemsDesktop : [1199,3],
                itemsDesktopSmall : [979,3]
        });

  	// setEventBuyPrograme();
}

function setEventOnTestProgramClick() {
	$('.test-program').off('click');
	$('.test-program').on('click',function(e){
		e.preventDefault();
		var req = {};
		req.id = $(this).data('id');

		price = 0;
		image = '';
		description = '';

		$.each(Programs, function(p,program) {
			if(req.id == program.id)
			{
				name=program.name;
				price = 'CAD '+program.price;
				description = program.description;
				image = program.image;
			}
		});

		var count = 0;
		html = '<table class="table table-striped table-hover table-bordered"><thead><tr><th>Test Topic</th><th>No. of Tests</th></tr></thead><tbody>';
		$.each(Papers, function(p,paper) {
			if(req.id == paper.test_program_id && paper.test_paper_type_id == 1)
			{

				html += '<tr>';
				html += '<td>'+paper.test_paper_type+'</td>';
				html += '<td>'+paper.count+'</td>';
				html += '</tr>';

				count++;
			}
		});
		$.each(Papers, function(p,paper) {
			if(req.id == paper.test_program_id && paper.test_paper_type_id == 6)
			{

				html += '<tr>';
				html += '<td>'+paper.test_paper_type+'</td>';
				html += '<td>'+paper.count+'</td>';
				html += '</tr>';

				count++;
			}
		});
		$.each(Papers, function(p,paper) {
			if(req.id == paper.test_program_id && paper.test_paper_type_id == 3)
			{
				html += '<tr>';
				html += '<td>'+paper.test_paper_type+'</td>';
				html += '<td>'+paper.count+'</td>';
				html += '</tr>';

				count++;
			}
		});
		$.each(Papers, function(p,paper) {
			if(req.id == paper.test_program_id && paper.test_paper_type_id == 4)
			{
				html += '<tr>';
				html += '<td>'+paper.test_paper_type+'</td>';
				html += '<td>'+paper.count+'</td>';
				html += '</tr>';

				count++;
			}
		});
		$.each(Papers, function(p,paper) {
			if(req.id == paper.test_program_id && paper.test_paper_type_id == 5)
			{
				html += '<tr>';
				html += '<td>'+paper.test_paper_type+'</td>';
				html += '<td>'+paper.count+'</td>';
				html += '</tr>';

				count++;
			}
		});
		html += '</tbody></table>';

		$('#program-details-modal .program-name').html(name);
		$('#program-details-modal .image').attr('src',image);
		$('#program-details-modal .price').html(price);
		$('#program-details-modal .description').html(description);

		if(count>0)
		{
			$('#program-details-modal #modal-table').html(html);
		}
		else
		{
			$('#program-details-modal #modal-table').html('No Tests Available in this Program...');
		}
		$('#program-details-modal').attr('data-program-id',req.id);
		$('#program-details-modal').modal("show");
	})
}

function setEventBuyPrograme()
{

	$('.buy-program').off('click');
	$('.buy-program').on('click', function(e) {
		e.preventDefault();
		buyTestProgram();
	})
}

function buyTestProgram() {
	var req = {};
	req.action = "buyTestProgram";
	req.program_id = $('#program-details-modal').attr('data-program-id');

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res){
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			toastr.success(res.message);
			$('#program-details-modal').modal("hide");
		}
		else if(res.status == 2)
		{
			toastr.warning(res.message);
			window.location = "login.php";
		}
		else if(res.status == 0)
		{
			toastr.error(res.message);
		}

	});
}

