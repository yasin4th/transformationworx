$(function() {

	getQuestionDetails();
})

// function to fetch question tags
function getQuestionDetails() {
	var req = {};
	req.action = 'getQuestionDetails';
	req.question_id = getUrlParameter('question_id');
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
			// console.log(res);
		if(res.status == 1) {
			console.log(res);

			$('#question').html(res.questionData.question);
			$('#solution').html(res.questionData.description);
			fillOptions("1",res.options);
		}
		else{
			toastr.error('Error');
		}
	});
	
}

function fillOptions(type,options) {
	html ='';

	switch(type) {
		case "1":
			$.each(options, function(i, option) {
				
				if (parseInt(option.answer) == 1) {
					html += '<li class="green">'+ option.option +'</li>';			
				}else {
					html += '<li>'+ option.option +'</li>';
				}
			});
			break;
			
		case "2":
			$.each(options, function(i, option) {
				
				if (parseInt(option.answer) == 1) {
					html += '<li class="green">'+ option.option +'</li>';			
				}else {
					html += '<li>'+ option.option +'</li>';
				}
			});
			break;
			
		default:
			break;
	}

	$('#option').html(html);
}