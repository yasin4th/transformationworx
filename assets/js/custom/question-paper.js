test_program = getUrlParameter('program_id');
var start = '0';
var end = '0';

$(function() {
fetchSections();
	// $("#launch-question-paper").attr('href',"launch.php");
	/*$("#launch-question-paper").on("click", function(e) {
		e.preventDefault();
		window.location = "launch.php?program="+test_program+"&paper="+getUrlParameter("test_paper");
	})*/
})
var test_paper_type;
function fetchSections() {
	var req = {};
	req.action = "fetchSections";
	req.test_paper_id = getUrlParameter("test_paper");
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			/*console.clear('log');
			console.log(res);*/
			$("#total-test-paper-question").text(res.total_question);
			test_paper_type = res.test_paper_details.type;
			fillSections(res.sections);
			fillPaperDetails(res.test_paper_details);
			start = res.test_paper_details.start_time;
			end = res.test_paper_details.end_time;

			if(test_paper_type == '5')
			{
				$('#launch-question-paper').removeAttr('onclick');
			}
			else
			{
				$.cookie('paper', res.test_paper_details.id);
				$.cookie('program', test_program);
				
			}
			// $("#launch-question-paper").text(' Start Test ');
		}
		else if(res.status == 0)
		{
			alert(res.message);
		}
	});
}

function fillPaperDetails (test_paper) {
	html = '';
	$('.TestName').html('<strong>'+test_paper.name+'</strong>');

	if(test_paper.type == '6')
	{
		$('#test_paper_time').text('N/A');
	}
	else
	{
		$('#test_paper_time').text(test_paper.time+' Mins');
	}

	$('#test_paper_instructions').html(test_paper.Instructions);
	
	if(test_paper.type == 5)
	{
		setEventScheduled();
	}
}

function fillSections(sections) {
	html = '';
	$.each(sections, function(i,section) {
		if(test_paper_type == '6'){	
			cut_off_marks = "N/A";
			max_marks = "N/A";
			neg_marks = "N/A";
		}else{
			//cut_off_marks = section.cut_off_marks;
			cut_off_marks = "N/A";
			max_marks = section.max_marks;
			neg_marks = section.neg_marks;
		}
		html += '<tr><td><div class="padd5">'+(i+1)+'</div></td><td><div class="padd5">'+section.name+'</div></td><td><div class="padd5">'+section.total_question+'</div></td><td><div class="padd5">'+cut_off_marks+'</div></td><td><div class="padd5">'+max_marks+'</div></td><td><div class="padd5">'+neg_marks+'</div></td></tr>';
	});
	$('#sections tbody').html(html);
}

setEventScheduled = function() {
	$('#launch-question-paper').on('click',function(e) {
		e.preventDefault();
		var href = "launch.php";
		var paper = getUrlParameter("test_paper");
		
		var req = {};
		req.progrm_id = test_program;
		req.paper = paper;
		req.action = 'checkScheduled';

		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req) 
		}).done(function(res) {
			res = $.parseJSON(res);
			
			if(res.status == 1)
			{
				//window.open('launch.php','Test Paper','height=screen.availHeight,width=screen.availWidth,toolbar=yes,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes');
				//window.location = href;
				$.cookie('paper', getUrlParameter('test_paper'));
				$.cookie('program', test_program);
				$('#launch-question-paper1').click();
				$('#launch-question-paper').attr('disabled','disabled');	
				//window.open( 'launch.php', 'Test Paper', 'location=no,scrollbars=yes,status=no,toolbar=yes,resizable=yes' );
			}
			else
			{
				alert('Scheduled Test Timings: '+start+ ' to '+end);
			}
		});

	})
}
