$(function() {
	setTimeout(function() {$('.alert').hide(500);}, 3000);

	Profile.fetchData();
	Profile.setEventChangeProfilePicture();
	Profile.updateProfileData();
	Profile.updateStudentPassword();
	$('#profile-picture').on('change', function(event){
		//e.preventDefault();
		if($('#profile-picture').val() !="")
		{	var tmppath = URL.createObjectURL(event.target.files[0]);
			$("#edit-profile-image").attr('src',tmppath);
		}
	});

	$('#btn-cancel').click(function(e){
		e.preventDefault();
		$("#edit-profile-image").attr('src',$("#display-picture").attr('src'));
	})
});


var Profile = {};

Profile.fetchData = function() {
	var req = {};
	req.action = "fetchStudentProfileData";

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {

		res = $.parseJSON(res);
		//$(".profile-usertitle-name").text(res.data[0]['first_name'].caps()+' '+res.data[0]['last_name'].caps());
		if(res.data.length > 0)
		{	$(".profile-usertitle-name").text(res.data[0]['first_name']+' '+res.data[0]['last_name']);
			$("#email").val(res.data[0]["email"]);
			$("#first_name").val(res.data[0]['first_name'].caps());
			$("#last_name").val(res.data[0]['last_name'].caps());
			$("#mobile").val(res.data[0]['mobile']);
			$("#interests").val(res.data[0]['interests'].caps());
			// $("#occupation").val(res.data[0]['occupation'].caps());
			$("#about").val(res.data[0]['about'].caps());

			$("#address").val(res.data[0]['address']);

			$("#website_url").val(res.data[0]['website_url']);
			$("#display-picture").attr('src',res.data[0]['image']);
			$("#edit-profile-image").attr('src',res.data[0]['image']);


		}
	});
}

Profile.updateProfileData = function() {
	$('#student-details-form').off('submit');
	$('#student-details-form').on('submit', function(e) {
		e.preventDefault();
		var req = {};
		req.action = "updateProfile";

		req.first_name = $("#first_name").val();
		req.last_name = $("#last_name").val();
		req.mobile = $("#mobile").val();
		req.interests = $("#interests").val();
		// req.occupation = $("#occupation").val();
		req.about = $("#about").val();
		req.address = $("#address").val();
		req.website_url = $("#website_url").val();

		if($('#email') != undefined)
		{
			req.email = $("#email").val();
		}

		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			$(".profile-usertitle-name").text(req.first_name+' '+req.last_name);
			res = $.parseJSON(res);
			if(res.status == 1)
			{
				toastr.success(res.message);
			}
			else
			{
				toastr.error("Something went wrong");
			}
		});
	});
}


Profile.updateStudentPassword = function() {
	$('#change-password-form').off('submit');
	$('#change-password-form').on('submit', function(e) {
		e.preventDefault();
		if (validateChangePassword()) {
			var req ={};
			req.action = "changeProfilePassword";
			req.current_password = $("#current_password").val();
			req.new_password = $("#new_password").val();
			req.confirm_password = $("#confirm_password").val();

			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 2)
				{
					$('#change-password-form')[0].reset();
					toastr.error(res.data);
				}
				else if(res.status == 1)
				{
					toastr.success("Password successfully updated");
					$('#change-password-form')[0].reset();
				}
			});
		};
	})
}

function validateChangePassword() {
	unsetError($("#current_password"));
	unsetError(("#new_password"));
	unsetError($("#confirm_password"));

	current_password = $("#current_password").val();
	new_password = $("#new_password").val();
	confirm_password = $("#confirm_password").val();

	if (current_password.length < 6) {
		setError($("#current_password"), 'Password is too small.')
		return false;
	};

	if (new_password.length < 6) {
		setError($("#new_password"), 'Password is too small.')
		return false;
	};

	if (confirm_password != new_password) {
		setError($("#confirm_password"), 'Password not match.')
		return false;
	};


	return true;
}

Profile.setEventChangeProfilePicture = function() {
	$('form#form-change-profile-picture').ajaxForm({
        beforeSubmit: function (e) {

            ext = $("#profile-picture").val().split('.').pop().toLowerCase();

            if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                toastr.error('Unknown Image Type!', 'Error:');
                return false;
            }
		},beforeSend: function(arr, $form, data) {
            // console.log('starting');
        },
		uploadProgress: function(event, position, total, percentComplete) {
			//Progress bar
			$('#percentage').show();
			$('#percentage').html(percentComplete + '% done.') //update progressbar percent complete
        },
        success: function() {

        },
        complete: function(resp) {
        	$('#percentage').hide();
            res = $.parseJSON(resp.responseText);
                console.log(res)
            if(res.status == 1) {
                $('#display-picture').attr('src', res.src);
                $('.user-image').attr('src', res.src);
                toastr.success(res.message);

			}
			else
				alert('An error occured. Please refresh the page and try again. Error Details: ' + res.message);
        },
		error: function() {
			console.log('Error');
        }
    });
}