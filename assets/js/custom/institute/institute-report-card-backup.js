$(function() {
    Report.init();
    getTestPaperPerformanceReport.init();
})
    

var Report = {
    getPrograms : function() {
        var req = {};
        req.action = 'fetchInstitutePrograms';
        $.ajax({
            type: "post",
            url: EndPoint,
            data: JSON.stringify(req)  
        }).done(function(res) {
            res = $.parseJSON(res);
            if (res.status == 1) 
            {
                Report.fillPrograms(res.programs, $('#select-program-test-performance'));
                Report.fillPrograms(res.programs, $('#select-program-class-performance'));
            } 
            else
            {
                toastr.error(res.message);
            }
        });
    },
    fillPrograms : function(programs, target) {
        var html = '<option value="0"> Select Program </option>';
        $.each(programs, function(i, program) {
            html += '<option value="'+program.id+'" >'+program.name+'</option>';
        });
        $(target).html(html);
    },
    onProgramChange : function() {
        $('#select-program-test-performance').off('change');
        $('#select-program-test-performance').on('change', function (e) {
            e.preventDefault();
            var value = $(this).val();
            $('#select-test-paper').html('');
            Report.fetchTests(value);
        });
    },
    fetchTests : function(program_id) {
        if (program_id != 0) {
            var req = {};
            req.action = "fetchTestPapersOfPragram";
            req.program_id = program_id;

            $.ajax({
                'type'  :   'post',
                'url'   :   EndPoint,
                'data'  :   JSON.stringify(req)
            }).done(function(res) {
                res = $.parseJSON(res);
                if(res.status == 1)
                {
                    Report.fillTests(res.test_papers, $('#select-test-paper'));
                }
            });
        };
    },
    fillTests : function(papers, target) {
        var html = '<option value="0"> Select Test Paper </option>';
        $.each(papers, function(i, paper) {
            html += '<option value="'+paper.id+'" >'+paper.name+'</option>';
        });
        $(target).html(html);
    },
    getTestPaperPerformanceReport : function() {
        $('#test-performance-report-form').off('submit');
        $('#test-performance-report-form').on('submit', function(e) {
            e.preventDefault();
            var program_id = $('#select-program-test-performance').val();
            var paper_id = $('#select-test-paper').val();
            if (program_id != 0 && paper_id != 0) {
                var request = {};
                request.action = "getTestPaperPerformanceReport";
                request.program_id = program_id; 
                request.paper_id = paper_id; 
                $.ajax({
                    type: "post",
                    url: EndPoint,
                   data: JSON.stringify(request)  
                }).done(function(response) {
                    response = $.parseJSON(response);
                    if (response.status == 1) {
                        toastr.success(response.message);
                        Report.fillTestPaperPerformanceReport(response);
                    }
                    else {
                        toastr.error(response.message);
                    };
                });
            }
            else {
                toastr.error("Please select program and paper first.")
            };
        });
    },
    fillTestPaperPerformanceReport : function(report) {
        var html = '';
        $.each(report.section_report, function(i, section_report) {
            html += '<tr>'
                html += '<td> '+ section_report.section_name +' </td>'
                html += '<td> '+ section_report.avg +' </td>'
                html += '<td> '+ section_report.max +' </td>'
                html += '<td> '+ section_report.min +' </td>'
            html += '</tr>'
        })
        $("#section-wise-report-table tbody").html(html);

        var html = '';
        $.each(report.question_report, function(i, question_report) {
            html += '<tr>'
                html += '<td>Q '+ (i+1) +' </td>'
                // html += '<td> '+ question_report.skill +' </td>'
                html += '<td> '+ question_report.attempted +' </td>'
                html += '<td> '+ question_report.correct +' </td>'
                html += '<td> '+ (question_report.attempted - question_report.correct) +' </td>'
                html += '<td> '+ (question_report.launched - question_report.attempted) +' </td>'
                // html += '<td> '+ ((question_report.correct == 0)? "NAN":question_report.correct_time_taken/question_report.correct) +' </td>'
                // html += '<td> '+ (((question_report.launched - question_report.attempted) == 0)? "NAN": question_report.incorrect_time_taken/(question_report.launched - question_report.attempted)) +' </td>'
            html += '</tr>'
        })
        $("#question-wise-report-table tbody").html(html);
        
        var html = '';
        $.each(report.skill_report, function(i, skill_report) {
            html += '<tr>';
                html += '<td> '+ skill_report.skill +' </td>';
                html += '<td> '+ skill_report.total_question +' </td>';
                html += '<td> '+ skill_report.total_marks +' </td>';
                html += '<td> '+ skill_report.max +' </td>';
                html += '<td> '+ skill_report.min +' </td>';
                html += '<td> '+ skill_report.avg +' </td>';
            html += '</tr>';
        });
        $("#skill-wise-report-table tbody").html(html);
        

        var html = '<tr><th></th>';
        var thead_html = '<tr><th></th>';
        var td = '';
        $.each(report.student_performance_papers, function(i, papers) {
            td += '<td data-paper-id="'+ papers.test_paper_id +'">NA</td>';
            thead_html += '<th data-id="'+ papers.test_paper_id +'"> '+ papers.name +' </th>';
            html += '<th>';
                html += ' <a>Highest:</a>'+ papers.max +' <br>';
                html += '<a>Lowest:</a>'+ papers.min +' <br>';
                html += '<a>Average:</a>'+ papers.avg;
            html += ' </th>';
            $.each(report.student_performance_students, function(i, student) {
                // $("#class-perfomance-report-table tbody").html('<tr> <td> '+ student.student_name +' </td> </tr>');
                var tbody_html = '';
            });
        });
        thead_html += '</tr>'
        html += '</tr>';
        $("#class-perfomance-report-table thead").html(thead_html+html);;

        $.each(report.student_performance_students, function(i, student) {
            if ($('#class-perfomance-report-table tbody tr[data-id="'+ student.student_id +'"]').length == 0) {
                var html = '';
                html += '<tr data-id="'+ student.student_id +'"> <td> '+ student.student_name +' </td>'+td+' </tr>';
                $("#class-perfomance-report-table tbody").append(html);
            }
            $('#class-perfomance-report-table tbody tr[data-id="'+ student.student_id +'"] td[data-paper-id="'+student.test_paper_id+'"]').text(student.marks_archived);
            // eq = $('#class-perfomance-report-table thead tr:eq(0) data-id="'+ student.test_paper_id +'"').index();
            // $(tr +' td:eq('+eq+')').text(student.marks_archived);
        });
    },

    init : function() {
        Report.getPrograms();
        Report.onProgramChange();
        Report.getPrograms();
        Report.getTestPaperPerformanceReport();
    }
}