$(function() {
    Report.init();
})

var Report = {
     getPrograms : function() {
        var req = {};
        req.action = 'fetchInstitutePrograms';
        $.ajax({
            type: "post",
            url: EndPoint,
            data: JSON.stringify(req)  
        }).done(function(res) {
            res = $.parseJSON(res);
            if (res.status == 1) 
            {
                Report.fillPrograms(res.programs, $('#select-program-test-performance'));
                Report.fillPrograms(res.programs, $('#select-program-class-performance'));
            } 
            else
            {
                toastr.error(res.message);
            }
        });
    },
    fillPrograms : function(programs, target) {
        var html = '<option value="0"> Select Test Program </option>';
        $.each(programs, function(i, program) {
            html += '<option value="'+program.id+'" >'+program.name+'</option>';
        });
        $(target).html(html);
    },
    getTestPaperPerformanceReport : function() {
        $('#test-performance-report-form').off('submit');
        $('#test-performance-report-form').on('submit', function(e) {
            e.preventDefault();
            var program_id = $('#select-program-test-performance').val();
            var paper_id = $('#select-test-paper').val();
            if (program_id != 0) {
                var request = {};
                request.action = "getTestPaperPerformanceReport";
                request.program_id = program_id; 
                request.paper_id = paper_id; 
                $.ajax({
                    type: "post",
                    url: EndPoint,
                   data: JSON.stringify(request)  
                }).done(function(response) {
                    response = $.parseJSON(response);
                    if (response.status == 1) {
                        toastr.success(response.message);
                        Report.fillTestPaperPerformanceReport(response);
                    }
                    else {
                        toastr.error(response.message);
                    };
                });
            }
            else {
                toastr.error("Please select Test Program.")
            };
        });
    },
    fillTestPaperPerformanceReport : function(report) {
        $('#class-perfomance-report-table').parents('div:eq(4)').show()

        var html = '<tr><th></th>';
        var thead_html = '<tr><th></th>';
        var td = '';
        $.each(report.student_performance_papers, function(i, papers) {
            td += '<td data-paper-id="'+ papers.test_paper_id +'">NA</td>';
            thead_html += '<th data-id="'+ papers.test_paper_id +'"> '+ papers.name +' </th>';
            html += '<th>';
                html += ' <a>Highest:</a>'+ papers.max +' <br>';
                html += '<a>Lowest:</a>'+ papers.min +' <br>';
                html += '<a>Average:</a>'+ papers.avg;
            html += ' </th>';
            $.each(report.student_performance_students, function(i, student) {
                // $("#class-perfomance-report-table tbody").html('<tr> <td> '+ student.student_name +' </td> </tr>');
                var tbody_html = '';
            });
        });
        thead_html += '</tr>'
        html += '</tr>';
        $("#class-perfomance-report-table thead").html(thead_html+html);;

        $.each(report.student_performance_students, function(i, student) {
            if ($('#class-perfomance-report-table tbody tr[data-id="'+ student.student_id +'"]').length == 0) {
                var html = '';
                html += '<tr data-id="'+ student.student_id +'"> <td> '+ student.student_name +' </td>'+td+' </tr>';
                $("#class-perfomance-report-table tbody").append(html);
            }
            $('#class-perfomance-report-table tbody tr[data-id="'+ student.student_id +'"] td[data-paper-id="'+student.test_paper_id+'"]').text(student.marks_archived);
            // eq = $('#class-perfomance-report-table thead tr:eq(0) data-id="'+ student.test_paper_id +'"').index();
            // $(tr +' td:eq('+eq+')').text(student.marks_archived);
        });
    },
    init : function() {
        Report.getPrograms();
        Report.getTestPaperPerformanceReport();
    }
}