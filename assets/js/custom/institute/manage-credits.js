$(function() {
	Students.fetch();
})

var Students = {};

Students.fetch = function() {
	var req = {};
	req.action = 'fetchNotifications';
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res) {

		res = $.parseJSON(res);

		if (res.status == 1) {
			Students.fill(res.notifications);
			$('#credits-info').html(`<h5><strong>Available credits: ${res.user[0].credits}</strong></h5>`);
		} else {
			toastr.error(res.message);
			$('#students-table tbody').html('<tr><td colspan="3">No Messages Found.</td></tr>')
		};

	});
}

Students.fill = function(students) {
	var html = ''

	$.each(students, function(i, student) {
		html += '<tr data-id='+student.id+'>';
		html += '<td>'+(i+1)+'</td>';
		html += '<td>'+student.message+'</td>';
		html += '<td>'+student.created_at+'</td>';
		html += '</tr>';
	});

    $('#students-table tbody').html(html);

    if (typeof oTable != "undefined") {
        oTable.fnDestroy();
    }
    // $('#students-table tbody').html(html);
    oTable = $('#students-table').dataTable({
        "columnDefs": [{ // set default column settings
            'orderable': false,
            'targets': 'no-sort'
        }]
    });

}
