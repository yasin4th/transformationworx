$(function() {
	fetchAssistant();
	addAssistant();
	deleteAssistantDetails();
	updateAssistantDetails();
})

function validateEmail(sEmail)
{
	var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(filter.test(sEmail))
	{
		return true;
	}
	else
	{
		return false;
	}
}

fetchAssistant = function() {
	var req = {};
	req.action = 'fetchInstituteAssistant';
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);

		if (res.status == 1) {
			var html = ''
	     	$.each(res.assistants, function(i, assistant) {
				html += '<tr data-id='+assistant.student_id+'>';
				html += '<td>'+(i+1)+'</td>';
				html += '<td>'+assistant.first_name+' '+assistant.last_name+'</td>';
				html += '<td>'+assistant.email+'</td>';
				html += '<td>'+assistant.mobile+'</td>';
				html += '<td> <a href="#edit-assistant" class="edit" title="Edit" data-toggle="modal"> <i class="fa fa-pencil"></i></a> | <a id="assistant-delete" title="Delete" data-id='+assistant.student_id+' ><i class="fa fa-trash-o"></i></a></td>';
				html += '</tr>';
			});
			$('#assistant-table tbody').html(html);
			editassistant();
			deleteAssistantDetails();
	    }
		else{
			toastr.error(res.message);
		}
	});
}

validateAssistant = function() {
	// var pass = $('#password').val();
	// var confirm = $('#confirm-password').val();
	var regx = /^[0-9]*$/;
	var regy = /^[a-zA-Z]*$/;


	if($('#fname').val() == 0)
	{
		toastr.error("Please enter firstname.");
		return false;
	}

	if(!regy.test($('#fname').val()))
	{
		toastr.error("Please enter first name in alphabetical.");
		return false;
	}

	if($('#fname').val() != "" && $('#fname').val().length < 3) {
		toastr.error("Please enter alteast 3 characters in firstname.");
		return false;
	}

	if($('#lname').val() != "" && $('#lname').val().length < 3) {
		toastr.error("Please enter alteast 3 characters in lastname.");
		return false;
	}

	if($('#lname').val() != "" && !regy.test($('#lname').val())) {
		toastr.error("Please enter last name in alphabetical");
		return false;
	}

	if($('#assistant_email').val() == 0)
	{	toastr.error("Please enter email.");
		return false;
	}
	if(!validateEmail($('#assistant_email').val())) {
			toastr.error('Please enter valid email.');
			return false;
	}

	if($('#assistant_phone').val() == 0)
	{
		toastr.error("Please enter mobile no..");
		return false;
	}

	if(!regx.test($('#assistant_phone').val()))
	{
		toastr.error("Please enter valid mobile no.");
		return false;
	}

	if($('#assistant_phone').val() != "" && $('#assistant_phone').val().length<10){
		toastr.error("Please enter valid mobile no.");
		return false;
	}

	if($('#assistant_phone').val() != "" && $('#assistant_phone').val().length>11){
		toastr.error("Please enter valid mobile no.");
		return false;
	}

	return true;
}

addAssistant = function(){
	$('#add-assistant-form').on('submit', function(e) {
		e.preventDefault();

		if(validateAssistant()) {
			var req = {};
			req.action = "add-assistant";
			req.fname = $('#fname').val();
			req.lname = $('#lname').val();
			req.email = $('#assistant_email').val();
			req.mobile = $('#assistant_phone').val();
			req.role ="6";

			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);

				if(res.status == 1)
				{
					$('#add-assistant-form')[0].reset();
					toastr.success("Assistant Successfully added. ");
					fetchAssistant();
				}
				else
				{
					toastr.error(res.message);
				}
			});
		}
	});
}

editassistant = function(){
	$('.edit').off('click');
	$('.edit').on('click',function(e) {

		e.preventDefault();
		var req = {};
		assistantid = $(this).parents('tr').data('id');
		req.action = "fetchInstituteAssistant";
		req.assistid = assistantid;
		$.ajax({
			'type' : 'post',
			'url'  :  EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);

			if(res.status ==1) {
				$('#edit-assistant').attr('data-id',assistantid);
				$('#assistant-edit-form').data('id',assistantid);
				$('#assistant-edit-form .fname').val(res.assists.first_name);
				$('#assistant-edit-form .lname').val(res.assists.last_name);
				$('#assistant-edit-form .email1').val(res.assists.email);
				$('#assistant-edit-form .phone').val(res.assists.mobile);
				updateAssistantDetails();

			}else{
				toastr.error(res.message, "Error:");
			}
		})

	})
}

updateAssistantDetails = function() {
	$("form#assistant-edit-form").off('submit');
	$("form#assistant-edit-form").on('submit', function(e) {
		e.preventDefault();
		if(validate()){
			var req = {};
			req.action = "updateAssistantDetails";
			req.id = $("#edit-assistant").data('id');
			req.fname = $('.fname').val();
			req.lname = $('.lname').val();
			req.email = $('.email1').val();
			req.mobile = $('.phone').val();
			req.password = $('input[name=password]').val();
			req.role ="6";
			$.ajax({
				type: "post",
				url: EndPoint,
				data: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				// console.log(res);
				if (res.status == 1) {
					toastr.success(res.message);
					$("#edit-assistant").modal('hide');
					$('form#assistant-edit-form')[0].reset();
					fetchAssistant();
				} else{
					toastr.error(res.message, "Error:");
				};
			});
		}
	})
}

validate = function() {
	var pass = $('input[name=password]').val();
	var confirm = $('#confirm-password').val();
	if($('input[name=password]').val()!=="")
	{
		if($('#password').val().length<6){
			toastr.error("please enter atleast six characters long password");
			return false;
		}
		if(confirm != pass){
			toastr.error("Confirm password incorrect.");
			return false;
		}
	}
	return true;
}


deleteAssistantDetails = function() {
	$('#assistant-delete').off('click');
	$('#assistant-delete').on('click', function(e) {
		e.preventDefault();
		id = $(this).data('id');
		if(confirm('Are you sure want to delete Assistant')) {
			var req = {};
			req.action = 'DeleteAssistantDetails';
			req.id = id;
			req.deleted = 1;
			$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 1){
					toastr.success(res.message);
					fetchAssistant();
				}
				else {
					toastr.error(res.message,'Error:');
				}
			});
		}
	});
}