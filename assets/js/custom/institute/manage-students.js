$(function() {
	Students.fetch();
	// getTestprograms()
	Programs.fetch();
	addStudent();
	setEventFilterStudent();
	assignProgram();
	deleteAssignStudent();
	editStudent();
	updateStudentDetails();
	viewProgram();
	updateStudentProgram();

})

var Students = {};

Students.fetch = function() {
	var req = {};
	req.action = 'fetchInstituteStudents';
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 1) {
			Students.fill(res.students, res.ins_lab_type);
		} else {
			toastr.error(res.message);
			$('#students-table tbody').html('<tr><td colspan="6">No students found.</td></tr>')
		};

	});
}

Students.fill = function(students, ins_lab_type) {
	var html = ''

	$.each(students, function(i, student) {
		html += '<tr data-id='+student.student_id+' data-ins_lab_type='+ins_lab_type+'>';
		html += '<td>'+(i+1)+'</td>';
		html += '<td>'+student.first_name+' '+student.last_name+'</td>';
		html += '<td>'+student.email+'</td>';
		html += '<td>'+student.mobile+'</td>';
		html += '<td> <div><a href="#view-program" class="view-program" title="View Program" data-toggle="modal"  data-id='+student.student_id+' ><i class="fa fa-eye"></i></a> | <a href="#assign-program" class="assign-program" title="Assign Program" data-toggle="modal"  data-id='+student.student_id+' ><i class="fa fa-plus-circle"></i></a> | <a href="#edit-assign-student" data-toggle="modal" title="Edit"  data-id='+student.student_id+' class="edit-student"><i class="fa fa-pencil"></i> </a> | ';
		if (student.deleted == 0) {
			html += '<a class="student_delete"  data-id='+student.student_id+' title="Deactivate"><i class="fa fa-times-circle"></i></a>';
		} else {
			html += '<a class="student_delete"  data-id='+student.student_id+' title="Activate"><i class="fa fa-check-square-o"></i></a> '
		};
		html += '</tr>';
	});

    $('#students-table tbody').html(html);

    if (typeof oTable != "undefined") {
        oTable.fnDestroy();
    }
    // $('#students-table tbody').html(html);
    oTable = $('#students-table').dataTable(
        {
        "fnDrawCallback": function( oSettings ) {
            setEvents(); // or fnInfoCallback
            viewProgram();
            // setEvents();
            deleteAssignStudent();
            editStudent();
        },
        "columnDefs": [{ // set default column settings
            'orderable': false,
            'targets': 'no-sort'
        }]
    });

	// viewProgram();
	// setEvents();
	// deleteAssignStudent();
	// editStudent();
}


Students.delete = function() {
	var req = {};
	req.action = 'deleteInstituteStudent';
	$.ajax({
		type: "post",
		url: Endpoint,
		data: JSON.stingify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 1) {
			Students.fetch();
		} else{
			toastr.error(res.message);
		};
	});
}

// function getTestprograms() {
// 	var req = {};
// 	req.action = "getTestprograms";
// 	$.ajax({
// 		'type'	:	'post',
// 		'url'	:	EndPoint,
// 		'data'	:	JSON.stringify(req)
// 	}).done(function(res) {
// 		res = $.parseJSON(res);
// 		if(res.status == 1){
// 			fillTestprograms(res.data);
// 		}
// 		else if(res.status == 0) {
// 			toastr.error(res.message);
// 		}
// 	});
// }

// function fillTestprograms(programs) {
// 	var html = '<option value="0"> Select Program </option>';

// 	$.each(programs, function(i, program) {
// 		html += '<option value="'+program.id+'" >'+program.name+'</option>';
// 	});

// 	$('#test-programs').html(html);
// 	$('.test-programs').html(html);
// }

var Programs = {};
Programs.fetch = function() {
	var req = {};
	req.action = 'fetchInstitutePrograms';
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 1) {
			Programs.fill(res.programs);
		} else{
			toastr.error(res.message);
		};

	});
}

Programs.fill = function(programs) {

	var html = '<option value="0"> Select Program </option>';

	$.each(programs, function(i, program) {
		html += `<option value="${program.id}" credits="${program.credits}">${program.name}</option>`;
	});

	$('#test-programs').html(html);
	$('.test-programs').html(html);
	$('.test-programs').on('change', function(e) {
		val = $(this).val();
		console.log(val);
		credits = $(`select.test-programs option[value=${val}]`).attr('credits');
		$('#credits-info').html(`Note: ${credits} credits will be deducted.`);
	})
}

function setEventFilterStudent() {
	$('#filter-form').off('submit');
	$('#filter-form').on('submit', function(e) {
		e.preventDefault();
		var req = {};
		req.action = "fetchInstituteStudents";
		if ($('.test-programs').val() != "0") {
			req.program = $('.test-programs').val();
		};

		if ($('.studentemail').val() != null) {
			req.email = $('.studentemail').val();
		};

		if ($('.studentphone').val() != "0") {
			req.phone = $('.studentphone').val();
		};

		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == 1)
				Students.fill(res.students);
			else if(res.status == 0)
				toastr.error(res.message);
		});

		$('#filter-student').modal('hide');

	});
}

function validateEmail(sEmail)
{
	var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(filter.test(sEmail))
	{
		return true;
	}
	else
	{
		return false;
	}
}

validate = function() {
	// var pass = $('#password').val();
	// var confirm = $('#confirm-password').val();
	var regx = /^[0-9]*$/;
	var regy = /^[a-zA-Z]*$/;

	if($('.fname').val() == "")
	{
		toastr.error("Please enter firstname.");
		return false;
	}

	// if(!regy.test($('.fname').val()))
	// {
	// 	toastr.error("Please enter first name in alphabetical.");
	// 	return false;
	// }

	if($('.fname').val() != "" && $('.fname').val().length<3){
		toastr.error("Please enter alteast 3 characters in firstname.");
		return false;
	}

	if($('.student_email').val() == '')
	{	toastr.error("Please enter email.");
		return false;
	}
	if(!validateEmail($('.student_email').val())){
			toastr.error('Please enter valid email.');
			return false;
	}

	if($('.student_mobile').val() == '')
	{
		toastr.error("Please enter mobile no..");
		return false;
	}

	if(!regx.test($('.student_mobile').val()))
	{
		toastr.error("Please enter valid mobile no.");
		return false;
	}

	if($('.student_mobile').val() != "" && $('.student_mobile').val().length<10){
		toastr.error("Please enter valid mobile no.");
		return false;
	}

	if($('.student_mobile').val() != "" && $('.student_mobile').val().length>11){
		toastr.error("Please enter valid mobile no.");
		return false;
	}

	return true;
}

addStudent = function(){
	$('#add-student-form').on('submit', function(e) {
		e.preventDefault();
		if(validate()){
			var req = {};
			req.action = "add-student";
			req.fname = $('.fname').val();
			req.lname = $('.lname').val();
			req.email = $('.student_email').val();
			req.mobile = $('.student_mobile').val();
			/*req.testprograms = $('#test-programs option:selected').val();
			req.totalpaper = $('.totalpaper').val();
			req.lab = $('.inlab').val();
			req.expire_date = $('.expire_date').val();*/
			req.role ="4";

			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);

				if(res.status == 1)
				{	$('#add-student').modal('hide');
					$('#add-student-form')[0].reset();
					toastr.success("Student Successfully added. ");
					Students.fetch();
				}
				else
				{
					toastr.error(res.message);
				}
			});
		}
	});
}

pr_validate = function() {

	if($('[name="assignProgram"]').val() == 0){
		toastr.error("please select test program.");
		return false;
	}

	if($('[name="expire_date"]').val() == 0)
	{
		toastr.error("please enter expire date.");
		return false;
	}

	if(parseInt($('.assigninlab').val()) > parseInt($('.assigntotalpaper').val())) {
		toastr.error("lab value not greater than total no of paper ");
		return false;
	}
	return true;
}

setEvents = function()
{
    $('.assign-program').off('click');
	$('.assign-program').click(function(e){
		student_id = $(this).attr('data-id');
		$('#assign-program-form').attr('data-id', student_id);
	});
}

assignProgram = function(){
	$('#assign-program-form').off('submit');
	$('#assign-program-form').on('submit', function(e) {
		e.preventDefault();
		var req = {};
		studentid = $(this).attr('data-id');
		req.action = "assignProgramStudent";
		req.studentid = studentid;
		req.program = $('[name=assignProgram]').val();
		req.assigntotalpaper = $('.assigntotalpaper').val();
		req.assigninlab = $('.assigninlab').val();
		req.expire_date = $('input[name=expire_date]').val();

		if(pr_validate()){
			$.ajax({
				'type' : 'post',
				'url'  :  EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res){
				res = $.parseJSON(res);

				if(res.status ==1){
					$('#assign-program').modal('hide');
					$('#assign-program-form')[0].reset();
					toastr.success(res.message);
					Students.fetch();
				}else{
					toastr.error(res.message, "Error:");
				}
			});
		}

	})
}

deleteAssignStudent = function(){
	$('.student_delete').off('click');
	$('.student_delete').on('click',function(e){
		e.preventDefault();
			var req = {};
			req.user_id = $(this).data('id');
			req.action = "deletestudent";
			req.changed_status = 0;
			if ($(this).find('i').hasClass('fa-times-circle')) {
				req.changed_status = 1;
			};
			$.ajax({
				'type' : 'post',
				'url'  :  EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res){
				res = $.parseJSON(res);
				if (res.status == 1) {
					Students.fetch();
                    toastr.success(res.message);
				} else {
					toastr.error(res.message, "Error:");
				}
			});
	})
}

editStudent = function() {
	$('.edit-student').off('click');
	$('.edit-student').on('click',function(e){
		e.preventDefault();
		var req = {};
		userId =  $(this).parents('tr').data('id');
		req.action = "fetchInstituteStudents";
		req.studentId = userId;
		$.ajax({
			'type' : 'post',
			'url'  :  EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status ==1) {
				$('#edit-student-form').attr('data-id',userId);
				$('[name="student_fname"]').val(res.student.first_name);
				$('[name="student_lname"]').val(res.student.last_name);
				$('[name="student_email"]').val(res.student.email);
				$("#stu_mobile").val(res.student.mobile);
			} else {
				toastr.error(res.message, "Error:");
			}
		})
	})
}

updateStudentDetails = function() {
	$("form#edit-student-form").off('submit');
	$("form#edit-student-form").on('submit', function(e) {
		e.preventDefault();
		if (validate_pass()) {
			var req = {};
            req.action   = "updateStudentDetails";
            req.id       = $("#edit-student-form").attr('data-id');
            req.fname    = $('[name="student_fname"]').val();
            req.lname    = $('[name="student_lname"]').val();
            req.mobile   = $("#stu_mobile").val();
            req.password = $('#password').val();

            $.ajax({
				type: "post",
				url: EndPoint,
				data: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if (res.status == 1) {
					toastr.success(res.message);
					$("#edit-assign-student").modal('hide');
					$('form#edit-student-form')[0].reset();
					Students.fetch();
				} else{
					toastr.error(res.message, "Error:");
				};
			});
		}
	})
}

validate_pass = function() {
	var pass = $('#password').val();
	var confirm = $('#confirm-password').val();
	if($('#password').val()!=="")
	{
		if($('#password').val().length<6) {
			toastr.error("please enter atleast six characters long password");
			return false;
		}
		if(confirm != pass) {
			toastr.error("Confirm password incorrect.");
			return false;
		}
	}
	return true;
}

viewProgram = function() {
	$('.view-program').off('click');
	$('.view-program').on('click', function(e) {
		var req = {};
		req.user_Id =  $(this).parents('tr').data('id');
		req.ins_lab_type =  $(this).parents('tr').data('ins_lab_type');
		req.action = 'fetchStudentsProgram';
		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if (res.status == 1) {
				viewProgram.fill(res.programs, res.ins_lab_type);
			} else{
				$('#program-table tbody').html('');
        			editViewProgram();
				toastr.error(res.message);
			};

		});
	})
}

viewProgram.fill = function(programs, ins_lab_type) {
	var html = ''

	$.each(programs, function(i, program) {
        total = parseInt(program.total_paper) - parseInt(program.total_attempts);
        lab = ( parseInt(program.total_lab) - parseInt(program.total_lab_attempts) );
        total = (total < 0) ? 0 : total;
        lab = (lab < 0) ? 0 : lab;
		html += '<tr data-id='+program.id+' data-program_id='+program.program_id+'>';
		html += '<td>'+(i+1)+'</td>';
		html += '<td>'+program.prname+'</td>';
		html += '<td>' + total + '</td>';
		if(ins_lab_type == 1){
			html += '<td>' + lab + '</td>';
		}
		html += '<td>'+program.vaild.substr(0,10)+'</td>';
		html += '<td> <div><a href="#edit-student-program" data-toggle="modal" title="Edit"  data-id='+program.program_id+' class="edit-program" ><i class="fa fa-pencil"></i></a>';
		html += '</tr>';
	});

	$('#program-table tbody').html(html);
	editViewProgram();
	// deleteProgram();
}

editViewProgram = function(){
	$('.edit-program').off('click');
	$('.edit-program').on('click',function(e){
	$('#view-program').modal('hide');
		e.preventDefault();
		var req = {};
		req.action = "fetchTestProgram";
		req.user_Id =  $(this).parents('tr').data('id');
		req.program_id = $(this).parents('tr').data('program_id');
		$.ajax({
			'type' : 'post',
			'url'  :  EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status ==1){
				$('#edit-student-program').attr('data-id',req.user_Id);
				$('#edit-student-program').attr('data-program_id',req.program_id);
				$('.program_totalpaper').val(parseInt(res.programs.total_paper) - parseInt(res.programs.total_attempts));
				$('.program_inlab').val(parseInt(res.programs.total_lab) - parseInt(res.programs.total_lab_attempts) );
				$('#edit-programs').val(req.program_id);
				$('.program_expire_date').val(res.programs.vaild);
			}else{
				toastr.error(res.message, "Error:");
			}
		})

	})
}

// deleteProgram = function(){
// 	$('.delete-program').off('click');
// 	$('.delete-program').on('click',function(e){
// 		e.preventDefault();
// 		var req = {};
// 		req.action = "deleteTestProgram";
// 		req.user_Id =  $(this).parents('tr').data('id');
// 		req.program_id = $(this).parents('tr').data('program_id');
// 		$.ajax({
// 			'type' : 'post',
// 			'url'  :  EndPoint,
// 			'data'	:	JSON.stringify(req)
// 		}).done(function(res){
// 			res = $.parseJSON(res);
// 			if(res.status ==1){
// 				viewProgram();
// 			}else{
// 				toastr.error(res.message, "Error:");
// 			}
// 		})

// 	})
// }

updateStudentProgram = function() {
	$("form#edit-student-program").off('submit');
	$("form#edit-student-program").on('submit', function(e) {
		e.preventDefault();
		if(validateProgram())
		{
			var req = {};
			req.action = "updateStudentProgram";
			req.id = $("#edit-student-program").attr('data-id');
			req.program_id = $("#edit-student-program").attr('data-program_id');
			req.totalpaper = $('.program_totalpaper').val();
			req.inlab = $(".program_inlab").val();
			req.expire_date = $('.program_expire_date').val();
			$.ajax({
				type: "post",
				url: EndPoint,
				data: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if (res.status == 1) {
					toastr.success(res.message);
					$("#edit-student-program").modal('hide');
					$('form#edit-student-program')[0].reset();
					Students.fetch();
				} else{
					toastr.error(res.message, "Error:");
				};
			});



		}
	})
}

validateProgram = function(){
	if(parseInt($('.program_inlab').val()) > parseInt($('.program_totalpaper').val())) {
			toastr.error("lab value not greater than total no of paper ");
			return false;
		}
		return true;
}
