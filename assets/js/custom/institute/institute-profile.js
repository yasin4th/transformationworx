$(function() {
	Institute.loadProfile();
	Institute.updateProfile();
	Institute.changePassword();
	Institute.updateAvtar();
})

var Institute  = {};

Institute.loadProfile = function() {
	var req = {};
	req.action = 'fetchInstituteProfile';
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 1) {
			Institute.fillProfile(res.user);
		} else{
			toastr.error(res.message);
		};
	});
}


Institute.fillProfile = function(institute) {
	$(".name").text((institute.first_name+' '+institute.last_name).caps());
	$(".email").text(institute.email);
	$("#mobile").text(institute.mobile);
	$("#about").val(institute.about);
	$("#url").val(institute.website_url);
	$("#address").val(institute.address);

	$("#display-picture").attr('src', institute.image);
}


Institute.updateProfile = function() {
	$('#update-info-form').off('submit');
	$('#update-info-form').on('submit', function(e) {
		e.preventDefault();
		if (Institute.validateProfile()) {
			var req = {};
			req.action = 'updateInstituteProfile';

			req.address = $("#address").val();
			req.about = $("#about").val();
			req.url = $("#url").val();

			$.ajax({
				type: "post",
				url: EndPoint,
				data: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				// console.log(res);
				if (res.status == 1) {
					toastr.success(res.message);
				} else{
					toastr.error(res.message);
				};

			});
		};
	});
}


Institute.validateProfile = function() {
	address = $("#address").val();
	about = $("#about").val();
	url = $("#url").val();
	check = true;
	unsetError($("#about"));

	if (about == '' || about == ' ') {
		setError($("#about"), 'About can`t blank');
		check = false;
	};


	return check;
}



Institute.validateImage = function() {
	check = true;
	if ($('#select-avtar')[0].value == '') {
		check = false;
	};
	toastr.error('Please Select Image');
	return check;
}


Institute.changePassword = function() {
	$('#chage-password-form').off('submit');
	$('#chage-password-form').on('submit', function(e) {
		e.preventDefault();
		if (Institute.validatePassword()) {
			var req = {};
			req.current_password = $('#current-password').val();
			req.new_password = $('#password').val();
			req.confirm_password =  $('#confirm-password').val();

			req.action = 'updateInstitutePassword';

			$.ajax({
				type: "post",
				url: EndPoint,
				data: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				// console.log(res);
				if (res.status == 1) {
					toastr.success(res.message);
					$('#chage-password-form')[0].reset();
				} else if(res.status == 2) {
					toastr.info(res.message);
				} else {
					toastr.error(res.message);
				};
			});
		}
	});
}

Institute.validatePassword = function() {
	current_password =  $('#current-password').val();
	new_password =  $('#password').val();
	confirm_password =  $('#confirm-password').val();
	check = true;

	unsetError($('#current-password'));
	unsetError($('#password'));
	unsetError($('#confirm-password'));

	if (current_password == '') {
		setError($('#current-password'), 'Please enter current password.')
		check = false;
	};
	if (new_password.length > 5) {
		if (new_password != confirm_password) {
			setError($('#confirm-password'), 'Password not match')
			check = false;
		};

	} else{
		check = false;
		setError($('#password'), 'Password must contain 5 charaters.');
	};

	return check;
}


Institute.updateAvtar = function() {
	$('form#form-change-profile-picture').ajaxForm({
        beforeSubmit: function (e) {

            ext = $('#select-avtar').val().split('.').pop().toLowerCase();

            if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                toastr.error('Unknown Image Type!', 'Error:');
                return false;
            }
		},beforeSend: function(arr, $form, data) {
            // console.log('starting');
        },
		uploadProgress: function(event, position, total, percentComplete) {
			//Progress bar
			$('#percentage').show();
			$('#percentage').html(percentComplete + '% done.') //update progressbar percent complete
        },
        success: function() {

        },
        complete: function(resp) {
        	$('#percentage').hide();
            res = $.parseJSON(resp.responseText);
            // console.log(res)
            if(res.status == 1) {
                $('#display-picture').attr('src', res.src);
                toastr.success(res.message);
			}
			else
				toastr.error('An error occured. Please refresh the page and try again. Error Details: ' + res.message);
        },
		error: function() {
			console.log('Error');
        }
    });
}