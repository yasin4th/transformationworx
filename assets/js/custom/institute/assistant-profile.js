var limit = 0;
var page_no = 1;
var filter = false;
$(function() {
	Students.fetch();
	updateAssistantPassword();
})

var Students = {};

Students.fetch = function() {
	var req = {};
	req.action = 'fetchInstituteStudents';
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 1) {
			Students.fill(res.students);
		} else{
			toastr.error(res.message);
		};

	});
}

Students.fill = function(students) {
	var html = '';
	count = 0;

	$.each(students, function(i, student) {
		html += '<tr data-id='+student.student_id+' data-totallab='+student.totallab+'>';
		html += '<td>'+(i+1)+'</td>';
		html += '<td>'+student.first_name+' '+student.last_name+'</td>';
		html += '<td>'+student.email+'</td>';
		html += '<td>'+student.mobile+'</td>';
		html += '<td> <div> <label  class="student-login btn green" style=" padding:0px 8px;"> <i class="fa fa-user"></i>  Login </label > </div> </td>';

		html += '</tr>';
	});

	if (typeof oTable != "undefined") {
		oTable.fnDestroy();
	}
	$('#students-table tbody').html(html);
	oTable = $('#students-table').dataTable({
		"fnDrawCallback": function( oSettings ) {
	      studentLogin(); // or fnInfoCallback
	    },
        "columnDefs": [{ // set default column settings
            'orderable': false,
            'targets': 'no-sort'
        }]

    });
}

studentLogin = function(){
	$('.student-login').off('click');
	$('.student-login').on('click', function(e) {
			e.preventDefault();
			var req = {};
			req.action = "student-login";
			req.id = $(this).parents('tr').data('id');
			req.totallab = $(this).parents('tr').data('totallab');
			req.role ="4";
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 1)
				{
					window.location = "dashboard.php";
				}
				else
				{
					toastr.error(res.message);
				}
			});
	});
}

updateAssistantPassword = function() {
	$("form#edit-student-form").off('submit');
	$("form#edit-assistant-password-form").on('submit', function(e) {
		e.preventDefault();
		if(validate()){
			var req = {};
			req.action = "updateAssistantPassword";
			req.password = $('#password').val();
			$.ajax({
				type: "post",
				url: EndPoint,
				data: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if (res.status == 1) {
					toastr.success(res.message);
					$("#edit-assistant-password").modal('hide');
					$('form#edit-assistant-password-form')[0].reset();
					Students.fetch();
				} else{
					toastr.error(res.message, "Error:");
				};
			});
		}	
	})
}

validate = function() {
	var pass = $('#password').val();
	var confirm = $('#confirm-password').val();

	if($('#password').val() != "" && $('#password').val().length<6){
		toastr.error("please enter atleast six characters long password");
		return false;
	}
	if(confirm != pass){
		toastr.error("Confirm password incorrect.");
		return false;
	}
	
	return true;
}



