$(function() {
    Students.fetch();
    getTestprograms();
    setEvent();
});

var Students = {};

Students.fetch = function() {
    var req = {};
    req.action = 'fetchInstituteStudents';
    $.ajax({
        type: "post",
        url: EndPoint,
        data: JSON.stringify(req)  
    }).done(function(res) {
        res = $.parseJSON(res);
        if (res.status == 1) {
            Students.fill(res.students);
        } else{
            toastr.error(res.message);
        };

    });
}

Students.fill = function(students) {
    var html = ''

    $.each(students, function(i, student) {
        html += '<tr>';
        html += '<td>'+(i+1)+'</td>';
        html += '<td>'+student.first_name+' '+student.last_name+'</td>';
        html += '<td>'+student.email+'</td>';
        html += '<td>'+student.mobile+'</td>';
        html += '<td>'+student.name+'</td>';
        
        html += '</tr>';
    });

    $('#students-table tbody').html(html);
}

function getTestprograms() {
    var req = {};
    req.action = "getTestprograms";
    $.ajax({
        'type'  :   'post',
        'url'   :   EndPoint,
        'data'  :   JSON.stringify(req)
    }).done(function(res) {
        res = $.parseJSON(res);
        if(res.status == 1){
            fillTestprograms(res.data);
        }
        else if(res.status == 0) {
            toastr.error(res.message);
        }
    });
}

function fillTestprograms(programs) {
    var html = '<option value="0"> Select Program </option>';

    $.each(programs, function(i, program) {
        html += '<option value="'+program.id+'" >'+program.name+'</option>';
    });

    $('#test-programs').html(html);
    $('.test-programs').html(html);
}

setEvent = function()
{
    $('#test-programs').off('change');
    $('#test-programs').change(function(e){
        e.preventDefault();
        var req = {};
        req.action = "fetchAllPapers";
        req.test_program = $(this).val();

        $.ajax({
            'type'  :   'post',
            'url'   :   EndPoint,
            'data'  :   JSON.stringify(req)
        }).done(function(res) {
            res = $.parseJSON(res);
            if(res.status == 1){
                fillTestPapers(res.data, req.test_program, res.lab);
            }
            else if(res.status == 0) {
                toastr.error(res.message);
            }
        });
    });
    
    // check or uncheck
    $('.lab').off('click');   
    $('.lab').click(function(e) {
        var req = {};
        req.action = 'addLabTest';
        req.program = $(this).attr('data-program');
        req.paper = $(this).attr('data-paper');
        req.status = ($(this).attr('checked')) ? 1: 0;
        $.ajax({
            type: "post",
            url: EndPoint,
            data: JSON.stringify(req)
        }).done(function(res) {
            // res = $.parseJSON(res);
            // if (res.status == 1) {
            // // Programs.fill(res.programs);
            // } else{
            // // toastr.error(res.message);
            // }

        });
    });
}

fillTestPapers = function(data, program, lab)
{
    lab = lab.split(',');
        
    html = '';
    $.each(data, function(i, paper) {
        html += '<tr>';
        html += '<td>'+(i+1)+'</td>';
        html += '<td>'+paper.name+'</td>';
        html += '<td>'+getPaperType(paper.type)+'</td>';        
        html += '<td><input type="checkbox" class="lab" data-id="'+paper.id+'" data-program="'+program+'" ';
        html += (lab.indexOf(paper.id) != -1) ? 'checked' : '';
        html += '></td>';        
        html += '</tr>';
    });

    $('#tests-table tbody').html(html);
    setEvent();
}

getPaperType = function(type)
{
    switch(type)
    {
        case '4':
            return "Mock Test";
            break;
        case '5':
            return "Scheduled Test";
            break;
        case '6':
            return "Practice Test";
            break;
        default:
            return "";
            break;
    }
}

// var Programs = {};
// Programs.fetch = function() {
//  var req = {};
//  req.action = 'fetchInstitutePrograms';
//  $.ajax({
//      type: "post",
//      url: EndPoint,
//      data: JSON.stringify(req)  
//  }).done(function(res) {
//      res = $.parseJSON(res);
//      if (res.status == 1) {
//          Programs.fill(res.programs);
//      } else{
//          toastr.error(res.message);
//      };

//  });
// }

// Programs.fill = function(programs) {

//  var html = '<option value="0"> Select Program </option>';

//  $.each(programs, function(i, program) {
//      html += '<option value="'+program.id+'" >'+program.name+'</option>';
//  });

//  $('#test-programs').html(html);
// }
