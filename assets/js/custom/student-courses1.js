test_program = getUrlParameter('program_id');

$(function() {

	getProgramDetails();
	//getSubjectPapers();
	
});


function getProgramDetails() {
	var	req = {};
	req.test_program = test_program;
	req.action = 'getProgramDetails';

	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req) 
	}).done(function(res) {
		res = $.parseJSON(res);
		
		if(res.status == 1) {
			$('.program-name').html(res.details.name);
			$('a.program-name').attr('href', 'student-course-details.php?program_id='+test_program);
			getSubjectPapers()
			//genrateSubjects(res.subjectList);
		}
		else {
			toastr.error(res.message);
		}
	});
}

function getSubjectPapers() {
	var	req = {};
	req.program_id = test_program;
	
	req.action = 'getSubjectPapers1';

	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req) 
	}).done(function(res) {
		res = $.parseJSON(res);
		
		if(res.status == 1) {
			fillMock(res.mock);
			fillDiagnostic(res.diagnostic);
			fillScheduled(res.scheduled);
			//genrateSubjects(res.subjectList);
		}
		else {
			toastr.error(res.message);
		}
	});
}

fillMock = function(papers) {
	html = '';
	$.each(papers, function(i,paper) {
		rem = paper.total_attempts-paper.attempts;
		rem = (rem < 1) ? 0 : rem;
		start_href = 'question-paper.php?program_id='+test_program+'&test_paper='+paper.id;
		start_href = (rem == 0) ? '#' : start_href;
		result_href = 'my-test-history.php?attempt='+paper.attempts_first;
		result_href = (rem == paper.total_attempts) ? '#' : result_href;
		html += '<tr>';
		html += '<td>';
		html += paper.name;
		html += '</td>';
		html += '<td>';
		html += 'Mock Test';
		html += '</td>';
		html += '<td>';
		html += 'NA';
		html += '</td>';
		html += '<td>';
		html += 'NA';
		html += '</td>';
		html += '<td>';
		html += paper.total_attempts;
		html += '</td>';
		html += '<td>';
		html += rem;
		html += '</td>';
		html += '<td>';
		if(rem==0 && paper.resume==1) {
			html += 'Start';
		}else{
			html += "<a href="+start_href+">";			
			html += (paper.resume == 2)?"Resume":"Start";
		}
		html += '</td>';
		html += '<td>';
		html += (paper.attempts == 0)?"See Results":"<a href="+result_href+">See Results</a>";
		html += '</td>';
		html += '</tr>';
	});
		
	$('#papers > tbody').append(html);
}

fillDiagnostic = function(papers) {
	html = '';
	$.each(papers, function(i,paper) {
		rem = paper.total_attempts-paper.attempts;
		rem = (rem < 1) ? 0 : rem;
		start_href = 'question-paper.php?program_id='+test_program+'&test_paper='+paper.id;
		start_href = (rem == 0) ? '#' : start_href;
		result_href = 'my-test-history.php?attempt='+paper.attempts_first;
		result_href = (rem == paper.total_attempts) ? '#' : result_href;
		html += '<tr>';
		html += '<td>';
		html += paper.name;
		html += '</td>';
		html += '<td>';
		html += 'Diagnostic Test';
		html += '</td>';
		html += '<td>';
		html += 'NA';
		html += '</td>';
		html += '<td>';
		html += 'NA';
		html += '</td>';
		html += '<td>';
		html += paper.total_attempts;
		html += '</td>';
		html += '<td>';
		html += rem;
		html += '</td>';
		html += '<td>';
		if(rem==0 && paper.resume==1) {
			html += 'Start';
		}else{
			html += "<a href="+start_href+">";			
			html += (paper.resume == 2)?"Resume":"Start";
		}
		html += '</td>';
		html += '<td>';

		html += (paper.attempts == 0)?"See Results":"<a href="+result_href+">See Results</a>";
		html += '</td>';
		html += '</tr>';
	});
		
	$('#papers > tbody').append(html);
}
fillScheduled = function(papers) {
	html = '';
	$.each(papers, function(i,paper) {
		rem = paper.total_attempts-paper.attempts;
		rem = (rem < 1) ? 0 : rem;
		start_href = 'question-paper.php?program_id='+test_program+'&test_paper='+paper.id;
		start_href = (rem == 0) ? '#' : start_href;
		result_href = 'my-test-history.php?attempt='+paper.attempts_first;
		result_href = (rem == paper.total_attempts) ? '#' : result_href;
		html += '<tr>';
		html += '<td>';
		html += paper.name;
		html += '</td>';
		html += '<td>';
		html += 'Scheduled Test';
		html += '</td>';
		html += '<td>';
		html += paper.start_time;
		html += '</td>';
		html += '<td>';
		html += paper.end_time;
		html += '</td>';
		html += '<td>';
		html += paper.total_attempts;
		html += '</td>';
		html += '<td>';
		html += rem;
		html += '</td>';
		html += '<td>';
		if(paper.resume == 0){
			html += 'Start';
		}else if(rem==0 && paper.resume==1) {
			html += 'Start';
		}else if(rem > 0){
			html += "<a class='scheduled-test' data-id="+paper.id+" data-start='"+paper.start_time+"' data-end='"+paper.end_time+"' href="+start_href+">";			
			html += (paper.resume == 2)?"Resume":"Start";
		}

		html += '</td>';
		html += '<td>';
		html += (paper.attempts == 0)?"See Results":"<a href="+result_href+">See Results</a>";
		html += '</td>';
		html += '</tr>';
	});
		
	$('#papers > tbody').append(html);
	setEventScheduled();
}

setEventScheduled = function() {
	$('.scheduled-test').on('click',function(e) {
		e.preventDefault();

		var href = $(this).attr('href');
		var paper = $(this).data('id');
		var start = $(this).data('start');
		var end = $(this).data('end');
		var req = {};
		req.progrm_id = test_program;
		req.paper = paper;
		req.action = 'checkScheduled';

		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req) 
		}).done(function(res) {
			res = $.parseJSON(res);
			
			if(res.status == 1) {
				window.location =href;
				
			}
			else {
				alert('Scheduled Test Timings: '+start+ ' to '+end);
				//toastr.error(res.message);
			}
		});

	})
}

setBreadcrumb = function() {

}
