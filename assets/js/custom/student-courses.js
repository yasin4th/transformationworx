test_program = getUrlParameter('program_id');
subject = getUrlParameter('subject');
$(function() {

	getProgramDetails();
	//getSubjectPapers();

});


function getProgramDetails() {
	var	req = {};
	req.test_program = test_program;
	req.action = 'getProgramDetails';

	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);

		if(res.status == 1) {
			$('.program-name').html(res.details.name);
			$('a.program-name').attr('href', 'student-course-details.php?program_id='+test_program);
			getSubjectPapers()
			//genrateSubjects(res.subjectList);
		}
		else {
			toastr.error(res.message);
		}
	});
}

function getSubjectPapers() {
	var	req = {};
	req.program_id = test_program;
	req.subject = subject;
	req.action = 'getSubjectPapers';

	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);

		if(res.status == 1) {
			fillPapers1(res.chapters);
			//genrateSubjects(res.subjectList);
		}
		else {
			toastr.error(res.message);
		}
	});
}

fillPapers = function(chapters) {
	html = '';
	$.each(chapters, function(i,chap) {
		html += "<tr>";

		html += "<td>";
		html += chap.name;
		html += "</td>";

		html += "<td><a href='view-illustration.php?test_program="+test_program+"&chapter="+chap.id+"&subject="+subject+"'>View</a></td>";

		html += "<td>";
		html += '<table class="table">';
		$.each(chap.practice, function(i,practice) {
			start_href = 'question-paper.php?program_id='+test_program+'&test_paper='+practice.id;
			html += "<tr>";
			html += "<td>";
			html += practice.name;
			html += "</td>";
			html += "<td>";
			html += "<a href="+start_href+">Start</a>";
			html += "</td>";
			html += "</tr>";
		});
		html += "</table>";
		html += "</td>";

		html += "<td>";
		html += '<table class="table">';
		$.each(chap.chapter, function(i,chapter) {
			rem = chapter.total_attempts-chapter.attempts;
			rem = (rem < 1) ? 0 : rem;
			start_href = 'question-paper.php?program_id='+test_program+'&test_paper='+chapter.id;
			start_href = (rem == 0) ? '#' : start_href;
			result_href = 'my-test-history.php?attempt='+chapter.attempts_first;
			result_href = (rem == chapter.total_attempts) ? '#' : result_href;
			html += "<tr>";

			html += "<td>";
			html += chapter.name;
			html += "</td>";

			html += "<td>";
			html += chapter.total_attempts;
			html += "</td>";

			html += "<td>";
			html += rem;
			html += "</td>";

			html += "<td>";
			html += "<a href="+start_href+">";

			html += (chapter.resume == 2)?"Resume":"Start";
			html += "</a>";
			html += "</td>";

			html += "<td>";
			html += (chapter.attempts == 0)?"See Results":"<a href="+result_href+">See Results</a>";
			html += "</td>";

			html += "</tr>";
		});
		html += "</table>";
		html += "</td>";

		html += "</tr>";
	});
	$('#papers > tbody').html(html);
}


fillPapers1 = function(chapters) {
	html = '';
	$.each(chapters, function(i,chap) {
		html += "<tr>";
		t1 = (chap.practice.length>chap.chapter.length)?chap.practice.length:chap.chapter.length;
		t1 = (t1==0)?1:t1;

		html += "<td rowspan="+t1+">";
		html += chap.name;
		html += "</td>";
		html += "<td rowspan="+t1+"><a href='view-illustration.php?test_program="+test_program+"&chapter="+chap.id+"&subject="+subject+"'>View</a></td>";


		for (var i = 0; i < t1; i++) {
			if(i!=0){
				html += '<tr>';
			}
			var flag=false;
			$.each(chap.practice,function(j,p)
			{
				if(j==i){
					start_href = 'question-paper.php?program_id='+test_program+'&test_paper='+p.id;
					html += "<td>";
					html += p.name;
					html += "</td>";
					html += "<td>";
					html += "<a href="+start_href+">Start</a>";
					html += "</td>";
					flag=true;
				}

			});
			if(flag==false)
			{
				//2 X td
				html += "<td>";
				html += "</td>";
				html += "<td>";
				html += "</td>";

			}
			var flag =false;
			$.each(chap.chapter,function(k,c)
			{
				if(k==i){
					rem = c.total_attempts-c.attempts;
					rem = (rem < 1) ? 0 : rem;
					start_href = 'question-paper.php?program_id='+test_program+'&test_paper='+c.id;
					start_href = (rem == 0) ? '#' : start_href;
					result_href = 'my-test-history.php?attempt='+c.attempts_first;
					result_href = (rem == c.total_attempts) ? '#' : result_href;


					html += "<td>";
					html += c.name;
					html += "</td>";

					html += "<td>";
					html += c.total_attempts;
					html += "</td>";

					html += "<td>";
					html += rem;
					html += "</td>";

					html += "<td>";
					html += "<a href="+start_href+">";

					html += (c.resume == 2)?"Resume":"Start";
					html += "</a>";
					html += "</td>";

					html += "<td>";
					html += (c.attempts == 0)?"See Results":"<a href="+result_href+">See Results</a>";
					html += "</td>";
					flag=true;
					//html += "</tr>";
				}

			});
			if(flag==false)
			{
				//5 X td
				html += "<td>";

				html += "</td>";
				html += "<td>";

				html += "</td>";
				html += "<td>";

				html += "</td>";
				html += "<td>";

				html += "</td>";
				html += "<td>";

				html += "</td>";


			}

			html += "</tr>";

		}
		/*html += "<td>1";
		html += "</td>";
		html += "<td>2";
		html += "</td>"
		html += "<td>1";
		html += "</td>";
		html += "<td>2";
		html += "</td>";
		html += "<td>1";
		html += "</td>";
		html += "<td>2";
		html += "</td>";
		html += "<td>2";
		html += "</td>";
		html += "</tr>";*/




	});
	$('#papers > tbody').html(html);
}

setBreadcrumb = function() {

}
