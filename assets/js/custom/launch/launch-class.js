// @author Ravi Arya 3rd March,2016 Copyright 2016 by Ravi Arya. All Rights Reserved.
var Questions = function (section_id, lable_id, question_id, question, type, time, answers, parent_id, parent_question, marks, neg_marks, qstatus, options, description, active, question_attempt_id) {

	this.section_id = section_id;
	this.question_id    = question_id;
	this.question = question;
	this.type = type;
	this.time = time;
	this.answers = answers;
	this.parent_id = parent_id;
	this.parent_question = parent_question;
	this.marks = marks;
	this.neg_marks = neg_marks;
	this.qstatus = qstatus;
	this.options = options;
	this.description = description;
	this.active = active;
	this.question_attempt_id = question_attempt_id;
}

Questions.prototype.displayQuestion = function(type) {

	html = "<div class='question'>";
	if(this.parent_id == "0"){
		//not a passage
		html += "<div class='form-group'>";
		html += "<label><strong>Question No."+(obQuestions.indexOf(this)+1)+"</strong></label>";
		html += "</div>";
		html += "<div class='col-md-12 normal-question-div'>";
		html += "<div class='row'>";
		html += "<div class='col-md-12 mrg15 mrg-top10'>";
		html += "<h5><strong><p>" + this.question + "</p></strong></h5>";
		html += '<div id="options">'+this.displayOptions()+'</div>';
		html += "</div>";
		html += "</div>";
		html += "</div>";
		if(type == "6") {
			//displaying solution in case of practice test
			html += '<div class="row">';
			html += '<div class="col-md-12">';
			html += '<div class="panel-group accordion" id="accordion' + this.question_id + '">';
			html += '<div class="panel panel-default">';
			html += '<div class="panel-heading">';
			html += '<h4 class="panel-title">';
			html += '<a class="accordion-toggle accordion-toggle-styled color-dark" data-toggle="collapse" data-parent="#accordion' + this.question_id + '" href="#collapseOne' + this.question_id + '">';
			html += '<strong>';
			html += 'View Solution';
			html += '</strong>';
			html += '</a>';
			html += '</h4>';
			html += '</div>';
			html += '<div id="collapseOne' + this.question_id + '" class="panel-collapse collapse">';
			html += '<div class="panel-body" style="height:100px; overflow-y:auto;">';
			html += this.description;
			html += '</div></div></div></div></div>';
		}
	}else{
		//passage
		html += "<div class='form-group'>";
		html += "<label><strong>"+this.getPassageText()+"</strong></label>";
		html += "</div>";

		html += "<div class='col-md-12'>";
		/*html += '<div class="row border-top-bbb"><div class="col-md-6 border-right-bbb mrg15"><div class="row"><div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 400px;"><div class="scroller QuestionDetailsHeight" data-always-visible="1" data-rail-visible="0" style="overflow: hidden; width: auto; height: 400px;"><div class="col-md-12 mrg-top10">'+this.parent_question+</div></div><div class="slimScrollBar" style="width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 400px; background: rgb(187, 187, 187);"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(234, 234, 234);"></div></div> </div> </div>'<div class="col-md-6">'<div class="col-md-12">'<div class="row">'<div class="col-md-12 mrg15 mrg-top10"><h5>'+this.question+'</h5>';*/

		html += '<div class="row border-top-bbb">';
		html += '<div class="col-md-6 border-right-bbb mrg15">';
		html += '<div class="row">';

		html += '<div class="slimScrollDiv">';

			html += '<div class="scroller QuestionDetailsHeight" data-always-visible="1" data-rail-visible="0" style="overflow-y: scroll; overflow-x: hidden; width: auto; height: 400px;">';
			html += '<div class="col-md-12 mrg-top10">';
			html += this.parent_question;
			html += '</div>';
			html += '</div>';

			// html += '<div class="slimScrollBar" style="width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 400px; background: rgb(187, 187, 187);"></div>';
			// html += '<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(234, 234, 234);"></div>';

		html += '</div>';

		html += '</div>';//row
		html += '</div>';//col-md-6 border-right-bbb mrg15

		html += '<div class="col-md-6">';
		html += '<div class="col-md-12">';
		html += "<div class='form-group'>";
		html += "<label><strong>Question No. "+(obQuestions.indexOf(this)+1)+"</strong></label>"
		html += "</div>";
		html += '<div class="row">';
		html += '<div class="col-md-12 mrg15 mrg-top10"><h5><strong>'+this.question+'</strong></h5>';
		html += '<div id="options">'+this.displayOptions()+'</div>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		if(type == "6") {
			//displaying solution in case of practice test
			html += '<div class="row">';
			html += '<div class="col-md-12">';
			html += '<div class="panel-group accordion" id="accordion' + this.question_id + '">';
			html += '<div class="panel panel-default">';
			html += '<div class="panel-heading">';
			html += '<h4 class="panel-title">';
			html += '<a class="accordion-toggle accordion-toggle-styled color-dark" data-toggle="collapse" data-parent="#accordion' + this.question_id + '" href="#collapseOne' + this.question_id + '">';
			html += '<strong>';
			html += 'View Solution';
			html += '</strong>';
			html += '</a>';
			html += '</h4>';
			html += '</div>';
			html += '<div id="collapseOne' + this.question_id + '" class="panel-collapse collapse">';
			html += '<div class="panel-body" style="height:100px; overflow-y:auto;">';
			html += this.description;
			html += '</div>';	html += '</div>';	html += '</div>';	html += '</div>';	html += '</div>';
		}

		html += '</div>';

		html += '</div>';//row border-top-bbb

		html += "</div>";
	}
	html += "</div>";
	$('#questions').hide();
	$('#questions').html(html);
	Metronic.init();
	Layout.init();
	Demo.init();
	// MathJax.Hub.Queue(["Typeset",MathJax.Hub,"questions"]);
	$('#questions').show();
};

Questions.prototype.displayOptions = function() {
	html = '';
	switch (this.type) {
		case '1':
			$.each(this.options, function(z, option) {
				html += '<div data-id=' + option.id + ' class="option col-md-12 col-sm-12">';
				html += '<label style="display:flex">';
				html += '<div class="pull-left"> <input type="radio" name="options" value="' + option.id + '">&nbsp;</div>';
				html += '<div class="pull-left">' + option.option.esc() + ' </div>';
				html += '</label></div>';
			});
			break;
		case '2':
			$.each(this.options, function(z, option) {
				html += '<div data-id=' + option.id + ' class="option col-md-12 col-sm-12">';
				html += '<label style="display:flex"> ';
				html += '<div class="pull-left">';
				html += ' <input type="checkbox" name="checkbox" value="'+option.id+'">&nbsp;';
				html += '</div>';
				html += '<div class="pull-left">' + option.option.esc() + ' </div>';
				html += '</label></div>';
			});
			break;

		case '3':$.each(this.options, function(z, option) {	html += '<div data-id=' + option.id + ' class="option col-md-12 col-sm-12">';	html += '<label style="display:block">';html += '<div class="pull-left"> <input type="radio" name="options" value="' + option.id + '">&nbsp;</div>';html += '<div class="pull-left">' + option.option + ' </div>';html += '</label></div>';	});	break;

		case '4':
			html='<ul class="sequencing-list ui-sortable">';
			answers = this.answers;
			if(answers.length > 0){
				for (var i = 0; i <= answers.length; i++) {

					$.each(this.options, function(z, option) {
						if(option.id == answers[i]){
							html+= '<li data-id="'+option.id+'" class="option mrg-bot5 ui-sortable-handle">';
							html += '<div class="row"><div class="btn btn-default col-md-9">';
							html += '<label draggable="true" style="display:block">';
							html += '<div class="pull-left">';
							html += option.option.esc();
							html += '</div></label></div></div></li>';
						}
					});
				}
			}else{
				$.each(this.options, function(z, option) {
					html+= '<li data-id="'+option.id+'" class="option mrg-bot5 ui-sortable-handle">';
					html += '<div class="row"><div class="btn btn-default col-md-9">';
					html += '<label draggable="true" style="display:block">';
					html += '<div class="pull-left">';
					html += option.option.esc();
					html += '</div></label></div></div></li>';
				});
			}
			html += '</ul>';
			break;

		case '5':
			html = '<div class="row"><div class="col-md-5 col-sm-5 col-xs-6">';
			$.each(this.options, function(z,option) {
				if(option.number == '1') {
					html += '<div class="col-md-12 col-sm-12 border-ccc mrg-bot10">';
					html += option.option;
					html += '</div>';
				}
			});
			html += '</div>';
			html += '<div class="col-md-5 col-sm-5 col-xs-6 sequencing-list ui-sortable">';
			answers = this.answers;
			if(answers.length > 0){
				for (var i = 0; i <= answers.length; i++) {
					$.each(this.options, function(z, option) {
						if(option.id == answers[i] && option.number == '2'){
							html += '<div class="option col-md-12 col-sm-12 border-ccc mrg-bot10 ui-sortable-handle" data-id="'+option.id+'">';
							html += option.option;
							html += '</div>';
						}
					});
				}
			}else{
				$.each(this.options, function(z,option) {
					if(option.number == '2') {
						html += '<div class="option col-md-12 col-sm-12 border-ccc mrg-bot10 ui-sortable-handle" data-id="'+option.id+'">';
						html += option.option;
						html += '</div>';
					}
				});
			}
			html += '</div>';
			html += '</div>';
			break;
		case '6':
			break;
		case '7':
			break;
		case '8':
			$.each(this.options, function(z, option) {
				html += '<span data-id=' + option.id + ' class="option mrg-bot5">';
				html += '<div class="mrg-bot10">';
				html += '<span class="draggable border-ccc" data-i="'+z+'" data-id="'+option.id+'">' + option.option.esc() + ' </span>';
				html += '</div>';
				//html += '<!--<div class="row"><div class="draggable btn btn-default col-md-4"> <label draggable="true" style="display:block"> <div class="pull-left"><span class="">' + option.option + ' </span></div> </label></div></div>-->';
				html += '</span>';
			});
			break;
		case '12':
			answers = this.answers;
			html += '<div id="matrix-options-div" class="col-md-11 matrix-table-options"> <div class="margin-bottom-10 table-scrollable">';
			html += '<table id="matrix-table" class="table table-bordered table-striped table-hover">';
			html += '<tr><td></td>';

			var max = 1;

			$.each(this.options, function(i,option) {
				max = option.answer;
			})

			for (var i = 1; i <= max; i++) {
				$.each(this.options, function(j,option) {
					if(option.answer == i){
						if(option.option != 0 && option.option !=1){
							html += '<td><strong>'+option.option+'<strong></td>';
						}else{
							html += '<td><input type="checkbox" name="checkbox" data-id="'+option.id+'"></td>';
						}
					}
				});
				html += '</tr>';
			}

			html += '</table></div> </div>';
			break;

		case '13':
			html += '<input type="text">';
			break;
		default:
			break;
	}
	return html;
}

Questions.prototype.activateOptions = function() {
	html = '';
	switch (this.type) {
		case '4':
			if ($('.sequencing-list').length){
				$('.sequencing-list').sortable();
			}
			break;
		case '5':
			if ($('.sequencing-list').length){
				$('.sequencing-list').sortable();
			}
			break;
		case '6':
			$('.question').find('input:text').removeAttr('disabled');
			break;
		case '7':
			Options = this.options;
			$('.question').find('select').removeAttr('disabled');

			$.each($('.question').find('select'),function(i,option) {
				html = '<option></option>';
				$.each(Options, function(z, option) {
					if(option.number == (i+1)){
						html += '<option value="'+option.id+'">'+option.option+'</option>';
					}
				});
				$('.question').find('select').eq(i).html(html);

			});
			$('.question').find('select').css('height','30px');
			$('.question').find('select').css('width','150px');

			break;
		case '8':

			$('.input-droppable').css('width','200px');
			$('.input-droppable').css('height','30px');
			if ($('.input-droppable').length) {
				$('.input-droppable').droppable({
					tolerance: "intersect",
					greedy: true,
					drop: function(event, ui) {

						$(this).addClass('filled');
						$(this).droppable( "disable" );

						$(this).attr('data-in',$(ui.draggable).data('id'));
						$(ui.draggable).attr('data-on',$(this).data('id'));

						//console.log($(this).data('id').substring(5));

						//console.log(ui.draggable.data('id'));
						//answers[ui.draggable.data('z')] = ui.draggable.data('id');
						//storeFND(ui.draggable.data('i'), ui.draggable.data('id'));
					}
					/*over: function(event, ui) {

						// Enable all the .droppable elements
						$('.input-droppable').droppable('enable');

						// If the droppable element we're hovered over already contains a .draggable element,
						// don't allow another one to be dropped on it
						console.log($(this).);

						if($(this).has('.draggable').length) {
							$(this).droppable('disable');
						}
					},*/
					/*revert: function (event, ui) {
						console.log("h");
						$(this).droppable( "option", "disabled", false );
					},*/
					/*out: function(event, ui) {
						console.log("h");
						$(this).removeClass('filled');
						$(this).droppable("enable");
					}*/
				});
			}
			if ($('#options').length) {
				$('#options').droppable({
					tolerance: "fit",
					drop: function(event, ui) {
						//$(this).removeClass('ui-state-default');
						//console.log(ui);
						droppedOn = $(ui.draggable).data('on');
						console.log(droppedOn);
						$('.question .input-droppable[data-id='+droppedOn+']').droppable("enable");
						$(ui.draggable).removeAttr('data-on');
						//console.log(ui.draggable.data('id'));
						//answers[ui.draggable.data('z')] = ui.draggable.data('id');
						//storeFND(ui.draggable.data('i'), ui.draggable.data('id'));
					}
				});
			}
			if ($('.draggable').length) {
				$('.draggable').draggable({
					revert: "invalid",
					cursor: "move"
				});
			}
			break;

		default:
			break;
	}
	return html;
}

Questions.prototype.getPassageText = function() {
	parent_id = this.parent_id;
	text = '';
	start = obQuestions.indexOf(this)+1;
	end = 0;
	$.each(obQuestions,function(i,question){
		if(parent_id == question.parent_id){
			if(start > i+1){
				start = i+1;
			}
			end = i+1;
		}
	});
	text = "Passage for Questions ["+start+" - "+end+"]";
	return text;
}

Questions.prototype.storeAnswers = function() {
	//store answers
	var answers=[];
	switch(this.type){
		case '1':
			$.each($('.question').find('input:checked'), function(i, answer) {
				answers.push(answer.value);
			})
			break;
		case '2':
			$.each($('.question').find('input:checked'), function(i, answer) {
				answers.push(answer.value);
			})
			break;
		case '4':
			$.each($('.question').find('li.option'), function(i, answer) {
				answers.push($(answer).data('id'));
			})
			break;
		case '5':
			$.each($('.question').find('.option'), function(i, answer) {
				answers.push($(answer).data('id'));
			})
			break;
		case '6':
			$.each($(".question").find('input:text'), function(i, answer) {
				answers.push($(answer).val());
			});
			break;
		case '7':
			$.each($(".question").find('select'), function(i, answer) {
				answers.push($(answer).val());
			});
			break;
		case '8':
			$.each($('.question .input-droppable'), function(i, droppable) {
				data = $(this).data('id');

				answer = 0;
				$.each($('#options .draggable[data-on]'), function(i, draggable) {
					if( data == $(this).data('on')){
						answer = $(this).data("id");
					}
				});
				answers.push(answer);
			});
			break;
		case '12':
			$.each($(".question").find('input:checked'), function(i, answer) {
				answers.push($(answer).data('id'));
			})
			break;
		case '13':
			$.each($(".question").find('input:text'), function(i, answer) {
				answers.push($(answer).val());
			});
			break;
		default:
			break;
	}
	this.answers = answers;
}

Questions.prototype.setAnswers = function() {
	//set answers on the paper
	switch(this.type){
		case '1':
			answers = this.answers;

			$.each($('.question').find('input:radio'), function(i, answer) {
				if($(answer).val() == answers[0]){
					$(answer).prop('checked',true);
				}
			})
			$('input').uniform();
			break;
		case '2':
			answers = this.answers;

			$.each($('.question').find('input:checkbox'), function(i, answer) {
				if(answers.indexOf($(answer).val()) != -1){
					//console.log($(answer).val());

					$(this).prop('checked',true);
				}
			})
			$('input').uniform();
			break;
		case '4':
			break;
		case '6':
			answers = this.answers;
			$.each($(".question").find('input:text'), function(i, answer) {
				$(answer).val(answers[i]);
			});
			break;
		case '7':
			answers=this.answers;
			$.each($(".question").find('select'), function(i, answer) {
				$(answer).val(answers[i]);
			});
			break;
		case '8':
			answers = this.answers;
			for (var i = 0; i <= answers.length; i++) {
				if(answers[i] != '0'){
					position = $('.question .input-droppable').eq(i).offset();
					dataOn = $('.question .input-droppable').eq(i).data("on");
					$('#options .draggable[data-id='+answers[i]+']').css("position", "absolute");
					$('#options .draggable[data-id='+answers[i]+']').offset(position);
					$('#options .draggable[data-id='+answers[i]+']').data("on",dataOn);

					//TODO : disable particular droppable
				}
			}

			//$.each($(".question").find('input:text'), function(i, answer) {
				//answers.push($(answer).val());
			//});
			break;
		case '12':
			answers = this.answers;
			$.each($('.question').find('input:checkbox'), function(i, answer) {
				if(answers.indexOf($(answer).data('id')) != -1){
					$(this).prop('checked',true);
				}
			})
			$('input').uniform();
			break;
		case '13':
			$('.question').find('input:text').val(this.answers[0]);
			break;
		default:
			break;
	}
}

Questions.prototype.jumpSection = function(section_id) {
	for (var i = 0; i < obQuestions.length; i++)
	{
		if(section_id == obQuestions[i].section_id)
		{
			$('.numbering').eq(i).click();
			return;
		}
	}
}

Questions.prototype.setColor = function() {

	$.each(obQuestions, function(i, question){
		switch(question.qstatus){

			case '1':
				$('.numbering').eq(i).addClass('btn-warning').removeClass('red green purple btn-default');
				break;
			case '2':
				//marked for review and saved
				$('.numbering').eq(i).addClass('purple').removeClass('red green btn-default btn-warning');
				break;
			case '3':
				//save and next
				$('.numbering').eq(i).addClass('green').removeClass('red purple btn-warning btn-default');
				break;
			default:
				if(question.qstatus == 0 && question.active)
				{
					$('.numbering').eq(i).addClass('red').removeClass('green purple btn-default btn-warning');
				}
				else if(question.qstatus == 0)
				{
					$('.numbering').eq(i).addClass('btn-default').removeClass('red green purple btn-warning');
				}

				break;
		}
	});
}

Questions.prototype.getActive = function() {
	for (var i = 0; i < obQuestions.length; i++) {
		if(obQuestions[i].active == true){
			return i;
		}
	}
}

Questions.prototype.updateTime = function(){
	index = Questions.prototype.getActive();
	obQuestions[index].time++;
}

Questions.prototype.saveQuestionPaper = function(){
	var req = {};

	$.each(obQuestions,function(i,question) {
		obQuestions[i].question = "";
		obQuestions[i].options = "";
		obQuestions[i].description = "";
	});

	req.answers = obQuestions;
	req.action = "saveQuestionPaper";
	req.testPaper = paper;
	req.program = program;
	req.paper_attempt = paper_attempt;
	$.ajax({
		'type': 'post',
		'url': EndPoint,
		'data': JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 1) {
			//$('.optmizer').hide();
			clearInterval(clock);
			if (res.result_status.status == 1) {
				$('#result-progressbar-div').hide('slow');
				$('.modal-body h3').hide('slow');
				/*if (res.result_status.test_type != 0) {
					$('.optmizer').show()
				};*/
				$('#result-marks-div').show('slow');
				$('#marks-archived').text(res.result_status.marks_archived);
				//$('#total-marks').text(res.result_status.total_marks);
				$('#go-to-test-history').attr('disabled', false);
				if(res.result_status.total_marks == res.result_status.marks_archived) {
					$('#create-test-optmizer').remove();
					$('.optmizer').remove();
				}else{
					$("#create-test-optmizer").attr('disabled', false);
					setEventCreateOptmizer();
				}
				setTimeout(function(){
					window.close();
				},3000);
			}
		}
	});
}

Questions.prototype.saveQuestion = function() {
	var req = {};
	var active = Questions.prototype.getActive();
	req.time = obQuestions[active].time;
	req.qstatus = obQuestions[active].qstatus;
	req.answers = obQuestions[active].answers;
	req.question_id = obQuestions[active].question_id;
	req.question_attempt_id = obQuestions[active].question_attempt_id;
	req.action = "saveQuestion";
	req.testPaper = paper;
	req.program = program;
	$.ajax({
		'type': 'post',
		'url': EndPoint,
		'data': JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 1) {

		}else if(res.status == 2) {
			window.location = "login.php";
		}
	});
}

Questions.prototype.displayAllQuestion = function(){
	html = '<table class="table table-striped table-hover table-bordered"><thead><tr><th><i class="fa fa-caret-down"></i> Sr No </th><th><i class="fa fa-caret-down"></i> Question </th></tr></thead><tbody>';
	$.each(obQuestions,function(i,question){
		html += '<tr><td>'+(i+1)+'</td><td style="text-align:left;">'+question.question+'</td></tr>';
	})
	html += '<tbody></table>';
	$('#all-questions-modal .modal-body').html(html);
	// MathJax.Hub.Queue(["Typeset",MathJax.Hub,"table"]);
}


function shuffle(a) {
	var j, x, i;
	for (i = a.length; i; i--) {
		j = Math.floor(Math.random() * i);
		x = a[i - 1];
		a[i - 1] = a[j];
		a[j] = x;
	}
}