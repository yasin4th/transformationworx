$(function(){
	submitForm();
	$('#cancel').on('click',function(e){
		window.location = "index.php";
	});
});

validate = function(){
	if($('#name').val() == ""){
		toastr.error('Please fill your name.');
		return false
	}

	if($('#email').val() == ""){
		toastr.error('Please fill your email.');
		return false;
	}

	if($('#message').val() == ""){
		toastr.error('Please fill your message.');
		return false;
	}
	if(!validateEmail($('#email').val())){
			toastr.error('please enter valid email.');
			return false;
	}

	return true;

}

submitForm = function(){

	$('#contact-form').off('submit');
	$('#contact-form').on('submit', function(e){
		e.preventDefault();
		req = {};
		req.action = 'submitContactForm';
		if(validate()){
			req.name = $('#name').val();
			req.email = $('#email').val();
			req.message = $('#message').val();
			$.ajax({
				'type' : 'POST',
				'url' : EndPoint,
				'data' : JSON.stringify(req),
			}).done(function(res){
				res = $.parseJSON(res);
				if(res.status == 1){
					$('#contact-form')[0].reset();
					toastr.success(res.message);
				}
				else
				{
					toastr.error(res.message);
				}
			});
		}

	});
}

validateEmail = function(sEmail) {
	var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if (filter.test(sEmail)) {
	return true;
	}
	else {
	return false;
	}
}
