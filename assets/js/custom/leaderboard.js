test_paper = getUrlParameter('paper');
$(function(){	
	fetchLeaderboard();	
});

function fetchLeaderboard() {
	var req = {};
	req.action = "fetchLeaderboard";
	req.test_paper = test_paper;

	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillData(res.data,res.paper[0].count);
			if(res.paper.length > 0)
			{
				$('#paper-name').html("Test Paper : "+res.paper[0].name);
			}
		}
		else {
			toastr.error(res.message,'Error');
		}			
	});
}

fillData = function(data,total)
{
	html = '';
	$.each(data, function(i,d){
		html += '<tr>';
		html += '<td>';
		html += i+1;
		html += '</td>';
		html += '<td>';
		html += '<img src="'+d.image+'" class="img-responsive">';
		html += '</td>';
		html += '<td>';
		html += d.first_name+' '+d.last_name;
		html += '</td>';
		html += '<td>';
		html += total;
		html += '</td>';
		html += '<td>';
		html += d.attempt;
		html += '</td>';
		html += '<td>';
		html += d.correct;
		html += '</td>';
		html += '<td>';
		html += d.marks_archived;
		html += '</td>';
		html += '</tr>';
	})
	$('#leaderboard tbody').html(html)
}
