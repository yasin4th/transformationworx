test_program = getUrlParameter('program_id');
subject = getUrlParameter('subject');

$(function() {
	// this is for BCT(BreadCrumb Trail)
	//$('.subject').attr('href', 'student-course-details.php?program_id='+test_program);

	getProgramDetails();
	setEventViewTest();
	fetchNumberOfAttachedTestPapers();
	fetchStudentDifficulties();
	genrateTest();
});



function getProgramDetails() {
	var	req = {};
	req.test_program = test_program;
	req.action = 'getProgramDetails';

	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req) 
	}).done(function(res) {
		res = $.parseJSON(res);
		//console.log(res);
		if(res.status == 1) {
			$('.program-name').html(res.details.name);
			fillChaptrsToAddTest(res.subjectList);
			setBreadcrumb(res.subjectList);
		}
		else {
			toastr.error(res.message);
		}
	});

}

function fillChaptrsToAddTest () {
	req = {};
	req.action = 'fetchChaptersOfSubject';
	req.subject_id = [subject];
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			html = '';
			$.each(res.chapters, function(i,chapter) {

				html += '<div class="col-md-4 col-sm-6 col-xs-12"> <div class="form-group sub-box"> <div class="sub-name tooltips" data-container="body" data-placement="top" title="'+chapter.name+'"> <a href="student-course-chapter-tests-types.php?program_id='+test_program+'&subject='+getUrlParameter('subject')+'&chapter='+chapter.id+'" data-id="'+chapter.id+'" >'+chapter.name+'</a> </div> </div> </div>';
			})
			$('#chapters-div').html(html);
			$('.tooltips').tooltip();
			var html = '';
			// var html = '<option> Select Chapters </option>';
			$.each(res.chapters, function(i, chapter) {
				html += '<option value="'+chapter.id+'">'+chapter.name+'</option>';
			})
			$('.chapters').html(html).select2();
		}
		else{
			// toastr.error('Error Chapters not load');
		}
	});
}

function setEventViewTest() {
	$('.view-test').off('click');
	$('.view-test').on('click', function(e) {
		e.preventDefault();
		type = $(this).parents('.dashboard-stat').data('category');
		window.location = 'view-course-details-2.php?program_id='+test_program+'&type='+type+'&parent_id='+subject+'&parent_type=1';
	})
}

/*
* function to  show how number of papers attached
*/
function fetchNumberOfAttachedTestPapers() {
	var req = {};
	req.action = "fetchNumberOfAttachedTestPapers";
	req.test_program = test_program;
	req.parent_id = subject;
	req.parent_type = 1;
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			
			// console.log(res);
			if(res.data.diagnostic != undefined){
				$('#diagnostic .count').text(res.data.diagnostic);
			}
			if(res.data.fullSyllabus != undefined){
				$('#full-syllabus-test .count').text(res.data.fullSyllabus);
			}
		}
		else {
			toastr.error(res.message);
		}
	});
}


function fetchStudentDifficulties() {
	var req = {};
	req.action = 'fetchStudentDifficulties';
	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req) 
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 1) {
			fillDifficulties(res.difficulties);
		}
		else{
			toastr.error(res.message);
		};
	});
}

function fillDifficulties(difficulties) {
	var html = '';
	$.each(difficulties, function(i, difficulty) {
		html += '<option value="'+difficulty.id+'">'+difficulty.difficulty+'</option>';
	})
	$('.difficulties').html(html).select2();
}

function genrateTest() {
	$('#test-genrator-form').off('submit');
	$('#test-genrator-form').on('submit', function(e) {
		e.preventDefault();
		var req = {};
		req.action = "createStudentTestPaper";
		req.difficulty = $('#test-genrator-form .difficulties:eq(1)').val();
		req.time = $('#test-genrator-form .time').val();
		req.chapter = $('#test-genrator-form .chapters:eq(1)').val();
		req.numberOfQuestion = $('#test-genrator-form .number-of-questions').val();
		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req) 
		}).done(function(res) {
			res = $.parseJSON(res);
			if (res.status == 1) {
				toastr.info("Paper Successfully Genrated. We will redirect you to paper. Please Wait..");
				setTimeout(function(){ window.location = "launch-question-paper.php?paper="+res.test_paper_id}, 2500)
			}
			else{
				toastr.error(res.message);
			};
			console.log(res);
		});
			console.log('function not ready');
	});
}

setBreadcrumb = function(subjects) {
	$.each(subjects, function(i, sub){
		if(sub.id==subject){
			$('.subject').html(sub.name);
			$('a.subject').attr('href','student-course-chapters.php?program_id='+test_program+'&subject='+subject);
			$('a.program-name').attr('href','student-course-details.php?program_id='+test_program);
			
			
		}
	});

}