test_program = getUrlParameter('program_id');

$(function() {

	getProgramDetails();
	fetchNumberOfAttachedTestPapers();
	setEventViewTest();	
});


function getProgramDetails() {
	var	req = {};
	req.test_program = test_program;
	req.action = 'getProgramDetails';

	$.ajax({
		type: "post",
		url: EndPoint,
		data: JSON.stringify(req) 
	}).done(function(res) {
		res = $.parseJSON(res);
		
		if(res.status == 1) {
			$('.program-name').html(res.details.name);
			$('a.program-name').attr('href', 'student-course-details.php?program_id='+test_program);
			genrateSubjects(res.subjectList);
		}
		else {
			toastr.error(res.message);
		}
	});
}

function fetchNumberOfAttachedTestPapers() {
	var req = {};
	req.action = "fetchNumberOfAttachedTestPapers";
	req.test_program = test_program;
	req.parent_id = 0;
	req.parent_type = 0;
	$.ajax({
		type: "post",
		url: EndPoint,
		data:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			
			if(res.data.diagnostic != undefined){
				$('#diagnostic .count').text(res.data.diagnostic);
			}
			if(res.data.fullSyllabus != undefined){
				$('#full-syllabus-test .count').text(res.data.fullSyllabus);
			}
			if(res.data.scheduled != undefined){
				$('#scheduled-test .count').text(res.data.scheduled);
			}	
		}
		else {
			toastr.error(res.message);
		}
	});
}


function genrateSubjects(subjects) {
	html = '';
	$.each(subjects, function(i,subject) {
		//html += '<div class="col-md-4 col-sm-6 col-xs-12"> <div class="form-group sub-box" > <div class="sub-name tooltips"> <a href="student-course-chapters.php?program_id='+test_program+'&subject='+subject.id+'" data-id="'+subject.id+'" class="tooltips" data-placement="top" data-toggle="tooltip" title="'+subject.name+'">'+subject.name+'</a> </div> </div> </div>';
		html += '<div class="col-md-4 col-sm-6 col-xs-12"> <div class="form-group sub-box" > <div class="sub-name tooltips"> <a href="student-courses.php?program_id='+test_program+'&subject='+subject.id+'" data-id="'+subject.id+'" class="tooltips" data-placement="top" data-toggle="tooltip" title="'+subject.name+'">'+subject.name+'</a> </div> </div> </div>';
	})
		html += '<div class="col-md-4 col-sm-6 col-xs-12"> <div class="form-group sub-box" > <div class="sub-name tooltips"> <a href="student-courses1.php?program_id='+test_program+'" class="tooltips" data-placement="top" data-toggle="tooltip" title="Combined Tests">Combined Tests</a> </div> </div> </div>';
	$('#subjects-div').html(html);
	Metronic.init();
	$('.tooltips').tooltip();
}

function setEventViewTest() {
	$('.view-test').off('click');
	$('.view-test').on('click', function(e) {
		e.preventDefault();
		type = $(this).parents('.dashboard-stat').data('category');
		window.location = 'view-course-details-1.php?program_id='+test_program+'&type='+type+'&parent_id=0&parent_type=0';
	})
}

setBreadcrumb = function() {

}
