$(function() {

    //getTestPrograms();
    //setEventOnTestProgramChange();
    //setEventGetReportCardData();
    getReport();
    //$('#sub').hide();
})
getReport = function()
{
    var req = {};
    req.action = 'getReportCardData';
    
    $.ajax({
        type: "post",
        url: EndPoint,
        data: JSON.stringify(req)
    }).done(function(res) {
        res = $.parseJSON(res);
        if (res.status == 1)
        {            
            genrateReport(res.report);
        }
        else
        {
            toastr.error(res.message)
        }       
    });
}

function setEventOnTestProgramChange() {
	$('#select-test-programs').off('change');
    $('#select-test-programs').on('change', function (e) {
        e.preventDefault();
        var html = '';
        $('#sub').hide();
        if(this.value == 0)
        {
            $('#select-subject').html('<option value="0"> Select Subject </option>');
        }
        else
        {
            if($('#select-test-type').val() == 3)
            {
                $('#sub').show(100);
                $('#select-test-type')
                getTestProgramsSubjects();
            }
        }
    });
    
	$('#select-test-type').on('change', function (e) {
		e.preventDefault();
		var html = '';
        if(this.value == 3)
        {
            $('#sub').show(100);
            getTestProgramsSubjects();
        }
        else
        {
            $('#sub').hide(100);
            $('#select-subject').html('<option value="0"> Select Subject </option>');
        }
	});
}

/**
* function to fetch programs
*/
function getTestPrograms()
{
	var req = {};
	req.action = "getTestProgramsOfStudent";

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			fillTestProgram(res.data);
		}
	});
}

/**
* function to fill programs
*/
function fillTestProgram(programs) 
{
	var html = '<option value="0"> Select Test Program </option>';
	$.each(programs, function(i, program) {
		html += '<option  value="'+program.id+'">';
		html += program.name;
		html += '</option>';
	})
	$('#select-test-programs').html(html);
}

/**
* function to fetch subjects os programs
*/
function getTestProgramsSubjects() 
{
	var req = {};
	req.action = "getTestProgramsSubjects";
	req.program_id = $('#select-test-programs').val();

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			fillTestProgramSubjects(res.data);
		}
	});
}

/**
* function to fill subjects
*/
function fillTestProgramSubjects(subjects) 
{
	var html = '<option value="0"> Select Subject </option>';
	$.each(subjects, function(i, subject) {
		html += '<option  value="'+subject.id+'">';
		html += subject.name;
		html += '</option>';
	})
	$('#select-subject').html(html);
}

function setEventGetReportCardData() 
{
	$('#create-paper-form').off('submit');
	$('#create-paper-form').on('submit', function(e) {
		e.preventDefault();
		var req = {};
		req.action = 'getReportCardData';
		req.test_programs = $('#select-test-programs').val();
		req.subject = $('#select-subject').val();
		req.test_type = $('#select-test-type').val();
		//req.test_type = 3;
		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if (res.status == 1) {
				//console.log(res.report);
                genrateReport(res.report);
			} else{
				toastr.error(res.message)
			};
			$('.create-report-page .tools a').removeClass('expand').addClass('collapse');
			$(".create-report-page .portlet-body").removeClass('hide');
		});
	});
}

var all_tests = [];
function genrateReport(tests) {
    // console.log(tests);
    var html="You have not attempt any exams, To view report card please select exams and attempt test from <a href='my-program.php'>here</a> ";
    if(tests.length ==0){
        $('#percentage-score-spline-chart-target').html(html);
        return;
    }
     $("#tests-table").removeClass('hidden');
	var data = {};
    data.name = [];
	data.speed = [];
	data.strike_rate = [];
	data.eas = [];
	data.percent_score = [];
	data.topper_gap = [];

     html = '';
    if(tests.length != 0){

    	$.each(tests,  function(i, test) {
           

            var topper_gap = parseFloat(test.topper_marks - test.marks_archived);
            var eas = (parseFloat(test.total_time) > 0)?parseFloat((test.correct_questions / test.total_time).toFixed(2)):0;
            var strike_rate = (parseFloat(test.attempted_questions) > 0)?parseFloat(((test.correct_questions / test.attempted_questions) * 100).toFixed(2)):0;
    		var speed = (parseFloat(test.time_taken) > 0)?parseFloat((test.attempted_questions * 60 / test.time_taken).toFixed(2)):0;
            var percent_marks = parseFloat((parseFloat((test.marks_archived * 100)/ test.total_marks)).toFixed(2));

            data.name.push(test.name);
            data.topper_gap.push(topper_gap);
            data.speed.push(speed);
            data.eas.push(eas);
            data.percent_score.push(percent_marks);
            data.strike_rate.push(strike_rate);

    		all_tests.push(test);

    		html += '<tr>'
    		html += '<td> <a href="my-test-history.php?attempt='+test.attempt_id+'"> '+test.name+' </a> </td>';
    		html += '<td> '+ test.total_questions +' </td>';
    		html += '<td> '+ test.attempted_questions +' </td>';
    		html += '<td> '+ test.correct_questions +' </td>';
    		html += '<td> '+ test.marks_archived +' </td>';
    		html += '<td> '+ percent_marks +' </td>';
    		html += '<td> '+ speed +' </td>';
    		// html += '<td> '+ strike_rate +' </td>';
    		// html += '<td> '+ eas +' </td>';
    		html += '</tr>';

    	})
    }

	$("#tests-table tbody").html(html);
	 genrateChartsReport(data);
}


function genrateChartsReport(data) {
    Highcharts.setOptions({
		colors: ['#22FF04', '#223DE6', '#FF1212', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
	}); // red: #FF1212, blue: #223DE6, green: #22FF04,
    //console.log("speed "+data.speed);

	$('#speed-spline-chart-target').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: '  '
        },
        subtitle: {
            text: ' Speed Analysis '
        },
        xAxis: {
            // min: 1,
			// max: data.speed.length,
            categories: data.name,
 			tickInterval: 1,
            title: {
                text: 'Test'
            }
        },
        yAxis: {
            min: 0,
			// max: 15,
 			// tickInterval: 1,
            title: {
                text: 'Speed (Q. Attempted / min)'
            },
        },

        series: [{
            name: "Speed",
            color: '#235A9C',
			data: data.speed
        }]
    });

	$('#eas-spline-chart-target').highcharts({
         chart: {
            type: 'spline'
        },
        title: {
            text: '  '
        },
        subtitle: {
            text: ' Effective Average Speed '
        },
        xAxis: {
            // min: 1,
			// max: data.speed.length,
            categories: data.name,
 			tickInterval: 1,
            title: {
                text: 'Test'
            }
        },
        yAxis: {
            min: 0,
			// max: 15,
 			// tickInterval: 1,
            title: {
                text: ' E A S '
            },
        },

        series: [{
            name: "E A S",
            color: '#AD44A9',
			data: data.eas
        }]
    });

	$('#percentage-score-spline-chart-target').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '  '
        },
        subtitle: {
            text: ' Percentage Score Analysis '
        },
        xAxis: {
            // min: 1,
			// max: data.speed.length,
            categories: data.name,
 			tickInterval: 1,
            title: {
                text: 'Test'
            }
        },
        yAxis: {
            min: 0,
			// max: 15,
 			// tickInterval: 1,
            title: {
                text: 'Percentage Score'
            },
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                },
                maxPointWidth: 40
            }
        },
        // tooltip: {
        //     headerFormat: '<b>test</b><br>',
        // },

        series: [{
            name: "Score%",
            colorByPoint: true,
			data: data.percent_score
        }]
    });

	$('#strike-rate-spline-chart-target').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: '  '
        },
        subtitle: {
            text: ' Strike Rate Analysis '
        },
        xAxis: {
            // min: 1,
			// max: data.speed.length,
            categories: data.name,
 			tickInterval: 1,
            title: {
                text: 'Test'
            }
        },
        yAxis: {
            min: 0,
			// max: 15,
 			// tickInterval: 1,
            title: {
                text: ' Strike Rate '
            },
        },

        series: [{
            name: "Strike Rate",
            color: '#249C23',
			data: data.strike_rate
			// data: data.strike_rate
        }]
    });

	$('#toppers-gap-spline-chart-target').highcharts({
       chart: {
            type: 'spline'
        },
        title: {
            text: '  '
        },
        subtitle: {
            text: ' Topper Gap Analysis '
        },
        xAxis: {
            // min: 1,
			// max: data.speed.length,
            categories: data.name,
 			tickInterval: 1,
            title: {
                text: 'Test'
            }
        },
        yAxis: {
            min: 0,
			// max: 15,
 			// tickInterval: 1,
            title: {
                text: 'Topper Gap'
            },
        },


        series: [{
            name: "Gap",
            color: '#E02222',
			data: data.topper_gap
        }]
    });
}
