$(document).ready(function() {
	fetchCertificates();
})

fetchCertificates = function() {
	var req = {};
	req.action = "fetchCertificates";

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)

	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1)
		{
			fillCertificates(res.data);
		}
	});
}

fillCertificates = function(data) {
	i = 1;
	if(data.length > 0) {
		html = `<table class="table table-bordered">
			<thead>
				<tr>
					<th>S No.</th>
					<th>Package</th>
					<th>Transaction Hash</th>
					<th>License No</th>
					<th>Download</th>
				</tr>
			</thead>
		<tbody>`;
		$.each(data, function(i, cert) {
			html += `<tr>
				<td>${i++}</td>
				<td>${cert.program}</td>
				<td>${cert.hash}</td>
				<td>${cert.license_no}</td>
				<td>
					<a href="/downloadpdf.php?phrase=${cert.hash}" target="_blank">Download <i class="fa fa-download"></i></a>
				</td>
			</tr>`;
		});
		html += `</tbody></table>`;
		$('#CertificationDescriptions').html(html);
	} else {
		$('#CertificationDescriptions').html('<h2>No certificates found.</h2>')
	}
}
