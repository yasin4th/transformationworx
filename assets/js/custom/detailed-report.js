function setEventShortQuestionsEvent() {
	$('#short-questions-dorp-down').off('change');
	$('#short-questions-dorp-down').on('change', function(e) {
		e.preventDefault();
		status = $(this).val();
		$('.question').addClass('hidden');

		switch(parseInt(status)) {
			case 1:
				$('.question[data-correct="1"]').removeClass('hidden');
				break;

			case 2:
				$('.question[data-correct="0"][data-status="3"]').removeClass('hidden');
				break;

			case 3:
				$('.question').removeClass('hidden');
				$('.question[data-status="3"]').addClass('hidden');
				break;

			default:
				$('.question').removeClass('hidden');
				break;
		}

	});
}


function cerateDetailedReport(questions) {
	var html = '';
	$.each(questions, function(i, question) {

		html += getQuestionHtml(i, question);

	})

	$('#detailed-questions-report').html(html);
	setEventShortQuestionsEvent();
	Metronic.init();
	markQuestionEvent();
	MathJax.Hub.Queue(["Typeset",MathJax.Hub,"detailed-questions-report"]);
}

function getQuestionHtml(num, question) {
    // console.log(question);

	switch(question.question_type) {
		case "1":
			html = '';
			html += '<div  data-id="'+question.question_id+'" data-correct='+question.correct+' data-status='+question.question_status+' class="question row mrg-top40"> <div class="col-md-12">';
			html += '<div class="row"> <div class="col-md-12"> <div class="col-md-1">';

			// html += '<h4><strong>Qu.'+(num+1)+':</strong></h4>';
			if(question.correct == 1)
			{
				html += '<h4><a class="deleteOption btn btn-circle btn-icon-only green" style="margin-bottom: inherit;">'+(num+1)+'</a></h4>';
			}
			else
			{
				if(question.student_answer.length == 0)
				{
					html += '<h4><a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">'+(num+1)+'</a></h4>';
				}
				else
				{
					html += '<h4><a class="deleteOption btn btn-circle btn-icon-only red" style="margin-bottom: inherit;">'+(num+1)+'</a></h4>';
				}
			}
			html += '</div>';

			html += '<div class="col-md-11 borderccc">';
			//console.log(question.parent);
			if(question.parent != undefined){
				html += '<div class="mrg-bot20"><strong>  '+question.parent+'<br><br>'+question.question+' </strong></div>';
			} else {
				html += '<div class="mrg-bot20"><strong>  '+question.question+' </strong></div>';
			}

			html += '<div class="row option padd-left30">';
			$.each(question.options,  function(x, option) {
				html += '<div class="col-md-12">';
				html += '<div class="pull-left">'+option.option+'  </div> ';
					$.each(question.student_answer,  function(y, answered)
					{
						if (question.correct == 0 && answered.option_id == option.id) {
							html += '<div class="pull-left mrg-left5"> <p class="in-correct"><i class="fa fa-times"></i></p> </div>';
						}


						if (question.correct == 1 && answered.option_id == option.id && option.answer == 1) {
							html += '<div class="pull-left mrg-left5"> <p class="correct"><i class="fa fa-check"></i></p> </div>';
						};
					});
					if(question.correct == 0 && option.answer == 1)
					{
						html += '<div class="pull-left mrg-left5"> <p class="correct"><i class="fa fa-check"></i></p> </div>';
					}

				html += '</div>';
			})
			html += '</div>';


			html += '<div class="row">	<div class="col-md-12"> <div class="pull-right">';

			if(question.correct == 1) {
				html += '<h5> Score <span>'+question.marks+'/'+question.marks+'</span></h5>';
			} else {
				html += '<h5> Score <span>0/'+question.marks+'</span></h5>';
			}

            html += '<h6>Time taken: '+question.time+' sec</h6></div>';

			html += '</div>	</div>';

			html += '</div>	</div> </div>';

			html += '<div class="row"> <div class="col-md-1"></div>  <div class="col-md-11">';
			// html += '<h4><strong>Click to view Solution / Explanation</strong></h4>';
			html += '<div class="panel-group accordion" id="accordion'+(num+1)+'">';
			html += '<div class="panel panel-default">';
			html += '<div class="panel-heading">';
			html += '<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion'+(num+1)+'" href="#collapse_'+(num+1)+'">Click to view Solution / Explanation </a></h4>';
			html += '</div>';
			html += '<div id="collapse_'+(num+1)+'" class="panel-collapse collapse">';
			html += '<div class="panel-body">';
			html += '<div class="sol-expla-scrllbar">'	+ question.solution +	'</div>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			html += '</div>';

			// html += '<div class="borderccc sol-expla-scrllbar">'	+ question.solution +	'</div>';
			//html += '<div class="row mrg-top20"> <div class="col-md-12 col-sm-12"> <h4><strong>Choose Option :</strong></h4> <div class="form-group"> <div class="checkbox-list"> <label class="checkbox-inline"> <input class="revision" type="checkbox"> Revision </label> <label class="checkbox-inline"> <input class="unable-to-solve" type="checkbox"> Unable to Soluion </label> <label class="checkbox-inline"> <input class="important" type="checkbox"> Important Question </label> </div> </div> </div> </div>';


			html += '</div>';
			html += '</div> </div> </div>';

			html += '';
			break;

		case "2":
			html = '';
			html += '<div  data-id="'+question.question_id+'" data-correct='+question.correct+' data-status='+question.question_status+' class="question row mrg-top40"> <div class="col-md-12">';
			html += '<div class="row"> <div class="col-md-12"> <div class="col-md-1">';

			// html += '<h4><strong>Qu.'+(num+1)+':</strong></h4>';
			if(question.correct == 1)
			{
				html += '<h4><a class="deleteOption btn btn-circle btn-icon-only green" style="margin-bottom: inherit;">'+(num+1)+'</a></h4>';
			}
			else
			{
				if(question.student_answer.length == 0)
				{
					html += '<h4><a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">'+(num+1)+'</a></h4>';
				}
				else
				{
					html += '<h4><a class="deleteOption btn btn-circle btn-icon-only red" style="margin-bottom: inherit;">'+(num+1)+'</a></h4>';
				}
			}
			html += '</div>';

			html += '<div class="col-md-11 borderccc">';
			html += '<div class="well mrg-bot20">  '+question.question+' </div>';

			html += '<div class="row option padd-left30">';
			$.each(question.options,  function(x, option) {
				html += '<div class="col-md-12">';
				html += '<div class="pull-left">'+option.option+'  </div> ';
					$.each(question.student_answer,  function(y, answered) {
						if (question.correct == 0 && answered.option_id == option.id) {
							html += '<div class="pull-left mrg-left5"> <p class="in-correct"><i class="fa fa-times"></i></p> </div>';
						};

						if (question.correct == 1 && answered.option_id == option.id && option.answer == 1) {
							html += '<div class="pull-left mrg-left5"> <p class="correct"><i class="fa fa-check"></i></p> </div>';
						};
					});

				html += '</div>';

			})
			html += '</div>';


			html += '<div class="row">	<div class="col-md-12">	';

			if(question.correct == 1) {
				html += '<h5 class="pull-right"> Score <span>'+question.marks+'/'+question.marks+'</span></h5>';
			} else {
				html += '<h5 class="pull-right"> Score <span>0/'+question.marks+'</span></h5>';
			}

			html += '</div>	</div>';

			html += '</div>	</div> </div>';

			html += '<div class="row"> <div class="col-md-1"></div>  <div class="col-md-11">';
			// html += '<h4><strong>Click to view Solution / Explanation</strong></h4>';
			html += '<div class="panel-group accordion" id="accordion'+(num+1)+'">';
			html += '<div class="panel panel-default">';
			html += '<div class="panel-heading">';
			html += '<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion'+(num+1)+'" href="#collapse_'+(num+1)+'">Click to view Solution / Explanation </a></h4>';
			html += '</div>';
			html += '<div id="collapse_'+(num+1)+'" class="panel-collapse collapse">';
			html += '<div class="panel-body">';
			html += '<div class="sol-expla-scrllbar">'	+ question.solution +	'</div>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			html += '</div>';


			// html += '<div class="borderccc sol-expla-scrllbar">'	+ question.solution +	'</div>';
			html += '<div class="row mrg-top20"> <div class="col-md-12 col-sm-12"> <h4><strong>Choose Option :</strong></h4> <div class="form-group"> <div class="checkbox-list"> <label class="checkbox-inline"> <input class="revision" type="checkbox"> Revision </label> <label class="checkbox-inline"> <input class="unable-to-solve" type="checkbox"> Unable to Soluion </label> <label class="checkbox-inline"> <input class="important" type="checkbox"> Important Question </label> </div> </div> </div> </div>';

			html += '</div>';
			html += '</div> </div> </div>';

			html += '';
			break;


		case "4":
			html = '';
			html += '<div  data-id="'+question.question_id+'" data-correct='+question.correct+' data-status='+question.question_status+' class="question row mrg-top40"> <div class="col-md-12">';
			html += '<div class="row"> <div class="col-md-12"> <div class="col-md-1">';

			// html += '<h4><strong>Qu.'+(num+1)+':</strong></h4>';
			if(question.correct == 1)
			{
				html += '<h4><a class="deleteOption btn btn-circle btn-icon-only green" style="margin-bottom: inherit;">'+(num+1)+'</a></h4>';
			}
			else
			{
				if(question.student_answer.length == 0)
				{
					html += '<h4><a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">'+(num+1)+'</a></h4>';
				}
				else
				{
					html += '<h4><a class="deleteOption btn btn-circle btn-icon-only red" style="margin-bottom: inherit;">'+(num+1)+'</a></h4>';
				}
			}
			html += '</div>';

			html += '<div class="col-md-11 borderccc">';
			html += '<div class="well mrg-bot20">  '+question.question+' </div>';

			html += '<div class="row option padd-left30">';
			$.each(question.options,  function(x, option) {
				html += '<div class="col-md-12">';
				html += '<div class="pull-left">'+option.option+'  </div> ';
					$.each(question.student_answer,  function(y, answered) {
						if (question.correct == 0 && answered.option_id == option.id) {
							html += '<div class="pull-left mrg-left5"> <p class="in-correct"><i class="fa fa-times"></i></p> </div>';
						};

						if (question.correct == 1 && answered.option_id == option.id && option.answer == 1) {
							html += '<div class="pull-left mrg-left5"> <p class="correct"><i class="fa fa-check"></i></p> </div>';
						};
					});

				html += '</div>';

			})
			html += '</div>';

			html += '<div class="row">	<div class="col-md-12">	';

			if(question.correct == 1) {
				html += '<h5 class="pull-right"> Score <span>'+question.marks+'/'+question.marks+'</span></h5>';
			} else {
				html += '<h5 class="pull-right"> Score <span>0/'+question.marks+'</span></h5>';
			}

			html += '</div>	</div>';

			html += '</div>	</div> </div>';

			html += '<div class="row"> <div class="col-md-1"></div>  <div class="col-md-11">';
			html += '<h4><strong>Click to view Solution / Explanation</strong></h4>';
			html += '<div class="borderccc sol-expla-scrllbar">'	+ question.solution +	'</div>';
			html += '<div class="row mrg-top20"> <div class="col-md-12 col-sm-12"> <h4><strong>Choose Option :</strong></h4> <div class="form-group"> <div class="checkbox-list"> <label class="checkbox-inline"> <input class="revision" type="checkbox"> Revision </label> <label class="checkbox-inline"> <input class="unable-to-solve" type="checkbox"> Unable to Soluion </label> <label class="checkbox-inline"> <input class="important" type="checkbox"> Important Question </label> </div> </div> </div> </div>';


			html += '</div>';
			html += '</div> </div> </div>';

			html += '';
			break;



		case "10":
			break;


		default:
			html = '';
			html += '<div data-id="'+question.question_id+'" data-correct='+question.correct+' data-status='+question.question_status+' class="question row mrg-top40"> <div class="col-md-12">';
			html += '<div class="row"> <div class="col-md-12"> <div class="col-md-1">';

			// html += '<h4><strong>Qu.'+(num+1)+':</strong></h4>';
			if(question.correct == 1)
			{
				html += '<h4><a class="deleteOption btn btn-circle btn-icon-only green" style="margin-bottom: inherit;">'+(num+1)+'</a></h4>';
			}
			else
			{
				if(question.student_answer.length == 0)
				{
					html += '<h4><a class="deleteOption btn btn-circle btn-icon-only btn-default" style="margin-bottom: inherit;">'+(num+1)+'</a></h4>';
				}
				else
				{
					html += '<h4><a class="deleteOption btn btn-circle btn-icon-only red" style="margin-bottom: inherit;">'+(num+1)+'</a></h4>';
				}
			}
			html += '</div>';

			html += '<div class="col-md-11 borderccc">';
			html += '<div class="well mrg-bot20">  '+question.question+' </div>';

			html += '<div class="row option padd-left30">';
			$.each(question.options,  function(x, option) {
				html += '<div class="col-md-12">';
				html += '<div class="pull-left">'+option.option+'  </div> ';
					$.each(question.student_answer,  function(y, answered) {
						if (question.correct == 0 && answered.option_id == option.id) {
							html += '<div class="pull-left mrg-left5"> <p class="in-correct"><i class="fa fa-times"></i></p> </div>';
						};

						if (question.correct == 1 && answered.option_id == option.id && option.answer == 1) {
							html += '<div class="pull-left mrg-left5"> <p class="correct"><i class="fa fa-check"></i></p> </div>';
						};
					});

				html += '</div>';

			})
			html += '</div>';


			html += '<div class="row">	<div class="col-md-12">	';

			if(question.correct == 1) {
				html += '<h5 class="pull-right"> Score <span>'+question.marks+'/'+question.marks+'</span></h5>';
			} else {
				html += '<h5 class="pull-right"> Score <span>0/'+question.marks+'</span></h5>';
			}

			html += '</div>	</div>';

			html += '</div>	</div> </div>';

			html += '<div class="row"> <div class="col-md-1"></div>  <div class="col-md-11">';
			html += '<h4><strong>Click to view Solution / Explanation</strong></h4>';
			html += '<div class="borderccc sol-expla-scrllbar">'	+ question.solution +	'</div>';
			html += '<div class="row mrg-top20"> <div class="col-md-12 col-sm-12"> <h4><strong>Choose Option :</strong></h4> <div class="form-group"> <div class="checkbox-list"> <label class="checkbox-inline"> <input class="revision" type="checkbox"> Revision </label> <label class="checkbox-inline"> <input class="unable-to-solve" type="checkbox"> Unable to Soluion </label> <label class="checkbox-inline"> <input class="important" type="checkbox"> Important Question </label> </div> </div> </div> </div>';


			html += '</div>';
			html += '</div> </div> </div>';

			html += '';
			break;
	}

	return html;
}


function markQuestionEvent() {
	$('.important').off('change')
	$('.important').on('change', function(e) {
		status = 0;

		if($(this).prop('checked')) {
			status = 1;
		}

		var req = {};
		req.action = "markQuestionEvent";
		req.markAs = "important";
		req.status = status;
		req.question_id = $(this).parents('.question').data('id');


		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			console.log(res);
			if ( res.status == 1 ) {
				toastr.success(res.message);
			} else{
				toastr.error(res.message);
			};
		});
	});

	$('.revision').off('change')
	$('.revision').on('change', function(e) {
		status = 0;

		if($(this).prop('checked')) {
			status = 1;
		}

		var req = {};
		req.action = "markQuestionEvent";
		req.markAs = "revision";
		req.status = status;
		req.question_id = $(this).parents('.question').data('id');

		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			console.log(res);
			if ( res.status == 1 ) {
				toastr.success(res.message);
			} else{
				toastr.error(res.message);
			};
		});
	});

	$('.unable-to-solve').off('change')
	$('.unable-to-solve').on('change', function(e) {
		status = 0;

		if($(this).prop('checked')) {
			status = 1;
		}

		var req = {};
		req.action = "markQuestionEvent";

		req.markAs = "unable_to_solve";
		req.status = status;
		req.question_id = $(this).parents('.question').data('id');

		$.ajax({
			type: "post",
			url: EndPoint,
			data: JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			console.log(res);
			if ( res.status == 1 ) {
				toastr.success(res.message);
			} else{
				toastr.error(res.message);
			};
		});
	});
}
