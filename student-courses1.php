<?php include 'Access-API.php'; ?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <!-- <li><a>Student</a></li> -->
            <li><a href="my-program.php">My Program</a></li>
            <li><a class="program-name"><!-- Program Name --></a></li>
            <li class="active"><a class="subject"><!-- Subject --></a></li>
            <!-- <li class="active">Chapter</li> -->
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          <div class="sidebar col-md-2 col-sm-3">
            <?php include('html/student/sidebar.php'); ?>
          </div>
          <!-- END SIDEBAR -->

          <!-- BEGIN CONTENT -->
          <div class="col-md-10 col-sm-9">
          	<!-- strat div for meter -->
          	<div class="row">
				<div class="col-md-12 col-sm-12">
					
				</div>
			</div>
			<!-- end div for meter -->
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2 class="program-name"> &nbsp; </h2>
				</div>
				<div class="col-md-6 col-sm-6"></div>
			</div>
			
			<!-- BEGIN DASHBOARD STATS -->
			<div class="row">
				<!-- Subjects -->
				<div class="col-md-12">
					
					<table id="papers" class="table table-striped table-hover table-bordered">
						<thead>
							<tr>
								<td>Test Name</td>
								<td>Test Type</td>
								<td>Start Time</td>
								<td>End Time</td>
								<td>Total Attempts</td>
								<td>Remaining Attempts</td>
								<td>Action</td>
								<td>Result</td>
							</tr>							
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>

				<!-- Test Types -->
				<!--<div class="col-md-3 VTP-right-side VTP-right-side-divider">
					<div class="row">
						 <div id="diagnostic" class="col-md-12 col-sm-12">
							<div data-category="1" class="dashboard-stat blue-madison">
								<div class="visual">
									<span class="count"> 0</span>
								</div>
								<div class="details">
									<div class="desc">
										<a class="view-test color-white" title="View Test">Diagnostic</a>
									</div>
								</div>
							</div>
						</div>
						<div id="full-syllabus-test" class="col-md-12 col-sm-12">
							<div data-category="4" class="dashboard-stat red-intense">
								<div class="visual">
									<span class="count"> 0</span>
								</div>
								<div class="details">
									<div class="desc">
										<a class="view-test color-white" title="View Test">Mock Test</a>
									</div>
								</div>
							</div>
						</div> 
						<div id="test-gentrator" class="col-md-12 col-sm-12">
							<div class="dashboard-stat red-intense">
								<div class="visual">
								</div>
								<div class="details">
									<div class="desc">
										<a class="color-white" data-toggle="modal" href="#test-generator" title="Generate Test">Test Generator</a>
									</div>
								</div>
							</div>
						</div> 

					</div>
				</div>-->
			</div>
			<!-- END DASHBOARD STATS -->
			
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
    <!-- END FOOTER -->
    <!-- Start modal-dialog -->
    <div class="modal fade" id="test-generator" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="test-genrator-form">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Test Generator</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
			                <label>Select Difficulty Level</label>
			                <select class="difficulties form-control" placeholder="Select Difficulty Level" multiple="multiple"></select>
			            </div>
			            <div class="form-group">
			                <label>Enter time duration ( In Minutes )</label>
			                <input type="number" class="time form-control" min="1" placeholder="Time Duration">
			            </div>
			            <div class="form-group">
			                <label>Select Chapters</label>
			                <select class="chapters form-control" placeholder="Select Chapters" multiple="multiple"></select>
			            </div>
			            <div class="form-group">
			                <label>Enter No of questions</label>
			                <input type="number" min="1" class="number-of-questions form-control" placeholder="Question Count">
			            </div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn blue">Generate Paper</button>
						<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
					</div>
				</form>				
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- / End modal-dialog -->
	</div>
	<!-- /.modal -->
	
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
	<?php include('html/student/js-files.php'); ?>

	<script src="assets/js/custom/student-courses1.js" type="text/javascript"></script>
	

    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>