<?php include 'Access-API.php'; ?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li><a href="my-program.php">My Program</a></li>            
            <li class="active"><a href="goal.php" class="program-name"><!-- Goal --></a></li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          <div class="sidebar col-md-2 col-sm-3">
            <?php include('html/student/sidebar.php'); ?>
          </div>
          <!-- END SIDEBAR -->

          <!-- BEGIN CONTENT -->
          <div class="col-md-10 col-sm-9">
          	<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2> 
						<span class="program-name"> Overall Progress </span>
					</h2>
				</div>
				<div class="col-md-6 col-sm-6"><h3> <a class="go-to-course pull-right"> Continue To Course </a></h3></div>
			</div>
          	<!-- strat first meter -->
          	<div class="row">
					<div class="col-md-4" id="meter1" style="height: 180px; "></div>
			<!-- end first meter -->
			<!-- strat second meter -->
					<div class="col-md-4" id="meter2" style="height: 180px; "></div>
			<!-- end second meter -->
			<!-- strat Third meter -->
					<div class="col-md-4" id="meter3" style="height: 180px; "></div>
			</div>
			<!-- end Third meter -->
			
			<div class="row">
				<div style="height: 100px;"></div>
			</div>

			<div class="row">
				<div class="progress" style="height: 50px;">
					<div id="progress-meter" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%;padding: 15px;">0% </div>
					<div style="margin-top: -50px; position: absolute; display: inline-block;"><img src="assets/frontend/pages/img/boy3.png" style="width: 40px;"></div>
					<div class="pull-right" style="margin-top: 50px;"> <img src="assets/frontend/pages/img/flag.png" style="width: 40px; margin-top: -70px; -ms-transform: rotate(22deg); -webkit-transform: rotate(22deg); transform: rotate(22deg);"></div>
					<div class="" style="margin-top: 50px;"> <img src="assets/frontend/pages/img/flag.png" style="width: 40px; margin-top: -70px; -ms-transform: rotate(22deg); -webkit-transform: rotate(22deg); transform: rotate(22deg);" id="cutoff-flag"></div>
				</div>
			</div>
			<div class="row">
				<div class="progress" style="height: 30px;">
				  <div class="progress-bar " role="progressbar" style="background:orange; width:20%">
				    Poor
				  </div>
				  <div class="progress-bar progress-bar-warning" role="progressbar" style="background:yellow;width:20%">
				    Fair
				  </div>
				  <div class="progress-bar blue" role="progressbar" style="width:40%">
				    Good
				  </div>
				  <div class="progress-bar green" role="progressbar" style="width:20%;">
				    Excellent
				  </div>
				</div>
	        </div>

          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
    <!-- END FOOTER -->
	
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
	<?php include('html/student/js-files.php'); ?>
	<script src="admin/assets/global/plugins/highcharts/highcharts.js" type="text/javascript"></script>
	<script src="admin/assets/global/plugins/highcharts/highcharts-more.js" type="text/javascript"></script>
	<script src="admin/assets/global/plugins/highcharts/modules/solid-gauge.js" type="text/javascript"></script>

	<script src="assets/js/custom/goal.js" type="text/javascript"></script>
	
	<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>