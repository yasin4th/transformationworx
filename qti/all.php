<form action="../vendor/fileUploads.php" method="post" enctype="multipart/form-data" id="zipform">
	<input type="file" name="zipfile"><br>
	<input type="hidden" name="action" value="qtiZipUpload" />
	<input type="submit" name="submit" value="Submit">
</form>
<div id="percentage" style="display: none;">
</div>
<script src="../assets/global/plugins/jquery.min.js"></script>
<script src="../assets/global/plugins/jquery.form.js"></script>
<script>
$(function() {
	$('form#zipform').ajaxForm({
        beforeSend: function(arr, $form, data) {
            console.log('starting');
        },
        uploadProgress: function(event, position, total, percentComplete) {
            //Progress bar
            $('#percentage').show();
            $('#percentage').html(percentComplete + '% done.') //update progressbar percent complete
        },
        success: function() {
            
        },
        complete: function(resp) {
        	$('#percentage').hide();
            res = $.parseJSON(resp.responseText);
            if(res.status == 1) {
                alert(res.message);
                window.location = 'show.php?total=' + res.total
            }
            else
                alert('An error occured. Please refresh the page and try again. Error Details: ' + res.message);
        },
        error: function() {
            console.log('Error');
        }
    });
});
</script>