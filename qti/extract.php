<?php
//zip extractor
if(isset($_POST['submit'])) {
	$ext = end(explode('.', $_FILES['zipfile']['name']));
	if($ext == 'zip') {
		$dest = time();
		$filename = $dest.'.'.$ext;
		move_uploaded_file($_FILES['zipfile']['tmp_name'], $filename);
		$zip = new ZipArchive();
		if($zip->open($filename)) {
			if(mkdir($dest)) {
				$zip->extractTo($dest);
				$zip->close();
				echo 'success';
			}
			else
				echo 'Can not create folder please check server settings';
		}
		else
			echo 'failed to open file';
	}
	else
		echo 'Invalid file type';
}
?>
<form action="extract.php" method="post" enctype="multipart/form-data">
	<input type="file" name="zipfile"><br>
	<input type="submit" name="submit" value="Submit">
</form>