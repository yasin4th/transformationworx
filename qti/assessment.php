<?php
$dest = '1433401412';
$xml = simplexml_load_file($dest.'/resources/assessment.xml');
$title = $xml['title'];
$description = $xml['description'];

foreach($xml->testPart[0]->assessmentSection[0]->assessmentItemRef as $question) {
	include_once 'import.php';
	importSingleQuestion($dest.'/resources/'.$question['href']);
}
?>