<?php
function importSingleQuestion($dest) {
	$xml = simplexml_load_file($dest);

	$answer = $xml->responseDeclaration[0]->correctResponse[0]->value->__toString();;

	$explain = null;
	foreach($xml->modalFeedback as $feedback) {
		if($feedback['showHide'] == 'hide')
			$explain = $feedback;
	}

	$body = null;
	$body = $xml->itemBody[0];

	$options = array();
	//echo count($body->choiceInteraction[0]->simpleChoice);
	foreach($body->choiceInteraction[0]->simpleChoice as $opt) {
		$obj = new stdClass();
		$obj->option = $opt->p[0]->__toString();
		$obj->correct = 0;
		if($opt['identifier']->__toString() == $answer) {
			$obj->correct = 1;
		}
		$options[] = $obj;
	}

	$question = $body->p[0];
	var_dump($question);
}
?>