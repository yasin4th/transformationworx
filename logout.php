<?php
   require_once 'config.inc.test.php';

	@session_start();
	if(isset($_SESSION['referrer']))
	{
		$url = $_SESSION['referrer'];
		@session_destroy();
		header('location: '.$url);
		die();
	}else if(isset($_SESSION['website']))
	{
		$url = $_SESSION['website'];
		@session_destroy();
		header('location: '.$url);
	}
	else if(!isset($_SESSION['mode']))
	{ 
		@session_destroy();
		header('location: '.URI1);
	}
	else
	{
		@session_destroy();
		header('location: '.URI1);
	}
?>

