<?php
  ob_start();


  if(isset($_GET['id']))
  {
    if(!empty($_GET['id']))
    {

      $id = $_GET['id'];
      $request = new stdClass();
      $request->action = 'filterPrograms';
      $request->test_program_id = $id;

      $request->option = 1;

      $json = json_encode($request);
      require('vendor/api.php');
      ob_clean();


      if($res->status == 0)
      {
        header('Location: ../404');
      }

    }
    else
    {
      header('Location: ../404');
    }

  }else{
    header('Location: ../404');
  }

    session_start();
    $email = strtolower($_SESSION['user']);
    $username = strtolower($_SESSION['username']);
    // $MERCHANT_KEY = "OygoFs";
    $MERCHANT_KEY = "7JmoczZp";

    // $SALT = "BV1QBwCv";
    $SALT = "1dD8Un7Oyo";

    // End point - change to https://secure.payu.in for LIVE mode https://test.payu.in
    $PAYU_BASE_URL = "https://secure.payu.in";

    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);

    //print_r($res);
    // $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
    $hash_string = "{$MERCHANT_KEY}|$txnid|{$res->data[0]['price']}|{$id}|{$username}|{$email}|||||||||||".$SALT;
    // echo "<br>".$hashSequence."<br>";

    // $hash_string = $MERCHANT_KEY."|".$txnid."|{$res->data[0]['price']}|".{$res->data[0]['name']}."| '.$USER->lastname."|".$USER->email."|".$course->id."|0|".$course->price."|0|||||||".$SALT;

    // var_dump($hash_string);
    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <base href="http://exampointer.com/">
  <!-- <base href="http://localhost/test_engine_new/"> -->
  <?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
  <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <!-- BEGIN CONTENT -->
        <div class="content-page">
          <div class="row mrg-top40">
            <div class="col-md-9 col-sm-9 exam-details-div">
              <div class="row">
                <div class="col-md-4 col-sm-4">
                  <img alt="" src="<?=$res->data[0]['image'];?>" class="img-responsive" id="plan-image">
                </div>
                <div class="col-md-8 col-sm-8">
                  <h2><span><?=$res->data[0]['name'];?></span> <span class="strem"></span></h2>
                  <?=$res->data[0]['description'];?>
                </div>
              </div>
              <div class="row margin-top-20">
                <div class="col-md-12">
                  <table class="table table-striped table-hover table-bordered"><thead><tr><th>Test Type</th><th>No. of Tests</th></tr></thead><tbody>
                  <tr>
                  <td>Mock Test</td>
                  <td>
                  <?php
                    $count = 0;
                    for ($i=0; $i < count($res->papers); $i++)
                    {
                      if($res->papers[$i]['test_program_id'] == $id && $res->papers[$i]['test_paper_type_id'] == '4'){
                        echo $res->papers[$i]['count'];
                      }
                    }
                  ?>
                  </td>

                  </tr>

                  </tbody></table>
                </div>
              </div>
            </div>

            <div class="col-md-3 col-sm-3">
              <div class="">
                <h5></h5>
                <div id="pricing">
                  <h3>Price : CAD <span><?=$res->data[0]['price'];?></span></h3>
                  <br/>
                </div>
                <a href='my-subscription-package.php' style="color:#fff" id="subscribe" data-id="<?=$res->data[0]['id'];?>" class="btn btn-primary btn-lg btn-block margin-bottom-10">Buy Now</a>
                <span id="info" class=""></span>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <form id="idform" action="<?php echo $action ?>" method="post" class="hide">

        <input type="hidden" name="firstname" value="<?=$username?>" />
        <input type="hidden" name="email" value="<?=$email?>" />
        <input type="hidden" name="key" value="<?=$MERCHANT_KEY ?>" />
        <input type="hidden" name="hash" value="<?=$hash ?>"/>
        <input type="hidden" name="txnid" value="<?=$txnid ?>" />
        <!-- <input type="hidden" name="discount" value="5" />
        <input type="hidden" name="udf3" value="" />
        <input type="hidden" name="udf2" value=""/>
        <input type="hidden" name="udf4" value="" /> -->
        <!-- <input type="hidden" name="ctype" value="" /> -->
        <input type="hidden" name="phone" value="8373916199" />
        <input type="hidden" name="amount" value="<?=$res->data[0]['price']?>" />
        <input type="hidden" name="productinfo" value="<?=$id?>" />
        <input type="hidden" name="surl" value="http://exampointer.com/payment-status.php" />
        <input type="hidden" name="furl" value="http://exampointer.com/payment-unsuccessful.php" />

      <!--
        <input name="address2" value=""/>
        <input name="city"  value=""/>
        <input name="zipcode" value=""/>
        <input name="state" value=""/>
        <input name="phone" value=""/> -->



        <input type="hidden" name="service_provider" value="payu_paisa"  />
        <input type="submit" id="pay_now" value="Pay Now" /></td>

    </form>


    <!-- BEGIN PRE-FOOTER -->
  <?php include('new-html/footer.php'); ?>
    <!-- END FOOTER -->
  <!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
    <script type="text/javascript">
        $(function() {

            $('#subscribe').click(function(e){
                e.preventDefault();
                var req = {};
                req.action = "ifBought";
                req.program_id = $(this).attr('data-id');
                $.ajax({
                 'type'  :   'post',
                 'url'   :   EndPoint,
                 'data'  :   JSON.stringify(req)
                }).done(function(res){
                    res = $.parseJSON(res);
                    if(res.status == 1)
                    {
                        toastr.success(res.message);
                    }
                    else if(res.status == 2)
                    {
                        toastr.warning(res.message);
                        window.location = "login";
                    }
                    else if(res.status == 0)
                    {
                        toastr.error(res.message);
                    }
                    else if(res.status == 3)
                    {
                        document.getElementById('idform').submit();
                    }

                });
            })

        })
    </script>
    <!-- <script type="text/javascript" src="assets/js/custom/exam-details1.js"></script> -->

</body>
<!-- END BODY -->
</html>