<?php
	require_once 'config.inc.test.php';
?>

<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
	<?php include HEADER; ?>

	<!-- Navigation END -->
	<div style="margin-top:150px;"></div>
	<div class="main">
	  <div class="container">
		<ul class="breadcrumb">
			<li><a href="<?=URL1?>">Home</a></li>
			<li class="active">Contact</li>
		</ul>
		<div class="row margin-bottom-40">
		  <!-- BEGIN CONTENT -->
		  <div class="col-md-12">
			<h1>Contact Us</h1>
			<div class="content-page">
			  <div class="row">
				<div class="col-md-12">
				  We are always excited to hear from you. Just fill out the form and we will get back to you shortly.
				</div>

				<div class="col-md-9 col-sm-9">
				
				  <!-- BEGIN FORM-->
				  <form role="form" id="contact-form">
					<div class="form-group"  >
					  <label for="contacts-name">Name</label>
					  <input type="text" class="form-control" id="name" name="name" required="required">
					</div>
					<div class="form-group">
					  <label for="contacts-email">Email</label>
					  <input type="email" class="form-control" id="email" name="email" required="required">
					</div>
					<div class="form-group">
					  <label for="contacts-message">Message</label>
					  <textarea class="form-control" rows="5" id="message" name="message" required="required" placeholder="How can we help?"></textarea>
					</div>
					<button type="submit" id="btnSend" name="btnSend" class="btn btn-primary"><i class="icon-ok"></i> Send</button>
					<button type="button" id ="cancel" class="btn btn-default">Cancel</button>
				  </form>
				  <!-- END FORM-->
				</div>

				<div class="col-md-3 col-sm-3 sidebar2">
				  <!-- <h2>Our Contacts</h2>   -->
				  <!-- <address>
					<strong>Office:</strong><br>
					D-44, First Floor, Sector 6, Noida,<br> India, 201301<br>
					<abbr title="Phone">Mob:</abbr> +91-8882715488, 8750115488
				  </address> -->

				  <address>
					<strong>Email</strong><br>
					<a href="mailto:support@transformationworx.com">support@transformationworx.com</a>
				  </address>
				  <ul class="social-icons margin-bottom-40">
					<li><a href="#" target="_blank" data-original-title="facebook" class="facebook"></a></li>
					<li><a href="# target="_blank" data-original-title="twitter" class="twitter"></a></li>
					<li><a href="#" target="_blank" data-original-title="linkedin" class="linkedin"></a></li>
				  </ul>

				</div>
			  </div>
			</div>
		  </div>
		  <!-- END CONTENT -->
		</div>
	  </div>
	  </div>


	<!-- BEGIN PRE-FOOTER -->
	<?php include FOOTER; ?>

	<!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
	<?php include('html/js-files.php'); ?>
	<!-- END PAGE LEVEL JAVASCRIPTS -->
<script src="assets/js/custom/contact-us.js" type="text/javascript"></script>

</body>
<!-- END BODY -->
</html>