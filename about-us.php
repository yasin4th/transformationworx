<?php
	require_once 'config.inc.test.php';

	@session_start();
	if(isset($_SESSION['role']) && $_SESSION['role'] != '4')
	{
		@session_destroy();
	}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
		<?php include('html/navigation.php'); ?>
		<!-- Navigation END -->

		<div class="main">
			<div class="container">
				<!-- BEGIN SIDEBAR & CONTENT -->
				<div class="row margin-bottom-40">
					<!-- BEGIN CONTENT -->
					<div class="col-md-12 col-sm-12">
						<h1>About Us</h1>
						<div class="content-page">
							<div class="row margin-bottom-30">
								<!-- BEGIN INFO BLOCK -->
								<div class="col-md-7">
									<h2 class="no-top-space">About ExamPointer</h2>
									<p>copyright &copy; exampointer.com</p>
									<!-- BEGIN LISTS -->
									<div class="row front-lists-v1">
										<div class="col-md-6">
											<ul class="list-unstyled margin-bottom-20">
												<li><i class="fa fa-check"></i> Officia deserunt molliti</li>
												<li><i class="fa fa-check"></i> Consectetur adipiscing </li>
												<li><i class="fa fa-check"></i> Deserunt fpicia</li>
											</ul>
										</div>
										<div class="col-md-6">
											<ul class="list-unstyled">
												<li><i class="fa fa-check"></i> Officia deserunt molliti</li>
												<li><i class="fa fa-check"></i> Consectetur adipiscing </li>
												<li><i class="fa fa-check"></i> Deserunt fpicia</li>
											</ul>
										</div>
									</div>
									<!-- END LISTS -->
								</div>
								<!-- END INFO BLOCK -->

								<!-- BEGIN CAROUSEL -->
								<div class="col-md-5 front-carousel">
									<img class="img-rounded"src="assets/frontend/pages/img/pics/rishikesh-rafting.jpg" alt="">
								</div>
								<!-- END CAROUSEL -->
							</div>
							<!-- END CONTENT -->
						</div>
					</div>
				</div>
			</div>
			<div class="team-block content text-center margin-bottom-50">
				<div class="container">
					<h2>Meet <strong>the team</strong></h2>
					<h4 class="margin-bottom-50">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</h4>
					<div class="row">
						<div class="col-md-3 item">
							<img src="assets/frontend/onepage/img/people/img6-large.jpg" alt="Marcus Doe" class="img-responsive">
							<h3>Marcus Doe</h3>
							<em>Founder</em>
							<p>Lorem ipsum dolor amet, tempor ut labore magna tempor dolore</p>
							<div class="tb-socio">
								<a href="javascript:void(0);" class="fa fa-facebook"></a>
								<a href="javascript:void(0);" class="fa fa-twitter"></a>
								<a href="javascript:void(0);" class="fa fa-google-plus"></a>
							</div>
						</div>
						<div class="col-md-3 item">
							<img src="assets/frontend/onepage/img/people/img7-large.jpg" alt="Elena Taylor" class="img-responsive">
							<h3>Elena Taylor</h3>
							<em>Designer</em>
							<p>Lorem ipsum dolor amet, tempor ut labore magna tempor dolore</p>
							<div class="tb-socio">
								<a href="javascript:void(0);" class="fa fa-facebook"></a>
								<a href="javascript:void(0);" class="fa fa-twitter"></a>
								<a href="javascript:void(0);" class="fa fa-google-plus"></a>
							</div>
						</div>
						<div class="col-md-3 item">
							<img src="assets/frontend/onepage/img/people/img8-large.jpg" alt="Cris Nilson" class="img-responsive">
							<h3>Cris Nilson</h3>
							<em>Developer</em>
							<p>Lorem ipsum dolor amet, tempor ut labore magna tempor dolore</p>
							<div class="tb-socio">
								<a href="javascript:void(0);" class="fa fa-facebook"></a>
								<a href="javascript:void(0);" class="fa fa-twitter"></a>
								<a href="javascript:void(0);" class="fa fa-google-plus"></a>
							</div>
						</div>
						<div class="col-md-3 item">
							<img src="assets/frontend/onepage/img/people/img8-large.jpg" alt="Cris Nilson" class="img-responsive">
							<h3>Cris Nilson</h3>
							<em>Developer</em>
							<p>Lorem ipsum dolor amet, tempor ut labore magna tempor dolore</p>
							<div class="tb-socio">
								<a href="javascript:void(0);" class="fa fa-facebook"></a>
								<a href="javascript:void(0);" class="fa fa-twitter"></a>
								<a href="javascript:void(0);" class="fa fa-google-plus"></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- BEGIN PRE-FOOTER -->
	<?php include('new-html/footer.php'); ?>
	<!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
	<?php include('html/js-files.php'); ?>
<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>