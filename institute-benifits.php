<?php
	include 'config.inc.test.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Effort Education</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Loading materilize -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="blade/main/css/style.css" rel="stylesheet">
	<link href="blade/main/css/materialize.min.css" rel="stylesheet">
	<link href="blade/main/css/materialdesignicons.min.css" rel="stylesheet">
	<link rel="shortcut icon" href="images/favicon.ico">
	<link href="blade/main/fa/css/font-awesome.css" rel="stylesheet">
</head>

<body>
	<!-- Preloader -->
<!-- <div id="preloader">
	<div id="status"></div>
</div> -->
<?php
	include HEADER;
?>

<!-- Body BEGIN -->
<body>
	<div class="services-block">
		<div class="container">
			<div class="row">
				<div class="col s12 m12 l12">
					<h2 class="center">Institute <strong class=" blue-text">Benifits</strong></h2>
					<p>This platform works perfectly on any device like mobile, tab, laptop, desktop, and on any platform
						 like i.e Windows, Mac or Linux. This revolutionary product is a must for every institution who wants to make their pupil high achievers.
					</p>
				</div>
			</div>
			<div class="row institute-benifits ">
					<div class="col s12 m6 l4">
						<div class="institute-box">
								<img src="blade/main/images/institute-2.jpg" class="responsive-img">
						</div>
							<h4>Content Quality</h4>
							<p> Created and reviewed by Experts and Top Faculty across the country,
							 Find just the right content for any exam vetted by educators.</p>
					</div>
					<div class="col s12 m6 l4">
						<div class="institute-box">
									<img src="blade/main/images/institute-3.jpg" class="responsive-img">
						</div>
							<h4>Content Quality</h4>
							<p> Created and reviewed by Experts and Top Faculty across the country,
							 Find just the right content for any exam vetted by educators.</p>
					</div>
					<div class="col s12 m6 l4">
						<div class="institute-box">
									<img src="blade/main/images/institute-2.jpg" class="responsive-img">
						</div>
							<h4>Content Quality</h4>
							<p> Created and reviewed by Experts and Top Faculty across the country,
							 Find just the right content for any exam vetted by educators.</p>
					</div>
					<div class="col s12 m6 l4">
						<div class="institute-box">
								<img src="blade/main/images/institute-2.jpg" class="responsive-img">
						</div>
							<h4>Content Quality</h4>
							<p> Created and reviewed by Experts and Top Faculty across the country,
							 Find just the right content for any exam vetted by educators.</p>
					</div>
					<div class="col s12 m6 l4">
						<div class="institute-box">
									<img src="blade/main/images/institute-3.jpg" class="responsive-img">
						</div>
							<h4>Content Quality</h4>
							<p> Created and reviewed by Experts and Top Faculty across the country,
							 Find just the right content for any exam vetted by educators.</p>
					</div>
					<div class="col s12 m6 l4">
						<div class="institute-box">
									<img src="blade/main/images/institute-2.jpg" class="responsive-img">
						</div>
							<h4>Content Quality</h4>
							<p> Created and reviewed by Experts and Top Faculty across the country,
							 Find just the right content for any exam vetted by educators.</p>
					</div>
			</div>
			<div class="row">
				<div class="col s12 center">
						<a class="waves-effect waves-light btn orange">Get Quote</a>
				</div>
			</div>
		</div>
	</div>

									<!-- END CONTENT -->


	<!--Footer-->
<?php
	include FOOTER;
?>



</body>
<!-- END BODY -->
</html>