<?php include 'Access-API.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li><a href="#">Student</a></li>
            <li class="active">My Question Bank</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          <div class="sidebar col-md-2 col-sm-3">
            <?php include('html/student/sidebar.php'); ?>
          </div>
          <!-- END SIDEBAR -->

          <!-- BEGIN CONTENT -->
          <div class="col-md-10 col-sm-9">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="pull-left">     
            <h2>My Question Bank</h2>
          </div>
  
        </div>
				<div class="col-md-6 col-sm-6">
          <div class="col-md-8 pull-right">
            <select id="filter" class="form-control">
              <option value="0"> Filter </option>
              <option value="1"> Revision </option>
              <option value="2"> Important </option>
              <option value="3"> Un Solved </option>
            </select>
          </div>    
        </div>
			</div>
			<div class="row margin-bottom-20">
				<table id="questions-table" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>S/N</th>
              <th>Type</th>
              <th>Class</th>
              <th> Subjects </th>
              <th>Question</th>
              <th>Action</th>
              <!-- <th></th>
              <th></th> -->
            </tr>
          </thead>
          <tbody>
          </tbody>    
        </table>
				
			</div>
			<div class="row">
				
		  </div>
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
    <!-- END FOOTER -->
	
	<!-- /modal -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
	 <?php include('html/student/js-files.php'); ?>

    <script src="assets/js/custom/question-bank.js" type="text/javascript"></script>

    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>