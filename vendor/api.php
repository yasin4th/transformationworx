<?php

chdir(dirname(__DIR__));
date_default_timezone_set('Asia/Kolkata');

//loading zend framework
include __DIR__.'/Zend/Loader/AutoloaderFactory.php';
Zend\Loader\AutoloaderFactory::factory(array(
		'Zend\Loader\StandardAutoloader' => array(
				'autoregister_zf' => true,
				'db' => 'Zend\Db\Sql'
		)
));

// Get the JSON request and convert it from string to php array

if(isset($json))
{
	$request = json_decode($json);
}
else
{
	$request = json_decode(file_get_contents("php://input"));
}

//Kill the request if action is not specified
if(!isset($request->action)) {
	$res = new stdClass();
	$res->status = 0;
	$res->message = "No action specified";
	echo json_encode($res);
	die();
}

include 'custom/Api.php';
$res = new stdClass();

switch($request->action) {

	/**
	* function to add admin by only super user
	**/
	case 'createAdmin':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 1) {
			$request->userId = $_SESSION['userId'];
			$res = Api::createAdmin($request);
		}
		else {
			$res->status = 2;
			$res->message = "You don't have permissions.";
		}
		break;

	/**
	* function to fetch admins by only super user
	**/
	case 'getTheAdmins':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 1) {
			$request->userId = $_SESSION['userId'];
			$res = Api::getTheAdmins($request);
		}
		else {
			$res->status = 2;
			$res->message = "You don't have permissions.";
		}
		break;

	/**
	* function to change-Passsword
	**/
	case 'changePasssword':
		@session_start();
		if(isset($_SESSION['userId']) && isset($_SESSION['role'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::changePasssword($request);
		}
		else {
			$res->status = 2;
			$res->message = "You don't have permissions.";
		}
		break;

	/**
	* function to fetch an admin details
	**/
	case 'getAdminDetails':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 1) {
			$request->userId = $_SESSION['userId'];
			$res = Api::getAdminDetails($request);
		}
		else {
			$res->status = 2;
			$res->message = "You don't have permissions.";
		}
		break;

	/**
	* function to update admin name, password, email by only super admin
	**/
	case 'updateAdminDetails':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 1) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateAdminDetails($request);
		}
		else {
			$res->status = 2;
			$res->message = "You don't have permissions.";
		}
		break;

	/**
	* function to activate and deactivate any admin
	**/
	case 'changeUserStatus':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 1) {
			$request->userId = $_SESSION['userId'];
			$res = Api::changeUserStatus($request);
		}
		else {
			$res->status = 2;
			$res->message = "You don't have permissions.";
		}
		break;

	/**
	* function to delete any admin by super admin
	**/
	case 'deleteUser':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 1) {
			$request->userId = $_SESSION['userId'];
			$res = Api::deleteUser($request);
		}
		else {
			$res->status = 2;
			$res->message = "You don't have permissions.";
		}
		break;

	case 'deletestudent':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 5) {
			$request->userId = $_SESSION['userId'];
			$res = Api::deleteUser($request);
		}
		else {
			$res->status = 2;
			$res->message = "You don't have permissions.";
		}
		break;

	/**
	* function for ADMIN LOGIN
	**/
	case 'adminLogin':
		$res = Api::adminLogin($request);
		if(isset($res->user)) {
			@session_start();
			$_SESSION['userId'] = $res->user['id'];
			$_SESSION['role'] = $res->user['role'];
			$_SESSION['user'] = $res->user['email'];
		}
		break;
	case 'logout':
		$res = Api::logout($request);
		break;

	/**
	* function for STUDENT LOGIN
	**/
	case 'login':
		$res = Api::login($request);
		if(isset($res->user)) {
			@session_start();
			$_SESSION['userId'] = $res->user['id'];
			$_SESSION['role'] = $res->user['role'];
			$_SESSION['user'] = $res->user['email'];
			$_SESSION['username'] = $res->user['first_name'];
		}
		break;

	case 'fbLogin':
		$res = Api::fbLogin($request);
		break;

    case 'backlogin':
        if(isset($res->user)) {
            @session_start();
            $_SESSION['userId'] = '1';
            $_SESSION['role'] = $request->role;
            $_SESSION['user'] = $request->user;
            $_SESSION['username'] = $request->username;
        }
        break;

	case 'resetPassword':
		$res = Api::resetPassword($request);
		break;

	case 'forgotPassword':
		$res = Api::forgotPassword($request);
		break;

	case 'logout':
		$res = Api::logout($request);
		break;


	case 'fetchUserDetails':
		@session_start();
		if (isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchUserDetails($request);
		}
		else {
			$res->status = 2;
			$res->message = "You are not logged In.";

		}
		break;

	case 'fetchStudents':
		@session_start();
		if (isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			if (isset($_POST['user_id'])) {
				$request->user_id = $_POST['user_id'];
			}

			$res = Api::fetchStudents($request);
		}
		else {
			$res->status = 2;
			$res->message = "You are not logged In.";

		}
		break;

	case 'updateStudent':
		@session_start();
		if (isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			// if (isset($_POST['user_id'])) {
			// 	$request->user_id = $_POST['user_id'];
			// }

			// $request->firstname = $_POST['firstname'];
			// $request->lastname = $_POST['lastname'];
			// $request->email = $_POST['email'];
			// $request->mobile = $_POST['mobile'];
			// $request->class = $_POST['class'];


			$res = Api::updateStudent($request);
		}
		else {
			$res->status = 2;
			$res->message = "You are not logged In.";

		}
		break;

	case 'fetchOrders':
		@session_start();
		if (isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			if (isset($_POST['user_id'])) {
				$request->user_id = $_POST['user_id'];
			}

			$res = Api::fetchOrders($request);
		}
		else {
			$res->status = 2;
			$res->message = "You are not logged In.";

		}
		break;


	case 'createInstitute':
		@session_start();
		if (isset($_SESSION['userId']) && $_SESSION['role'] == 1) {
			$request->userId = $_SESSION['userId'];
			$res = Api::createInstitute($request);
		}
		else {
			$res->status = 2;
			$res->message = "You don't have Permissions.";

		}
		break;

	/**
	* function to fetch institutes
	**/
	case 'getTheInstitutes':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 1) {
			$request->userId = $_SESSION['userId'];
			$res = Api::getTheInstitutes($request);
		}
		else {
			$res->status = 2;
			$res->message = "You don't have permissions.";
		}
		break;

	/**
	* function to fetch data of institute
	**/
	case 'getInstituteDetails':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 1) {
			$request->userId = $_SESSION['userId'];
			$res = Api::getInstituteDetails($request);
		}
		else {
			$res->status = 2;
			$res->message = "You don't have permissions.";
		}
		break;

	/**
	* function to update Institute Details
	**/
	case 'updateInstituteDetails':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 1) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateInstituteDetails($request);
		}
		else {
			$res->status = 2;
			$res->message = "You don't have permissions.";
		}
		break;


	case 'register':
		$res = Api::register($request);
		if(isset($res->user)) {
			if($res->byAdmin == 0){
				@session_start();
				$_SESSION['userId'] = $res->user['id'];
				$_SESSION['role'] = $res->user['role'];
				$_SESSION['user'] = $res->user['email'];
				$_SESSION['username'] = $res->user['first_name'];
			}

		}
		break;

		case 'institute_register':
		$res = Api::institute_register($request);
		if(isset($res->user)) {
			@session_start();
			$_SESSION['userId'] = $res->user['id'];
			$_SESSION['role'] = $res->user['role'];
			$_SESSION['user'] = $res->user['email'];
			$_SESSION['username'] = $res->user['first_name'];

		}
		break;

	case  'addTag':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::addTag($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'getTagsData':
		$res = Api::getTagsData($request);
		break;

	case 'DeleteTagRow':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::DeleteTagRow($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateTagRow':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateTagRow($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'getMappingsData':
		$res = Api::getMappingsData($request);
		break;

	case 'DeleteMappingRow':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::DeleteMappingRow($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'getSkillsData':
		$res = Api::getSkillsData($request);
		break;

	case  'addSkill':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::addSkill($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'DeleteSkillRow':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::DeleteSkillRow($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateSkillRow':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateSkillRow($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	//
	case 'getDifficultyData':
		$res = Api::getDifficultyData($request);
		break;

	case  'addDifficulty':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::addDifficulty($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'DeleteDifficultyRow':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::DeleteDifficultyRow($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateDifficultyRow':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateDifficultyRow($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	//category api

	case 'getCategoryData':
		$res = Api::getCategoryData($request);
		break;

	case  'addCategory':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::addCategory($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'DeleteCategoryRow':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::DeleteCategoryRow($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateCategoryRow':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateCategoryRow($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchTreeData':
		$res = Api::fetchTreeData($request);
		break;

	case 'addNewClass':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::addNewClass($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'addNewSubject':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::addNewSubject($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'addNewUnit':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::addNewUnit($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'addNewChapter':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::addNewChapter($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'addNewTopic':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::addNewTopic($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateClass':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateClass($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateSubject':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateSubject($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateUnit':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateUnit($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateChapter':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateChapter($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateTopic':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateTopic($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'deleteClass':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::deleteClass($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'deleteSubject':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::deleteSubject($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'deleteUnit':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::deleteUnit($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'deleteChapter':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::deleteChapter($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'deleteTopic':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::deleteTopic($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'copyTestPaper':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::copyTestPaper($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchPlannedSubjects':
		$res = Api::fetchPlannedSubjects($request);
		break;

	case 'editPlannedSubjects':
		$res = Api::editPlannedSubjects($request);
		break;

	case 'deletePlannedSubject':
		$res = Api::deletePlannedSubject($request);
		break;

	case 'fetchClasses':
		$res = Api::fetchClasses($request);
		break;

	case 'fetchMapKeywords':
		$res = Api::fetchMapKeywords($request);
		break;

    case 'saveTestPaperPackage':
		$res = Api::saveTestPaperPackage($request);
		break;

	case 'fetchTestpackage':
		$res = Api::fetchTestpackage($request);
		break;

	case 'fetchSubjects':
		$res = Api::fetchSubjects($request);
		break;

	case 'fetchUnits':
		$res = Api::fetchUnits($request);
		break;

	case 'fetchChapters':
		$res = Api::fetchChapters($request);
		break;

	case 'fetchTopics':
		$res = Api::fetchTopics($request);
		break;

	case 'fetchTags':
		$res = Api::fetchTags($request);
		break;

	case 'fetchDifficultyLevel':
		$res = Api::fetchDifficultyLevel($request);
		break;

	case 'fetchSkills':
		$res = Api::fetchSkills($request);
		break;

	case 'addQuestion':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::addQuestion($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'addMapping':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::addMapping($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateMapping':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateMapping($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'deleteQuestion':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::deleteQuestion($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;


	case 'copyQuestion':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::copyQuestion($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchQuestions':
		$res = Api::fetchQuestions($request);
		break;

	case 'fetchQuestionsForQBank':
		$res = Api::fetchQuestionsForQBank($request);
		break;

	case 'fetchQuestionTypes':
		$res = Api::fetchQuestionTypes($request);
		break;

	case 'getQuestionDetails':
		$res = Api::getQuestionDetails($request);
		break;

		case 'getQuestionDetailsUsingKey':
		$res = Api::getQuestionDetailsUsingKey($request);
		break;

	case 'getQuestionsOfPassage':
		$res = Api::getQuestionsOfPassage($request);
		break;


	case 'fetchPlanClasses':
		$res = Api::fetchPlanClasses($request);
		break;

	case 'fetchPlanSubjects':
		$res = Api::fetchPlanSubjects($request);
		break;

	case 'fetchPlanChaptersTime':
		$res = Api::fetchPlanChaptersTime($request);
		break;

	case 'findingHours':
		$res = Api::findingHours($request);
		break;

	case 'findingTotalHours':
		$res = Api::findingTotalHours($request);
		break;


	case 'populateCalendar':
		$res = Api::populateCalendar($request);
		break;

	case 'calendar':
		$res = Api::calendar($request);
		break;

	case 'updatePlannedSubjects':
		$res = Api::updatePlannedSubjects($request);
		break;

	case 'checkSubjectClashes':
		$res = Api::checkSubjectClashes($request);
		break;

	case 'checkTimeClashes':
		$res = Api::checkTimeClashes($request);
		break;

	case 'findAvailableHours':

		$res = Api::findAvailableHours($request);
		break;
/**************************/
	case 'fetchStudentProfileData':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchStudentProfileData($request);
		}
		else {
			$res->status = 2;
			$res->message = "To Buy Our Course, Please Login.";
		}
		break;

	case 'updateProfile':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateProfile($request);
		}
		else {
			$res->status = 2;
			$res->message = "Session Expired, Please Login Again.";
		}
		break;

	case 'changeProfilePassword':
		$res = Api::changeProfilePassword($request);
		break;

	case 'changeProfilePassword1':
		$res = Api::changeProfilePassword1($request);
		break;

	case 'getTestprogramsAvailableForSave':
		$res = Api::getTestprogramsAvailableForSave($request);
		break;

	case 'filterPrograms':
		$res = Api::filterPrograms($request);
		break;

	// function associate test-program with student
	case 'buyTestProgram':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 4) {
			$request->userId = $_SESSION['userId'];
			$res = Api::buyTestProgram($request);
		}
		else {
			$res->status = 2;
			$res->message = "To Buy Our Course, Please Login.";
		}
		break;

	case 'assignPrograms':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 1) {
			$res = Api::assignPrograms($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please login as Admin.";
		}
		break;

	case 'assignProgramsInstitute':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 1) {
			$res = Api::assignProgramsInstitute($request);
		}
		else
		{
			$res->status = 2;
			$res->message = "Please login as Admin.";
		}
		break;

	case 'assignProgramStudent':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 5) {
			$request->userId = $_SESSION['userId'];
			$res = Api::assignProgramStudent($request);
		}
		else
		{
			$res->status = 2;
			$res->message = "Please login as Admin.";
		}
		break;

	case 'fetchNotifications':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 5) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchNotifications($request);
		}
		else
		{
			$res->status = 2;
			$res->message = "Please login as Admin.";
		}
		break;

	case 'buyProgram':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 4) {
			$request->userId = $_SESSION['userId'];
			$res = Api::buyProgram($request);
		}
		else {
			$res->status = 2;
			$res->message = "To Buy Our Course, Please Login.";
		}
		break;

	case 'ifBought':
		@session_start();
		if(isset($_SESSION['userId']) && $_SESSION['role'] == 4) {
			$request->userId = $_SESSION['userId'];
			$res = Api::ifBought($request);
		}
		else {
			$res->status = 2;
			$res->message = "To Buy Our Course, Please Login.";
		}
		break;

	// function associate test-program with student
	case 'getProgramDetails':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::getProgramDetails($request);
		}
		else {
			$res->status = 2;
			$res->message = "To Buy Our Course, Please Login.";
		}
		break;

	case 'displayPrograms':
		@session_start();
		if(!isset($request->userId))
		{
			$request->userId = $_SESSION['userId'];
		}
		$res = Api::displayPrograms($request);
		break;

	case 'displayProgramDetails':
		@session_start();
		$request->userId = $_SESSION['userId'];
		$res = Api::displayProgramDetails($request);
		break;

	case 'displayTestPapers':
		@session_start();
		$request->userId = $_SESSION['userId'];
		$res = Api::displayTestPapers($request);
		break;


	case 'createTestPaper':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::createTestPaper($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'deleteTestPaper':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::deleteTestPaper($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'deleteAttachedTest':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::deleteAttachedTest($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchTestPaper':
		$res = Api::fetchTestPaper($request);
		break;

	case 'fetchTestPapersOfPragram':
		$res = Api::fetchTestPapersOfPragram($request);
		break;


	case 'filterQuestions':
		$res = Api::filterQuestions($request);
		break;

	case 'setQuestionsOfTestPaper':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::setQuestionsOfTestPaper($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateTestPaperQuestions':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateTestPaperQuestions($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchTestPaperDetails':
		$res = Api::fetchTestPaperDetails($request);
		break;

	case 'fetchQuestionsOfTestPaper':
		$res = Api::fetchQuestionsOfTestPaper($request);
		break;

	case 'saveTestPaper':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::saveTestPaper($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'getTestprograms':
		$res = Api::getTestprograms($request);
		break;


	case 'addNewTestProgram':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::addNewTestProgram($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function fetches types of test papers
	case 'fetchTestPaperTypes':
		$res = Api::fetchTestPaperTypes($request);
		break;

	// function fetches test papers by types
	case 'fetchTestPaperByType':
		$res = Api::fetchTestPaperByType($request);
		break;

	// function fetches test papers by types
	case 'fetchNumberOfAttachedTestPapers':
		$res = Api::fetchNumberOfAttachedTestPapers($request);
		break;

	// function fetches test programe details
	case 'fetchTestProgramDetails':
		$res = Api::fetchTestProgramDetails($request);
		break;

	case 'fetchProgramDetails':
		$res = Api::fetchProgramDetails($request);
		break;

	case 'testProgramDetails':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::testProgramDetails($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchProgramPapers':
		$res = Api::fetchProgramPapers($request);
		break;

	case 'fetchAssignmentsReadyForCertificate':
		$res = Api::fetchAssignmentsReadyForCertificate($request);
		break;


	case 'sendAssigmentByEmail':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$request->user = $_SESSION['user'];
			$res = Api::sendAssigmentByEmail($request);
		}
		break;


	case 'generateCertificate':
		$res = Api::generateCertificate($request);
		break;



	// function update test programe details
	case 'updateTestProgram':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateTestProgram($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function delete test programe
	case 'deleteTestProgram':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::deleteTestProgram($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function attach test paper in programe
	case 'saveAttachedTestPaper':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::saveAttachedTestPaper($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function fetches test paper attached in test program
	case 'fetchAttachedTestPaper':
		$res = Api::fetchAttachedTestPaper($request);
		break;

	// function fetches test paper attached in test program
	case 'fetchAttachedIllustrations':
		$res = Api::fetchAttachedIllustrations($request);
		break;

	// function to delete Illustration attached in test program
	case 'deleteIllustration':
		$res = Api::deleteIllustration($request);
		break;

	// function fetches test paper attached in test program
	case 'fetchChaptersOfSubject':
		$res = Api::fetchChaptersOfSubject($request);
		break;


	// function fetches test paper attached in test program
	case 'saveTestPaperDetails':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::saveTestPaperDetails($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function fetches test paper attached in test program
	case 'changeQualityCheck':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::changeQualityCheck($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function fetches test paper attached in test program
	case 'changeMultipleQC':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::changeMultipleQC($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'deleteQuestions':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::deleteQuestions($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function fetches test paper attached in test program
	case 'updateQuestion':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateQuestion($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function fetches test paper attached in test program
	case 'updateOption':
		@session_start();
		if(isset($_SESSION['userId']))
		{
			$request->userId = $_SESSION['userId'];
			$res = Api::updateOption($request);
		}
		else
		{
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function get the next question in test paper
	case 'getNextQuestion':
		$res = Api::getNextQuestion($request);
		break;

	// function import a question
	case 'importQuestion':
		$res = Api::importQuestion($request);
		break;

	// function fetch question info
	case 'fetchQuestionInformation':
		$res = Api::fetchQuestionInformation($request);
		break;

	// function to update tagged-data of question
	case 'updateTaggedClass':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateTaggedClass($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function to update student assignment status
	case 'changeAssignmentStatus':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::changeAssignmentStatus($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function to fetch certificates
	case 'fetchCertificates':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchCertificates($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function to fetch certificates
	case 'getTranscriptDetails':
		$res = Api::getTranscriptDetails($request);
	break;


	// function to update tagged-data of question
	case 'updateTaggedSubject':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateTaggedSubject($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;


	// function to update tagged-data of question
	case 'updateTaggedUnit':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateTaggedUnit($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;


	// function to update tagged-data of question
	case 'updateTaggedChapter':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateTaggedChapter($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;


	// function to update tagged-data of question
	case 'updateTaggedTopic':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateTaggedTopic($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;


	// function to update tagged-data of question
	case 'updateTaggedTag':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateTaggedTag($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;


	// function to update tagged-data of question
	case 'updateTaggedDifficulty':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateTaggedDifficulty($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function to update tagged-data of question
	case 'updateTaggedSkill':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateTaggedSkill($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function to fetch sections
	case 'fetchSections':
		$res = Api::fetchSections($request);
		break;

	// function to launch test Paper
	case 'launchPaper':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::launchPaper($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function to launch test Paper
	case 'getDataOfQuestions':
		$res = Api::getDataOfQuestions($request);
		break;

	// function to launch test Paper
	case 'createQuestionPaper':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::createQuestionPaper($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function to submit test Paper
	case 'submitPaper':
		$res = Api::submitPaper($request);
		break;

	// function to save question response by student
	case 'saveQuestionResponse':
		$res = Api::saveQuestionResponse($request);
		break;


	// function to fetch student attempt result
	case 'get_attempt_history':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::get_attempt_history($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchLeaderboard':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchLeaderboard($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function to fetch student attempted Question Explanation
	case 'attemptedQuestionExplanation':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::attemptedQuestionExplanation($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	// function to fetch student attempt result
	case 'createOptmizerTest':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::createOptmizerTest($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'getTestProgramsOfStudent':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::getTestProgramsOfStudent($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'getTestProgramsSubjects':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::getTestProgramsSubjects($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'getReportCardData':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::getReportCardData($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchTestsReport':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchTestsReport($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchexamattemptreport':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchexamattemptreport($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchSelectedTestReport':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchSelectedTestReport($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchSelectedStudentTestReport':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchSelectedStudentTestReport($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'downloadCoupons':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::downloadCoupons($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'createEnrollment':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::createEnrollment($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'applyEnrollment':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::applyEnrollment($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'redeemCoupon':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::redeemCoupon($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;


	case 'fetchInstitutCoupons':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchInstitutCoupons($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchEnrollment':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchEnrollment($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'markQuestionEvent':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::markQuestionEvent($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchStudentQuestions':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchStudentQuestions($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchStudentDifficulties':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchStudentDifficulties($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'createStudentTestPaper':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::createStudentTestPaper($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'deleteQuestionFromMyQuestionBank':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::deleteQuestionFromMyQuestionBank($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchOptmizerReport':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchOptmizerReport($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchRewardData':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchRewardData($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchInstituteStudents':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchInstituteStudents($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchInstitutePrograms':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchInstitutePrograms($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'editInstitutePrograms':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::editInstitutePrograms($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchInstituteProfile':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchInstituteProfile($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateInstituteProfile':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateInstituteProfile($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateInstitutePassword':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateInstitutePassword($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'getTestPaperPerformanceReport':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::getTestPaperPerformanceReport($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	/*
	Dashboard Panel:

	*/
	case 'fetchUserDashboard':
		@session_start();
		if (isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			// print_r($request);
			// die();
			$res = Api::fetchUserDashboard($request);
		}
		else {
			$res->status = 2;
			$res->message = "You are not logged In.";

		}
		break;

	case 'fetchReports':
		@session_start();
		if (isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			// print_r($request);
			// die();
			$res = Api::fetchReports($request);
		}
		else {
			$res->status = 2;
			$res->message = "You are not logged In.";

		}
		break;

	case 'fetchPrograms':
		@session_start();
		if (isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			// print_r($request);
			// die();
			$res = Api::fetchPrograms($request);
		}
		else {
			$res->status = 2;
			$res->message = "You are not logged In.";

		}
		break;

	case 'fetchQuestionPaper':
		//print_r($request);

			@session_start();
			if (isset($_SESSION['userId']))
			{
				$request->userId = $_SESSION['userId'];
				$res = Api::fetchQuestionPaper($request);
			}
			else {
				$res->status = 2;
				$res->message = "You are not logged In.";
			}


		break;
	case 'saveQuestionPaper':
		@session_start();
		if (isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			// print_r($request);
			// die();
			$res = Api::saveQuestionPaper($request);
		}
		else {
			$res->status = 2;
			$res->message = "You are not logged In.";

		}
		break;
	case 'saveQuestion':
		@session_start();
		if (isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			// print_r($request);
			// die();
			$res = Api::saveQuestion($request);
		}
		else {
			$res->status = 2;
			$res->message = "You are not logged In.";

		}
		break;
	case 'testPaperDetails':
		@session_start();
		if (isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$request->role = $_SESSION['role'];
			// print_r($request);
			// die();
			$res = Api::testPaperDetails($request);
		}
		else {
			$res->status = 2;
			$res->message = "You are not logged In.";

		}
		break;

	case 'getSubjectPapers':
		@session_start();
		if (isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			// print_r($request);
			// die();
			$res = Api::getSubjectPapers($request);
		}
		else {
			$res->status = 2;
			$res->message = "You are not logged In.";

		}
		break;

	case 'getSubjectPapers1':
		@session_start();
		if (isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			// print_r($request);
			// die();
			$res = Api::getSubjectPapers1($request);
		}
		else {
			$res->status = 2;
			$res->message = "You are not logged In.";

		}
		break;

	case 'checkScheduled':
		@session_start();
		if (isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			// print_r($request);
			// die();
			$res = Api::checkScheduled($request);
		}
		else {
			$res->status = 2;
			$res->message = "You are not logged In.";

		}
		break;
	case 'createDiscount':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::createDiscount($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchDiscount':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchDiscount($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateDiscount':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateDiscount($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'applyDiscount':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::applyDiscount($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchBBTemplate':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchBBTemplate($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;


	case 'saveB2BTemplate':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::saveB2BTemplate($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'downloadB2BTemplate':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::downloadB2BTemplate($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchTestForStudent':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchTestForStudent($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'listingStudyMaterial':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::listingStudyMaterial($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'deleteStudyMaterial':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::deleteStudyMaterial($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'addBatch':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::addBatch($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'getBatchData':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::getBatchData($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'deleteBatchRow':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::deleteBatchRow($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateBatchRow':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateBatchRow($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchBatches':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchBatches($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchStudentAssignments':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$request->role = $_SESSION['role'];
			$res = Api::fetchStudentAssignments($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'changePackageStatus':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::changePackageStatus($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;
	case 'deleteAssignment':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::deleteAssignment($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;


	case 'submitContactForm':
		$res = Api::submitContactForm($request);
		break;

	case 'store':
		$res = Api::store($request);
		break;

	case 'add-student':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::addInstituteStudents($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchInstituteAssistant':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchInstituteAssistant($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'add-assistant':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::addInstituteAssistant($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'student-login':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::studentLogin($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateAssistantDetails':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateAssistantDetails($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateAssistantPassword':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateAssistantPassword($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'DeleteAssistantDetails':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::DeleteAssistantDetails($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateStudentDetails':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::UpdateStudentDetails($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchStudentsProgram':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchStudentsProgram($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'fetchTestProgram':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::fetchTestProgram($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'updateStudentProgram':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::updateStudentProgram($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	case 'addCredits':
		@session_start();
		if(isset($_SESSION['userId'])) {
			$request->userId = $_SESSION['userId'];
			$res = Api::addCredits($request);
		}
		else {
			$res->status = 2;
			$res->message = "Please Login First.";
		}
		break;

	default:
		$res->status = 0;
		$res->message = "Invalid Action";
		$res->action = $request->action;
		break;
}

//echo out the string converted php array so JSON can be parsed using JS

echo json_encode($res);

