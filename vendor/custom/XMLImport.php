<?php
//<font    color="\#.{6}">Dir[^\/]+<\/font>
//<font    color="\#.{6}"> Dir[^\/]+<\/font>
//<font    color="\#.{6}">निर्देश.[^\/]+<\/font>:?
//निर्देश\.*\s*\(\d{1,}-\d{1,}\)\s*:
////INSERT INTO tagsofquestion(`question_id`,`tagged_id`,`tag_type`) SELECT id,'19','5' FROM `questions` where id>4216
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Expression;

	require_once 'Connection.php';

	class XMLImport extends Connection
	{
		public static $Q;
		public static $class_id;
		public static $subject_id;

		public function __construct()
		{
			XMLImport::$Q = array();
			$this->db = new DB();
			// parent::__construct();
		}
		public function exmlImport($req)
		{
			XMLImport::$class_id = $req->class_id;
			XMLImport::$subject_id = $req->subject_ids;

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$filename	=	$req->file['name'];
				$filetype	=	$req->file['type'];

				$excel = $filename;

				if (move_uploaded_file($req->file['tmp_name'], "../assets/qimages/".$excel))
				{
					$result->start = date('Y-m-d H:i:s');
					$filename = "../assets/qimages/".$excel;

					$xml = simplexml_load_file($filename,'SimpleXMLElement', LIBXML_NOCDATA);
					$topics = $xml->quiz->topics;

					$S = array();

					/**
					 * Save Test Paper
					 */
					$data = array(
						'name'   =>'UNTITLED'.date('H:i:s'),
						'type' 	 => 4,
						'class'	 => XMLImport::$class_id,
						'time' 	 => 60,
						'user_id'=> 1,
						'status' => 1,
						'total_attempts'=>1);
					$test_paper = $this->db->_insert('test_paper',$data);



					foreach ($topics->topic as $key => $value)
					{
						array_push($S, new Sections($value,$test_paper));
					}
				}

				$result->end = date('Y-m-d H:i:s');

				$result->status = 1;
				$result->message = "XML Import Successful.";
				return $result;
			}
			catch(Exception $e){
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
	}

	class Sections
	{
		protected $name;
		protected $test_paper;
		protected $questions;
		protected $eng;
		protected $hin;
		protected $xml;
		public function __construct($xml, $test_paper)
		{
			$this->xml = $xml;
			$this->test_paper = $test_paper;
			$this->questions = array();
			$this->db = new DB();
			$this->generateQuestions();
		}

		public function generateQuestions()
		{
			$MAX = 1;
			$NEG = 0.25;
			$this->name = $this->xml->attributes()->label;
			$this->eng = $this->xml->lang[0];
			$this->hin = $this->xml->lang[1];

			$parent = 0;
			$prevParaText = '';
			$child_count = array();
			$parent_ids = array();
			for ($i=0; $i < count($this->eng->questions->ques); $i++)
			{
				if($this->eng->questions->ques[$i]->attributes()->para == 'true')
				{

					if($this->eng->questions->ques[$i]->paraText.'' != $this->hin->questions->ques[$i]->paraText.'')
					{
						$paraText = $this->eng->questions->ques[$i]->paraText . '<br><br>' . $this->hin->questions->ques[$i]->paraText;
					}
					else
					{
						$paraText = $this->eng->questions->ques[$i]->paraText . '';
					}

					if($prevParaText == $paraText)
					{
						$count = array_pop($child_count);
						$count++;
						array_push($child_count, $count);
					}
					else
					{
						$data = array('question'=>$paraText,'type'=>11,'class_id'=>XMLImport::$class_id);
						$parent = $this->db->_insert('questions',$data);
						array_push(XMLImport::$Q, $parent);
						array_push($parent_ids, $parent);
						array_push($child_count, 1);
					}
					$prevParaText = $paraText;
				}
				else
				{
					$parent = 0;
					$prevParaText = '';
				}

				if($this->name == 'English')
				{
					array_push($this->questions, new Questions($this->eng->questions->ques[$i],$this->hin->questions->ques[$i],0, $parent));
				}
				else
				{
					array_push($this->questions, new Questions($this->eng->questions->ques[$i],$this->hin->questions->ques[$i],1, $parent));
				}
			}

			$this->xml = '';

			$data = array('name'=>$this->name,
				'total_question'=>count($this->questions),
				'cut_off_marks'=>0,
				'test_paper'=>$this->test_paper);
			$section_id = $this->db->_insert('section',$data);

			$data = array('section_id'=>$section_id,
				'class_id'  =>XMLImport::$class_id,
				'subject_id'=>XMLImport::$subject_id[0],
				'question_type'=>1,
				'max_marks' => $MAX,
				'neg_marks' => $NEG,
				'number_of_question' => count($this->questions) - array_sum($child_count));

			$label_id = $this->db->_insert('test_paper_labels',$data); //label of scqs

			$label_parent_id = array(); // labels of passage
			for ($i=0; $i < count($parent_ids); $i++) {
				$data = array('section_id'=>$section_id,
				'class_id'  =>XMLImport::$class_id,
				'subject_id'=>XMLImport::$subject_id[0],
				'question_type'=>11,
				'max_marks' => floatval($MAX*$child_count[$i]),
				'neg_marks' => $NEG,
				'number_of_question' => $child_count[$i]);

				array_push( $label_parent_id , $this->db->_insert('test_paper_labels',$data));
			}


			$data = array();
			$dataopt = array();
			for ($i=0; $i < count($this->questions); $i++)
			{
				if($this->questions[$i]->parent == 0)
				{
					array_push($data, $this->questions[$i]->storeInPaper($section_id, $label_id, $this->test_paper, $MAX, $NEG));
				}
				//var_dump($this->questions[$i]->parent);

				$options = $this->questions[$i]->Options;


				for ($j=0; $j < count($options); $j++) {

					array_push($dataopt, $options[$j]->getData());
				}

			}


            $data1 = array();
			for ($i = 0; $i < count($parent_ids); $i++)
			{
				array_push($data, array('question_id'=>$parent_ids[$i],
										'section_id'=>$section_id,
										'test_paper_id'=>$this->test_paper,
										'lable_id' => $label_parent_id[$i],
										'max_marks'=>floatval($MAX*$child_count[$i]),
										'neg_marks'=>$NEG));

			}


			$this->db->_minsert('test_paper_questions',$data);
			$this->db->_minsert('options',$dataopt);
		}
	}

	class Questions
	{
		public $id;
		public $question;
		public $Options;
		public $type;
		public $parent;
		public $description;
		public $xmleng;
		public $xmlhin;

		public function __construct($xmleng, $xmlhin, $merge, $parent)
		{
			$this->xmleng = $xmleng;
			$this->xmlhin = $xmlhin;
			$this->merge = $merge;
			$this->Options = array();
			$this->parent = $parent;
			$this->id = 0;
			$this->db = new DB();
			$this->generateOptions();
		}

		public function generateOptions()
		{
			if($this->merge)
			{
				$this->question = $this->xmleng->quesText . '<br><br>' . $this->xmlhin->quesText;
			}
			else
			{
				$this->question = $this->xmleng->quesText . '';
			}

			// $this->question = removeDir($this->question);

			switch($this->xmleng->attributes()->type)
			{
				case 'mcq':
					$this->type = 1;
					break;
				default:
					break;
			}

			/**
			 * Store questions
			 */

			$data = array('question' => $this->question,'type' => $this->type,'class_id' => XMLImport::$class_id,'parent_id'=> $this->parent);

			$id = $this->db->_insert('questions',$data);

			//$id = $this->_insert1('questions',$data);
			$this->id = $id;
			array_push(XMLImport::$Q, $id);

			// $data = array('question' => $this->question,'type' => $this->type,'class_id' => XMLImport::$class_id,'parent_id'=> $this->parent);
			// $id = $this->_insert('questions',$data);


			for ($i=0; $i < count($this->xmleng->option); $i++)
			{
				if($this->xmleng->attributes()->ans == $i)
				{
					$correct = 1;
				}
				else
				{
					$correct = 0;
				}
				array_push($this->Options, new Options($this->xmleng->option[$i],$this->xmlhin->option[$i], $correct,$this->merge, $id));
			}
			$this->xmleng = '';
			$this->xmlhin = '';
		}

		public function storeInPaper($section, $label, $test_paper, $MAX, $NEG)
		{
			$data = array('question_id'=>$this->id,
							'section_id'=>$section,
							'test_paper_id'=>$test_paper,
							'lable_id' => $label,
							'max_marks'=>$MAX,
							'neg_marks'=>$NEG);

			return $data;
			// $section_id = $this->db->_insert('test_paper_questions',$data);
		}
	}

	class Options
	{
		protected $title;
		protected $question_id;
		protected $answer;
		protected $correct;
		protected $question;

		public function __construct($xmleng, $xmlhin, $correct, $merge, $id)
		{
			if($merge && $xmleng.'' != $xmlhin.'')
			{
				$this->title = $xmleng . '/' . $xmlhin;
			}
			else
			{
				$this->title = $xmleng . '';
			}
			$this->correct = $correct;
			$this->question_id = $id;

			// $data = array('option'=>$this->title,'answer'=>$this->correct,'user_id'=>1,'question_id'=>$this->question_id);
			// $this->db = new DB();
			// $this->db->_insert('options',$data);
		}

		public function getData()
		{
			$data = array('option'=>$this->title,'answer'=>$this->correct,'user_id'=>1,'question_id'=>$this->question_id);
			return $data;
		}
	}


function removeDir($str) {


	$str = preg_replace('/size="\d+" /', ' ', $str);
	$str = preg_replace('/<\/img>/', '', $str);
	$str = preg_replace('/<u>/', '', $str);
	$str = preg_replace('/<font\s+color="#.{6}"></font>/', '', $str);
	$str = preg_replace('/<font\s*color="#000000">/', '', $str);
	$str = preg_replace('/Dir\.\s*\(\d+-\d+\)\s*;?:?/', '', $str);

	return $str;
}