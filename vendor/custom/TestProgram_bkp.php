<?php
    use Zend\Db\TableGateway\TableGateway;
    use Zend\Db\Sql\Sql;
    use Zend\Db\ResultSet\HydratingResultSet;
    use Zend\Db\Sql\Predicate\PredicateInterface;
    use Zend\Db\Sql\Expression;

    //including connection.php file to make connection with database
    require_once 'Connection.php';

    class TestProgram extends Connection{

        /**
        * function fetch all test programs
        *
        * @author Utkarsh Pandey
        * @date 06-06-2015
        * @param JSON
        * @return JSON status and data
        **/
        public function getTestprograms($data) {

            $result = new stdClass();

            try{

                $adapter = $this->adapter;
                // SELECT test_program.id, test_program.name, classes.name AS class, test_program.price, test_program.description, test_program.image, COUNT(test_papers_of_test_program.id) AS test_papers FROM test_program LEFT JOIN test_papers_of_test_program ON test_papers_of_test_program.test_program_id = test_program.id LEFT JOIN classes ON classes.id = test_program.class LEFT JOIN test_program_subjects ON test_program_subjects.test_program_id = test_program.id GROUP BY test_program.id

                $sql = new Sql($adapter);
                $select = $sql->select();
                $select->from('test_program')
                        ->join(array('test_papers_of_test_program' => 'test_papers_of_test_program'), 'test_papers_of_test_program.test_program_id = test_program.id', array('test_papers' => new Expression('COUNT(test_papers_of_test_program.id)')), 'left')
                        ->join(array('classes' => 'classes'), 'classes.id = test_program.class', array('class' => 'name'), 'left');
                
                $select->columns(array('id','name', 'class_id' => 'class', 'price', 'description','image'));
                // $select->limit($data->limit);
                // $select->offset($data->offset);
                $select->order('test_program.id DESC');
                $select->group('test_program.id');
                $where = new $select->where();
                $where->equalTo("test_program.deleted", 0);
                //$where->equalTo("test_papers_of_test_program.deleted", 0);

                if (isset($data->class)) {
                    $where->equalTo('test_program.class', $data->class);
                }
                if (isset($data->status)) {
                    $where->equalTo('test_program.status', $data->status);
                }

                // if (isset($data->subjects)) {
                //     $where->in('test_program_subjects.subject_id', $data->subjects);
                // }
                if (isset($data->name)) {
                    $where->like('test_program.name', "%$data->name%");
                }
                if (isset($data->limit)) {
                    $select->limit(50);
                    $select->offset(intval($data->limit));
                }
                $select->where($where);

                $selectString = $sql->getSqlStringForSqlObject($select);
                
                // $selectString = "SELECT test_program.id, test_program.name, classes.name AS class, test_program.price, test_program.description, test_program.image, COUNT(test_papers_of_test_program.id) AS test_papers FROM test_program LEFT JOIN test_papers_of_test_program ON test_papers_of_test_program.test_program_id = test_program.id LEFT JOIN classes ON classes.id = test_program.class GROUP BY test_program.id";
                //die($selectString);
                $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
                $res = $run->toArray();
                
                if(count($res))
                $result->status = 1;
                $result->data = $res;

                //count
                $select = $sql->select();
                $select->from('test_program')
                        ->join(array('test_papers_of_test_program' => 'test_papers_of_test_program'), 'test_papers_of_test_program.test_program_id = test_program.id', array(), 'left')
                        ->join(array('classes' => 'classes'), 'classes.id = test_program.class', array(), 'left');
                        //->join(array('test_program_subjects' => 'test_program_subjects'), 'test_program_subjects.test_program_id = test_program.id', array(), 'left');

                $select->columns(array('total_questions' => new Expression('COUNT(test_program.id)')));

                $select->order('test_program.id DESC');
                $select->group('test_program.id');
                $where = new $select->where();
                $where->equalTo("test_program.deleted", 0);

                if (isset($data->class)) {
                    $where->equalTo('test_program.class', $data->class);
                }
                if (isset($data->status)) {
                    $where->equalTo('test_program.status', $data->status);
                }
                // if (isset($data->subjects)) {
                //     $where->in('test_program_subjects.subject_id', $data->subjects);
                // }
                if (isset($data->name)) {
                    $where->like('test_program.name', "%$data->name%");
                }
                $select->where($where);

                $selectString = $sql->getSqlStringForSqlObject($select);
                // $selectString = "SELECT test_program.id, test_program.name, classes.name AS class, test_program.price, test_program.description, test_program.image, COUNT(test_papers_of_test_program.id) AS test_papers FROM test_program LEFT JOIN test_papers_of_test_program ON test_papers_of_test_program.test_program_id = test_program.id LEFT JOIN classes ON classes.id = test_program.class GROUP BY test_program.id";
                //die($selectString);
                $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                $total_questions = $run->toArray();
                $result->total_questions = count($total_questions);


                return $result;

            }catch(Exception $e ) {
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }


        /**
        * function to add test programs
        *
        * @author Utkarsh Pandey
        * @date 08-06-2015
        * @param JSON
        * @return JSON status and data
        **/
        public function addNewTestProgram($data) {

            $result = new stdClass();
            try {
                $adapter = $this->adapter;
                $insert = new TableGateway('test_program', $adapter, null, new HydratingResultSet());
                $insert->insert(array(
                    'name'  => $data->name,
                    'class' => $data->class,
                    'category' => $data->category,
                    'price' => $data->price,
                    'cut_off' => $data->cut_off,
                    'description'   => $data->description,
                    'created'   => time()
                    ));
                $id = $insert->getLastInsertValue();

                foreach ($data->subjects as $subject) {
                    $insertSubjects = new TableGateway('test_program_subjects', $adapter, null, new HydratingResultSet());
                    $insertSubjects->insert(array(
                        'test_program_id'   => $id,
                        'subject_id'    => $subject
                        ));
                }

                if($id)
                $result->id = $id;
                $result->status = 1;
                $result->message = "ok";
                return $result;
            }catch(Exception $e) {
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }

        
        /**
        *
        * @author Ravi Arya
        * @date 2-2-2016
        * @param $arr added
        * @return returns
        * @todo todo
        *
        **/

        public function updateTestProgram($data) {
            $result = new stdClass();
            try{
                $adapter = $this->adapter;
                $sql = new Sql($adapter);
                $update = $sql->update();
                    $update->table('test_program');
                    $arr = array( 'name'  => htmlentities($data->name),
                        'class' =>  $data->class,
                        'category' =>  $data->category,
                        'price' =>  $data->price,
                        'description'   =>  $data->description,
                        'user'  =>  1,
                        'status'    =>  $data->status,
                        'created'   =>  time()
                    );
                    if(isset($data->cut_off)){
                        $arr['cut_off'] =  $data->cut_off;
                    }
                    $update->set($arr);

                $update->where(array(
                    'id' => $data->id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();


                /*
                * Delete the sujects which already in test-program
                */
                $delete = $sql->delete('test_program_subjects');
                $where = new $delete->where();
                $where->equalTo('test_program_id',$data->id);
                $delete->where($where);

                $statement = $sql->getSqlStringForSqlObject($delete);
                $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

                if(!empty($data->subjects)) {
                    foreach($data->subjects as $subject) {
                        $insertSubject = new TableGateway('test_program_subjects', $adapter, null, new HydratingResultSet());
                        $insertSubject->insert(array(
                            'test_program_id' => $data->id,
                            'subject_id' => $subject
                        ));
                    }
                    $subject_id = $insertSubject->getLastInsertValue();
                }

                $result->status = 1;
                $result->message = "Successfully Saved.";
                return $result;;
            }
            catch(Exception $e)
            {
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }

        /**
        * function to delete test program
        *
        * @author Utkarsh Pandey
        * @date 06-08-2015
        * @param JSON
        * @return json with status and all data
        **/
        public function deleteTestProgram($data) 
        {
            $result = new stdClass();
            try{
                $adapter = $this->adapter;
                $sql = new Sql($adapter);
                $update = $sql->update();
                    $update->table('test_program');
                    $update->set(array(
                        'deleted'   =>  1,
                        'user'  =>  1,
                        'created'   =>  time()
                    ));
                    $update->where(array(
                        'id' => $data->id
                    ));
                    $statement = $sql->prepareStatementForSqlObject($update);
                    $statement->execute();
                    $result->status = 1;
                    $result->message = "Successfully Deleted.";
                    return $result;;
            }
            catch(Exception $e)
            {
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }

        /**
        * function to fetch test papers by type
        *
        * @author Utkarsh Pandey
        * @date 08-06-2015
        * @param JSON
        * @return json with status and all data
        **/
        public function fetchTestPaperByType($data)
        {
            $result = new stdClass();
            try
            {
                $adapter = $this->adapter;
                $sql = new Sql($adapter);
                // first we will get ids which have selected type and are already attached to the program
                $select = $sql->select();
                $select->from('test_papers_of_test_program');
                $select->where(array(
                    'test_paper_type_id'    =>  $data->type,
                    'test_program_id'   =>  $data->test_program,
                    'deleted'   =>  0
                ));
                $select->columns(array('id' => 'test_paper_id'));

                $selectString = $sql->getSqlStringForSqlObject($select);
                $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
                $notInIdArray = $run->toArray();
                if(!empty($notInIdArray))
                    $notInIds = array();
                foreach ($notInIdArray as $id) {
                    array_push($notInIds,$id["id"]);
                }


                // now we get those test papers which have select ed type and are not added in the test program
                $select = $sql->select();

                $select->from('test_paper');
                $where = new $select->where();
                $where->equalTo('type', $data->type);
                $where->equalTo('deleted',0);
                $where->equalTo('class',$data->class_id);
                if(!empty($data->subject_id)) {
                    $selectTestPapersBySubject = $sql->select();

                    $selectTestPapersBySubject->from('test_paper_subjects');
                    $selectTestPapersBySubject->where(array('subject_id' => $data->subject_id));
                    $selectString = $sql->getSqlStringForSqlObject($selectTestPapersBySubject);
                    $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
                    $IDS = $run->toArray();

                    $inIds = array();
                    foreach ($IDS as $id) {
                        array_push($inIds,$id["test_paper_id"]);
                    }

                    if(!empty($inIds))
                        $where->in('id',$inIds);
                }
                if(!empty($notInIds))
                    $where->notIn('id',$notInIds);
                $select->where($where);
                $select->columns(array('id','name'));

                $selectString = $sql->getSqlStringForSqlObject($select);
                $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
                $papers = $run->toArray();

                $result->test_papers = $papers;
                $result->status = 1;
                $result->message = "fetched.";
                return $result;
            }catch(Exception $e){
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }

        /**
        * Function to fetch test-paper counts attached in a test-program
        *
        * @author Utkarsh Pandey
        * @date 14-09-2015
        * @param JSON with test-program-id,parent_type and parent_id
        * @return JSON data(status, meaasge, counts)
        **/

        public function fetchNumberOfAttachedTestPapers($data) {
            $result = new stdClass();

            try{

                    $adapter = $this->adapter;

                    $sql = new Sql($adapter);
                    $select = $sql->select();

                    $select->from('test_papers_of_test_program');

                    $select->where(array('test_program_id'  =>  $data->test_program));
                    $select->where(array('parent_id'  =>  $data->parent_id));
                    $select->where(array('parent_type'  =>  $data->parent_type));
                    $select->where(array('deleted'  =>  0));

                    $selectString = $sql->getSqlStringForSqlObject($select);
                    $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                    $run = $run->toArray();

                    $result->data = new stdClass();
                    $result->data->diagnostic = 0;
                    $result->data->fullSyllabus = 0;
                    $result->data->scheduled = 0;
                    $result->data->chapter = 0;
                    $result->data->practice = 0;
                    $result->data->illustration = 0;

                    if ($data->parent_type == 0 || $data->parent_type == 1) {
                        foreach ($run as $testProgram) {
                            switch ($testProgram['test_paper_type_id']) {
                                case 1:
                                    $result->data->diagnostic++;
                                    break;

                                case 4:
                                    $result->data->fullSyllabus++;
                                    break;

                                case 5:
                                    $result->data->scheduled++;
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                    elseif ($data->parent_type == 2) {
                        foreach ($run as $testProgram) {
                            switch ($testProgram['test_paper_type_id']) {
                                case 3:
                                    $result->data->chapter++;
                                    break;

                                case 6:
                                    $result->data->practice++;
                                    break;

                                default:
                                    break;
                            }
                        }

                        $select = $sql->select();
                        $select->from('qcr_illustration');
                        $where = new $select->where();
                        $where->equalTo('test_program_id',$data->test_program);
                        $where->equalTo('chapter_id',$data->parent_id);
                        $where->equalTo('deleted', 0);
                        $select->where($where);
                        $select->columns(array('id'));
                        $statement = $sql->getSqlStringForSqlObject($select);
                        $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                        $illsutartions = $run->toArray();

                        $result->data->illustration = count($illsutartions);

                    }

                $result->status = 1;
                $result->message = "Successfully Fetched.";
                return $result;
            }
            catch(Exception $e ){
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }


        /**
        * Function to fetch chapters of subjects
        *
        * @author Utkarsh Pandey
        * @date 20-08-2015
        * @param JSON with subject-id
        * @return JSON data(status, meaasge,chapter_id,chapter_name)
        **/

        public function fetchChaptersOfSubject($data) {
            //return $data;
            $result = new stdClass();

            try{
                $adapter = $this->adapter;
                $sql = new Sql($adapter);
                // QUERY SELECT `units`.`id`  FROM `units` WHERE `deleted` = '0' AND subject_id IN($data->subject_id)
                $select = $sql->select();
                $select->from('units');
                $select->columns(array('id'));
                $where = new $select->where();
                $where->equalTo('deleted',0);
                $where->in('subject_id',$data->subject_id);
                $select->where($where);
                $selectString = $sql->getSqlStringForSqlObject($select);
                $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                $units = $run->toArray();

                $unit_ids = array();
                if(count($units) > 0) {
                    foreach ($units as $unit) {
                        array_push($unit_ids, $unit['id']);
                    }

                    $sql = new Sql($adapter);
                    // QUERY SELECT `chapters`.`id`, `chapters`.`name` FROM `chapters` WHERE `deleted` = '0' AND unit_id In($data->unit_id)
                    $select = $sql->select();
                    $select->from('chapters');
                    $select->columns(array('id','name'));
                    $where = new $select->where();
                    $where->equalTo('deleted',0);
                    $where->in('unit_id',$unit_ids);
                    $select->where($where);
                    $selectString = $sql->getSqlStringForSqlObject($select);
                    $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                    $run = $run->toArray();
                    $chapters = $run;
                    $result->chapters = $chapters;
                }
                $result->status = 1;
                $result->message = 'Operation Successful';
                return $result;
            }
            catch(Exception $e ){
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }


        }



        /**
        * function to fetch test program
        *
        * @author Utkarsh Pandey
        * @date 08-06-2015
        * @param JSON
        * @return json with status and all data
        **/
        public function fetchTestProgramDetails($data) {

            $result = new stdClass();

            try{
                $adapter = $this->adapter;
                $sql = new Sql($adapter);

                // SELECT `test_program`.`id` AS `id`, `test_program`.`name` AS `name`, `test_program`.`class` AS `class`, `test_program`.`price` AS `price`, `test_program`.`cut_off` AS `cut_off`, `test_program`.`description` AS `description`, `test_program`.`status` AS `status`, `test_program`.`image` AS `image`, GROUP_CONCAT(subject_id) as subjects FROM `test_program` LEFT JOIN test_program_subjects on test_program_subjects.test_program_id = test_program.id WHERE test_program.id = '18' AND `deleted` = '0'
                $select = $sql->select();

                $select->from('test_program')
                        ->join(array('test_program_subjects' => 'test_program_subjects'), 'test_program_subjects.test_program_id = test_program.id', array( 'subjects' => new Expression('GROUP_CONCAT(subject_id)')), 'left');
                $select->where(array(
                    'test_program.id'    =>  $data->test_program
                   
                ));
                $select->columns(array('id','name','class','price', 'cut_off', 'description','status','image','category'));

                $selectString = $sql->getSqlStringForSqlObject($select);

                $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
                $details = $run->toArray();

                /*
                * fetchinng class list
                */
                $select = $sql->select();
                $select->from('classes');
                $select->columns(array('id','name'));
                $select->where(array(
                    'deleted'   =>  0
                ));
                $selectString = $sql->getSqlStringForSqlObject($select);
                $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                $classesList = $run->toArray();


                if (!empty($details[0]['class'])) {

                    /*
                    * fetchinng subject list
                    */
                    $select = $sql->select();
                    $select->from('subjects');
                    $select->columns(array('id','name'));
                    $where = new $select->where();
                    $where->equalTo('deleted',0);
                    $where->equalTo('class_id',$details[0]['class']);
                    $select->where($where);
                    $selectString = $sql->getSqlStringForSqlObject($select);
                    // die($selectString);
                    $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                    $run = $run->toArray();
                    $subjectsList = $run;
                }
                $select = $sql->select();
                $select->from('chapters');
                $select->columns(array(
                    'chapter_id'    => 'id',
                    'chapter_name'  =>  'name',
                    'chapter_description'   =>  'description',
                    'unit_id'   =>  'unit_id',
                    'time'  =>  'time'
                ));
                $select->where(array(
                    'deleted'   =>  0
                ));
                $selectString = $sql->getSqlStringForSqlObject($select);
                $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                $run = $run->toArray();
                $chapters = $run;

                $sql = new Sql($adapter);
                $select = $sql->select();
                $select->from('category');
                $select->where(array(
                    'deleted'   =>  0
                ));
                // $select->limit($data->limit);
                // $select->offset($data->offset);
                $select->columns(array('id','name' => 'category', 'description',));
                
                $selectString = $sql->getSqlStringForSqlObject($select);
                $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
                $category = $run->toArray();
                
                $result->details = $details[0];
                $result->subjectsList = $subjectsList;
                $result->classesList = $classesList;
                $result->chapters = $chapters;
                $result->category = $category;

                $result->status = 1;
                $result->message = "Successfully Fetched.";
                return $result;
            }
            catch(Exception $e)
            {
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }


        /**
        * function to attach test Papers in test Program
        *
        * @author Utkarsh Pandey
        * @date 09-07-2015
        * @param json with all data
        * @return json with status,message and all data
        **/
        public function saveAttachedTestPaper($data) {
            $result = new stdClass();

            try{
                $adapter = $this->adapter;

                foreach ($data->testPapers as $id) {
                    $insert = new TableGateway('test_papers_of_test_program', $adapter, null, new HydratingResultSet());
                    $insert->insert(array(
                        'test_paper_id' => $id,
                        'test_program_id'   => $data->test_program,
                        'test_paper_type_id'    => $data->type,
                        'parent_id' => $data->parent_id,
                        'parent_type'   => $data->parent_type,
                        'created'   =>  time()
                        ));
                    $id = $insert->getLastInsertValue();
                }

                $result->details = $data;
                $result->status = 1;
                $result->message = "Successfully Updated.";
                return $result;
        }catch(Exception $e){
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }


        /**
        * function to fetch test Papers attached in test Program
        *
        * @author Utkarsh Pandey
        * @date 09-07-2015
        * @param json with all data
        * @return json with status,message and all data
        **/
        public function fetchAttachedTestPaper($data) {
            $result = new stdClass();
            try {

                $adapter = $this->adapter;
                $sql = new Sql($adapter);

                /*
                $statement = 'SELECT test_paper.id, test_paper.name, test_paper.type, test_paper.total_attempts, test_paper.start_time, test_paper.end_time, test_paper.Instructions, test_paper.status  FROM test_papers_of_test_program LEFT JOIN test_paper ON test_paper.id=test_papers_of_test_program.test_paper_id WHERE  test_papers_of_test_program.test_program_id ='.$data->test_program.' AND test_papers_of_test_program.test_paper_type_id ='.$data->type;
                if(!empty($data->parent_id) && ($data->parent_id != 0) && !empty($data->parent_type) && ($data->parent_type != 0))
                    $statement .= ' AND parent_id='.$data->parent_id.' AND parent_type='.$data->parent_type;
                // return $statement;*/
                $select = $sql->select();
                $select->from('test_program');
                $select->columns(array('name'));
                $where = new $select->where();
                $where->equalTo('id',$data->test_program);
                $select->where($where);
                $selectString = $sql->getSqlStringForSqlObject($select);
                $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                $run = $run->toArray();
                $result->test_program = $run[0]['name'];


                $select = $sql->select();

                $select->from(array('t_p' => 'test_paper'),array())
                       ->join(array('tp_tp' => 'test_papers_of_test_program'),
                        't_p.id = tp_tp.test_paper_id', array('row_id' => 'id'));

                $where = new $select->where();
                $where->equalTo('test_program_id',$data->test_program);
                $where->equalTo('test_paper_type_id',$data->type);
                $where->equalTo('tp_tp.deleted',0);
                $where->equalTo('t_p.deleted',0);

                if(!empty($data->parent_id) && ($data->parent_id != 0) && !empty($data->parent_type) && ($data->parent_type != 0)) {
                    $where->equalTo('parent_id',$data->parent_id);
                    $where->equalTo('parent_type',$data->parent_type);
                }

                $select->where($where);
                $select->columns(array('*'));

                $statement = $sql->getSqlStringForSqlObject($select);

                $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                $test_paper = $run->toArray();
                $result->status = 1;
                $result->message = "Operation Successful.";
                $result->data = $test_paper;
                return $result;
            }catch(Execption $e) {
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }


        /**
        * function to delete attached test Papers in test Program
        *
        * @author Utkarsh Pandey
        * @date 09-07-2015
        * @param json with all data
        * @return json with status,message and all data
        **/
        public function deleteAttachedTest($data) {
            $result = new stdClass();

            try{
                $adapter = $this->adapter;
                $sql = new Sql($adapter);

                $update = $sql->update();
                $update->table('test_papers_of_test_program');
                $update->set(array(
                    'deleted'  => 1,
                    'created'   =>  time()
                ));
                $update->where(array(
                    'id' => $data->id
                ));
                $statement = $sql->getSqlStringForSqlObject($update);
                $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                // $data = $run->toArray();

                // $result->details = $data;
                $result->status = 1;
                $result->message = "Test Deleted.";
                return $result;
        }catch(Exception $e){
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }


        /**
        * function change image of test program
        *
        * @author Utkarsh Pandey
        * @date 30-08-2015
        * @param JSON
        * @return json with status and all data
        **/
        public function change_test_program_image($data) {
            $result = new stdClass();
            try{
                $orgFileName = $data->image["name"];
                $ext = pathinfo($orgFileName, PATHINFO_EXTENSION);
                $newFileName =  $data->id.'.'.$ext;
                $destination = __DIR__."/../../assets/frontend/pages/img/test_program/".$newFileName;
                move_uploaded_file($data->image["tmp_name"], $destination);

                $adapter = $this->adapter;
                $sql = new Sql($adapter);
                $update = $sql->update();
                    $update->table('test_program');
                    $update->set(array(
                        'image'  => 'assets/frontend/pages/img/test_program/'.$newFileName,
                        'created'   =>  time()
                ));
                $update->where(array(
                    'id' => $data->id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

                $result->status = 1;
                $result->image = 'assets/frontend/pages/img/test_program/'.$newFileName;
                $result->message = "Image Successfully Updated.";
                return $result;;
            }catch(Exception $e){
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }

        /**
        * function add illustration file in test program
        *
        * @author Utkarsh Pandey
        * @date 31-08-2015
        * @param JSON
        * @return json with status and all data
        **/
        public function add_illustration_file($data) {
            $result = new stdClass();
            try{
                $orgFileName = $data->file["name"];
                // $ext = pathinfo($orgFileName, PATHINFO_EXTENSION);
                // $newFileName =  $data->program_id.'_'.$data->chapter_id.'.'.$ext;
                $destination = __DIR__."/../../assets/frontend/pages/test_program/illustration_file/".$orgFileName;
                move_uploaded_file($data->file["tmp_name"], $destination);
                $source = "assets/frontend/pages/test_program/illustration_file/".$orgFileName;
                $adapter = $this->adapter;

                $sql = new Sql($adapter);
                // INSERT INTO `qcr_illustration`(`test_program_id`, `chapter_id`, `source`) VALUES ($data->program_id, $data->chapter_id, $source)
                $insertIllustration = new TableGateway('qcr_illustration', $adapter, null, new HydratingResultSet());
                $insertIllustration->insert(array(
                    'name'  => $data->name,
                    'test_program_id'  => $data->program_id,
                    'chapter_id' => $data->chapter_id,
                    'source' => $source,
                    'created'   => time()
                    ));
                $id = $insertIllustration->getLastInsertValue();


                $result->status = 1;
                $result->message = "File Successfully Added.";
                return $result;;
            }catch(Exception $e){
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }



        /**
        * function to fetch attached illsutartions of test Program
        *
        * @author Utkarsh Pandey
        * @date 01-09-2015
        * @param json with all data
        * @return json with status,message and all data
        **/
        public function fetchAttachedIllustrations($data) {
            $result = new stdClass();
            try {

                $adapter = $this->adapter;
                $sql = new Sql($adapter);

                $select = $sql->select();
                $select->from('test_program');
                $select->columns(array('name'));
                $where = new $select->where();
                $where->equalTo('id',$data->test_program);
                $select->where($where);
                $selectString = $sql->getSqlStringForSqlObject($select);
                $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                $run = $run->toArray();
                $result->test_program = $run[0]['name'];


                $select = $sql->select();
                $select->from('qcr_illustration');
                $where = new $select->where();
                $where->equalTo('test_program_id',$data->test_program);
                $where->equalTo('chapter_id',$data->chapter);
                $where->equalTo('deleted', 0);
                $select->where($where);

                $select->columns(array('id', 'name', 'source', 'created'));

                $statement = $sql->getSqlStringForSqlObject($select);

                $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                $illsutartions = $run->toArray();
                $result->status = 1;
                $result->message = "Operation Successful.";
                $result->data = $illsutartions;
                return $result;
            }catch(Execption $e) {
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }


        /**
        * function to delete illsutartions of test Program
        *
        * @author Utkarsh Pandey
        * @date 03-09-2015
        * @param json with all data
        * @return json with status,message and all data
        **/
        public function deleteIllustration($data) {
            $result = new stdClass();
            try {

                $adapter = $this->adapter;

                $sql = new Sql($adapter);
                $update = $sql->update();
                    $update->table('qcr_illustration');
                    $update->set(array(
                        'deleted'  => 1,
                ));
                $update->where(array(
                    'id' => $data->id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

                $result->status = 1;
                $result->message = "Operation Successful.";
                return $result;
            }catch(Execption $e) {
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }

        public function fetchProgramDetails($data)
        {

            $result = new stdClass();
            try{
                $adapter = $this->adapter;
                // $sql = new Sql($adapter);

                //$statement = "SELECT test_program.id,test_program.name,test_program.image,test_program.cut_off,subjects.id,subjects.name AS subject FROM `test_program` Inner JOIN test_program_subjects as s ON s.test_program_id = test_program.id LEFT JOIN subjects ON subjects.id=s.subject_id where test_program.id = $data->program_id ";
                
                
                // $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                // $res = $run->toArray(); 
                
                $sql = new Sql($adapter);

                $select = $sql->select();

                $select->from(array('ps'=>'test_program_subjects'))
                        ->join(array('s'=>'subjects'),'s.id=ps.subject_id',array('name'=>'name'),'left')

                        ->join(array('tp'=>'test_papers_of_test_program'),'s.id=tp.parent_id',
                            array(
                                'diagnostic' => new Expression("SUM(IF(tp.test_paper_type_id=1 AND tp.parent_type=1 AND tp.deleted=0 AND tp.test_program_id= $data->program_id,1,0))"),
                                'practice' => new Expression("SUM(IF(tp.test_paper_type_id=6 AND tp.parent_type=1 AND tp.deleted=0 AND tp.test_program_id= $data->program_id,1,0))"),
                                'chapter' => new Expression("SUM(IF(tp.test_paper_type_id=3 AND tp.parent_type=1 AND tp.deleted=0 AND tp.test_program_id= $data->program_id,1,0))"),
                                'mock' => new Expression("SUM(IF(tp.test_paper_type_id=4 AND tp.parent_type=1 AND tp.deleted=0 AND tp.test_program_id= $data->program_id,1,0))"),
                                'scheduled' => new Expression("SUM(IF(tp.test_paper_type_id=5 AND tp.parent_type=1 AND tp.deleted=0 AND tp.test_program_id= $data->program_id,1,0))")
                                ),'left');
                $where = array('ps.test_program_id'=>$data->program_id);
                
                $select->where($where);
                $select->group('ps.subject_id');
               
                $selectString = $sql->getSqlStringForSqlObject($select);                 
                
                $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                $count = $run->toArray();

                $result->details = $count;
                $result->status = 1;
                $result->message = "Successfully Fetched.";
                return $result;
            }
            catch(Exception $e)
            {
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }

        public function fetchProgramPapers($data)
        {
        
            $result = new stdClass();
            try{
                $adapter = $this->adapter;
                $sql = new Sql($adapter);
                // first we will get ids which have selected type and are already attached to the program
                $select = $sql->select();
                $select->from('test_papers_of_test_program');
                $select->where(array(
                    'test_paper_type_id'    =>  $data->type,
                    'test_program_id'   =>  $data->test_program,
                    'deleted'   =>  0
                ));
                $select->columns(array('id' => 'test_paper_id'));

                $selectString = $sql->getSqlStringForSqlObject($select);
                
                $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
                $notInIdArray = $run->toArray();
                $notInIds = array();

                if(!empty($notInIdArray))
                {
                    $notInIds = array();
                }
                foreach ($notInIdArray as $id)
                {
                    array_push($notInIds, $id["id"]);
                }

                
                $sql = new Sql($adapter);
                
                $select = $sql->select();
                
                $select->from('test_paper_subjects')
                        ->join(array('paper'=>'test_paper'),'test_paper_subjects.test_paper_id = paper.id',array('name'=>'name'),'left');
                
                $where = new $select->where();
                $where->equalTo('subject_id', $data->subject_id);
                $where->equalTo('paper.type',$data->type);
                if(count($notInIds))
                {
                    $where->notIn('paper.id',$notInIds);                    
                }
                $select->where($where);
                
                $select->columns(array('id' => 'test_paper_id'));

                $selectString = $sql->getSqlStringForSqlObject($select);
                
                $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
                $papers = $run->toArray();             

                $result->test_papers = $papers;
                $result->status = 1;
                $result->message = "fetched.";
                return $result;
            }catch(Exception $e){
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }
        
    }
?>