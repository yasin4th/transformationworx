<?php 
	require __DIR__.'/../PHPMailer/PHPMailerAutoload.php';

	/**
	* 
	*/
	class Email {

		private $mail;

		function __construct() {
			$this->mail = new PHPMailer;

			// $this->mail->SMTPDebug = 3;
			$this->mail->isSMTP();
			// $this->mail->Host = 'smtp.gmail.com';
			// $this->mail->Port = 587;
			// $this->mail->SMTPSecure = 'tls';
			// $this->mail->SMTPAuth = true;
			$this->mail->IsHTML(true);


			$this->mail->Host = 'mail.smtp2go.com';
			$this->mail->Port = 2525;
			$this->mail->SMTPSecure = 'tls';
			$this->mail->SMTPAuth = true;

			$this->mail->Username = 'utkarsh.4thpointer@gmail.com';
			$this->mail->Password = 'meramailer';
			$this->mail->setFrom('no-reply@transformationworx.com');
		}


		private $header = '
<div style="margin:0;padding:0;height:100%!important;width:100%!important;background-color:#eee;color:#333;">
	<center>
		<table style="border-collapse:collapse;margin:0;padding:0;background-color:transparent;max-width:660px;">
			<tbody>
				<tr>
					<td style="height: 20px;"></td>
				</tr>
			</tbody>
		</table>
		<table style="border-collapse:collapse;margin:0;padding:0;background-color:#fff;max-width:660px;border:1px solid #ccc;">
			<tbody>
				<tr>
					<td style="width: 30px"></td>
					<td>
						<table>
							<tbody>
								<tr>
									<td style="height: 30px;"></td>
								</tr>
							</tbody>
						</table>
						<table>
							<tbody>
								<tr>
									<td style="width: 50px;"></td>
									<td style="text-align: center;">
										<a href="http://transformationworx.dltlabs.ca" title="TransformationWorx"><img src="http://transformationworx.dltlabs.ca/transformationworx-mail-logo.jpg" style="max-width: 100%;height: auto;display: block;"></a>
									</td>
									<td style="width: 50px;"></td>
								</tr>
							</tbody>
						</table>';
		private $footer = '
							<table>
								<tbody>
									<tr>
										<td style="height: 15px;">
										</td>
									</tr>
									<tr>
										<td>
											<p>If you have any question, feel free contact us.</p>
										</td>
									</tr>
									<tr>
										<td style="padding: 20px 0px;">
											<p>Sincerely,</p>
											<p>TransformationWorx Team</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>Contact us: dkhan@redmobileco.com | 416-909-6270 (9am-5pm, weekdays) | <a href="http://transformationworx.com/" target="_blank">transformationworx.com</a></p>
											<p>Click <a href="http://transformationworx.com/" target="_blank">here</a> to learn about other courses.</p>
										</td>
									</tr>
								</tbody>
							</table>
							<table>
								<tbody>
									<tr>
										<td style="height: 30px;"></td>
									</tr>
								</tbody>
							</table>
						</td>
						<td style="width: 30px"></td>
					</tr>
				</tbody>
			</table>
			<table style="border-collapse:collapse;margin:0;padding:0;background-color:transparent;max-width:660px;">
			<tbody>
				<tr>
					<td style="height: 20px;"></td>
				</tr>
			</tbody>
		</table>
	</center>
</div>';

		public function registration($name, $email, $password) {
			$result = new stdClass;
			$result->status = 1;
			$result->message = "Mail sent.";
			$body = '
				<table>
					<tbody>
						<tr>
							<td>
								<p>Dear '.$name.',</p>
							</td>
						</tr>
					</tbody>
				</table>
				<table>
					<tbody>
						<tr>
							<td>
								<p>Congratulations on completing the CIO Certified Blockchain Bootcamp with us on  25th May 2017. There are only two more steps to becoming a CIO Certified Blockchain Professional!</p>
								<ul style="list-style: none;">
									<li><span>&#10004;</span> Attend the CIO Certified Blockchain Bootcamp</li>
									<li><span>&#9744;</span> Upload the Capstone Project by 29th July 2017</li>
									<li><span>&#9744;</span> Complete the final Examination by 29th July 2017</li>
								</ul>
								<p>Here are the steps you need to follow:</p>
								<ol>
									<li>
										<p><b>Login:</b></p>
										<p>Click <a href="http://transformationworx.dltlabs.ca/" target="_blank">here</a> to login to your online account using the following credentials:</p>
										<p>Email: '.$email.'</p>
										<p>Password: '.$password.'</p>
									</li>
									<li>
										<p>Once you&rsquo;re logged in, please go to  &#10077;<b>My Profile</b>&#10078;</p>
										<ol style="list-style-type: lower-alpha;">
											<li><p>You can change your password</p></li>
											<li><p>Confirm that your full name is accurately recorded as this will be used to generate your official certificate, which will also be permanently notarized on Blockchain.</p></li>
											<li><p>Please also ensure that your complete contact information, including address is provided.</p></li>
										</ol>
									</li>
									<li>
										<p>Register and pay for the Exam and Capstone Project</p>
										<ol style="list-style-type: lower-alpha;">
											<li><p>Go to &#10077;Exams&#10078; tab</p></li>
											<li><p>Find <b>CIO Certified Blockchain Professional Exam</b> under the &#10077;Exams&#10078; tab and enter your unique <b>Enrollment Code</b> (will be sent via a separate email)</p></li>
											<li><p>Click on <b>Buy Now</b> and pay through PayPal (credit card or Paypal payment accepted)</p></li>
										</ol>
									</li>
									<li>
										<p>Take Exam</p>
										<ol style="list-style-type: lower-alpha;">
											<li><p>Click on <b>CIO Certified Blockchain Professional Exam</b> under &#10077;My Exams&#10078; tab</p></li>
											<li><p>Click on <b>Start Test</b> when you\'re ready to take the Exam (We highly recommend to take the <b>Mock Exam</b> first to familiarize yourself with the tool)</p></li>
										</ol>
									</li>
									<li>
										<p><b>Submit Capstone Project: </b>There are two ways to submit your Capstone Project.</p>
										<p>Upload online:</p>
										<ol>
											<li><p>Click on <b>CIO Certified Blockchain Professional Exam</b> under &#10077;My Exams&#10078; tab</p></li>
											<li>Choose your file to upload and select <b>&#10077;Upload&#10078;</b> - CAUTION: you will only be able to upload once</li>
										</ol>
										<p>Send via Email:</p>
										<ol>
											<li><p>Click on <b>CIO Certified Blockchain Professional Exam</b> under &#10077;My Exams&#10078; tab</p></li>
											<li><p>Send file to dkhan@redmobileco.com</p></li>
											<li><p>Click on &#10077;Sent by Email&#10078;</p></li>
										</ol>
									</li>
								</ol>
								<p>IMPORTANT INSTRUCTIONS ON TAKING THE EXAM:</p>
								<ol>
									<li><p>We highly recommend that you take the <b>Mock Exam</b> prior to the actual Exam. You can take the Mock Exam as many times as you wish. Please read the instructions <a target="_blank" href="http://transformationworx.dltlabs.ca/Exam%20Taking%20Instructions.pdf">Here</a> on how to use the <b>Exam Tool</b></p></li>
									<li>
										<p>Details for the <b>CIO Certified Blockchain Professional Exam:</b></p>
										<ul style="list-style-type: disc;">
											<li><p>The Exam is an open book Exam and include material from the main body of the course, the supporting material as well as main discussion points in the workshop</p></li>
											<li><p>You will have 60 minutes to complete all 40 questions.</p></li>
											<li><p>The first 30 questions are single choice and have only one correct answer.</p></li>
											<li><p>The last 10 questions are multiple-choice, and will have more than one correct answer and you have to select all of the correct answers to answer correctly. There are no partial marks.</p></li>
											<li><p>There is no negative marking for selecting the wrong answer.</p></li>
											<li><p>If you are unsure about a question, you can skip it and come back to it later.</p></li>
											<li><p>Exam can be taken online anytime, but must be completed by 29th July 2017 11:59 PM Eastern Time</p></li>
										</ul>
									</li>
									<li>
										<p>You will receive the result of the Exam as soon as you finish the Exam</p>
									</li>
								</ol>
								<p>IMPORTANT INSTRUCTIONS ON SUBMITTING THE CAPSTONE PROJECT:</p>
								<ol>
									<li><p>Please note you only have one chance to submit your Capstone Project. Please make sure you attach the right file when you either upload onto the portal or submit it by email</p></li>
									<li><p>You will receive the result of the Capstone Project within 10 days of submission. At that time, the instructor will send you some feedback regarding the Capstone Project.</p></li>
								</ol>
							</td>
						</tr>
					</tbody>
				</table>';
			// $this->mail->addAddress('utkarsh.4thpointer@gmail.com', 'Utkarsh Pandey');
			// $this->mail->addCC('sk06796@gmail.com','sur06796@gmail.com');
			// $this->mail->addBCC('sur06796@gmail.com');
			$this->mail->addAddress($email, $name);
			$this->mail->Subject = 'CIO Blockchain Professional Certification Exam and Capstone - Immediate Action Required';
			$this->mail->Body = $this->header .' '. $body .' '. $this->footer;

			if (!$this->mail->send()) {
				// die(var_dump($this->mail));
				$result->status = 0;
				$result->error = $this->mail;
				$result->message = "Mail sending failed.";
			};
			return $result;
		}

		public function firstFail($name, $email, $exam_name) {
			$today = time();
			$next_attempt_date = date('d-M-Y', $today + (60 * 60 * 24 * 6));
			$valid_till = date('d-M-Y', $today + (60 * 60 * 24 * 66));

			$result = new stdClass;
			$result->status = 1;
			$result->message = "Mail sent.";
			$body = '<table>
						<tbody>
							<tr>
								<td>
									<p>Dear '.$name.',</p>
								</td>
							</tr>
						</tbody>
					</table>
					<table>
						<tbody>
							<tr>
								<td>
									<p>Thank you for taking the <b>'.$exam_name.'</b>. We appreciate your efforts to prepare for the Exam as well as your interests in learning Blockchain. However, you did not meet the passing mark of 70%.</p>
									<p>The good news is that you can attempt the Exam one more time without any charge. We are happy to provide you with an extra 5 days of preparation. You can take the Exam anytime from '.$next_attempt_date.' until '.$valid_till.' .</p>
								</td>
							</tr>
						</tbody>
					</table>';
			// $this->mail->addAddress('utkarsh.4thpointer@gmail.com', 'Utkarsh Pandey');
			// $this->mail->addCC('sk06796@gmail.com','sur06796@gmail.com');
			// $this->mail->addBCC('sur06796@gmail.com');
			$this->mail->addAddress($email, $name);
			$this->mail->Subject = 'CIO Exam Results - Action Required';
			$this->mail->Body = $this->header .' '. $body .' '. $this->footer;
			$this->mail->IsHTML(true);

			if (!$this->mail->send()) {
				// die(var_dump($this->mail));
				$result->status = 0;
				$result->error = $this->mail;
				$result->message = "Mail sending failed.";
			};
			return $result;
		}

		public function assigmentFail($name, $email) {
			$today = time();
			$next_attempt_date = date('d-M-Y', $today + (60 * 60 * 24 * 6));
			$valid_till = date('d-M-Y', time() + (60 * 60 * 24 * 66));

			$result = new stdClass;
			$result->status = 1;
			$result->message = "Mail sent.";
			$body = '
					<table>
						<tbody>
							<tr>
								<td>
									<p>Dear '.$name.',</p>
								</td>
							</tr>
						</tbody>
					</table><table>
						<tbody>
							<tr>
								<td>
									<p>Thank you for submitting your Capstone Project. We appreciate your efforts to develop the Capstone Project as well as your interests in learning Blockchain. However, you did not pass the Capstone Project. </p>
									<p>You can find the instructor\'s feedback attached in the email / by logging into the portal. Please review the comments carefully.</p>
									<p>The good news is that you can resubmit the Capstone one more time without any charge. We are happy to provide you with an extra 5 days of preparation. You can resubmit the Capstone Project anytime from  '.$next_attempt_date.' until '.$valid_till.'.</p>
								</td>
							</tr>
						</tbody>
					</table>';
			// $this->mail->addAddress('utkarsh.4thpointer@gmail.com', 'Utkarsh Pandey');
			// $this->mail->addCC('sk06796@gmail.com','sur06796@gmail.com');
			// $this->mail->addBCC('sur06796@gmail.com');
			$this->mail->addAddress($email, $name);
			$this->mail->Subject = 'CIO Capstone Project Results - Action Required';
			$this->mail->Body = $this->header .' '. $body .' '. $this->footer;
			$this->mail->IsHTML(true);

			if (!$this->mail->send()) {
				// die(var_dump($this->mail));
				$result->status = 0;
				$result->error = $this->mail;
				$result->message = "Mail sending failed.";
			};
			return $result;
		}


		public function forgotPassword($name, $email, $code) {
			$url = 'http://transformationworx.dltlabs.ca/reset-password.php?token='.$code;

			$result = new stdClass;
			$result->status = 1;
			$result->message = "Mail sent.";
			$body = '
					<table>
						<tbody>
							<tr>
								<td>
									<p>Dear '.$name.',</p>
								</td>
							</tr>
						</tbody>
					</table><table>
						<tbody>
							<tr>
								<td>
									<p>
									We received a request to reset your password for your TransformationWorx account. We’re here to help! <br>
									</p>
									<p> Simply click on the link below to set a new password: <br> <a href="'.$url.'" target="_blank">'.$url.'</a> </p>

									<p>
									If you didn’t ask to reset your password, you can ignore this email. Your password will not be changed.<br>
									</p>	
								</td>
							</tr>
						</tbody>
					</table>';
			// $this->mail->addAddress('utkarsh.4thpointer@gmail.com', 'Utkarsh Pandey');
			// $this->mail->addCC('sk06796@gmail.com','sur06796@gmail.com');
			// $this->mail->addBCC('sur06796@gmail.com');
			$this->mail->addAddress($email, $name);
			$this->mail->Subject = 'TransformationWorx: Password Reset Code';
			$this->mail->Body = $this->header .' '. $body .' '. $this->footer;
			$this->mail->IsHTML(true);

			if (!$this->mail->send()) {
				// die(var_dump($this->mail));
				$result->status = 0;
				$result->error = $this->mail;
				$result->message = "Mail sending failed.";
			};
			return $result;
		}
	}




 ?>