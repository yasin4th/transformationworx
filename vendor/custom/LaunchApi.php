<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;


	require_once 'Connection.php';
	require_once 'Assignment.php';

	class LaunchApi extends Connection {

		/*--------------------------------------------------------------------------
		 *
		 *--------------------------------------------------------------------------

		 * @date 27-02-2016
		 * @param paper  =>  test paper id
		 * @return questions in the attempt
		 * @todo fetch answers of each question attempt
		 *
		 *--------------------------------------------------------------------------
		 */
		public function fetchQuestionPaper($req) {

			//program
			//testPaper
			//userId
			date_default_timezone_set('Asia/Kolkata');
			$result = new stdClass();
			$result->time_began = date("Y-m-d H:i:s");
			try{
				$adapter = $this->adapter;

				if($this->ifBought($req))
				{
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from("test_paper");
					$select->where(array("id" => $req->testPaper, "deleted" =>  0, "status" =>  1));
					//$select->columns(array("id","name","time",'type','start_time','end_time','total_attempts'));
					$statement = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$test_paper_details = $run->toArray();

					if(count($test_paper_details))
					{
						//update attempt as submitted if started but not submitted by user  but not in case of practice test
						if($test_paper_details[0]['type'] != '6')
						{
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from("test_paper_attempts");
							$select->where(array(
								'student_id'    =>  $req->userId,
								'program_id'    =>  $req->program,
								'test_paper_id' =>  $req->testPaper,
								'complete'      => '0'
							));
							$selectString = $sql->getSqlStringForSqlObject($select);

							$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$completes = $run->toArray();

							$result->completes = $completes;
							$time = $test_paper_details[0]['time'];
							$resume = 0;

							//check and submit test
							foreach ($completes as $paper)
							{
								$now1 = $paper['start_time'];
								$now = time();
								$start_time = date_create_from_format('U', $now);
								if(($now - $now1) > $time*60)
								{
									$sql = new Sql($adapter);
									$update = $sql->update();
									$update->table('test_paper_attempts');
									$update->set(array(
										'complete' => '1',
										'end_time' => $now
									));
									$update->where(array('id' => $paper['id']));
									$statement = $sql->getSqlStringForSqlObject($update);
									$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
								}
							}
						}

						if($test_paper_details[0]['type'] == '6'){
							/*--------------------------------------------------------------------------
							 * Practice Test
							 *--------------------------------------------------------------------------
							 */
							$result->paper_attempt_id = "0";
							$result->questions = $this->getQ($req,'0',$test_paper_details[0]['type'],0);
							//function getQ($request,$attempt,$type,$resume)

						} else {
							/*--------------------------------------------------------------------------
							 * Other Tests (!Practice Test)
							 *      1. $resume state = 1
							 *      2. $resume state = 0  => complete = 0
							 *      3. $time correction for scheduled test
							 *--------------------------------------------------------------------------
							 */

							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from('test_paper_attempts');
							$where = new $select->where();
							$where->equalTo('student_id',$req->userId);
							$where->equalTo('test_paper_id',$req->testPaper);
							$where->equalTo('program_id',$req->program);
							$where->equalTo('complete', 0);
							$select->where($where);
							$select->columns(array('id','start_time','end_time'));
							$statement = $sql->getSqlStringForSqlObject($select);

							$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
							$selectResumePaperAttempt = $run->toArray();

							if(count($selectResumePaperAttempt) == 0) {
								/*--------------------------------------------------------------------------
								 * Other Tests (!Practice Test)
								 *      2. $resume state = 0  => complete = 0
								 *      3. $time correction for scheduled test
								 *--------------------------------------------------------------------------
								 */

								if($test_paper_details[0]['type'] == '5')
								{

									$end_time = date_create_from_format('d F Y - H:i', $test_paper_details[0]['end_time']);

									$end_time = intval(date_format($end_time, 'U'));

									if(time() > $end_time)
									{
										$test_paper_details[0]['time'] = 0;
									}
									else if(($end_time - time()) < $test_paper_details[0]['time'] * 60)
									{
										$test_paper_details[0]['time'] = ($end_time-time())/60;
									}
								}

								$insert_test_paper_attempt = new TableGateway('test_paper_attempts', $adapter, null, new HydratingResultSet());

								$insertdata = array(
									'test_paper_id' => $req->testPaper,
									'program_id'    => $req->program,
									'student_id'    => $req->userId,
									'start_time'    => time()
								);
								@session_start();
								if(isset($_SESSION['assistant'])){
									$insertdata['inlab'] = 1;
								}

								$insert_test_paper_attempt->insert($insertdata);

								$attempt_id = $insert_test_paper_attempt->getLastInsertValue();
								$result->paper_attempt_id = $attempt_id;

								$result->questions = $this->getQ($req, $attempt_id, $test_paper_details[0]['type'], 0);

							} else if(count($selectResumePaperAttempt) == 1) {
								/*--------------------------------------------------------------------------
								 * Other Tests (!Practice Test)
								 *      1. $resume state = 1                                 *
								 *      3. $time correction for scheduled test
								 *--------------------------------------------------------------------------
								 */

								if($test_paper_details[0]['type'] == '5')
								{

									$end_time = date_create_from_format('d F Y - H:i', $test_paper_details[0]['end_time']);
									$end_time = intval(date_format($end_time, 'U'));

									if(time() > $end_time)
									{
										$test_paper_details[0]['time'] = 0;

									}
									else if(($end_time - time()) < $test_paper_details[0]['time'] * 60)
									{

										$test_paper_details[0]['time'] = ($end_time-time()) / 60;
									}
									else
									{
										$test_paper_details[0]['time'] = ($test_paper_details[0]['time'] * 60 -  (time() - $selectResumePaperAttempt[0]['start_time'])) / 60;
									}
								}
								else
								{

									$test_paper_details[0]['time'] = ($test_paper_details[0]['time'] * 60 -  (time() - $selectResumePaperAttempt[0]['start_time']))/60;
								}

								$sql = new Sql($adapter);
								$select = $sql->select();
								$select->from('test_paper_attempts');
								$where = new $select->where();
								$where->equalTo('test_paper_id',$req->testPaper);
								$where->equalTo('program_id',$req->program);
								$where->equalTo('student_id',$req->userId);
								$select->where($where);
								$select->columns(array('count' => new Expression('COUNT(*)')));
								$statement = $sql->getSqlStringForSqlObject($select);
								$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
								$res = $run->toArray();
								$attempted = count($res);
								if($attempted >= $test_paper_details[0]['total_attempts'])
								{
									//---------------------cant start--------------------
									/*$result->status = '0';
									$result->message = "Attempts exceeded.";
									$result->details = $test_paper_details;
									return $result;*/
									$result->questions = $this->getQ($req,$selectResumePaperAttempt[0]['id'],$test_paper_details[0]['type'],1);
									$result->paper_attempt_id = $selectResumePaperAttempt[0]['id'];
								}
								else
								{
									//attempts remaining
									/*--------------------------------------------------------------------------
									 * Other Tests (!Practice Test)
									 *      1. $resume state = 1
									 *      3. $time correction for scheduled test
									 *--------------------------------------------------------------------------
									 */
									$result->questions = $this->getQ($req,$selectResumePaperAttempt[0]['id'],$test_paper_details[0]['type'],1);
									$result->paper_attempt_id = $selectResumePaperAttempt[0]['id'];
								}
							}
						}
					}
					else
					{
						$result->status = 0;
						$result->message = "Invalid Test Paper Id.";
						return $result;
					}
				}
				else
				{
					$result->status = 0;
					$result->message = "Not Bought.";
					return $result;
				}
				$result->test_paper_details=$test_paper_details;
				$result->sections = $this->getSections($req);
				$result->status = 1;
				$result->message = "Question Paper Fetched.";
				$result->time_finished = date("Y-m-d H:i:s");

				return $result;
			}
			catch(Exception $e)
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}
		}

		public function ifBought($request)
		{
			if(isset($request->demo)){
				return true;
			}
			if(!isset($request->testPaper) || !isset($request->program)){
				return false;
			}
			try
			{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('test_papers_of_test_program');
				$select->where(array('test_paper_id' => $request->testPaper, 'test_program_id' => $request->program));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();

				if(count($rows))
				{

					$program = $request->program;
					$userId = $request->userId;

					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('student_program');
					$select->where(array('user_id' => $request->userId,'test_program_id'=>$program ));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$res = $run->toArray();
					if(count($res)){
						return true;
					}

					return false;
				}
				return false;
			}
			catch(Exception $e)
			{
				echo "Something went wrong".$e;
				return false;
			}
		}


		public function getQ($request, $attempt, $type, $resume)
		{
			try{

				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('section');

				$select->from(array('s' => 'section'))
						->join(array('l'=>'test_paper_labels'),
								's.id = l.section_id',
								array(
								'lable_id'           => 'id',
								'number_of_question' => 'number_of_question',
								'question_type'      => 'question_type'),'right');

				$where = new $select->where();
				$where->equalTo('s.test_paper',$request->testPaper);
				$where->equalTo('l.question_type','11');
				$select->where($where);
				$select->columns(array('id','name','total_question'));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$sections = $run->toArray();

				/*
				* fetching saved question of test paper
				*/

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('test_paper_questions');

				$where = new $select->where();
				$where->equalTo('test_paper_id',$request->testPaper);
				$select->where($where);
				$select->columns(array('section_id','lable_id','question_id', 'max_marks', 'neg_marks'));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$questions = $run->toArray();
				$i = 0;
				$total_marks = 0;
				$Q = array();
				foreach ($questions as $key => $question)
				{
					$total_marks = $total_marks + intval($question['max_marks']);
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('questions');
					$where = new $select->where();
					$where->equalTo('delete',0);
					// $where->equalTo('quality_check',0);
					$where->equalTo('id',$question["question_id"]);
					$select->where($where);
					if($type == '6')
					{
						$select->columns(array('id','question','type','description','parent_id'));
					}
					else
					{
						$select->columns(array('id','question','type','description'=>new Expression('""'),'parent_id'));
					}
					$statement = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$temp = $run->toArray();
					if(count($temp))
					{
						if($temp[0]['type'] == '11')
						{
							$lable_id = $question['lable_id'];
							foreach ($sections as $section)
							{
								if($section['lable_id']==$lable_id)
								{
									$limit = intval($section['number_of_question']);
								}
							}
							//find child question

							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from('questions');
							$where = new $select->where();
							$where->equalTo('delete',0);
							// $where->equalTo('quality_check',0);
							$where->equalTo('parent_id',$question["question_id"]);
							$select->where($where);

							$select->limit($limit);
							if($type == '6')
							{
								$select->columns(array('id','question','type','description'));
							}
							else
							{
								//no answer should be given on papers other than practice test
								$select->columns(array('id','question','type','description' => new Expression('""')));
							}
							$statement = $sql->getSqlStringForSqlObject($select);

							$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
							$res = $run->toArray();

							if(count($res))
							{
								foreach ($res as $child)
								{
									if($resume == 0)
									{
										/*
										* Saving question-attempt by student
										*/

										// INSERT INTO `question_attempt`(`attempt_id`, `question_id`, `question_type`, `marks`, `neg_marks`,student_id) VALUES ($attempt_id,$data->userId)
										$question_attempt_id = '0';
										$marks = 0;
										if($type != '6')
										{
											$marks = $question['max_marks'];
											if($limit != 0)
											{
												$marks = intval($question['max_marks'])/$limit;
											}
											$insert_test_paper_attempt = new TableGateway('question_attempt', $adapter, null, new HydratingResultSet());

											$insert_test_paper_attempt->insert(array(
												'attempt_id'    => $attempt,
												'question_id'   => $child["id"],
												'section_id'    => $section['id'],
												'question_type' => $child['type'],
												'marks'         => $marks,
												'neg_marks'     => $question['neg_marks'],
												'student_id'    => $request->userId
											));
											$question_attempt_id = $insert_test_paper_attempt->getLastInsertValue();
										}
										$qstatus = 0;
										$time = 0;
										$answers = array();
										array_push($Q, array(
											'question_id'         => $child['id'],
											'question'            => $child['question'],
											'type'                => $child['type'],
											'parent_question'     => $temp[0]['question'],
											'parent_id'           => $temp[0]['id'],
											'description'         => $child['description'],
											'section_id'          => $question['section_id'],
											'lable_id'            => $question['lable_id'],
											'answers'             => $answers,
											'marks'               => $marks,
											'neg_marks'           => $question['neg_marks'],
											'qstatus'             => $qstatus,
											'time'                => $time,
											'question_attempt_id' => $question_attempt_id
										));
									}
									else if($resume == 1)
									{
										//fetch question attempt answers, status and time
										$sql = new Sql($adapter);
										$select = $sql->select();
										$select->from('question_attempt');
										$select->where->equalTo('attempt_id',$attempt)->equalTo('question_id',$child["id"]);

										if($type == '6')
										{
											//$select->columns(array('id','question_status','time','answer'));
										}
										else
										{
											$select->columns(array('id','question_status','time'));
										}
										$statement = $sql->getSqlStringForSqlObject($select);

										$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
										$rows1 = $run->toArray();

										$qstatus='0';
										$time='0';
										$question_attempt_id='0';
										if(count($rows1))
										{
											foreach ($rows1 as $value)
											{
												$qstatus             = $value['question_status'];
												$time                = $value['time'];
												$question_attempt_id = $value['id'];

												$select = $sql->select();
												$select->from('answers_of_question_attempts');
												$where = new $select->where();
												$where->equalTo('question_attempt_id',$question_attempt_id);
												$where->equalTo('question_id',$child["id"]);
												$select->where($where);
												$select->columns(array('option_id'));
												$statement = $sql->getSqlStringForSqlObject($select);
												//die($statement);
												$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
												$options = $run->toArray();
												$answers = array();
												foreach ($options as $option)
												{
													array_push($answers, $option['option_id']);
												}
											}
											array_push($Q, array(
												'question_id'         => $child['id'],
												'question'            => $child['question'],
												'type'                => $child['type'],
												'parent_question'     => $temp[0]['question'],
												'parent_id'           => $temp[0]['id'],
												'description'         => $child['description'],
												'section_id'          => $question['section_id'],
												'lable_id'            => $question['lable_id'],
												'answers'             => $answers,
												'marks'               => $question['max_marks'],
												'neg_marks'           => $question['neg_marks'],
												'qstatus'             => $qstatus,
												'time'                => $time,
												'question_attempt_id' => $question_attempt_id
												));
										}
									}
								}
							}
						}
						else
						{
							$section_id = $question['section_id'];
							$parentq = '';
							$parentid = '0';
							if($temp[0]['parent_id'] != '0')
							{
								$sql = new Sql($adapter);
								$select = $sql->select();
								$select->from('questions');
								$where = new $select->where();
								$where->equalTo('id',$temp[0]['parent_id']);
								$select->where($where);
								$select->columns(array('id','question'));
								$statement = $sql->getSqlStringForSqlObject($select);
								$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
								$t = $run->toArray();
								if(count($t))
								{
									$parentq = $t[0]['question'];
									$parentid = $t[0]['id'];
								}
							}

								if($resume == 0)
								{

									// INSERT INTO `question_attempt`(`attempt_id`, `question_id`, `question_type`, `marks`, `neg_marks`,student_id) VALUES ($attempt_id,$data->userId)
									$question_attempt_id = '0';
									if($type != '6')
									{
										$insert_test_paper_attempt = new TableGateway('question_attempt', $adapter, null, new HydratingResultSet());

										$insert_test_paper_attempt->insert(array(
											'attempt_id'    => $attempt,
											'question_id'   => $question["question_id"],
											'section_id'    => $question['section_id'],
											'question_type' => $temp[0]['type'],
											'marks'         => $question['max_marks'],
											'neg_marks'     => $question['neg_marks'],
											'student_id'    => $request->userId
										));
										$question_attempt_id = $insert_test_paper_attempt->getLastInsertValue();
									}
									$time = 0;
									$qstatus = 0;
									$answers = array();
									array_push($Q, array(
										'question_id'         => $question['question_id'],
										'question'            => $temp[0]['question'],
										'type'                => $temp[0]['type'],
										'parent_question'     => $parentq,
										'parent_id'           => $parentid,
										'description'         => $temp[0]['description'],
										'section_id'          => $question['section_id'],
										'lable_id'            => $question['lable_id'],
										'answers'             => $answers,
										'marks'               => $question['max_marks'],
										'neg_marks'           => $question['neg_marks'],
										'qstatus'             => $qstatus,
										'time'                => $time,
										'question_attempt_id' => $question_attempt_id
									));
								}
								else if($resume == 1)
								{
									//no check for practice test, as resume == 0
									//fetch question attempt answers, status and time

									$sql = new Sql($adapter);
									$select = $sql->select();
									$select->from('question_attempt');
									$where = array('attempt_id'=>$attempt,'question_id'=>$question['question_id']);

									$select->columns(array('id','question_status','time'));
									$select->where($where);

									$statement = $sql->getSqlStringForSqlObject($select);

									$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
									$rows1 = $run->toArray();

									if(count($rows1))
									{
										foreach ($rows1 as $value)
										{
											$qstatus = $value['question_status'];
											$time = $value['time'];
											$select = $sql->select();
											$select->from('answers_of_question_attempts');
											$where = new $select->where();
											$where->equalTo('question_attempt_id',$value['id']);
											$where->equalTo('question_id',$question["question_id"]);
											$select->where($where);
											$select->columns(array('option_id'));
											$statement = $sql->getSqlStringForSqlObject($select);
											$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
											$options = $run->toArray();
											$question_attempt_id = $value['id'];
											$answers = array();
											foreach ($options as $option)
											{
												array_push($answers, $option['option_id']);
											}
										}

										array_push($Q, array(
											'question_id'         => $question['question_id'],
											'question'            => $temp[0]['question'],
											'type'                => $temp[0]['type'],
											'parent_question'     => $parentq,
											'parent_id'           => $parentid,
											'description'         => $temp[0]['description'],
											'section_id'          => $question['section_id'],
											'lable_id'            => $question['lable_id'],
											'answers'             => $answers,
											'marks'               => $question['max_marks'],
											'neg_marks'           => $question['neg_marks'],
											'qstatus'             => $qstatus,
											'time'                => $time,
											'question_attempt_id' => $question_attempt_id
										));
									}
								}
						}
						$i++;
					}
				}//$question {question_id,section_id,lable_id}
				if(isset($total_marks)){
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('test_paper_attempts');
					$update->set(array(
						'total_marks' => $total_marks
						// 'start_time' =>  time()
					));
					$update->where(array(
						'id' => $attempt
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$run = $statement->execute();
				}

				require_once 'TestPaperDeliverApi.php';
				$TestPaperDeliverApi = new TestPaperDeliverApi();
				foreach ($Q as $key => $question)
				{
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('options');
					$where = new $select->where();
					$where->equalTo('question_id',$question["question_id"]);
					$where->equalTo('deleted',0);
					$select->where($where);
					//if($type=='6'){
					$select->columns(array('id','option','number','answer'));
					//}else{
					//  $select->columns(array('id','option','number','answer'=>new Expression('""')));
					//}
					$statement = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$options = $run->toArray();
					$options = $TestPaperDeliverApi->shuffleOptions($question['type'], $options);
					$Q[$key]['options']=$options;
				}
				return $Q;
			}
			catch(Exception $e) {
				echo "Something went wrong".$e;
				return false;
			}
		}

		public function getSections($request){
			try
			{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('section');
				$where = new $select->where();
				$where->equalTo('test_paper',$request->testPaper);
				$select->where($where);
				$select->columns(array('id','name','total_question'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$sections = $run->toArray();
				return $sections;
			}
			catch(Exception $e) {
				echo "Something went wrong".$e;
				return false;
			}
		}


		public function saveQuestionPaper($req)
		{
			date_default_timezone_set('Asia/Kolkata');
			$result = new stdClass();
			try
			{
				$adapter = $this->adapter;
				foreach ($req->answers as $answer)
				{
					$question_attempt_id = $answer->question_attempt_id;
					$question_id = $answer->question_id;
					$qstatus = $answer->qstatus;
					$time = $answer->time;
					$sql = new Sql($adapter);

					// $delete = $sql->delete();
					$delete = $sql->delete('answers_of_question_attempts');

					$where = new $delete->where();
					$where->equalTo('question_attempt_id',$question_attempt_id);
					$where->equalTo('question_id',$question_id);
					$delete->where($where);

					$statement = $sql->getSqlStringForSqlObject($delete);

					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

					foreach ($answer->answers as $value)
					{
						$sql = new Sql($adapter);
						$data = array(
							'question_attempt_id' => $question_attempt_id,
							'question_id'         => $question_id,
							'option_id'           => $value
						);
						$query = new TableGateway('answers_of_question_attempts', $adapter, null, new HydratingResultSet());
						$query->insert($data);
					}

					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('question_attempt');
					$update->set(array(
						'question_status'   =>  $qstatus,
						'time'  =>  $time
					));
					$update->where(array(
						'id' => $question_attempt_id
					));
					$selectString = $sql->getSqlStringForSqlObject($update);
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				}

				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('test_paper_attempts');
				$update->set(array(
					'complete'  =>  '1',
					'end_time'  =>  time(),
				));
				$update->where(array(
					'id' => $req->paper_attempt,
				));
				$selectString = $sql->getSqlStringForSqlObject($update);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

				require_once 'TestPaperDeliverApi.php';
				$resultAttempt = new TestPaperDeliverApi();
				$result->result_status = $resultAttempt->genrateResultOfAttemptId($req->paper_attempt);

				$result->status = '1';
				$result->message = 'Save successful';
				return $result;
			}
			catch(Exception $e) {
				echo "Something went wrong ".$e;
				return false;
			}
		}

		public function saveQuestion($req)
		{
			date_default_timezone_set('Asia/Kolkata');
			$result = new stdClass();
			try
			{
				$adapter = $this->adapter;

				$question_attempt_id = $req->question_attempt_id;
				$question_id = $req->question_id;
				$qstatus = $req->qstatus;
				$time = $req->time;
				$sql = new Sql($adapter);

				// $delete = $sql->delete();
				$delete = $sql->delete('answers_of_question_attempts');

				$where = new $delete->where();
				$where->equalTo('question_attempt_id',$question_attempt_id);
				$where->equalTo('question_id',$question_id);
				$delete->where($where);

				$statement = $sql->getSqlStringForSqlObject($delete);
				//echo ($statement);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('question_attempt');
				$update->set(array(
					'question_status'   =>  $qstatus,
					'time'  =>  $time
				));
				$update->where(array(
					'id' => $question_attempt_id,
					'question_id'   =>  $question_id
				));


				$selectString = $sql->getSqlStringForSqlObject($update);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);


				foreach ($req->answers as $value) {

					$sql = new Sql($adapter);

					$data = array(
						'question_attempt_id' => $question_attempt_id,
						'question_id' => $question_id,
						'option_id' => $value
					);

					$query = new TableGateway('answers_of_question_attempts', $adapter, null, new HydratingResultSet());
					$query->insert($data);
				}

				$result->status = '1';
				$result->message = 'Save successful';
				return $result;
			}
			catch(Exception $e) {
				echo "Something went wrong".$e;
				return false;
			}

		}

		public function testPaperDetails($data)
		{
			date_default_timezone_set('Asia/Kolkata');
			$result = new stdClass();
			$assignment = new Assignment;
			$result->assignments_details = $assignment->fetchStudentAssignments($data);

			try {
				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();

				$select->from('student_program')->join(['sa'=>'student_assignment'],"sa.test_program_id = student_program.test_program_id",['assignment'=>new Expression('count(sa.id)')],'left')->join(['tp'=>'test_program'],'tp.id = student_program.test_program_id',['cut_off'],'left');
				$where = new $select->where();
				$where->equalTo('student_program.user_id', $data->userId);
				$where->equalTo('student_program.test_program_id', $data->program_id);
				$select->where($where);
				$select->columns(array('test_program_id', 'allowed_total_paper', 'allowed_total_lab', 'valid_till'));
				$sqlQuery= $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($sqlQuery, $adapter::QUERY_MODE_EXECUTE);
				$user_program_id=$run->toArray();
				$prgm_id = array();
				//dd($sqlQuery);

				foreach ($user_program_id as $user_program) {
					array_push($prgm_id, $user_program['test_program_id']);
					$allowed_total_lab = $user_program['allowed_total_lab'];
					$allowed_total_paper = $user_program['allowed_total_paper'];
					$result->expiry = date($user_program['valid_till']);
					$result->isExpired = (boolean) ($result->expiry < date('Y-m-d').' 23:59:59');
					$result->assignment = intval($user_program['assignment']);
					$result->cutoff = $user_program['cut_off'];
				}

				if(!in_array($data->program_id, $prgm_id)) {
					$result->status  = 2;
					$result->message = "Package Not Available.";
					return $result;
				}

				$sql = new Sql($adapter);
				$select = $sql->select();

				$select->from(array('t_p' => 'test_paper'), array())
					->join(array('tp_tp' => 'test_papers_of_test_program'),
					't_p.id = tp_tp.test_paper_id', array())
					->join(array('section' => 'section'),
					'section.test_paper = t_p.id', array());

				$where = new $select->where();
				$where->equalTo('tp_tp.test_program_id', $data->program_id);
				$where->equalTo('t_p.deleted', 0);
				$select->where($where);
				$select->columns(array( '*','section' => new Expression('Group_concat(section.id)')));
				$select->order('type');
				$select->group('id');

				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				foreach ($res as $key => $test_paper) {
					preg_match_all('/\d+/', $test_paper['name'], $matches);
					$res[$key]['order'] = intval(end($matches[0]));
					$section = explode( ',', $test_paper['section']);
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('test_paper_labels');
					$where = new $select->where();
					$where->in('section_id',$section);
					$select->where($where);
					$select->columns(array('questions'=>new Expression('SUM(number_of_question)')));
					$sqlQuery= $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($sqlQuery, $adapter::QUERY_MODE_EXECUTE);
					$questionsres = $run->toArray();

					if(count($questionsres)) {
						$res[$key]['questions'] = $questionsres[0]['questions'];
					}
				}

				$price = array();
				foreach ($res as $key => $row) {
					$price[$key] = $row['order'];
				}
				array_multisort($price, SORT_ASC, $res);
				$result->data = $res;

				$i = 0;

				foreach ($res as $test_paper) {
					$time = $test_paper['time'];

					$resume = 0;

					if($test_paper['type'] == '5') {
						$now = time();
						$start_time = date_create_from_format('d F Y - H:i', $test_paper['start_time']);
						$start_time = intval(date_format($start_time, 'U'));

						$end_time = date_create_from_format('d F Y - H:i', $test_paper['end_time']);
						$end_time = intval(date_format($end_time, 'U'));

						//$resume = 0;
						$result->data[$i]['flag'] = false;

						if($now >= $start_time && $now <= $end_time){
							$resume = 0;
							$result->data[$i]['flag'] = true;
						}
						//print_r($now);
						$result->data[$i]['now'] = date('d F Y - H:i:s',time());
						$result->data[$i]['now1'] = date('d F Y - H:i:s',$start_time);
						$result->data[$i]['now2'] = date('d F Y - H:i:s',$end_time);
					}

					$sql = new Sql($adapter);
					$select = $sql->select();

					$select->from('test_paper_attempts');

					$select->where(array(
						'student_id'    =>  $data->userId,
						'test_paper_id' =>  $test_paper['id']
					));

					$select->columns(array('total_marks','marks_archived','complete','id','start_time','test_paper_id'));

					$selectString = $sql->getSqlStringForSqlObject($select);
					// die($selectString);
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$completes = $run->toArray();
					if($test_paper['type'] == '4') {
						$result->attempts = $completes;
					}
					foreach ($completes as $paper) {

						$now1 = $paper['start_time'];
						$now = time();
						$start_time = date_create_from_format('U', $now);

						if($paper['complete'] == 0)
						{

							$now = time();
							$diff = $now - $paper['start_time'];
							$test_paper_id = $paper['test_paper_id'];

							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from('test_paper');

							$select->where(array(
								'id'    =>  $test_paper_id
							));

							$select->columns(array('id','time','start_time','end_time','type'));
							// $select->columns(array('attempted' => new Expression('COUNT(id)')));

							$selectString = $sql->getSqlStringForSqlObject($select);

							$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
							$res = $run->toArray();
							$papertime = $res[0]['time'];

							if(($diff / 60) > $papertime )
							{
								$resume = 0;
								if($res[0]['type'] == '6')
								{
									$resume = 1;
								}
							}
							else{
								$resume = 1;
							}


							if($res[0]['type'] == '5')
							{
								$start_time = date_create_from_format('d F Y - H:i', $res[0]['start_time']);
								$start_time = intval(date_format($start_time, 'U'));

								$end_time = date_create_from_format('d F Y - H:i', $res[0]['end_time']);
								$end_time = intval(date_format($end_time, 'U'));

								$resume = 0;

								if($now >= $start_time && $now <= $end_time)
								{
									$resume = 1;
								}
								$result->data[$i]['now'] = date('d F Y - H:i:s',time());
								$result->data[$i]['now1'] = date('d F Y - H:i:s',$start_time);
								$result->data[$i]['now2'] = date('d F Y - H:i:s',$end_time);
							}
						} else if($paper['complete'] == 1) {
							if($res[0]['type'] == '5') {
								$start_time = date_create_from_format('d F Y - H:i', $res[0]['start_time']);
								$start_time = intval(date_format($start_time, 'U'));

								$end_time = date_create_from_format('d F Y - H:i', $res[0]['end_time']);
								$end_time = intval(date_format($end_time, 'U'));

								$resume = 0;
								if($now >= $start_time && $now <= $end_time) {
									$resume = 1;
								}
								//print_r($now);
								$result->data[$i]['now'] = date('d F Y - H:i:s',time());
								$result->data[$i]['now1'] = date('d F Y - H:i:s',$start_time);
								$result->data[$i]['now2'] = date('d F Y - H:i:s',$end_time);
							}
						}
					}

					$result->data[$i]['status'] = $resume;
					$result->data[$i]['attempted'] = count($completes);

					//first attempt id grabbed
					if(count($completes)) {
						$result->data[$i]['attempt_first'] = $completes[count($completes) - 1]['id'];
						$result->data[$i]['attempt_time'] = $completes[count($completes) - 1]['start_time'];
						$result->data[$i]['attempt_first_marks'] = $completes[count($completes) - 1]['marks_archived'];
						$result->data[$i]['total_marks'] = $completes[count($completes) - 1]['total_marks'];
					}

					$i++;
				}

				/**
				 * Logic for assistant
				 * @param attemptStatus
				 * institute
				 *     assistant login [lab+attemtps]
				 *     !assistatnt login [attempts]
				 * !institute
				 *     true
				 */


				// $validateLabAttempts = $this->validateAttempts($data->userId, $data->program_id, 1);
				// $validateInstitute = $this->validateInstitute($data->userId, $data->program_id, 0);
				
				$validateAttempts = $this->validateAttempts($data->userId, $data->program_id, 0);
				
				$result->totalAttempts = $this->validateAttempts($data->userId, $data->program_id, 0, 1);
				// $result->institute = $validateInstitute;

				// $validateInstituteLabType = $this->validateInstituteLabType($data->userId);
				// $result->institute_lab_type = $validateInstituteLabType;

				// $result->labAttempts = $this->validateAttempts($data->userId, $data->program_id, 1, 1);
				// $result->remainingAttempts = $allowed_total_lab - $result->labAttempts;
				// $result->allowed_total_paper = intval($allowed_total_paper) - $result->totalAttempts;
				// $status = ($validateInstitute) ? 1 : 0;

				// if ($validateInstitute) {
				// 	if(isset($_SESSION['assistant'])) {
				// 		$status = ($validateLabAttempts && $validateAttempts);
				// 	} else {
				// 		$status = (boolean) $validateAttempts;
				// 	}
				// } else {
				// 	$status = true;
				// }
				$status = true;

				$result->attemptStatus = $status;
				$result->status = 1;
				$result->message = 'Successfully Fetched Paper Details';

				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function getSubjectPapers($data)
		{
			date_default_timezone_set('Asia/Kolkata');
			$result = new stdClass();

			try
			{
				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				// QUERY SELECT `units`.`id`  FROM `units` WHERE `deleted` = '0' AND subject_id IN($data->subject_id)
				$select = $sql->select();
				$select->from('units');
				$select->columns(array('id'));
				$where = new $select->where();
				$where->equalTo('deleted',0);
				$where->equalTo('subject_id',$data->subject);
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$units = $run->toArray();

				$unit_ids = array();
				if(count($units) > 0) {
					foreach ($units as $unit) {
						array_push($unit_ids, $unit['id']);
					}
					// QUERY SELECT `chapters`.`id`, `chapters`.`name` FROM `chapters` WHERE `deleted` = '0' AND unit_id In($data->unit_id)
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('chapters');
					$select->columns(array('id','name'));
					$where = new $select->where();
					$where->equalTo('deleted',0);
					$where->in('unit_id',$unit_ids);
					$select->where($where);
					$selectString = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$chapters = $run->toArray();

					foreach ($chapters as $key => $value)
					{
						$sql = new Sql($adapter);

						$select = $sql->select();

						$select->from(array('tp'=>'test_papers_of_test_program'))
								->join(array('t'=>'test_paper'),'tp.test_paper_id=t.id',array('name','type','time','total_attempts','start_time','end_time'),'left');

						$where = new $select->where();
						$where->equalTo('parent_type',2);
						$where->equalTo('parent_id',$value['id']);
						$where->equalTo('test_program_id',$data->program_id);
						$select->where($where);
						$select->columns(array('id'=>'test_paper_id'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						//echo $selectString;
						$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$count = $run->toArray();
						//_print_r($count);
						$practice = array();
						$chapter = array();
						foreach ($count as $key1 => $value1)
						{
							if($value1['type'] == 3)
							{
								$sql = new Sql($adapter);
								$select = $sql->select();

								$select->from('test_paper_attempts');
								$select->where(array(
									'student_id'    =>  $data->userId,
									'test_paper_id' =>  $value1['id']
								));

								$select->columns(array('complete','id','start_time','test_paper_id'));

								$selectString = $sql->getSqlStringForSqlObject($select);

								$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

								$attempts = $run->toArray();

								foreach ($attempts as $k => $v) {
									if($v['complete'] == 0){
										$now = time();
										$diff = $now - $v['start_time'];
										$test_paper_id = $value1['id'];

										$papertime = $value1['time'];

										if(($diff / 60) > $papertime )
										{
											$value1['resume'] = 1; //start
										}
										else{
											$value1['resume'] = 2; //resume
										}
									}else{
										$value1['resume'] = 1; //start
									}
								}
								$value1['attempts'] = count($attempts);
								if(count($attempts)){
									$value1['attempts_first'] = $attempts[0]['id'];
								}else{
									$value1['attempts_first'] = '0';
								}
								array_push($chapter, $value1);
							}//chapter type
							if($value1['type'] == 6)
							{
								$value1['resume'] = 1;
								array_push($practice, $value1);
							}
						}
						$chapters[$key]['practice'] = $practice;
						$chapters[$key]['chapter'] = $chapter;

					}//end of for chapters
				}
				//_print_r($chapters);

				$result->chapters = $chapters;

				$result->status = 1;
				$result->message = 'Successfully Fetched Paper Details';

				return $result;
			}
			catch(Exception $e )
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function getSubjectPapers1($data)
		{
			date_default_timezone_set('Asia/Kolkata');
			$result = new stdClass();

			try{

				$adapter = $this->adapter;

				$sql = new Sql($adapter);

				$select = $sql->select();

				$select->from(array('tp'=>'test_papers_of_test_program'))
						->join(array('t'=>'test_paper'), 'tp.test_paper_id=t.id', array('name','type','time','total_attempts', 'start_time', 'end_time'), 'left');

				$where = new $select->where();
				$where->equalTo('tp.parent_type', 1);
				$where->equalTo('test_program_id', $data->program_id);
				//$where->equalTo('tp.parent_id',0);
				$where->in('tp.test_paper_type_id', array(1,3,4,5,6));
				$select->where($where);
				$select->order('test_paper_type_id');

				$select->columns(array('id'=>'test_paper_id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				// echo $selectString;
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$papers = $run->toArray();
				$scheduled = array();
				$mock = array();
				$diagnostic = array();
				if(count($papers)){
					foreach ($papers as $key => $value) {
						$sql = new Sql($adapter);
						$select = $sql->select();

						$select->from('test_paper_attempts');

						$select->where(array(
							'student_id'    =>  $data->userId,
							'test_paper_id' =>  $value['id']
						));

						$select->columns(array('complete','id','start_time','test_paper_id'));

						$selectString = $sql->getSqlStringForSqlObject($select);

						$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

						$attempts = $run->toArray();
						$value['resume'] = 1; //start
						foreach ($attempts as $k => $v)
						{
							if($v['complete'] == 0)
							{
								$now = time();
								$diff = $now - $v['start_time'];
								$test_paper_id = $value['id'];

								$papertime = $value['time'];

								if(($diff / 60) > $papertime )
								{
									$value['resume'] = 1; //start
								}
								else{
									$value['resume'] = 2; //resume
								}
							}
						}

						//scheduled test chack time start_time<=now<end_time
						//____________________________________________________

						if($value['type']=='5'){
							$now = time();
							$start_time = date_create_from_format('d F Y - H:i', $value['start_time']);
							$start_time = intval(date_format($start_time, 'U'));

							$end_time = date_create_from_format('d F Y - H:i', $value['end_time']);
							$end_time = intval(date_format($end_time, 'U'));

							$value['resume'] = 0;
							if($now >= $start_time && $now < $end_time){
								$value['resume'] = 1;
							}
							//print_r($now);
							$value['now'] = date('d F Y - H:i:s',time());
							$value['now1'] = date('d F Y - H:i:s',$start_time);
							$value['now2'] = date('d F Y - H:i:s',$end_time);
						}
						//scheduled test
						//______________

						$value['attempts'] = count($attempts);
						if(count($attempts))
						{
							$value['attempts_first'] = $attempts[0]['id'];
						}
						else
						{
							$value['attempts_first'] = '0';
						}

						switch($value['type'])
						{
							case '1':
								array_push($diagnostic, $value);
								break;
							case '4':
								array_push($mock, $value);
								break;
							case '5':
								array_push($scheduled, $value);
								break;
						}
					}
				}

				$result->mock = $mock;
				$result->scheduled = $scheduled;
				$result->diagnostic = $diagnostic;

				$result->status = 1;
				$result->message = 'Successfully Fetched Paper Details';

				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function checkScheduled($data)
		{
			date_default_timezone_set('Asia/Kolkata');
			$resume = 0;
			$result = new stdClass();

			try
			{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();

				$select->from('test_paper');
				$where = array('id'=>$data->paper);
				$select->where($where);

				$select->columns(array('id','start_time','end_time','type'));
				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();

				if(count($rows) == 1) {
					foreach ($rows as $key => $value)
					{
						if($value['type']=='5')
						{
							$now = time();
							$start_time = date_create_from_format('d F Y - H:i', $value['start_time']);
							$start_time = intval(date_format($start_time, 'U'));

							$end_time = date_create_from_format('d F Y - H:i', $value['end_time']);
							$end_time = intval(date_format($end_time, 'U'));

							if($now >= $start_time && $now < $end_time){
								$resume = 1; //permission granted
							}
						}
					}
				}

				$result->status = $resume;
				$result->message = 'Can Start';

				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function validateInstitute($userId, $program)
		{
			/**
			 * 1. if assistant login
			 * 2. if institute-student map
			 * 3. program fetch allowed_total, lab
			 */
			date_default_timezone_set('Asia/Kolkata');
			$result = new stdClass();

			try
			{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();

				$select->from('coupons');
				$select->where->equalTo('test_program_id', $program)
							  ->equalTo('student_id', $userId);

				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$coupons = $run->toArray();

				if(!count($coupons))
				{
					return 0;
				}
				else
				{
					// $institute = $coupons[0]['institute_id'];
					return 1;
				}
			}
			catch(Exception $e)
			{
				return 0;
			}

		}

		// RETURN INSTITUTE LAB TYPE FROM PROFILE TABLE
		public function validateInstituteLabType($userId)
		{
			date_default_timezone_set('Asia/Kolkata');
			$result = new stdClass();

			try
			{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('coupons');
				$select->where->equalTo('student_id', $userId);

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				if (count($rows) == 0) {
					return 0;
				}
				$coupons = (object) $rows[0];
				if((int)$coupons->institute_id != 0){
					$statement = "SELECT ins_lab_type FROM profile WHERE user_id = $coupons->institute_id";
						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$rows = $run->toArray();
						return (int)$coupons->ins_lab_type;
				}
			}
			catch(Exception $e)
			{
				return 0;
			}

		}

		/**
		 * [validateAttempts description]
		 * @param  [lab]  0 for total attempts
		 * @param  [lab]  1 for total lab attempts
		 */
		public function validateAttempts($userId, $program, $lab = 0, $valueFlag = 0)
		{
			try
			{
				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();

				$select->from('student_program');
				$select->where->equalTo('test_program_id', $program)
							  ->equalTo('user_id', $userId);

				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$programs = $run->toArray();

				if(!count($programs)) {
					return 0;
				}

				$allowed_total_paper = $programs[0]['allowed_total_paper'];
				$allowed_total_lab = $programs[0]['allowed_total_lab'];

				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();

				$select->from('test_paper_attempts');
				$select->where->equalTo('student_id', $userId)->equalTo('program_id', $program)->greaterThanOrEqualTo('complete', 0);

				if($lab) {
					$select->where->equalTo('inlab', 1);
				}

				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$count = $run->toArray();

				$paper = ($lab) ? $allowed_total_lab : $allowed_total_paper;
				if ($valueFlag) {
					return count($count);
				}
				return (count($count) < $paper) ? 1 : 0;

			}
			catch(Exception $e)
			{
				return 0;
			}

		}
	}
?>
