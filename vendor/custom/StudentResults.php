<?php

	//SELECT *, @curRank := IF(@prevRank = marks_archived, @curRank, @incRank) AS rank, @incRank := @incRank + 1, @prevRank := marks_archived FROM test_paper_attempts r, ( SELECT @curRank :=0, @prevRank := NULL, @incRank := 1 ) p WHERE test_paper_id=2 ORDER BY marks_archived

	// add group by student_id

	use Zend\Db\TableGateway\TableGateway;
    use Zend\Db\Sql\Sql;
    use Zend\Db\ResultSet\HydratingResultSet;
    use Zend\Db\Sql\Predicate\PredicateInterface;
    use Zend\Db\Sql\Expression;


	require_once 'Connection.php';
	class StudentResults extends Connection
	{
		public function get_attempt_history($data)
		{
			$result = new stdClass();
			$result->_tbegin = microtime(true);
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('student_program');
				$where = new $select->where();
				$where->equalTo('user_id',$data->userId);
				$select->where($where);
         		$select->columns(array('test_program_id'));
         		$sqlQuery= $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($sqlQuery, $adapter::QUERY_MODE_EXECUTE);
				$user_program_id=$run->toArray();
				$prgm_id = array();
				foreach ($user_program_id as $user_program) {
					array_push($prgm_id, $user_program['test_program_id']);
				}

				$statement = "SELECT test_paper_attempts.*, test_paper.type, test_paper.name, test_paper.time FROM test_paper_attempts LEFT JOIN test_paper ON test_paper.id  = test_paper_attempts.test_paper_id WHERE test_paper_attempts.id = ".$data->attempt_id;
				// die($statement);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$attempt_data = $run->toArray();
				// $attempt_prgm_id=$attempt_data[0]['test_program_id'];
				// foreach ($attempt_data as $user_program) {
				// 	array_push($attempt_prgm_id, $attempt_data['test_program_id']);
				// }
				if(count($attempt_data)==0 || !in_array($attempt_data[0]['program_id'],$prgm_id))
				{
					$result->status = 2;
					$result->message = "Package Not Available.";
					return $result;
				}

				if ($attempt_data[0]["type"] == 0) {
					$select = $sql->select();
					$select->from("test_paper_attempts")
							->join(array('test_paper' => 'test_paper'), 'test_paper.id  = test_paper_attempts.test_paper_id', array('name', 'time', 'type'), 'left');
					// $select->columns(array("id"));
					$select->where(array('optimizer_id' => $attempt_data[0]['test_paper_id']));
					$statement = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$attempt_data = $run->toArray();
					// die(print_r($attempt_data));

					$result->optimizer_id = $attempt_data[0]['optimizer_id'];
					$data->attempt_id = $attempt_data[0]["id"];
				}
				else {
					$result->optimizer_id = $attempt_data[0]['optimizer_id'];
				}



				$result->test_paper_time = $attempt_data[0]['time'];
				$result->test_name = $attempt_data[0]['name'];

				//block in case of scheduled test
				$sql = new Sql($adapter);
				$select = $sql->select();

				$select->from('test_paper');
				$select->where->equalTo('id',$attempt_data[0]['test_paper_id']);
				$select->columns(array('end_time','type','id'));
				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$paper = $run->toArray();


				if(count($paper))
				{
					if($paper[0]['type'] == '5')
					{
						$end_time = date_create_from_format('d F Y - H:i', $paper[0]['end_time']);
						$end_time = intval(date_format($end_time, 'U'));
						if(time() <= $end_time)
						{
							$result->status = 3;
							$result->message = 'Result Available after '.$paper[0]['end_time'];
							return $result;
						}
					}
				}

				/*
				* get all questions of given attempt id
				*/
				$select = $sql->select();
				$select->from("question_attempt");
				$select->columns(array("id"));
				$select->where(array('attempt_id' => $data->attempt_id));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$total_questions = $run->toArray();

				/*
				* get all attempted questions of given attempt id
				*/
				$select->where('question_status > 1');
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$attempted = $run->toArray();

				/*
				* get all questions of given attempt id which answered correct by student
				*/
				$select->where(array('correct' => 1));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$correct = $run->toArray();

				/*
				* creating result for breif pie CHART
				*/
				$result->total_questions = count($total_questions);
				$result->attempted = count($attempted);
				$result->correct = count($correct);
				$result->wrong = $result->attempted - $result->correct;
				$result->unattempted = $result->total_questions - $result->attempted;

				/** BREIF SERIAL CART **/

				/*
				* fetching test paper attempt details
				*/
				$select = $sql->select();
				$select->from("test_paper_attempts");
				// $select->columns(array("id"));
				$select->where(array('id' => $data->attempt_id));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$attempt_data = $run->toArray();

				$result->your_score = $attempt_data[0]["marks_archived"];

				/*
				* get the topper marks
				*/
				$statement = 'SELECT MAX(marks_archived) AS marks_archived, FROM_UNIXTIME(end_time,"%d-%m-%Y") as test_date FROM `test_paper_attempts` WHERE test_paper_id= '.$attempt_data[0]["test_paper_id"];
				// die($statement);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$topper = $run->toArray();
				$result->topper_score = $topper[0]["marks_archived"];
				$result->test_date = $topper[0]["test_date"];

				/*
				* get the average marks
				*/
				$statement = "SELECT AVG(marks_archived) AS marks_archived FROM `test_paper_attempts` WHERE test_paper_id= ".$attempt_data[0]["test_paper_id"];
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$average = $run->toArray();
				$result->average_score = $average[0]["marks_archived"];


				/** Question Wise Report Rows **/


				$statement = "SELECT
					qa.id AS id,
					section.name AS section,
					IFNULL(group_concat(distinct if(tags.tag_type = 3,chapters.name,' ')),'') AS chapters,
					IFNULL(group_concat(distinct if(tags.tag_type = 2,units.name,' ')),'') AS units,
					IFNULL(group_concat(distinct if(tags.tag_type = 6,difficulty.difficulty,' ')),'') AS difficulty,
					IFNULL(group_concat(distinct if(tags.tag_type = 4,topics.name,' ')),'') AS topics,
					qa.id AS attempt,
					qa.time,
					qa.correct,
					qa.question_status,
					(SELECT COUNT(DISTINCT(student_id))/(SELECT COUNT(*) FROM users WHERE role='4')*100 FROM `question_attempt`  WHERE question_id = qa.question_id) AS percent,
					tp.time AS avgtime,
					(SELECT AVG(time) FROM question_attempt WHERE question_id=qa.question_id) AS avgtimeall

					FROM `question_attempt` AS qa
					LEFT JOIN tagsofquestion AS tags ON qa.question_id=tags.question_id
					LEFT JOIN chapters AS chapters ON chapters.id=tags.tagged_id
					LEFT JOIN units AS units ON units.id=tags.tagged_id
					LEFT JOIN difficulty AS difficulty ON difficulty.id=tags.tagged_id
					LEFT JOIN topics AS topics ON topics.id=tags.tagged_id
					LEFT JOIN test_paper_attempts AS tpa ON tpa.id = qa.attempt_id
					LEFT JOIN test_paper AS tp ON tp.id = tpa.test_paper_id
					LEFT JOIN section AS section ON section.id=qa.section_id

					WHERE attempt_id=$data->attempt_id GROUP BY(qa.question_id)";
				//die($statement);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$questionWiseReportRows = $run->toArray();

				$result->questionWiseReportRows = array();
				foreach ($questionWiseReportRows as $key => $questionWiseReportRow) {
					$questionWiseReportRow['units'] =  rtrim((str_replace(" ,","", $questionWiseReportRow['units'])), ', ');
					$questionWiseReportRow['chapters'] =  rtrim((str_replace(" ,","", $questionWiseReportRow['chapters'])), ', ');
					$questionWiseReportRow['difficulty'] =  rtrim((str_replace(" ,","", $questionWiseReportRow['difficulty'])), ', ');
					$questionWiseReportRow['topics'] =  rtrim((str_replace(" ,","", $questionWiseReportRow['topics'])), ', ');

					$questionWiseReportRow['units'] = ($questionWiseReportRow['units']==''?'NA':$questionWiseReportRow['units']);
					$questionWiseReportRow['chapters'] = ($questionWiseReportRow['chapters']==''?'NA':$questionWiseReportRow['chapters']);
					$questionWiseReportRow['difficulty'] = ($questionWiseReportRow['difficulty']==''?'NA':$questionWiseReportRow['difficulty']);
					$questionWiseReportRow['topics'] = ($questionWiseReportRow['topics']==''?'NA':$questionWiseReportRow['topics']);
					$result->questionWiseReportRows[] = $questionWiseReportRow;
				}

				/** For Topic Wise Report Chart Rows **/


				$statement = "SELECT chapters.name AS name,COUNT(*) AS total_questions,SUM(correct) AS correct,SUM(IF(qa.question_status > 1 AND correct=0,1,0)) AS wrong,SUM(IF(qa.question_status > 1,1,0)) AS attempted,SUM(IF(qa.question_status < 2,1,0)) AS unattempted,SUM(marks) AS max_marks,SUM(if(qa.question_status>1 AND qa.correct = 1,marks,-neg_marks)) AS score, section_id, section.name AS section
					FROM `question_attempt` AS qa
					INNER JOIN tagsofquestion AS tags ON qa.question_id=tags.question_id
					LEFT JOIN chapters AS chapters ON chapters.id=tags.tagged_id
					LEFT JOIN section AS section ON section.id=qa.section_id

					WHERE attempt_id=$data->attempt_id AND tag_type='3'
					Group BY tags.tagged_id, qa.section_id";

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->topicWiseReportChart = $run->toArray();


			/** For Difficulty Wise Report Rows **/

				$statement = 'SELECT section_id,section.name AS section,(SELECT difficulty.difficulty FROM `difficulty` WHERE difficulty.id = tagsofquestion.tagged_id) AS name, (SELECT COUNT(t2.correct) FROM `question_attempt` AS t2 INNER JOIN tagsofquestion AS toq1 ON toq1.question_id = t2.question_id WHERE t2.question_status = 3 AND t2.attempt_id = '.$data->attempt_id.' AND toq1.tag_type=6 AND toq1.tagged_id=tagsofquestion.tagged_id AND t2.question_status = 3 AND t2.correct = 1) AS correct , (SELECT COUNT(t2.correct) FROM `question_attempt` AS t2 INNER JOIN tagsofquestion AS toq1 ON toq1.question_id = t2.question_id WHERE t2.question_status = 3 AND t2.attempt_id = '.$data->attempt_id.' AND toq1.tag_type=6 AND toq1.tagged_id=tagsofquestion.tagged_id AND t2.question_status = 3 AND t2.correct = 0) AS wrong FROM `question_attempt` INNER JOIN tagsofquestion ON question_attempt.question_id = tagsofquestion.question_id LEFT JOIN section AS section ON section.id=question_attempt.section_id WHERE question_attempt.question_status = 3 AND question_attempt.attempt_id = '.$data->attempt_id.' AND tagsofquestion.tag_type=6 GROUP BY tagsofquestion.tagged_id,question_attempt.section_id';



				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->difficultyWiseReportChart = $run->toArray();


				$statement = "SELECT section_id,section.name AS section,skills.skill AS name,COUNT(*) AS total_questions,SUM(correct) AS correct,SUM(IF(qa.question_status > 1 AND correct=0,1,0)) AS wrong,SUM(IF(qa.question_status > 1,1,0)) AS attempted,SUM(IF(qa.question_status < 2,1,0)) AS unattempted,SUM(marks) AS max_marks,SUM(if(qa.question_status>1 AND qa.correct = 1,marks,-neg_marks)) AS score FROM `question_attempt` AS qa INNER JOIN tagsofquestion AS tags ON qa.question_id=tags.question_id LEFT JOIN skills AS skills ON skills.id=tags.tagged_id LEFT JOIN section AS section ON section.id=qa.section_id where attempt_id=$data->attempt_id AND tag_type='7' Group BY tags.tagged_id,qa.section_id";

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->skillWiseReportChart = $run->toArray();





				$statement = "SELECT section_id,section.name AS section,difficulty.difficulty AS name,COUNT(*) AS total_questions,SUM(correct) AS correct,SUM(IF(qa.question_status > 1 AND correct=0,1,0)) AS wrong,SUM(IF(qa.question_status > 1,1,0)) AS attempted,SUM(IF(qa.question_status < 2,1,0)) AS unattempted,SUM(marks) AS max_marks,SUM(if(qa.question_status>1 AND qa.correct = 1,marks,-neg_marks)) AS score FROM `question_attempt` AS qa INNER JOIN tagsofquestion AS tags ON qa.question_id=tags.question_id LEFT JOIN difficulty AS difficulty ON difficulty.id=tags.tagged_id LEFT JOIN section AS section ON section.id=qa.section_id where attempt_id=$data->attempt_id AND tag_type='6' Group BY tags.tagged_id,qa.section_id";

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->difficultyWiseTableReportRows = $run->toArray();


				$statement = "SELECT id, marks_archived, total_marks, (end_time - start_time) as total_time, ((marks_archived * 100) / total_marks) AS percent_marks, MAX(marks_archived) as topper_marks FROM test_paper_attempts WHERE id = ".$data->attempt_id;
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$percentileTable = $run->toArray();
				$percentileTable = $percentileTable[0];

				$statement = 'SELECT COUNT(id) as total_questions, SUM(correct) AS correct, SUM(IF(question_status = 3, 1, 0)) AS attempted FROM question_attempt WHERE attempt_id ='.$data->attempt_id;
				// die($statement);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$question_data = $run->toArray();
				$question_data = $question_data[0];

				$percentileTable['total_questions'] = $question_data['total_questions'];
				$percentileTable['correct'] = $question_data['correct'];
				$percentileTable['attempted'] = $question_data['attempted'];
				$result->percentileTable = $percentileTable;


				$statement = 'SELECT question_attempt.section_id, section.name, section.cut_off_marks, (SUM(IF(question_attempt.correct =1, question_attempt.marks, -question_attempt.neg_marks))) AS marks FROM question_attempt LEFT JOIN section ON section.id = question_attempt.section_id WHERE question_attempt.attempt_id = '.$data->attempt_id.' GROUP BY question_attempt.section_id';

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$sections = $run->toArray();

				$section_wise_report = array();
				foreach ($sections as $key => $section) {

					$statement = 'SELECT MAX(marks) AS topper_marks FROM (SELECT (SUM(IF(question_attempt.correct =1, question_attempt.marks, -question_attempt.neg_marks))) AS marks  FROM question_attempt WHERE question_attempt.section_id='.$section['section_id'].' GROUP BY student_id) AS section_wise_report';

					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$section_topper_marks = $run->toArray();
					$section_topper_marks = $section_topper_marks[0];

					$section['topper_marks'] = $section_topper_marks['topper_marks'];
					$section['success_gap'] = $section['cut_off_marks'] - $section['marks'];
					$section['topper_gap'] = $section['topper_marks'] - $section['marks'];
					$section_wise_report[] = $section;
				}

				$result->section_wise_report = $section_wise_report;


				$result->detailedReport = $this->fetchDetailedQuestionWiseReport($data->attempt_id);

				$statement = "SELECT COUNT(*)+1 AS rank FROM `test_paper_attempts` where test_paper_id = {$attempt_data[0]['test_paper_id']} AND marks_archived > '{$attempt_data[0]['marks_archived']}' group by student_id";

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$rank = $run->toArray();
				if(count($rank))
				{
					$result->ranking = $rank[0]['rank'];
				}
				else
				{
					$result->ranking = 0;
				}

				$statement = 'SELECT distinct(student_id),marks_archived
								FROM `test_paper_attempts` where test_paper_id = '.$attempt_data[0]['test_paper_id'].' group by student_id';

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$percentile_rank = $run->toArray();
				$percentile = 100;
				if(count($percentile_rank))
				{	$marks=array();
					$a = 0;
					$b = 0;
					foreach ($percentile_rank as $key => $value) {
						array_push($marks, intval($value['marks_archived']));

					}
					$x=intval($attempt_data[0]['marks_archived']);
					foreach ($marks as $key => $value) {
						if($value < $x)
						{
							$a++;
						}else if($value == $x)
						{
							$b++;
						}
					}

					$percentile = ( ( $a + (0.5 * $b) )*100) / count($marks);
				}

				$result->a = $a;
				$result->b = $b;
				$result->c = $marks;
				$result->percentile_rank = intval($percentile);


				$result->status = 1;
				$result->message = "Result Successfully Fetched";
				$result->_tend = microtime(true);

				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Some thing went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}


		public function attemptedQuestionExplanation($data)
		{
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$statement = 'SELECT question_attempt.question_id, questions.question, questions.description FROM question_attempt LEFT JOIN questions ON questions.id = question_attempt.question_id WHERE question_attempt.id = '.$data->question_attempt_id;
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$details = $run->toArray();
				$details = $details[0];


				$select = $sql->select();
				$select->from('options');
				$where = new $select->where();
				$where->equalTo('question_id',$details['question_id']);
				$where->equalTo('deleted',0);
				$select->where($where);
				$select->columns(array('id','option','number'));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$options = $run->toArray();
				$details['options'] = $options;


				$statement = 'SELECT options.option FROM `answers_of_question_attempts` LEFT JOIN options ON options.id = answers_of_question_attempts.option_id WHERE answers_of_question_attempts.question_attempt_id = '.$data->question_attempt_id;
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$studentAnswers = $run->toArray();
				$details['studentAnswers'] = $studentAnswers;


				$result->details = $details;
				$result->status = 1;
				$result->message = "Successfully Fetched";
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Some thing went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		public function createOptmizerTest($data)
		{
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				/*
				* fetch all wrong and unattempted questions
				*/
				$select = $sql->select();
				$select->from('question_attempt');
				$select->columns(array('question_id', 'marks', 'neg_marks'));
				$select->where(array('correct' => 0, 'attempt_id' => $data->attempt_id));

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$questions = $run->toArray();
				$numbersofQuestions = count($questions);

				if ($numbersofQuestions) {
					//creating array of question ids
					$question_ids = array();
					foreach ($questions as $question) {
						$question_ids[] = $question['question_id'];
					}

					/*
					* fetch time test
					*/
					$statement = "SELECT COUNT(question_attempt.attempt_id) AS number_of_question, test_paper.time AS total_time, (test_paper.time / COUNT(question_attempt.attempt_id)) AS time FROM test_paper_attempts LEFT JOIN question_attempt on question_attempt.attempt_id = test_paper_attempts.id
					LEFT JOIN test_paper on test_paper.id = test_paper_attempts.test_paper_id
					WHERE test_paper_attempts.id = ".$data->attempt_id;
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$time = $run->toArray();
					$number_of_question = intval($time[0]['number_of_question']);
					$total_time = intval($time[0]['total_time']);
					$time = intval($time[0]['time']);
					// print_r($numbersofQuestions);
					// echo "<br>";
					// print_r($number_of_question);
					// echo "<br>";
					// print_r($total_time);
					// echo "<br>";
					// print_r($time);
					$time=($total_time/$number_of_question)*$numbersofQuestions;


					/*
					* creating otmizer question paper
					*/
					$insert_test_paper = new TableGateway('test_paper', $adapter, null, new HydratingResultSet());

					$test_paper_details = array(
						'name' => "Optmizer Question",
						'type'	=>	0,
						'class'	=>	0,
						'time'	=>	$time,
						'total_attempts'	=>	15,
						'start_time'	=>	0,
						'end_time'	=>	0,
						'Instructions'	=>	"Optmizer Question Test",
						'status'	=>	1,
						'user_id'	=>	$data->userId,
						'created'	=>	time()
					);
					$insert_test_paper->insert($test_paper_details);
					$test_paper_id = $insert_test_paper->getLastInsertValue();


					$insert_section = new TableGateway('section', $adapter, null, new HydratingResultSet());
					$insert_section->insert(array(
						'name'	=>	'Section 1',
						'total_question'	=>	count($questions),
						'test_paper'	=>	$test_paper_id
					));
					$section_id = $insert_section->getLastInsertValue();

					foreach($questions as $question) {
						$insert = new TableGateway('test_paper_questions', $adapter, null, new HydratingResultSet());

						$insert->insert(array(
							'test_paper_id'	=>	$test_paper_id,
							'section_id'	=>	$section_id,
							'question_id'	=>	$question['question_id'],
							'max_marks'	=>	$question['marks'],
							'neg_marks'	=>	$question['neg_marks'],
							'created'	=>	time()
						));
						$id = $insert->getLastInsertValue();
					}

					// set optmizer test id of whis this test is optmizer
					$update = $sql->update();
					$update->table('test_paper_attempts');
					$update->set(array(
						'optimizer_id'	=>	$test_paper_id
					));
					$update->where(array(
						'id' => $data->attempt_id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$run = $statement->execute();

					$result->test_paper_id = $test_paper_id;

					$result->status = 1;
					$result->message = "Successfully ";
				}
				else {
					$result->status = 0;
					$result->message = "No questions.";
				}
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Some thing went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function fetchDetailedQuestionWiseReport($attempt_id)
		{
			$result = new stdClass();

			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->columns(array('id', 'question_id', 'section_id', 'question_type', 'question_status', 'time', 'correct', 'marks', 'neg_marks'));
				$select->from('question_attempt');
				$select->where(array(
					'attempt_id' => $attempt_id
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run  = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$question_attempts = $run->toArray();

				$detailedQuestionWiseReportData = array();
				// $parents = array();
				foreach ($question_attempts as $key => $question_attempt_data) {

					$select = $sql->select();
					$select->columns(array('id', 'question', 'description', 'type', 'quality_check', 'ideal_time', 'parent_id', 'user_id', 'created'));
					$select->from('questions');
					$select->where(array(
						'id' => $question_attempt_data['question_id']
					));
					$statement = $sql->getSqlStringForSqlObject($select);
					$run  = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$question_data = $run->toArray();
					$question_data = $question_data[0];

					//array_push($parents, $question_data['parent_id']);


					$select = $sql->select();
					$select->columns(array( 'id', 'option', 'answer', 'number'));
					$select->from('options');
					$select->where(array(
						'deleted'	=>	0,
						'question_id'	=>	$question_attempt_data['question_id']
					));
					$statement = $sql->getSqlStringForSqlObject($select);
					$run  = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$option_data = $run->toArray();

					$select = $sql->select();
					$select->columns(array('id', 'question_attempt_id', 'question_id', 'option_id'));
					$select->from('answers_of_question_attempts');
					$select->where(array(
						'question_attempt_id'	=>	$question_attempt_data['id']
					));
					$statement = $sql->getSqlStringForSqlObject($select);

					// $statement = 'SELECT options.option FROM `answers_of_question_attempts` LEFT JOIN options ON options.id = answers_of_question_attempts.option_id WHERE answers_of_question_attempts.question_attempt_id = '.$attempt_id;
					$run  = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$student_answer = $run->toArray();

					$question_attempt_data['question'] = $question_data['question'];
					$question_attempt_data['parent_id'] = $question_data['parent_id'];
					$question_attempt_data['solution'] = $question_data['description'];
					$question_attempt_data['options'] = $option_data;
					$question_attempt_data['student_answer'] = $student_answer;
					if($question_data['parent_id'] != 0) {

					$select = $sql->select();
					$select->columns(array('id', 'question', 'description', 'type', 'quality_check', 'ideal_time', 'parent_id', 'user_id', 'created'));
					$select->from('questions');
					$select->where(array(
						'id' => $question_attempt_data['parent_id']
					));
					$statement = $sql->getSqlStringForSqlObject($select);
					$run  = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$question_data_p = $run->toArray();
					$question_data_p = $question_data_p[0]['question'];
						$question_attempt_data['parent'] = $question_data_p;//'Passage';
					}


					$detailedQuestionWiseReportData[] = $question_attempt_data;
				}

				return $detailedQuestionWiseReportData;
				$result->status = 1;
				$result->message = "Successfully Fetched.";
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Some thing went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function fetchOptmizerReport($data)
		{
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				// SELECT optimizer_test.id as optimizer_attempt_id, main_test.id as main_test_attempt_id FROM test_paper_attempts AS optimizer_test left JOIN test_paper_attempts as main_test on main_test.optimizer_id = optimizer_test.test_paper_id
				$select->from(array('optimizer_test' => 'test_paper_attempts'))
						->join(array('main_test' => 'test_paper_attempts'), 'main_test.optimizer_id = optimizer_test.test_paper_id', array('optimizer_attempt_id' => 'id'), 'left');
				$where = new $select->where();
				$where->equalTo('optimizer_test.test_paper_id', $data->paper_id);
				$where->equalTo('main_test.student_id', $data->userId);
				$select->columns(array('main_test_attempt_id' => 'id'));
				$select->where($where);
				$statement = $sql->getSqlStringForSqlObject($select);
				// $statement = 'SELECT * FROM test_paper_attempts WHERE test_paper_id = '.$data->paper_id;
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$attempt_data = $run->toArray();
				$attempt_data = $attempt_data[0];
				// Unattempted Question Result
				$statement = "SELECT COUNT(id) as total, SUM(correct) AS correct, SUM(IF(question_status = 3, 1, 0)) AS attempted, SUM(IF(question_status = 3 AND correct = 0, 1, 0)) AS wrong FROM question_attempt WHERE question_attempt.attempt_id= ".$attempt_data['main_test_attempt_id']." and question_attempt.question_id IN (SELECT question_attempt.question_id FROM question_attempt WHERE question_attempt.attempt_id=".$attempt_data['optimizer_attempt_id']."  and question_attempt.question_status < 3)";
				// die($statement);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$result->unattempted_result = $rows[0];


				// Wrong Question Result
				$statement = "SELECT COUNT(id) as total, SUM(correct) AS correct, SUM(IF(question_status = 3, 1, 0)) AS attempted, SUM(IF(question_status = 3 AND correct = 0, 1, 0)) AS wrong FROM question_attempt WHERE question_attempt.attempt_id= ".$attempt_data['main_test_attempt_id']."  and question_attempt.question_id IN (SELECT question_attempt.question_id FROM question_attempt WHERE question_attempt.attempt_id=".$attempt_data['optimizer_attempt_id']." AND question_attempt.question_status = 3 AND question_attempt.correct=0)";
				// die($statement);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$result->wrong_questoions_result = $rows[0];



				$result->status = 1;
				$result->message = "Successfully Fetched";
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Some thing went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function fetchLeaderboard($data)
		{
			$result = new stdClass();
			$result->_tbegin = microtime(true);
			try
			{
				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('test_paper_attempts')
						->join(array('p'=>'profile'),'p.user_id = test_paper_attempts.student_id',array('first_name','last_name','image'),'left')
						->join(array('qa'=>'question_attempt'),'qa.attempt_id=test_paper_attempts.id',array('correct'=>new Expression('SUM(correct)'),'attempt'=>new Expression('SUM(IF(question_status > 1,1,0))')));

				$select->where->equalTo('test_paper_id',$data->test_paper)->equalTo('test_paper_attempts.complete','2');
				$select->order('marks_archived DESC');
				$select->group('student_id');
				$select->offset(0);
				$select->limit(20);

				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$count = $run->toArray();

                // pp(array_column($count, 'id'));

				$result->data = $count;

        //         $sql = new Sql($adapter);
        //         $select = $sql->select();
        //         $select->from('test_paper_attempts')
        //                 ->join(array('p'=>'profile'),'p.user_id = test_paper_attempts.student_id',array('first_name','last_name','image'),'left')
        //                 ->join(array('qa'=>'question_attempt'),'qa.attempt_id=test_paper_attempts.id',array('correct'=>new Expression('SUM(correct)'),'attempt'=>new Expression('SUM(IF(question_status > 1,1,0))')));

        //         $select->where->equalTo('test_paper_id',$data->test_paper)->equalTo('test_paper_attempts.complete','2')->equalTo('test_paper_attempts.student_id', $data->userId);
        //         $select->order('marks_archived DESC');
        //         $select->group('student_id');


        //         $selectString = $sql->getSqlStringForSqlObject($select);

        //         $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        //         $count = $run->toArray();
        //         if(count($count)){
				    // $a = array_column($count, 'id')[0];
        //             echo in_array($a, $result->data);
        //         }

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('test_paper')->join(array('s'=>'section'),'s.test_paper = test_paper.id',array('count'=>new Expression('SUM(total_question)')));

				$select->where->equalTo('test_paper.id',$data->test_paper);

				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$paper = $run->toArray();

				$result->paper = $paper;

				$result->status = 1;
				$result->message = "Result Successfully Fetched";
				$result->_tend = microtime(true);

				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Some thing went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}

	}
?>
