<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	//including connection.php file to make connection with database
	require_once 'Connection.php';

	class TestPaperDeliverApi extends Connection {

		/**
		* Function to buy test programs
		*
		* @author Utkarsh Pandey
		* @date 25-07-2015
		* @param JSON with all required details
		* @return JSON status and data(Associates program with the student id)
		**/
		public function fetchSections($data) {

			$result = new stdClass();

			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				
				// $statement = "SELECT sum(total_question) AS total_question FROM section WHERE deleted=0 AND test_paper=".$data->test_paper_id;

				/*$select = $sql->select();
						

				$select->from('section');
         		$select->where(array(
					'deleted'	=>	0,
					'test_paper'	=>	$data->test_paper_id
				));
         		$select->columns(array('total_question' => new Expression('SUM(total_question)')));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$total_question = $run->toArray();*/


				$select = $sql->select();
				
				$select->from("test_paper");
				$select->where(array("deleted"	=>	0,"status"	=>	1,"id" => $data->test_paper_id));
				$select->columns(array("id","name","time",'Instructions','type','start_time','end_time'));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$test_paper_details = $run->toArray();
				

				$select = $sql->select();
				
				$select->from("section")
						->join(array('test_paper_labels'=>'test_paper_labels'),'section.id=test_paper_labels.section_id',array(),'left');
				$select->where(array("section.deleted"	=>	0,"section.test_paper" => $data->test_paper_id));
				$select->columns(array("id","name","cut_off_marks","total_question"=>new Expression('SUM(test_paper_labels.number_of_question)')));
				$select->group('section.id');
				// $select->order('id DESC');
				$statement = $sql->getSqlStringForSqlObject($select);
				//die($statement);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$sections = $run->toArray();

				/**
				*
				* @author Ravi Arya
				* @date 27-1-2016
				* @param 
				* @return max marks, neg marks
				* @todo nothing
				*
				**/				
				$total_question = 0;
				$x=0;
				foreach ( $sections as $section ) {
					# code...
					$total_question += intval($section['total_question']);
					$select = $sql->select();
					$select->from("test_paper_questions");
					$select->where(array("test_paper_id"	=>	$data->test_paper_id,"section_id" => $section['id']))
							->group('lable_id');
					//$select->columns(array("id","name","cut_off_marks","total_question"));
					$statement = $sql->getSqlStringForSqlObject($select);
					//die($statement);
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$labels = $run->toArray();
					$count = count($labels);
					if($count>=0){
						$max_marks = $labels[0]['max_marks'];
						$neg_marks = $labels[0]['neg_marks'];
						$i = $j = 0;
						foreach ($labels as $label) {
							# code...
							if($max_marks == $label['max_marks']){
								$i++;
							}
							if($neg_marks == $label['neg_marks']){
								$j++;
							}

						}

						$sections[$x]['max_marks'] = 'N/A';
						$sections[$x]['neg_marks'] = 'N/A';

						if( $i == $count ){
							$sections[$x]['max_marks'] = $max_marks;							
						}
						if( $j == $count ){							
							$sections[$x]['neg_marks'] = $neg_marks;
						}
						if( $i!=$count && $j!=$count ){
							$sections[$x]['max_marks'] = 'N/A';
							$sections[$x]['neg_marks'] = 'N/A';
						}
					}
					$x++;
				}				

				$result->total_question = intval($total_question);
				$result->status = 1;
				$result->message = "Operation Successful.";
				//$result->total_question = $total_question[0]["total_question"];
				$result->sections = $sections;
				$result->test_paper_details = $test_paper_details[0];
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}
		
		}

		/**
		* Function to launch test-Paper
		*
		* @author Utkarsh Pandey
		* @date 30-07-2015
		* @param JSON with all required details
		* @return JSON status and data(Associates program with the student id)
		**/
		public function launchPaper($req) {

			$result = new stdClass();
			$result->Q = array();

			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				/*
				* Fetching Test Paper Details
				*/
				$select = $sql->select();
				
				$select->from("test_paper");
				$select->where(array("deleted"	=>	0,"status"	=>	1,"id" => $req->testPaper));
				$select->columns(array("id","name","time",'type','start_time','end_time'));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$test_paper_details = $run->toArray();

				
				/*
				* checking that is the test paper in resume state or not
				complete
				{
					0>started 
					1>submitted
					2>submitted & result created
				}
				*/

				$selectResumePaper = $sql->select();
				$selectResumePaper->from('test_paper_attempts');
				$where = new $selectResumePaper->where();
				$where->equalTo('test_paper_id',$req->testPaper);
				$where->equalTo('student_id',$req->userId);
				$where->equalTo('complete',0);
				$selectResumePaper->where($where);
				$selectResumePaper->columns(array('id','start_time','end_time'));
				$statement = $sql->getSqlStringForSqlObject($selectResumePaper);
				
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$selectResumePaperAttempt = $run->toArray();
				$index = 0;
				
				if(count($selectResumePaperAttempt) == 0) {
					/*
					* Save the test paper attempt by sudent
					*/

					// INSERT INTO `test_paper_attempts`(`test_paper_id`, `student_id`, `start_time`) VALUES ("$req->testPaper",$req->userId,time())
					$insert_test_paper_attempt = new TableGateway('test_paper_attempts', $adapter, null, new HydratingResultSet());
					
					$insert_test_paper_attempt->insert(array(
						'test_paper_id' => $req->testPaper,
						'student_id'	=>	$req->userId,
						// 'start_time'	=>	time()
					));

					$attempt_id = $insert_test_paper_attempt->getLastInsertValue();
					$result->attempt_id = $attempt_id;

					/*
					* fetching section of test paper
					*/
					$select = $sql->select();
					$select->from('section');
					$where = new $select->where();
					$where->equalTo('test_paper',$req->testPaper);
					$select->where($where);
					$select->columns(array('id','name','total_question'));
					$statement = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$sections = $run->toArray();
					
					/*
					* fetching saved question of test paper
					*/
					$select = $sql->select();
					$select->from('test_paper_questions');
					$where = new $select->where();
					$where->equalTo('test_paper_id',$req->testPaper);
					$select->where($where);
					$select->columns(array('section_id','lable_id','question_id', 'max_marks', 'neg_marks'));
					$statement = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$questions = $run->toArray();

					$total_marks = 0;
					$data = array();
					foreach ($sections as $section) {
						
						$Section = new stdClass();
						$Section->section = $section;
						$Section->section["questionData"] = array();
						foreach ($questions as $question) {

							if($section["id"] == $question["section_id"]) {
								$index++;
								$total_marks = $total_marks + $question['max_marks'];

								/*
								* fetching question one by one
								*/
								$select = $sql->select();
								$select->from('questions');
								$where = new $select->where();
								$where->equalTo('delete',0);
								// $where->equalTo('quality_check',0);
								$where->equalTo('id',$question["question_id"]);
								$select->where($where);
								$select->columns(array('id','question','type','description'));
								$statement = $sql->getSqlStringForSqlObject($select);

								$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
								$temp = $run->toArray();
								$question['question'] = $temp[0]['question'];
								$question['type'] = $temp[0]['type'];
								// $question['description'] = $temp[0]['description'];


								/*
								* If question is in a paragrap then this will be executed
								*/
								if($temp[0]['type'] == 11) {
									$select = $sql->select();
									$select->from('questions');
									$where = new $select->where();
									$where->equalTo('delete',0);
									// $where->equalTo('quality_check',0);
									$where->equalTo('parent_id',$temp[0]['id']);
									$select->where($where);
									$select->columns(array('id','question','type','description'));
									$statement = $sql->getSqlStringForSqlObject($select);

									$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
									$childQuestions = $run->toArray();
									
									foreach($childQuestions as $childQuestion) {
										$select = $sql->select();
										$select->from('options');
										$where = new $select->where();
										$where->equalTo('question_id',$childQuestion['id']);
										$where->equalTo('deleted',0);
										$select->where($where);
										$select->columns(array('id','option','number','answer'));
										$statement = $sql->getSqlStringForSqlObject($select);
										$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
										$options = $run->toArray();


										$options = $this->shuffleOptions($childQuestion['type'], $options);
										$question['options'] = $options;
										$question['passage'] = $question['question'];
										$question['question'] = $childQuestion['question'];
										$question['description'] = $childQuestion['description'];
										$question['child_type'] = $childQuestion['type'];
										array_push($Section->section["questionData"],$question);
										
										/*
										* Saving question-attempt by student 
										*/			
										
										// INSERT INTO `question_attempt`(`attempt_id`, `question_id`, `question_type`, `marks`, `neg_marks`,student_id) VALUES ($attempt_id,$data->userId)
										$insert_test_paper_attempt = new TableGateway('question_attempt', $adapter, null, new HydratingResultSet());
										
										$insert_test_paper_attempt->insert(array(
											'attempt_id' => $attempt_id,
											'question_id'	=>	$question["question_id"],
											'section_id'	=> $section['id'],
											'question_type'	=>	$question['type'],
											'marks'	=>	$question['max_marks'],
											'neg_marks'	=>	$question['neg_marks'],
											'student_id'	=>	$req->userId
										));
										$question_attempt_id = $insert_test_paper_attempt->getLastInsertValue();
										$result->Q[$index] = array();
										$result->Q[$index]['status'] = 0;
										$result->Q[$index]['answers'] = array();
										$result->Q[$index]['type'] = $question['type'];
										$result->Q[$index]['question_id'] = $question['question_id'];
										$result->Q[$index]['time'] = 0;
										$result->Q[$index]['question_attempt_id'] = $question_attempt_id;

									}

								}
								else {
									
									/*
									* fetching options for every question
									*
									*/

									$select = $sql->select();
									$select->from('options');
									$where = new $select->where();
									$where->equalTo('question_id',$question["question_id"]);
									$where->equalTo('deleted',0);
									$select->where($where);
									$select->columns(array('id','option','number','answer'));
									$statement = $sql->getSqlStringForSqlObject($select);
									$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
									$options = $run->toArray();
									
									
									$options = $this->shuffleOptions($question['type'], $options);
									
									$question['options'] = $options;

									array_push($Section->section["questionData"],$question);
									
									/*
									* Saving question-attempt by student 
									*/			
									
									// INSERT INTO `question_attempt`(`attempt_id`, `question_id`, `question_type`, `marks`, `neg_marks`,student_id) VALUES ($attempt_id,$data->userId)
									$insert_test_paper_attempt = new TableGateway('question_attempt', $adapter, null, new HydratingResultSet());
									
									$insert_test_paper_attempt->insert(array(
										'attempt_id' => $attempt_id,
										'question_id'	=>	$question["question_id"],
										'section_id'	=>	$section['id'],
										'question_type'	=>	$question['type'],
										'marks'	=>	$question['max_marks'],
										'neg_marks'	=>	$question['neg_marks'],
										'student_id'	=>	$req->userId
									));
									$question_attempt_id = $insert_test_paper_attempt->getLastInsertValue();
									$result->Q[$index] = array();
									$result->Q[$index]['status'] = 0;
									$result->Q[$index]['answers'] = array();
									$result->Q[$index]['type'] = $question['type'];
									$result->Q[$index]['question_id'] = $question['question_id'];
									$result->Q[$index]['time'] = 0;
									$result->Q[$index]['question_attempt_id'] = $question_attempt_id;

								}
							}
						}
						array_push($data, $Section);
					}


					$update = $sql->update();
					$update->table('test_paper_attempts');
					$update->set(array(
						'total_marks' => $total_marks,
						'start_time'	=>	time()
					));
					$update->where(array(
						'id' => $attempt_id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$run = $statement->execute();
				}
				else if(count($selectResumePaperAttempt) == 1) {

					$result->attempt_id = $selectResumePaperAttempt[0]['id'];

					// return time() - $selectResumePaperAttempt[0]['start_time'];


					date_default_timezone_set('Asia/Kolkata');
					
					if($test_paper_details[0]['type'] == '5'){

						$end_time = date_create_from_format('d F Y - H:i', $test_paper_details[0]['end_time']);							
						$end_time = intval(date_format($end_time, 'U'));

						if(time() > $end_time){
							$test_paper_details[0]['time'] = 0;

						}else if(($end_time - time()) < $test_paper_details[0]['time'] * 60){

							$test_paper_details[0]['time'] = ($end_time-time())/60;							
							
						}else{
							$test_paper_details[0]['time'] = ($test_paper_details[0]['time'] *60 -  (time() - $selectResumePaperAttempt[0]['start_time']))/60;
						}
					}else{
						$test_paper_details[0]['time'] = ($test_paper_details[0]['time'] * 60 -  (time() - $selectResumePaperAttempt[0]['start_time']))/60;
					}
					/*
					* fetching details of test paper
					*/
					$select = $sql->select();
					$select->from('section');
					$where = new $select->where();
					$where->equalTo('test_paper',$req->testPaper);
					$select->where($where);
					$select->columns(array('id','name','total_question'));
					$statement = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$sections = $run->toArray();

					
					/*
					* fetching saved question of test paper
					*/
					$select = $sql->select();
					$select->from('test_paper_questions');
					$where = new $select->where();
					$where->equalTo('test_paper_id',$req->testPaper);
					$select->where($where);
					$select->columns(array('section_id','lable_id','question_id'));
					$statement = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$questions = $run->toArray();

					// echo '<pre>';
					// print_r($this->getQ($questions));
					// echo '</pre>';
					// die();
					
					$result->questions=$this->getQ($questions);
					
					$data = array();
					foreach ($sections as $section) {
						
						$Section = new stdClass();
						$Section->section = $section;
						$Section->section["questionData"] = array();
						foreach ($questions as $question) {

							if($section["id"] == $question["section_id"]) {
								$index++;

								/*
								* fetching question one by one
								*/
								$select = $sql->select();
								$select->from('questions');
								$where = new $select->where();
								$where->equalTo('delete',0);
								//$where->equalTo('delete',0);
								// $where->equalTo('quality_check',0);
								$where->equalTo('id',$question["question_id"]);	
								$where->equalTo('parent_id','0');	
								$select->where($where);
								$select->columns(array('id','question','type','description'));
								$statement = $sql->getSqlStringForSqlObject($select);

								$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
								$temp = $run->toArray();
								
								
								
								$question['question'] = $temp[0]['question'];
								$question['description'] = $temp[0]['description'];
								$question['type'] = $temp[0]['type'];


								
								
								/*
								* If question is in a paragrap then this will be executed
								*/
								if($temp[0]['type'] == 11) {
									$select = $sql->select();
									$select->from('questions');
									$where = new $select->where();
									$where->equalTo('delete',0);
									// $where->equalTo('quality_check',0);
									$where->equalTo('parent_id',$temp[0]['id']);	
									$select->where($where);
									$select->columns(array('id','question','type','description'));
									$statement = $sql->getSqlStringForSqlObject($select);

									$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
									$childQuestions = $run->toArray();
									
									
									foreach($childQuestions as $childQuestion) {
										$select = $sql->select();
										$select->from('options');
										$where = new $select->where();
										$where->equalTo('question_id',$childQuestion['id']);
										$where->equalTo('deleted',0);
										$select->where($where);
										$select->columns(array('id','option','number','answer'));
										$statement = $sql->getSqlStringForSqlObject($select);
										$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
										$options = $run->toArray();

										$options = $this->shuffleOptions($childQuestion['type'], $options);
										// shuffle($options);
										$question['options'] = $options;
										$question['passage'] = $question['question'];
										$question['question'] = $childQuestion['question'];
										$question['description'] = $childQuestion['description'];
										$question['child_type'] = $childQuestion['type'];
										array_push($Section->section["questionData"],$question);
										
										/*
										* fetching question-attempt data of every question one by one
										*/
										$select = $sql->select();
										$select->from('question_attempt');
										$where = new $select->where();
										$where->equalTo('attempt_id',$selectResumePaperAttempt[0]['id']);
										$where->equalTo('question_id',$question["question_id"]);
										$select->where($where);
										$select->columns(array('id','question_status','time'));
										$statement = $sql->getSqlStringForSqlObject($select);
										$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
										$question_attempt = $run->toArray();
										
										$result->Q[$index] = array();
										$result->Q[$index]['status'] = $question_attempt[0]['question_status'];
										$result->Q[$index]['type'] = $question['type'];
										$result->Q[$index]['question_id'] = $question['question_id'];
										$result->Q[$index]['time'] = $question_attempt[0]['time'];
										$result->Q[$index]['question_attempt_id'] = $question_attempt[0]['id'];
										
										$select = $sql->select();
										$select->from('answers_of_question_attempts');
										$where = new $select->where();
										$where->equalTo('question_attempt_id',$question_attempt[0]['id']);
										$select->where($where);
										$select->columns(array('option_id'));
										$statement = $sql->getSqlStringForSqlObject($select);
										$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
										$options = $run->toArray();

										$result->Q[$index]['answers'] = array();
										foreach ($options as $option) {
											array_push($result->Q[$index]['answers'], $option['option_id']);
										}
									}

								}
								else {

									
									/*
									* fetching options for every question
									*
									*/
									$select = $sql->select();
									$select->from('options');
									$where = new $select->where();
									$where->equalTo('question_id',$question["question_id"]);
									$where->equalTo('deleted',0);
									$select->where($where);
									$select->columns(array('id','option','number','answer'));
									$statement = $sql->getSqlStringForSqlObject($select);
									$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
									$options = $run->toArray();

									$options = $this->shuffleOptions($question['type'], $options);
									// shuffle($options);
									$question['options'] = $options;

									array_push($Section->section["questionData"],$question);

									/*
									* fetching question-attempt data of every question one by one
									*/
									$select = $sql->select();
									$select->from('question_attempt');
									$where = new $select->where();
									$where->equalTo('attempt_id',$selectResumePaperAttempt[0]['id']);
									$where->equalTo('question_id',$question["question_id"]);
									$select->where($where);
									$select->columns(array('id', 'marks', 'neg_marks','question_status','time'));
									$statement = $sql->getSqlStringForSqlObject($select);
									$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
									$question_attempt = $run->toArray();
									
									$result->Q[$index] = array();
									$result->Q[$index]['status'] = $question_attempt[0]['question_status'];
									$result->Q[$index]['type'] = $question['type'];
									$result->Q[$index]['question_id'] = $question['question_id'];
									$result->Q[$index]['time'] = $question_attempt[0]['time'];
									$result->Q[$index]['question_attempt_id'] = $question_attempt[0]['id'];
									
									$select = $sql->select();
									$select->from('answers_of_question_attempts');
									$where = new $select->where();
									$where->equalTo('question_attempt_id',$question_attempt[0]['id']);
									$where->equalTo('question_id',$question['question_id']);
									$select->where($where);
									$select->columns(array('option_id'));
									$statement = $sql->getSqlStringForSqlObject($select);
									$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
									$options = $run->toArray();

									$result->Q[$index]['answers'] = array();
									foreach ($options as $option) {
										array_push($result->Q[$index]['answers'], $option['option_id']);
									}
								}

							}
						}
						array_push($data, $Section);
					}
				}

				$result->data = $data;
				$result->test_paper_details = $test_paper_details[0];
				$result->status = 1;
				$result->message = "Successfully Done";

				
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}
		
		}

		/**
		* this to submit paper
		*
		* @author Utkarsh Pandey
		* @date 21-08-2015
		* @param JSON with all required details
		* @return JSON with status and message
		**/
		public function submitPaper($data) {
			$result = new stdClass();
			
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('test_paper_attempts');
				$select->where(array(
					'complete' => 0,
					'id' => $data->attempt_id
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$atttempt = $run->toArray();
				if(count($atttempt) == 1) {
					
					foreach ($data->rows as $row) {
						$req = new stdClass();
						$req->row = $row;
						$req->attempt_id = $data->attempt_id;
						$req->testPaper = $data->testPaper;
						$this->saveQuestionResponse($req);
					}					

					$update = $sql->update();
					$update->table('test_paper_attempts');
					$update->set(array(
						'end_time'	=>	time(),
						'complete'	=>	1
					));
					$update->where(array(
						'id' => $data->attempt_id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$run = $statement->execute();

					$result->status = 1;
					$result->message = "Successfully Update.";
					$result->result_status = $this->genrateResultOfAttemptId($data->attempt_id);
				}
				else {
					$result->status = 0;
					$result->message = "Some Thing Went Wrong.";
				}
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* this function update question status by student response 
		*
		* @author Utkarsh Pandey
		* @date 21-08-2015
		* @param JSON with all required details
		* @return JSON with status and message
		**/
		public function saveQuestionResponse($data) {
			$result = new stdClass();
			
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('test_paper_attempts');
				$select->where(array(
					'id' => $data->attempt_id
				));
				$select->columns(array('complete','test_paper_id'));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$attempt_data = $run->toArray();

				if($attempt_data[0]['complete'] == 0 && $attempt_data[0]['test_paper_id'] == $data->testPaper) {
					$update = $sql->update();
					$update->table('question_attempt');
					$update->set(array(
						'question_status'	=>	 $data->row->status,
						'time'	=>	 $data->row->time
					));

					$update->where(array(
						'attempt_id' =>  $data->attempt_id,
						'question_id' =>  $data->row->question_id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$run = $statement->execute();
					
					$select = $sql->select();
					$select->from('question_attempt');
					$select->where(array(
						'attempt_id' =>  $data->attempt_id,
						'question_id' =>  $data->row->question_id
					));
					$select->columns(array('id'));
					$statement = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$question_attempt_id = $run->toArray();
					$question_attempt_id = $question_attempt_id[0]['id'];
					
					$delete = $sql->delete('answers_of_question_attempts');

					$where = new $delete->where();
					$where->equalTo('question_attempt_id',$question_attempt_id);
					$delete->where($where);

					$statement = $sql->getSqlStringForSqlObject($delete);
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

					// foreach ($data->row->answers as $answer) {
						
					// }
					
						// INSERT INTO `answers_of_question_attempts`( `question_attempt_id`, `question_id`, `option_id`) VALUES ($data->question_attempt_id,$data->row->question_id,$data->answer)
						// return $data->row;
						if(!empty($data->row->answers)) {
							foreach ($data->row->answers as $option_id) {
								$insert = new TableGateway('answers_of_question_attempts', $adapter, null, new HydratingResultSet());
								$insert->insert(array(
									'question_attempt_id' => $question_attempt_id,
									'question_id' => $data->row->question_id,
									'option_id' => $option_id
								));
								$question_attempt_answer_id = $insert->getLastInsertValue();
							}
						}

					$result->status = 1;
					$result->message = "Successfully Update.";
					$result->question_id = $data->row->question_id;
				}
				else {
					$result->status = 0;
					$result->message = "Can't genrate result of given attempt.";
				}
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		/**
		* this function create result of test paper by given attempt
		*
		* @author Utkarsh Pandey
		* @date 25-09-2015
		* @param test paper attempt id
		* @return JSON with status and message
		**/
		public function genrateResultOfAttemptId($attempt_id) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				/*
				* fetching data of given attempt id
				*/
				$select = $sql->select();
				$select->from('test_paper_attempts')
				->join(['users' => 'users'], 'users.id = test_paper_attempts.student_id', ['name' => new Expression('concat(users.first_name, " ", users.last_name)'), 'email'], 'left');
				$select->where(array(
					'test_paper_attempts.id' => $attempt_id,
					'test_paper_attempts.complete' => 1
				));
				// $select->columns(array('complete','test_paper_id'));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$attempt_data = $run->toArray();
				
				
				/*
				* checking that exam is completed or not and result already genrated or not
				*/
				if (count($attempt_data) == 1) {
					/*
					* fetching attempted questions
					*/
					
					$select = $sql->select();
					$select->from('question_attempt');
					$select->where('attempt_id ='.$attempt_data[0]['id']);
					$select->where('question_status IN(2,3)');
					//$select->where($where);
					// $select->columns(array('id','question_status','time'));
					$statement = $sql->getSqlStringForSqlObject($select);
					
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$question_attempts = $run->toArray();

					foreach ($question_attempts as $question_attempt) {
						/*
						* fetching options of question
						*/
						$select = $sql->select();
						$select->from('options');
						$where = new $select->where();
						$where->equalTo('question_id',$question_attempt["question_id"]);
						if($question_attempt["question_type"] == '12'){
							$where->equalTo('option',1);
						}else{
							$where->equalTo('answer',1);
						}
						
						$where->equalTo('deleted',0);
						$select->where($where);
						$select->columns(array('id','option','number','answer'));
						$statement = $sql->getSqlStringForSqlObject($select);
						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$options = $run->toArray();
						
						/*
						* fetching answers given by student
						*/
						$select = $sql->select();
						$select->from('answers_of_question_attempts');
						$where = new $select->where();
						$where->equalTo('question_attempt_id',$question_attempt['id']);
						$where->equalTo('question_id',$question_attempt['question_id']);
						$select->where($where);
						$select->columns(array('option_id'));
						$statement = $sql->getSqlStringForSqlObject($select);
						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$attempt_options = $run->toArray();

						switch ($question_attempt["question_type"]) 
						{
							case '1':

								$corrrect_answer = 0;
								$incorrrect_answer = 0;
								if (count($options) == count($attempt_options)) {

									foreach ($attempt_options as $attempt_option) {
										foreach ($options as $option) {
											if ($option['id'] == $attempt_option["option_id"]) {
												$corrrect_answer++;
											}
										}
										if ($corrrect_answer == 0) {
											$incorrrect_answer++;
										}
									}
									if ($corrrect_answer && $corrrect_answer == 1) {
										$this->updateQuestionAttemptCorrect($question_attempt['id']);
									}
								}
								break;
							
							case '2':
								$a = array();
								$b = array();

								foreach ($attempt_options as $attempt_option) {
									array_push($a, $attempt_option['option_id']);
								}
								foreach ($options as $attempt_option) {
									array_push($b, $attempt_option['id']);
								}
								sort($a);
								sort($b);
								if($a === $b && count($a)){
									$this->updateQuestionAttemptCorrect($question_attempt['id']);
								}
								break;
							
							case '4'://sequencing type
								$corrrect_answer = 0;
								$incorrrect_answer = 0;
								if(count($options) == count($attempt_options)){
									for($i = 0; $i < count($options); $i++) {
											
											// print_r($options[$i]['id']);
											// print_r($attempt_options[$i]['option_id']);
										if ($options[$i]['id'] == $attempt_options[$i]["option_id"]) {
											// die();
											// print_r($attempt_option["option_id"]);
											$corrrect_answer++;
											// echo $corrrect_answer.'right<br>';
										}
										else {
											// print_r($option['id']);
											// print_r($attempt_option["option_id"]);
											$incorrrect_answer++;
											// echo $incorrrect_answer.'wrong<br>';
										}
									}
								}else {$incorrrect_answer++;}
								
								if ($incorrrect_answer == 0 && $corrrect_answer == count($attempt_options)) {
									$this->updateQuestionAttemptCorrect($question_attempt['id']);
								}
								break;
							
							case '5'://matching drag n drop
								$corrrect_answer = 0;
								$incorrrect_answer = 0;
								if(count($options) == count($attempt_options)){
									for($i = 0; $i < count($options); $i++) {
										if ($options[$i]['id'] == $attempt_options[$i]["option_id"]) {
											$corrrect_answer++;
										}
										else {
											$incorrrect_answer++;
										}
									}
								}else {$incorrrect_answer++;}
								
								if ($incorrrect_answer == 0 && $corrrect_answer == count($attempt_options)) {
									$this->updateQuestionAttemptCorrect($question_attempt['id']);
								}
								break;

							case '6'://fb
								$corrrect_answer = 0;
								$incorrrect_answer = 0;
								if(count($options) == count($attempt_options)){
									for($i = 0; $i < count($options); $i++) {
										if (strtolower($options[$i]['option']) == strtolower($attempt_options[$i]["option_id"])) {
											$corrrect_answer++;
										}
										else {
											$incorrrect_answer++;
										}
									}
								}else {
									$incorrrect_answer++;
								}

								if ($incorrrect_answer == 0 && $corrrect_answer==count($options)) {
									$this->updateQuestionAttemptCorrect($question_attempt['id']);
								}
								break;

							case '7'://Drop down
								$corrrect_answer = 0;
								$incorrrect_answer = 0;
								if(count($options) == count($attempt_options)){
									for($i = 0; $i < count($options); $i++) {
										if ($options[$i]['id'] == $attempt_options[$i]["option_id"]) {
											// print_r($option['id']);
											// print_r($attempt_option["option_id"]);
											$corrrect_answer++;
											// echo $corrrect_answer.'right<br>';
										}
										else {
											$incorrrect_answer++;
										}
									}
								}else{$incorrrect_answer++;}

								if ($incorrrect_answer == 0 && $corrrect_answer==count($options)) {
									$this->updateQuestionAttemptCorrect($question_attempt['id']);
								}
								break;

							case '8':
								$corrrect_answer = 0;
								$incorrrect_answer = 0;
								if(count($options) == count($attempt_options)){
									for($i = 0; $i < count($options); $i++) {
											
										if ($options[$i]['id'] == $attempt_options[$i]["option_id"]) {
											$corrrect_answer++;
										}
										else {
											// print_r($option['id']);
											// print_r($attempt_option["option_id"]);
											$incorrrect_answer++;
											// echo $incorrrect_answer.'wrong<br>';
										}
									}
								}else {$incorrrect_answer++;}
								
								if ($incorrrect_answer == 0 && $corrrect_answer == count($attempt_options)) {
									$this->updateQuestionAttemptCorrect($question_attempt['id']);
								}
								break;

							case '12'://matrix
								$corrrect_answer = 0;
								$incorrrect_answer = 0;
								if (count($options) == count($attempt_options)) {
									foreach ($attempt_options as $attempt_option) {
										$corrrect_answer = 0;
										//$incorrrect_answer = 0;
										foreach ($options as $option) {
											if ($option['id'] == $attempt_option["option_id"]) {
												// print_r($option['id']);
												// print_r($attempt_option["option_id"]);
												$corrrect_answer++;
												// echo $corrrect_answer.'right<br>';
											}
										}
										if ($corrrect_answer == 0) {
											// print_r($option['id']);
											// print_r($attempt_option["option_id"]);
											$incorrrect_answer++;
											// echo $incorrrect_answer.'wrong<br>';
										}
									}
								}else{$incorrrect_answer++;}

								if ($incorrrect_answer == 0 && count($options)) {
									$this->updateQuestionAttemptCorrect($question_attempt['id']);
								}
								break;
							
							case '13':
								if(count($options)==2 && count($attempt_options)){
									if(floatval($attempt_options[0]['option_id']) >= floatval($options[0]['option']) && floatval($attempt_options[0]['option_id']) <= floatval($options[1]['option']) ){
										$this->updateQuestionAttemptCorrect($question_attempt['id']);
									}
								}
								break;
							
							default:
								
								break;
						}
					}
					
					// QUERY SELECT  SUM(IF(correct=1, correct,0)) AS max_marks, SUM(IF(correct=0, neg_marks,0)) AS neg_marks  FROM `question_attempt` WHERE attempt_id=51
					/*
					* fetch sum of marks of correct question
					*/
					$statement = "SELECT  SUM(marks) as total_marks, SUM(IF(correct = 1, marks, 0)) AS max_marks, SUM(IF(correct = 0 AND question_status > 2, neg_marks, 0)) AS neg_marks  FROM `question_attempt`  WHERE attempt_id = ".$attempt_data[0]['id'];
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$marks = $run->toArray();
					$marks = $marks[0];
					$marks_archived = $marks['max_marks'] - $marks['neg_marks'];

					/*
					* updating test_paper attempt table setting result genrated
					*/
					$update = $sql->update();
					$update->table('test_paper_attempts');
					$update->set(array(
						'marks_archived'	=>	$marks_archived,
						'complete'	=>	2
					));
					$update->where(array(
						'id' => $attempt_id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$run = $statement->execute();

					/*
					* fetching test paper type
					*/
					$select = $sql->select();
					$select->from('test_paper');
					$select->where(array(
						'id' => $attempt_data[0]['test_paper_id'],
					));
					// $select->columns(array('type',''));
					$statement = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$test_paper_data = $run->toArray();
					$test_paper_data = $test_paper_data[0];

					$result->test_type = $test_paper_data['type'];
					$result->marks_archived = $marks_archived;
					$result->total_marks = $marks['total_marks'];

					$select = $sql->select();
					$statement = $sql->getSqlStringForSqlObject($select->from('test_program')->where(['id' => $attempt_data[0]['program_id']]));
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$test_program = $run->toArray();
					$test_program = (object) $test_program[0];

					if ($test_program->cut_off > $marks_archived) {
						$update = $sql->update();
						$update->table('student_program')
						->set([
							'valid_till'	=>	date('Y-m-d H:i:s', time() + (60 * 60 * 24 * 66))
						]);
						$update->where(array(
							'user_id' => $attempt_data[0]['student_id'],
							'test_program_id' => $attempt_data[0]['program_id'],
						));
						$statement = $sql->getSqlStringForSqlObject($update);
						// print_r($statement);
						$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

						// Sending e-mail
						require_once 'Email.php';
						$mailer = new Email;
						$mailer->firstFail($attempt_data[0]['name'], $attempt_data[0]['email'], $test_program->name);
					}

					$result->status = 1;
					$result->message = "Successfully Result Genrated";
				}
				else {
					$result->status = 0;
					$result->message = "Error: Can't genrate result of given test.";
				}
	
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Error: Can't genrate result of given test.";
				$result->error = $e->getMessage();	
			}
		}

		/*
		* funnction to update question attempt row 
		*/
		public function updateQuestionAttemptCorrect($attempt_id) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('question_attempt');
				
				$update->set(array(
					'correct'	=>	 1,
				));

				$update->where(array(
					'id' => $attempt_id
				));

				$statement = $sql->prepareStatementForSqlObject($update);
				$run = $statement->execute();				
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "Some thing went Wrong.";
				$result->error = $e->getMessage();;
			}
		}
		
		public function shuffleOptions($question_type, $options)
		{
			$result = new stdClass();
			try {
				switch ($question_type) {
					case '1':
						//shuffle($options);
						break;
					
					case '2':
						//shuffle($options);
						break;
					
					case '3':
						//shuffle($options);
						break;
					
					case '4':
						//shuffle($options);
						break;
					
					case '5':

						$result->column1 = array();
						$result->column2 = array();
						foreach ($options as $option) {
							if ($option['number'] == 2) {
								array_push($result->column2, $option);
							}
							else {
								array_push($result->column1, $option);
							}
						}
						shuffle($result->column2);
						$result->options = array_merge($result->column1, $result->column2);
						$options = $result->options;
						break;
					
					case '6':
						shuffle($options);
						break;
					
					case '7':
						shuffle($options);
						break;
					
					case '8':
						shuffle($options);
						break;
					
					case '9':
						shuffle($options);
						break;
					
					case '10':
						shuffle($options);
						break;
					
					case '11':
						shuffle($options);
						break;
					
					case '12':
						//shuffle($options);
						break;
					
					case '13':
						//shuffle($options);
						break;
					
					default:
						break;
				}		
				return $options;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "Some thing went Wrong.";
				$result->error = $e->getMessage();;
			}
			
		}

		public function getQ($questions) {
			
			try
			{
				foreach ($questions as $key => $value) {
				# code...

					$adapter = $this->adapter;
					$sql = new Sql($adapter);

					$select = $sql->select();
					$select->from('questions');
					$where = new $select->where();
					$where->equalTo('delete',0);
					
					$where->equalTo('id',$value["question_id"]);	
					$where->equalTo('parent_id','0');
					$select->where($where);
					$select->columns(array('id','question','type','description'));
					$statement = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$temp = $run->toArray();
					if(count($temp)){
						$result['questionData'][$key]['question_id'] = $temp[0]['id'];
						$result['questionData'][$key]['question'] = $temp[0]['question'];
						$result['questionData'][$key]['type'] = $temp[0]['type'];
						$result['questionData'][$key]['description'] = $temp[0]['description'];
					
						if($temp[0]['type'] == '11'){
							$select = $sql->select();
							$select->from('questions');
							$where = new $select->where();
							$where->equalTo('delete',0);
							
							$where->equalTo('parent_id',$temp[0]["id"]);						
							$select->where($where);
							$select->columns(array('id','question','type','description'));
							$statement = $sql->getSqlStringForSqlObject($select);

							$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
							$child = $run->toArray();
							foreach ($child as $key1 => $value1) {
								# code...

								$result['questionData'][$key]['children'][$key1]['question_id'] = $value1['id'];
								$result['questionData'][$key]['children'][$key1]['type'] = $value1['type'];
								$result['questionData'][$key]['children'][$key1]['question'] = $value1['question'];
								$result['questionData'][$key]['children'][$key1]['description'] = $value1['description'];
								$sql = new Sql($adapter);

								$select = $sql->select();
								$select->from('options');
								$where = new $select->where();
								$where->equalTo('deleted',0);
								
								$where->equalTo('question_id',$value1['id']);	
								$select->where($where);
								$select->columns(array('id','option','answer','number'));
								$statement = $sql->getSqlStringForSqlObject($select);

								$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
								$temp2 = $run->toArray();
								$result['questionData'][$key]['children'][$key1]['options'] = $temp2;

							}
						}else{
							$sql = new Sql($adapter);

							$select = $sql->select();
							$select->from('options');
							$where = new $select->where();
							$where->equalTo('deleted',0);
							
							$where->equalTo('question_id',$temp[0]['id']);	
							$select->where($where);
							$select->columns(array('id','option','answer','number'));
							$statement = $sql->getSqlStringForSqlObject($select);

							$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
							$temp2 = $run->toArray();
							$result['questionData'][$key]['options'] = $temp2;

						}
					}
					
				}
			
				return $result['questionData'];
			
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "Some thing went Wrong.";
				$result->error = $e->getMessage();;
			}
		}
	}

?>