<?php
	@session_start();
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;
	
	//including connection.php file to make connection with database
	require_once 'Connection.php';
	
	class InstituteReportCard extends Connection{
		
		/**
		* Function Institute Profile Data
		*
		* @author Utkarsh Pandey
		* @date 07-03-2016
		* @param JSON
		* @return JSON status message and data
		**/

		public function getTestPaperPerformanceReport($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				# SECTION WISE REPORT
				#
				# SELECT section_wise_report.section_name, MAX(section_wise_report.marks_get) AS max, MIN(section_wise_report.marks_get) AS min, AVG(section_wise_report.marks_get) AS avg FROM section_wise_report WHERE program_id = 28 AND test_paper_id = 253 AND institute_id = 48

		//(A) UNCOMMENT THIS IF YOU USE INSTITUTE REPORT CARD BACKUP
				/*$select = $sql->select();
				$select->columns(array('section_name','max' => new Expression('MAX(section_wise_report.marks_get)'), 'min' => new Expression('MIN(section_wise_report.marks_get)'), 'avg' => new Expression('AVG(section_wise_report.marks_get)')));
				$select->from('section_wise_report')->group('section_id');
				$select->where(array(
					'program_id' => $request->program_id,
					'test_paper_id' => $request->paper_id,
					'institute_id'	=>	$request->userId
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$result->section_report = $rows;*/
		//(A) UNCOMMENT THIS IF YOU USE INSTITUTE REPORT CARD BACKUP


				# SKILL WISE REPORT
				#
				# SELECT skill, total_question,  total_marks, MAX(marks_get) AS max, MIN(marks_get) AS min, AVG(marks_get) AS avg FROM `skill_wise_report` WHERE program_id = 28 AND test_paper_id = 253 AND institute_id = 48 GROUP BY skill_id

		//(B) UNCOMMENT THIS IF YOU USE INSTITUTE REPORT CARD BACKUP
				/*$select = $sql->select();
				$select->columns(array('skill', 'total_question', 'total_marks', 'max' => new Expression('MAX(marks_get)'), 'min' => new Expression('MIN(marks_get)'), 'avg' => new Expression('AVG(marks_get)')));
				$select->from('skill_wise_report');
				$select->where(array(
					'program_id' => $request->program_id,
					'test_paper_id' => $request->paper_id,
					'institute_id'	=>	$request->userId
				));
				$select->group('skill_id');
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$result->skill_report = $rows;*/
		//(B) UNCOMMENT THIS IF YOU USE INSTITUTE REPORT CARD BACKUP


				# QUESTION WISE REPORT
				#

		//(C) UNCOMMENT THIS IF YOU USE INSTITUTE REPORT CARD BACKUP
				/*$statement = "SELECT IFNULL(skills.skill, 'NAN') AS skill, question_attempt.question_id, COUNT(question_attempt.id) AS launched, SUM(IF(question_attempt.question_status = 3, 1, 0)) AS attempted, SUM(IF(question_attempt.correct = 1, 1, 0)) AS correct, SUM(IF(question_attempt.correct = 1,question_attempt.time, 0)) AS correct_time_taken, SUM(IF(question_attempt.correct = 0 AND question_attempt.question_status = 3,question_attempt.time, 0)) AS incorrect_time_taken FROM coupons LEFT JOIN test_paper_attempts ON test_paper_attempts.student_id = coupons.student_id LEFT JOIN question_attempt ON question_attempt.attempt_id = test_paper_attempts.id LEFT JOIN tagsofquestion ON tagsofquestion.tag_type = 7 AND tagsofquestion.question_id = question_attempt.question_id LEFT JOIN skills ON skills.id = tagsofquestion.tagged_id WHERE coupons.student_id != 0 AND question_attempt.section_id != 0 AND coupons.institute_id = $request->userId AND test_paper_attempts.program_id = $request->program_id AND test_paper_attempts.test_paper_id = $request->paper_id GROUP BY question_attempt.question_id";
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$result->question_report = $rows;*/
		//(C) UNCOMMENT THIS IF YOU USE INSTITUTE REPORT CARD BACKUP

				$statement = "SELECT test_paper_attempts.test_paper_id, test_paper.name,  MAX(test_paper_attempts.marks_archived) AS max, MIN(test_paper_attempts.marks_archived) as min, AVG(test_paper_attempts.marks_archived) AS avg FROM coupons LEFT JOIN test_paper_attempts ON test_paper_attempts.student_id = coupons.student_id LEFT JOIN test_paper ON test_paper.id = test_paper_attempts.test_paper_id LEFT JOIN users on users.id = test_paper_attempts.student_id WHERE coupons.student_id != 0 AND coupons.institute_id = $request->userId AND test_paper_attempts.program_id = $request->program_id GROUP BY  test_paper_attempts.test_paper_id";
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$result->student_performance_papers = $rows;


				$statement = "SELECT test_paper_attempts.test_paper_id, test_paper.name, test_paper_attempts.student_id, IFNULL(users.first_name,'--') AS student_name, test_paper_attempts.marks_archived FROM coupons LEFT JOIN test_paper_attempts ON test_paper_attempts.student_id = coupons.student_id LEFT JOIN test_paper ON test_paper.id = test_paper_attempts.test_paper_id LEFT JOIN users on users.id = test_paper_attempts.student_id WHERE coupons.student_id != 0 AND coupons.institute_id = $request->userId AND test_paper_attempts.program_id = $request->program_id GROUP BY  test_paper_attempts.id, test_paper_attempts.student_id";
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$result->student_performance_students = $rows;
				
				$result->status = 1;
				$result->message = "REPORT Fetched";
				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}	



		public function updateInstituteProfile($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				$update = $sql->update();
				$update->table('profile');

				$update->set(array(
					'about' => $data->about,
					'website_url' => $data->url,
					'address' => $data->address
				));

				$update->where(array(
					'user_id'	=>	$data->userId
				));
				$updateString = $sql->getSqlStringForSqlObject($update);
				// die($updateString);
				$update = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				
				
				
				$result->status = 1;
				$result->message = "Profile Successfully Updated";
			
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}


		}


		/**
		* Function to change student profile pic
		*
		* @author Utkarsh Pandey
		* @date 26-07-2015
		* @param JSON of image data
		* @return JSON status and message
		**/

		public function studentChangeProfilePicture($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$orgFileName = $data->image["name"];
				$ext = pathinfo($orgFileName, PATHINFO_EXTENSION);
				$newFileName =  md5($data->userId).'.'.$ext;
				$destination = __DIR__."/../../profilePictures/".$newFileName;
				move_uploaded_file($data->image["tmp_name"], $destination);

				$sql = new Sql($adapter);
				
				$update = $sql->update();
				$update->table('profile');
				$src = "profilePictures/".$newFileName;
				$update->set(array(
					'image' => $src
				));

				$update->where(array(
					'user_id'	=>	$data->userId
				));
				
				$updateString = $sql->getSqlStringForSqlObject($update);
				$update = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				
				$result->status = 1;
				$result->message = "Avtar Updated.";
				$result->src = $src."?".time();
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function updateInstitutePassword($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'id'	=>	$data->userId
				));

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$user_data = $run->toArray();
				$user_data = $user_data[0];

				if($data->new_password == "" || $data->confirm_password == "" || $data->current_password == "")
				{
					$result->status = 2;
					$result->message = "Some fields are required";
				}
				else if($user_data['password'] != md5($data->current_password))
				{
					$result->status = 2;
					$result->message = "You have entered the wrong current password";
				}
				else if($data->new_password != $data->confirm_password)
				{
					$result->status = 2;
					$result->message = "Please enter same password both times.";
				}
				else
				{
					$update = $sql->update();
					$update->table('users');

					$update->set(array(
						'password' => md5($data->confirm_password)
					));

					$update->where(array(
						'id'	=>	$data->userId
					));
					
					$updateString = $sql->getSqlStringForSqlObject($update);
					$run = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE);

					$result->status = 1;
					$result->message = 'Password Updated';
				}
				
			
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}


		}


	}