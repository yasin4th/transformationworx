<?php

	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Expression;

	require_once 'Connection.php';

	class Assignment extends connection{

		public function DeleteBatchRow($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('batch');
				$update->set(array(
					'deleted'	=> 1,
					'created'	=>	time()
				));
				$update->where(array(
					'id' => $data->id
				));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				$result->status = 1;
				$result->message = "Successfully Deleted.";
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function fetchStudentAssignments($data) {

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->columns(array('id', 'doc_name', 'path', 'status', 'created', 'deleted', 'test_program_id', 'user_id'));
				$select->from('student_assignment')

						->join(array('users' => 'users'), 'users.id = student_assignment.user_id' , array('first_name' => 'first_name', 'last_name' => 'last_name', 'email' => 'email'), 'left')
						->join(array('test_program' => 'test_program'), 'test_program.id = student_assignment.test_program_id' , array('package_name' => 'name', 'cut_off'), 'left')
						->join(array('classes' => 'classes'), 'test_program.class = classes.id' , array('class_name' => 'name'), 'left');

				$where = new $select->where();
				$where->equalTo('student_assignment.deleted', 0); //$_SESSION['userId']

				if(isset($data->program_id)) {
					$where->equalTo('test_program.id', $data->program_id); //$_SESSION['userId']
				}

				if(isset($data->role) && $data->role == 4) {
					$where->equalTo('student_assignment.user_id', $data->userId); //$_SESSION['userId']
				}

				if(isset($data->id)) {
					$where->equalTo('student_assignment.id', $data->id); //$_SESSION['userId']
				}

				$select->where($where);
				$statement = $sql->getSqlStringForSqlObject($select);
				// die($statement);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();

				$result->up_assignments = $rows;
				$result->status = 1;
				$result->message = "Assignment Docs fetched.";
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function changeAssignmentStatus($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('student_assignment')->where(['id' => $data->as_id]);
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				if (count($rows) == 0) {
					$result->status = 0;
					$result->message = 'Error: Assignment not found.';
					return $result;
				}
				$assignment = (object) $rows[0];


				$update = $sql->update();
				$update->table('student_assignment');
				$update->set(array(
					'status' => htmlentities($data->as_status),
					'test_program_id' => $data->program,
					'updated' => time()
				));
				$update->where(array(
					'id' => $assignment->id,
				));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();

				// if($data->as_status == 1) {
					// $response = $this->getHash($data->as_id);
					// $a = $this->saveCertificate($response,$data);
				// }
				if ($data->as_status == 2) {
					$select = $sql->select();
					$select->from('users')->where(['id' => $assignment->user_id]);
					$statement = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$rows = $run->toArray();
					if (count($rows) == 0) {
						$result->status = 0;
						$result->message = 'Error: Mail sending failed, Student not found.';
						return $result;
					}
					$student = (object) $rows[0];

					$update = $sql->update();
					$update->table('student_program')
						->set([
						'valid_till'	=>	date('Y-m-d H:i:s', time() + (60 * 60 * 24 * 66))
					]);
					$update->where(array(
						'user_id' => $assignment->user_id,
						'test_program_id' => $assignment->test_program_id,
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$run = $statement->execute();

					require_once 'Email.php';
					// Send E-mail
					$mailer = new Email;
					$mailer->assigmentFail($student->first_name.' '.$student->last_name, $student->email);
				}
				$result->status = 1;
				$result->message = "Status Successfully Updated.";
				return $result;

			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function generateCertificate($data) {
			$result = new stdClass();
			try{
				if (!isset($data->id)) {
					$result->status = 0;
					$result->message = "Assignment ID is required.";
					return $result;
				}
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('certificates')->where->like('license_no', $data->license);
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				if (count($rows)) {
					$result->status = 0;
					$result->message = "License no. already exist.";
					return $result;
				}



				$select = $sql->select();
				$select->from('student_assignment')
						->join(array('users' => 'users'), 'users.id = student_assignment.user_id' , array('first_name' => 'first_name', 'last_name' => 'last_name', 'email' => 'email'), 'left')
						->join(array('test_program' => 'test_program'), 'test_program.id = student_assignment.test_program_id' , array('package_name' => 'name'), 'left')
						->join(array('classes' => 'classes'), 'test_program.class = classes.id' , array('class_name' => 'name'), 'left');

				$select->where->equalTo('student_assignment.id', $data->id);

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				if (count($rows) == 0) {
					$result->status = 0;
					$result->message = "Invalid assignment id.";
					return $result;
				}
				$assignment = (object) $rows[0];
				if ($assignment->status != 1) {
					$result->status = 0;
					$result->message = "Fail.";
					return $result;
				}
				$response = json_decode('{"hash":{"license":"a0x","txhash":"tsxhash"},"email":"jblackfield@deloitte.ca","username":"Johnny","document":"Send By E-mail"}'); 
				// $this->getHash($data->id, $data->license);
				$response->license = $data->license;
				$result = $this->saveCertificate($response, $assignment);
				if (!$result->status) {
					return $result;
				}
				$result->status = 1;
				$result->message = "Certificate Successfully Generated.";
				return $result;

			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function getHash($id, $license) {

			$result = new stdClass();
			try {

				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->columns(array('id', 'doc_name', 'path', 'status', 'created', 'deleted', 'test_program_id'));
				$select->from('student_assignment')
						->join(array('users' => 'users'), 'users.id = student_assignment.user_id',
								 array('first_name' => 'first_name', 'last_name' => 'last_name', 'email' => 'email'), 'left')
						->join(array('tp' => 'test_program'), 'tp.id = student_assignment.test_program_id',
								 array('name' => 'name'), 'left');

				$where = new $select->where();
				$where->equalTo('student_assignment.id', $id); //$_SESSION['userId']

				$select->where($where);
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				if (count($rows) == 0) {
					$result->status = 0;
					$result->message = "Invalid assignment id.";
					return $result;
				}
				$assignment = (object) $rows[0];
				$time = time();

				$issued = date('d-M-Y', $assignment->created);
				$expired = date('d-M-Y', strtotime('+3 years', $assignment->created));
				// $rand = rand(10000,99999);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,"http://ico.dltlabs.io:3008/generateCertificateHash");
				// curl_setopt($ch, CURLOPT_URL,"http://192.168.1.20:3002/generateCertificateHash");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,"firstname={$assignment->first_name}&lastname={$assignment->last_name}&email={$assignment->email}&issuedOn={$issued}&validUntil={$expired}&coursename=CIO Certified Blockchain Professional (CCBP)&license={$license}");

				// in real life you should use something like:
				// curl_setopt($ch, CURLOPT_POSTFIELDS,
				//          http_build_query(array('postvar1' => 'value1')));

				// receive server response ...
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				// $server_output = curl_exec ($ch);

				curl_close ($ch);
				// $result->hash = $server_output;
				$result->hash = 'checksum123';
				$result->email = $assignment->email;
				$result->username = $assignment->first_name . " " . $assignment->last_name;
				$result->document = $assignment->doc_name;

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function saveCertificate($req, $request) {
			date_default_timezone_set('Asia/Kolkata');

			$result = new stdClass();
			try {

				$adapter = $this->adapter;
				if (!isset($request->user_id) || !isset($req->username) || !isset($req->hash) || !isset($req->license) || !isset($request->package_name) || !isset($request->test_program_id)) {
					$result->status = 0;
					$result->error = "ERROR: all required fields not filled.";
					$result->message = "ERROR: Some thing Went Wrong.";
					return $result;
				}
				
				$data = [
					'user_id'  => $request->user_id,
					'username' => $req->username,
					'email' => $req->email,
					'hash' => $req->hash,
					'txhash' => $req->hash->txhash,
					'license_no' => $req->license,
					'program' => $request->package_name,
					'test_program_id' => $request->test_program_id,
					'created' => time()
				];

				$query = new TableGateway('certificates', $adapter, null, new HydratingResultSet());
				$query->insert($data);

				$a = $this->createCertificate($req);
				$result->status = 1;
				$result->message = "Certificate Stored";

				return $result;

			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function getCertificate($req) {
			$result = new stdClass();
			try {

				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('certificates');

				$select->where->equalTo('hash', $req->hash);

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				if(count($rows)){
					$result->status = 1;
					$result->data = $rows[0];
				} else {
					$result->status = 0;
					$result->message = "Wrong hash";
				}
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function fetchCertificates($req) {
			$result = new stdClass();
			try {

				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('certificates');

				$select->where->equalTo('user_id', $req->userId);

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();

				$result->status = 1;
				$result->message = "Certificates fetched";
				$result->data = $rows;

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function fetchAssignmentsReadyForCertificate($request) {
			$result = new stdClass;
			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			$select = $sql->select();
			$select->from('test_paper_attempts')
					->join(['test_program' => 'test_program'], 'test_paper_attempts.program_id = test_program.id' , ['course_name' => 'name', 'cut_off'], 'left')
					->join(['student_assignment' => 'student_assignment'], new Expression('student_assignment.test_program_id = test_paper_attempts.program_id AND student_assignment.user_id = test_paper_attempts.student_id AND student_assignment.deleted = 0'), ['assigment_status' => 'status', 'assignment_id' => 'id'])
					->join(['users' => 'users'], 'users.id = test_paper_attempts.student_id' , ['first_name', 'last_name'], 'left')
					->join(['certificates' => 'certificates'], new Expression('test_paper_attempts.student_id = certificates.user_id AND test_paper_attempts.program_id = certificates.test_program_id') , ['license_no'], 'left');

			$select->where->equalTo('student_assignment.status', 1)->greaterThanOrEqualTo('test_paper_attempts.marks_archived', new Expression('test_program.cut_off'));

			$select->group(['test_paper_attempts.program_id', 'test_paper_attempts.student_id']);
			$statement = $sql->getSqlStringForSqlObject($select);
			// die($statement);
			$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			$rows = $run->toArray();
			$result->data = $rows;
			$result->status = 1;
			$result->message = "Status Successfully Updated.";
			return $result;
		}

		public function createCertificate($req) {
			$result = new stdClass();
			try{

				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('certificates');

				$select->where->equalTo('hash', $req->hash);
				$select->where->equalTo('license_no', $req->license);

				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();

				if(count($rows) == 0) {
					$result->status = 0;
					$result->message = "Something went wrong.";
					return $result;
				}

				$a = $rows[0];
				require_once('PDF-Library/autoload.php');

				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				$pdf->SetAuthor($a['username']);
				$pdf->SetTitle('Certificate');
				$pdf->SetSubject('TCPDF Tutorial');
				$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
				$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

				// set auto page breaks
				$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

				// set image scale factor
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

				// set default font subsetting mode
				$pdf->setFontSubsetting(true);

				// Set font
				// dejavusans is a UTF-8 Unicode font, if you only need to
				// print standard ASCII chars, you can use core fonts like
				// helvetica or times to reduce file size.
				$pdf->SetFont('dejavusans', '', 14, '', true);

				// Add a page
				// This method has several options, check the source code documentation for more information.
				$pdf->AddPage();

				// set text shadow effect
				$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
				$issued = date('d-M-Y', $a['created']);
				$expired = date('d-M-Y', strtotime('+3 years', $a['created']));
				// die($issued);
						// Set some content to print
				$name = ucwords($a['username']);
				$license_no = $req->license;
$html = <<<EOD
	<div style="text-align: center;max-width: 900px;margin: auto;border: 10px solid #d4af37;">
		<table style="width:100%;">
			<tbody>
				<tr>
					<td style="width:15px;">
					</td>
					<td style="width:600px;">
						<p></p>
						<table style="width: 100%;">
							<tbody>
								<tr>
									<td>
										<div><img src="http://{$_SERVER["HTTP_HOST"]}/dltexams/assets/global/img/certificate-header.png" style="max-width: 100%; height: auto; display: block;"></div>
									</td>
								</tr>
							</tbody>
						</table>

						<table style="width: 100%;">
							<tbody>
								<tr>
									<td>
										<div>
											<span style="text-align: center;font-size: 30px;color: #222;text-transform:capitalize;margin: 0px 0px 0px;font-weight: 300;line-height:1.5;">{$name}</span>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						<table style="width: 100%;">
							<tbody>
								<tr>
									<td>
										<div><img src="http://{$_SERVER["HTTP_HOST"]}/dltexams/assets/global/img/certificate-content.png" style="max-width: 100%; height: auto; display: block;"></div>
									</td>
								</tr>
							</tbody>
						</table>

						<table style="width: 100%">
							<tbody><tr><td style="height: 10px;"></td></tr></tbody>
						</table>

						<table style="width: 100%">
								<tr>
									<td style="text-align:left;">
										<h4 style="font-size: 16px;padding: 0px 0px 5px;margin: 0px;font-weight:300;">Date Issued: $issued</h4>
										<h4 style="font-size: 16px;padding: 0px 0px 5px;margin: 0px;font-weight:300;">Licence No: $license_no</h4>
										<h4 style="font-size: 16px;padding: 0px 0px 5px;margin: 0px;font-weight:300;">ETH: Insert ETH Address</h4>
									</td>
									<td style="text-align:right;">
										<h4 style="font-size: 16px;padding: 0px 0px 5px;margin: 0px;font-weight:300;">Valid Until: $expired</h4>
									</td>
								</tr>
							</tbody>
						</table>
						<p></p>
					</td>
					<td style="width:15px;">
					</td>
				</tr>
			</tbody>
		</table>
	</div>
EOD;

	// Print text using writeHTMLCell()
	$pdf->writeHTML($html, true, false, false, false, '');//0, 0, '', '', $html, 0, 1, 0, true, '', true);

	// ---------------------------------------------------------

	// Close and output PDF document
	// This method has several options, check the source code documentation for more information.

	// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
	$pdf->setFooterData(array(0,64,0), array(0,64,128));
	mkdir(dirname(dirname(dirname(__FILE__)))."/certificates/{$req->license}", 0755, true);
	$pdf->Output(dirname(dirname(dirname(__FILE__)))."/certificates/{$req->license}/Certificate.pdf", 'F');

				$result->status = 1;
				$result->message = $a;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function getTranscriptDetails($req) {
			$result = new stdClass();
			try {

				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('certificates');

				$select->where->equalTo('license_no', $req->license_no);

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();

				if(count($rows) > 0) {

					$result->status = 1;
					$result->message = "Transcript fetched";
					$a = $rows[0];
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL,"http://ico.dltlabs.io:3008/getCertificate");
					// curl_setopt($ch, CURLOPT_URL,"http://192.168.1.20:3002/getCertificate");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS,"license={$a['license_no']}");

					// in real life you should use something like:
					// curl_setopt($ch, CURLOPT_POSTFIELDS,
					//          http_build_query(array('postvar1' => 'value1')));

					// receive server response ...
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					$server_output = curl_exec ($ch);
					// echo "<pre>";
					$so = json_decode($server_output); //object

					curl_close ($ch);

					$result->data = $so;

				} else {
					$result->status = 0;
					$result->message = "Transcript not found.";
				}

				return $result;

			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function deleteAssignment($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('student_assignment');
				$update->set(array(
					'deleted'	=> 1,
				));
				$update->where(array(
					'id' => $data->id
				));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				$result->status = 1;
				$result->message = "Successfully Deleted.";
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

	}
