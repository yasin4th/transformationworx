<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	//including connection.php file to make connection with database
	require_once 'Connection.php';

	class Coupons extends Connection{

		/**
		* Function to genrate and download Coupons
		*
		* @author Utkarsh Pandey
		* @date 30-07-2015
		* @param JSON with all required details
		* @return JSON status and data(Associates program with the student id)
		**/
		public function downloadCoupons($data) {

			$result = new stdClass();

			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				for ($x=0; $x < $data->number_of_coupons; $x++) {

					$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
					$coupon = strtoupper($data->prefix);
					for ($i = 0; $i < 16; $i++) {
					    $coupon .= $chars[mt_rand(0, strlen($chars)-1)];
					}

					$dub = 0;
					foreach ($result->coupon as $key => $coup) {
						if ($coup == $coupon) {
							$dub = 1;
						}
					}

					if ($dub == 0) {

						$statement = "SELECT * FROM coupons WHERE code = '$coupon'";
						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$rows = $run->toArray();

						if (count($rows) == 0) {
							$insert .= "('$coupon', $data->id, '$data->end_date', '$data->start_date'),";
							$result->coupon[] = $coupon;
						}
						else {
							$x--;
						}

					}
					else {
						$x--;
					}
				}

				$insert = rtrim($insert, ',');
				// echo $insert;
				// die()
				$run = $adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);


				// return $data;

				$result->status = 1;
				$result->message = "Coupons Created.";
				return $result;
			}
			catch(Exception $e)
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}
		}

		/**
		* Function to fetchinstitute coupons
		*
		* @author Utkarsh Pandey
		* @date 31-07-2015
		* @param JSON with all required details
		* @return JSON status and data(Associates program with the student id)
		**/
		public function fetchInstitutCoupons($data) {

			$result = new stdClass();

			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

					$statement = "SELECT coupons.id, coupons.code, test_program.name AS program_name, coupons.test_program_id, coupons.student_id, from_unixtime(coupons.created, '%d-%M-%Y : %h:%i:%s') AS start_date, from_unixtime(coupons.expiry_date, '%d-%M-%Y %h:%i:%s') AS expiry_date, from_unixtime(coupons.created, '%d-%M-%Y %h:%i:%s') AS created FROM coupons LEFT JOIN test_program ON test_program.id = coupons.test_program_id WHERE coupons.institute_id = '$data->id'";

					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$coupons = $run->toArray();

					$result->coupons =	$coupons;

				$result->status = 1;
				$result->message = "Coupons Fetched.";

				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}

		}

		public function redeemCoupon($data) {

			$result = new stdClass();

			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('coupons');
				// $select->columns(array('id'));
				$select->where(array(
					'deleted'	=>	0,
					'student_id'	=>	0,
					'code'	=>	$data->code
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();

				if(count($rows) == 1) {
					$coupon = $rows[0];
					// dd(time());
					// dd($rows);
					if (($coupon['created'] < time() && $coupon['expiry_date'] > time())  ) {

						$data->program_id = $coupon['test_program_id'];
						require_once('StudentProgram.php');
						$buy = new StudentProgram();
						$bought = $buy->buyTestProgram($data);

						if ($bought->status == 1) {

							$update = $sql->update();
							$update->table('coupons');
							$update->set(array(
								'student_id'	=>	$data->userId
							));
							$update->where(array(
								'deleted'	=>	0,
								'code'	=>	$data->code
							));
							$statement = $sql->getSqlStringForSqlObject($update);
							$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify

							$result->status = 1;
							$result->message = 'Applied Successful';

						} else {
							$result = $bought;
						}
					}
					else {
						$result->status = 0;
						$result->message = 'Coupon is Invalid.';
					}
				}
				else {
					$result->status = 0;
					$result->message = 'Invalid Coupon Code';
				}

				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}

		}




	}
