<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	//including connection.php file to make connection with database
	require_once 'Connection.php';

	class BBTemplate extends Connection
	{

		public function fetchBBTemplate($data) {

			$result = new stdClass();

			try{

				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();

				$select->from('institute');

				$select->where("institute = $data->institute");

				$selectString = $sql->getSqlStringForSqlObject($select);


				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$count = $run->toArray();

				if(count($count))
				{
                    $result->institute = $count[0]['institute'];
                    $result->logo      = $count[0]['logo'];
                    $result->banner    = $count[0]['banner'];
                    $result->features  = $count[0]['features'];
                    $result->about     = $count[0]['about'];
                    $result->contact   = $count[0]['contact'];
                    $result->director  = $count[0]['director'];
                    $result->courses   = $count[0]['courses'];
                    $result->website   = $count[0]['website'];


				}else{
                    $result->institute = '';
                    $result->logo      = 'assets/instituteimages/logo/default.png';
                    $result->banner    = 'assets/instituteimages/banners/default.jpg';
                    $result->features  = '';
                    $result->about     = '';
                    $result->contact   = '';
                    $result->director  = '';
                    $result->courses   = '';
                    $result->website   = '';
				}




				$result->status = 1;
				$result->message = "Details Fetched.";

				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}

		}

		public function b2blogo($req) {


			$result = new stdClass();
			try
			{
				$adapter = $this->adapter;


				$sql = new Sql($adapter);

				$select = $sql->select();

				$select->from('institute');
				$select->where("institute = $req->id");


				$selectString = $sql->getSqlStringForSqlObject($select);


				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$count = $run->toArray();



				if(count($count)){

					$filename = 'logo_'.time().'_'.$req->logo['name'];

					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('institute');
					$update->set(array(
						'logo'	=>	'assets/instituteimages/logo/'.$filename
					));
					$update->where(array(
					    'institute' => $req->id
					));


					$selectString = $sql->getSqlStringForSqlObject($update);

					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

					if (isset($req->logo)) {
						move_uploaded_file($req->logo['tmp_name'], '../assets/instituteimages/logo/'.$filename);
					}

					$result->status = 1;
					$result->message = 'Logo Uploaded';


				}
				else {

					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					$filename = 'logo_'.time().'_'.$req->logo['name'];
					$data = array(
						'institute'	=>	$req->id,
						'logo'	=> 'assets/instituteimages/logo/'.$filename

					);

					$query = new TableGateway('institute', $adapter, null, new HydratingResultSet());
					$query->insert($data);

					if (isset($req->logo)) {
						move_uploaded_file($req->logo['tmp_name'], '../assets/instituteimages/logo/'.$filename);
					}else{
						die("asdas");
					}

					$result->status = 1;
					$result->message = 'Logo Added';

					return $result;


				}

				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function b2bbanner($req) {

			$result = new stdClass();
			try
			{
				$adapter = $this->adapter;


				$sql = new Sql($adapter);

				$select = $sql->select();

				$select->from('institute');
				$select->where("institute = $req->id");


				$selectString = $sql->getSqlStringForSqlObject($select);


				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$count = $run->toArray();



				if(count($count))
				{

					$filename = 'logo_'.time().'_'.$req->banner['name'];

					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('institute');
					$update->set(array(
						'banner'	=>	'assets/instituteimages/banners/'.$filename
					));
					$update->where(array(
					    'institute' => $req->id
					));


					$selectString = $sql->getSqlStringForSqlObject($update);

					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

					if (isset($req->banner)) {
						move_uploaded_file($req->banner['tmp_name'], '../assets/instituteimages/banners/'.$filename);
					}else{
						die("not uploaded");
					}

					$result->status = 1;
					$result->message = 'Banner Uploaded';

				}
				else
				{

					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					$filename = 'logo_'.time().'_'.$req->banner['name'];
					$data = array(
						'institute'	=>	$req->id,
						'banner'	=> 'assets/instituteimages/banners/'.$filename

					);

					$query = new TableGateway('institute', $adapter, null, new HydratingResultSet());
					$query->insert($data);

					if (isset($req->banner)) {
						move_uploaded_file($req->banner['tmp_name'], '../assets/instituteimages/banners/'.$filename);
					}

					$result->status = 1;
					$result->message = 'Logo Added';

					return $result;


				}

				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function saveB2BTemplate($req) {


			$result = new stdClass();
			try
			{
				$adapter = $this->adapter;


				$sql = new Sql($adapter);

				$select = $sql->select();

				$select->from('institute');
				$select->where("institute = $req->id");


				$selectString = $sql->getSqlStringForSqlObject($select);


				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$count = $run->toArray();

				if(count($count)){



					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('institute');
					$update->set(array(
						'features'	=>	$req->features,
						'about'	=>	$req->about,
						'contact'	=>	$req->contact,
						'director'	=>	$req->director,
						'courses'	=>	$req->courses,
						'website'	=>	$this->formWebsiteUrl($req->website)
					));
					$update->where(array(
					    'institute' => $req->id
					));


					$selectString = $sql->getSqlStringForSqlObject($update);

					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);



					$result->status = 1;
					$result->message = 'Template Saved';
					return $result;

				}
				else
				{

					$adapter = $this->adapter;
					$sql = new Sql($adapter);

					$data = array(
						'institute' => $req->id,
						'features'	=>	$req->features,
						'about'	=>	$req->about,
						'contact'	=>	$req->contact,
						'director'	=>	$req->director,
						'courses'	=>	$req->courses,
						'website'	=>	$this->formWebsiteUrl($req->website)

					);

					$query = new TableGateway('institute', $adapter, null, new HydratingResultSet());
					$query->insert($data);



					$result->status = 1;
					$result->message = 'Template Saved';

					return $result;


				}

				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function downloadB2BTemplate($req)
		{


			try
			{
				$adapter = $this->adapter;

				$sql = new Sql($adapter);

				$select = $sql->select();

				$select->from('institute')
						->join(array('users'=>'users'),'institute.institute=users.id',array('name'=>'first_name'),'left');
				$select->where("institute = $req->id");

				$selectString = $sql->getSqlStringForSqlObject($select);
				// return $selectString;

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$count = $run->toArray();

				if(count($count))
				{
					if($req->template == 1)
					{
						$template = file_get_contents('template1/template.html');
					}
					else if($req->template == 2)
					{
						$template = file_get_contents('template2/template.html');
					}
					else
					{
						$template = file_get_contents('template3/template.html');
					}

                    // return $template;

					$template = str_replace('_TITLE_', $count[0]['name'], $template);
					$template = str_replace('_LOGO_', 'http://exampointer.com/'.$count[0]['logo'], $template);
					$template = str_replace('_BANNER_', 'http://exampointer.com/'.$count[0]['banner'], $template);
					$template = str_replace('_ABOUT_', $count[0]['about'] , $template);
					$template = str_replace('_COURSES_', $count[0]['courses'], $template);
					$template = str_replace('_DIRECTOR_', $count[0]['director'], $template);
					$template = str_replace('_CONTACT_', $count[0]['contact'], $template);
					$template = str_replace('_FEATURES_', $count[0]['features'], $template);
					$template = str_replace('_WEBSITE_', $this->formWebsiteUrl($count[0]['website']), $template);
				}
				else {
					return false;
				}

				return $template;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function generateStudentIds($req)
		{

			$result = new stdClass();
			try
			{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();

				$select->from('users');

				$select->where("id = $req->institute");
				$select->where("role = '5'");

				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$users = $run->toArray();

				if(count($users)){
					$adapter = $this->adapter;
					$sql = new Sql($adapter);

					$select = $sql->select();

					$select->from('users');

					$select->columns(array('max' => new Expression('MAX(id)')));
					$selectString = $sql->getSqlStringForSqlObject($select);


					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$count = $run->toArray();

					$ids = array();
					if(count($count))
					{
						$min = intval($count[0]['max']) + 1;
						$max = $req->number + intval($count[0]['max']);


						$statement1 = "INSERT INTO `users` (`id`, `registered_on`, `role`, `mode`, `password`) VALUES ";
						$statement2 = "INSERT INTO `coupons` (`test_program_id`, `institute_id`, `student_id`,`created`,`expiry_date`) VALUES ";
						$statement3 = "INSERT INTO `student_program` (`test_program_id`, `user_id`, `time`,`institute_id`,`allowed_total_paper`,`valid_till`) VALUES ";
						$statement4 = "INSERT INTO `profile` (`user_id`, `registered_on`) VALUES ";

						for ($i=$min; $i <= $max; $i++)
						{
							$time = time();
							$p = rand(100000,999999);
							$pass = md5($p);
							array_push($ids, array('userid'=>'STUDENT00'.$i,'password'=>$p));
							$statement1 .= "($i,$time,4,'institute','$pass') ,";
							$statement2 .= "($req->program,$req->institute,$i,$time,$time),";
							$statement3 .= "($req->program,$i,$time,$req->institute,999999999999,'2030-12-31') ,";
							$statement4 .= "($i,$time) ,";
						}

						$statement1 = rtrim($statement1, ",");
						$statement2 = rtrim($statement2, ",");
						$statement3 = rtrim($statement3, ",");
						$statement4 = rtrim($statement4, ",");
						/*_print_r($statement1);
						_print_r($statement2);
						_print_r($statement3);*/
						$run = $adapter->query($statement1, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
						$run = $adapter->query($statement2, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
						$run = $adapter->query($statement3, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
						$run = $adapter->query($statement4, $adapter::QUERY_MODE_EXECUTE); //$run can be modify

						$result->students = $ids;
						$result->status = 1;
						$result->message = "Students Created";
					}

				}else
				{
					$result->status = 0;
					$result->message = "Institute doesnt exits";
				}

				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function b2blogin($data) {

			$result = new stdClass();

			try {

				$adapter = $this->adapter;
				//creating a sql object to create a sql statement using zend framework
				$sql = new Sql($adapter);
				$hashed = md5($data->password);
				$student_roles = array(4,5,6);

				$select = $sql->select();
				$select->from('users')
						->join(array('coupons'=>'coupons'),'coupons.student_id=users.id',array('institute_id'),'left')
						->join(array('institute'=>'institute'),'institute.institute=coupons.institute_id',array('logo','website'),'left');
                $where = new $select->where();

                if (strpos($data->email, '@')) {
                    $where->equalTo('users.email', $data->email);
                } else {
                    $id=substr($data->email, 9);
                    $where->equalTo('users.id', $id);
                }
				$where->equalTo('users.password', $hashed);
				$where->equalTo('users.banned',0);
				$where->equalTo('users.deleted',0);
				$where->in('users.role',$student_roles);
				$select->where($where);

				$select->columns(array('id','email','role','mode','first_name'));

                $select->group('id');

				$selectString = $sql->getSqlStringForSqlObject($select);

				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

				$temp = $temp->toArray();

                // dd($temp);

				if (count($temp) == 1)
				{
					$update = $sql->update();
					$update->table('users');
					$update->set(array(
						'last_login'	=>	time()
					));
					$update->where(array(
						'id'	=>	$temp[0]['id']
					));
					$statement = $sql->getSqlStringForSqlObject($update);
					$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

					$result->status = 1;
					$result->user = $temp[0];
					return $result;
				}

				@session_start();
				@session_destroy();
				$result->status = 0;
				$result->message = 'Invalid Credentials...';
				return $result;

			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function formWebsiteUrl($url) {
	        if (strpos($url,'http://') === 0 || strpos($url,'https://') === 0){
	            return $url;
	        }
	        return 'http://'. $url;
	    }
	}
