<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	//including connection.php file to make connection with database
	require_once 'Connection.php';

	class ProgramApi extends Connection{

		public function getProgramDetails($data) {


			$result = new stdClass();

			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('student_program');
				$select->where(array(
					'test_program_id'   =>  $data->test_program,
					'user_id'   =>  $data->userId
				));
				$select->columns(array('id'));

				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$rows = $run->toArray();
				if(count($rows)) {
					/*
					* fetching test program details
					*/
					$select = $sql->select();
					$select->from('test_program');
					$select->where(array(
						'id'    =>  $data->test_program
					));
					$select->columns(array('name','class','price','description'));

					$selectString = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$details = $run->toArray();


					/*
					* fetching subjects associated with test program
					*/
					$select = $sql->select();
					$select->from('test_program_subjects');
					$select->columns(array('subject_id'));
					$where = new $select->where();
					$where->equalTo('test_program_id',$data->test_program);
					$select->where($where);
					$selectString = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$run = $run->toArray();
					$subjectIds = $run;

					$Subjects = array();
					foreach ($subjectIds as $value) {
						array_push($Subjects, $value['subject_id']);
					}
					/*
					* fetching subjects detailss
					*/
					$select = $sql->select();
					$select->from('subjects');
					$select->columns(array('id','name'));
					$where = new $select->where();
					$where->equalTo('deleted',0);
					if(!empty($Subjects))
						$where->in('id',$Subjects);
					$select->where($where);
					$selectString = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$run = $run->toArray();
					$subjectList = $run;

					if(count($details)){
						$result->details = $details[0];
					}else{
						$result->details = $details;
					}
					$result->subjectList = $subjectList;

					$result->status = 1;
					$result->message = "Successfully Fetched.";
				}
				else {
					$result->status = 0;
					$result->message = "Sorry This Program Is Not Associated With This USER ID.";
				}

				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}

		}

		/**
		* Function displayPrograms for the display of programs bought by the student
		*
		* @author Pushpam Matah
		* @date 17-07-2015
		* @param JSON
		* @return JSON status and data(program names)
		**/

		public function displayPrograms($data) {

			$result = new stdClass();

			try{

					$adapter = $this->adapter;

					$sql = new Sql($adapter);
					$select = $sql->select();

					$select->from(array('t_p' => 'test_program'),array())
						   ->join(array('s_p' => 'student_program'),
							't_p.id = s_p.test_program_id', array());

					$select->where(array(
						'user_id'   =>  $data->userId

					));

					$select->columns(array('id','name','description','image'));

					$selectString = $sql->getSqlStringForSqlObject($select);
					//die($selectString);
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$res = $run->toArray();

				if(count($res)){
					foreach ($res as $p => $program) {


						# code...
						$adapter = $this->adapter;
						$sql = new Sql($adapter);

						$select = $sql->select();

						$select->from(array('ps'=>'test_program_subjects'))
								->join(array('s'=>'subjects'),'s.id=ps.subject_id',array('name'=>'name'),'left')

								->join(array('tp'=>'test_papers_of_test_program'),'s.id=tp.parent_id',
									array(
										'diagnostic' => new Expression('SUM(IF(tp.test_paper_type_id=1 AND tp.parent_type=2,1,0))'),
										'practice' => new Expression('SUM(IF(tp.test_paper_type_id=6 AND tp.parent_type=2,1,0))'),
										'chapter' => new Expression('SUM(IF(tp.test_paper_type_id=3 AND tp.parent_type=2,1,0))'),
										'mock' => new Expression('SUM(IF(tp.test_paper_type_id=4 AND tp.parent_type=2,1,0))'),
										'scheduled' => new Expression('SUM(IF(tp.test_paper_type_id=5 AND tp.parent_type=2,1,0))')
										),'left');
						$where = array('ps.test_program_id'=>$program['id']);
						//$where['tp.parent_type']='2';
						$select->where($where);
						$select->group('ps.subject_id');

						//$select->columns(array('count' => new Expression('COUNT(*)')));
						$selectString = $sql->getSqlStringForSqlObject($select);
						//die($selectString);

						$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$count = $run->toArray();
						$res[$p]['subjects'] = $count;


						$sql = new Sql($adapter);

						$select = $sql->select();

						$select->from('test_papers_of_test_program');
						$where = array();
						$where['test_program_id'] = $program['id'];

						$select->where($where);

						$select->columns(array(
							'diagnostic' => new Expression('SUM(IF(test_paper_type_id=1 AND parent_type=0,1,0))'),
							'mock' => new Expression('SUM(IF(test_paper_type_id=4 AND parent_type=0,1,0))'),
							'scheduled' => new Expression('SUM(IF(test_paper_type_id=5 AND parent_type=0,1,0))')
							));
						$selectString = $sql->getSqlStringForSqlObject($select);
						//die($selectString);

						$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$rows = $run->toArray();


						if(count($rows)){
							$arr = array('name'=>'Combined Tests','scheduled'=>$rows[0]['scheduled'],'diagnostic'=>$rows[0]['diagnostic'],'mock'=>$rows[0]['mock'],'practice'=>0,'chapter'=>0);
							array_push($res[$p]['subjects'], $arr);
						}

					}
				}
				$result->status = 1;
				$result->data = $res;
				return $result;
			}
			catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function displayPrograms1($data)
		{
			$result = new stdClass();

			try
			{
				$adapter = $this->adapter;

				$sql = new Sql($adapter);

				/******************************************
				Before batch integration select query start
				*******************************************/
				$select = $sql->select();

				$select->from(array('t_p' => 'test_program'),array())
					   ->join(array('s_p' => 'student_program'),
					   't_p.id = s_p.test_program_id', array());
				$date = date('Y-m-d H:i:s');
				// $select->where->equalTo('user_id',$data->userId)->equalTo('t_p.deleted' , 0)->greaterThan('s_p.valid_till',$date);

				$select->where(array(
					'user_id'   =>  $data->userId,
					't_p.deleted'   =>  0
				));

				$select->columns(array('id','name','description','image'));
				$selectString = $sql->getSqlStringForSqlObject($select);

				/******************************************
				Before batch integration select query end
				*******************************************/

				/******************************************
				After batch integration select query start
				*******************************************/
				// $selectString ="SELECT id,name,description,image,type FROM test_program WHERE id IN(
				//              SELECT bp.program AS program FROM batch_users AS bu
				//              LEFT JOIN batch_program AS bp ON bu.batch=bp.batch
				//              WHERE bu.user=".$data->userId."
				//              UNION
				//              SELECT tp.id AS program
				//              FROM test_program AS tp
				//              Inner JOIN student_program AS sp ON sp.test_program_id= tp.id
				//              WHERE sp.user_id=".$data->userId.") and status = 1";

				/******************************************
				After batch integration select query end
				*******************************************/

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				// dd($res);

				if(count($res))
				{
					foreach ($res as $p => $program)
					{

						$adapter = $this->adapter;
						$sql = new Sql($adapter);

						$select = $sql->select();

						$select->from(array('ps'=>'test_program_subjects'))
								->join(array('s'=>'subjects'),'s.id=ps.subject_id',array('name'=>'name','id'=>'id'),'left');

						$where = array('ps.test_program_id'=>$program['id']);
						$select->where($where);
						$select->group('ps.subject_id');

						$selectString = $sql->getSqlStringForSqlObject($select);


						$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$count = $run->toArray();
						$res[$p]['subjects'] = array();
						foreach ($count as $key => $subject)
						{
							$sql = new Sql($adapter);
							$select = $sql->select();
							$where = new $select->where();
							$where->equalTo('deleted',0);
							$where->equalTo('subject_id',$subject['subject_id']);
							$select->where($where);

							$select->from('units');
							$selectString = $sql->getSqlStringForSqlObject($select);


							$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$rows = $run->toArray();
							$unit = array();
							if(count($rows))
							{
								foreach ($rows as $k => $sub)
								{
									array_push($unit, $sub['id']);
								}
							}

							$sql = new Sql($adapter);
							$select = $sql->select();
							$where = new $select->where();
							$where->equalTo('deleted',0);
							if(count($unit))
							{
								$where->in('unit_id',$unit);
							}
							$select->where($where);

							$select->from('chapters');
							$selectString = $sql->getSqlStringForSqlObject($select);

							$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$rows = $run->toArray();
							$chapters = array();
							if(count($rows))
							{
								foreach ($rows as $k => $chapter)
								{
									array_push($chapters, $chapter['id']);
								}
							}

							$sql = new Sql($adapter);

							$select = $sql->select();

							$select->from('test_papers_of_test_program')
									->columns(array(
											'diagnostic' => new Expression('IFNULL(SUM(IF(test_paper_type_id=1,1,0)),0)'),
											'practice' => new Expression('IFNULL(SUM(IF(test_paper_type_id=6,1,0)),0)'),
											'chapter' => new Expression('IFNULL(SUM(IF(test_paper_type_id=3,1,0)),0)'),
											'mock' => new Expression('IFNULL(SUM(IF(test_paper_type_id=4,1,0)),0)'),
											'scheduled' => new Expression('IFNULL(SUM(IF(test_paper_type_id=5,1,0)),0)')
											));
							$where = new $select->where();
							$where->equalTo('test_program_id',$program['id']);
							$where->equalTo('parent_type','2');
							// if(count($chapters))
							// {
							//  $where->in('parent_id',$chapters);
							// }
							$select->where($where);
							$selectString = $sql->getSqlStringForSqlObject($select);


							//echo "<br>";
							$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$rs = $run->toArray();

							if(count($rs))
							{
								$arr = array('name'=>$subject['name'],'scheduled'=>$rs[0]['scheduled'],'diagnostic'=>$rs[0]['diagnostic'],'mock'=>$rs[0]['mock'],'practice'=>$rs[0]['practice'],'chapter'=>$rs[0]['chapter']);
								array_push($res[$p]['subjects'], $arr);
							}
						}


						//for combined test
						$sql = new Sql($adapter);

						$select = $sql->select();

						$select->from('test_papers_of_test_program');
						$where = array();
						$where['test_program_id'] = $program['id'];
						//$where['parent_id'] = 0;

						$select->where($where);

						$select->columns(array(
							'diagnostic' => new Expression('IFNULL(SUM(IF(test_paper_type_id=1,1,0)),0)'),
							'mock' => new Expression('IFNULL(SUM(IF(test_paper_type_id=4,1,0)),0)'),
							'scheduled' => new Expression('IFNULL(SUM(IF(test_paper_type_id=5,1,0)),0)')
							));
						$selectString = $sql->getSqlStringForSqlObject($select);
						//die($selectString);

						$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$rows = $run->toArray();

						if(count($rows))
						{
							$arr = array('name'=>'Combined Tests','scheduled'=>$rows[0]['scheduled'],'diagnostic'=>$rows[0]['diagnostic'],'mock'=>$rows[0]['mock'],'practice'=>0,'chapter'=>0);
							array_push($res[$p]['subjects'], $arr);
						}
					}
				}

				$result->status = 1;
				$result->data = $res;
				return $result;
			}
			catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* Function displayProgramDetails for displaying the details in the program
		*
		* @author Pushpam Matah
		* @date 23-07-2015
		* @param JSON
		* @return JSON status and data(Count of test paper types)
		**/

		public function displayProgramDetails($data)
		{

			$result = new stdClass();

			try{

					$adapter = $this->adapter;

					$sql = new Sql($adapter);
					$select = $sql->select();

					$select->from('test_papers_of_test_program');

					$select->where(array(
					'test_program_id'   =>  $data->program_id,
						));

					$select->group('test_paper_type_id');

					$select->columns(array(
					'test_paper_type_id',
					'details_count' => new Expression('COUNT(id)')
					));

					$selectString = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$res = $run->toArray();

				if($res)
				$result->status = 1;
				$result->data = $res;
				return $result;

			}
			catch(Exception $e)
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}


		}

		/**
		* Function displayTestPapers for displaying the details of test papers in the category
		*
		* @author Pushpam Matah
		* @date 23-07-2015
		* @date 25-08-2015 - Modified by Utkarsh
		* @param JSON
		* @return JSON status and data(Test paper details)
		**/

		public function displayTestPapers($data) {



			$result = new stdClass();

			try{

				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();

				$select->from(array('t_p' => 'test_paper'),array())
					   ->join(array('tp_tp' => 'test_papers_of_test_program'),
						't_p.id = tp_tp.test_paper_id', array());

				$where = new $select->where();
				$where->equalTo('test_program_id',$data->program_id);
				$where->equalTo('test_paper_type_id',$data->type);
				// $where->equalTo('status',1);

				if(!empty($data->parent_id) && ($data->parent_id != 0) && !empty($data->parent_type) && ($data->parent_type != 0)) {
					$where->equalTo('parent_id',$data->parent_id);
					$where->equalTo('parent_type',$data->parent_type);
				}

				$select->where($where);
				$select->columns(array('*'));

				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				$result->data = $res;

				$i =0;

				foreach ($res as $test_paper) {

					// SELECT count(id) AS attempted FROM `test_paper_attempts` WHERE student_id=25 AND test_paper_id=2
					$sql = new Sql($adapter);
					$select = $sql->select();

					$select->from('test_paper_attempts');

					$select->where(array(
						'student_id'    =>  $data->userId,
						'test_paper_id' =>  $test_paper['id']
					));

					$select->columns(array('complete','id','start_time','test_paper_id'));
					// $select->columns(array('attempted' => new Expression('COUNT(id)')));

					$selectString = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$completes = $run->toArray();


					$time = $test_paper['time'];


					date_default_timezone_set('Asia/Kolkata');
					$now = time();
					$resume = 2;
					if($res[0]['type'] == '4')
					{
						$resume = 2;
					}

					if($res[0]['type'] == '5'){
						$resume = 2;
						$start_time = date_create_from_format('d F Y - H:i', $test_paper['start_time']);
						$start_time = intval(date_format($start_time, 'U'));

						$end_time = date_create_from_format('d F Y - H:i', $test_paper['end_time']);
						$end_time = intval(date_format($end_time, 'U'));

						$resume = 2;
						if($now >= $start_time && $now <= $end_time){
							$resume = 1;
						}
						//print_r($now);
						$result->data[$i]['now'] = date('d F Y - H:i:s',time());
						$result->data[$i]['now1'] = date('d F Y - H:i:s',$start_time);
						$result->data[$i]['now2'] = date('d F Y - H:i:s',$end_time);

					}

					foreach ($completes as $paper)
					{

						//check and submit test

						$now1 = $paper['start_time'];
						$now = time();
						$start_time = date_create_from_format('U', $now);

						if(($now - $now1)>$time*60)
						{
							$sql = new Sql($adapter);
							$update = $sql->update();
							$update->table('test_paper_attempts');
							$update->set(array(
								'complete' => '1',
								'end_time' => $now
							));
							$update->where(array(
								'id' => $paper['id']
							));
							$statement = $sql->getSqlStringForSqlObject($update);
							// die($statement);
							$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						}

						if($paper['complete'] == 0){

							$now = time();
							$diff = $now - $paper['start_time'];
							$test_paper_id = $paper['test_paper_id'];

							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from('test_paper');

							$select->where(array(

								'id'    =>  $test_paper_id
							));

							$select->columns(array('id','time','start_time','end_time','type'));
							// $select->columns(array('attempted' => new Expression('COUNT(id)')));

							$selectString = $sql->getSqlStringForSqlObject($select);
							//die($selectString);
							$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
							$res = $run->toArray();


							$papertime = $res[0]['time'];



							if(($diff / 60) > $papertime )
							{
								$resume = 0;
								if($res[0]['type'] == '6'){
									$resume = 1;
								}
							}
							else{
								$resume = 1;
							}


							if($res[0]['type'] == '5'){
								$start_time = date_create_from_format('d F Y - H:i', $res[0]['start_time']);
								$start_time = intval(date_format($start_time, 'U'));

								$end_time = date_create_from_format('d F Y - H:i', $res[0]['end_time']);
								$end_time = intval(date_format($end_time, 'U'));

								$resume = 0;
								if($now >= $start_time && $now <= $end_time){
									$resume = 1;
								}
								//print_r($now);
								$result->data[$i]['now'] = date('d F Y - H:i:s',time());
								$result->data[$i]['now1'] = date('d F Y - H:i:s',$start_time);
								$result->data[$i]['now2'] = date('d F Y - H:i:s',$end_time);



								/*print_r("now: ".date('d F Y - H:i:s',$now));
								echo "<br>";
								print_r("start: ".date('d F Y - H:i:S',$start_time));
								echo "<br>";
								print_r("end: ".date('d F Y - H:i:s',$end_time));
								echo "<br>";

								print_r("now: ".$now);
								echo "<br>";
								print_r("start: ".$start_time);
								echo "<br>";
								print_r("end: ".$end_time);
								echo "<br>";*/

							}

						}else if($paper['complete'] == 1){
							if($res[0]['type'] == '5'){
								$start_time = date_create_from_format('d F Y - H:i', $res[0]['start_time']);
								$start_time = intval(date_format($start_time, 'U'));

								$end_time = date_create_from_format('d F Y - H:i', $res[0]['end_time']);
								$end_time = intval(date_format($end_time, 'U'));

								$resume = 0;
								if($now >= $start_time && $now <= $end_time){
									$resume = 1;
								}
								//print_r($now);
								$result->data[$i]['now'] = date('d F Y - H:i:s',time());
								$result->data[$i]['now1'] = date('d F Y - H:i:s',$start_time);
								$result->data[$i]['now2'] = date('d F Y - H:i:s',$end_time);

							}

						}
					}






					$result->data[$i]['status'] = $resume;
					$result->data[$i]['attempted'] = count($completes);



					//first attempt id grabbed
					if(count($completes)){
						$result->data[$i]['attempt_first'] = $completes[0]['id'];
					}

					$i++;
				}

				$result->status = 1;
				$result->message = 'Successfully Fetched Paper Details';

				return $result;
			}
			catch(Exception $e )
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* Function to fetch test programs points
		*
		* @author Utkarsh Pandey
		* @date 04-11-2015
		* @param JSON with all required details
		* @return JSON status and data(Reward Points)
		**/
		public function fetchRewardData($data) {

			$result = new stdClass();

			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				// return $data;

				$select = $sql->select();
				$select->columns(array('id'));
				$select->from('student_program');
				$select->where(array(
					'test_program_id' => $data->program_id,
					'user_id' => $data->userId
				));

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$check = $run->toArray();

				$reward_points = 0;
				$total_tests = 0;
				$total_attempted_tests = 0;
				$total_questions = 0;
				$total_attempted_questions = 0;
				$total_correct_questions = 0;

				$total_diagnostic_test = 0;
				$total_attempted_diagnostic_test = 0;
				$total_chapter_test_questions = 0;
				$total_correct_chapter_test_questions = 0;
				$total_practice_test_questions = 0;
				$total_correct_practice_test_questions = 0;

				if (count($check) == 1)
				{
					$select = $sql->select();
					$select->columns(array('id', 'test_paper_id', 'test_program_id', 'test_paper_type_id', 'parent_id', 'parent_type'));
					$select->from('test_papers_of_test_program');
					$select->where(array(
						'test_program_id' => $data->program_id
					));
					$statement = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$tests_of_program = $run->toArray();

					$select = $sql->select();
					// $select->columns(array());
					$select->from('qcr_illustration');
					$select->where(array(
						'test_program_id' => $data->program_id
					));
					$statement = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$illustration_of_programe = $run->toArray();

					$total_tests = count($tests_of_program);
					foreach ($tests_of_program as $key => $test) {

						/*** start checking test is attempted or not  **/
						$select = $sql->select();
						$select->columns(array('id', 'test_paper_id', 'marks_archived', 'total_marks', 'start_time', 'end_time', 'optimizer_id', 'complete'));
						$select->from('test_paper_attempts');
						$select->where(array(
							'test_paper_id' => $test['test_paper_id'],
							'student_id' => $data->userId
						));
						$statement = $sql->getSqlStringForSqlObject($select);
						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
						$attempt_data = $run->toArray();

						if (count($attempt_data) > 0) {
							$total_attempted_tests++;
							$attempt_data = $attempt_data[0]; // setting first attempt
						}
						/*** end checking test is attempted or not  **/


						/*** start count total questions of test  **/
						$select = $sql->select();
						$select->columns(array('total_question' => new Expression('COUNT(id)')));
						$select->from('test_paper_questions');
						$select->where(array(
							'test_paper_id' => $test['test_paper_id']
						));
						$statement = $sql->getSqlStringForSqlObject($select);
						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
						$questions = $run->toArray();
						$questions = $questions[0];

						$total_questions = $total_questions + $questions['total_question'];
						/*** fetching total questions of test  **/

						/** start this logic is for count total Diagnoctic Tests, total questions in test practice test and chapter test **/
						switch ($test['test_paper_type_id']) {
							case '1':
								$total_diagnostic_test++;
								break;

							case '3':
								$total_chapter_test_questions = $total_chapter_test_questions + $questions['total_question'];
								break;

							case '6':
								$total_practice_test_questions = $total_practice_test_questions + $questions['total_question'];
								break;

							default:
								break;
						}

						if (!empty($attempt_data)) {

							/** end this logic is for count total Diagnoctic Tests, total questions in test practice test and chapter test **/
							$statement = "SELECT COUNT(id) AS total_question, SUM(IF(question_status = 3, 1, 0)) AS attempted, SUM(correct) AS correct FROM question_attempt WHERE attempt_id =".$attempt_data['id'];
							$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
							$questions = $run->toArray();
							$questions = $questions[0];
							// die(print_r($questions));

							$total_attempted_questions = $total_attempted_questions + $questions['attempted'];
							$total_correct_questions = $total_correct_questions + $questions['correct'];



							/** start this logic is for count total atttempted Diagnoctic Tests, total correct questions in test practice test and chapter test **/
							switch ($test['test_paper_type_id']) {
								case '1':
									$total_attempted_diagnostic_test++;
									break;

								case '3':
									$total_correct_chapter_test_questions = $total_correct_chapter_test_questions + $questions['total_question'];
									break;

								case '6':
									$total_correct_practice_test_questions = $total_correct_practice_test_questions + $questions['correct'];
									break;

								default:
									break;
							}
							/** end this logic is for count total Diagnoctic Tests, total questions in test practice test and chapter test **/


							if($attempt_data['complete'] == 2) {
								$percent_marks = intval(( intval($attempt_data['marks_archived']) * 100 )/ intval($attempt_data['total_marks']));
								// die('data: '.$percent_marks);
								if ($percent_marks > 70 && $percent_marks <= 80) {
									$reward_points = $reward_points + 10;
								} elseif ($percent_marks > 80 && $percent_marks <= 90) {
									$reward_points = $reward_points + 15;
								} elseif ($percent_marks > 90) {
									$reward_points = $reward_points + 20;
								}
							}
						}

					}

					/***** fetching Test Program Details *****/
					$select = $sql->select();
					$select->columns(array('id', 'name', 'class', 'price', 'cut_off', 'description', 'image', 'created', 'user', 'status'));
					$select->from('test_program');
					$select->where(array(
						'id' => $data->program_id,
					));

					$statement = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$program_details = $run->toArray();
					$program_details = $program_details[0];

					$total_reward_points = (count($illustration_of_programe) * 10) + ($total_diagnostic_test * 10) + ($total_practice_test_questions * 1) + ($total_chapter_test_questions * 2) + ($total_tests * 20);

					$archived_reward_points = (count($illustration_of_programe) * 10) + ($total_attempted_diagnostic_test * 10) + ($total_correct_practice_test_questions * 1) + ($total_correct_chapter_test_questions * 2) + $reward_points ;


					$result->total_reward_points = $total_reward_points;
					$result->archived_reward_points = $archived_reward_points;
					$result->total_tests = $total_tests;
					$result->total_attempted_tests = $total_attempted_tests;
					$result->total_questions = $total_questions;
					$result->total_attempted_questions = $total_attempted_questions;
					$result->total_correct_questions = $total_correct_questions;
					$result->cut_off = $program_details['cut_off'];

				}


				$result->status = 1;
				$result->message = 'True';
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}

		}

	}