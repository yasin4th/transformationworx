<?php
	@session_start();
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;
	
	//including connection.php file to make connection with database
	require_once 'Connection.php';
	
	class UserDashboard extends Connection{
		
		/**
		* Function fetchPlanClasses for the planning of the schedule
		*
		* @author Pushpam Matah
		* @date 13-07-2015
		* @param JSON
		* @return JSON status and data(classes names)
		**/

		public function fetchUserDashboard($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
			
				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'role'	=>	'4'
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				$result->total_students = count($res);
				$active = 0;
				$inactive = 0;
				foreach ($res as $key => $student) {
					if($student['last_login'] > time()-(30*24*60*60))
					{
						$active++;
					}else
					{
						$inactive++;
					}
				}



				$result->active = $active;
				$result->inactive = $inactive;

				// questions
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('questions');
				$select->where(array(
					'delete'	=>	'0',
					'parent_id'	=>	'0'
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				$result->total_questions = count($res);


				// test paper
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('test_paper');
				$select->where(array(
					'deleted'	=>	'0'
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				$result->total_test_paper = count($res);$select = $sql->select();


				// orders
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('student_program');
				
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				$result->total_orders = count($res);

				// total sale
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('sp' => 'student_program'))
						->join(array('tp'=>'test_program'),'tp.id = sp.test_program_id', array('price' => new Expression('SUM(tp.price)')),'left');

				
				$statement = $sql->getSqlStringForSqlObject($select);
				
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				$result->total_sale = 0;
				if(count($res)){
					$result->total_sale = $res[0]['price'];
					if($res[0]['price'] == null){
						$result->total_sale = 0;
					}
				}

				// top selling programs 
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('sp'=>'student_program'))
						->join(array('tp' => 'test_program'),'tp.id = sp.test_program_id', array('count' => new Expression('COUNT(*)'),'name','price'),'left')->group('sp.test_program_id')->order('count desc');
						

				$select->columns(array('test_program_id'));
				$select->limit(7);
				$select->offset(0);

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				$result->top_selling = $res;

				// new students
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'role'	=>	'4'
				));
				$select->columns(array('id', 'first_name' => new Expression('IFNULL(users.first_name,"")'), 'last_name' => new Expression('IFNULL(users.last_name,"")'),'registered_on' => new Expression('FROM_UNIXTIME(registered_on,"%d-%m-%y")')));
				$select->limit(7);
				$select->offset(0);
				$select->order('id desc');	
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				$result->new_students = $res;

				//latest orders
				$sql = new Sql($adapter);
				$select = $sql->select();
				//$select->from('student_program');
				$select->from(array('sp' => 'student_program'))
						->join(array('tp'=>'test_program'),'tp.id = sp.test_program_id', array('name' => 'name','price' => 'price'),'left');
						
				$select->columns(array('id', 'user_id','time' => new Expression('FROM_UNIXTIME(time,"%d-%m-%y")')));
				$select->limit(7);
				$select->offset(0);
				$select->order('time desc');
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				$result->latest_orders = $res;

				

				$result->status = 1;
				$result->message = "Operation Successful.";
				
				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function fetchReports($data) {
			if(isset($data->days)){
				$days = $data->days;
				if($days == 0){
					$limit = 12;
					$datePattern = "%Y-%m";
					$having = "_realTime > DATE_SUB(NOW(), INTERVAL 1 YEAR)";
				}else if($days == 7 || $days == 15){
					$limit = $days;
					$datePattern = "%Y-%m-%d";
					$having = "_realTime > DATE_SUB(NOW(), INTERVAL ".$days." DAY)";
				}
			}
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				
				// revenue -> top selling
				$sql = new Sql($adapter);
				$select = $sql->select();				
				$select->from(array('sp'=>'student_program'))
						->join(array('tp' => 'test_program'),'tp.id = sp.test_program_id', array(),'left')
						->group('_time')->order('_time DESC')
						->limit($limit)
						->having($having);
						

				$select->columns(array('_time' => new Expression('DATE_FORMAT(FROM_UNIXTIME(sp.time), "'.$datePattern.'")'),'_year' => new Expression('DATE_FORMAT(FROM_UNIXTIME(sp.time), "%Y")'),'_month' => new Expression('DATE_FORMAT(FROM_UNIXTIME(sp.time), "%b")'),'_day' => new Expression('DATE_FORMAT(FROM_UNIXTIME(sp.time), "%d")'),'_realTime' => new Expression('DATE_FORMAT(FROM_UNIXTIME(sp.time), "%Y-%m-%d")'),'count' => new Expression('COUNT(*)')));

				$statement = $sql->getSqlStringForSqlObject($select);				
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				
				
				$result->revenue = $res;


				//revenue amount
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('sp'=>'student_program'))
						->join(array('tp' => 'test_program'),'tp.id = sp.test_program_id', array(),'left')
						->group('_time')->order('_time DESC')
						->limit($limit)
						->having($having);
												

				$select->columns(array('_time' => new Expression('DATE_FORMAT(FROM_UNIXTIME(sp.time), "'.$datePattern.'")'),'_year' => new Expression('DATE_FORMAT(FROM_UNIXTIME(sp.time), "%Y")'),'_month' => new Expression('DATE_FORMAT(FROM_UNIXTIME(sp.time), "%b")'),'_day' => new Expression('DATE_FORMAT(FROM_UNIXTIME(sp.time), "%d")'),'_realTime' => new Expression('DATE_FORMAT(FROM_UNIXTIME(sp.time), "%Y-%m-%d")'),'count' => new Expression('sum(tp.price)')));


				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				
				
				$result->amount = $res;


				$adapter = $this->adapter;
				
				// revenue -> top selling
				
				if($limit == 7){
					$statement = "SELECT DATE(NOW() - INTERVAL n DAY) AS date, DATE_FORMAT(NOW() - INTERVAL n DAY, '%d %b') AS day FROM ( SELECT 0 n UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 ) q;";
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$day = $run->toArray();
					$result->days = $day;
				}
				if($limit == 15){
					$statement = "SELECT DATE(NOW() - INTERVAL n DAY) AS date, DATE_FORMAT(NOW() - INTERVAL n DAY, '%d %b') AS day FROM ( SELECT 0 n UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9 UNION SELECT 10 UNION SELECT 11 UNION SELECT 12 UNION SELECT 13 UNION SELECT 14 UNION SELECT 15 ) q;";
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$day = $run->toArray();
					$result->days = $day;
				}
				


				$result->status = 1;
				$result->message = "Operation Successful.";
				
				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		*
		* @author Ravi Arya
		* @date 30-1-2016
		* @param filter options
		* @return programs according to options given
		* @todo todo
		*
		**/
		public function fetchPrograms($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;

				// top selling programs 
				$select = $sql->select();
				$select->from(array('sp'=>'student_program'))
						->join(array('tp' => 'test_program'),'tp.id = sp.test_program_id', array('count' => new Expression('COUNT(*)'),'name','price'),'left')->group('sp.test_program_id')->order('count desc');
						

				$select->columns(array('test_program_id'));
				

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				$result->top_selling = $res;

				//latest programs
				$select = $sql->select();
				$select->from('test_program');

				//$select->columns(array('id', 'user_id','time' => new Expression('FROM_UNIXTIME(time,"%d-%m-%y")')));
				
				$select->order('time desc');
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				$result->latest_programs = $res;

				$result->status = 1;
				$result->message = "Operation Successful.";
				
				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
	}