<?php
    use Zend\Db\TableGateway\TableGateway;
    use Zend\Db\Sql\Sql;
    use Zend\Db\ResultSet\HydratingResultSet;
    use Zend\Db\Sql\Expression;
    

    //including connection.php file to make connection with database
    require_once 'Connection.php';
    
    class TestPaperQuestions extends Connection {
        
        /**
        * this function Set Questions Of Test-Paper
        *
        * @author Utkarsh Pandey
        * @date 01-06-2015
        * @param JSON with all required details
        * @return JSON with name and description
        * @return JSON with id, status and message
        **/

        public $qs = array();
        public function setQuestionsOfTestPaper($data)
        {

            $result = new stdClass();

            try{
                $adapter = $this->adapter;
                
                $sql = new Sql($adapter);
                

                /*
                * fetching all section`s data of test paper
                *
                */
                $select = $sql->select();
                $select->from('section');
                $select->where(array(
                    'deleted' => 0,
                    'test_paper' => $data->testPaper
                ));
                $select->columns(array('id','name','total_question'));
                

                $statement = $sql->getSqlStringForSqlObject($select);
                

                // $statement = "SELECT section.name, section.total_question, test_paper_labels.id AS lable_id, test_paper_labels.section_id, test_paper_labels.class_id, test_paper_labels.subject_id, test_paper_labels.unit_id, test_paper_labels.chapter_id, test_paper_labels.topic_id, test_paper_labels.question_type, test_paper_labels.number_of_question, test_paper_labels.difficulty_level, test_paper_labels.skill_id, test_paper_labels.max_marks, test_paper_labels.neg_marks FROM section INNER JOIN test_paper_labels ON section.id = test_paper_labels.section_id WHERE test_paper = ".$data->testPaper;
                
                $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

                $sections = $run->toArray();

                
                

                $paper = array();
                /*
                * All section contains one or more types of question(lables) and every question type may have different filters.
                * So here we will fetch all lables for each section
                * A lable contain a question type, all question filter data and some other data
                *
                */
                $total_question = 0;
                foreach($sections as $section) {

                    $Sections = new stdClass();
                    $questionData = array();
                    
                    $total_question += intval($section['total_question']);

                    /*
                    * fetching all lable`s data of a section
                    * 
                    */
                    $select = $sql->select();
                    $select->from('test_paper_labels');
                    $select->where(array(
                        'section_id' => $section["id"]
                    ));
                    // $select->columns(array());
                    $statement = $sql->getSqlStringForSqlObject($select);
                    

                    $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                    
                    $lables = $run->toArray();
                
                    foreach($lables AS $lable) {
                        $data = new stdClass();
                        
                        
                        $select = $sql->select();
                        $select->from('tags_of_test_paper_label');
                        $select->where(array(
                            'test_paper_label_id' => $lable["id"]
                        ));
                        $select->columns(array('tag_id'));
                        $statement = $sql->getSqlStringForSqlObject($select);
                        
                        $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

                        $tags  = $run->toArray();
                        $lable["tags"] = array();

                        foreach ($tags as $tag) {
                            array_push($lable["tags"],$tag["tag_id"]);
                        }

                        $data->lable = $lable;

                        
                        $temp = $this->questionFilter($lable);
                        
                        
                        if($temp->status == 0)
                            return $temp;
                        else
                            $questions = $temp->data;
                        $data->questions = $questions;
                        
                        /*
                        * $data contain all lables(filter data) and questions
                        * All lables and question of eatch section will compresses in $questionData Variable
                        */
                        array_push($questionData, $data);
                        // $result->data = $this->creatQuestions($question_ids);
                    }
                    $Sections->section = $section;
                    $Sections->questionData = $questionData;
                    /*
                    * all section data will be compressed in this variable($paper array)
                    */
                    array_push($paper, $Sections);
                    
                }

                


                $result->data = $paper;
                $result->status = 1;
                $result->message = "Working";
                $result->qs = $this->qs;

                return $result;
            }
            catch(Execption $e){
                $result->status = 0;
                $result->error = $e->getMessage();
                $result->message = "ERROR: Some Thing went wrong.";
            }   
        }   
    
        /**
        * function to filter questions
        * 
        * @author Utkarsh Pandey
        * @date 01-06-2015
        * @param JSON
        * @return json with status and all data
        **/
        public function questionFilter($data) {
            

            $result = new stdClass();
            try{
                $adapter = $this->adapter;
                // return$data;// = $data[0];
                $class_filtered_question_ids = array();
                $sql = new Sql($adapter);
                $select = $sql->select();
                $where = new $select->where();
                /* checking that classes are set or not     */
                if(!empty($data["class_id"])) {
                    if(!empty($data["subject_id"]) && $data["subject_id"] !=  0) {
                        if(!empty($data["unit_id"]) && $data["unit_id"] != 0) {
                            if(!empty($data["chapter_id"]) && $data["chapter_id"] != 0) {
                                if(!empty($data["topic_id"]) && $data["topic_id"] != 0) {
                                    $where->equalTo('tag_type',4);
                                    $where->equalTo('tagged_id',$data["topic_id"]);
                                }
                                else{
                                    $where->equalTo('tag_type',3);
                                    $where->equalTo('tagged_id',$data["chapter_id"]);
                                }
                            }
                            else {
                                $where->equalTo('tag_type',2);
                                $where->equalTo('tagged_id',$data["unit_id"]);
                            }
                        }
                        else {
                            $where->equalTo('tag_type',1);
                            $where->equalTo('tagged_id',$data["subject_id"]);
                        }
                        $select->from('tagsofquestion');
                        $select->columns(array('id'=>'question_id'));                           
                    }
                    else {
                        $select->from('questions');
                        $where->equalTo('class_id',$data["class_id"]);
                        $where->equalTo('parent_id',0);                     
                        $where->equalTo('delete',0);
                        $select->columns(array('id'));
                    }

                    $select->where($where);
                    $statement = $sql->getSqlStringForSqlObject($select);

                    $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                    $questionIdFilteredBYClass = $run->toArray();

                }

                if(!empty($questionIdFilteredBYClass))
                    foreach($questionIdFilteredBYClass as $questionId)
                        array_push($class_filtered_question_ids, $questionId["id"]);
                
                /* checking that Tags are set or not    */
                if(!empty($data["tags"])) {
                    $select = $sql->select();
                    $select->from('tagsofquestion');
                    $where = new $select->where();
                    $where->equalTo('tag_type',5);
                    $where->in('tagged_id',$data["tags"]);
                    if(!empty($class_filtered_question_ids))
                        $where->in('question_id',$class_filtered_question_ids);
                    $select->where($where);
                    $select->columns(array('question_id'));
                    $statement = $sql->getSqlStringForSqlObject($select);
                    $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                    $questionIdFilteredByTags = $run->toArray();
                    $class_filtered_question_ids = array();                     
                    foreach($questionIdFilteredByTags as $questionId)
                        array_push($class_filtered_question_ids, $questionId["question_id"]);
                }
                
            
                /* checking that Difficulty-Level are set or not    */
                if(!empty($data["difficulty_level"]) && $data["difficulty_level"] != 0) {
                    $select = $sql->select();
                    $select->from('tagsofquestion');
                    $where = new $select->where();
                    $where->equalTo('tag_type',6);
                    $where->equalTo('tagged_id',$data["difficulty_level"]);
                    if(!empty($class_filtered_question_ids))
                        $where->in('question_id',$class_filtered_question_ids);
                    $select->where($where);
                    $select->columns(array('question_id'));
                    $statement = $sql->getSqlStringForSqlObject($select);
                    $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                    $questionIdFilteredByDifficulty = $run->toArray();
                    $class_filtered_question_ids = array();
                    foreach($questionIdFilteredByDifficulty as $questionId)
                        array_push($class_filtered_question_ids, $questionId["question_id"]);
                }
            
                /* checking that skills are set or not  */
                if(!empty($data["skill_id"]) && $data["skill_id"] != 0) {
                    $select = $sql->select();
                    $select->from('tagsofquestion');
                    $where = new $select->where();
                    $where->equalTo('tag_type',7);
                    $where->equalTo('tagged_id',$data["skill_id"]);
                    if(!empty($class_filtered_question_ids))
                        $where->in('question_id',$class_filtered_question_ids);
                    $select->where($where);
                    $select->columns(array('question_id'));
                    $statement = $sql->getSqlStringForSqlObject($select);
                    $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                    $questionIdFilteredBySkills = $run->toArray();
                    
                    
                    $class_filtered_question_ids = array();
                    foreach($questionIdFilteredBySkills as $questionId)
                        array_push($class_filtered_question_ids, $questionId["question_id"]);
                }


                if(!empty($data["notIn"]))              
                    $class_filtered_question_ids =array_diff($class_filtered_question_ids, $data["notIn"]);
                $class_filtered_question_ids =array_diff($class_filtered_question_ids, $this->qs);
                
                    
                
                
                
                if(!empty($class_filtered_question_ids)) {
                    $select = $sql->select();
                    $select->from('questions');
                    $where = new $select->where();
                    $where->equalTo('delete',0);                    
                    $where->equalTo('parent_id','0');
                    
                    // $where->equalTo('quality_check',0);
                    $where->in('id',$class_filtered_question_ids);
                    if(!empty($data["question_type"]) && $data["question_type"] != 0)
                        $where->equalTo('type',$data["question_type"]); 
                    $select->where($where);
                    $select->columns(array('id','question','description','type'));
                    if(!empty($data['question_type']) && $data['question_type'] == 11){
                        //in case of paasage only 1
                        //$select->limit(1);

                    }else if(!empty($data["number_of_question"]))
                        $select->limit($data["number_of_question"]);
                    $statement = $sql->getSqlStringForSqlObject($select);                   
                                        
                

                    $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                    //echo $statement;
                    $lable = $run->toArray();

                    
                    if(!empty($data["question_type"]) && $data["question_type"] == 11){
                        foreach ($lable as $key => $val) {
                            
                            
                            $select = $sql->select();
                            $select->from('questions');
                            $where = new $select->where();
                            $where->equalTo('delete',0);
                            $where->equalTo('parent_id',$val['id']);
                            // $where->equalTo('quality_check',0);
                            $select->where($where);
                            $select->columns(array('count' =>new Expression('COUNT(*)')));
                            $statement = $sql->getSqlStringForSqlObject($select);
                            $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                            $count = $run->toArray();
                            if(isset($data['number_of_question'])){
                                if($count[0]['count'] < $data['number_of_question']){
                                    unset($lable[$key]);
                                }
                            }
                        }
                        
                    }

                    if(isset($data['number_of_question'])){
                        $no = $data['number_of_question'];
                    }
                    
                    $data = array();
                    foreach ($lable as $quest) {
                        $question_data = new stdClass();
                        $question_data = $quest;
                        array_push($this->qs,$quest['id']);

                        
                        if($quest['type'] == 11) {
                            
                            $select = $sql->select();
                            $select->from('questions');
                            $where = new $select->where();
                            $where->equalTo('delete',0);
                            $where->equalTo('parent_id',$quest['id']);
                            // $where->equalTo('quality_check',0);
                            $select->where($where);
                            $select->columns(array('id','question','description','type'));
                            if(isset($no)){
                                $select->limit($no);
                            }
                            $statement = $sql->getSqlStringForSqlObject($select);
                            $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                            $questionsInPassage = $run->toArray();

                            $question_data['passageQuestions'] = array();
                            foreach ($questionsInPassage as $questionInPassage) {
                                $questionOfPassage = new stdClass();
                                $select = $sql->select();
                                $select->from('options');
                                $where = new $select->where();
                                $where->equalTo('question_id',$questionInPassage["id"]);
                                $where->equalTo('deleted',0);
                                $select->where($where);
                                $select->columns(array('option','answer','number'));
                                $statement = $sql->getSqlStringForSqlObject($select);
                                $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                                $questionOfPassage = $questionInPassage;
                                $questionOfPassage['options'] = $run->toArray();
                            
                                array_push($question_data['passageQuestions'], $questionOfPassage);
                            }

                        }
                        else {
                            $select = $sql->select();
                            $select->from('options');
                            $where = new $select->where();
                            $where->equalTo('question_id',$quest["id"]);
                            $where->equalTo('deleted',0);
                            $select->where($where);
                            $select->columns(array('option','answer','number'));
                            $statement = $sql->getSqlStringForSqlObject($select);
                            $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                            $question_data['options'] = $run->toArray();    
                        }

                        array_push($data, $question_data);
                    }
                    
                }
                else {
                    $result->status = 0;
                    $result->message = "Error: There Is No Question Of This Filter." ;
                    return $result;
                }

                $result->status = 1;
                $result->message = "Successfully Done." ;
                $result->data = $data;
                return $result;
            }catch(Exception $e) {
                $result->status = 0;
                $result->message = "ERROR: Some thing Went Wrong.";
                $result->error = $e->getMessage();
                return $result;
            }
        }

        /**
        * function to save all questions and details of test paper
        * 
        * @author Utkarsh Pandey
        * @date 01-06-2015
        * @param JSON
        * @return json with status and all data
        **/
        public function saveTestPaper($data) {
            
            
            
            $result = new stdClass();
            try {
                $adapter = $this->adapter;
                $sql = new Sql($adapter);
                
                $select = $sql->select();
                
                $select->from('test_paper');
                $select->where(array(
                    'name'  =>  htmlentities($data->test_paper_name),
                    'type'  =>  $data->test_paper_type,
                    'deleted'   =>  0
                ));
                $select->columns(array('id'));
    
                $selectString = $sql->getSqlStringForSqlObject($select);
                $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
                $rows = $run->toArray();
                if(count($rows) == 0) {

                    $this->saveTestPaperDetails($data);
                    foreach($data->questions as $question) {
                        
                            $this->saveQuestionsOfTestPaper($question);
                        
                    }

                    // create section






                    $result->status = 1;
                    $result->message = "Successfully Saved.";
                    return $result;
                }
                else if(count($rows) == 1 && $rows[0]['id'] == $data->id) {
                    $this->saveTestPaperDetails($data);
                    foreach($data->questions as $question) {
                        $this->saveQuestionsOfTestPaper($question); 
                    }
                    $result->status = 1;
                    $result->message = "Successfully Updated.";
                    return $result;
                }
                else {
                    $result->status = 0;
                    $result->message = 'Not Updated. this Paper is already created.';
                    return $result;
                }
            }catch(Exception $e){
                $result->status = 0;
                $result->message = "ERROR: Some thing Went Wrong.";
                $result->error = $e->getMessage();
                return $result;
            }
        }


        /**
        * function update test paper questions
        * 
        * @author Utkarsh Pandey
        * @date 04-08-2015
        * @param JSON
        * @return json with status and all data
        **/
        public function updateTestPaperQuestions($data) {
            $result = new stdClass();
            try{
                $adapter = $this->adapter;
                $sql = new Sql($adapter);
                
                $delete = $sql->delete();
                $delete = $sql->delete('test_paper_questions');

                $where = new $delete->where();
                $where->equalTo('test_paper_id',$data->test_paper_id);
                $delete->where($where);

                $statement = $sql->getSqlStringForSqlObject($delete);
                $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                    
                foreach($data->questions as $question) {
                    $this->saveQuestionsOfTestPaper($question);
                }
                $result->status = 1;
                $result->message = "Successfully Saved.";
                return $result;
            }catch(Exception $e){
                $result->status = 0;
                $result->message = "ERROR: Some thing Went Wrong.";
                $result->error = $e->getMessage();
                return $result;
            }
        }



        /**
        * function save questions of test paper
        * 
        * @author Utkarsh Pandey
        * @date 06-06-2015
        * @param JSON
        * @return json with status and all data
        **/
        public function saveQuestionsOfTestPaper($data) {
            $result = new stdClass();
            try {
                $adapter = $this->adapter;
                // INSERT INTO `test_paper_questions`(`test_paper_id`, `section_id`, `lable_id`, `question_id`) VALUES ()
                $insert = new TableGateway('test_paper_questions', $adapter, null, new HydratingResultSet());
                    $insert->insert(array(
                        'test_paper_id' =>  $data->test_paper,
                        'section_id'    =>  $data->section_id,
                        'lable_id'  =>  $data->lable_id,
                        'question_id'   =>  $data->question_id,
                        'max_marks' =>  $data->max_marks,
                        'neg_marks' =>  $data->neg_marks,
                        'created'   =>  time()
                    ));

                $id = $insert->getLastInsertValue();            
                
                $result->status = 1;
                $result->message = "Test Paper Saved.";
                return $result;
            }
            catch(Execption $e) {
                $result->status = 0;
                $result->message = "ERROR: Some thing Went Wrong.";
                $result->error = $e->getMessage();
                return $result;
            }
        }


        /**
        * function fetch questions of test paper
        * 
        * @author Utkarsh Pandey
        * @date 06-06-2015
        * @param JSON
        * @return json with status and all data
        **/
        public function fetchQuestionsOfTestPaper($data) {
            $result = new stdClass();
            try {
                $adapter = $this->adapter;
                $sql = new Sql($adapter);

                /*
                * fetching sections of test paper
                */
                $select = $sql->select();
                $select->from('section');
                $where = new $select->where();
                $where->equalTo('test_paper',$data->testPaper);
                $select->where($where);
                $select->columns(array('id','name','total_question'));
                $statement = $sql->getSqlStringForSqlObject($select);
                $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                $sections = $run->toArray();
                
                /*
                * fetching saved question of test paper
                *
                */
                $select = $sql->select();
                $select->from('test_paper_questions');
                $where = new $select->where();
                $where->equalTo('test_paper_id',$data->testPaper);
                $select->where($where);
                $select->columns(array('section_id','lable_id','question_id', 'max_marks', 'neg_marks'));
                $statement = $sql->getSqlStringForSqlObject($select);
                $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                $questions = $run->toArray();

                $data = array();
                foreach ($sections as $section) {
                    
                    $Section = new stdClass();
                    $Section->section = $section;
                    $Section->section["questionData"] = array();
                    foreach ($questions as $question) {

                        if($section["id"] == $question["section_id"]) {

                            /*
                            * fetching question one by one
                            */
                            $select = $sql->select();
                            $select->from('questions');
                            $where = new $select->where();
                            $where->equalTo('delete',0);
                            // $where->equalTo('quality_check',0);
                            $where->equalTo('id',$question["question_id"]); 
                            $select->where($where);
                            $select->columns(array('id','question','description','type'));
                            $statement = $sql->getSqlStringForSqlObject($select);

                            $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                            $temp = $run->toArray();
                            if(count($temp))
                            {


                                $question['question'] = $temp[0]['question'];
                                $question['type'] = $temp[0]['type'];
                                $question['description'] = $temp[0]['description'];


                                if($temp[0]['type'] == 11) {
                                    
                                    /*
                                    * fetching questions of passage
                                    */
                                    $select = $sql->select();
                                    $select->from('questions');
                                    $where = new $select->where();
                                    $where->equalTo('delete',0);
                                    $where->equalTo('parent_id',$temp[0]['id']);
                                    // $where->equalTo('quality_check',0);
                                    $select->where($where);
                                    $select->columns(array('id','question','description','type'));
                                    $statement = $sql->getSqlStringForSqlObject($select);

                                    $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                                    $questionsOfPassage = $run->toArray();
                                    
                                    $question['passageQuestions'] = array();
                                    foreach($questionsOfPassage as $questionOfPassage ) {
                                        // $passageQuestions = new stdClass();
                                        
                                        /*
                                        * fetching options for every question
                                        */
                                        $select = $sql->select();
                                        $select->from('options');
                                        $where = new $select->where();
                                        $where->equalTo('question_id',$questionOfPassage["id"]);
                                        $select->where($where);
                                        $select->columns(array('option','answer','number'));
                                        $statement = $sql->getSqlStringForSqlObject($select);
                                        $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                                        $options = $run->toArray();
                                        $passageQuestions = $questionOfPassage;
                                        $passageQuestions['options'] = $options;

                                        array_push($question['passageQuestions'],$passageQuestions);
                                    }
                                
                                    array_push($Section->section["questionData"],$question);
                                }
                                else {
                                    
                                    /*
                                    * fetching options for every question
                                    *
                                    */
                                    $select = $sql->select();
                                    $select->from('options');
                                    $where = new $select->where();
                                    $where->equalTo('question_id',$question["question_id"]);
                                    $select->where($where);
                                    $select->columns(array('option','answer','number'));
                                    $statement = $sql->getSqlStringForSqlObject($select);
                                    $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                                    $options = $run->toArray();
                                    $question['options'] = $options;

                                    array_push($Section->section["questionData"],$question);
                                }
                            }
                        }
                    }
                    array_push($data, $Section);
                }
                $result->data = $data;
                $result->status = 1;
                $result->message = "ok";
                return $result;
            }
            catch(Execption $e) {
                $result->status = 0;
                $result->message = "ERROR: Some thing Went Wrong.";
                $result->error = $e->getMessage();
                return $result;
            }
        }

        /**
        * function save only details of test paper
        * 
        * @author Utkarsh Pandey
        * @date 09-07-2015
        * @param JSON
        * @return json with status and all data
        **/
        public function saveTestPaperDetails($data) {
            $result = new stdClass();
            try{
                $adapter = $this->adapter;
                $sql = new Sql($adapter);

                $update = $sql->update();
                $update->table('test_paper');
                $update->set(array(
                    'name'  => htmlentities($data->test_paper_name),
                    'type'  =>  $data->test_paper_type,
                    'class' =>  $data->test_paper_class,
                    'time'  =>  $data->test_paper_time,
                    'total_attempts'    =>  $data->test_paper_attempts,
                    'start_time'    =>  $data->test_paper_start,
                    'end_time'  =>  $data->test_paper_end,
                    'instructions'  =>  $data->test_paper_instructions,
                    'status'    =>  1,
                    'user_id'   =>  $data->userId,
                    'created'   =>  time()
                ));
                $update->where(array(
                    'id' => $data->id
                ));
                $statement = $sql->getSqlStringForSqlObject($update);
                $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

                /*
                * Delete the sujects which alresdy in test-paper
                */
                $delete = $sql->delete('test_paper_subjects');
                $where = new $delete->where();
                $where->equalTo('test_paper_id',$data->id);
                $delete->where($where);

                $statement = $sql->getSqlStringForSqlObject($delete);
                $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
                foreach($data->test_paper_subject as $subject) {
                    $insertSubject = new TableGateway('test_paper_subjects', $adapter, null, new HydratingResultSet());
                    $insertSubject->insert(array(
                        'test_paper_id' => $data->id,
                        'subject_id' => $subject
                    ));
                }
                $subject_id = $insertSubject->getLastInsertValue();
                

                $result->status = 1;
                $result->message = "Deatils Updated.";
                return $result;
                    
            }catch(Exception $e){
                $result->status = 0;
                $result->message = "ERROR: Some thing Went Wrong.";
                $result->error = $e->getMessage();
                return $result;
            }
        }


        /**
        * function to delete test paper
        * 
        * @author Utkarsh Pandey
        * @date 06-08-2015
        * @param JSON
        * @return json with status and all data
        **/
        public function deleteTestPaper($data) {
            $result = new stdClass();
            try{
                $adapter = $this->adapter;
                $sql = new Sql($adapter);

                $update = $sql->update();
                $update->table('test_paper');
                $update->set(array(
                    'deleted'   =>  1,
                    'user_id'   =>  $data->userId,
                    'created'   =>  time()
                ));
                $update->where(array(
                    'id' => $data->id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

                $result->status = 1;
                $result->message = "Successfully Deleted";
                return $result;
                    
            }catch(Exception $e){
                $result->status = 0;
                $result->message = "ERROR: Some thing Went Wrong.";
                $result->error = $e->getMessage();
                return $result;
            }
        }


        /**
        * function to create test paper from selected questions
        * 
        * @author Utkarsh Pandey
        * @date 09-09-2015
        * @param JSON
        * @return json with status and all data
        **/
        public function createQuestionPaper($data) {
            
            
            
            $result = new stdClass();
            $Lable = array();
            try {
                $adapter = $this->adapter;
                $sql = new Sql($adapter);
                
                $insert_test_paper = new TableGateway('test_paper', $adapter, null, new HydratingResultSet());

                if (empty($data->test_paper_start)) {
                    $data->test_paper_start = 1;
                }
                if (empty($data->test_paper_end)) {
                    $data->test_paper_end = 0;
                }
                if (empty($data->test_paper_cutoff)) {
                    $data->test_paper_cutoff = 0;
                }
                $test_paper_details = array(
                    'name' => $data->test_paper_name,
                    'type'  =>  $data->test_paper_type,
                    'class' =>  $data->test_paper_class,
                    'time'  =>  $data->test_paper_time,
                    'total_attempts'    =>  $data->test_paper_attempts,                 
                    'start_time'    =>  $data->test_paper_start,
                    'end_time'  =>  $data->test_paper_end,
                    'Instructions'  =>  $data->test_paper_instructions,
                    'status'    =>  1,
                    'user_id'   =>  $data->userId,
                    'mode'  =>  '1',
                    'created'   =>  time()
                );
                $insert_test_paper->insert($test_paper_details);

                $test_paper_id = $insert_test_paper->getLastInsertValue();

                foreach($data->test_paper_subject as $subject) {
                    $insertSubject = new TableGateway('test_paper_subjects', $adapter, null, new HydratingResultSet());
                    $insertSubject->insert(array(
                        'test_paper_id' => $test_paper_id,
                        'subject_id' => $subject
                    ));
                }
                //$subject_id = $insertSubject->getLastInsertValue();
                

                $insert_section = new TableGateway('section', $adapter, null, new HydratingResultSet());
                $insert_section->insert(array(
                    'name'  =>  'Section 1',
                    'total_question'    =>  $data->totalQuestion,
                    'test_paper'    =>  $test_paper_id,
                    'cut_off_marks' =>  $data->test_paper_cutoff
                ));
                $section_id = $insert_section->getLastInsertValue();

                //*****************************************************
                //find 11 how many child are there
                //type=>array(questionids)
                $Qs=array('1'=>array(),'2'=>array(),'4'=>array(),'5'=>array(),'6'=>array(),'7'=>array(),'8'=>array(),'1array()'=>array(),'12'=>array(),'13'=>array());
                //type=>nos of question of that type
                $Q=array('1'=>0,'2'=>0,'4'=>0,'5'=>0,'6'=>0,'7'=>0,'8'=>0,'10'=>0,'12'=>0,'13'=>0);
                foreach ($data->question_ids as $question) {
                    
                    $sql = new Sql($adapter);                   
                    $select = $sql->select();
                    
                    $select->from('questions');
                    
                    
                    $select->where("id = $question");                   
                    
                    $select->columns(array('type'));
                    $selectString = $sql->getSqlStringForSqlObject($select);
                    
                    
                    $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                    $rows = $run->toArray();                    
                    
                    if(count($rows)){
                        if($rows[0]['type']=='11'){
                            $sql = new Sql($adapter);                   
                            $select = $sql->select();
                            
                            $select->from('questions');                         
                            
                            $select->where("parent_id = $question");                    
                            
                            $select->columns(array('id'));
                            $selectString = $sql->getSqlStringForSqlObject($select);        
                            
                            $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                            $rows1 = $run->toArray();
                            $question_count = 0;
                            if(count($rows1)){
                                $question_count = count($rows1);                                
                            }

                            $insert_lable = new TableGateway('test_paper_labels', $adapter, null, new HydratingResultSet());
                            $insert_lable->insert(array(
                                
                                'section_id'    =>  $section_id,
                                'class_id'  =>  $data->test_paper_class,
                                'subject_id'    =>  $subject,
                                'question_type' =>  '11',
                                'number_of_question'    =>  $question_count
                            ));
                            
                            //$Lable[$question] = $insert_lable->getLastInsertValue();
                            $lable_id = $insert_lable->getLastInsertValue();

                            $insert = new TableGateway('test_paper_questions', $adapter, null, new HydratingResultSet());
                            if (empty($data->max_marks)) {
                                $data->max_marks = 1;
                            }
                            if (empty($data->neg_marks)) {
                                $data->neg_marks = 0;
                            }

                            $insert->insert(array(
                                'test_paper_id' =>  $test_paper_id,
                                'section_id'    =>  $section_id,
                                'lable_id'  =>  $lable_id,                          
                                'question_id'   =>  $question,
                                'max_marks' =>  $data->max_marks,
                                'neg_marks' =>  $data->neg_marks,
                                'created'   =>  time()
                            ));
                            $id = $insert->getLastInsertValue();
                            

                        }else{
                            
                            $Q[strval($rows[0]['type'])] =  intval($Q[strval($rows[0]['type'])]) + 1;               
                            //print_r($Qs[strval($rows[0]['type'])]);
                            array_push($Qs[strval($rows[0]['type'])], $question);
                            //$Lable[$question] = $rows[0]['type'].'type';
                            
                        }
                        
                    }
                    
                }

                //print_r($Qs);
                
                foreach ($Q as $key => $value) {
                    # code...
                    if($value > 0){
                    
                        $insert = new TableGateway('test_paper_labels', $adapter, null, new HydratingResultSet());
                        $insert->insert(array(
                            
                            'section_id'    =>  $section_id,
                            'class_id'  =>  $data->test_paper_class,
                            'subject_id'    =>  $subject,
                            'question_type' =>  $key,
                            'number_of_question'    =>  $value
                        ));
                        $lable_id = $insert->getLastInsertValue();
                        //print_r($Qs[$key]);
                        if(count($Qs[$key])){
                            foreach ($Qs[$key] as $ky => $value1) {
                                # code...
                                $insert = new TableGateway('test_paper_questions', $adapter, null, new HydratingResultSet());
                                if (empty($data->max_marks)) {
                                    $data->max_marks = 1;
                                }
                                if (empty($data->neg_marks)) {
                                    $data->neg_marks = 0;
                                }

                                $insert->insert(array(
                                    'test_paper_id' =>  $test_paper_id,
                                    'section_id'    =>  $section_id,
                                    'lable_id'  =>  $lable_id,                          
                                    'question_id'   =>  $value1,
                                    'max_marks' =>  $data->max_marks,
                                    'neg_marks' =>  $data->neg_marks,
                                    'created'   =>  time()
                                ));
                                $id = $insert->getLastInsertValue();
                            }
                        }                   
                                
                    }
                }           
                

                $result->test_paper_id = $test_paper_id;
                $result->status = 1;
                $result->message = "Successfully Saved.";
                return $result;
            }catch(Exception $e){
                $result->status = 0;
                $result->message = "ERROR: Some thing Went Wrong.";
                $result->error = $e->getMessage();
                return $result;
            }
        }
    }
?>