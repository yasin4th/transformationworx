<?php
	@session_start;
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	
	//including connection.php file to make connection with database
	require_once 'Connection.php';
	
	class DifficultyApi extends Connection{
		
		/**
		* function fetch difficulty details to show the difficulty and their description to user on difficulty.php file
		*
		* @author Utkarsh Pandey
		* @date 17-05-2015
		* @param JSON limit and offset
		* @return JSON status and data(difficulty and description)
		**/
		public function getDifficultyData($data) {
			
			$result = new stdClass();
			
			try{
				
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('difficulty');
				$select->where(array(
					'delete'	=>	0
				));
				// $select->limit($data->limit);
				// $select->offset($data->offset);
				$select->columns(array('id','name' => 'difficulty', 'description',));
				
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				$result->status = 1;
				$result->data = $res;
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
			
		}
		
		
		/**
		* function add new difficulty details row in difficulty table
		*
		* @author Utkarsh Pandey
		* @date 17-05-2015
		* @param JSON with all required details
		* @return JSON status and message
		**/
		public function addDifficulty($data) {  
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('difficulty');
				$select->where(array(
					'difficulty'	=>	htmlentities($data->difficulty),
					'delete'	=>	0
				));
				$select->columns(array('id','difficulty', 'description',));
	
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				if(count($res) == 0) {
					$insert = new TableGateway('difficulty', $adapter, null, new HydratingResultSet());
					$insert->insert(array(
						'difficulty'	=>	htmlentities($data->difficulty),
						'description'	=>	htmlentities($data->description),
						'user_id'	=>	1, //$_SESSION['userId'],
						'created'	=>	time()
					));
					$id= $insert->getLastInsertValue();
					if($id) {
						$result->id = $id;
						$result->status = 1;
						$result->message = "Row Success Added";
						return $result;
					}
					else {
						$result->status = 0;
						$result->message = "Error Occured";
						return $result;
					}
				}
				else {
					$result->status = 0;
					$result->message = "Duplicate difficulty level entry.";
					return $result;
				}
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		
		
		/**
		* function delete a row in difficulty table
		*
		* @author Utkarsh Pandey
		* @date 17-05-2015
		* @param JSON id
		* @return JSON status and message
		**/
		public function DeleteDifficultyRow($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('difficulty');
				$update->set(array(
					'delete'	=> 1,
					'created'	=>	time()
				));
				$update->where(array(
					'id' => $data->id
				));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				$result->status = 1;
				$result->message = "Successfully Deleted.";
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		
		
		/**
		* function update difficulty and description in a row of difficulty table
		*
		* @author Utkarsh Pandey
		* @date 17-05-2015
		* @param JSON with all required details
		* @return JSON status and message
		**/
		public function updateDifficultyRow($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);								
				$select = $sql->select();
				$select->from('difficulty');
				$select->where(array(
					'difficulty'	=>	htmlentities($data->difficulty),
					'delete'	=>	0
				));
				$select->columns(array('id','difficulty', 'description',));
	
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = array();
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				if(count($res) == 0) {
					$update = $sql->update();
					$update->table('difficulty');
					$update->set(array(
						'difficulty'	=> htmlentities($data->difficulty),
						'description'	=>	htmlentities($data->description),
						'created'	=>	time()
					));
					$update->where(array(
						'id' => $data->id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
					$result->status = 1;
					$result->message = "Successfully Update.";
					return $result;
				}
				else if(count($res) == 1 && $res[0]['id'] == $data->id) {
						$result->status = 1;
						$result->message = "Successfully Update.";
						return $result;
					}
				else {
					$result->status = 0;
					$result->message = 'Not Updated because this Difficulty already created.';
					return $result;
				}
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
	}
	
?>