<?php 
class Utility
{
	public function sendEmail($mailTo, $mailSubject, $mailBody) 
	{
		$mailBodyHeader = '<!DOCTYPE html>
		<html>
		<body>
		<div text="#000000" style="background-color:#ffffff;width:100%!important">
		    <div>
		        <table style="font-size:11px;line-height:14px;color:#666666;background-image:url(img/bg-img.jpg);background-repeat:repeat;font-family:Helvetica,Arial,sans-serif" border="0" cellspacing="0" cellpadding="0" width="100%" align="center" bgcolor="#e4e4e4">
		            <tbody>
		                <tr>
		                    <td>
		                        <table style="font-size:11px;line-height:14px;color:#666666" border="0" cellspacing="0" cellpadding="0" width="648" align="center">
		                            <tbody>
		                                <tr>
		                                    <td style="line-height:0;font-size:0" height="47">&nbsp;</td>
		                                </tr>
		                            </tbody>
		                        </table>
		                        <table style="font-size:13px;line-height:18px;color:#666666;border-radius:4px;border:#e1e1e1 1px solid" border="0" cellspacing="0" cellpadding="0" width="648" align="center" bgcolor="#ffffff">
		                            <tbody>
		                                <tr>
		                                    <td>
		                                        <table style="font-size:13px;line-height:18px;color:#666666" border="0" cellspacing="0" cellpadding="0" width="648" align="center">
		                                            <tbody>
		                                                <tr>
		                                                    <td style="line-height:0;font-size:0" colspan="3" height="34">&nbsp;</td>
		                                                </tr>
		                                                <tr>
		                                                    <td width="40">&nbsp;</td>
		                                                    <td align="center">
		                                                        <p>
		                                                            <a href="http://exampointer.com/" target="_blank"><img style="border:0px;float:left" src="http://exampointer.com/assets/global/img/exampointer-logo-colored-small.png" alt="" ></a>
		                                                        </p>
		                                                    </td>
		                                                    <td width="40">&nbsp;</td>
		                                                </tr>
		                                                <tr>
		                                                    <td style="line-height:0;font-size:0" colspan="3" height="40">&nbsp;</td>
		                                                </tr>
		                                                <tr>
		                                                    <td width="40">&nbsp;</td>
		                                                    <td width="40">
		                                                        <h2 style="text-align:center">For Your Examination Preparation Process</h2></td>
		                                                </tr>
		                                                <tr>
		                                                    <td style="line-height:0;font-size:0" colspan="3" height="22">&nbsp;</td>
		                                                </tr>
		                                                <!--<tr>
		                                                    <td width="40">&nbsp;</td>
		                                                    <td>
		                                                        <p><strong>Welcome,&nbsp;exampointer</strong> [<a style="color:#2673b4;text-decoration:underline" href="http://exampointer.com/" target="_blank">Read more</a>].</p>
		                                                        <p style="text-align: center;">
		                                                            <a href="http://exampointer.com" target="_blank"><img style="border:0" src="http://exampointer.com/assets/global/img/exampointer-logo-colored-small.png" alt="" width="420" height="240"></a>
		                                                        </p>http://localhost/test_engine_new/assets/global/img/exampointer-logo-colored-small.png
		                                                    </td>
		                                                    <td width="40">&nbsp;</td>
		                                                </tr>-->
		                                                <!--<tr>
		                                                    <td style="line-height:0;font-size:0" colspan="3" height="35">&nbsp;</td>
		                                                </tr>
		                                                <tr>
		                                                    <td style="font-size:0px;line-height:0px" width="40">&nbsp;</td>
		                                                    <td style="font-size:0px;line-height:0px" height="1" bgcolor="#d5d5d5">&nbsp;</td>
		                                                    <td style="font-size:0px;line-height:0px" width="40">&nbsp;</td>
		                                                </tr>
		                                                <tr>
		                                                    <td style="line-height:0;font-size:0" colspan="3" height="40">&nbsp;</td>
		                                                </tr>-->
		                                                <tr>
		                                                    <td width="40">&nbsp;</td>
		                                                    <td style="font-size:20px;line-height:20px;color:#000000;font-weight:bold" align="center">&nbsp;</td>
		                                                    <td width="40">&nbsp;</td>
		                                                </tr>
		                                                <tr>
		                                                    <td style="line-height:0;font-size:0" colspan="3" height="22">&nbsp;</td>
		                                                </tr>
		                                                <tr>
		                                                    <td width="40">&nbsp;</td>
		                                                    <td>';
        $mailBodyFooter = '</td>
		                                                    <td width="40">&nbsp;</td>
		                                                </tr>
		                                                <tr>
		                                                    <td style="line-height:0;font-size:0" colspan="3" height="35">&nbsp;</td>
		                                                </tr>
		                                                <!--<tr>
		                                                    <td style="line-height:0;font-size:0" colspan="3" height="25">&nbsp;</td>
		                                                </tr>
		                                                <tr>
		                                                    <td style="line-height:0;font-size:0" colspan="3" height="25">&nbsp;</td>
		                                                </tr>
		                                                <tr>
		                                                    <td width="40">&nbsp;</td>
		                                                    <td width="40">
		                                                        <br>
		                                                    </td>
		                                                </tr>-->
		                                                <tr>
		                                                    <td style="font-size:0px;line-height:0px" width="40">&nbsp;</td>
		                                                    <td style="font-size:0px;line-height:0px" height="1" bgcolor="#d5d5d5">&nbsp;</td>
		                                                    <td style="font-size:0px;line-height:0px" width="40">&nbsp;</td>
		                                                </tr>
		                                                <tr>
		                                                    <td style="line-height:0;font-size:0" colspan="3" height="31">&nbsp;</td>
		                                                </tr>
		                                                <tr>
		                                                    <td width="40">&nbsp;</td>
		                                                    <td style="border:#e5e5e5 1px solid;padding:17px 10px 22px 10px;border-radius:4px" align="center" bgcolor="#f5f5f5">
		                                                        <p><strong>Get Success in Exams</strong></p>
		                                                        <p>Exampointer has the full rights to change the contents of any programs, resources or any information provided on the website at its discretion. Further exampointer can change the prices, eligibility, period of offer, offers expiry or the contents of the products and services being offered on the website from time to time.</p>
		                                                        <p><a style="color:#2673b4;text-decoration:underline;display:inline-block;margin-top:6px" href="http://exampointer.com/registration.php" target="_blank">Register Now</a></p>
		                                                    </td>
		                                                    <td width="40">&nbsp;</td>
		                                                </tr>
		                                                <tr>
		                                                    <td style="line-height:0;font-size:0" colspan="3" height="31">&nbsp;</td>
		                                                </tr>
		                                            </tbody>
		                                        </table>
		                                    </td>
		                                </tr>
		                            </tbody>
		                        </table>
		                        <table style="font-size:13px;line-height:18px;color:#666666" border="0" cellspacing="0" cellpadding="0" width="650" align="center">
		                            <tbody>
		                                <tr>
		                                    <td colspan="3" height="44">&nbsp;</td>
		                                </tr>
		                                <tr>
		                                    <td style="font-size:20px;line-height:20px;color:#000000;font-weight:bold" colspan="3" align="center">Follow Exampointer</td>
		                                </tr>
		                                <tr>
		                                    <td style="line-height:0;font-size:0" colspan="3" height="5">&nbsp;</td>
		                                </tr>
		                                <tr>
		                                    <td style="color:#010101;padding:0 30px" colspan="3" align="center">Be the first to know about the exampointer, Services and Courses</td>
		                                </tr>
		                                <!--<tr>
		                                    <td style="line-height:0;font-size:0" colspan="3" height="18">&nbsp;</td>
		                                </tr>
		                                <tr>
		                                    <td colspan="3" align="center">
		                                        <table border="0" cellspacing="0" cellpadding="0" width="145">
		                                            <tbody>
		                                                <tr>
		                                                    <td width="38">
		                                                        <a style="display:inline-block" href="#" target="_blank"><img style="display:block" src="img/facebook.png" border="0" alt="" class="CToWUd"></a>
		                                                    </td>
		                                                    <td width="38">
		                                                        <a style="display:inline-block" href="#" target="_blank"><img style="display:block" src="img/twitter.png" border="0" alt="" class="CToWUd"></a>
		                                                    </td>
		                                                    <td width="38">
		                                                        <a style="display:inline-block" href="#" target="_blank"><img style="display:block" src="img/google-plus.png" border="0" alt="" class="CToWUd"></a>
		                                                    </td>
		                                                    <td width="30">
		                                                        <a style="display:inline-block" href="#" target="_blank"><img style="display:block" src="img/pinterest.png" border="0" alt="" class="CToWUd"></a>
		                                                    </td>
		                                                </tr>
		                                            </tbody>
		                                        </table>
		                                    </td>
		                                </tr>-->
		                                <tr>
		                                    <td style="line-height:0;font-size:0" colspan="3" height="35">&nbsp;</td>
		                                </tr>
		                                <tr>
		                                    <td style="font-size:0px;line-height:0px" colspan="3" height="1" bgcolor="#c9c9c9">&nbsp;</td>
		                                </tr>
		                                <tr>
		                                    <td style="line-height:0;font-size:0" colspan="3" height="21">&nbsp;</td>
		                                </tr>
		                                <tr>
		                                    <td width="10">&nbsp;</td>
		                                    <td>You receive this newsletter because you send enquiry mail to it on <a style="color:#2673b4;text-decoration:underline" href="http://exampointer.com" target="_blank">Exampointer Website</a></td>
		                                    <td width="10">&nbsp;</td>
		                                </tr>
		                                <tr>
		                                    <td style="line-height:0;font-size:0" colspan="3" height="40">&nbsp;</td>
		                                </tr>
		                                <tr>
		                                    <td width="40">&nbsp;</td>
		                                    <td>© 2016 <span style="color:#666666;text-decoration:none">Exampointer.com</span></td>
		                                    <td width="40">&nbsp;</td>
		                                </tr>
		                                <tr>
		                                    <td style="line-height:0;font-size:0" colspan="3" height="40">&nbsp;</td>
		                                </tr>
		                            </tbody>
		                        </table>
		                    </td>
		                </tr>
		            </tbody>
		        </table>
		    </div>
		</div>
		</body>
		</html>';

		$mailFrom = "support@exampointer.com"; 
		$mailHeader = "From: ".$mailFrom."\r\n"; 
        $mailHeader .= "Content-type: text/html; charset=iso-8859-1\r\n"; 

		$mailBody = $mailBodyHeader . $mailBody . $mailBodyFooter ;
		

		if(mail($mailTo, $mailSubject, $mailBody, $mailHeader)) {
		// var_dump($mailBody);
		// die();		

			return 1;
			// echo 'Message has been sent';
		} else {
			// return 0;
			//echo "hii";
			return 0;
			// Need to create a logger
			// echo 'Message could not be sent.';
			// echo 'Mailer Error: ' . $mail->ErrorInfo;
		}
	}
}
?>