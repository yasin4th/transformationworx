<?php

	/**
	* 
	*/
	class Operation {

		//Finding the correct option

		public static function guess_correct_option($data)
		{
			//Explode from (* ) symbol
			$data_new=array(explode("* ", $data));
			//Explode the string after (* ) from (.)
			//Example: if correct option is B, then $data_new[0][1] will be B. Option value,
		    $final_explode=array(explode(".", $data_new[0][1]));
			//After exploding by (.), we get 'B' in $final_explode[0][0]
			$correct_option= $final_explode[0][0];
			//Return correct option number
			return $correct_option;
		}

		//Finding number of Options

		public static function calculate_number_of_options($data)
		{
			//Calculate positions of tags A. B. C. D. E. F.
			$a= strpos($data, "A. ");
			$b= strpos($data, "B. ");
			$c= strpos($data, "C. ");
			$d= strpos($data, "D. ");
			$e= strpos($data, "E. ");
			$f= strpos($data, "F. ");

			//Establishing the value for Count of Options
			//Returning the Count simultaneously
			if ($a!=0 && $b!=0 && $c==0 && $d==0 && $e==0 && $f==0) 
			{
				$option_count=2;
				return $option_count;
			}
			elseif ($a!=0 && $b!=0 && $c!=0 && $d==0 && $e==0 && $f==0) 
			{
				$option_count=3;
				return $option_count;
			}
			elseif ($a!=0 && $b!=0 && $c!=0 && $d!=0 && $e==0 && $f==0) 
			{
				$option_count=4;
				return $option_count;
			}
			elseif ($a!=0 && $b!=0 && $c!=0 && $d!=0 && $e!=0 && $f==0) 
			{
				$option_count=5;
				return $option_count;
			}
			elseif ($a!=0 && $b!=0 && $c!=0 && $d!=0 && $e!=0 && $f!=0) 
			{
				$option_count=6;
				return $option_count;
			}
		}

		//Extracting the Question Part

		public static function extract_question($data, $correct)
		{
			//Explode from First Option i.e. A.
			$exploded=explode("A. ", $data);
			//Required Question will be exploded[0] 
			$question_part=$exploded[0];
			//Return the Question Part After Stripping Unwanted Question Numbers
			if ($correct!="A") 
			{
				$question_explode=explode(".", $exploded[0]);
				unset($question_explode[0]);
				$question_part=implode(" ", $question_explode);
				return $question_part;
			}
			else
			{
				$question_explode=explode(" ", $exploded[0]);
				$size=sizeof($question_explode);
				unset($question_explode[$size-2]);
				unset($question_explode[1]);
				$question_part=implode(" ", $question_explode);
				return $question_part;
			}
		}

		//Extracting the Options

		public static function extract_options($data, $question, $correct, $count)
		{
			//Calculating (A.) position
			$a_position= strpos($data, "A. ");
			//Separating options from whole string 
			$options=substr($data, $a_position);
			//New values of Positions of Options 	
			$new_a_position= strpos($options, "A. ");
			$new_b_position= strpos($options, "B. ");
			$new_c_position= strpos($options, "C. ");
			$new_d_position= strpos($options, "D. ");
			$new_e_position= strpos($options, "E. ");
			$new_f_position= strpos($options, "F. ");
			//Extracting Option A
			$option_a= substr($options, $new_a_position+2, $new_b_position-2);
			//Check if next Option exists
			if ($new_c_position!=0) 
			{
				//If next Option exists, this Options depends on position of next option
				$option_b= substr($options, $new_b_position+2, $new_c_position-$new_b_position-2);
			}
			else
			{	
				//If next Option doesnt exists, no dependency
				$option_b= substr($options, $new_b_position+2);
			}
			//Check if next Option exists
			if ($new_d_position!=0) 
			{
				//If next Option exists, this Options depends on position of next option
				$option_c= substr($options, $new_c_position+2, $new_d_position-$new_c_position-2);
			}
			else
			{
				//If next Option doesnt exists, no dependency
				$option_c= substr($options, $new_c_position+2);
			}
			//Check if next Option exists
			if ($new_e_position!=0) 
			{
				//If next Option exists, this Options depends on position of next option
				$option_d= substr($options, $new_d_position+2, $new_e_position-$new_d_position-2);
			}
			else
			{
				//If next Option doesnt exists, no dependency
				$option_d= substr($options, $new_d_position+2);
			}
			//Check if next Option exists
			if ($new_f_position!=0) 
			{
				//If next Option exists, this Options depends on position of next option
				$option_e= substr($options, $new_e_position+2, $new_f_position-$new_e_position-2);
			}
			else
			{
				//If next Option doesnt exists, no dependency
				$option_e= substr($options, $new_e_position+2);
			}
			//Switch Case for returning Only the Options that are provided
			switch ($count) {
				case '2':
					return array("OPTION A"=> $option_a, "OPTION B"=> $option_b);
					break;
				case '3':
					return array("OPTION A"=> $option_a, "OPTION B"=> $option_b, "OPTION C"=> $option_c );
					break;
				case '4':
					return array("OPTION A"=> $option_a, "OPTION B"=> $option_b, "OPTION C"=> $option_c, "OPTION D"=> $option_d );
					break;
				case '5':
					return array("OPTION A"=> $option_a, "OPTION B"=> $option_b, "OPTION C"=> $option_c, "OPTION D"=> $option_d, "OPTION E"=> $option_e );
					break;
			}
		}

		//Trimming (* ) symbol from every Option

		public static function strip_star_symbol($data, $count)
		{
			$i=0;
			//Initialising 
			$new_options=array();
			//Trimming for each option individually
			foreach ($data as $option_number => $option_value) 
			{
				//Trimming Operation
				$option_value = trim($option_value, "* ");
				$new_options[$i]=$option_value;
				$i++;
			}
			// modified by me
				$options = array();
				foreach ($new_options as $option) {
					$opt = new stdClass();
					$opt->option = $option;
					$opt->answer = 0;
					array_push($options, $opt);
				}
				return $options;
			//Switch Case for returning Only the Options that are provided
			/*switch ($count) {
				case '2':
					return array("OPTION A"=> $new_options[0], "OPTION B"=> $new_options[1]);
					break;
				case '3':
					return array("OPTION A"=> $new_options[0], "OPTION B"=> $new_options[1], "OPTION C"=> $new_options[2] );
					break;
				case '4':
					return array("OPTION A"=> $new_options[0], "OPTION B"=> $new_options[1], "OPTION C"=> $new_options[2], "OPTION D"=>$new_options[3] );
					break;
				case '5':
					return array("OPTION A"=> $new_options[0], "OPTION B"=> $new_options[1], "OPTION C"=> $new_options[2], "OPTION D"=>$new_options[3], "OPTION E"=> $new_options[4]);
					break;
			}
			//Return the Associative array having Options
			return $new_options;*/
		}

	}



?>