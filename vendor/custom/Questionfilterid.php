<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;
	
	//including connection.php file to make connection with database
	require_once 'Connection.php';
	
	class Questionfilterid extends Connection 
	{
		public function filterQuestions($data) 
		{
			date_default_timezone_set('Asia/Kolkata');
			$result = new stdClass();

			try
			{
				$adapter = $this->adapter;				

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('qs'=>'questions'));
				if(isset($data->classes))
				{					
					$select->where->equalTo('class_id', $data->classes);
				}
				if(isset($data->questionTypes))
				{					
					$select->where->equalTo('type', implode("",$data->questionTypes));
				}

				$select->where->equalTo('parent_id',0)->equalTo('delete',0);
				$select->columns(array('id'));					
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				
				$qids = array_column($run->toArray(), 'id');


				if(isset($data->subjects))
				{					
					$qids = $this->filterByTag($qids, $data->subjects, 1);
				}
				if(isset($data->units))
				{					
					$qids = $this->filterByTag($qids, $data->units, 2);
				}
				if(isset($data->chapters))
				{					
					$qids = $this->filterByTag($qids, $data->chapters, 3);
				}
				if(isset($data->topics))
				{					
					$qids = $this->filterByTag($qids, $data->topics, 4);
				}
				if(isset($data->tags))
				{					
					$qids = $this->filterByTag($qids, $data->tags, 5);
				}
				if(isset($data->difficultyLevel))
				{					
					$qids = $this->filterByTag($qids, $data->difficultyLevel, 6);
				}
				if(isset($data->skills))
				{					
					$qids = $this->filterByTag($qids, $data->skills, 7);
				}
				//$qids = array_unique($qids);

				$result->status = 1;				
				$result->question_id = $qids ; 
				$result->message = 'Operation Successful.';					

				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function filterByTag($qid, $tag, $type = 1)
		{
			try
			{
				$adapter = $this->adapter;				

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('tagsofquestion');
				$select->where->in('question_id', $qid)->in('tagged_id', $tag)->equalTo('tag_type', $type);
				
				$select->columns(array('question_id'));

				$statement = $sql->getSqlStringForSqlObject($select);
				
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				
				return array_values(array_unique(array_column($run->toArray(), 'question_id')));
			}
			catch(Exception $e)
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
	}
?>	