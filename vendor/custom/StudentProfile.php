<?php
	@session_start();
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;
	
	//including connection.php file to make connection with database
	require_once 'Connection.php';
	
	class StudentProfile extends Connection{

		public function fetchStudentProfileData($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('profile');
				$select->where(array(
					'user_id'	=>	$data->userId
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				$result->data = $res;

				$result->status = 1;
				$result->message = "Operation Successful.";

				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}	

		public function fetchStudents($data) {
			$result = new stdClass();
			$result->start = microtime();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
			
				$select = $sql->select();

				$select->from(array('users' => 'users'))
						->join(array('profile'=>'profile'),'users.id = profile.user_id', array('mobile' => new Expression('IFNULL(mobile,"")'),'class' => new Expression('IFNULL(class,"")') ),'left')
						->join(array('coupons'=>'coupons'),'users.id = coupons.student_id', array('institute_id' => new Expression('IFNULL(institute_id,"")') ),'left')
						->join(array('u'=>'users'),'coupons.institute_id = u.id', array('institute' => new Expression('IFNULL(u.first_name,"")') ),'left');
			
				$select->order('id desc');
				$select->columns(array('id', 'first_name' => new Expression('IFNULL(users.first_name,"")'), 'last_name' => new Expression('IFNULL(users.last_name,"")'), 'email' => new Expression('IFNULL(users.email,"")'),'registered_on','registered_on1' => new Expression('FROM_UNIXTIME(users.registered_on,"%d-%m-%y")'),'type'=>'mode' ));
				if (isset($data->user_id)){
					$select->where(array(
						'users.id'	=>	$data->user_id
					));
				}
				if (isset($data->to) && isset($data->from)) {
					if($data->to != '' && $data->from != ''){
						$from = strtotime($data->from);
						$to = strtotime($data->to);
						$select->where('(profile.registered_on BETWEEN "'.$from.'" AND "'.$to.'")');
					}
				}else if(!isset($data->to) && isset($data->from)) {
					if($data->from != ''){
						$from = strtotime($data->from);
						//$to = strtotime($data->to);
						$select->where('(profile.registered_on > "'.$from.'")');
					}
				}
				if (isset($data->name)) {
					$select->where('(users.first_name LIKE "%'.$data->name.'%" OR users.last_name LIKE "%'.$data->name.'%" )');
				}
				if (isset($data->email)) {
					$select->where('users.email LIKE "%'.$data->email.'%" ');
				}
				if(isset($data->role)){
					$select->where('users.role = "'.$data->role.'"');
				}
				$statement = $sql->getSqlStringForSqlObject($select);
				// echo $statement;
				// die();
				
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				$result->total_questions = count($res);

				if (isset($data->limit)) {
					$select->limit(50);
					$select->offset(intval($data->limit));
				}

				$statement = $sql->getSqlStringForSqlObject($select);
				
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				$result->data = $res;
				$result->batch = [];
				$user_batches =[];
				if (isset($data->user_id)){
					$stm = "select batch,user from batch_users where user=".$data->user_id." and deleted = 0";
					$run = $adapter->query($stm, $adapter::QUERY_MODE_EXECUTE);
					$batch = $run->toArray(); 	
					foreach ($batch as $key => $batch) {
						array_push($user_batches,$batch['batch']);
					}
					$result->batch = $user_batches;
				}
				$result->end = microtime();
				$result->status = 1;
				$result->message = "Operation Successful.";
				
				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		function updateStudent($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$time = time();

				$select = $sql->select();
				$select->from('users');

				$where = array();
				$select->where('id != "'.$request->user_id.'"');

				if (isset($request->email)) {
					$where['email'] = htmlentities($request->email);
				}

				$select->where($where);

				$select->columns(array('email'));
				$selectString = $sql->getSqlStringForSqlObject($select);

				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$res = $res->toArray();

				if(count($res) == 0) {

					$update = $sql->update();
					$update->table('profile');
					$data = array(
						'first_name'	=>	$request->firstname,
						'last_name'	=>	$request->lastname,
						'email'	=>	$request->email,
						'class'	=>	$request->class,
						'mobile'	=>	$request->mobile
					);

					$update->set($data);
					$update->where(array(
						'user_id' => $request->user_id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();


					$update = $sql->update();
					$update->table('users');
					$data = array(
						'first_name'	=>	$request->firstname,
						'last_name'	=>	$request->lastname,
						'email'	=>	$request->email	                    

					);

					$update->set($data);
					$update->where(array(
						'id' => $request->user_id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();

						/***********************
						User Batch update start
						************************/
						$select = $sql->select();
						$select->from('batch_users');
						$select->where(array('user' => $request->user_id));
						$select->columns(array('batch'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$batch_users = $res->toArray();

						$update = $sql->update();
						$update->table('batch_users');
						$update->set(array(
						   
							'modified'   =>  $time,
							'deleted' => 1
						));
						$update->where(array(
							'user' => $request->user_id
						));
						$statement = $sql->prepareStatementForSqlObject($update);
						$statement->execute(); 


					if(count($request->batch) != 0)
					{	
						
						if(count($batch_users) == 0) {
							foreach ($request->batch as $key => $batch) {
								$insert_new = new TableGateway('batch_users', $adapter, null, new HydratingResultSet());
								$insert_new->insert(array(
								'user' => $request->user_id,
								'batch' => htmlentities($batch),
								'created' => $time
								));

								$select = $sql->select();
								$select->from('batch_program');
								$select->where(array(
									'batch'	=>	htmlentities($batch),
									'deleted'	=> 0
								));
								$select->columns(array('program'));
								$selectString = $sql->getSqlStringForSqlObject($select);
								$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
								$program = $res->toArray();
								if(count($program) != 0){
									foreach ($program as $key => $program) {
										$select = $sql->select();
										$select->from('student_program');
										$select->where(array(
											'user_id'	=> $request->user_id,
											'test_program_id'	=> $program['program']
										));
										$select->columns(array('id'));
										$selectString = $sql->getSqlStringForSqlObject($select);
										$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
										$bought = $run->toArray();
										if(count($bought) == 0)
										{
											$insert_program = new TableGateway('student_program', $adapter, null, new HydratingResultSet());
											$insert_program->insert(array(
											'user_id' => $request->user_id,
											'test_program_id' => $program['program'],
											'time' => $time
											));

										}


									}
								}

								
							}
						}
						else
						{
							foreach ($request->batch as $key => $batch) {
						
								foreach ($batch_users  as  $i => $user_batch) {
								
									if($user_batch['batch'] == $batch)
									{
										$update = $sql->update();
										$update->table('batch_users');
										$update->set(array(
											'modified'   =>  $time,
											'deleted' => 0
										));

										$where = new $update->where();
										$where->equalTo('batch',$batch);
										$where->equalTo('user' , $request->user_id);
										$update->where($where);
										$statement = $sql->prepareStatementForSqlObject($update);
										$statement->execute();
										break;
									}
									else
									{	
										if($i == (count($batch_users)-1)){
											$insertBatch = new TableGateway('batch_users', $adapter, null, new HydratingResultSet());
											$insertBatch->insert(array(
												'user' => $request->user_id,
												'batch' => $batch,
												'created' => $time,
												'deleted' => 0
											)); 

											$select = $sql->select();
											$select->from('batch_program');
											$select->where(array(
												'batch'	=>	htmlentities($batch),
												'deleted'	=> 0
											));
											$select->columns(array('program'));
											$selectString = $sql->getSqlStringForSqlObject($select);
											$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
											$program = $res->toArray();
											if(count($program) != 0){
												foreach ($program as $key => $program) {
													$select = $sql->select();
													$select->from('student_program');
													$select->where(array(
														'user_id'	=> $request->user_id,
														'test_program_id'	=> $program['program']
													));
													$select->columns(array('id'));
													$selectString = $sql->getSqlStringForSqlObject($select);
													$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
													$bought = $run->toArray();
													if(count($bought) == 0)
													{
														$insert_program = new TableGateway('student_program', $adapter, null, new HydratingResultSet());
														$insert_program->insert(array(
														'user_id' => $request->user_id,
														'test_program_id' => $program['program'],
														'time' => $time
														));

													}


												}
											}
											
										}
									}

								}
							}

						}
					}
					

					/***********************
					 User Batch update end
					************************/

					$result->status = 1;
					$result->message = "Student successfully updated.";

					return $result;
				}else{
					$result->status = 0;
					$result->message = "Email already in use.";
				return $result;
				}
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}



		public function updateStudentProfileData($data) {

			$result = new stdClass();
			
			try{
				
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('profile');

				$update->set(array(

					'first_name' => $data->first_name,
					'last_name' => $data->last_name,
					'mobile' => $data->mobile,
					'interests' => $data->interests,
					'occupation' => $data->occupation,
					'about' => $data->about,
					'website_url' => $data->website_url

					));

				$update->where(array(
					'email'	=>	$_SESSION['user']
				));
				
				$updateString = $sql->getSqlStringForSqlObject($update);
				$update = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				
				
				$result->status = 1;
			
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}


		}


		/**
		* Function to change student profile pic
		*
		* @author Utkarsh Pandey
		* @date 26-07-2015
		* @param JSON of image data
		* @return JSON status and message
		**/

		public function studentChangeProfilePicture($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$orgFileName = $data->image["name"];
				$ext = pathinfo($orgFileName, PATHINFO_EXTENSION);
				$newFileName =  md5($data->userId).'.'.$ext;
				$destination = __DIR__."/../../profilePictures/".$newFileName;
				move_uploaded_file($data->image["tmp_name"], $destination);

				$sql = new Sql($adapter);
				
				$update = $sql->update();
				$update->table('profile');
				$src = "profilePictures/".$newFileName;
				$update->set(array(
					'image' => $src
				));

				$update->where(array(
					'email'	=>	$data->user
				));
				
				$updateString = $sql->getSqlStringForSqlObject($update);
				$update = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				
				$result->status = 1;
				$result->message = "Avtar Updated.";
				$result->src = $src."?".time();
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function changeProfilePassword($data) {

			$result = new stdClass();
			
			try{
				
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('users');

				$select->where(array(
					'email'	=>	$_SESSION['user']
				));
				
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				if($data->new_password == "" || $data->confirm_password == "" || $data->current_password == "")
				{
					$result->status = 2;
					$result->data = "Some fields are required";
				}
				else if($res[0]['password'] != md5($data->current_password))
				{
					$result->status = 2;
					$result->data = "You have entered the wrong current password";
				}
				else if($data->new_password != $data->confirm_password)
				{
					$result->status = 2;
					$result->data = "The new password and confirm password fields do not match";
				}
				else
				{
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('users');

					$update->set(array(
						'password' => md5($data->confirm_password)
					));

					$update->where(array(
						'email'	=>	$_SESSION['user']
					));
					
					$updateString = $sql->getSqlStringForSqlObject($update);
					$run = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE);

					$result->status = 1;
				}
				
			
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}


		}
		public function changeProfilePassword1($data) {

			$result = new stdClass();
			
			try{
				
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('users');

				$select->where(array(
					'id'	=>	$data->user_id
				));
				
				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('users');

				$update->set(array(
					'password' => md5($data->confirm_password)
				));

				$update->where(array(
					'id'	=>	$data->user_id
				));
				
				$updateString = $sql->getSqlStringForSqlObject($update);
				$run = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE);					

				$result->status = 1;
			
				
			
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}


		}

	}