<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	require_once 'Connection.php';

	class ExamAttemptReport extends Connection
	{
		function fetchexamattemptreport($req)
		{
			$result= new stdClass();

			try
			{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from(array('are' =>'attempt_reports'));

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement,$adapter::QUERY_MODE_EXECUTE);

				$res =  $run->toArray();
				$result->total_attempt = count($res);
				$select->join(array('user' =>'users'),'user.id=are.student_id',array('first_name','email'),'left');
				$select->order('attempt_id desc');

				if (isset($req->limit)) {
					$select->limit(50);
					$select->offset(intval($req->limit));
				}

				$select->columns(array('attempt_id','test_paper_id','name','marks_archived','total_questions','total_marks','attempted_questions','start_time' => new Expression('FROM_UNIXTIME(start_time,"%d-%m-%y")')));

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement,$adapter::QUERY_MODE_EXECUTE);
				$res =  $run->toArray();

				$result->status = 1;
				$result->data = $res;
				$result->message = "Sucess:Data fetch successfully";

				return $result;

			}
			catch (Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;

			}
		}
	}