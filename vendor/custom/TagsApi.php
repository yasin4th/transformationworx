<?php
	@session_start;
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	
	//including connection.php file to make connection with database
	require_once 'Connection.php';
	
	class TagsApi extends Connection{
		
		/**
		* function fetch tag details to show the tags and their description to user on tag.php file
		*
		* @author Utkarsh Pandey
		* @date 17-05-2015
		* @param JSON
		* @return JSON status and data(tags and description)
		**/
		public function getTagsData($data) {
			
			$result = new stdClass();
			
			try{
				
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('tags');
				$select->where(array(
					'delete'	=>	0
				));
				// $select->limit($data->limit);
				// $select->offset($data->offset);
				$select->order('id DESC');
				$select->columns(array('id','name' => 'tag', 'description',));
				
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				if($res)
				$result->status = 1;
				$result->data = $res;
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
			
		}
		
		
		/**
		* function add new tag details row in tags table
		*
		* @author Utkarsh Pandey
		* @date 17-05-2015
		* @param JSON with all required details
		* @return JSON status and message
		**/
		public function addTag($data) {  
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('tags');
				$select->where(array(
					'tag'	=>	htmlentities($data->tag),
					'delete'	=>	0
				));
				$select->columns(array('id','tag', 'description',));
	
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				if(count($res) == 0) {
					$insert = new TableGateway('tags', $adapter, null, new HydratingResultSet());
					$insert->insert(array(
						'tag'	=>	htmlentities($data->tag),
						'description'	=>	htmlentities($data->description),
						'user_id'	=>	1, //$_SESSION['userId'],
						'created'	=>	time()
					));
					$id= $insert->getLastInsertValue();
					if($id) {
						$result->id = $id;
						$result->status = 1;
						$result->message = "Row Success Added";
						return $result;
					}
					else {
						$result->status = 0;
						$result->message = "Error Occured";
						return $result;
					}
				}
				else {
					$result->status = 0;
					$result->message = "Duplicate Tag entry.";
					return $result;
				}
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		
		
		/**
		* function sets a row deleted in tags table
		*
		* @author Utkarsh Pandey
		* @date 17-05-2015
		* @param JSON id
		* @return JSON status and message
		**/
		public function DeleteTagRow($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('tags');
				$update->set(array(
					'delete'	=> 1,
					'created'	=>	time()
				));
				$update->where(array(
					'id' => $data->id
				));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				$result->status = 1;
				$result->message = "Successfully Deleted.";
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		
		
		/**
		* function update tag and description in a row of tags table
		*
		* @author Utkarsh Pandey
		* @date 17-05-2015
		* @param JSON with all required details
		* @return JSON status and message
		**/
		public function updateTagRow($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);								
				$select = $sql->select();
				$select->from('tags');
				$select->where(array(
					'tag'	=>	htmlentities($data->tag),
					'delete'	=>	0
				));
				$select->columns(array('id','tag', 'description',));
	
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = array();
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				if(count($res) == 0) {

					$update = $sql->update();
					$update->table('tags');
					$update->set(array(
						'tag'	=> htmlentities($data->tag),
						'description'	=>	htmlentities($data->description),
						'created'	=>	time()
					));
					$update->where(array(
						'id' => $data->id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
					$result->status = 1;
					$result->message = "Successfully Update.";
					return $result;
				}
				else if(count($res) == 1 && $res[0]['id'] == $data->id) {
					$update = $sql->update();
					$update->table('tags');
					$update->set(array(
						'tag'	=> htmlentities($data->tag),
						'description'	=>	htmlentities($data->description),
						'created'	=>	time()
					));
					$update->where(array(
						'id' => $data->id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
					$result->status = 1;
					$result->message = "Successfully Update.";
					return $result;
					}
				else {
					$result->status = 0;
					$result->message = 'Not Updated. this Tag already created.';
					return $result;
				}
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
	}
	
?>