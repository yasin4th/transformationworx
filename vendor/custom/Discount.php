<?php
	@session_start();
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	
	//including connection.php file to make connection with database
	require_once 'Connection.php';
	
	class Discount extends Connection{
		
		/**
		* Function Institute Profile Data
		*
		* @author Utkarsh Pandey
		* @date 18-11-2015
		* @param JSON
		* @return JSON status message and data
		**/

		public function createDiscount($data) {
			
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				
				$data = array(
					'user_id'	=>	$data->userId,
					'type'	=>	$data->type,
					'code'	=>	$data->code,
					'value'	=>	$data->value,
					'title'	=>	$data->title,
					'status'	=>	$data->status,
					'description'	=>	$data->description,
					'created'	=>	date('Y-m-d H:i:s'),
					'expiry'	=>	$data->expiry,
					'maxuses'	=>  $data->maxuses
					
				);
				
				$query = new TableGateway('discount', $adapter, null, new HydratingResultSet());
				$query->insert($data);
				
				$id = $query->getLastInsertValue();
				
				
				$result->status = 1;
				$result->message = 'Discount created';				

				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function fetchDiscount($data) {
			
			$result = new stdClass();
			try {
				$adapter = $this->adapter;				
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('discount');
				//$select->order("created");
				$where = array('deleted'=>'0');
				if(isset($data->id))
				{					
					$where['id']=$data->id;
				}
				$select->where->nest->lessThan('id',3)->or->greaterThan('id',10002)->unnest;
				$select->where($where);

				$selectString = $sql->getSqlStringForSqlObject($select);				
				
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$result->total_questions = count($rows);

				if(isset($data->limit)){
					$select->offset($data->limit)->limit(50);
				}
				$select->order('id desc');
				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();				
				
				$result->result = $rows;				
				
				$result->status = 1;
				$result->message = 'Discount fetched';

				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}	

		public function updateDiscount($req) {
			
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				
				
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('discount');
				$data=array();
				if(isset($req->title)){
					$data['title'] = $req->title;
				}
				if(isset($req->code)){
					$data['code'] = $req->code;
				}
				if(isset($req->type)){
					$data['type'] = $req->type;
				}
				if(isset($req->description)){
					$data['description'] = $req->description;
				}
				if(isset($req->min)){
					$data['min_price'] = $req->min;
				}
				if(isset($req->value)){
					$data['value'] = $req->value;
				}
				if(isset($req->status)){
					$data['status'] = $req->status;
				}
				if(isset($req->expiry)){
					$data['expiry'] = $req->expiry;
				}
				if(isset($req->deleted)){
					$data['deleted'] = $req->deleted;
				}
				if(isset($req->maxuses)){
					$data['maxuses'] = $req->maxuses;
				}
				
				$update->set($data);
				$update->where(array(
				    'id' => $req->id
				));	
				
				$selectString = $sql->getSqlStringForSqlObject($update);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);				
				
				
				$result->status = 1;
				$result->message = 'Discount updated';

				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}



		public function updateInstituteProfile($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				$update = $sql->update();
				$update->table('profile');

				$update->set(array(
					'about' => $data->about,
					'website_url' => $data->url,
					'address' => $data->address
				));

				$update->where(array(
					'user_id'	=>	$data->userId
				));
				$updateString = $sql->getSqlStringForSqlObject($update);
				// die($updateString);
				$update = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				
				
				
				$result->status = 1;
				$result->message = "Profile Successfully Updated";
			
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}


		}


		/**
		* Function to change student profile pic
		*
		* @author Utkarsh Pandey
		* @date 26-07-2015
		* @param JSON of image data
		* @return JSON status and message
		**/

		public function studentChangeProfilePicture($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$orgFileName = $data->image["name"];
				$ext = pathinfo($orgFileName, PATHINFO_EXTENSION);
				$newFileName =  md5($data->userId).'.'.$ext;
				$destination = __DIR__."/../../profilePictures/".$newFileName;
				move_uploaded_file($data->image["tmp_name"], $destination);

				$sql = new Sql($adapter);
				
				$update = $sql->update();
				$update->table('profile');
				$src = "profilePictures/".$newFileName;
				$update->set(array(
					'image' => $src
				));

				$update->where(array(
					'user_id'	=>	$data->userId
				));
				
				$updateString = $sql->getSqlStringForSqlObject($update);
				$update = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				
				$result->status = 1;
				$result->message = "Avtar Updated.";
				$result->src = $src."?".time();
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function updateInstitutePassword($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'id'	=>	$data->userId
				));

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$user_data = $run->toArray();
				$user_data = $user_data[0];

				if($data->new_password == "" || $data->confirm_password == "" || $data->current_password == "")
				{
					$result->status = 2;
					$result->message = "Some fields are required";
				}
				else if($user_data['password'] != md5($data->current_password))
				{
					$result->status = 2;
					$result->message = "You have entered the wrong current password";
				}
				else if($data->new_password != $data->confirm_password)
				{
					$result->status = 2;
					$result->message = "Please enter same password both times.";
				}
				else
				{
					$update = $sql->update();
					$update->table('users');

					$update->set(array(
						'password' => md5($data->confirm_password)
					));

					$update->where(array(
						'id'	=>	$data->userId
					));
					
					$updateString = $sql->getSqlStringForSqlObject($update);
					$run = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE);

					$result->status = 1;
					$result->message = 'Password Updated';
				}
				
			
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function applyDiscount($data)
		{			

			$result = new stdClass();
			try {
				
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);				
				$select = $sql->select();				
				$select->from('test_program');
				
				$select->where->equalTo("id", $data->program)
							  ->equalTo('deleted','0')
							  ->equalTo('status','1');
				
				$selectString = $sql->getSqlStringForSqlObject($select);				
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$res = $run->toArray();

				if(count($res))
				{
					$program = $res[0];
					$data->price = $res[0]['price'];
				}

				$sql = new Sql($adapter);				
				$select = $sql->select();				
				$select->from('discount');
				$datetime = date('Y-m-d H:i:s',time());
				$select->where->equalTo('code',$data->promo)
							  ->equalTo('deleted',0)
							  ->equalTo('status',1)
							  ->greaterThan('expiry',$datetime)
							  ->lessThan('min_price',$data->price)
							  ->greaterThan('maxuses',0)
							  ->lessThan('uses',new Expression('maxuses'));

				/*$select->where("code = '$data->promo'");
				$select->where("expiry > '$datetime'");
				$select->where("min_price < $data->price");
				$select->where("deleted = '0'");
				$select->where("status = '1'");*/
				
				$selectString = $sql->getSqlStringForSqlObject($select);
						
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$count = $run->toArray();
				$amount = intval($data->price);
				if(count($count))
				{
					$type = $count[0]['type'];
					$value = intval($count[0]['value']);
					$min_price = intval($count[0]['min_price']);					
					

					if($type == 'Fixed')
					{
						$amount = $amount - $value;
					}
					elseif($type == 'Percent')
					{
						$amount = $amount - (($amount*$value)/100);						
					}
					
					if($amount < 0)
					{
						$amount = 0;
					}

					$result->id = intval($count[0]['id']);

					$result->amount = $amount;
					$result->_amount = $data->price;
					$result->status = 1;
					$result->message = "Discount value fetched.";
					return $result;
					
				}

				$result->amount = $amount;
				$result->status = 0;
				$result->message = "Invalid Promo Code.";
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		


	}