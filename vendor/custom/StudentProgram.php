<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Expression;

	//including connection.php file to make connection with database
	require_once 'Connection.php';

	class StudentProgram extends Connection {

        public function __construct() {
            $this->db = new DB();
            parent::__construct();
        }

		public function getTestprogramsAvailableForSave($data) {

			$result = new stdClass();

			try{

				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('tp' => 'test_program'))
						->join(array('c'=>'classes'),'tp.class = c.id', array('class' => 'name'),'left');
				$select->columns(array('id','name', 'price', 'description', 'image'));

				$where = new $select->where();
				$where->equalTo('tp.status', 1);
				$where->in('tp.type', array(2,3));
				$where->equalTo('tp.deleted', 0);

				$select->where($where);
				$select->order('id DESC');
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				$result->status = 1;
				$result->data = $res;

				$sql = new Sql($adapter);
				$select = $sql->select();

				$select->from(array('tp' => 'test_papers_of_test_program'))
						->join(array('type'=>'test_paper_type'),'tp.test_paper_type_id = type.id',
						 array('test_paper_type' =>'type'),'left')
						->group(array('test_program_id','test_paper_type_id'));
				$select->columns(array('count' => new Expression('count(test_paper_id)'),'test_program_id','test_paper_type_id'));
				$select->where(array('tp.deleted'=>0));


				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				$result->papers = $res;
				return $result;

			}
            catch(Exception $e )
            {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function filterPrograms($data) {

			$result = new stdClass();

			try{

				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();

				if($data->option == "2" )
				{
					$select->from(array('tp' => 'test_program'))
							->join(array('tps'=>'test_program_subjects'),'tps.test_program_id = tp.id', array(),'left');

					$select->columns(array('id','name', 'price', 'description', 'image'));
					$where=array();
					if(isset($data->keyword)){
						$select->where('tp.name LIKE \'%'.$data->keyword.'%\'');
						//$where['tp.name']=$data->keyword;
					}
					if(isset($data->class_id)){
						$where['tp.class']=$data->class_id;
					}
					if(isset($data->subject_id)){
						$where['tps.subject_id']=$data->subject_id;
					}
					if(isset($data->checkExpiry))
	                {
	                    $today = date('Y-m-d H:i:s');
	                    $select->where->greaterThan('tp.valid_till',$today);
	                }

					$where['tp.status'] = 1;
					$where['tp.deleted'] = 0;
					$select->where($where);
					$select->group('tp.id');
					$select->order('created DESC');
					$selectString = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$res = $run->toArray();
					$result->data = $res;
					$result->status = 1;
				}
				else if($data->option == "1" )
				{

					$select->from(array('tp'=>'test_program'))
							->join(array('sp'=>'student_program'),'sp.test_program_id = tp.id',array('time','count'=>new Expression('count(sp.id)')),'left');

					$select->columns(array('name', 'price', 'description', 'image', 'id'));
					$where = array();
					if(isset($data->keyword)){
						$select->where('tp.name LIKE \'%'.$data->keyword.'%\'');
					}
					if(isset($data->class_id)){
						$where['tp.class']=$data->class_id;
					}
					if(isset($data->test_program_id)){
						$where['tp.id']=$data->test_program_id;
					}
					/*if(isset($data->checkExpiry))
	                {
	                    $today = date('Y-m-d H:i:s');
	                    $select->where->greaterThan('tp.valid_till',$today);
	                }*/
					$where['tp.status'] = 1;
					$where['tp.deleted'] = 0;
					$select->where($where);
					$select->order('count DESC');
					$select->group('tp.id');
					$selectString = $sql->getSqlStringForSqlObject($select);
					// echo $selectString;
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$res = $run->toArray();
					$result->data = $res;
					$result->status = 1;

				}
				else if($data->option == "0" )
				{
					//no sorting

					$select->from(array('tp' => 'test_program'))
							->join(array('tps'=>'test_program_subjects'),'tps.test_program_id = tp.id', array(),'left');

					$select->columns(array('id','name', 'price', 'description', 'image'));
					$where=array();
					if(isset($data->keyword))
					{
						$select->where('tp.name LIKE \'%'.$data->keyword.'%\'');
					}
					if(isset($data->class_id))
					{
						$where['tp.class']=$data->class_id;
					}
					if(isset($data->subject_id))
					{
						$where['tps.subject_id']=$data->subject_id;
					}

					if(isset($data->category))
					{
						$category = implode(',', $data->category);
						$select->where("tp.category IN ($category)");
					}
					/*if(isset($data->checkExpiry))
	                {
	                    $today = date('Y-m-d H:i:s');
	                    $select->where->greaterThan('tp.valid_till',$today);
	                }*/
					$where['tp.status'] = 1;
					$where['tp.deleted'] = 0;
					$select->where($where);
					$select->order('id DESC');
					$select->group('tp.id');

					$selectString = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$res = $run->toArray();
					$result->data = $res;
					$result->status = 1;
				}


				// program paper details
				$sql = new Sql($adapter);
				$select = $sql->select();

				$select->from(array('tp' => 'test_papers_of_test_program'))
						->join(array('type'=>'test_paper_type'),'tp.test_paper_type_id = type.id',
						 array('test_paper_type' =>'type'),'left')
						->join(array('test_paper'=>'test_paper'),'test_paper.id = tp.test_paper_id',
						 array('count' => new Expression('SUM(if(test_paper.deleted=0,1,0))')),'left');
				//$where = array('test_paper.deleted',0);

                //$select->where($where);

				$select->group(array('test_program_id','test_paper_type_id'));
				$select->columns(array('test_program_id','test_paper_type_id'));


				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();


				$result->papers = $res;
				return $result;

			}
			catch(Exception $e )
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}



		public function buyTestProgram($data)
		{
			$result = new stdClass();
			try{
				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('student_program');
				$select->where(array(
					'user_id'	=> $data->userId,
					'test_program_id'	=> $data->program_id
				));
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$bought = $run->toArray();

				if (count($bought) == 0) {
					// $insert = new TableGateway('student_program', $adapter, null, new HydratingResultSet());
					// $insert->insert(array(
					// 	'user_id' => $data->userId,
					// 	'test_program_id' => $data->program_id,
					// 	'time' => time()
					// ));
					// $id = $insert->getLastInsertValue();
					// $result->status = 1;
					// $result->id = $id;
					// $result->message = "Successfully Bought";

					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('test_program')->where->equalTo('id',$data->program_id);

					$select->columns(array('id','price'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$program = $run->toArray();

					if(count($program) && $program[0]['id'] == $data->program_id)
					{
						$price = $program[0]['price'];
					}



					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('orders');

					$select->columns(array('id'=>new Expression('MAX(id)')));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$max = $run->toArray();
					$order = intval($max[0]['id']) + 1;

					$date = date("dmy");
					$order = "EXP/".$date. "/" . $order;
					$insert = new TableGateway('orders', $adapter, null, new HydratingResultSet());
					$ins = array(
						'user_id' => $data->userId,
						'program' => $data->program_id,
						'order_id' => $order,
						'price' => $price,
						'created' => date('Y-m-d H:i:s')
					);
					if($price == 0)
					{
						$ins['status'] = 1;
					}
					$insert->insert($ins);
					$id = $insert->getLastInsertValue();

					if($price == 0)
					{
						$insert = new TableGateway('student_program', $adapter, null, new HydratingResultSet());
						$insert->insert(array(
							'user_id' => $data->userId,
							'test_program_id' => $data->program_id,
							'time' => time(),
							'price'	=>	$price,
							'order_id'	=>	$id
						));
						require_once 'User.php';
						$u = new User();
						$res = $u->fetchUserDetails($data);
						$user = $res->user;

						//$program_name = $this->fetchProgramName($data->program_id);

						/*$mailTo = $user['email'];
	                    $mailSubject="Exampointer: Program Bought Successfully.";

	                    $mailBody='<p><span style="background-color:rgb(255, 255, 255); color:rgb(34, 34, 34)">
	                    Hi '.$user['first_name'].' '.$user['last_name'].' , </span><br><br>
                      	Thanks for purchasing the product(s).<br><br>
						<strong>Order Information </strong><br>
						<br>
						<strong>Order Id : '.$order.'</strong><br>
						<strong>Order Amount : Rs. '.$price.'</strong><br>
						<strong>Program Name : '.$program_name.'</strong><br><br>
						To access the program(s), please login with your credentials. You would be able to view all the products when you launch your <a href="http://exampointer.com/my-program">My Program</a>.<br><br>
						Please note: Your login credentials for the website has not changed. It is the email and password you created when you registered at our web site.<br><br>
						Regards,<br>
						User Support<br>
						<a href="http://exampointer.com">ExamPointer</a><br>

                      	</p>';*/

		                // $util = new Utility();
		                // $util->sendEmail($user['email'],$mailSubject,$mailBody);
		                // $util->sendEmail('raviarya.4thpointer@gmail.com',$mailSubject,$mailBody);

					}

					$result->order_id = $id;
					$result->price = $price;
					$result->user = $data->userId;

					$result->status = 1;
					$result->id = $id;
					$result->message = "Successfully Bought";

				}
				else
				{
					$result->status = 0;

					$result->message = "Program Already Bought";
				}

				return $result;
			}
			catch(Exception $e)
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
			}

		}

		public function buyProgram($data)
		{
			$result = new stdClass();
			try{

					$adapter = $this->adapter;


					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('student_program');
					$select->where(array(
						'user_id'	=> $data->userId,
						'test_program_id'	=> $program
					));
					$select->columns(array('id'));
					$selectString = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$bought = $run->toArray();

					if (count($bought) == 0)
					{
						$sql = new Sql($adapter);
						$update = $sql->update();
						$update->table('orders');
						$update->set(array(
							'status'	=>	1,
							'txnid'	=>	$data->txnid,
							'price'	=>	$data->amount
						));
						$update->where(array(
						    'id' => $data->order
						));

						$selectString = $sql->getSqlStringForSqlObject($update);

						$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

						$insert = new TableGateway('student_program', $adapter, null, new HydratingResultSet());
						$insert->insert(array(
							'user_id' => $data->userId,
							'test_program_id' => $program,
							'time' => time(),
							'price'	=>	$data->amount,
							'order_id'	=>	$data->order
						));
						$id = $insert->getLastInsertValue();

						require_once 'User.php';
						$u = new User();
						$res = $u->fetchUserDetails($data);
						$user = $res->user;

						$mailTo = $user['email'];
	                    $mailSubject="MyLearningGraph: Program Bought Successfully.";

	                    $mailBody='<p><span style="background-color:rgb(255, 255, 255); color:rgb(34, 34, 34)">
	                    Hi '.$user['first_name'].' '.$user['last_name'].' , </span><br><br>
                      	Thanks for purchasing the product(s).<br><br>
						<strong>Order Information </strong><br>
						<br>
						<strong>Order Id : '.$order_id.'</strong><br>
						<strong>Order Amount : Rs. '.$data->amount.'</strong><br>
						<strong>Taxation Id : '.$data->txnid.'</strong><br>
						<strong>Program Name : '.$program_name.'</strong><br><br>

						To access the program(s), please login with your credentials. You would be able to view all the products when you launch your <a href="http://exampointer.com/my-program">My Program</a>.<br><br>
						Please note: Your login credentials for the website has not changed. It is the email and password you created when you registered at our web site.<br><br>
						Regards,<br>
						User Support<br>
						<a href="http://exampointer.com">ExamPointer</a><br>
                      	</p>';

		                $util = new Utility();
		                $util->sendEmail($user['email'],$mailSubject,$mailBody);
		                $util->sendEmail('raviarya.4thpointer@gmail.com',$mailSubject,$mailBody);


						$result->program = $id;
						$result->status = 1;
						$result->message = "Program Bought Successfully.";
					}
					else
					{
						$result->status = 0;
						$result->message = "Program Already Bought.";
					}

				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function buyFailProgram($data)
		{


			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('orders' => 'orders'))
						->join(array('test_program'=>'test_program'),'test_program.id = orders.program', array('program_name' => 'name'),'left');
				$select->where(array(
					'orders.id'	=> $data->order
				));
				$select->columns(array('id','program','order_id'));
				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$order = $run->toArray();


				$order_id = $data->order;
				if(count($order))
				{
					$order_id = $order[0]['order_id'];
					$program = $order[0]['program'];
					$program_name = $order[0]['program_name'];
				}

				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('orders');
				$update->set(array(
					'status'	=>	0,
					'txnid'	=>	intval($data->txnid),
					'price'	=>	$data->amount
				));
				$update->where(array(
				    'id' => $data->order
				));

				$selectString = $sql->getSqlStringForSqlObject($update);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

				require_once 'User.php';
				$u = new User();
				$res = $u->fetchUserDetails($data);
				$user = $res->user;


				$mailTo = $user['email'];
                $mailSubject="MyLearningGraph: Product Order fail.";

                $mailBody='<p><span style="background-color:rgb(255, 255, 255); color:rgb(34, 34, 34)">
                Hi '.$user['first_name'].' '.$user['last_name'].' , </span><br><br>
              	Thank you for shopping at MyLearningGraph.<br><br>
              	There was an error processing your transaction.<br><br>
              	<strong>Order Information </strong><br>
				<br>
				<strong>Order Id : '.$order_id.'</strong><br>
				<strong>Order Amount : Rs. '.$data->amount.'</strong><br>
				<strong>Taxation Id : '.$data->txnid.'</strong><br><br>
				<strong>Program Name : '.$program_name.'</strong><br><br>

              	For security and privacy reasons, we have been unable to obtain payment authorization. It may be that your card issuer bank has declined payment.<br><br>
              	Please ensure that the details you have supplied are correct.<br><br>
				We request you to contact your issuing bank to learn more about their policies.<br><br>
				Some banks put restrictions on using credit cards for electronic or internet purchases.<br><br>
				It will be helpful to have the exact order amount and details of this purchase when you call the bank.<br><br>
				For any further assistance, please mail us at <a href="mailto:support@mylearninggraph.com">support@mylearninggraph.com</a> <br><br>

				Best Regards,<br>
				User Support<br>
				<a href="http://mylearninggraph.com">MyLearningGraph</a><br>
              	</p>';

                $util = new Utility();
                $util->sendEmail($user['email'],$mailSubject,$mailBody);
                $util->sendEmail('raviarya.4thpointer@gmail.com',$mailSubject,$mailBody);

				$result->status = 1;
				$result->message = "Transaction Failed.";

				return $result;
			}
			catch(Exception $e)
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}

		public function fetchOrders($data) {

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				//$select->from('student_program');
				$select->from(array('sp' => 'student_program'))
						->join(array('tp'=>'test_program'),'tp.id = sp.test_program_id', array('name' => 'name','price' => 'price'),'left')
						->join(array('p'=>'users'),'p.id = sp.user_id', array('first_name' => new Expression('IFNULL(first_name,"")'),'last_name' => new Expression('IFNULL(last_name,"")')),'left')
						->join(array('pro'=>'profile'),'sp.user_id = pro.user_id', array('mobile'),'left');

				$select->order('id desc');
				$select->columns(array('id', 'user_id','time' => new Expression('FROM_UNIXTIME(time,"%d-%m-%y")')));
				if (isset($data->user_id)){
					$select->where(array(
						'user_id'	=>	$data->user_id
					));
				}
				if (isset($data->order_id)){
					$order = $data->order_id;
					//preg_match_all('/[{]{2}@([a-z]+)[}]{2}(.*)[{]{2}@\/\1[}]{2}/ixms', $template, $matches);
					// preg_match_all('/\/?(.+)\/(.+)/', $order, $matches);
					// // _print_r($matches);
					// if(count($matches))
					// {
					// 	$select->where(array(
					// 		'sp.id'	=>	end($matches)
					// 	));
					// }

					$select->where(array(
						'sp.test_program_id'	=>	$data->order_id
					));
				}
				if (isset($data->to) && isset($data->from)) {
					if($data->to != '' && $data->from != ''){
						$from = strtotime($data->from);
						$to = strtotime($data->to);
						$select->where('(sp.time BETWEEN "'.$from.'" AND "'.$to.'")');
					}
				}else if(!isset($data->to) && isset($data->from)) {
					if($data->from != ''){
						$from = strtotime($data->from);
						//$to = strtotime($data->to);
						$select->where('(sp.time > "'.$from.'")');
					}
				}
				if (isset($data->price_to) && isset($data->price_from)) {
					if($data->price_to != '' && $data->price_from != ''){
						$from = $data->price_from;
						$to = $data->price_to;
						$select->where('(tp.price BETWEEN "'.$from.'" AND "'.$to.'")');
					}
				}else if(!isset($data->price_to) && isset($data->price_from)) {
					if($data->price_from != ''){
						$from = $data->price_from;
						//$to = strtotime($data->to);
						$select->where('(tp.price > "'.$from.'")');
					}
				}
				if (isset($data->name)) {
					$select->where('(first_name LIKE "%'.$data->name.'%" OR last_name LIKE "%'.$data->name.'%" )');

				}
				if (isset($data->program_name)) {
					$select->where('tp.name LIKE "%'.$data->program_name.'%"');

				}
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				$result->total_questions = count($res);

				if (isset($data->limit)) {
					$select->limit(50);
					$select->offset(intval($data->limit));
				}

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				$result->data = $res;

				$result->status = 1;
				$result->message = "Operation Successful.";

				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function fetchOrders1($data) {

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from(array('o'=>'orders'))
				// $select->from(array('sp' => 'student_program'))
						->join(array('tp'=>'test_program'),'tp.id = o.program', array('name' => 'name','_price' => 'price'),'left')
						->join(array('p'=>'users'),'p.id = o.user_id', array('first_name' => new Expression('IFNULL(p.first_name,"")'),'last_name' => new Expression('IFNULL(p.last_name,"")')),'left')
						->join(array('d'=>'discount'),'d.id = o.discount_id', array('code' => new Expression('IFNULL(code,"")')),'left')
						->join(array('pro'=>'profile'),'o.user_id = pro.user_id', array('mobile'),'left');

				$select->order('id desc');
				$select->columns(array('id', 'user_id','created','order_id' => new Expression("CONCAT('EXP/',DATE_FORMAT(o.created,'%d%m%y'),'/',o.id)"),'price','status'=>new Expression('IF(o.status=1,"SUCCESS","UNSUCCESS")')));
				if (isset($data->user_id)){
					$select->where(array(
						'user_id'	=>	$data->user_id
					));
				}
				if (isset($data->order_id)){
					$order = $data->order_id;
					//preg_match_all('/[{]{2}@([a-z]+)[}]{2}(.*)[{]{2}@\/\1[}]{2}/ixms', $template, $matches);
					// preg_match_all('/\/?(.+)\/(.+)/', $order, $matches);
					// // _print_r($matches);
					// if(count($matches))
					// {
					// 	$select->where(array(
					// 		'sp.id'	=>	end($matches)
					// 	));
					// }

					$select->where(array(
						'o.program'	=>	$data->order_id
					));
				}
				if (isset($data->to) && isset($data->from)) {
					if($data->to != '' && $data->from != ''){
						$from = strtotime($data->from);
						$to = strtotime($data->to);
						$select->where('(sp.time BETWEEN "'.$from.'" AND "'.$to.'")');
					}
				}else if(!isset($data->to) && isset($data->from)) {
					if($data->from != ''){
						$from = strtotime($data->from);
						//$to = strtotime($data->to);
						$select->where('(sp.time > "'.$from.'")');
					}
				}
				if (isset($data->price_to) && isset($data->price_from)) {
					if($data->price_to != '' && $data->price_from != ''){
						$from = $data->price_from;
						$to = $data->price_to;
						$select->where('(o.price BETWEEN "'.$from.'" AND "'.$to.'")');
					}
				}else if(!isset($data->price_to) && isset($data->price_from)) {
					if($data->price_from != ''){
						$from = $data->price_from;
						//$to = strtotime($data->to);
						$select->where('(o.price > "'.$from.'")');
					}
				}
				if (isset($data->name)) {
					$select->where('(first_name LIKE "%'.$data->name.'%" OR last_name LIKE "%'.$data->name.'%" )');

				}
				if (isset($data->program_name)) {
					$select->where('tp.name LIKE "%'.$data->program_name.'%"');

				}
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				$result->total_questions = count($res);

				if (isset($data->limit)) {
					$select->limit(50);
					$select->offset(intval($data->limit));
				}

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				$result->data = $res;

				$result->status = 1;
				$result->message = "Operation Successful.";

				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function assignPrograms($data)
		{
			$result = new stdClass();
			try{


				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('student_program');
				$select->where->equalTo('user_id', $data->userId)->in('test_program_id',$data->programs);
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$bought = $run->toArray();

				if (count($bought) == 0) {
					$query = array();
					for ($i=0; $i < count($data->programs); $i++) {

					$select = $sql->select();
					$select->from('test_program')->where->equalTo('id', $data->programs[$i]);
					$selectString = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$count = $run->toArray();
					$validity_start_from = date('d m Y');
					if(count($count) == 1)
					{
						$validity_start_from = $count[0]['validity'];
					}
					$date = date('Y-m-d H:i:s', strtotime($validity_start_from) + (61 * 24 * 60 * 60));


						$data1 = array(
							'user_id'			=>	$data->userId,
							'test_program_id'	=>	$data->programs[$i],
							'time'				=>	time(),
							'valid_till'		=>	$date,
						);

						array_push($query, $data1);
					}

					$db = new DB();
					$db->_minsert('student_program',$query);

					$result->status = 1;
					$result->message = "Successfully Assigned";
				}
				else
				{
					$result->status = 0;

					$result->message = "Already assigned programs.";
				}

				return $result;
			}
			catch(Exception $e)
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
			}

		}

		public function assignProgramsInstitute($data)
		{
			$result = new stdClass();
			try{
                $this->db->_iou('institute_program',
                    ['institute' => $data->institute, 'program' => $data->program],
                    ['institute' => $data->institute, 'program' => $data->program, 'created_at' => date('Y-m-d H:i:s')]);

				$adapter = $this->adapter;

				// getting students of a institute
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('coupons');
				$select->where->equalTo('institute_id',$data->institute)->notEqualTo('student_id','0');
				$select->columns(array('id','student_id'));
				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$students = array_column($run->toArray(),'student_id');

                if(count($students) == 0) {
                    $result->status = 1;
                    $result->message = "Successfully updated";
                    return $result;
                }

				// getting students assigned the program
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('student_program');
				$select->where->in('user_id',$students)->equalTo('test_program_id',$data->program)->notEqualTo('user_id','0');
				$select->columns(array('id','user_id'));
				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$assigned = array_column($run->toArray(),'user_id');

				if (count($students) && count(array_diff($students, $assigned)))
				{
					$students = array_values(array_diff($students, $assigned));
					$query = array();
					if(count($students))
					{
						for ($i=0; $i < count($students); $i++)
						{
							if(!in_array($students[$i], $assigned))
							{
								$data1 = array(
									'user_id'	=>	$students[$i],
									'test_program_id' => $data->program,
                                    'valid_till' => date('Y-m-d', strtotime('+3 years')),
									'time'	=>	time()
								);
								array_push($query, $data1);
							}
						}
						// $db = new DB();
						// $db->_minsert('student_program',$query);
					}

					$result->status = 1;
					$result->message = "Successfully Assigned";
				}
				else
				{
					$result->status = 1;
					$result->message = "Successfully Assigned program.";
				}

				return $result;
			}
			catch(Exception $e)
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
			}

		}
	}
?>
