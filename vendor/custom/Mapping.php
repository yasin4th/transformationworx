<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;

	//including connection.php file to make connection with database
	require_once 'Connection.php';
	class Mapping extends Connection {

		public function addMapping($data) {

			$result = new stdClass();

			try {
				$adapter = $this->adapter;
				$insertMapping = new TableGateway('mappings', $adapter, null, new HydratingResultSet());
				$insertMappingArray = array(
					'map_keyword' => $data->keyword,
					'description' => $data->description,
					'class_id' => $data->class_id,
					'user_id' => $data->userId,
					'created' => time()
				);
				$insertMapping->insert($insertMappingArray);
				$mapping_id = $insertMapping->getLastInsertValue();
				$insertTaggedItem = "INSERT INTO `tagsofmapping`(`mapping_id`, `tagged_id`, `tag_type`) VALUES ";

				// ADDDING SUBJECT IDS IN INSERT QUERY
				foreach($data->subject_ids as $id) {
					$insertTaggedItem .= "(".$mapping_id.",".$id.",1),";
				}

				// ADDDING UNIT IDS IN INSERT QUERY
				foreach($data->unit_ids as $id) {
					$insertTaggedItem .= "(".$mapping_id.",".$id.",2),";
				}

				// ADDDING CHAPTER IDS IN INSERT QUERY
				foreach($data->chapter_ids as $id) {
					$insertTaggedItem .= "(".$mapping_id.",".$id.",3),";
				}

				// ADDDING TOPIC IDS IN INSERT QUERY
				foreach($data->topic_ids as $id) {
					$insertTaggedItem .= "(".$mapping_id.",".$id.",4),";
				}

				// ADDDING TAG IDS IN INSERT QUERY
				foreach($data->tag_ids as $id) {
					$insertTaggedItem .= "(".$mapping_id.",".$id.",5),";
				}

				// ADDDING DIFFICULTY LEVEL IDS IN INSERT QUERY
				foreach($data->difficultyLevel_ids as $id) {
					$insertTaggedItem .= "(".$mapping_id.",".$id.",6),";
				}

				// ADDDING SKILL IDS IN INSERT QUERY
				foreach($data->skill_ids as $id) {
					$insertTaggedItem .= "(".$mapping_id.",".$id.",7),";
				}

				$queryForTaggedItem = rtrim($insertTaggedItem,',');
				// return $queryForTaggedItem;
				$run = $adapter->query($queryForTaggedItem, $adapter::QUERY_MODE_EXECUTE);

				if($mapping_id) {
					$result->status = 1;
					$result->message = "Mapping Successfully Added.";
				}
				else {
					$result->status = 0;
					$result->message = "ERROR: Mapping is Not Added.";
				}
				return $result;

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}

		public function updateMapping($data) {

			$result = new stdClass();

			try {
				$mapping_id = $data->map_id;

				// update mappings using by map_id

				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('mappings');
				$update->set(array(
					'map_keyword' => $data->keyword,
					'description' => $data->description,
					'class_id' => $data->class_id,
					'user_id' => $data->userId,
					'created' => time()
				));
				$update->where(array(
					'id' => $data->map_id
				));

				$statement = $sql->getSqlStringForSqlObject($update);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				// delete records using by map_id

				$delete = $sql->delete('tagsofmapping');
				$where = new $delete->where();
				$where->equalTo('mapping_id',$data->map_id);
				$delete->where($where);

				$statement = $sql->getSqlStringForSqlObject($delete);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				//insert newly updated tagsof mapping using by map_id

				$insertTaggedItem = "INSERT INTO `tagsofmapping`(`mapping_id`, `tagged_id`, `tag_type`) VALUES ";

				// ADDDING SUBJECT IDS IN INSERT QUERY
				foreach($data->subject_ids as $id) {
					$insertTaggedItem .= "(".$mapping_id.",".$id.",1),";
				}

				// ADDDING UNIT IDS IN INSERT QUERY
				foreach($data->unit_ids as $id) {
					$insertTaggedItem .= "(".$mapping_id.",".$id.",2),";
				}

				// ADDDING CHAPTER IDS IN INSERT QUERY
				foreach($data->chapter_ids as $id) {
					$insertTaggedItem .= "(".$mapping_id.",".$id.",3),";
				}

				// ADDDING TOPIC IDS IN INSERT QUERY
				foreach($data->topic_ids as $id) {
					$insertTaggedItem .= "(".$mapping_id.",".$id.",4),";
				}

				// ADDDING TAG IDS IN INSERT QUERY
				foreach($data->tag_ids as $id) {
					$insertTaggedItem .= "(".$mapping_id.",".$id.",5),";
				}

				// ADDDING DIFFICULTY LEVEL IDS IN INSERT QUERY
				foreach($data->difficultyLevel_ids as $id) {
					$insertTaggedItem .= "(".$mapping_id.",".$id.",6),";
				}

				// ADDDING SKILL IDS IN INSERT QUERY
				foreach($data->skill_ids as $id) {
					$insertTaggedItem .= "(".$mapping_id.",".$id.",7),";
				}

				$queryForTaggedItem = rtrim($insertTaggedItem,',');
				// return $queryForTaggedItem;
				$run = $adapter->query($queryForTaggedItem, $adapter::QUERY_MODE_EXECUTE);

				if($mapping_id) {
					$result->status = 1;
					$result->message = "Mapping Successfully Added.";
				}
				else {
					$result->status = 0;
					$result->message = "ERROR: Mapping is Not Added.";
				}
				return $result;

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}

		public function fetchMapKeywords($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('mappings');
				$select->columns(array('id','map_keyword'));
				$select->where(array(
					'deleted'	=>	0
				));
				$select->order('map_keyword ASC');
				$selectString = $sql->getSqlStringForSqlObject($select);
				// die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$run = $run->toArray();
				$keywords = $run;
				if(count($keywords) > 0) {
					$result->status = 1;
					$result->mapKeywords = $keywords;
				}
				$result->status = 1;
				$result->message = 'Operation Successful';
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function getQuestionDetailsUsingKey($data) {
			$result =  new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('mappings');
				$where = new $select->where();
				$where->equalTo('deleted',0);
				$where->equalTo('id',$data->key_id);
				$select->where($where);
				$select->columns(array('map_keyword', 'class_id', 'description'));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$keyword = $run->toArray();
				//die(var_dump($keyword[0]['class_id']));
				if(count($keyword) == 0)
				{
					$result->status = 2;
					$result->message = "ERROR: Keyword not found.";
					return $result;
				}

				$keywordData = $keyword[0];

				/*
				* here we will fecth all tags-items of question
				*
				*/
				// $statement = 'SELECT tagged_id, tag_type FROM tagsofquestion WHERE question_id='.$data->question_id;
				$select = $sql->select();
				$select->from('tagsofmapping');
				$where = new $select->where();
				$where->equalTo('mapping_id',$data->key_id);
				$select->where($where);
				$select->columns(array('tagged_id','tag_type'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$taggedItems = $run->toArray();


				/*
				*	Now we will fetch all tagged data
				*
				*/
				$tagged = new stdClass();
				$tagged->class	=	array();
				$tagged->subject	=	array();
				$tagged->unit	=	array();
				$tagged->chapter	=	array();
				$tagged->topic	=	array();
				$tagged->tag	=	array();
				$tagged->difficulty	=	array();
				$tagged->skill	=	array();
				foreach ($taggedItems as $data) {
					switch($data["tag_type"]) {
						case "1":
							array_push($tagged->subject,$data["tagged_id"]);
							break;

						case "2":
							array_push($tagged->unit,$data["tagged_id"]);
							break;

						case "3":
							array_push($tagged->chapter,$data["tagged_id"]);
							break;

						case "4":
							array_push($tagged->topic,$data["tagged_id"]);
							break;

						case "5":
							array_push($tagged->tag,$data["tagged_id"]);
							break;

						case "6":
							array_push($tagged->difficulty,$data["tagged_id"]);
							break;

						case "7":
							array_push($tagged->skill,$data["tagged_id"]);
							break;

						default:
							break;

					}
				}
				//zzzzzz $data->key_id
				array_push($tagged->class,$keyword[0]['class_id']);
				//die(var_dump($tagged->class));
				//die(var_dump($keyword[0]['class_id']));
				///////
				$fill = new stdClass();

				// quesry SELECT id, name FROM `classes` WHERE deleted=0
				$select = $sql->select();
				$select->from('classes');
				$where = new $select->where();
				$where->equalTo('deleted',0);
				$select->where($where);
				$select->columns(array('id','name'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$fill->classes = $run->toArray();

				// quesry SELECT id, name FROM `subjects` WHERE deleted=0 AND class_id=1
				$select = $sql->select();
				$select->from('subjects');
				$where = new $select->where();
				$where->equalTo('deleted',0);
				$where->equalTo('class_id',$keywordData["class_id"]);
				$select->where($where);
				$select->columns(array('id','name'));
				$statement = $sql->getSqlStringForSqlObject($select);
				// die($statement );
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$fill->subjects = $run->toArray();
				if(!empty($tagged->subject)) {
					// quesry SELECT id, name FROM `units` WHERE deleted=0 AND subject_id=1
					$select = $sql->select();
					$select->from('units');
					$where = new $select->where();
					$where->equalTo('deleted',0);
					$where->in('subject_id',$tagged->subject);
					$select->where($where);
					$select->columns(array('id','name'));
					$statement = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$fill->units = $run->toArray();

					if(!empty($tagged->unit)) {
						// quesry SELECT id, name FROM `chapters` WHERE deleted=0 AND unit_id=1
						$select = $sql->select();
						$select->from('chapters');
						$where = new $select->where();
						$where->equalTo('deleted',0);
						$where->in('unit_id',$tagged->unit);
						$select->where($where);
						$select->columns(array('id','name'));
						$statement = $sql->getSqlStringForSqlObject($select);

						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$fill->chapters = $run->toArray();

						if(!empty($tagged->chapter)) {
							// quesry SELECT id, name FROM `topics` WHERE deleted=0 AND chapter_id=1
							$select = $sql->select();
							$select->from('topics');
							$where = new $select->where();
							$where->equalTo('deleted',0);
							$where->in('chapter_id',$tagged->chapter);
							$select->where($where);
							$select->columns(array('id','name'));
							$statement = $sql->getSqlStringForSqlObject($select);

							$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
							$fill->topics = $run->toArray();
						}

					}

				}

				// quesry SELECT id, name FROM `tags` WHERE deleted=0
				$select = $sql->select();
				$select->from('tags');
				$where = new $select->where();
				$where->equalTo('delete',0);
				$select->where($where);
				$select->columns(array('id','tag'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$fill->tags = $run->toArray();

				// quesry SELECT id, name FROM `difficulty` WHERE deleted=0
				$select = $sql->select();
				$select->from('difficulty');
				$where = new $select->where();
				$where->equalTo('delete',0);
				$select->where($where);
				$select->columns(array('id','difficulty'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$fill->difficulty = $run->toArray();

				// quesry SELECT id, name FROM `topics` WHERE deleted=0
				$select = $sql->select();
				$select->from('topics');
				$where = new $select->where();
				$where->equalTo('deleted',0);
				$select->where($where);
				$select->columns(array('id','name'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$fill->topics = $run->toArray();

				// quesry SELECT id, name FROM `skill` WHERE deleted=0
				$select = $sql->select();
				$select->from('skills');
				$where = new $select->where();
				$where->equalTo('delete',0);
				$select->where($where);
				$select->columns(array('id','skill'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$fill->skills = $run->toArray();



				$result->keywordData = $keywordData;
				$result->tagged = $tagged;
				$result->fill = $fill;

				$result->status = 1;
				$result->message = "Operation Successful";

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function getMappingsData($data){
			$result = new stdClass();
			
			try{
				
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('mappings');
				$select->where(array(
					'deleted'	=>	0
				));
				//$select->columns(array('id','name' => 'skill', 'description',));
				$select->columns(array('id','map_keyword', 'description'));
				
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				$result->status = 1;
				$result->data = $res;
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* function soft delete a row in mappings table
		**/
		public function DeleteMappingRow($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('mappings');
				$update->set(array(
					'deleted'	=>	1,
					'created'	=>	time()
				));
				$update->where(array(
					'id' => $data->id
				));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				$result->status = 1;
				$result->message = "Successfully Deleted.";
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

	}

?>