<?php
	@session_start();
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	
	//including connection.php file to make connection with database
	require_once 'Connection.php';
	
	class InstituteProfile extends Connection{
		
		/**
		* Function Institute Profile Data
		*
		* @author Utkarsh Pandey
		* @date 18-11-2015
		* @param JSON
		* @return JSON status message and data
		**/

		public function fetchInstituteProfile($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
			
				$select = $sql->select();
				$select->from('users');
				$select->where(array(
							'id'	=>	$data->userId
				));
				$select->columns(array('id','email','role'));
				$statement = $sql->getSqlStringForSqlObject($select);
				$statement = 'SELECT users.first_name, users.last_name, users.email, users.last_login, users.role, profile.mobile, profile.interests, profile.occupation, profile.about, profile.website_url, profile.address, profile.image FROM users LEFT JOIN profile ON profile.user_id = users.id WHERE users.id = '.$data->userId;
				$temp = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				
				if(count($temp)) {

					$result->user = $temp[0];
					$result->status = 1;
					$result->message = "Successfully Fetched.";

				}
				else {
					$result->status = 0;
					$result->message = "Your Id not found.";
				}
				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}	



		public function updateInstituteProfile($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				$update = $sql->update();
				$update->table('profile');

				$update->set(array(
					'about' => $data->about,
					'website_url' => $data->url,
					'address' => $data->address
				));

				$update->where(array(
					'user_id'	=>	$data->userId
				));
				$updateString = $sql->getSqlStringForSqlObject($update);
				// die($updateString);
				$update = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				
				
				
				$result->status = 1;
				$result->message = "Profile Successfully Updated";
			
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}


		}


		/**
		* Function to change student profile pic
		*
		* @author Utkarsh Pandey
		* @date 26-07-2015
		* @param JSON of image data
		* @return JSON status and message
		**/

		public function studentChangeProfilePicture($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$orgFileName = $data->image["name"];
				$ext = pathinfo($orgFileName, PATHINFO_EXTENSION);
				$newFileName =  md5($data->userId).'.'.$ext;
				$destination = __DIR__."/../../profilePictures/".$newFileName;
				move_uploaded_file($data->image["tmp_name"], $destination);

				$sql = new Sql($adapter);
				
				$update = $sql->update();
				$update->table('profile');
				$src = "profilePictures/".$newFileName;
				$update->set(array(
					'image' => $src
				));

				$update->where(array(
					'user_id'	=>	$data->userId
				));
				
				$updateString = $sql->getSqlStringForSqlObject($update);
				$update = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				
				$result->status = 1;
				$result->message = "Avtar Updated.";
				$result->src = $src."?".time();
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function updateInstitutePassword($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'id'	=>	$data->userId
				));

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$user_data = $run->toArray();
				$user_data = $user_data[0];

				if($data->new_password == "" || $data->confirm_password == "" || $data->current_password == "")
				{
					$result->status = 2;
					$result->message = "Some fields are required";
				}
				else if($user_data['password'] != md5($data->current_password))
				{
					$result->status = 2;
					$result->message = "You have entered the wrong current password";
				}
				else if($data->new_password != $data->confirm_password)
				{
					$result->status = 2;
					$result->message = "Please enter same password both times.";
				}
				else
				{
					$update = $sql->update();
					$update->table('users');

					$update->set(array(
						'password' => md5($data->confirm_password)
					));

					$update->where(array(
						'id'	=>	$data->userId
					));
					
					$updateString = $sql->getSqlStringForSqlObject($update);
					$run = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE);

					$result->status = 1;
					$result->message = 'Password Updated';
				}
				
			
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}


		}


	}