<?php

	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Expression;

	require_once 'Connection.php';

	class Payment extends Connection
	{

		public function __construct()
		{
			$this->db = new DB();
		}
		public function buyProgram($req)
		{
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$data = array(
					'user_id'   => $req->userId,
					'program' 	=> $req->productinfo,
					'price' 	=> $req->amount,
					'created'	=> date('Y-m-d H:i:s'),
					'status' 	=> ($req->status == 'success')?1:0,
					'txnid'  	=> $req->txnid,
					'mihpayid'  => $req->mihpayid,
					// 'bank_ref_num'=> $req->bank_ref_num,
					'mode'      => $req->mode,
					'payumoneyid'=> $req->payumoneyid,
					'discount_id'=> $req->param2,
				);
				// $order = $this->db->_insert('orders',$data);
				$order = $this->db->_iou('orders', array('txnid' => $req->txnid), $data);


				if($req->status == 'success')
				{
					parent::__construct();
					$adapter = $this->adapter;

					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('test_program');
					$select->where->equalTo('id',$req->productinfo);

					$selectString = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$count = $run->toArray();

					$validity_start_from = date('d m Y');
					if(count($count) == 1)
					{
						$validity_start_from = $count[0]['validity'];
					}
					$date = date('Y-m-d H:i:s', strtotime($validity_start_from) + (61 * 24 * 60 * 60));

					$data1 = array(
						'user_id'			=>	$req->userId,
						'test_program_id'	=>	$req->productinfo,
						'order_id'			=>	$order,
						'time'				=>	time(),
						'valid_till'		=>	$date,
						'price'				=>	$req->amount,
					);

					$order1 = $this->db->_iou('student_program', array('user_id'=>$req->userId, 'test_program_id'=>$req->productinfo), $data1);
					$adapter = $this->adapter;
					if(isset($req->param2) && $req->param2 != '0')
					{
						$sql = new Sql($adapter);
						$update = $sql->update();
						$update->table('discount');
						$update->set(array(
							'uses'	=>	new Expression('uses+1')
						));
						$update->where(array(
						    'id' => $req->param2
						));

						$selectString = $sql->getSqlStringForSqlObject($update);
						$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					}

					$result->status = 1;
					$result->message = "Payment Success.";
				}
				else
				{
					$result->message = "Payment Unsuccessful.";
				}
				$result->status = 1;
				$result->order = $order;

				return $result;
			}
			catch(Exception $e){
				$result->status = 0;
				$result->message = $e->getMessage();

				return $result;
			}
		}

		public function ifBought($req)
		{

			$result = new stdClass();
			try {

				$adapter = DB::$adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('student_program');
				$select->where(array(
					'user_id'	=> $req->userId,
					'test_program_id'	=> $req->program_id
				));
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$bought = $run->toArray();

				if(count($bought) == 0)
				{
					$result->status = 3;
					$result->message = "Not Bought";
				}
				else
				{
					$result->status = 1;
					$result->message = "Already Bought";
				}

				return $result;
			}
			catch(Exception $e){
				$result->status = 0;
				$result->message = $e->getMessage();

				return $result;
			}
		}

	}