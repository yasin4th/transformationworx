<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;
		
	require_once 'Connection.php';
	include 'PHPExcel/Classes/PHPExcel/IOFactory.php';	
	
	class JSONImport extends Connection
	{					
		public function jsonImport1($req) 
		{
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$filename	=	$req->file['name'];
				$filetype	=	$req->file['type'];
				
				$excel = $filename;
				
				if (move_uploaded_file($req->file['tmp_name'], "../assets/qimages/".$excel))
				{
					$date = date('Y-m-d H:i:s');
					$string = file_get_contents("../assets/qimages/".$excel);

					$json_a = json_decode($string, true);

					pp($json_a['ItemOptionModel']);
					die();
					/*foreach ($json_a as $person_name => $person_a) {
					   
					}*/

					$objPHPExcel = PHPExcel_IOFactory::load("../assets/qimages/".$excel);

					// $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
					$objWorksheet = $objPHPExcel->getActiveSheet();
					
					$min = $this->getMaxId();
					
					$counter = $min;
					$q = array();
					
					$struct = array(
						'options'=>array(),
						'correct'=>array(),
						'type'=>array(),
						'question'=>array(),
						'passage'=>array(),
						'description'=>array()
					);
					//_print_r($objWorksheet->getRowIterator());
					$Q = array();
					$parent = 0;
					foreach ($objWorksheet->getRowIterator() as $key => $row) 
					{	
						$question = '';		
						$description = '';		
						$options = array();	
						$correct = array();
						$type = 1;
						$cellIterator = $row->getCellIterator();
						$cellIterator->setIterateOnlyExistingCells(false);
						if($key == 1)
						{
							foreach ($cellIterator as $cell)
							{
								if(trim(strtolower($cell->getValue())) == 'option')
								{
									array_push($struct['options'], $cell->getColumn());
								}
								if(trim(strtolower($cell->getValue())) == 'correct')
								{
									array_push($struct['correct'], $cell->getColumn());
								}
								if(trim(strtolower($cell->getValue())) == 'question')
								{
									array_push($struct['question'], $cell->getColumn());
								}
								if(trim(strtolower($cell->getValue())) == 'passage')
								{
									array_push($struct['passage'], $cell->getColumn());
								}
								if(trim(strtolower($cell->getValue())) == 'type')
								{
									array_push($struct['type'], $cell->getColumn());
								}
								if(trim(strtolower($cell->getValue())) == 'description')
								{
									array_push($struct['description'], $cell->getColumn());
								}
							}
						}
						else
						{							
							foreach ($cellIterator as $cell)
							{
								if(in_array($cell->getColumn(), $struct['options']) && trim($cell->__toString()) != '')
								{
									$option = trim($cell->__toString());
									$option = addslashes($option);

									array_push($options, $option);
								}
								if(in_array($cell->getColumn(), $struct['correct']) )
								{									
									$correct = explode(',', trim($cell->__toString()));
								}
								if(in_array($cell->getColumn(), $struct['question']) )
								{
									$question = addslashes(trim($cell->__toString()));
									
								}
								if(in_array($cell->getColumn(), $struct['type']) && trim($cell->__toString()) != '')
								{
									switch(strtolower(trim( $cell->__toString() )))
									{
										case 'scq':
											$type = 1;
											break;
										case 'mcq':
											$type = 2;
											break;
										case 'ord':
											$type = 4;
											break;
										case 'fb':
											$type = 6;
											break;
										case 'dd':
											$type = 7;
											break;
										case 'dnd':
											$type = 8;
											break;
										case 'integer':
											$type = 13;
											break;
										default:
											$type = 1;
											break;
									}
								}
								if(in_array($cell->getColumn(), $struct['description']) )
								{
									$description = addslashes(trim($cell->__toString()));
								}
								if(in_array($cell->getColumn(), $struct['passage']) && trim($cell->__toString()) != '' )
								{
									$passage = addslashes(trim($cell->__toString()));
									array_push($Q, new Questions($counter, $passage, array(), array(), '', 11));
									$parent = $counter;
									$counter++;
								}								
								if(in_array($cell->getColumn(), $struct['passage']) && !$cell->isInMergeRange() )
								{
									$parent = 0;
								}								
							}
						}
						if($key != 1 && $question != '')
						{
							array_push($Q, new Questions($counter, $question, $options, $correct, $description, $type, $parent));
							$counter++;
						}
					}										
				}
				
				// $Q -> all questions
				if(count($Q))
				{
					$string = "INSERT INTO questions (id,class_id,question,description,type,parent_id,user_id,created) VALUES ";
					$stringOptions = "INSERT INTO options (`option`,`question_id`,`user_id`,`created`,`answer`,`number`) VALUES ";
					$stringSubjects = "INSERT INTO `tagsofquestion`(`question_id`, `tagged_id`, `tag_type`) VALUES ";
					$stringUnits = "INSERT INTO `tagsofquestion`(`question_id`, `tagged_id`, `tag_type`) VALUES ";
					$stringChapters = "INSERT INTO `tagsofquestion`(`question_id`, `tagged_id`, `tag_type`) VALUES ";
					$stringTopics = "INSERT INTO `tagsofquestion`(`question_id`, `tagged_id`, `tag_type`) VALUES ";
					$stringTags = "INSERT INTO `tagsofquestion`(`question_id`, `tagged_id`, `tag_type`) VALUES ";
					$stringDifficultys = "INSERT INTO `tagsofquestion`(`question_id`, `tagged_id`, `tag_type`) VALUES ";
					$stringSkills = "INSERT INTO `tagsofquestion`(`question_id`, `tagged_id`, `tag_type`) VALUES ";
					

					foreach ($Q as $k => $question)
					{
						$string .= $question->getQuery($req->class_id);
						$stringOptions .= $question->getQueryOptions();
						
						foreach($req->subject_ids as $id) {						
							$stringSubjects .= $question->getQuerySubjects($id);
						}
						foreach($req->unit_ids as $id) {						
							$stringUnits .= $question->getQueryUnits($id);
						}
						foreach($req->chapter_ids as $id) {						
							$stringChapters .= $question->getQueryChapters($id);
						}
						foreach($req->topic_ids as $id) {						
							$stringTopics .= $question->getQueryTopics($id);
						}
						foreach($req->tag_ids as $id) {					
							$stringTags .= $question->getQueryTags($id);
						}
						foreach($req->difficultyLevel_ids as $id) {						
							$stringDifficultys .= $question->getQueryDifficultys($id);
						}
						foreach($req->skills_ids as $id) {						
							$stringSkills .= $question->getQuerySkills($id);
						}

					}
					$string = rtrim($string, ",");
					$stringOptions = rtrim($stringOptions, ",");
					$stringSubjects = rtrim($stringSubjects, ",");
					$stringUnits = rtrim($stringUnits, ",");
					$stringChapters = rtrim($stringChapters, ",");
					$stringTopics = rtrim($stringTopics, ",");
					$stringTags = rtrim($stringTags, ",");
					$stringDifficultys = rtrim($stringDifficultys, ",");
					$stringSkills = rtrim($stringSkills, ",");
					
									
					$run = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
					$run = $adapter->query($stringOptions, $adapter::QUERY_MODE_EXECUTE);
					$run = $adapter->query($stringSubjects, $adapter::QUERY_MODE_EXECUTE);
					if(count($req->unit_ids))
					{
						$run = $adapter->query($stringUnits, $adapter::QUERY_MODE_EXECUTE);
					}
					if(count($req->chapter_ids))
					{
						$run = $adapter->query($stringChapters, $adapter::QUERY_MODE_EXECUTE);
					}
					if(count($req->topic_ids))
					{
						$run = $adapter->query($stringTopics, $adapter::QUERY_MODE_EXECUTE);
					}
					if(count($req->tag_ids))
					{
						$run = $adapter->query($stringTags, $adapter::QUERY_MODE_EXECUTE);
					}
					if(count($req->difficultyLevel_ids))
					{
						$run = $adapter->query($stringDifficultys, $adapter::QUERY_MODE_EXECUTE);
					}
					if(count($req->skills_ids))
					{
						$run = $adapter->query($stringSkills, $adapter::QUERY_MODE_EXECUTE);
					}

				}

				$result->status = 1;
				$result->message = "Excel Import Successful.";
				
				

				
				return $result;
			}
			catch(Exception $e){
				$result->status = 0;
				$result->message = $e->getMessage();

				
				return $result;
			}
		}

		public function getMaxId()
		{
			try
			{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);							
				$select = $sql->select();							
				$select->from('questions');							
				$select->columns(array('max' => new Expression('MAX(id)')));
				$selectString = $sql->getSqlStringForSqlObject($select);				
				
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$count = $run->toArray();
				if(count($count))
				{
					return (intval($count[0]['max']) + 1);
				}
				return 0;
			}
			catch(Exception $e)
			{

			}
		}
	}

	class Questions
	{
		protected $id;
		protected $question;
		protected $options;
		protected $type;
		protected $parent;
		protected $description;

		public function __construct($id,$question,$options,$correct,$description,$type,$parent = 0)
		{					
			$this->id = $id;
			$this->question = $question;			
			$this->correct = $correct;
			$this->description = $description;
			$this->parent = $parent;
			$this->type = $type;
			$this->options = array();
			$this->generateOptions($options);
		}

		public function generateOptions($options)
		{
			$count = 1;
			for ($i=0; $i < count($options); $i++) 
			{ 
				if($this->type == 4)
				{
					// ord type = 4   =>  answer is always 1
					array_push($this->options, new Options($this->id,$options[$i],1));
				}
				else if($this->type == 6)
				{
					// fill in the blank type = 6   =>  answer is always 1  => replace with input box
					$this->question = preg_replace('/_{4,}/', '<input type="text">', $this->question);
					array_push($this->options, new Options($this->id,$options[$i],1));					
				}
				else if($this->type == 7)
				{
					// fill in the blank [drop down] type = 7   =>  number is always 1
					$this->question = preg_replace('/_{4,}/', '<select data-id="dd1" disabled="true" style="width:10%"></select>', $this->question);
					if(in_array($i+1, $this->correct))
					{
						array_push($this->options, new Options($this->id,$options[$i],1,1));
					}
					else
					{
						array_push($this->options, new Options($this->id,$options[$i],0,1));
					}
				}
				else if($this->type == 8)
				{
					// fill in the blank [drag n drop] type = 8   =>  number is always 1
					$this->question = preg_replace('/_{4,}/', '<input class="input-droppable" data-id="input2" disabled="true" size="8" />', $this->question);
					if(in_array($i+1, $this->correct))
					{
						array_push($this->options, new Options($this->id,$options[$i],1,1));
					}
					else
					{
						array_push($this->options, new Options($this->id,$options[$i],0,1));
					}
				}
				else if($this->type == 13)
				{
					// fill in the blank type = 13   =>  answer is always 1
					// number => 1,2
									
					array_push($this->options, new Options($this->id,$options[$i],1,$count));
					$count++;
				}
				else
				{
					if(in_array($i+1, $this->correct))
					{
						array_push($this->options, new Options($this->id,$options[$i],1));
					}
					else
					{
						array_push($this->options, new Options($this->id,$options[$i],0));
					}					
				}
			}
		}	

		public function getQuery($cls)
		{
			$time = time();
			$question = trim($this->question);
			$description = trim($this->description);
			$str = "('$this->id','$cls','<p>$question</p>','<p>$description</p>','$this->type','$this->parent','1','$time') ,";
			return $str;
		}

		public function getQueryOptions()
		{
			$time = time();
			$str = "";
			foreach ($this->options as $option) {
				$str .= $option->getQuery();
			}
			return $str;
		}

		public function getQuerySubjects($sub)
		{		
			
			$str = "('$this->id','$sub','1') ,";
			return $str;
		}

		public function getQueryUnits($sub)
		{		
			
			$str = "('$this->id','$sub','2') ,";
			return $str;
		}

		public function getQueryChapters($sub)
		{		
			
			$str = "('$this->id','$sub','3') ,";
			return $str;
		}

		public function getQueryTopics($sub)
		{		
			
			$str = "('$this->id','$sub','4') ,";
			return $str;
		}

		public function getQueryTags($sub)
		{		
			
			$str = "('$this->id','$sub','5') ,";
			return $str;
		}

		public function getQueryDifficultys($sub)
		{		
			
			$str = "('$this->id','$sub','6') ,";
			return $str;
		}

		public function getQuerySkills($sub)
		{		
			
			$str = "('$this->id','$sub','7') ,";
			return $str;
		}

	}

	class Options
	{
		protected $title;
		protected $answer;
		protected $number;
		protected $question;

		public function Options($id,$text,$answer,$number = 0)
		{
			$this->title = $text;
			$this->question = $id;
			$this->answer = $answer;
			$this->number = $number;
		}

		public function getQuery()
		{
			$time = time();
			$title = trim($this->title);
			$str = "('$title','$this->question','1','$time','$this->answer','$this->number') ,";
			return $str;
		}
	}