<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\Sql\Sql\Expression;
	use Zend\Db\ResultSet\HydratingResultSet;

	//including connection.php file to make connection with database
	require_once 'Connection.php';
	require_once('Question.php');
	//Include composer autoload
	//Require Operation
	require_once __dir__.'/Operation.php';
	include 'autoload.php';

	
	class PDF extends Connection {
		/**
		* this function fetch all classes
		*
		* @author Utkarsh Pandey
		* @date 01-06-2015
		* @param JSON with all required details
		* @return JSON with all details
		* @return JSON with all classesDetails, status and message
		**/
		public function pdfUpload($file) {
			$result = new stdClass();
			try {
				

				$filename	=	$file->file['name'];
				$filetype	=	$file->file['type'];
				if($filetype == "application/pdf") {
					$name_of_pdf = "document.pdf";
					// var_dump($file);
					move_uploaded_file($file->file['tmp_name'], $name_of_pdf);
					//New object for Smalot Library
					$parser = new \Smalot\PdfParser\Parser();

					//Parsing PDF and storing it in a string
					$pdf_text = $parser->parseFile($name_of_pdf);
					// var_dump($pdf_text);
					
					//Extract extra details of PDF from String
					$details= $pdf_text->getDetails();
					// var_dump($details);
					
					//Explode at Question to separate them all
					$questions_array = explode("Question", $pdf_text->getText());
					

						//Strip the first Space
						unset($questions_array[0]);
					// print_r($questions_array);
					// die("asd");	

					// creating object to add question
					$q = new Question();
					
					//Operate on each question individually
					foreach ($questions_array as $question) {
						// print_r($question);
						
						//Here, we have recieved the whole Questions Array, now we will operate
						//on it Question by Question

						// foreach ($questions_array as $question) {
							// print_r($question);
							// die("asd");	
							//Function for finding number of Options
							$number_of_options= Operation::calculate_number_of_options($question);
							//Function for finding out the correct Option
							$correct_option = $this->characterNumber(Operation::guess_correct_option($question));
							//die();//Function for extracting the Question Part
							$question_section= Operation::extract_question($question, $correct_option);
							//Function that returns Options of that Question as an Associative array
							$options_array= Operation::extract_options($question, $question_section, $correct_option, $number_of_options);
							//Function that strips the Star (*) symbol from correct option
							$options= Operation::strip_star_symbol($options_array, $number_of_options);
							// seting the correct option
							
							$options[$correct_option]->answer = 1;
							
							//creating object for add question
							$obj = new stdClass();
							$obj->questionData = $question_section;
							$obj->DescriptionData = "No Description.";
							$obj->questionType = 1;
							$obj->class_id = $file->class_id;
							$obj->options = $options;
							$obj->subject_ids = $file->subject_ids;
							$obj->unit_ids = $file->unit_ids;
							$obj->chapter_ids = $file->chapter_ids;
							$obj->topic_ids = $file->topic_ids;
							$obj->tag_ids = $file->tag_ids;
							$obj->difficultyLevel_ids = $file->difficultyLevel_ids;
							$obj->skill_ids = $file->skill_ids;
							
							// print_r($options);
							// die();
							$result = $q->addQuestion($obj);
							
						}


						return $result;
					}
					else {
						return "not pdf";
					}
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* this function change option index character to number 
		*
		* @author Utkarsh Pandey
		* @date 17-07-2015
		* @param character
		* @return integer value
		**/
		public function characterNumber($index) {
			$result = new stdClass();
			try {
				$change = array('a' => 1,'b' => 2,'c' => 3,'d' => 4,'e' => 5,'f' => 6,'g' => 7,'h' => 8,'i' => 9,'j' => 10);
				$index = strtolower($index);
				$number = $change[$index];
				return $number;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		
	}
	
?>
