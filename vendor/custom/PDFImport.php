<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;
	

	
	require_once 'Connection.php';

	/*--------------------------------------------------------------------------
	 *
	 *--------------------------------------------------------------------------
	 *
	 * @author Ravi Arya
	 * @date 13-April-2016
	 * @param params
	 * @return returns
	 * @todo todo
	 *
	 *--------------------------------------------------------------------------
	 */
	
	class PDFImport extends Connection	{
					
		public function pdfUpload($req) {
			
			$result = new stdClass();
			$result->data = $req;
			$result->uploadstart = date('Y-m-d H:i:s');
			try {
				$adapter = $this->adapter;
				$filename	=	$req->file['name'];
				$filetype	=	$req->file['type'];
				if($filetype == "application/pdf")
				{
					$pdf = "input-pdf.pdf";
					// var_dump($file);
					if (move_uploaded_file($req->file['tmp_name'], "../assets/qimages/".$pdf))
					{
						$date = date('Y-m-d-H-i-s');
						$command = escapeshellcmd("java -jar ../assets/qimages/jar1.8.jar ExtractText -encoding UTF-8 ../assets/qimages/input-pdf.pdf ../assets/qimages/output-text.txt");
						exec($command, $output, $exit_code);
						
						$command = escapeshellcmd("java -jar ../assets/qimages/jar1.8.jar ExtractImages -prefix ../assets/qimages/$date ../assets/qimages/input-pdf.pdf");
						exec($command, $output, $exit_code);

						$result->uploadend = date('Y-m-d H:i:s');

						$qimages = array();
						foreach ($output as $key => $value)
						{
							$a = substr($value, strlen("Writing image:../"));							
							$link = glob ("../$a.*");
							array_push($qimages,substr($link[0],3));
						}
						$outputtext = file_get_contents("../assets/qimages/output-text.txt");

						//dd($outputtext);
						$outputtext = preg_replace('/_{4,}/', '<input type="text">', $outputtext);

						$tagimages = array();						
						foreach ($qimages as $key => $value)
						{
							array_push($tagimages, "<img src=http://4thpointer.in/elearning/$value />");		
							// array_push($tagimages, "<img src=http://localhost/test_engine_new/$value />");		
						}
						
						preg_match_all('/<image>/', $outputtext, $matches);
						
						$match = $matches[0];
						if(count($match) == count($tagimages))
						{
							foreach ($match as $key => $value)
							{
								$pos = strpos($outputtext, '<image>');
								$outputtext = substr_replace($outputtext, $tagimages[$key], $pos, 7);
							}
							$questions = preg_split('/^[\s\n]*Question\s*\d{0,}\#/im', $outputtext);
							unset($questions[0]);
							
							$min = $this->getMaxId();
							$counter = $min;
							$q = array();
							foreach ($questions as $key => $value) {
								if(preg_match('/subquestions\s*\#passage\#/i', $value))
								{									
									$subquestions = preg_split('/subquestions\s*\#passage\#/i', $value);
									if(count($subquestions) == 2)
									{
										array_push($q,new Questions($subquestions[0],$counter));							
										$parent = $counter;
										$counter++;
										$sub = preg_split('/subquestion\s*\d{0,}\s*\#/i', $subquestions[1]);
										unset($sub[0]);
										foreach ($sub as $k => $s)
										{											
											array_push($q,new Questions($s,$counter,$parent));
											$counter++;
										}									
									}									
								}
								else
								{									
									array_push($q,new Questions($value,$counter));
									$counter++;

								}
							}

							$result->parsedend = date('Y-m-d H:i:s');

							if(count($q))
							{
								$string = "INSERT INTO questions (id,class_id,question,description,type,parent_id,user_id,created) VALUES ";
								$stringOptions = "INSERT INTO options (`option`,`question_id`,`user_id`,`created`,`answer`) VALUES ";
								$stringSubjects = "INSERT INTO `tagsofquestion`(`question_id`, `tagged_id`, `tag_type`) VALUES ";
								foreach ($q as $k => $question)
								{
									$string .= $question->getQuery($req->class_id);
									$stringOptions .= $question->getQueryOptions();
									foreach($req->subject_ids as $id)
									{						
										$stringSubjects .= $question->getQuerySubjects($id);
									}
								}
								$string = rtrim($string, ",");
								$stringOptions = rtrim($stringOptions, ",");
								$stringSubjects = rtrim($stringSubjects, ",");
								
								$run = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
								$run = $adapter->query($stringOptions, $adapter::QUERY_MODE_EXECUTE);
								$run = $adapter->query($stringSubjects, $adapter::QUERY_MODE_EXECUTE);

							}
							$result->queryend = date('Y-m-d H:i:s');


							$result->status = 1;
							$result->message = "Uploaded pdf.";
						}
						else
						{
							$result->match = count($match);
							$result->matchimages = count($tagimages);
							$result->status = 0;
							$result->message = "Incorrect tags.";
						}
					}				
				}
				else
				{
					$result->status = 0;
					$result->message = "Incorrect file format.";
				}
				//_print_r($result);
				return $result;
			}
			catch(Exception $e){
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result; 
			}
		}

		public function getMaxId()
		{
			try
			{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);							
				$select = $sql->select();							
				$select->from('questions');							
				$select->columns(array('max' => new Expression('MAX(id)')));
				$selectString = $sql->getSqlStringForSqlObject($select);				
				
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$count = $run->toArray();
				if(count($count))
				{
					return (intval($count[0]['max']) + 1);
				}
				return 0;
			}
			catch(Exception $e)
			{

			}
		}
	}

	class Questions
	{
		protected $id;
		protected $text;
		protected $question;
		protected $options;
		protected $type;
		protected $parent;
		protected $description;

		public function __construct($data, $id, $parent = 0)
		{
			$this->text = $data;			
			$this->id = $id;
			$this->parent = $parent;
			$this->description = "";
			$this->options = array();
			$this->getType();
			$this->parseText();

		}

		public function parseText()
		{
			// question
			$matches = preg_split('/(Options\s*\#\w*\#|SubQuestions\s*\#\w*\#)/i', $this->text);
			if(isset($matches[0]))
			{
				$this->question = $matches[0];
			}

			// description
			if($this->type != 11 && preg_match('/Solution\s*\#/i', $this->text)){
				$solution = preg_split('/Solution\s*\#/imsx', $this->text);	
				if(isset($solution[1]))
				{
					$this->description = $solution[1];
				}
			}

			if($this->type != 11 && preg_match('/Options\s*\#\w*\#/i', $this->text))
			{				
				if(preg_match('/Solution\s*\#/i', $this->text))
				{
					preg_match_all('/.*Options\s*\#[a-zA-Z0-9_]+\#(.+)Solution\s*\#/ismx', $this->text, $options);
				}
				else
				{
					preg_match_all('/.*Options\s*\#[a-zA-Z0-9_]+\#(.+)/ismx', $this->text, $options);
				}
				if(isset($options[1]))
				{
					$opts = $options[1];
					
					$op1 = preg_match_all('/^\*\w\#(.*)/im', trim($opts[0]),$correct);
					unset($correct[0]);
					$op1 = preg_match_all('/^[^*]?\w\#(.*)/im', trim($opts[0]),$incorrect);
					unset($incorrect[0]);

					// _print_r($correct);
					// _print_r($incorrect);

					foreach ($correct[1] as $key => $value) {
						array_push($this->options,new Options($value,$this->id,1));
					}
					foreach ($incorrect[1] as $key => $value) {
						array_push($this->options,new Options($value,$this->id,0));
					}
					if(count($correct[1]) > 1 && $this->type == 1)
					{
						$this->type = 2;
					}
				}
			}			
		}

		public function getType()
		{
			if(preg_match('/Options #scq#/', $this->text)) {
				$this->type = "1";
			}
			elseif(preg_match('/Options #mcq#/', $this->text)) {
				$this->type = "2";
			}
			elseif(preg_match('/Options #ord#/', $this->text)) {
				$this->type = "4";
			}
			elseif(preg_match('/Options #matching#/', $this->text)) {
				$this->type = "5";
			}
			elseif(preg_match('/Options #fb#/', $this->text)) {
				$this->type = "6";
			}
			elseif(preg_match('/Options #integer#/', $this->text)) {
				$this->type = "13";
			}
			else {
				$this->type = "11";
				// die('Unknown Question Type: '.var_dump($this->text));
			}		
		}

		public function getQuery($cls)
		{
			$time = time();
			$question = addslashes(trim($this->question));
			$description = addslashes(trim($this->description));
			$str = "('$this->id','$cls','<p>$question</p>','<p>$description</p>','$this->type','$this->parent','1','$time') ,";
			return $str;
		}

		public function getQueryOptions()
		{
			$time = time();
			$str = "";
			foreach ($this->options as $option) {
				$str .= $option->getQuery();
			}
			return $str;
		}

		public function getQuerySubjects($sub)
		{		
			
			$str = "('$this->id','$sub','1') ,";
			return $str;
		}

	}

	class Options
	{
		protected $title;
		protected $answer;
		protected $number;
		protected $question;

		public function Options($text,$id,$answer)
		{
			$this->title = addslashes($text);
			$this->question = $id;
			$this->answer = $answer;
			$this->number = 0;
		}

		public function getQuery()
		{
			$time = time();
			
			$title = trim($this->title);
			$str = "('$title','$this->question','1','$time','$this->answer') ,";
			return $str;
		}
	}