<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	
	//including connection.php file to make connection with database
	require_once 'Connection.php';
		
	class Store extends Connection 
	{
		public function store($data)
		{
			$result = new stdclass();
			$adapter = $this->adapter;
			
			$query = new TableGateway('store', $adapter, null, new HydratingResultSet());
			$query->insert(array('cooks'=>$data->cooks));			
			
			return $result;
		}
	}
?>