<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;

	//including connection.php file to make connection with database
	require_once 'Connection.php';
	
	class TreeApi extends Connection {
		/**
		* this function adds new class row 
		*
		* @author Utkarsh Pandey
		* @date 25-05-2015
		* @param JSON with all required details
		* @return JSON with name and description
		* @return JSON with id, status and message
		**/
		public function addNewClass($data) {
			$result = new stdClass();
			
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('classes');
				$select->where(array(
					'deleted' => 0,
					'name' => $data->name
				));
				// $select->columns(array('id'));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exist = $run->toArray();
				if(count($exist) < 1) {
					$insert = new TableGateway('classes', $adapter, null, new HydratingResultSet());
					$insert->insert(array(
						'name'	=>	htmlentities($data->name),
						'description'	=>	htmlentities($data->description),
						'user_id'	=>	1, //$_SESSION['userId'],
						'created'	=>	time()
					));
					
					$id = $insert->getLastInsertValue();
					if($id > 0) {
						$result->id = $id;
						$result->status = 1;
						$result->message = "Class Successfully Added";
					}
				}
				else {
					$result->status = 0;
					$result->message = "Class Name already exist.";
				}
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		
		
		/**
		* this function adds new subject row 
		*
		* @author Utkarsh Pandey
		* @date 25-05-2015
		* @param JSON with all required details
		* @return JSON with name and description
		* @return JSON with id, status and message
		**/
		public function addNewSubject($data) {
			$result = new stdClass();
			
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subjects');
				$select->where(array(
					'deleted' => 0,
					'name' => $data->name,
					'class_id' => $data->p_id
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exist = $run->toArray();
				if(count($exist) < 1) {
					$insert = new TableGateway('subjects', $adapter, null, new HydratingResultSet());
					$insert->insert(array(
						'name'	=>	htmlentities($data->name),
						'description'	=>	htmlentities($data->description),
						'class_id'	=>	$data->p_id,
						'user_id'	=>	1, //$_SESSION['userId'],
						'created'	=>	time()
					));
					$id= $insert->getLastInsertValue();
					if($id > 0) {
						$result->id = $id;
						$result->status = 1;
						$result->message = "Subject Successfully Added";
					}
				}
				else {
					$result->status = 0;
					$result->message = "Subject already exist.";
				}
					return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		
		/**
		* this function adds new unit row 
		*
		* @author Utkarsh Pandey
		* @date 25-05-2015
		* @param JSON with all required details
		* @return JSON with name and description
		* @return JSON with id, status and message
		**/
		public function addNewUnit($data) {
			$result = new stdClass();
			
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('units');
				$select->where(array(
					'deleted' => 0,
					'name' => $data->name,
					'subject_id' => $data->p_id
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exist = $run->toArray();
				if(count($exist) < 1) {
					$insert = new TableGateway('units', $adapter, null, new HydratingResultSet());
					$insert->insert(array(
						'name'	=>	htmlentities($data->name),
						'description'	=>	htmlentities($data->description),
						'subject_id'	=>	$data->p_id,
						'user_id'	=>	1, //$_SESSION['userId'],
						'created'	=>	time()
					));
					$id= $insert->getLastInsertValue();
					if($id > 0) {
						$result->id = $id;
						$result->status = 1;
						$result->message = "Unit Successfully Added";
					}
				}
				else {
					$result->status = 0;
					$result->message = "Unit already exist.";
				}
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* this function adds new chapter row 
		*
		* @author Utkarsh Pandey
		* @date 25-05-2015
		* @param JSON with all required details
		* @return JSON with name and description
		* @return JSON with id, status and message
		**/
		public function addNewChapter($data) {
			$result = new stdClass();
			
			try {
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('chapters');
				$select->where(array(
					'deleted' => 0,
					'name' => $data->name,
					'unit_id' => $data->p_id
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exist = $run->toArray();
				if(count($exist) < 1) {
					$insert = new TableGateway('chapters', $adapter, null, new HydratingResultSet());
					$insert->insert(array(
						'name'	=>	htmlentities($data->name),
						'description'	=>	htmlentities($data->description),
						'unit_id'	=>	$data->p_id,
						'user_id'	=>	1, //$_SESSION['userId'],
						'time'	=>	$data->time,
						'created'	=>	time()
					));
					$id= $insert->getLastInsertValue();
					if($id > 0) {
						$result->id = $id;
						$result->status = 1;
						$result->message = "Chapter Successfully Added";
					}
				}
				else {
					$result->status = 0;
					$result->message = "Chapter already exist.";
				}
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		
		
		
		/**
		* this function adds new topic row 
		*
		* @author Utkarsh Pandey
		* @date 28-05-2015
		* @param JSON with all required details
		* @return JSON with name and description
		* @return JSON with id, status and message
		**/
		public function addNewTopic($data) {
			$result = new stdClass();
			
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('topics');
				$select->where(array(
					'deleted' => 0,
					'name' => $data->name,
					'chapter_id' => $data->p_id
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exist = $run->toArray();
				if(count($exist) < 1) {
					$insert = new TableGateway('topics', $adapter, null, new HydratingResultSet());
					$insert->insert(array(
						'name'	=>	htmlentities($data->name),
						'description'	=>	htmlentities($data->description),
						'chapter_id'	=>	$data->p_id,
						'user_id'	=>	1, //$_SESSION['userId'],
						'created'	=>	time()
					));
					$id= $insert->getLastInsertValue();
					if($id > 0) {
						$result->id = $id;
						$result->status = 1;
						$result->message = "Topic Successfully Added";
					}
				}
				else {
					$result->status = 0;
					$result->message = "Topic already exist.";
				}
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
/*********************************************************************************************************************************/		



		/**
		* this function updates class row 
		*
		* @author Utkarsh Pandey
		* @date 29-05-2015
		* @param JSON with all required details
		* @return JSON with status and message
		**/
		public function updateClass($data) {
			$result = new stdClass();
			
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('classes');
				$select->where(array(
					'deleted' => 0,
					'name' => $data->name
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exist = $run->toArray();
				if(count($exist) < 1) {
					$update = $sql->update();
					$update->table('classes');
					$update->set(array(
						'name'	=> htmlentities($data->name),
						'description'	=>	htmlentities($data->description),
						'user_id'	=>	1, // $_SESSION['user_id'],
						'created'	=>	time()
					));
					$update->where(array(
						'id' => $data->id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$run = $statement->execute();
					$result->status = 1;
					$result->message = "Successfully Update.";
				}
				else {
					$result->status = 0;
					$result->message = "Class already exist.";
				}
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}




		/**
		* this function updates subject row 
		*
		* @author Utkarsh Pandey
		* @date 29-05-2015
		* @param JSON with all required details
		* @return JSON with status and message
		**/
		public function updateSubject($data) {
			$result = new stdClass();
			
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subjects');
				$select->where(array(
					'deleted' => 0,
					'name' => $data->name,
					'class_id' => $data->p_id					
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exist = $run->toArray();
				if(count($exist) < 1) {
					$update = $sql->update();
					$update->table('subjects');
					$update->set(array(
						'name'	=> htmlentities($data->name),
						'description'	=>	htmlentities($data->description),
						'user_id'	=>	1, // $_SESSION['user_id'],
						'created'	=>	time()
					));
					$update->where(array(
						'id' => $data->id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$run = $statement->execute();
					$result->status = 1;
					$result->message = "Successfully Update.";
				}
				else {
					$result->status = 0;
					$result->message = "Subject already exist.";
				}
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}




		/**
		* this function updates unit row 
		*
		* @author Utkarsh Pandey
		* @date 29-05-2015
		* @param JSON with all required details
		* @return JSON with status and message
		**/
		public function updateUnit($data) {
			$result = new stdClass();
			
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('units');
				$select->where(array(
					'deleted' => 0,
					'name' => $data->name,
					'subject_id' => $data->p_id					
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exist = $run->toArray();
				if(count($exist) < 1) {
					$update = $sql->update();
					$update->table('units');
					$update->set(array(
						'name'	=> htmlentities($data->name),
						'description'	=>	htmlentities($data->description),
						'user_id'	=>	1, // $_SESSION['user_id'],
						'created'	=>	time()
					));
					$update->where(array(
						'id' => $data->id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$run = $statement->execute();
					$result->status = 1;
					$result->message = "Successfully Update.";
				}
				else {
					$result->status = 0;
					$result->message = "Unit already exist.";
				}
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}




		/**
		* this function updates chapter row 
		*
		* @author Utkarsh Pandey
		* @date 29-05-2015
		* @param JSON with all required details
		* @return JSON with status and message
		**/
		public function updateChapter($data) {
			$result = new stdClass();
			
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('chapters');
				$select->where(array(
					'deleted' => 0,					
					'unit_id' => $data->p_id,
					'id'	=>	$data->id
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exist = $run->toArray();
				if(count($exist) == 1) {
					$update = $sql->update();
					$update->table('chapters');
					$update->set(array(
						'name'	=> htmlentities($data->name),
						'description'	=>	htmlentities($data->description),
						'user_id'	=>	1, // $_SESSION['user_id'],
						'time'	=>	$data->time,
						'unit_id' => $data->p_id,
						'created'	=>	time()
					));
					$update->where(array(
						'id' => $data->id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$run = $statement->execute();
					$result->status = 1;
					$result->message = "Successfully Update.";
				}
				else {
					$result->status = 0;
					$result->message = "Chapter doesnt exist.";
				}
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		


		/**
		* this function updates topic row 
		*
		* @author Utkarsh Pandey
		* @date 29-05-2015
		* @param JSON with all required details
		* @return JSON with status and message
		**/
		public function updateTopic($data) {
			$result = new stdClass();
			
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('topics');
				$select->where(array(
					'deleted' => 0,
					'name' => $data->name,
					//'chapter_id' => $data->p_id					
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exist = $run->toArray();
				if(count($exist) < 1) {
					$update = $sql->update();
					$update->table('topics');
					$update->set(array(
						'name'	=> htmlentities($data->name),
						'description'	=>	htmlentities($data->description),
						'user_id'	=>	1, // $_SESSION['user_id'],
						'created'	=>	time()
					));
					$update->where(array(
						'id' => $data->id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$run = $statement->execute();
					$result->status = 1;
					$result->message = "Successfully Update.";
				}
				else {
					$result->status = 0;
					$result->message = "Topic already exist.";
				}
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}








		
/****************************************************************************************************/		



		/**
		* this function deletes class row 
		*
		* @author Utkarsh Pandey
		* @date 09-08-2015
		* @param JSON with all required details
		* @return JSON with status and message
		**/
		public function deleteClass($data) {
			$result = new stdClass();
			
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				$update = $sql->update();
				$update->table('classes');
				$update->set(array(
					'deleted'	=>	1,
					'user_id'	=>	1, // $_SESSION['user_id'],
					'created'	=>	time()
				));
				$update->where(array(
					'id' => $data->id
				));
				$statement = $sql->prepareStatementForSqlObject($update);
				$run = $statement->execute();
				$result->status = 1;
				$result->message = "Class Successfully Deleted.";
				
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->error = $e->getMessage();
				$result->message = "Error: Some thing went wrong";
				return $result;
			}
		}




		/**
		* this function deletes subject 
		*
		* @author Utkarsh Pandey
		* @date 09-08-2015
		* @param JSON with all required details
		* @return JSON with status and message
		**/
		public function deleteSubject($data) {
			$result = new stdClass();
			
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				$update = $sql->update();
				$update->table('subjects');
				$update->set(array(
					'deleted' => 1,
					'user_id'	=>	1, // $_SESSION['user_id'],
					'created'	=>	time()
				));
				$update->where(array(
					'id' => $data->id
				));
				$statement = $sql->prepareStatementForSqlObject($update);
				$run = $statement->execute();
				$result->status = 1;
				$result->message = "Subject Successfully Deleted.";
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "Error: Some thing went wrong";
				$result->error = $e->getMessage();
				return $result;
			}
		}




		/**
		* this function deletes unit 
		*
		* @author Utkarsh Pandey
		* @date 09-08-2015
		* @param JSON with all required details
		* @return JSON with status and message
		**/
		public function deleteUnit($data) {
			$result = new stdClass();
			
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
			
				$update = $sql->update();
				$update->table('units');
				$update->set(array(
					'deleted' => 1,
					'user_id'	=>	1, // $_SESSION['user_id'],
					'created'	=>	time()
				));
				$update->where(array(
					'id' => $data->id
				));
				$statement = $sql->prepareStatementForSqlObject($update);
				$run = $statement->execute();
				$result->status = 1;
				$result->message = "Unit Successfully Deleted.";
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "Error: Some thing went wrong";
				$result->error = $e->getMessage();
				return $result;
			}
		}




		/**
		* this function deletes chapter
		*
		* @author Utkarsh Pandey
		* @date 09-08-2015
		* @param JSON with all required details
		* @return JSON with status and message
		**/
		public function deleteChapter($data) {
			$result = new stdClass();
			
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('chapters');
				$update->set(array(
					'deleted' => 1,
					'user_id'	=>	1, // $_SESSION['user_id'],
					'created'	=>	time()
				));
				$update->where(array(
					'id' => $data->id
				));
				$statement = $sql->prepareStatementForSqlObject($update);
				$run = $statement->execute();
				$result->status = 1;
				$result->message = "Chapter Successfully Deleted.";
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "Error: Some thing went wrong";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		


		/**
		* this function deletes topic 
		*
		* @author Utkarsh Pandey
		* @date 09-08-2015
		* @param JSON with all required details
		* @return JSON with status and message
		**/
		public function deleteTopic($data) {
			$result = new stdClass();
			
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				$update = $sql->update();
				$update->table('topics');
				$update->set(array(
					'deleted' => 1,
					'user_id'	=>	1, // $_SESSION['user_id'],
					'created'	=>	time()
				));
				$update->where(array(
					'id' => $data->id
				));
				$statement = $sql->prepareStatementForSqlObject($update);
				$run = $statement->execute();
				$result->status = 1;
				$result->message = "Topic Successfully Deleted.";
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "Error: Some thing went wrong";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		
/***************************************************************************************************************/		
		
		/**
		* this function fetch all classes, subjects, units, chapters and topics
		*
		* @author Utkarsh Pandey
		* @date 26-05-2015
		* @param JSON with all required details
		* @return JSON with all details
		* @return JSON with classes, subjects, units, chapters, topics, status and message
		**/
		public function fetchTreeData($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				// QUERY SELECT `classes`.`id` AS `class_id`, `classes`.`name` AS `class_name`, `classes`.`description` AS `class_description` FROM `classes` WHERE `deleted` = '0'
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('classes');
				$select->columns(array(
					'class_id'=> 'id',
					'class_name'	=>	'name',
					'class_description'	=>	'description'
				));
				$select->where(array(
					'deleted'	=>	0
				));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$run = $run->toArray();
				$classes = $run;
				
				// QUERY SELECT `subjects`.`id` AS `subject_id`, `subjects`.`name` AS `subject_name`, `subjects`.`description` AS `subject_description` FROM `subjects` WHERE `deleted` = '0'
				$select = $sql->select();
				$select->from('subjects');
				$select->columns(array(
					'subject_id'	=> 'id',
					'subject_name'	=>	'name',
					'subject_description'	=>	'description',
					'class_id'	=>	'class_id'
				));
				$select->where(array(
					'deleted'	=>	0
				));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$run = $run->toArray();
				$subjects = $run;
				
				
				// QUERY SELECT `units`.`id` AS `unit_id`, `units`.`name` AS `unit_name`, `units`.`description` AS `unit_description` FROM `units` WHERE `deleted` = '0'
				$select = $sql->select();
				$select->from('units');
				$select->columns(array(
					'unit_id'	=> 'id',
					'unit_name'	=>	'name',
					'unit_description'	=>	'description',
					'subject_id'	=>	'subject_id'
				));
				$select->where(array(
					'deleted'	=>	0
				));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$run = $run->toArray();
				$units = $run;


				// QUERY SELECT `chapters`.`id` AS `chapter_id`, `chapters`.`name` AS `chapter_name`, `chapters`.`description` AS `chapter_description` FROM `chapters` WHERE `deleted` = '0'
				$select = $sql->select();
				$select->from('chapters');
				$select->columns(array(
					'chapter_id'	=> 'id',
					'chapter_name'	=>	'name',
					'chapter_description'	=>	'description',
					'unit_id'	=>	'unit_id',
					'time'	=>	'time'
				));
				$select->where(array(
					'deleted'	=>	0
				));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$run = $run->toArray();
				$chapters = $run;


				// QUERY SELECT `topics`.`id` AS `topic_id`, `topics`.`name` AS `topic_name`, `topics`.`description` AS `topic_description` FROM `topics` WHERE `deleted` = '0'
				$select = $sql->select();
				$select->from('topics');
				$select->columns(array(
					'topic_id'	=> 'id',
					'topic_name'	=>	'name',
					'topic_description'	=>	'description',
					'chapter_id'	=> 'chapter_id'
				));
				$select->where(array(
					'deleted'	=>	0
				));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$run = $run->toArray();
				$topics = $run;


				$result->html = $this->mapping($classes,$subjects,$units,$chapters,$topics);
				// $result->classes = $classes;
				// $result->subjects = $subjects;
				// $result->units = $units;
				// $result->topics = $topics;
				
				$result->status = 1;
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		
		public function mapping($classes,$subjects,$units,$chapters,$topics) {
			$html = '<ul>';
			foreach($classes as $class) {
				$html .= "<li data-mode='edit' data-action='getClass' data-id='{$class['class_id']}'  data-desc='{$class['class_description']}'>{$class['class_name']}<ul>";
				foreach($subjects as $subject) {
					if($subject['class_id'] == $class['class_id']) {
						$html .= "<li data-mode='edit' data-action='getSubject' data-id='{$subject['subject_id']}' data-desc='{$subject['subject_description']}'>{$subject['subject_name']}<ul>";
						foreach($units as $unit) {
							if($subject['subject_id'] == $unit['subject_id']) {
								$html .= "<li data-mode='edit' data-action='getUnit' data-id='{$unit['unit_id']}' data-desc='{$unit['unit_description']}'>{$unit['unit_name']}<ul>";
								foreach($chapters as $chapter) {
									if($unit['unit_id'] == $chapter['unit_id']) {
										$html .= "<li data-mode='edit' data-action='getChapter' data-id='{$chapter['chapter_id']}' data-time='{$chapter['time']}' data-desc='{$chapter['chapter_description']}'>{$chapter['chapter_name']}<ul>";
										foreach($topics as $topic) {
											if($chapter['chapter_id'] == $topic['chapter_id'])
												$html .= "<li data-mode='edit' data-action='getTopic' data-id='{$topic['topic_id']}' data-desc='{$topic['topic_description']}'>{$topic['topic_name']}</li>";
										}
										$html .= "<li data-mode='addNew' data-action='addTopic'>Add Topic</li></ul></li>";
									}
								}
								$html .= "<li  data-mode='addNew' data-action='addChapter'>Add Chapter</li></ul></li>";
							}
						}
						$html .= "<li data-mode='addNew' data-action='addUnit'>Add Unit</li></ul></li>";
					}
				}
				$html .= "<li data-mode='addNew' data-action='addSubject'>Add Subject</li></ul></li>";
			}
			$html .= "<li data-mode='addNew' data-action='addClass'>Add Class</li></ul>";
			
			return $html;
		}
		
	}	
?>
