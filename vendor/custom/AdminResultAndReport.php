<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;


	//including connection.php file to make connection with database
	require_once 'Connection.php';

	/**
	* this class contains all the functions of report card
	*/
	class AdminResultAndReport extends Connection	{

		/**
		* this get test program list
		*/
		function fetchTestsReport($req) {
			$result = new stdClass();
			try {

				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				//$statement = "SELECT tpa_tpo.test_paper_id, test_paper.name, (SELECT COUNT(id) FROM test_paper_questions WHERE test_paper_questions.test_paper_id = tpa_tpo.test_paper_id) AS total_questions, COUNT(test_paper_attempts.id) AS student_attempted, AVG(test_paper_attempts.marks_archived) AS avg_marks, test_paper_attempts.total_marks, AVG((test_paper_attempts.marks_archived*100) / test_paper_attempts.total_marks) AS avg_percent_marks, (SELECT COUNT(id) FROM student_program WHERE student_program.test_program_id = tpa_tpo.test_program_id) AS total_students FROM test_papers_of_test_program AS tpa_tpo LEFT JOIN test_paper ON test_paper.id = tpa_tpo.test_paper_id LEFT JOIN test_paper_attempts ON test_paper_attempts.test_paper_id = tpa_tpo.test_paper_id WHERE tpa_tpo.test_program_id = ".$req->test_program." AND tpa_tpo.test_paper_type_id = ".$req->test_type." GROUP BY tpa_tpo.test_paper_id";
				$statement = "SELECT pop.test_paper_id, test_paper.name,

					(SELECT SUM(test_paper_labels.number_of_question) FROM section INNER JOIN test_paper_labels ON section.id=test_paper_labels.section_id WHERE section.test_paper= pop.test_paper_id) AS total_questions,

					(SELECT COUNT(DISTINCT(student_id)) FROM test_paper_attempts WHERE test_paper_id=pop.test_paper_id) AS student_attempted,

					AVG(test_paper_attempts.marks_archived) AS avg_marks, test_paper_attempts.total_marks,

					AVG((test_paper_attempts.marks_archived*100) / test_paper_attempts.total_marks) AS avg_percent_marks,

					(SELECT COUNT(id) FROM student_program WHERE student_program.test_program_id = pop.test_program_id) AS total_students

					FROM test_papers_of_test_program AS pop
					LEFT JOIN test_paper ON test_paper.id = pop.test_paper_id
					LEFT JOIN test_paper_attempts ON test_paper_attempts.test_paper_id = pop.test_paper_id
					RIGHT JOIN test_paper_subjects ON test_paper_subjects.test_paper_id = pop.test_paper_id WHERE test_paper.deleted =0 AND pop.test_program_id =  $req->test_program  ";

					if(isset($req->test_type)){
						$statement .= "AND pop.test_paper_type_id = $req->test_type ";
					}
					/*if(isset($req->subject)){
						$statement .= "AND test_paper_subjects.subject_id = $req->subject ";
					}*/
					$statement .= " GROUP BY pop.test_paper_id ";
					//die($statement);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				$result->status = 1;
				$result->data = $res;
				return $result;
			}
			catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* function to Fetch Report Card Data
		*/
		function fetchSelectedTestReport($req) {

			$result = new stdClass();
			try {



				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$statement = "SELECT test_paper_attempts.id,
					users.id as user_id, users.first_name, users.last_name, users.email,
					test_paper_attempts.total_marks,
					test_paper_attempts.marks_archived, ((test_paper_attempts.marks_archived * 100) / test_paper_attempts.total_marks) as percennt_marks,
					test_paper.time as total_time
					FROM test_paper_attempts
					LEFT JOIN test_paper ON test_paper.id = test_paper_attempts.test_paper_id
					LEFT JOIN users ON users.id = test_paper_attempts.student_id WHERE test_paper_attempts.test_paper_id = $req->test_paper
					GROUP BY test_paper_attempts.student_id";


				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$students_data = $run->toArray();

				if(count($students_data))
				{
					foreach ($students_data as $key => $student)
					{
						$sql = new Sql($adapter);

						$statement = "SELECT COUNT(question_attempt.id) AS total_questions, sum(if(question_attempt.question_status > 1, 1, 0)) AS attemted_questions, sum(if(question_attempt.correct = 1, 1, 0)) AS correct_questions FROM question_attempt WHERE attempt_id = ".$student['id'];
						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
						$data = $run->toArray();
						if(count($data))
						{
							$students_data[$key]['attemted_questions'] = $data[0]['attemted_questions'];
							$students_data[$key]['correct_questions'] = $data[0]['correct_questions'];
							$students_data[$key]['total_questions'] = $data[0]['total_questions'];
							if($student['total_time'] != '0')
							{
								$students_data[$key]['speed'] = intval($data[0]['attemted_questions']) * 60 / intval($student['total_time']);
								$students_data[$key]['eas'] = intval($data[0]['correct_questions']) * 60 / intval($student['total_time']);
							}else
							{
								$students_data[$key]['speed'] = '0';
								$students_data[$key]['eas'] = '0';
							}
							if($data[0]['attemted_questions'] != '0')
							{
								$students_data[$key]['strike'] = intval($data[0]['correct_questions']) * 100 / intval($data[0]['attemted_questions']);
							}else{
								$students_data[$key]['strike'] = '0';
							}
						}
					}
				}



				/*$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$statement = "SELECT test_paper_attempts.id, question_attempt.attempt_id, users.id as user_id, users.first_name, users.email, COUNT(question_attempt.id) AS total_questions, sum(if(question_attempt.question_status = 3, 1, 0)) AS attemted_questions, sum(if(question_attempt.correct = 1, 1, 0)) AS correct_questions, test_paper_attempts.total_marks, test_paper_attempts.marks_archived, ((test_paper_attempts.marks_archived * 100) / test_paper_attempts.total_marks) as percennt_marks FROM test_paper_attempts LEFT JOIN users ON users.id = test_paper_attempts.student_id LEFT JOIN question_attempt ON question_attempt.attempt_id = test_paper_attempts.id WHERE test_paper_attempts.test_paper_id = ".$req->test_paper." GROUP BY test_paper_attempts.student_id";
				die($statement);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$students_data = $run->toArray();*/

				$result->data = $students_data;
				$result->status = 1;
				$result->message = "Opration Successful";
				return $result;
			}
			catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function fetchSelectedStudentTestReport($data)
		{
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from("users");
				$select->columns(array("id","first_name","last_name"));
				$select->where(array(
					'id' => $data->student
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$student = $run->toArray();
				$student = $student[0];

				$result->student = $student;



				/*$select = $sql->select();
				$select->from("test_paper_attempts");
				$select->columns(array("id","time"));
				$select->where(array(
					'test_paper_id' => $data->test_paper,
					'student_id' => $data->student
				));*/
				$statement = "SELECT test_paper_attempts.*, test_paper.type, test_paper.name, test_paper.time FROM test_paper_attempts LEFT JOIN test_paper ON test_paper.id  = test_paper_attempts.test_paper_id WHERE test_paper_attempts.test_paper_id = ".$data->test_paper." AND test_paper_attempts.student_id=".$data->student;

				//$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$attempt_data = $run->toArray();

				$data->attempt_id = $attempt_data[count($attempt_data) - 1]['id'];
				$data->userId = $data->student;


				$statement = "SELECT test_paper_attempts.*, test_paper.type,test_paper.name,test_paper.time FROM test_paper_attempts LEFT JOIN test_paper ON test_paper.id  = test_paper_attempts.test_paper_id WHERE test_paper_attempts.id = ".$data->attempt_id;
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$attempt_data = $run->toArray();

				if ($attempt_data[0]["type"] == 0) {
					$select = $sql->select();
					$select->from("test_paper_attempts");
					// $select->columns(array("id"));
					$select->where(array('optimizer_id' => $attempt_data[0]['test_paper_id']));
					$statement = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$attempt_data = $run->toArray();
					// die(print_r($attempt_data));

					$result->optimizer_id = $attempt_data[0]['optimizer_id'];
					$data->attempt_id = $attempt_data[0]["id"];
				}
				else {
					$result->optimizer_id = $attempt_data[0]['optimizer_id'];
				}
				$result->test_paper = "";
				if(count($attempt_data)){
					$result->test_paper = $attempt_data[0]['name'];
					$result->test_paper_time = $attempt_data[0]['time'];
				}

				$select = $sql->select();
				$select->from("question_attempt");
				$select->columns(array("id"));
				$select->where(array('attempt_id' => $data->attempt_id));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$total_questions = $run->toArray();

				/*
				* get all attempted questions of given attempt id
				*/
				$select->where('question_status > 1');
				$statement = $sql->getSqlStringForSqlObject($select);
				//die($statement);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$attempted = $run->toArray();

				/*
				* get all questions of given attempt id which answered correct by student
				*/
				$select->where(array('correct' => 1));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$correct = $run->toArray();

				/*
				* creating result for breif pie CHART
				*/
				$result->total_questions = count($total_questions);
				$result->attempted = count($attempted);
				$result->correct = count($correct);
				$result->wrong = $result->attempted - $result->correct;
				$result->unattempted = $result->total_questions - $result->attempted;

				$select = $sql->select();
				$select->from("test_paper_attempts");
				// $select->columns(array("id"));
				$select->where(array('id' => $data->attempt_id));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$attempt_data = $run->toArray();

				$result->your_score = $attempt_data[0]["marks_archived"];

				/*
				* get the topper marks
				*/
				$statement = 'SELECT MAX(marks_archived) AS marks_archived, FROM_UNIXTIME(end_time,"%d-%m-%Y") as test_date FROM `test_paper_attempts` WHERE test_paper_id= '.$attempt_data[0]["test_paper_id"];
				// die($statement);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$topper = $run->toArray();
				$result->topper_score = $topper[0]["marks_archived"];
				$result->test_date = $topper[0]["test_date"];

				/*
				* get the average marks
				*/
				$statement = "SELECT AVG(marks_archived) AS marks_archived FROM `test_paper_attempts` WHERE test_paper_id= ".$attempt_data[0]["test_paper_id"];
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$average = $run->toArray();
				$result->average_score = $average[0]["marks_archived"];

				$statement = "SELECT
					qa.id AS id,
					section.name AS section,
					IFNULL(group_concat(distinct if(tags.tag_type = 3,chapters.name,' ')),'') AS chapters,
					IFNULL(group_concat(distinct if(tags.tag_type = 2,units.name,' ')),'') AS units,
					IFNULL(group_concat(distinct if(tags.tag_type = 6,difficulty.difficulty,' ')),'') AS difficulty,
					IFNULL(group_concat(distinct if(tags.tag_type = 4,topics.name,' ')),'') AS topics,
					qa.id AS attempt,
					qa.time,
					qa.correct,
					qa.question_status,
					(SELECT COUNT(DISTINCT(student_id))/(SELECT COUNT(*) FROM users WHERE role='4')*100 FROM `question_attempt`  WHERE question_id = qa.question_id) AS percent, tp.time AS avgtime

					FROM `question_attempt` AS qa
					LEFT JOIN tagsofquestion AS tags ON qa.question_id=tags.question_id
					LEFT JOIN chapters AS chapters ON chapters.id=tags.tagged_id
					LEFT JOIN units AS units ON units.id=tags.tagged_id
					LEFT JOIN difficulty AS difficulty ON difficulty.id=tags.tagged_id
					LEFT JOIN topics AS topics ON topics.id=tags.tagged_id
					LEFT JOIN test_paper_attempts AS tpa ON tpa.id = qa.attempt_id
					LEFT JOIN test_paper AS tp ON tp.id = tpa.test_paper_id
					LEFT JOIN section AS section ON section.id=qa.section_id
					WHERE attempt_id=$data->attempt_id GROUP BY(qa.question_id)";

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$questionWiseReportRows = $run->toArray();

				$result->questionWiseReportRows = array();
				foreach ($questionWiseReportRows as $key => $questionWiseReportRow) {
					$questionWiseReportRow['units'] =  rtrim((str_replace(" ,","", $questionWiseReportRow['units'])), ', ');
					$questionWiseReportRow['chapters'] =  rtrim((str_replace(" ,","", $questionWiseReportRow['chapters'])), ', ');
					$questionWiseReportRow['difficulty'] =  rtrim((str_replace(" ,","", $questionWiseReportRow['difficulty'])), ', ');
					$questionWiseReportRow['topics'] =  rtrim((str_replace(" ,","", $questionWiseReportRow['topics'])), ', ');
					$questionWiseReportRow['units'] = ($questionWiseReportRow['units']==''?'NA':$questionWiseReportRow['units']);
					$questionWiseReportRow['chapters'] = ($questionWiseReportRow['chapters']==''?'NA':$questionWiseReportRow['chapters']);
					$questionWiseReportRow['difficulty'] = ($questionWiseReportRow['difficulty']==''?'NA':$questionWiseReportRow['difficulty']);
					$questionWiseReportRow['topics'] = ($questionWiseReportRow['topics']==''?'NA':$questionWiseReportRow['topics']);
					$result->questionWiseReportRows[] = $questionWiseReportRow;
				}

				$statement = "SELECT chapters.name AS name,COUNT(*) AS total_questions,SUM(correct) AS correct,SUM(IF(qa.question_status > 1 AND correct=0,1,0)) AS wrong,SUM(IF(qa.question_status > 1,1,0)) AS attempted,SUM(IF(qa.question_status < 2,1,0)) AS unattempted,SUM(marks) AS max_marks,SUM(if(qa.question_status>1 AND qa.correct = 1,marks,-neg_marks)) AS score, section_id, section.name AS section
					FROM `question_attempt` AS qa
					INNER JOIN tagsofquestion AS tags ON qa.question_id=tags.question_id
					LEFT JOIN chapters AS chapters ON chapters.id=tags.tagged_id
					LEFT JOIN section AS section ON section.id=qa.section_id
					WHERE attempt_id=$data->attempt_id AND tag_type='3'
					Group BY tags.tagged_id, qa.section_id";
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->topicWiseReportChart = $run->toArray();

				$statement = 'SELECT section_id,section.name AS section,(SELECT difficulty.difficulty FROM `difficulty` WHERE difficulty.id = tagsofquestion.tagged_id) AS name, (SELECT COUNT(t2.correct) FROM `question_attempt` AS t2 INNER JOIN tagsofquestion AS toq1 ON toq1.question_id = t2.question_id WHERE t2.question_status = 3 AND t2.attempt_id = '.$data->attempt_id.' AND toq1.tag_type=6 AND toq1.tagged_id=tagsofquestion.tagged_id AND t2.question_status = 3 AND t2.correct = 1) AS correct , (SELECT COUNT(t2.correct) FROM `question_attempt` AS t2 INNER JOIN tagsofquestion AS toq1 ON toq1.question_id = t2.question_id WHERE t2.question_status = 3 AND t2.attempt_id = '.$data->attempt_id.' AND toq1.tag_type=6 AND toq1.tagged_id=tagsofquestion.tagged_id AND t2.question_status = 3 AND t2.correct = 0) AS wrong FROM `question_attempt` INNER JOIN tagsofquestion ON question_attempt.question_id = tagsofquestion.question_id LEFT JOIN section AS section ON section.id=question_attempt.section_id WHERE question_attempt.question_status = 3 AND question_attempt.attempt_id = '.$data->attempt_id.' AND tagsofquestion.tag_type=6 GROUP BY tagsofquestion.tagged_id,question_attempt.section_id';
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->difficultyWiseReportChart = $run->toArray();

				$statement = "SELECT section_id, section.name AS section, skills.skill AS name,COUNT(*) AS total_questions,SUM(correct) AS correct,SUM(IF(qa.question_status > 1 AND correct=0,1,0)) AS wrong,SUM(IF(qa.question_status > 1,1,0)) AS attempted,SUM(IF(qa.question_status < 2,1,0)) AS unattempted,SUM(marks) AS max_marks,SUM(if(qa.question_status>1 AND qa.correct = 1,marks,-neg_marks)) AS score FROM `question_attempt` AS qa INNER JOIN tagsofquestion AS tags ON qa.question_id=tags.question_id LEFT JOIN skills AS skills ON skills.id=tags.tagged_id LEFT JOIN section AS section ON section.id=qa.section_id where attempt_id=$data->attempt_id AND tag_type='7' Group BY tags.tagged_id,qa.section_id";
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->skillWiseReportChart = $run->toArray();


				$statement = "SELECT section_id,section.name AS section,difficulty.difficulty AS name,COUNT(*) AS total_questions,SUM(correct) AS correct,SUM(IF(qa.question_status > 1 AND correct=0,1,0)) AS wrong,SUM(IF(qa.question_status > 1,1,0)) AS attempted,SUM(IF(qa.question_status < 2,1,0)) AS unattempted,SUM(marks) AS max_marks,SUM(if(qa.question_status>1 AND qa.correct = 1,marks,-neg_marks)) AS score FROM `question_attempt` AS qa INNER JOIN tagsofquestion AS tags ON qa.question_id=tags.question_id LEFT JOIN difficulty AS difficulty ON difficulty.id=tags.tagged_id LEFT JOIN section AS section ON section.id=qa.section_id where attempt_id=$data->attempt_id AND tag_type='6' Group BY tags.tagged_id,qa.section_id";

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->difficultyWiseTableReportRows = $run->toArray();

				$statement = "SELECT id, marks_archived, total_marks, (end_time - start_time) as total_time, ((marks_archived * 100) / total_marks) AS percent_marks, MAX(marks_archived) as topper_marks FROM test_paper_attempts WHERE id = ".$data->attempt_id;
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$percentileTable = $run->toArray();
				$percentileTable = $percentileTable[0];

				$statement = 'SELECT COUNT(id) as total_questions, SUM(correct) AS correct, SUM(IF(question_status = 3, 1, 0)) AS attempted FROM question_attempt WHERE attempt_id ='.$data->attempt_id;

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$question_data = $run->toArray();
				$question_data = $question_data[0];

				$percentileTable['total_questions'] = $question_data['total_questions'];
				$percentileTable['correct'] = $question_data['correct'];
				$percentileTable['attempted'] = $question_data['attempted'];

				$result->percentileTable = $percentileTable;


				$statement = 'SELECT distinct(student_id), marks_archived
								FROM `test_paper_attempts` where test_paper_id = '.$attempt_data[0]['test_paper_id'].' group by student_id';

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$percentile_rank = $run->toArray();
				$percentile = 100;
				if(count($percentile_rank))
				{	$marks=array();
					$a = 0;
					$b = 0;
					foreach ($percentile_rank as $key => $value) {
						array_push($marks, intval($value['marks_archived']));

					}
					$x = intval($attempt_data[0]['marks_archived']);
					foreach ($marks as $key => $value) {
						if($value < $x)
						{
							$a++;
						}else if($value == $x)
						{
							$b++;
						}
					}

					$percentile = ( ( $a + (0.5 * $b) )*100) / count($marks);
				}

				$result->a = $a;
				$result->b = $b;
				$result->c = $marks;
				$result->percentile_rank = intval($percentile);

				$statement = 'SELECT question_attempt.section_id, section.name, section.cut_off_marks, (SUM(IF(question_attempt.correct =1, question_attempt.marks, -question_attempt.neg_marks))) AS marks FROM question_attempt LEFT JOIN section ON section.id = question_attempt.section_id WHERE question_attempt.attempt_id = '.$data->attempt_id.' GROUP BY question_attempt.section_id';

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$sections = $run->toArray();

				$section_wise_report = array();
				foreach ($sections as $key => $section) {

					$statement = 'SELECT MAX(marks) AS topper_marks FROM (SELECT (SUM(IF(question_attempt.correct =1, question_attempt.marks, -question_attempt.neg_marks))) AS marks  FROM question_attempt WHERE question_attempt.section_id='.$section['section_id'].' GROUP BY student_id) AS section_wise_report';

					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$section_topper_marks = $run->toArray();
					$section_topper_marks = $section_topper_marks[0];

					$section['topper_marks'] = $section_topper_marks['topper_marks'];
					$section['success_gap'] = $section['cut_off_marks'] - $section['marks'];
					$section['topper_gap'] = $section['topper_marks'] - $section['marks'];
					$section_wise_report[] = $section;
				}

				$result->section_wise_report = $section_wise_report;

				require_once('StudentResults.php');
				$student_res = new StudentResults();
				$result->detailedReport = $student_res->fetchDetailedQuestionWiseReport($data->attempt_id);

				$result->status = 1;
				$result->message = "Result Successfully Fetched";
				return $result;
			}
            catch (Exception $e)
            {
				$result->status = 0;
				$result->message = "Some thing went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}

		public function fetchTestForStudent($data) {
			date_default_timezone_set('Asia/Kolkata');
			$resume = 0;
			$result = new stdClass();

			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$stm = "select first_name from users where id=".$data->student_id.";";
				$run = $adapter->query($stm, $adapter::QUERY_MODE_EXECUTE);
				$user = $run->toArray();
				$user_name=$user[0]['first_name']; //username

				$select = $sql->select();
				$select->from(array('test_paper_attempts' => 'test_paper_attempts'))
         			   ->join(array('test_paper'=>'test_paper'),'test_paper.id= test_paper_attempts.test_paper_id',array('name'),'left')
         			   ->join(array('test_program'=>'test_program'),'test_program.id= test_paper_attempts.program_id',array('program_name'=>'name'),'left');


				$where = new $select->where();
				$where->equalTo('student_id',$data->student_id);

				$select->where($where);
         		$select->columns(array('id' => 'test_paper_id','marks_archived','total_marks',"attempt_data_time" => new Expression('FROM_UNIXTIME(test_paper_attempts.start_time,"%d %b,%Y %h:%i %p")')));
         		$select->order('id');
         		$select->group('test_paper_id');
         		// print_r($select);

         		$selectString = $sql->getSqlStringForSqlObject($select);
         		// die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();

				if(count($rows)){
					$result->test_paper=$rows;
					$result->username = $user_name;
					$result->status = 1;
					$result->message = 'Test paper found.';
				}
				else{
					$result->test_paper=array();
					$result->username = $user_name;
					$result->status = 1;
					$result->message = 'No test paper found.';
				}

				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
	}