<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	//including connection.php file to make connection with database
	require_once 'Connection.php';

	class Enrollment extends Connection{

		public function createEnrollment($data) {

			$result = new stdClass();

			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$time = time();
				$insert = "INSERT INTO enrollment(code, test_program_id, expiry_date, created) VALUES ";
				$insert .= "('$data->code', $data->id, '0', $time),";
				$insert = rtrim($insert, ',');

				$run = $adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);

				$result->status = 1;
				$result->message = "Enrollment ID Created.";
				return $result;
			}
			catch(Exception $e)
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}
		}

		public function applyEnrollment($data) {

			$result = new stdClass();

			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('enrollment');
				// $select->columns(array('id'));
				$select->where(array(
					'deleted' => 0,
					'student_id' => 0,
					'code' => $data->promo,
					'test_program_id' => $data->program
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();

				if(count($rows) == 1) {
					$result->status = 1;
					$result->message = "Valid Enrollment ID.";
					$result->enrollment = $rows[0];
				}
				else {
					$result->status = 0;
					$result->message = 'Invalid Enrollment ID';
				}

				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}

		}

		public function fetchEnrollment($data) {

			$result = new stdClass();

			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

					$statement = "SELECT coupons.id, coupons.code, test_program.name AS program_name, coupons.test_program_id, coupons.student_id, from_unixtime(coupons.created, '%d-%M-%Y : %h:%i:%s') AS start_date, from_unixtime(coupons.expiry_date, '%d-%M-%Y %h:%i:%s') AS expiry_date, from_unixtime(coupons.created, '%d-%M-%Y %h:%i:%s') AS created FROM enrollment as coupons LEFT JOIN test_program ON test_program.id = coupons.test_program_id WHERE coupons.test_program_id = '$data->test_program_id' order by coupons.id";

					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$coupons = $run->toArray();

					$result->coupons =	$coupons;

				$result->status = 1;
				$result->message = "Coupons Fetched.";

				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}

		}
	}