<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\Sql\Sql\Expression;
	use Zend\Db\ResultSet\HydratingResultSet;

	//including connection.php file to make connection with database
	require_once 'Connection.php';
	require_once('Question.php');
	
	// Include 'Composer' autoloader.
	include 'PDF-Library/autoload.php';
	
	class PDF extends Connection {
		/**
		* this function fetch all classes
		*
		* @author Utkarsh Pandey
		* @date 01-06-2015
		* @param JSON with all required details
		* @return JSON with all details
		* @return JSON with all classesDetails, status and message
		**/
		public function pdfUpload($file) {
			$result = new stdClass();
			try {
				
				$filename	=	$file->file['name'];
				$filetype	=	$file->file['type'];
				if($filetype == "application/pdf") {
					$pdf = "imported_pdf.pdf";
					// var_dump($file);
					if (move_uploaded_file($file->file['tmp_name'], $pdf)) {
						
						//Parsed Data Recieved
						//$parsedData = $this->parseData($pdf);
						$parsedData = $this->parseData($pdf);

						//Split Each Question
						
						$questions = preg_split('/(^Question|[[:space:]]Question)[[:space:]][[:digit:]]#/', $parsedData);
						unset($questions[0]);
						$number_of_questions = 0;
						foreach ($questions as $question) 
						{
							$file->parent_id = 0;								
							$result = $this->saveQuestion($question, $file);
							if($result->status == 1) {
								$number_of_questions++;
							}
						}

						$result->message = $number_of_questions.' '.$result->message;
						unlink($pdf);
						return $result;
					} 
					else {

					}
				}
				else {
					$result->status = 0;
					$result->message = "ERROR: This File is not a PDF file.";
					return $result;
				}
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* this function change option index character to number 
		*
		* @author Utkarsh Pandey
		* @date 17-07-2015
		* @param character
		* @return integer value
		**/
		public function characterNumber($index) {
			$result = new stdClass();
			try {
				$change = array('a' => 1,'b' => 2,'c' => 3,'d' => 4,'e' => 5,'f' => 6,'g' => 7,'h' => 8,'i' => 9,'j' => 10);
				$index = strtolower($index);
				$number = $change[$index];
				return $number;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		
		/**
		* function to save a question
		*/
		public function saveQuestion($question, $file) {
			/*
			* SCQ
			*/
			if(preg_match('/[SubQuestions] #passage#/', $question)) {
				$questionType= "11";
			}
			elseif(preg_match('/[Options] #scq#/', $question)) {
				$questionType= "1";
			}
			elseif(preg_match('/[Options] #mcq#/', $question)) {
				$questionType= "2";
			}
			elseif(preg_match('/[Options] #ord#/', $question)) {
				$questionType= "4";
			}
			elseif(preg_match('/[Options] #matching#/', $question)) {
				$questionType= "5";
			}
			elseif(preg_match('/[Options] #fb#/', $question)) {
				$questionType= "6";
			}
			else {
				die('Unknown Question Type: '.var_dump($question));
			}


			switch ($questionType) 
			{
				
				/*
				* Single Choice
				*/
				case '1':
				{
					$data = $this->singleChoiceLogic($question);
					break;
				}

				/*
				*Multiple Choice
				*/
				case '2':
				{
					$data = $this->multiChoiceLogic($question);
					break;
				}
				
				/*
				* Ordering/Re-arranging
				*/
				case '4':
				{
					$data = $this->orderingLogic($question);
					break;
				}
				
				/*
				* Matching
				*/
				case '5':
				{
					$data = $this->matchingLogic($question);
					break;
				}
				/*
				*Fill in Blanks
				*/
				case '6':
				{
					$data = $this->fillinblanksLogic($question);
					break;
				}
				
				/*
				* Passage
				*/
				case '11':
				{
					$data = $this->passageLogic($question);
					break;
				}

				/*
				* Integer
				*/								
				case '13':
				{
					$data = $this->integerLogic($question);
					break;
				}
			}


				// var_dump($data);
				// die();
					
				if ($questionType == '11') {
					$result = $this->savePassage($data, $file);
					return $result;
				}
				//creating object for add question
				$obj = new stdClass();
				$obj->questionData = $data->question;
				$obj->DescriptionData = "No Description.";
				$obj->questionType = $questionType;
				$obj->class_id = $file->class_id;
				$obj->options = $data->options;
				$obj->subject_ids = $file->subject_ids;
				$obj->unit_ids = $file->unit_ids;
				$obj->chapter_ids = $file->chapter_ids;
				$obj->topic_ids = $file->topic_ids;
				$obj->tag_ids = $file->tag_ids;
				$obj->difficultyLevel_ids = $file->difficultyLevel_ids;
				$obj->skill_ids = $file->skill_ids;
				$obj->parent_id = $file->parent_id;
				$obj->userId = $file->userId;
				$q = new Question();
				// die(print_r($obj));
				$result = $q->addQuestion($obj);
			
			return $result;
		}

		/*
		* function to save passage
		*/
		public function savePassage($question_full, $file)
		{
			$result = new stdClass();
			try {
				// die(print_r($passage));


				//creating object for add question
				$obj = new stdClass();
				$obj->questionData = $question_full->passage;
				$obj->DescriptionData = "";
				$obj->questionType = 11;
				$obj->class_id = $file->class_id;
				$obj->options = array();
				$obj->subject_ids = $file->subject_ids;
				$obj->unit_ids = $file->unit_ids;
				$obj->chapter_ids = $file->chapter_ids;
				$obj->topic_ids = $file->topic_ids;
				$obj->tag_ids = $file->tag_ids;
				$obj->difficultyLevel_ids = $file->difficultyLevel_ids;
				$obj->skill_ids = $file->skill_ids;
				$obj->parent_id = 0;
				$obj->userId = $file->userId;
				$q = new Question();
				$result = $q->addQuestion($obj);
				
				if(isset($result->questionId)) {
					$file->parent_id = $result->questionId;
				}
					
				$number_of_questions = 0;
				foreach ($question_full->sub_questions as $i => $subQuestion) {
					$subQuestion = trim($subQuestion);
					if (strlen($subQuestion) == 0) {
						continue;
					}
					// var_dump($question_full->sub_questions);
					// die();
					$result = $this->saveQuestion($subQuestion, $file);
					if ($result->status == 1) {
						$number_of_questions++;
					}
				}
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/******************************************************************************************************************************/

		function parseData($fileName)
		{
			//Parser Object
			$parser = new \Smalot\PdfParser\Parser();

			//Parse Passed File
			$pdf = $parser->parseFile($fileName);

			/*_print_r($pdf->getPages());
			die();*/

			//Store Contents
			$text = $pdf->getText();
			
			//Return Contents
			return $text;
		}


		//Single Choice Logic
		public function singleChoiceLogic($question_full) {
			$result = new stdClass();
			try {
				$question_parts = split('Options #[a-z]{3,3}#', $question_full);
				$result->question = $question_parts['0'];
				//Get Correct Option\
				// die(var_dump($question_parts['0']));
				preg_match('/[*][A-Z]{1}:-/', $question_parts['1'], $correct_option);
				preg_match('/[A-Z]{1}/', $correct_option[0], $correct_option);
				$correct_option = $this->characterNumber($correct_option[0]);

				$options = split('[A-Z]{1}:- ', trim($question_parts['1']));
				// die(var_dump($options));
				
				$options[$correct_option - 1] = rtrim($options[$correct_option - 1], '* ');;
				$result->options = array();
				foreach ($options as $i => $opt) {
					$option = new stdClass();
					$option->option = trim($opt);
					if (strlen($option->option) == 0) {
						continue;
					}
					
					$option->answer = 0;
					$option->number = 0;
					if ($i == $correct_option) {
						$option->answer = 1;
					}
					$result->options[] = $option;
				}				
				unset($options);
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function multiChoiceLogic($question_full) {
			$result = new stdClass();
			try {
				$question_parts = split('Options #[a-z]{3,3}#', $question_full);
				$result->question = $question_parts['0'];
				$options = split('[A-Z]{1}:- ', trim($question_parts['1']));
				
				// Get Correct Options
				preg_match_all('/[*][A-Z]{1}:-/', $question_parts['1'], $correct_options);
				foreach ($correct_options[0] as $i => $correct_option) {
					preg_match('/[A-Z]{1}/', $correct_option, $correct_option);
					$correct_options[$i] = $this->characterNumber($correct_option[0]);
					$options[$correct_options[$i] - 1] = rtrim($options[$correct_options[$i] - 1], '* ');;
				}
				// die(var_dump(in_array(2, $correct_options)));
				
				$result->options = array();
				foreach ($options as $i => $opt) {
					$option = new stdClass();
					$option->option = trim($opt);
					if (strlen($option->option) == 0) {
						continue;
					}
					
					$option->answer = 0;
					$option->number = 0;
					if (in_array($i, $correct_options)) {
						$option->answer = 1;
					}
					$result->options[] = $option;
				}				
				unset($options);
				// die(var_dump($result));
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//Ordering Logic
		public function orderingLogic($question_full) {
			$result = new stdClass();
			try {
				$question_parts = split('Options #[a-z]{3,3}#', $question_full);
				$result->question = $question_parts['0'];
				$options = split('[A-Z]{1}:- ', trim($question_parts['1']));
				// die(var_dump($options));
				
				$result->options = array();
				foreach ($options as $i => $opt) {
					$option = new stdClass();
					$option->option = trim($opt);
					if (strlen($option->option) == 0) {
						continue;
					}
					$option->answer = 0;
					$option->number = $i;
					$result->options[] = $option;
				}				
				unset($options);
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//Matching Logic
		public function matchingLogic($question_full) {
			$result = new stdClass();
			try {
				$question_parts = split('Options #matching#', $question_full);
				$result->question = $question_parts['0'];
				$options = split('Column#[A-Z]{1}:-', trim($question_parts['1']));
				#Extra Options
				$column_A = preg_split('/[\s]?[0-9]{1,3}[#]{1}/', $options[1]);
				$extra_options = split('#Extra Options:-', trim($options[count($options) - 1]));
				$column_B = preg_split('/[\s]?[0-9]{1,3}[#]{1} /', $extra_options[0]);
				$extra_options = preg_split('/[\s]?[0-9]{1,3}[#]{1}/', $extra_options[1]);
				// die(var_dump($column_A).var_dump($column_B).var_dump($extra_options));
				$result->options = array();
				// Putting all columns(column A, column B, Extra Options) in options
				foreach ($column_A as $i => $opt) {
					$option = new stdClass();
					$option->option = trim($opt);
					if (strlen($option->option) == 0) {
						continue;
					}
					$option->answer = 0;
					$option->number = 1;
					$result->options[] = $option;
				}
				foreach ($column_B as $i => $opt) {
					$option = new stdClass();
					$option->option = trim($opt);
					if (strlen($option->option) == 0) {
						continue;
					}
					$option->answer = 1;
					$option->number = 2;
					$result->options[] = $option;
				}
				foreach ($extra_options as $i => $opt) {
					$option = new stdClass();
					$option->option = trim($opt);
					if (strlen($option->option) == 0) {
						continue;
					}
					$option->answer = 0;
					$option->number = 2;
					$result->options[] = $option;
				}
				// die(var_dump($result));
				unset($column_A);
				unset($column_B);
				unset($extra_options);
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//Fill in the blanks Logic
		public function fillinblanksLogic($question_full) {
			$result = new stdClass();
			try {
				$question_parts = split('Options #fb#', $question_full);
				$result->question = $question_parts['0'];
				$i = 0;
				while (preg_match('/[__]{4}/', $result->question)) {
					$i++;
					$input_box = " <input id=fb-".$i."> ";
					$result->question = preg_replace('/[__]{4}/', $input_box, $result->question, 1);
				}
					
				$options =  preg_split('/[\s]?[0-9]{1,3}[#]{1}/', trim($question_parts['1']));
				// die(var_dump($options));
				
				$result->options = array();
				// Putting all columns(column A, column B, Extra Options) in options
				foreach ($options as $i => $opt) {
					$option = new stdClass();
					$option->option = trim($opt);
					if (strlen($option->option) == 0) {
						continue;
					}
					$option->answer = 1;
					$option->number = 1;
					$result->options[] = $option;
				}
				unset($options);
				// die(var_dump($result));
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//Passage Logic
		public function passageLogic($question_full) {
			$result = new stdClass();
			try {
				$question_parts = split('SubQuestions #passage#', $question_full);
				$result->passage = $question_parts['0'];
				// die(var_dump($question_parts));
				
				$sub_questions =  preg_split('/[\s]?SubQuestion [0-9]{1,3}[#]{1}/', trim($question_parts['1']));
				// die(var_dump($sub_questions));

				$result->sub_questions = $sub_questions;
				
				unset($sub_questions);
				unset($question_parts);
				// die(var_dump($result));
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//Integer Logic
		public function integerLogic($question)
		{
			$data = array();
			$questionData= explode("Answers:", $question);
			$data['question']= $questionData[0];
			$data['options'] = array(); 
			
			$opt = explode("From: ", $questionData[1]);
			$opt = explode("To:", $opt[1]);

			$options['option_data'] = trim($opt[0]);
			$options['correct'] = 1;
			$options['number'] = 1;
			$data['options'][] = $options;
			// die(print_r($opt));
			
			$options['option_data'] = trim($opt[1]);
			$options['correct'] = 1;
			$options['number'] = 2;
			$data['options'][] = $options;

			
			// $questionData[1]= $questionData[1]/10;
			// $tens= floor($questionData[1])%10;
			// $data['tens']= $tens;
			// $hundreds= floor($tens/10);
			// $data['hundreds']= $hundreds;
			
			// die(print_r($data));
			return $data;
		}

	}
?>
