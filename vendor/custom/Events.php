<?php
	require_once 'Connection.php';
	class Events{
		// [0] -> black
		private static $COLORS = array("#675B5B","#C7408B","#3a87ad","#35aa47","#E84C4C","#BB7F12");
		//private static $COLORS = array("#black","#blue","#3a87ad","#35aa47","#red","#orange");
		private static $OFFDAY = array();
		private static $HOLIDAY = array();
		private static $STUDYDAY = array();
		public static $counter = 0;
		public static $events = array();
		public static $flag = true;

		

		public static function getStudyDays($start, $end, $offDays = array(), $daily = true)
		{
			//returns the array of studydays which also includes school holidays
			$study = array();

			for($mk=strtotime($start);$mk<=strtotime($end);$mk+=86400)
			{
				$study[] = date('d-m-Y',$mk);
				if($daily == false)
				{
					//alternate case
					$mk += 86400;
				}
			}
			$study = array_diff($study, $offDays);
			$study = array_values($study);

			return $study;
		}

		public static function getDays($start, $end, $offDays = array(), $daily = true)
		{
			//returns the array of total days to study (holiday + normal) which also excludes offdays holidays
			$study = array();

			for($mk=strtotime($start);$mk<=strtotime($end);$mk+=86400)
			{
				array_push($study,date('d-m-Y',$mk));
				if($daily == false)
				{
					//alternate case
					$mk += 86400;
				}
			}
			$study = array_diff($study, $offDays);
			$study = array_values($study);

			return $study;
		}

		public static function getHoliDays($start, $end, $holiday = array(),$offDays = array())
		{
			//returns the array of holidays which also includes SUNDAYS
			$study = array();

			for($mk=strtotime($start);$mk<=strtotime($end);$mk+=86400)
			{
				if(date('l',$mk) == "Sunday" && !in_array(date('d-m-Y',$mk),$holiday) && !in_array(date('d-m-Y',$mk),$offDays))
				{
					array_push($holiday,date('d-m-Y',$mk));
				}
			}
			$holiday = array_diff($holiday, $offDays);
			$holiday = array_values($holiday);
			self::$HOLIDAY = $holiday;
			self::$OFFDAY = $offDays;
			return $holiday;
		}

		public static function getMin($dates)
		{
			if(count($dates)){
				$min = $dates[0];
				foreach ($dates as $date)
				{
					if(strtotime($date) < strtotime($min))
					{
						$min = $date;
					}
				}
			return $min;
			}
			return 0;
		}

		public static function getMax($dates)
		{
			if(count($dates)){
				$max = $dates[0];
				foreach ($dates as $date)
				{
					if(strtotime($date) > strtotime($max))
					{
						$max = $date;
					}
				}
			return $max;
			}
			return 0;
		}

		public static function _getEvent($d1, $d2, $h1, $h2, $holiday, $day, $off ,$chap, $lc, $pq, $tr, $start, $end)
		{
			//$s1=Events::_slotToTime($d1,time());
			//$s2=Events::_slotToTime($d2,time());
			self::$counter = strtotime($start);
			$events = array();
			//_print_r($day);
			//_print_r($off);


			for ($i=0; $i < count($lc); $i++)
			{
				$rand=rand(1,5);
				//_print_r(count($chap).count($lc).count($pq).count($tr));
				if($lc[$i] != '')
				{
					$dis = intval($lc[$i]) * 60 * 60;
					if(Events::$flag == true)
					{
						Events::_planner($dis,$day,$holiday,$off,$d1,$d2,$h1,$h2,$start,$end,'LC',$chap[$i],self::$COLORS[$rand]);
					}
				}
				if($pq[$i] != '')
				{
					$dis = intval($pq[$i]) * 60 * 60;
					if(Events::$flag == true)
					{
						Events::_planner($dis,$day,$holiday,$off,$d1,$d2,$h1,$h2,$start,$end,'PQ',$chap[$i],self::$COLORS[$rand]);
					}
				}
				if($tr[$i] != '')
				{
					$dis = intval($tr[$i]) * 60 * 60;
					if(Events::$flag == true)
					{
						Events::_planner($dis,$day,$holiday,$off,$d1,$d2,$h1,$h2,$start,$end,'TR',$chap[$i],self::$COLORS[$rand]);
					}
				}
				
			}
			
			//_print_r(self::$events);

		}

		public static function _planner($dis,$day,$holiday,$off,$d1,$d2,$h1,$h2,$start,$end,$type,$chapter,$color)
		{	
			$counter = self::$counter;
			while(in_array(date('d-m-Y',$counter),$off)){
				$counter = Events::_increment($counter,$day);
				//_date($counter);
			}
			
			$e = array();
			while($dis > 0 && $counter < strtotime($end)) 
			{
				
				if(in_array(date('d-m-Y',$counter),$day) && !in_array(date('d-m-Y',$counter),$holiday))
				{
					$s1 = Events::_slotToTime($d1,$counter);
					$s2 = Events::_slotToTime($d2,$counter);								
				}
				else if(in_array(date('d-m-Y',$counter),$day) && in_array(date('d-m-Y',$counter),$holiday))
				{
				
					$s1 = Events::_slotToTime($h1,$counter);
					$s2 = Events::_slotToTime($h2,$counter);

				}
				else if(in_array(date('d-m-Y',$counter),$off))
				{					
					$counter = Events::_increment($counter,$day);
				}
				for ($j = 0; $j < count($s1); $j++) 
				{
					$diff = $s2[$j] - $s1[$j];
					
					if($counter < $s1[$j] && $dis > 0)
					{
						$counter = $s1[$j];
						if($dis > $diff && $dis > 0)
						{
							$counter = $s2[$j];
							array_push(self::$events, array('chap'=>$chapter,'type'=>$type,'start'=>date('H:i:s',$s1[$j]),'end'=>date('H:i:s',$s2[$j]),'alloted'=>$diff/60,'color'=>$color,'day'=>date('Y-m-d',$counter),'counter'=>date('d-m-Y H:i:s',$counter),'t'=>'s'));
							$dis = $dis - $diff;

							//_print_r("$chapter");				

						}
						else if($dis <= $diff && $dis > 0)
						{
							$counter = $s1[$j] + $dis;
							array_push(self::$events, array('chap'=>$chapter,'type'=>$type,'start'=>date('H:i:s',$s1[$j]),'end'=>date('H:i:s',$counter),'alloted'=>$dis/60,'color'=>$color,'day'=>date('Y-m-d',$counter),'test'=>'test a','counter'=>date('d-m-Y H:i:s',$counter)));
							$dis = 0;						

						}							
					}
					else if($counter >= $s1[$j] && $counter < $s2[$j] && $dis > 0)
					{
						//if($dis > $diff && $dis > 0)
						if($dis > ($s2[$j]-$counter) && $dis > 0)
						{
							array_push(self::$events, array('chap'=>$chapter,'type'=>$type,'start'=>date('H:i:s',$counter),'end'=>date('H:i:s',$s2[$j]),'alloted'=>($s2[$j]-$counter)/60,'color'=>$color,'day'=>date('Y-m-d',$counter),'test'=>'test1','counter'=>date('d-m-Y H:i:s',$counter)));
							$dis = $dis - ($s2[$j] - $counter);
							$counter = $s2[$j];
							//echo "<br>".date('d-m-Y H:i:s',$counter)." --- $dis---";
							//echo "$dis";
							

						}
						else if($dis <= ($s2[$j]-$counter) && $dis > 0)
						{
							//date('d-m-Y H:i:s',$counter);
							array_push(self::$events, array('chap'=>$chapter,'type'=>$type,'start'=>date('H:i:s',$counter),'end'=>date('H:i:s',($counter+$dis)),'alloted'=>($dis)/60,'color'=>$color,'day'=>date('Y-m-d',$counter),'test'=>'test2','counter'=>date('d-m-Y H:i:s',$counter)));
							$counter = $counter + $dis;
							$dis = 0;
						}
					}					
				}
				if($dis > 0)
				{
					//_date($counter);
					$counter = Events::_increment($counter,$day);
					//_date($counter);
					//echo "------------";
					if($counter == 0){
						Events::$flag = false;
						self::$counter = $counter;
						return;
					}
				}
				//$dis = 0;
				
			}
			//_print_r(self::$events);
			self::$counter = $counter;

			//return $e;
		}
		
		public static function _plannerOff($off=array())
		{
			
			for ($i=0; $i < count($off); $i++) {
				if($off[$i]!=''){
					array_push(self::$events, array('chap'=>'Off Day','type'=>'off','start'=>'00:00:00','end'=>'23:59:59','alloted'=>0,'color'=>self::$COLORS[0],'day'=>date_create_from_format('d-m-Y',$off[$i])->format('Y-m-d')));
				}
			}
		}
		public static function _slotToTime($s1,$time)
		{
			for ($i=0; $i < count($s1); $i++) { 
				$temp = date('d-m-Y',$time)." ".$s1[$i];
				$s1[$i] = intval(date_create_from_format('d-m-Y G:i A', $temp)->format('U'));
			}
			return $s1;
		}

		public static function _diff_in_seconds($d1,$d2)
		{
			return ($d1->diff($d2)->format('%H') * 60 * 60)+($d1->diff($d2)->format('%i')*60);
		}

		public static function _increment($time,$arr)
		{
			//wrong in case of off day
			/*$pos = array_search(date('d-m-Y',$time), $arr) + 1;
			if($pos < count($arr))
			{
				//_print_r($pos);
				return strtotime($arr[$pos]);
				
			}
			else
			{
				Events::$flag = false; //ran out of school days -- no more study
				return;
			}*/
			foreach ($arr as $value) {
				if($time < intval(strtotime($value)))
				{
					return intval(strtotime($value));
				}
			}
			return 0;
		}

	}
?>