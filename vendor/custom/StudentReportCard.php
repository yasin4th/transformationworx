<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;


	//including connection.php file to make connection with database
	require_once 'Connection.php';

	/**
	* this class contains all the functions of report card
	*/
	class StudentReportCard extends Connection	{

		/**
		* this get test program list
		*/
		function getTestProgramsOfStudent($req) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from(array('t_p' => 'test_program'),array())
	     			   ->join(array('s_p' => 'student_program'),
						't_p.id = s_p.test_program_id', array());

	     		$select->where(array(
				'user_id'	=>	$req->userId
					));

	     		$select->columns(array('id','name'));

				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				if($res)
				$result->status = 1;
				$result->data = $res;
				return $result;
			}
			catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		/**
		* function to get subjects of test program
		*/
		function getTestProgramsSubjects($req) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$selectString = "SELECT subjects.id, subjects.name FROM `test_program_subjects` LEFT JOIN subjects on subjects.id = test_program_subjects.subject_id WHERE test_program_id = ".$req->program_id;
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				if($res) {
					$result->status = 1;
				}
				$result->data = $res;
				return $result;
			}
			catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		/**
		* function to Fetch Report Card Data
		*/
		function getReportCardData($request)
		{
			$result = new stdClass();
			try
			{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				$select = $sql->select();
				$select->from('attempt_reports')
						->join(array('topper_report' => 'attempt_reports'), 'topper_report.program_id = attempt_reports.program_id AND topper_report.test_paper_id =attempt_reports.test_paper_id', array( 'topper_marks' => new Expression('MAX(topper_report.marks_archived)')));
	     		$select->where(array(
					'attempt_reports.student_id'	=>	$request->userId
				));
				$select->group('attempt_reports.test_paper_id');
	     		$select->columns(array('program_id', 'attempt_id', 'test_paper_id', 'name', 'test_paper_type', 'total_marks', 'marks_archived', 'total_time', 'time_taken', 'total_questions', 'attempted_questions', 'correct_questions'));
				$statement = $sql->getSqlStringForSqlObject($select);
				
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$report = $run->toArray();

				$result->report = $report;
				$result->status = 1;
				$result->message = "Operation Successful";
				return $result;
			}
			catch(Exception $e )
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
	}