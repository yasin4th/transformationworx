<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;

	//including connection.php file to make connection with database
	require_once 'Connection.php';
	class Question extends Connection {


		public function fetchClasses($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				// QUERY SELECT `classes`.`id` AS `class_id`, `classes`.`name` AS `class_name`, `classes`.`description` AS `class_description` FROM `classes` WHERE `deleted` = '0'
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('classes');
				$select->columns(array('id','name'));
				$select->where(array(
					'deleted'	=>	0
				));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$run = $run->toArray();
				$classes = $run;
				if(count($classes) > 0) {
					$result->status = 1;
					$result->classes = $classes;
				}
				$result->status = 1;
				$result->message = 'Operation Successful';
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
			}
		}
         // fetch test package
		public function fetchTestpackage($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				// QUERY SELECT `classes`.`id` AS `class_id`, `classes`.`name` AS `class_name`, `classes`.`description` AS `class_description` FROM `classes` WHERE `deleted` = '0'
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('test_program');
				$select->columns(array('id','name'));
				$select->where(array(
					'deleted'	=>	0
				));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$run = $run->toArray();
				$testpackages = $run;
				if(count($testpackages) > 0) {
					$result->status = 1;
					$result->testpackages = $testpackages;
				}
				$result->status = 1;
				$result->message = 'Operation Successful';
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
			}
		}


		public function fetchSubjects($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				// QUERY SELECT `id` AS id`, `subjects`.`name` AS name` FROM `subjects` WHERE `deleted` = '0' AND class_id = $data->class_id
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subjects');
				$select->columns(array('id','name'));
				$where = new $select->where();
				$where->equalTo('deleted',0);
				$where->equalTo('class_id',$data->class_id);
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$run = $run->toArray();
				$subjects = $run;
				if(count($subjects) > 0) {
					$result->status = 1;
					$result->subjects = $subjects;
				}
			return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
			}

		}

		public function fetchUnits($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				// QUERY SELECT `units`.`id`, `units`.`name` , `units`.`description` AS `unit_description` FROM `units` WHERE `deleted` = '0' AND subject_id IN($data->subject_id)
				$select = $sql->select();
				$select->from('units');
				$select->columns(array('id','name'));
				$where = new $select->where();
				$where->equalTo('deleted',0);
				$where->in('subject_id',$data->subject_id);
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$run = $run->toArray();
				$units = $run;
				if(count($units) > 0) {
					$result->status = 1;
					$result->units = $units;
				}
				$result->message = 'Operation Successful';
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
			}

		}


		public function fetchChapters($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				// QUERY SELECT `chapters`.`id`, `chapters`.`name` FROM `chapters` WHERE `deleted` = '0' AND unit_id In($data->unit_id)
				$select = $sql->select();
				$select->from('chapters');
				$select->columns(array('id','name'));
				$where = new $select->where();
				$where->equalTo('deleted',0);
				$where->in('unit_id',$data->unit_id);
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$run = $run->toArray();
				$chapters = $run;
				if(count($chapters) > 0) {
					$result->status = 1;
					$result->chapters = $chapters;
				}
				$result->message = 'Operation Successful';
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
			}
		}


		public function fetchTopics($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				// QUERY SELECT `topics`.`id`, `topics`.`name` FROM `topics` WHERE `deleted` = '0' AND chapter_id In($data->chapter_id)
				$select = $sql->select();
				$select->from('topics');
				$select->columns(array('id','name'));
				$where = new $select->where();
				$where->equalTo('deleted',0);
				if (empty($data->chapter_id)) {
					$data->chapter_id = array(0);
				}
				$where->in('chapter_id',$data->chapter_id);
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$run = $run->toArray();
				$topics = $run;
				if(count($topics) > 0) {
					$result->status = 1;
					$result->topics = $topics;
				}
				$result->message = 'Operation Successful';
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
			}

		}

		public function fetchTags($data) {

			$result = new stdClass();

			try{

				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('tags');
				$select->where(array(
					'delete'	=>	0
				));
				$select->order('id DESC');
				$select->columns(array('id','tag'));

				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				if($res)
				$result->status = 1;
				$result->tags = $res;
				$result->message = 'Operation Successful';
				return $result;

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		public function fetchDifficultyLevel($data) {

			$result = new stdClass();

			try{

				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('difficulty');
				$select->where(array(
					'delete'	=>	0
				));
				$select->columns(array('id','difficulty',));

				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$res = $run->toArray();
				$result->status = 1;
				$result->difficulty = $res;
				$result->message = 'Operation Successful';
				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}

		public function fetchSkills($data) {

			$result = new stdClass();

			try{

				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('skills');
				$select->where(array(
					'delete'	=>	0
				));
				$select->columns(array('id','skill'));

				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$res = $run->toArray();
				$result->status = 1;
				$result->skills = $res;
				$result->message = 'Operation Successful';
				return $result;

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}



		/**
		* function to delete question
		*
		* @author Utkarsh Pandey
		* @date 04-08-2015
		* @param JSON all data
		* @return JSON status and data
		**/
		public function deleteQuestion($data) {

			$result = new stdClass();

			try {
				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('questions');
				$update->set(array(
					'delete'	=> 1,
					'created'	=>	time()
				));
				$update->where(array(
					'id' => $data->question_id
				));

				$statement = $sql->getSqlStringForSqlObject($update);
				// $statement = 'UPDATE questions SET delete = 0 WHERE id ='.$data->question_id;

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				// $result->data = $run->toArray();
				$result->status = 1;
				$result->message = 'Question Deleted.';

				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}



		/**
		* function add question
		*
		* @author Utkarsh Pandey
		* @date 03-06-2015
		* @param JSON all data
		* @return JSON status and data
		**/
		public function addQuestion($data) {

			$result = new stdClass();

			try {
				$adapter = $this->adapter;
				// Query INSERT INTO `questions`(`class_id`, `question`, `description`, `type`, `user_id`, `created`) VALUES ($data->class_id,'$data->questionData','$data->DescriptionData',$data->questionType,$_SESSION['userId'],time())
				$insertQuestion = new TableGateway('questions', $adapter, null, new HydratingResultSet());
				$insertQuestionArray = array(
					'class_id' => $data->class_id,
					'question' => $data->questionData,
					'description' => $data->DescriptionData,
					'type' => $data->questionType,
					'parent_id' => $data->parent_id,
					'user_id' => $data->userId,
					'created' => time()
				);
				$insertQuestion->insert($insertQuestionArray);
				$question_id = $insertQuestion->getLastInsertValue();
				// creating query of Insert Options
				$insertOptions = "INSERT INTO `options`(`option`,`answer`,`number`,`question_id`, `user_id`, `created`) VALUES ";

				if(!empty($data->options)) {
					foreach($data->options as $opt) {
						if (empty($opt->number)) {
							$opt->number = 0;
						}
						$insertOptions .= "('".$this->safeString($opt->option)."','".$opt->answer."','".$opt->number."',".$question_id.",".$data->userId.",".time()."),";
					}
					$optionsQuery = rtrim($insertOptions,',');
					$run = $adapter->query($optionsQuery, $adapter::QUERY_MODE_EXECUTE);
				}

				$insertTaggedItem = "INSERT INTO `tagsofquestion`(`question_id`, `tagged_id`, `tag_type`) VALUES ";

				// ADDDING SUBJECT IDS IN INSERT QUERY
				foreach($data->subject_ids as $id) {
					$insertTaggedItem .= "(".$question_id.",".$id.",1),";
				}

				// ADDDING UNIT IDS IN INSERT QUERY
				foreach($data->unit_ids as $id) {
					$insertTaggedItem .= "(".$question_id.",".$id.",2),";
				}

				// ADDDING CHAPTER IDS IN INSERT QUERY
				foreach($data->chapter_ids as $id) {
					$insertTaggedItem .= "(".$question_id.",".$id.",3),";
				}

				// ADDDING TOPIC IDS IN INSERT QUERY
				foreach($data->topic_ids as $id) {
					$insertTaggedItem .= "(".$question_id.",".$id.",4),";
				}

				// ADDDING TAG IDS IN INSERT QUERY
				foreach($data->tag_ids as $id) {
					$insertTaggedItem .= "(".$question_id.",".$id.",5),";
				}

				// ADDDING DIFFICULTY LEVEL IDS IN INSERT QUERY
				foreach($data->difficultyLevel_ids as $id) {
					$insertTaggedItem .= "(".$question_id.",".$id.",6),";
				}

				// ADDDING SKILL IDS IN INSERT QUERY
				foreach($data->skill_ids as $id) {
					$insertTaggedItem .= "(".$question_id.",".$id.",7),";
				}

				$queryForTaggedItem = rtrim($insertTaggedItem,',');
				// return $queryForTaggedItem;
				$run = $adapter->query($queryForTaggedItem, $adapter::QUERY_MODE_EXECUTE);

				if($question_id) {
					$result->status = 1;
					$result->message = "Question Successfully Added.";
					$result->questionId = $question_id;
				}
				else {
					$result->status = 0;
					$result->message = "ERROR: This Question is Not Added.";
				}
				return $result;

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}


		/**
		* function fetch questions
		*
		* @author Utkarsh Pandey
		* @date 13-06-2015
		* @param JSON
		* @return jeson with status and all data
		**/
		public function fetchQuestions($data) {
			//date_default_timezone_set('Asia/Kolkata');
			$result = new stdClass();
			try{
				$adapter = $this->adapter;

				// $sql = new Sql($adapter);
				// $select = $sql->select();
				// $select->from('questions');
				// $where = new $select->where();
				// $where->equalTo('delete',0);
				// $select->where($where);
				// $select->columns(array('id','question','description','type','created'));
				// $statement = $sql->getSqlStringForSqlObject($select);

				// $statement = 'select questions.id, questions.question, questions.description,(CASE WHEN type= 1 THEN "SCQ" WHEN type=2 then "MCQ"  WHEN type=3 then "T/F" WHEN type=4 then "Sequencing" WHEN type=5 then "MTF" WHEN type=6 then "FB" WHEN type=7 then "Fb DD" WHEN type=8 then "FB DND" WHEN type=9 then "Matching" WHEN type=10 then "Descriptive" WHEN type=11 then "Passage Based" WHEN type=12 then "Matrix" WHEN type=13 then "Integer Based" END) AS type, (CASE WHEN questions.quality_check = 0 THEN "Pending" WHEN quality_check = 1 THEN "Done" END) AS qc,classes.name as class, (SELECT GROUP_CONCAT(subjects.name) FROM `tagsofquestion` LEFT JOIN subjects ON subjects.id = tagsofquestion.tagged_id WHERE tagsofquestion.tag_type=1 AND tagsofquestion.question_id=questions.id) as subjects, FROM_UNIXTIME(questions.created,"%d-%m-%y %h:%i %p") AS modified FROM questions left join classes on questions.class_id=classes.id WHERE questions.delete = 0 AND questions.parent_id = 0 ORDER BY questions.created DESC';
				@session_start();
				$statement = 'SELECT questions.id, questions.question, questions.description, question_types.short_name AS type, (CASE WHEN questions.quality_check = 0 THEN "Pending" WHEN quality_check = 1 THEN "Done" END) AS qc,classes.name as class, (SELECT GROUP_CONCAT(subjects.name) FROM `tagsofquestion` LEFT JOIN subjects ON subjects.id = tagsofquestion.tagged_id ';


				$statement .= ' WHERE tagsofquestion.tag_type=1 AND tagsofquestion.question_id=questions.id) as subjects, FROM_UNIXTIME(questions.created,"%d-%m-%y %h:%i %p") AS modified FROM questions';

				if($_SESSION['role'] == 3) {
					$statement .= " LEFT JOIN users ON questions.user_id = users.id ";
				}

				$statement .= ' left join classes on questions.class_id=classes.id left join question_types on question_types.id = questions.type WHERE questions.delete = 0 AND questions.parent_id = 0';

				if($_SESSION['role'] == 3) {
					$statement .= ' AND users.id = '.$_SESSION['userId'];
				}
				$statement .= ' ORDER BY questions.created DESC LIMIT '.$data->limit.',50';

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();

				$result->data = $rows;

				$statement = 'SELECT count(questions.id) as total_questions FROM questions';

				if($_SESSION['role'] == 3) {
					$statement .= " LEFT JOIN users ON questions.user_id = users.id ";
				}

				$statement .= ' WHERE questions.delete = 0 AND questions.parent_id = 0';

				if($_SESSION['role'] == 3) {
					$statement .= ' AND users.id = '.$_SESSION['userId'];
				}
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$total_questions = $run->toArray();
				$result->total_questions = $total_questions[0]['total_questions'];

				$result->status = 1;
				$result->message = 'Operation Successful';

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* function fetch questions for Question Bank Data Table
		**/
		public function fetchQuestionsForQBank($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;

				$statement = 'SELECT questions.id, questions.question, questions.description, question_types.short_name AS type, (CASE WHEN questions.quality_check = 0 THEN "Pending" WHEN quality_check = 1 THEN "Done" END) AS qc,classes.name as class, (SELECT GROUP_CONCAT(subjects.name) FROM `tagsofquestion` LEFT JOIN subjects ON subjects.id = tagsofquestion.tagged_id WHERE tagsofquestion.tag_type=1 AND tagsofquestion.question_id=questions.id) as subjects, FROM_UNIXTIME(questions.created,"%d-%m-%y %h:%i %p") AS modified FROM questions left join classes on questions.class_id=classes.id left join question_types on question_types.id = questions.type WHERE questions.delete = 0 AND questions.parent_id = 0 ORDER BY questions.created DESC';

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();

				$result->data = $rows;
				$statement = 'SELECT count(id) as total_questions FROM questions WHERE questions.delete = 0 AND questions.parent_id = 0';
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$total_questions = $run->toArray();
				$result->total_questions = $total_questions[0]['total_questions'];

				$result->status = 1;
				$result->message = 'Operation Successful';

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* function fetch all questions type
		*
		* @author Utkarsh Pandey
		* @date 17-06-2015
		* @param JSON
		* @return jeson with status and all data
		**/
		public function fetchQuestionTypes($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->columns(array('id', 'question_type'));
				$select->from('question_types');
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->data = $run->toArray();
				$result->status = 1;
				$result->message = 'fetched';

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* function to copy a question
		*
		* @author Utkarsh Pandey
		* @date 18-08-2015
		* @param JSON
		* @return jeson with status and all data
		**/
		public function copyQuestion($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				/*
				*	here we will fetch question and description and some other data
				*/
				$select = $sql->select();
				$select->from('questions');
				$where = new $select->where();
				$where->equalTo('delete',0);
				$where->equalTo('id',$data->question_id);
				$select->where($where);
				// $select->columns(array('question','description','class_id','type','quality_check'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$question = $run->toArray();

				/*
				*	now creating dublicate question
				*/
				$insertQuestion = new TableGateway('questions', $adapter, null, new HydratingResultSet());
				$insertQuestion->insert(array(
					'class_id' => $question[0]['class_id'],
					'question' => $question[0]['question'],
					'description' => $question[0]['description'],
					'type' => $question[0]['type'],
					'parent_id' => 0,
					'user_id' => $_SESSION['userId'],
					'created' => time()
				));
				$question_id = $insertQuestion->getLastInsertValue();

				if ($question[0]['type'] != 11) {
					/*
					*	here we will fetch all options
					*/
					$select = $sql->select();
					$select->from('options');
					$where = new $select->where();
					$where->equalTo('question_id',$data->question_id);
					$where->equalTo('deleted',0);
					$select->where($where);
					$select->columns(array('id','option','answer','number'));
					$statement = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$options = $run->toArray();



					// creating query of Insert Options
					$insertOptions = "INSERT INTO `options`(`option`,`answer`,`number`,`question_id`, `user_id`, `created`) VALUES ";

					if(!empty($options)) {
						foreach($options as $opt) {
							$insertOptions .= "('".$this->safeString($opt['option'])."','".$opt['answer']."','".$opt['number']."',".$question_id.",".$data->userId.",".time()."),";
						}
						$optionsQuery = rtrim($insertOptions,',');
						$run = $adapter->query($optionsQuery, $adapter::QUERY_MODE_EXECUTE);
					}

				}
				else {
					/*
					* fetch sub questions
					*/
					$select = $sql->select();
					$select->from('questions');
					$where = new $select->where();
					$where->equalTo('delete',0);
					$where->equalTo('parent_id',$data->question_id);
					$select->where($where);
					// $select->columns(array('question','description','class_id','type','quality_check'));
					$statement = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$sub_questions = $run->toArray();

					foreach($sub_questions as $x => $sub_question) {
						/*
						*	now creating dublicate sub question
						*/
						$insertQuestion = new TableGateway('questions', $adapter, null, new HydratingResultSet());
						$insertQuestion->insert(array(
							'class_id' => $sub_question['class_id'],
							'question' => $sub_question['question'],
							'description' => $sub_question['description'],
							'type' => $sub_question['type'],
							'parent_id' => $question_id,
							'user_id' => $_SESSION['userId'],
							'created' => time()
						));
						$sub_question_id = $insertQuestion->getLastInsertValue();

						/*
						*	here we will fetch all options
						*/
						$select = $sql->select();
						$select->from('options');
						$where = new $select->where();
						$where->equalTo('question_id',$sub_question['id']);
						$where->equalTo('deleted',0);
						$select->where($where);
						$select->columns(array('id','option','answer','number'));
						$statement = $sql->getSqlStringForSqlObject($select);

						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$options = $run->toArray();

						// creating query of Insert Options
						$insertOptions = "INSERT INTO `options`(`option`,`answer`,`number`,`question_id`, `user_id`, `created`) VALUES ";

						if(!empty($options)) {
							foreach($options as $opt) {
								$insertOptions .= "('".$this->safeString($opt['option'])."','".$opt['answer']."','".$opt['number']."',".$sub_question_id.",".$data->userId.",".time()."),";
							}
							$optionsQuery = rtrim($insertOptions,',');
							$run = $adapter->query($optionsQuery, $adapter::QUERY_MODE_EXECUTE);
						}
					}

				}

				$result->question_id = $question_id;
				$result->status = 1;
				$result->message = "Operation Successful";
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function getQuestionDetails($data) {
			$result =  new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				/*
				*	here we will fetch question and description and some other data
				*
				*/

				$select = $sql->select();
				$select->from('questions');
				$where = new $select->where();
				$where->equalTo('delete',0);
				$where->equalTo('id',$data->question_id);
				$select->where($where);
				$select->columns(array('question','description','class_id','type','quality_check'));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$question = $run->toArray();
				if(count($question) == 0)
				{
					$result->status = 2;
					$result->message = "ERROR: Question not found.";
					return $result;
				}

				$questionData = $question[0];

				/*
				*	here we will fetch all options
				*
				*/

				$select = $sql->select();
				$select->from('options');
				$where = new $select->where();
				$where->equalTo('question_id',$data->question_id);
				$where->equalTo('deleted',0);
				$select->where($where);
				$select->columns(array('id','option','answer','number'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$options = $run->toArray();


				/*
				* here we will fecth all tags-items of question
				*
				*/
				// $statement = 'SELECT tagged_id, tag_type FROM tagsofquestion WHERE question_id='.$data->question_id;
				$select = $sql->select();
				$select->from('tagsofquestion');
				$where = new $select->where();
				$where->equalTo('question_id',$data->question_id);
				$select->where($where);
				$select->columns(array('tagged_id','tag_type'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$taggedItems = $run->toArray();


				/*
				*	Now we will fetch all tagged data
				*
				*/
				$tagged = new stdClass();
				$tagged->subject	=	array();
				$tagged->unit	=	array();
				$tagged->chapter	=	array();
				$tagged->topic	=	array();
				$tagged->tag	=	array();
				$tagged->difficulty	=	array();
				$tagged->skill	=	array();
				foreach ($taggedItems as $data) {
					switch($data["tag_type"]) {
						case "1":
							array_push($tagged->subject,$data["tagged_id"]);
							break;

						case "2":
							array_push($tagged->unit,$data["tagged_id"]);
							break;

						case "3":
							array_push($tagged->chapter,$data["tagged_id"]);
							break;

						case "4":
							array_push($tagged->topic,$data["tagged_id"]);
							break;

						case "5":
							array_push($tagged->tag,$data["tagged_id"]);
							break;

						case "6":
							array_push($tagged->difficulty,$data["tagged_id"]);
							break;

						case "7":
							array_push($tagged->skill,$data["tagged_id"]);
							break;

						default:
							break;

					}
				}

				$fill = new stdClass();

				// quesry SELECT id, name FROM `classes` WHERE deleted=0
				$select = $sql->select();
				$select->from('classes');
				$where = new $select->where();
				$where->equalTo('deleted',0);
				$select->where($where);
				$select->columns(array('id','name'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$fill->classes = $run->toArray();

				// quesry SELECT id, name FROM `subjects` WHERE deleted=0 AND class_id=1
				$select = $sql->select();
				$select->from('subjects');
				$where = new $select->where();
				$where->equalTo('deleted',0);
				$where->equalTo('class_id',$questionData["class_id"]);
				$select->where($where);
				$select->columns(array('id','name'));
				$statement = $sql->getSqlStringForSqlObject($select);
				// die($statement );
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$fill->subjects = $run->toArray();
				if(!empty($tagged->subject)) {
					// quesry SELECT id, name FROM `units` WHERE deleted=0 AND subject_id=1
					$select = $sql->select();
					$select->from('units');
					$where = new $select->where();
					$where->equalTo('deleted',0);
					$where->in('subject_id',$tagged->subject);
					$select->where($where);
					$select->columns(array('id','name'));
					$statement = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$fill->units = $run->toArray();

					if(!empty($tagged->unit)) {
						// quesry SELECT id, name FROM `chapters` WHERE deleted=0 AND unit_id=1
						$select = $sql->select();
						$select->from('chapters');
						$where = new $select->where();
						$where->equalTo('deleted',0);
						$where->in('unit_id',$tagged->unit);
						$select->where($where);
						$select->columns(array('id','name'));
						$statement = $sql->getSqlStringForSqlObject($select);

						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$fill->chapters = $run->toArray();

						if(!empty($tagged->chapter)) {
							// quesry SELECT id, name FROM `topics` WHERE deleted=0 AND chapter_id=1
							$select = $sql->select();
							$select->from('topics');
							$where = new $select->where();
							$where->equalTo('deleted',0);
							$where->in('chapter_id',$tagged->chapter);
							$select->where($where);
							$select->columns(array('id','name'));
							$statement = $sql->getSqlStringForSqlObject($select);

							$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
							$fill->topics = $run->toArray();
						}

					}

				}

				// quesry SELECT id, name FROM `tags` WHERE deleted=0
				$select = $sql->select();
				$select->from('tags');
				$where = new $select->where();
				$where->equalTo('delete',0);
				$select->where($where);
				$select->columns(array('id','tag'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$fill->tags = $run->toArray();

				// quesry SELECT id, name FROM `difficulty` WHERE deleted=0
				$select = $sql->select();
				$select->from('difficulty');
				$where = new $select->where();
				$where->equalTo('delete',0);
				$select->where($where);
				$select->columns(array('id','difficulty'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$fill->difficulty = $run->toArray();

				// quesry SELECT id, name FROM `skill` WHERE deleted=0
				$select = $sql->select();
				$select->from('skills');
				$where = new $select->where();
				$where->equalTo('delete',0);
				$select->where($where);
				$select->columns(array('id','skill'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$fill->skills = $run->toArray();



				$result->questionData = $questionData;
				$result->options = $options;
				$result->tagged = $tagged;
				$result->fill = $fill;

				$result->status = 1;
				$result->message = "Operation Successful";

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		/**
		* function to get question which are added under passage based question
		*
		* @author Utkarsh Pandey
		* @date 10-08-2015
		* @param JSON
		* @return jeson with status and all data
		**/
		public function getQuestionsOfPassage($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('questions');
				$where = new $select->where();
				$where->equalTo('delete',0);
				$where->equalTo('parent_id',$data->id);
				// $where->in('id',$class_filtered_question_ids);
				$select->where($where);
				$select->columns(array('id','question','description','type'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$Questions = $run->toArray();

				$result->data = array();
				foreach ($Questions as $quest) {
					$question = new stdClass();
					$select = $sql->select();
					$select->from('options');
					$where = new $select->where();
					$where->equalTo('question_id',$quest["id"]);
					$where->equalTo('deleted',0);
					$select->where($where);
					$select->columns(array('option','answer','number'));
					$statement = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$question->options = $run->toArray();

					$question->question = $quest;
					array_push($result->data, $question);
				}

				$result->status = 1;
				$result->message = 'Successful';

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		/**
		* function to filter questions according to admin
		*
		* @author Utkarsh Pandey
		* @date 30-06-2015
		* @param JSON
		* @return json with status and all data
		**/
		public function filterQuestions($data) {
			date_default_timezone_set('Asia/Kolkata');
			$result = new stdClass();

			try
			{
				$adapter = $this->adapter;

				if(!empty($data->question_id) || !empty($data->question_from_id) || !empty($data->question_to_id)) {
					if($data->question_id != ''){
						$statement = 'select questions.id, questions.question, questions.description, question_types.short_name AS type, (CASE WHEN questions.quality_check = 0 THEN "Pending" WHEN quality_check = 1 THEN "Done" END) AS qc,classes.name as class, (SELECT GROUP_CONCAT(subjects.name) FROM `tagsofquestion` LEFT JOIN subjects ON subjects.id = tagsofquestion.tagged_id WHERE tagsofquestion.tag_type=1 AND tagsofquestion.question_id=questions.id) as subjects, FROM_UNIXTIME(questions.created,"%d-%m-%y %h:%i %p") AS modified FROM questions ';

						$statement .= 'left join classes on questions.class_id=classes.id left join question_types on question_types.id = questions.type WHERE `delete` = 0 AND `questions`.`parent_id` = 0 AND questions.id = '.$data->question_id;

						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$result->data = $run->toArray();

						$result->status = 1;
						$result->query = $statement;
						$result->message = 'Operation Successful';
					}
					if($data->question_from_id != '' && $data->question_to_id != ''){
						$statement = 'select questions.id, questions.question, questions.description, question_types.short_name AS type, (CASE WHEN questions.quality_check = 0 THEN "Pending" WHEN quality_check = 1 THEN "Done" END) AS qc,classes.name as class, (SELECT GROUP_CONCAT(subjects.name) FROM `tagsofquestion` LEFT JOIN subjects ON subjects.id = tagsofquestion.tagged_id WHERE tagsofquestion.tag_type=1 AND tagsofquestion.question_id=questions.id) as subjects, FROM_UNIXTIME(questions.created,"%d-%m-%y %h:%i %p") AS modified FROM questions left join classes on questions.class_id=classes.id left join question_types on question_types.id = questions.type WHERE `delete` = 0 AND `questions`.`parent_id` = 0 AND questions.id >= '.$data->question_from_id.' AND questions.id <= '.$data->question_to_id;

						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$result->data = $run->toArray();

						$result->status = 1;
						$result->query = $statement;
						$result->message = 'Operation Successful';
					}
				}
				else
				{
					$class_filtered_question_ids = array();
					$sql = new Sql($adapter);
					$select = $sql->select();
					$where = new $select->where();
					/* checking that classes are set or not 	*/
					if(!empty($data->classes)) {
						if(!empty($data->subjects)) {
							if(!empty($data->units)) {
								if(!empty($data->chapters)) {
									if(!empty($data->topics)) {
										$where->equalTo('tag_type',4);
										$where->in('tagged_id',$data->topics);
									}
									else{
										$where->equalTo('tag_type',3);
										$where->in('tagged_id',$data->chapters);
									}
								}
								else {
									$where->equalTo('tag_type',2);
									$where->in('tagged_id',$data->units);
								}
							}
							else {
								$where->equalTo('tag_type',1);
								$where->in('tagged_id',$data->subjects);
							}
							$select->from('tagsofquestion');
							$select->columns(array('id'=>'question_id'));
						}
						else {
							$select->from('questions');
							$where->equalTo('parent_id', 0);
							$where->equalTo('class_id',$data->classes);
							$where->equalTo('delete',0);
							$select->columns(array('id'));
						}
						$select->where($where);

						$statement = $sql->getSqlStringForSqlObject($select);
						//die($statement);
						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$questionIdFilteredBYClass = $run->toArray();
						foreach($questionIdFilteredBYClass as $questionId)
							array_push($class_filtered_question_ids, $questionId["id"]);
					}


					/* checking that Tags are set or not 	*/
					if(!empty($data->tags)) {
						$select = $sql->select();
						$select->from('tagsofquestion');
						$where = new $select->where();
						$where->equalTo('tag_type',5);
						$where->in('tagged_id',$data->tags);
						if(!empty($class_filtered_question_ids))
							$where->in('question_id',$class_filtered_question_ids);
						$select->where($where);
						$select->columns(array('question_id'));
						$statement = $sql->getSqlStringForSqlObject($select);
						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$questionIdFilteredByTags = $run->toArray();
						$class_filtered_question_ids = array();
						foreach($questionIdFilteredByTags as $questionId)
							array_push($class_filtered_question_ids, $questionId["question_id"]);
					}


					/* checking that Difficulty-Level are set or not 	*/
					if(!empty($data->difficultyLevel)) {
						$select = $sql->select();
						$select->from('tagsofquestion');
						$where = new $select->where();
						$where->equalTo('tag_type',6);
						$where->in('tagged_id',$data->difficultyLevel);
						if(!empty($class_filtered_question_ids))
							$where->in('question_id',$class_filtered_question_ids);
						$select->where($where);
						$select->columns(array('question_id'));
						$statement = $sql->getSqlStringForSqlObject($select);
						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$questionIdFilteredByDifficulty = $run->toArray();
						$class_filtered_question_ids = array();
						foreach($questionIdFilteredByDifficulty as $questionId)
							array_push($class_filtered_question_ids, $questionId["question_id"]);
					}

					/* checking that skills are set or not 	*/
					if(!empty($data->skills)) {
						$select = $sql->select();
						$select->from('tagsofquestion');
						$where = new $select->where();
						$where->equalTo('tag_type',7);
						$where->in('tagged_id',$data->skills);
						if(!empty($class_filtered_question_ids))
							$where->in('question_id',$class_filtered_question_ids);
						$select->where($where);
						$select->columns(array('question_id'));
						$statement = $sql->getSqlStringForSqlObject($select);
						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$questionIdFilteredBySkills = $run->toArray();
						$class_filtered_question_ids = array();
						foreach($questionIdFilteredBySkills as $questionId)
							array_push($class_filtered_question_ids, $questionId["question_id"]);
					}

					/*$select = $sql->select();
					$select->from('questions');
					$where = new $select->where();
					$where->equalTo('delete',0);
					if(!empty($class_filtered_question_ids))
						$where->in('id',$class_filtered_question_ids);
					if(!empty($class_filtered_question_ids))
							$where->in('type',$data->questionTypes);
					if(!empty($class_filtered_question_ids))
							$where->equalTo('quality_check',$data->quality_check);
					$select->where($where);
					$select->columns(array('id','question','description','type' => new Expression('CASE WHEN `type`= 1 THEN "MCQ" WHEN type=2 then "MAQ" END'),'quality_check','created'));
					$statement = $sql->getSqlStringForSqlObject($select);*/

					$statement = 'select questions.id, questions.question, questions.description, question_types.short_name AS type, (CASE WHEN questions.quality_check = 0 THEN "Pending" WHEN quality_check = 1 THEN "Done" END) AS qc,classes.name as class, (SELECT GROUP_CONCAT(subjects.name) FROM `tagsofquestion` LEFT JOIN subjects ON subjects.id = tagsofquestion.tagged_id WHERE tagsofquestion.tag_type=1 AND tagsofquestion.question_id=questions.id) as subjects, FROM_UNIXTIME(questions.created,"%d-%m-%y %h:%i %p") AS modified FROM questions left join classes on questions.class_id=classes.id left join question_types on question_types.id = questions.type WHERE `delete` = 0 AND parent_id=0';
					if(!empty($class_filtered_question_ids)) {
						$statement .= " AND questions.id IN('".implode("','",$class_filtered_question_ids)."')";
					}
					else if((!empty($data->classes)) || (!empty($data->tags)) || (!empty($data->difficultyLevel)) || (!empty($data->skills))) {
						$statement .= " AND questions.id IN(0)";
					}
					if(!empty($data->questionTypes))
							$statement .= " AND type IN('".implode("','",$data->questionTypes)."')";
					if(isset($data->quality_check))
						$statement .= ' AND quality_check = '.$data->quality_check;
					if(isset($data->text))
						$statement .= ' AND question like "%'.$data->text.'%" ';
					// die($statement);
					// return $statement;

					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$total_questions = count($run->toArray());
					$statement .= ' LIMIT '.$data->limit.',50';
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

					$result->total_questions = $total_questions;
					$result->data = $run->toArray();


					$result->status = 1;
					$result->message = 'Operation Successful.';
				}

				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}



		/**
		* function change multiple questions's quality check
		*
		* @author Utkarsh Pandey
		* @date 10-08-2015
		* @param JSON
		* @return jeson with status and all data
		**/
		public function changeMultipleQC($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;


				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('questions');
				$update->set(array(
					'quality_check'	=> $data->quality_check,
					'user_id'	=> $data->userId,
					'created'	=>	time()
				));
				$where = new $update->where();
				$where->in('id', $data->question_ids);
				$update->where($where);
				$statement = $sql->getSqlStringForSqlObject($update);
				// $statement = 'UPDATE questions SET quality_check = '.$data->quality_check.' WHERE id ='.$data->question_id;

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				// $result->data = $run->toArray();
				$result->status = 1;
				$result->message = 'QC Successfully Updated';

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}



		/**
		* function change questions's quality check
		*
		* @author Utkarsh Pandey
		* @date 10-07-2015
		* @param JSON
		* @return jeson with status and all data
		**/
		public function changeQualityCheck($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;


				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('questions');
				$update->set(array(
					'quality_check'	=> $data->quality_check,
					'created'	=>	time()
				));
				$update->where(array(
					'id' => $data->question_id
				));

				$statement = $sql->getSqlStringForSqlObject($update);
				// $statement = 'UPDATE questions SET quality_check = '.$data->quality_check.' WHERE id ='.$data->question_id;

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				// $result->data = $run->toArray();
				$result->status = 1;
				$result->message = 'QC Successfully Updated';

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		public function updateQuestion($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;


				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('questions');
				$update->set(array(
					'question'	=> $data->question,
					'description'	=> $data->description,
					'created'	=>	time(),
					 'class_id'	=>	$data->class_id
				));
				$update->where(array(
					'id' => $data->question_id
				));

				$statement = $sql->getSqlStringForSqlObject($update);
				// $statement = 'UPDATE questions SET questions = '.$data->questions.' WHERE id ='.$data->question_id;

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$delete = $sql->delete('options');

				$where = new $delete->where();
				$where->equalTo('question_id',$data->question_id);
				$delete->where($where);

				$statement = $sql->getSqlStringForSqlObject($delete);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$insertOptions = "INSERT INTO `options`(`option`,`answer`, `number`, `question_id`, `user_id`, `created`) VALUES ";

				if(!empty($data->options)) {
					foreach($data->options as $opt) {
						if (empty($opt->number)) {
							$opt->number = 0;
						}
						$insertOptions .= "('".$this->safeString($opt->option)."','".$opt->answer."','".$opt->number."',".$data->question_id.",".$data->userId.",".time()."),";
					}
					$optionsQuery = rtrim($insertOptions,',');
					$run = $adapter->query($optionsQuery, $adapter::QUERY_MODE_EXECUTE);
				}

				//new code insert into tagsofquestion
				$delete = $sql->delete('tagsofquestion');

				$where = new $delete->where();
				$where->equalTo('question_id',$data->question_id);
				$delete->where($where);

				$statement = $sql->getSqlStringForSqlObject($delete);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$insertTaggedItem = "INSERT INTO `tagsofquestion`(`question_id`, `tagged_id`, `tag_type`) VALUES ";

				// ADDDING SUBJECT IDS IN INSERT QUERY
				foreach($data->subject_ids as $id) {
					$insertTaggedItem .= "(".$data->question_id.",".$id.",1),";
				}

				// ADDDING UNIT IDS IN INSERT QUERY
				foreach($data->unit_ids as $id) {
					$insertTaggedItem .= "(".$data->question_id.",".$id.",2),";
				}

				// ADDDING CHAPTER IDS IN INSERT QUERY
				foreach($data->chapter_ids as $id) {
					$insertTaggedItem .= "(".$data->question_id.",".$id.",3),";
				}

				// ADDDING TOPIC IDS IN INSERT QUERY
				foreach($data->topic_ids as $id) {
					$insertTaggedItem .= "(".$data->question_id.",".$id.",4),";
				}

				// ADDDING TAG IDS IN INSERT QUERY
				foreach($data->tag_ids as $id) {
					$insertTaggedItem .= "(".$data->question_id.",".$id.",5),";
				}

				// ADDDING DIFFICULTY LEVEL IDS IN INSERT QUERY
				foreach($data->difficultyLevel_ids as $id) {
					$insertTaggedItem .= "(".$data->question_id.",".$id.",6),";
				}

				// ADDDING SKILL IDS IN INSERT QUERY
				foreach($data->skill_ids as $id) {
					$insertTaggedItem .= "(".$data->question_id.",".$id.",7),";
				}

				$queryForTaggedItem = rtrim($insertTaggedItem,',');
				// return $queryForTaggedItem;
				$run = $adapter->query($queryForTaggedItem, $adapter::QUERY_MODE_EXECUTE);

				//end of new code


				$result->status = 1;
				$result->message = 'Question Updated';
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->error = $e->getMessage();
				$result->message = "Error: Question Not Updated.";
			}
		}

		/**
		* function update option data
		*
		* @author Utkarsh Pandey
		* @date 10-07-2015
		* @param JSON
		* @return jeson with status and all data
		**/
		public function updateOption($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;


				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('options');
				$update->set(array(
					'option'	=> $this->safeString($data->option),
					'created'	=>	time()
				));
				$update->where(array(
					'id' => $data->option_id
				));

				$statement = $sql->getSqlStringForSqlObject($update);
				// $statement = 'UPDATE option SET option = '.$data->option.' WHERE id ='.$data->option_id;

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$result->status = 1;
				$result->message = 'Option Updated.';

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* function replaces special character and give a safe string
		*
		* @author Utkarsh Pandey
		* @date 12-07-2015
		* @param string
		* @return sting
		**/
		public function safeString($string) {
			$search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
			$replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
			return str_replace($search, $replace, $string);
		}


		/**
		* function to update tagged-data question
		*
		* @author Utkarsh Pandey
		* @date 23-07-2015
		* @param JSON
		* @return jeson with status and all data
		**/
		public function updateTaggedClass($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$delete = $sql->delete();
				$delete = $sql->delete('tagsofquestion');

				$where = new $delete->where();
				$where->equalTo('question_id',$data->question_id);
				$where->in('tag_type',array(1,2,3,4));
				$delete->where($where);

				$statement = $sql->getSqlStringForSqlObject($delete);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);


				$update = $sql->update();
				$update->table('questions');
				$update->set(array(
					'class_id'	=> $data->class_id,
					'created'	=>	time()
				));


				$update->where(array(
					'id' => $data->question_id
				));

				$statement = $sql->getSqlStringForSqlObject($update);
				// $statement = 'UPDATE questions SET class_id = '.$data->class_id.' WHERE id ='.$data->question_id;

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$result->status = 1;
				$result->message = 'Class Successfully Updated.';

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		/**
		* function to update tagged-data question
		*
		* @author Utkarsh Pandey
		* @date 23-07-2015
		* @param JSON
		* @return jeson with status and all data
		**/
		public function updateTaggedSubject($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$delete = $sql->delete();
				$delete = $sql->delete('tagsofquestion');

				$where = new $delete->where();
				$where->equalTo('question_id',$data->question_id);
				$where->in('tag_type',array(1,2,3,4));
				$delete->where($where);

				$statement = $sql->getSqlStringForSqlObject($delete);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);





				$insert = "INSERT INTO tagsofquestion(question_id, tagged_id, tag_type) VALUES ";

				if(!empty($data->subject_ids)) {
					foreach($data->subject_ids as $id) {
						$insert .= "(".$data->question_id.",".$id.",1),";
					}

					$statement = rtrim($insert,',');
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				}

				$result->status = 1;
				$result->message = 'Successfully Updated.';

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		/**
		* function to update tagged-data question
		*
		* @author Utkarsh Pandey
		* @date 23-07-2015
		* @param JSON
		* @return jeson with status and all data
		**/
		public function updateTaggedUnit($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$delete = $sql->delete();
				$delete = $sql->delete('tagsofquestion');

				$where = new $delete->where();
				$where->equalTo('question_id',$data->question_id);
				$where->in('tag_type',array(2,3,4));
				$delete->where($where);

				$statement = $sql->getSqlStringForSqlObject($delete);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$insert = "INSERT INTO tagsofquestion(question_id, tagged_id, tag_type) VALUES ";


				if(!empty($data->unit_ids)){
					foreach($data->unit_ids as $id) {
						$insert .= "(".$data->question_id.",".$id.",2),";
					}

					$statement = rtrim($insert,',');
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				}
				$result->status = 1;
				$result->message = 'Successfully Updated.';

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		/**
		* function to update tagged-data question
		*
		* @author Utkarsh Pandey
		* @date 23-07-2015
		* @param JSON
		* @return jeson with status and all data
		**/
		public function updateTaggedChapter($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$delete = $sql->delete();
				$delete = $sql->delete('tagsofquestion');

				$where = new $delete->where();
				$where->equalTo('question_id',$data->question_id);
				$where->in('tag_type',array(3,4));
				$delete->where($where);

				$statement = $sql->getSqlStringForSqlObject($delete);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);


				$insert = "INSERT INTO tagsofquestion(question_id, tagged_id, tag_type) VALUES ";


				if(!empty($data->chapter_ids)) {
					foreach($data->chapter_ids as $id) {
						$insert .= "(".$data->question_id.",".$id.",3),";
					}

					$statement = rtrim($insert,',');
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				}

				$result->status = 1;
				$result->message = 'Successfully Updated.';

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		/**
		* function to update tagged-data question
		*
		* @author Utkarsh Pandey
		* @date 23-07-2015
		* @param JSON
		* @return jeson with status and all data
		**/
		public function updateTaggedTopic($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$delete = $sql->delete();
				$delete = $sql->delete('tagsofquestion');

				$where = new $delete->where();
				$where->equalTo('question_id',$data->question_id);
				$where->in('tag_type',array(4));
				$delete->where($where);

				$statement = $sql->getSqlStringForSqlObject($delete);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$insert = "INSERT INTO tagsofquestion(question_id, tagged_id, tag_type) VALUES ";


				if(!empty($data->topic_ids)) {
					foreach($data->topic_ids as $id) {
						$insert .= "(".$data->question_id.",".$id.",4),";
					}

					$statement = rtrim($insert,',');
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				}

				$result->status = 1;
				$result->message = 'Successfully Updated.';

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		/**
		* function to update tagged-data question
		*
		* @author Utkarsh Pandey
		* @date 23-07-2015
		* @param JSON
		* @return jeson with status and all data
		**/
		public function updateTaggedTag($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;

				$sql = new Sql($adapter);

				$delete = $sql->delete();
				$delete = $sql->delete('tagsofquestion');

				$where = new $delete->where();
				$where->equalTo('question_id',$data->question_id);
				$where->in('tag_type',array(5));
				$delete->where($where);

				$statement = $sql->getSqlStringForSqlObject($delete);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$insert = "INSERT INTO tagsofquestion(question_id, tagged_id, tag_type) VALUES ";


				if(!empty($data->tag_ids)) {
					foreach($data->tag_ids as $id) {
						$insert .= "(".$data->question_id.",".$id.",5),";
					}

					$statement = rtrim($insert,',');
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				}

				$result->status = 1;
				$result->message = 'Successfully Updated.';

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		/**
		* function to update tagged-data question
		*
		* @author Utkarsh Pandey
		* @date 23-07-2015
		* @param JSON
		* @return jeson with status and all data
		**/
		public function updateTaggedDifficulty($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;

				$sql = new Sql($adapter);

				$delete = $sql->delete();
				$delete = $sql->delete('tagsofquestion');

				$where = new $delete->where();
				$where->equalTo('question_id',$data->question_id);
				$where->in('tag_type',array(6));
				$delete->where($where);

				$statement = $sql->getSqlStringForSqlObject($delete);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$insert = "INSERT INTO tagsofquestion(question_id, tagged_id, tag_type) VALUES ";


				if(!empty($data->difficultyLevel_ids)) {
					foreach($data->difficultyLevel_ids as $id) {
						$insert .= "(".$data->question_id.",".$id.",6),";
					}

					$statement = rtrim($insert,',');
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				}

				$result->status = 1;
				$result->message = 'Successfully Updated.';

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		/**
		* function to update tagged-data question
		*
		* @author Utkarsh Pandey
		* @date 23-07-2015
		* @param JSON
		* @return jeson with status and all data
		**/
		public function updateTaggedSkill($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;

				$sql = new Sql($adapter);

				$delete = $sql->delete();
				$delete = $sql->delete('tagsofquestion');

				$where = new $delete->where();
				$where->equalTo('question_id',$data->question_id);
				$where->in('tag_type', array(7));
				$delete->where($where);

				$statement = $sql->getSqlStringForSqlObject($delete);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$insertSkills = "INSERT INTO tagsofquestion(question_id, tagged_id, tag_type) VALUES ";


				if(!empty($data->skills)) {
					foreach($data->skills as $id) {
						$insertSkills .= "(".$data->question_id.",".$id.",7),";
					}

					$statement = rtrim($insertSkills,',');
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				}

				$result->status = 1;
				$result->message = 'Successfully Updated.';

				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->error = $e->getMessage();
				return $result;
			}
		}


		/**
		* function send question, solution(description) and its options of questions which ids are given by user
		*
		* @author Utkarsh Pandey
		* @date 19-08-2015
		* @param JSON with question ids array
		* @return jeson with status and all data
		**/
		public function getDataOfQuestions($question) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				/*
				* fetching questions
				*/
				$select = $sql->select();
				$select->from('questions');
				$where = new $select->where();
				$where->equalTo('delete',0);
				// $where->equalTo('quality_check',0);
				$where->in('id',$question->question_ids);
				$select->where($where);
				$select->columns(array('id','question','description','type','parent_id'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$questions = $run->toArray();
				// print_r($questions);
				// die();

				$result->data = array();
				foreach ($questions as $qu) {
					$data = new stdClass();
					$q = array();
					if ($qu['type'] == 11) {
						/*
						* fetching options for every question
						*/
						$select = $sql->select();
						$select->from('questions');
						$where = new $select->where();
						$where->equalTo('delete',0);
						// $where->equalTo('quality_check',0);
						$where->equalTo('parent_id', $qu['id']);
						$select->where($where);
						$select->columns(array('id','question','description','type','parent_id'));
						$statement = $sql->getSqlStringForSqlObject($select);
						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$sub_questions = $run->toArray();

						foreach ($sub_questions as $s_qu) {
							/*
							* fetching options for every question
							*/
							$sq = new stdClass();
							$select = $sql->select();
							$select->from('options');
							$where = new $select->where();
							$where->equalTo('question_id',$s_qu["id"]);
							$select->where($where);
							$select->columns(array('option','answer','number'));
							$statement = $sql->getSqlStringForSqlObject($select);
							$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
							$options = $run->toArray();
							$sq->question = $s_qu['question'];
							$sq->description = $s_qu['description'];
							$sq->type = $s_qu['type'];
							$sq->parent_id = $s_qu['parent_id'];

							require_once ('TestPaperDeliverApi.php');
							$TestPaperDeliverApi = new TestPaperDeliverApi();
							$sq->options = $TestPaperDeliverApi->shuffleOptions($s_qu['type'], $options);
							array_push($q, $sq);
						}

						$data->question = $qu['question'];
						$data->description = $qu['description'];
						$data->type = $qu['type'];
						$data->parent_id = $qu['parent_id'];
						$data->sub_questions = $q;
						array_push($result->data, $data);
					}
					else {
						/*
						* fetching options for every question
						*/
						$select = $sql->select();
						$select->from('options');
						$where = new $select->where();
						$where->equalTo('question_id',$qu["id"]);
						$select->where($where);
						$select->columns(array('option','answer','number'));
						$statement = $sql->getSqlStringForSqlObject($select);
						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$options = $run->toArray();
						$data->question = $qu['question'];
						$data->description = $qu['description'];
						$data->type = $qu['type'];
						require_once ('TestPaperDeliverApi.php');
						$TestPaperDeliverApi = new TestPaperDeliverApi();
						$data->options = $TestPaperDeliverApi->shuffleOptions($qu['type'], $options);
						array_push($result->data, $data);
					}
				}


				$result->status = 1;
				$result->message = 'Successful.';

				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function deleteQuestions($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('questions');
				$update->set(array(
					'delete'	=> 1,
					'user_id'	=> $data->userId
				));
				$where = new $update->where();
				$where->in('id', $data->question_ids);
				$update->where($where);
				$statement = $sql->getSqlStringForSqlObject($update);
				// $statement = 'UPDATE questions SET quality_check = '.$data->quality_check.' WHERE id ='.$data->question_id;

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$result->status = 1;
				$result->message = 'Deleted Successfully Updated';

				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

	}

?>
