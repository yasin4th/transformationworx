<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;
	
	//including connection.php file to make connection with database
	require_once 'Connection.php';
	
	class QuestionBank extends Connection{

		/**
		* Function to redeem coupon
		*
		* @author Utkarsh Pandey
		* @date 31-07-2015
		* @param JSON with all required details
		* @return JSON status and message
		**/
		public function markQuestionEvent($data) {

			$result = new stdClass();

			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$statement = "SELECT * FROM student_question_bank WHERE question_id = $data->question_id AND student_id = $data->userId";
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$rows = $run->toArray();
				
				if (count($rows) == 0) {

					$insert = new TableGateway('student_question_bank', $adapter, null, new HydratingResultSet());
					$insert->insert(array(
						'question_id' => $data->question_id,
						'student_id' => $data->userId,
						$data->markAs => $data->status
					));
					
					$id = $insert->getLastInsertValue();
				}
				else {
					$update = $sql->update();
					$update->table('student_question_bank');
					$update->set(array(
						$data->markAs => $data->status,
						'deleted' => 0
					));
					$update->where(array(
						'question_id' => $data->question_id,
						'student_id' => $data->userId
					));

					$statement = $sql->getSqlStringForSqlObject($update);
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				}
					
				$result->status = 1;
				$result->message = "Successfully Updated";
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}
		
		}


		/**
		* Function to fetch Student Questions
		*
		* @author Utkarsh Pandey
		* @date 31-07-2015
		* @param JSON with all required details
		* @return JSON status and message
		**/
		public function fetchStudentQuestions($data) {

			$result = new stdClass();

			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				$statement = 'select questions.id, questions.question, questions.description, question_types.short_name AS type,classes.name as class, (SELECT GROUP_CONCAT(subjects.name) FROM `tagsofquestion` LEFT JOIN subjects ON subjects.id = tagsofquestion.tagged_id WHERE tagsofquestion.tag_type=1 AND tagsofquestion.question_id=questions.id) as subjects, student_question_bank.revision, student_question_bank.important, student_question_bank.unable_to_solve FROM student_question_bank left join questions ON questions.id = student_question_bank.question_id left join classes on questions.class_id=classes.id left join question_types on question_types.id = questions.type WHERE student_question_bank.student_id = '.$data->userId.' AND student_question_bank.deleted = 0';
				// $statement = 'select questions.id, questions.question, questions.description, question_types.short_name AS type,classes.name as class, (SELECT GROUP_CONCAT(subjects.name) FROM `tagsofquestion` LEFT JOIN subjects ON subjects.id = tagsofquestion.tagged_id WHERE tagsofquestion.tag_type=1 AND tagsofquestion.question_id=questions.id) as subjects FROM questions left join classes on questions.class_id=classes.id left join question_types on question_types.id = questions.type WHERE questions.id in (SELECT question_id FROM student_question_bank WHERE student_id = '.$data->userId .' AND deleted = 0)';

				// rtrim((str_replace(" ,","", $questionWiseReportRow['units'])), ', ')
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$rows = $run->toArray();
				
				$result->questions = $rows;
				$result->status = 1;
				$result->message = "Successfully Updated";
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}
		}

	

		/**
		* Function to fetch Student Questions
		*
		* @author Utkarsh Pandey
		* @date 31-07-2015
		* @param JSON with all required details
		* @return JSON status and message
		**/
		public function deleteQuestionFromMyQuestionBank($data) {

			$result = new stdClass();

			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				$update = $sql->update();
					$update->table('student_question_bank');
					$update->set(array(
						'deleted' => 1
					));
					$update->where(array(
						'question_id' => $data->question_id,
						'student_id' => $data->userId
					));

					$statement = $sql->getSqlStringForSqlObject($update);
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				
				$result->status = 1;
				$result->message = "Successfully Deleted";
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}
		}
	


		/**
		* Function to fetch Student Difficulties
		*
		* @author Utkarsh Pandey
		* @date 01-08-2015
		* @param JSON with all required details
		* @return JSON status and message
		**/
		public function fetchStudentDifficulties($data) {

			$result = new stdClass();

			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$statement = "SELECT difficulty.difficulty, difficulty.id FROM student_question_bank LEFT JOIN tagsofquestion on tagsofquestion.question_id = student_question_bank.question_id LEFT JOIN difficulty on difficulty.id = tagsofquestion.tagged_id WHERE student_question_bank.student_id =  $data->userId AND student_question_bank.deleted = 0 AND tagsofquestion.tag_type = 7 group by tagged_id";
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$rows = $run->toArray();

				$result->difficulties = $rows;
				$result->status = 1;
				$result->message = "Successfully Fetched";
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}
		}



		/**
		* Function to create Student TestPaper
		*
		* @author Utkarsh Pandey
		* @date 02-08-2015
		* @param JSON with all required details
		* @return JSON status and message
		**/
		public function createStudentTestPaper($data) {

			$result = new stdClass();

			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				// return $data;

				$statement = "SELECT student_question_bank.question_id FROM student_question_bank WHERE student_question_bank.student_id = $data->userId";
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$students_questions = $run->toArray();
				$students_questions_ids = array();
				foreach ($students_questions as $key => $id) {
					array_push($students_questions_ids, $id['question_id']);
				}


				$select = $sql->select();
				$select->columns(array('question_id'));
				$select->from('tagsofquestion');
				$where = new $select->where();
				$where->equalTo('tag_type', 3);
				$where->in('tagged_id', $data->chapter);
				$where->in('question_id', $students_questions_ids);
				$select->where($where);
				// $select->limit($data->numberOfQuestion);
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$run = $run->toArray();

				$questions_ids = array();
				foreach ($run as $key => $id) {
					array_push($questions_ids, $id['question_id']);
				}


				$select = $sql->select();
				$select->columns(array('question_id'));
				$select->from('tagsofquestion');
				$where = new $select->where();
				$where->equalTo('tag_type', 6);
				$where->in('tagged_id', $data->difficulty);
				$where->in('question_id', $questions_ids);
				$select->where($where);
				$select->limit($data->numberOfQuestion);
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$questions = $run->toArray();

				$questions_ids = array();
					
				/****************************************************/
				$insert_test_paper = new TableGateway('test_paper', $adapter, null, new HydratingResultSet());
				if (empty($data->test_paper_start)) {
					$data->test_paper_start = 1;
				}
				if (empty($data->test_paper_end)) {
					$data->test_paper_end = 0;
				}
				$test_paper_details = array(
					'name' => "Practice Test",
					'type'	=>	0,
					'time'	=>	$data->time,
					// 'Instructions'	=>	$data->test_paper_instructions,
					'status'	=>	1,
					'user_id'	=>	$data->userId,
					'created'	=>	time()
				);
				$insert_test_paper->insert($test_paper_details);

				$test_paper_id = $insert_test_paper->getLastInsertValue();
				
				$insert_section = new TableGateway('section', $adapter, null, new HydratingResultSet());
				$insert_section->insert(array(
					'name'	=>	'Section 1',
					'total_question'	=>	count($questions),
					'test_paper'	=>	$test_paper_id
				));
				$section_id = $insert_section->getLastInsertValue();

				foreach($questions as $question) {
					$insert = new TableGateway('test_paper_questions', $adapter, null, new HydratingResultSet());
					if (empty($data->max_marks)) {
						$data->max_marks = 1;
					}
					if (empty($data->neg_marks)) {
						$data->neg_marks = 0;
					}

					$insert->insert(array(
						'test_paper_id'	=>	$test_paper_id,
						'section_id'	=>	$section_id,
						'question_id'	=>	$question['question_id'],
						'max_marks'	=>	$data->max_marks,
						'neg_marks'	=>	$data->neg_marks,
						'created'	=>	time()

					));
					$id = $insert->getLastInsertValue();
				}

				$result->test_paper_id = $test_paper_id;

				


				/*$no_of_chap = count($data->chapter);
				$no_of_diff = count($data->difficulty);
				$qu_in_chap = (($data->numberOfQuestion % $no_of_chap) == 0) ? $data->numberOfQuestion / $no_of_chap: intval($data->numberOfQuestion / $no_of_chap) + 1;
				$qu_in_diff = ((count($qu_in_chap) % $no_of_diff) == 0) ? count($qu_in_chap) / $no_of_diff: (intval(count($qu_in_chap) / $no_of_diff)) + 1;
				
				$prev = 0;
				$qu_for_test = array();
				foreach ($data->chapter as $key => $chapter) {
					
					foreach ($data->difficulty as $key => $difficulty) {
							
						$select = $sql->select();
						$select->columns(array('question_id'));
						$select->from('tagsofquestion');
						$where = new $select->where();
						$where->equalTo('tag_type', 6);
						$where->equalTo('tagged_id', $difficulty);
						$where->in('question_id', $students_questions_ids);
						$select->where($where);
						$statement = $sql->getSqlStringForSqlObject($select);
						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$run = $run->toArray();

						$diff_qu = array();
						foreach ($run as $key => $id) {
							array_push($diff_qu, $id['question_id']);
						}

						foreach ($students_questions_ids as $key => $id) {
							array_push($diff_qu, $id);
						}
						

						$select = $sql->select();
						$select->columns(array('question_id'));
						$select->from('tagsofquestion');
						$where = new $select->where();
						$where->equalTo('tag_type', 3);
						$where->equalTo('tagged_id', $chapter);
						$where->in('question_id', $diff_qu);
						$select->where($where);
						$select->limit($qu_in_chap + $qu_in_diff + $prev);
						$statement = $sql->getSqlStringForSqlObject($select);
						// die($statement);
						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$run = $run->toArray();

						$prev = (count($run) == $qu_in_chap + $qu_in_diff + $prev) ? 0 : (($qu_in_chap + $qu_in_diff + $prev) - count($run));
						foreach ($run as $key => $id) {
							array_push($qu_for_test, $id['question_id']);
						}
					}		
				}
				die(print_r($qu_for_test));*/


				$result->status = 1;
				$result->message = "Test Paper Genrated.";
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}

	
		}



	}