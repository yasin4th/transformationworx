<?php
	session_start();
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;

	//including connection.php file to make connection with database
	require_once 'Connection.php';
	date_default_timezone_set('Asia/Kolkata');

	class PlanApi extends Connection{

		/**
		* Function fetchPlanClasses for the planning of the schedule
		*
		* @author Pushpam Matah
		* @date 19-06-2015
		* @param JSON
		* @return JSON status and data(classes names)
		**/

		public function fetchPlanClasses() {
			date_default_timezone_set('Asia/Kolkata');
			$result = new stdClass();

			try{

				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('classes');
				$select->where(array(
					'user_id'	=>	1 //put session id only
				));

				$select->columns(array('name' ,'id'));

				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				if($res)
				$result->status = 1;
				$result->data = $res;
				return $result;

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}

		/**
		* Function fetchPlanSubjects for the planning of the schedule
		*
		* @author Pushpam Matah
		* @date 19-06-2015
		* @param JSON
		* @return JSON status and data(subject names of the respective classes)
		**/


		public function fetchPlanSubjects($data) {
			date_default_timezone_set('Asia/Kolkata');
			$result = new stdClass();

			try{

				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subjects');
				$select->where(array(
					'class_id'	=>	$data->class_id
				));

				$select->columns(array('name','id'));

				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				if($res)
				$result->status = 1;
				$result->data = $res;
				return $result;

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function getPlannerDetails($data)
		{
			$result = new stdClass();

			try{

				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();

				$select->from('planner');

             	$select->where(array(
             		'user_id' => $_SESSION['userId'],
					'subject_id'	=>	$data->subject_id
				));

				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				if($res)
				$result->status = 1;
				$result->data = $res;
				return $result;

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* Function findingHours for the planning of the schedule
		*
		* @author Pushpam Matah
		* @date 21-06-2015
		* @param JSON
		* @return JSON status and data(recommended hours and minutes or error response)
		**/


		public function findingHours($data) {
			date_default_timezone_set('Asia/Kolkata');
			$result = new stdClass();

			try{
				$newSchoolHolidays = array();

				$res1 = PlanApi::getPlannerDetails($data);

				if(count($res1->data)!=0 && $data->editValue == 0)
				{
					$res = "The subject you are trying to plan has already been planned";
					$result->data = $res;
					$result->status = 0;
					return $result;
				}
				elseif($data->startDate == "" || $data->targetDate == "" || $data->subject_id == "" || $data->dailyOrAlternate == "")
				{
					$res = "Some fields are required";
					$result->data = $res;
					$result->status = 0;
					return $result;
				}
				else
				{

				$res = PlanApi::fetchPlanChaptersTime($data);

				$startDate = $data->startDate;
				$targetDate = $data->targetDate;
				$offDay = $data->offDay;
				$schoolHoliday = explode(',',$data->schoolHoliday);
				$dailyOrAlternate = $data->dailyOrAlternate;

				$finalStartDate = new DateTime($startDate);
				$finalTargetDate = new DateTime($targetDate);

				for($mk=strtotime($startDate);$mk<=strtotime($targetDate);$mk+=86400)
				{
					$uncle[] = date('d-m-Y',$mk);
				}


				for($i=0;$i<count($schoolHoliday);$i++)
				{
					if(in_array($schoolHoliday[$i],$uncle))
					{
						$newSchoolHolidays[] = $schoolHoliday[$i];
					}
				}


				$allDaysCount = $finalTargetDate->diff($finalStartDate)->format("%a") + 1;
				$newOffDays = explode(',',$offDay);

				//$studyDaysCount = $allDaysCount - count($newOffDays);

				require_once 'Events.php';
				//$study = Events::getStudyDays($startDate,$targetDate,$newOffDays,false);


				$sumOfHours = 0;

				for($i=0;$i<count($res->data);$i++)
				{
					$sumOfHours = $sumOfHours + $res->data[$i]['time'];
				}


				if($dailyOrAlternate == "alternate")
				{
					$study = Events::getStudyDays($startDate, $targetDate, $newOffDays, false);
				}else{
					$study = Events::getStudyDays($startDate, $targetDate, $newOffDays, true);
				}
				$studyDaysCount = count($study);

				$recommendedTime = $sumOfHours / $studyDaysCount;
				$recommendedHours = floor($recommendedTime);
				$recommendedMinutes = round(60 * ($recommendedTime - $recommendedHours));


				if(count(array_intersect($newOffDays, $newSchoolHolidays)) != 0 && count($newOffDays)-1 != 0 && count($newSchoolHolidays)-1 != 0)
				{
					$res = "There is a clash between the dates you provided about your school holidays and off days.";
					$result->status = 0;
					$result->data = $res;
				}
				elseif($allDaysCount < 30)
				{
					$res = "A minimum time period of at least 30 days is required to make the calendar";
					$result->status = 0;
					$result->data = $res;
				}
				elseif(strtotime($startDate) > strtotime($targetDate))
				{
					$res = "Your target date must be greater than your start date";
					$result->status = 0;
					$result->data = $res;
				}
				else
				{
				$result->status = 1;
				$result->recommendedHours = $recommendedHours;
				$result->recommendedMinutes = $recommendedMinutes;

				}

				return $result;
			}

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}

		/**
		* Function fetchPlanChaptersTime for the planning of the schedule
		*
		* @author Pushpam Matah
		* @date 22-06-2015
		* @param JSON
		* @return JSON status and data(get chapters and time to study from database)
		**/


		public function fetchPlanChaptersTime($data) {

			$result = new stdClass();

			try{

				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();

				$select->from(array('c' => 'chapters'),array())
             			->join(array('u' => 'units'),
                    		'c.unit_id = u.id', array());

             	$select->where(array(
					'u.subject_id'	=>	$data->subject_id
				));

				$select->columns(array('time','name'));

				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				if($res)
				$result->status = 1;
				$result->data = $res;
				return $result;

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}

		/**
		* Function calendar for the planning of the schedule
		*
		* @author Pushpam Matah
		* @date 28-06-2015
		* @return JSON status and data(populates the calendar when user logs in based upon his previous study plans)
		**/


		public function calendar() {
			date_default_timezone_set('Asia/Kolkata');

			if(isset($_SESSION['userId']))
			{
				$calendarData = new stdClass();

				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();

				$select->from(array('s' => 'subjects'),array('id'))
             			->join(array('p' => 'planner'),
                    		's.id = p.subject_id', array('*'));

				$select->where(array(
					'p.user_id'	=>	$_SESSION['userId']
				));


				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				foreach($res as $myResult)
				{
					$subjectId[] = $myResult['id'];
					$subjectName[] = $myResult['name'];
				}

				if(count($res) == 0) {
					$calendarData->status = 0;
					$calendarData->planner = "No results";
				} else {

					$calendarStatus = 1;

					foreach($res as $result)
					{
						$calendarData->subjectName = $subjectName;
						$calendarData->subjectId[] = $subjectId;
						$calendarData->startDate[] = $result['startDate'];
						$calendarData->targetDate[] = $result['targetDate'];
						$calendarData->offDay[] = $result['offDays'];
						$calendarData->schoolHoliday[] = $result['schoolHolidays'];
						$calendarData->dailyOrAlternate[] = $result['dailyOrAlternate'];
						$calendarData->schoolDaysFrom[] = explode(',',$result['schoolDaysSlotsFrom']);
						$calendarData->schoolDaysTo[] = explode(',',$result['schoolDaysSlotsTo']);
						$calendarData->schoolHolidaysFrom[] = explode(',',$result['schoolHolidaysSlotsFrom']);
						$calendarData->schoolHolidaysTo[] = explode(',',$result['schoolHolidaysSlotsTo']);
						$calendarData->alloted[] = explode(',',$result['alloted']);
						$calendarData->chapters[] = explode(',',$result['chapters']);
						$calendarData->rectimes[] = explode(',',$result['rectimes']);
						$calendarData->lc[] = explode(',',$result['lc']);
						$calendarData->pq[] = explode(',',$result['pq']);
						$calendarData->tr[] = explode(',',$result['tr']);
					}
					$final = PlanApi::fetchCalendar1($calendarData,$calendarStatus);
					$calendarData->planner = $final;
					$calendarData->status = 1;
				}
			}
			else
			{
				$final = "done";
			}

			return $calendarData;
		}


		/**
		* Function populateCalendar for the planning of the schedule
		*
		* @author Pushpam Matah
		* @date 30-06-2015
		* @param JSON
		* @return JSON status and data(populates the calendar after the "submit" button is pressed)
		**/


		public function populateCalendar($data)
		{

			date_default_timezone_set('Asia/Kolkata');
			$new_result = new stdClass();

			try{

				$calendarData = new stdClass();

				$adapter = $this->adapter;


				$sql = new Sql($adapter);
				$select1 = $sql->select();
				$select1->from('subjects');
				$select1->where(array(
					'id'	=>	$data->subject_id
				));

				$selectString1 = $sql->getSqlStringForSqlObject($select1);
				$run1 = $adapter->query($selectString1, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res1 = $run1->toArray();

				$calendarStatus = 2;

				$calendarData->subjectName[] = $res1[0]['name'];
				$calendarData->subjectId[] = $data->subject_id;
				$calendarData->startDate[] = $data->startDate;
				$calendarData->targetDate[] = $data->targetDate;

				$calendarData->offDay[] = $data->offDay;
				$calendarData->schoolHoliday[] = $data->schoolHoliday;
				$calendarData->dailyOrAlternate[] = $data->dailyOrAlternate;
				$calendarData->schoolDaysFrom[] = $data->schoolDaysFrom;
				$calendarData->schoolDaysTo[] = $data->schoolDaysTo;
				$calendarData->schoolHolidaysFrom[] = $data->schoolHolidaysFrom;
				$calendarData->schoolHolidaysTo[] = $data->schoolHolidaysTo;
				$calendarData->alloted[] = $data->alloted;
				$calendarData->chapters[] = $data->chapters;
				$calendarData->rectimes[] = $data->rectimes;
				$calendarData->lc[] = $data->lc;
				$calendarData->pq[] = $data->pq;
				$calendarData->tr[] = $data->tr;

				$final = PlanApi::fetchCalendar1($calendarData,$calendarStatus);
				$calendarData->planner = $final;
				$calendarData->status = 1;
				return $calendarData;


				}catch(Exception $e ){
				$new_result->status = 0;
				$new_result->message = $e->getMessage();
				return $new_result;
			}
		}



		/**
		* Function fetchCalendar for the planning of the schedule
		*
		* @author Pushpam Matah
		* @date 29-06-2015
		* @param JSON
		* @return JSON status and data(internal logic of calendar population)
		**/

		public function fetchCalendar($data,$calendarStatus)
		{
			//_print_r($calendarStatus);
			date_default_timezone_set('Asia/Kolkata');
			$new_result = new stdClass();
			$latest_array = array();
			$adapter = $this->adapter;

			$flag = 0;

			$colors = array("#3a87ad","#C7408B","#35aa47","red","orange");

			$adapter = $this->adapter;

			$sql = new Sql($adapter);
			$select = $sql->select();

			$select->from('planner');

         	$select->where(array(
         		'user_id' => $_SESSION['userId'],

			));

			$selectString = $sql->getSqlStringForSqlObject($select);
			$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
			$res = $run->toArray();
			//_print_r($res);

			foreach($res as $google)
			{
				$abc = explode(',',$google['offDays']);
				for($i=0;$i<count($abc);$i++)
				{
					if(!in_array($abc[$i],$latest_array))
					{
						array_push($latest_array,$abc[$i]);
					}
				}
			}


			for($man=0;$man<count($data->startDate);$man++) {

				$startDate[$man] = $data->startDate[$man];
				$targetDate[$man] = $data->targetDate[$man];
				$dailyOrAlternate[$man] = $data->dailyOrAlternate[$man];
				$subjectName[$man] = $data->subjectName[$man];

				$subjectId[$man] = $data->subjectId[$man];

				$alloted[$man] = $data->alloted[$man];
				$rectimes[$man] = $data->rectimes[$man];
				$chapters[$man] = $data->chapters[$man];
				$lc[$man] = $data->lc[$man];
				$pq[$man] = $data->pq[$man];
				$tr[$man] = $data->tr[$man];


				$offDay[$man] = $data->offDay[$man];
				$schoolHoliday[$man] = $data->schoolHoliday[$man];
				$newOffDays[$man] = explode(',',$offDay[$man]);
				$newSchoolHolidays[$man] = explode(',',$schoolHoliday[$man]);

				for($m=strtotime($startDate[$man]);$m<=strtotime($targetDate[$man])+86400;$m+=86400)
				{
					//_print_r(date('l',$m));
					if(date('l',$m) == "Sunday" && !in_array(date('d-m-Y',$m),$newSchoolHolidays[$man]) && !in_array(date('d-m-Y',$m),$newOffDays[$man]))
					{
						array_push($newSchoolHolidays[$man],date('d-m-Y',$m));
					}
				}


				$dailyOrAlternate[$man] = $data->dailyOrAlternate[$man];

				$finalStartDate[$man] = new DateTime($startDate[$man]);
				$finalTargetDate[$man] = new DateTime($targetDate[$man]);
				$finalTargetDate[$man]->add(new DateInterval('P1D'));
				$allDaysCount[$man] = $finalTargetDate[$man]->diff($finalStartDate[$man])->format("%a");
				$studyDaysCount[$man] = $allDaysCount[$man] - count($newOffDays[$man]);
				//_print_r($allDaysCount[$man]);


				$counter = 86400;

				$schoolDaysFrom[$man] = $data->schoolDaysFrom[$man];
				$schoolDaysTo[$man] = $data->schoolDaysTo[$man];
				$schoolHolidaysFrom[$man] = $data->schoolHolidaysFrom[$man];
				$schoolHolidaysTo[$man] = $data->schoolHolidaysTo[$man];


				$schoolDaysHours[$man] = 0;
				$schoolHolidaysHours[$man] = 0;

				for($i=0;$i<count($schoolDaysFrom[$man]);$i++)
				{
					if(substr($schoolDaysFrom[$man][$i],-2) == "PM" && substr($schoolDaysTo[$man][$i],-2) == "AM" )
						$schoolDaysHours[$man] = $schoolDaysHours[$man] + (abs((strtotime("2015-06-30 ".$schoolDaysTo[$man][$i])-strtotime("2015-06-29 ".$schoolDaysFrom[$man][$i]))/3600));
					else
						$schoolDaysHours[$man] = $schoolDaysHours[$man] + (abs((strtotime($schoolDaysTo[$man][$i])-strtotime($schoolDaysFrom[$man][$i]))/3600));


				}

				for($i=0;$i<count($schoolHolidaysFrom[$man]);$i++)
				{
					if(substr($schoolHolidaysFrom[$man][$i],-2) == "PM" && substr($schoolHolidaysTo[$man][$i],-2) == "AM" )
						$schoolHolidaysHours[$man] = $schoolHolidaysHours[$man] + (abs((strtotime("2015-06-30 ".$schoolHolidaysTo[$man][$i])-strtotime("2015-06-30 ".$schoolHolidaysFrom[$man][$i]))/3600));
					else
						$schoolHolidaysHours[$man] = $schoolHolidaysHours[$man] + (abs((strtotime($schoolHolidaysTo[$man][$i])-strtotime($schoolHolidaysFrom[$man][$i]))/3600));

				}
				//_print_r($schoolDaysHours[$man]);
				//_print_r($schoolHolidaysHours[$man]);


				$sumOfHours[$man] = 0;

				for($i=0;$i<count($rectimes[$man]);$i++)
				{
					$sumOfHours[$man] = $sumOfHours[$man] + $rectimes[$man][$i];
				}

				if($dailyOrAlternate[$man] == "alternate")
				{
					$studyDaysCount[$man] = ceil($studyDaysCount[$man] / 2);
					$counter = 86400*2;

				}

				for ($i=strtotime($startDate[$man]); $i<=strtotime($targetDate[$man]); $i+=$counter) {
    					$allDays[$man][] = date('d-m-Y',$i);
				}
				//_print_r($allDays[$man]);


				$allDaysMinusSchoolHolidays[$man] = array_diff($allDays[$man],$newSchoolHolidays[$man]);
				$newSchoolDays[$man] = array_diff($allDaysMinusSchoolHolidays[$man],$newOffDays[$man]);

				$totalSlotHours[$man] = ($schoolDaysHours[$man] * count($newSchoolDays[$man])) + ($schoolHolidaysHours[$man] * count($newSchoolHolidays[$man]));


				for($k=0;$k<count($alloted[$man]);$k++)
				{
					if(($lc[$man][$k] + $pq[$man][$k] + $tr[$man][$k]) != $alloted[$man][$k])
					{
						$flag++;
					}
				}

				if(array_sum($alloted[$man]) > $totalSlotHours[$man])
				{
					$new_result->status = 2;
					$new_result->data = "The sum of total calculated hours according to the time slots that are provided by the user during the given duration of the plan are less than the allotted time";
				}
				elseif($flag != 0)
				{
					$new_result->status = 3;
					$new_result->data = "The sum of LC, PQ and TR are not equal to the alloted times";
				}
				else
				{
					$j=0;

					for($i=0;$i<count($alloted[$man]);$i++)
					{

						while($lc[$man][$i] > 0 && $j < count($allDays[$man]))
						{

							if(in_array($allDays[$man][$j],$newSchoolDays[$man]) && !in_array($allDays[$man][$j],$latest_array))
							{
									for($p=0;$p<count($schoolDaysFrom[$man]);$p++)
									{
									$time_from[$man][$j][$p] =  date("H:i:s", strtotime($schoolDaysFrom[$man][$p]));
									$time_to[$man][$j][$p] =  date("H:i:s", strtotime($schoolDaysTo[$man][$p]));
									}
									$timer_text[$man][$j] = "LC";
									$timer[$man][$j] = $lc[$man][$i];
									$lc[$man][$i] = $lc[$man][$i] - $schoolDaysHours[$man];
									$subject[$man][$j] = $subjectName[$man];
									$chapter[$man][$j] = $chapters[$man][$i];
									$color[$man][$j] = $colors[$man];
							}

							else if(in_array($allDays[$man][$j],$newSchoolHolidays[$man]) && !in_array($allDays[$man][$j],$latest_array))
							{
								for($p=0;$p<count($schoolHolidaysFrom[$man]);$p++)
								{
								$time_from[$man][$j][$p] = date("H:i:s", strtotime($schoolHolidaysFrom[$man][$p]));
								$time_to[$man][$j][$p] =  date("H:i:s", strtotime($schoolHolidaysTo[$man][$p]));
								}
								$timer_text[$man][$j] = "LC";
								$timer[$man][$j] = $lc[$man][$i];
								$lc[$man][$i] = $lc[$man][$i] - $schoolHolidaysHours[$man];
								$subject[$man][$j] = $subjectName[$man];
								$chapter[$man][$j] = $chapters[$man][$i];
								$color[$man][$j] = "#C7408B";

							}
							else
							{
								for($p=0;$p<1;$p++)
								{
								$time_from[$man][$j][$p] = date("H:i:s", strtotime($newOffDays[$man][$p]));
								$time_to[$man][$j][$p] =  date("H:i:s", strtotime($newOffDays[$man][$p]));
								}
								$subject[$man][$j] = "Its your offday !!";
								$chapter[$man][$j] = "";
								$color[$man][$j] = "#675B5B";
							}

							$start[$man][$j] = $allDays[$man][$j];
							$end[$man][$j] = $allDays[$man][$j];
							$j++;

						}


						while($pq[$man][$i] > 0 && $j < count($allDays[$man]))
						{

							if(in_array($allDays[$man][$j],$newSchoolDays[$man]) && !in_array($allDays[$man][$j],$latest_array))
							{
								for($p=0;$p<count($schoolDaysFrom[$man]);$p++)
								{
								$time_from[$man][$j][$p] =  date("H:i:s", strtotime($schoolDaysFrom[$man][$p]));
								$time_to[$man][$j][$p] =  date("H:i:s", strtotime($schoolDaysTo[$man][$p]));
								}
								$timer_text[$man][$j] = "PQ";
								$timer[$man][$j] = $pq[$man][$i];
								$pq[$man][$i] = $pq[$man][$i] - $schoolDaysHours[$man];
								$subject[$man][$j] = $subjectName[$man];
								$chapter[$man][$j] = $chapters[$man][$i];
								$color[$man][$j] = $colors[$man];
							}

							else if(in_array($allDays[$man][$j],$newSchoolHolidays[$man]) && !in_array($allDays[$man][$j],$latest_array))
							{
								for($p=0;$p<count($schoolHolidaysFrom[$man]);$p++)
								{
								$time_from[$man][$j][$p] = date("H:i:s", strtotime($schoolHolidaysFrom[$man][$p]));
								$time_to[$man][$j][$p] =  date("H:i:s", strtotime($schoolHolidaysTo[$man][$p]));
								}
								$timer_text[$man][$j] = "PQ";
								$timer[$man][$j] = $pq[$man][$i];
								$pq[$man][$i] = $pq[$man][$i] - $schoolHolidaysHours[$man];
								$subject[$man][$j] = $subjectName[$man];
								$chapter[$man][$j] = $chapters[$man][$i];
								$color[$man][$j] = "#C7408B";

							}
							else
							{
								for($p=0;$p<1;$p++)
								{
								$time_from[$man][$j][$p] = date("H:i:s", strtotime($newOffDays[$man][$p]));
								$time_to[$man][$j][$p] =  date("H:i:s", strtotime($newOffDays[$man][$p]));
								}
								$subject[$man][$j] = "Its your offday !!";
								$chapter[$man][$j] = "";
								$color[$man][$j] = "#675B5B";
							}

							$start[$man][$j] = $allDays[$man][$j];
							$end[$man][$j] = $allDays[$man][$j];
							$j++;

						}


						while($tr[$man][$i] > 0 && $j < count($allDays[$man]))
						{

							if(in_array($allDays[$man][$j],$newSchoolDays[$man]) && !in_array($allDays[$man][$j],$latest_array))
							{
									for($p=0;$p<count($schoolDaysFrom[$man]);$p++)
									{
									$time_from[$man][$j][$p] =  date("H:i:s", strtotime($schoolDaysFrom[$man][$p]));
									$time_to[$man][$j][$p] =  date("H:i:s", strtotime($schoolDaysTo[$man][$p]));
									}
									$timer_text[$man][$j] = "TR";
									$timer[$man][$j] = $tr[$man][$i];
									$tr[$man][$i] = $tr[$man][$i] - $schoolDaysHours[$man];
									$subject[$man][$j] = $subjectName[$man];
									$chapter[$man][$j] = $chapters[$man][$i];
									$color[$man][$j] = $colors[$man];
							}

							else if(in_array($allDays[$man][$j],$newSchoolHolidays[$man]) && !in_array($allDays[$man][$j],$latest_array))
							{
									for($p=0;$p<count($schoolHolidaysFrom[$man]);$p++)
									{
									$time_from[$man][$j][$p] = date("H:i:s", strtotime($schoolHolidaysFrom[$man][$p]));
									$time_to[$man][$j][$p] =  date("H:i:s", strtotime($schoolHolidaysTo[$man][$p]));
									}
									$timer_text[$man][$j] = "TR";
									$timer[$man][$j] = $tr[$man][$i];
									$tr[$man][$i] = $tr[$man][$i] - $schoolHolidaysHours[$man];
									$subject[$man][$j] = $subjectName[$man];
									$chapter[$man][$j] = $chapters[$man][$i];
									$color[$man][$j] = "#C7408B";

							}
							else
							{
									for($p=0;$p<1;$p++)
									{
									$time_from[$man][$j][$p] = date("H:i:s", strtotime($newOffDays[$man][$p]));
									$time_to[$man][$j][$p] =  date("H:i:s", strtotime($newOffDays[$man][$p]));
									}
									$subject[$man][$j] = "Its your offday !!";
									$chapter[$man][$j] = "";
									$color[$man][$j] = "#675B5B";
							}

							$start[$man][$j] = $allDays[$man][$j];
							$end[$man][$j] = $allDays[$man][$j];
							$j++;

						}

					}


					if($calendarStatus == 2) {

						$insert = new TableGateway('planner', $adapter, null, new HydratingResultSet());
						$insert->insert(array(
							'user_id'	=>	$_SESSION['userId'],
							'subject_id' =>	htmlentities($data->subjectId[$man]),
							'startDate'	=>	htmlentities($data->startDate[$man]),
							'targetDate'	=>	htmlentities($data->targetDate[$man]),
							'offDays'	=>	htmlentities($data->offDay[$man]),
							'schoolHolidays'	=>	htmlentities($data->schoolHoliday[$man]),
							'dailyOrAlternate'	=>	htmlentities($data->dailyOrAlternate[$man]),
							'schoolDaysSlotsFrom'	=>	htmlentities(implode(',',$data->schoolDaysFrom[$man])),
							'schoolDaysSlotsTo'	=>	htmlentities(implode(',',$data->schoolDaysTo[$man])),
							'schoolHolidaysSlotsFrom'	=>	htmlentities(implode(',',$data->schoolHolidaysFrom[$man])),
							'schoolHolidaysSlotsTo'	=>	htmlentities(implode(',',$data->schoolHolidaysTo[$man])),
							'chapters' => htmlentities(implode(',',$data->chapters[$man])),
							'rectimes' => htmlentities(implode(',',$data->rectimes[$man])),
							'alloted'	=>	htmlentities(implode(',',$data->alloted[$man])),
							'lc' => htmlentities(implode(',',$data->lc[$man])),
							'pq' => htmlentities(implode(',',$data->pq[$man])),
							'tr' => htmlentities(implode(',',$data->tr[$man]))

							));

					}

					$new_result->status = 4;
					$new_result->time_from = $time_from;
					$new_result->time_to = $time_to;
					$new_result->subject = $subject;
					$new_result->chapter = $chapter;
					$new_result->start = $start;
					$new_result->end = $end;
					$new_result->color = $color;
					$new_result->timer = $timer;
					$new_result->timer_text = $timer_text;

				}
					// else
					// {
					// $j=0;

					// 	for($i=0;$i<count($alloted[$man]);$i++)
					// 	{

					// 			while($alloted[$man][$i] > 0 && $j < count($allDays[$man]))
					// 			{

					// 					if(in_array($allDays[$man][$j],$newSchoolDays[$man]) && !in_array($allDays[$man][$j],$latest_array))
					// 					{
					// 							for($p=0;$p<count($schoolDaysFrom[$man]);$p++)
					// 							{
					// 							$time_from[$man][$j][$p] =  date("H:i:s", strtotime($schoolDaysFrom[$man][$p]));
					// 							$time_to[$man][$j][$p] =  date("H:i:s", strtotime($schoolDaysTo[$man][$p]));
					// 							}
					// 							$lc_new[$man][$j] = $lc[$man][$i];
					// 							$pq_new[$man][$j] = $pq[$man][$i];
					// 							$tr_new[$man][$j] = $tr[$man][$i];
					// 							$subject[$man][$j] = $subjectName[$man];
					// 							$chapter[$man][$j] = $chapters[$man][$i];
					// 							$alloted[$man][$i] = $alloted[$man][$i] - $schoolDaysHours[$man];
					// 							$color[$man][$j] = $colors[$man];
					// 					}

					// 					else if(in_array($allDays[$man][$j],$newSchoolHolidays[$man]) && !in_array($allDays[$man][$j],$latest_array))
					// 					{
					// 							for($p=0;$p<count($schoolHolidaysFrom[$man]);$p++)
					// 							{
					// 							$time_from[$man][$j][$p] = date("H:i:s", strtotime($schoolHolidaysFrom[$man][$p]));
					// 							$time_to[$man][$j][$p] =  date("H:i:s", strtotime($schoolHolidaysTo[$man][$p]));
					// 							}
					// 							$lc_new[$man][$j] = $lc[$man][$i];
					// 							$pq_new[$man][$j] = $pq[$man][$i];
					// 							$tr_new[$man][$j] = $tr[$man][$i];
					// 							$subject[$man][$j] = $subjectName[$man];
					// 							$chapter[$man][$j] = $chapters[$man][$i];
					// 							$alloted[$man][$i] = $alloted[$man][$i] - $schoolHolidaysHours[$man];
					// 							$color[$man][$j] = "purple";

					// 					}
					// 					else
					// 					{
					// 							for($p=0;$p<1;$p++)
					// 							{
					// 							$time_from[$man][$j][$p] = date("H:i:s", strtotime($newOffDays[$man][$p]));
					// 							$time_to[$man][$j][$p] =  date("H:i:s", strtotime($newOffDays[$man][$p]));
					// 							}
					// 							$subject[$man][$j] = "Its your offday !!";
					// 							$chapter[$man][$j] = "";
					// 							$alloted[$man][$i] = $alloted[$man][$i];
					// 							$color[$man][$j] = "black";
					// 					}

					// 					$start[$man][$j] = $allDays[$man][$j];
					// 					$end[$man][$j] = $allDays[$man][$j];
					// 					$j++;

					// 			}

					// 	}


					// 	if($calendarStatus == 2)
					// 	{

					// 	$insert = new TableGateway('planner', $adapter, null, new HydratingResultSet());
					// 	$insert->insert(array(
					// 		'user_id'	=>	$_SESSION['userId'],
					// 		'subject_id' =>	htmlentities($data->subjectId[$man]),
					// 		'startDate'	=>	htmlentities($data->startDate[$man]),
					// 		'targetDate'	=>	htmlentities($data->targetDate[$man]),
					// 		'offDays'	=>	htmlentities($data->offDay[$man]),
					// 		'schoolHolidays'	=>	htmlentities($data->schoolHoliday[$man]),
					// 		'dailyOrAlternate'	=>	htmlentities($data->dailyOrAlternate[$man]),
					// 		'schoolDaysSlotsFrom'	=>	htmlentities(implode(',',$data->schoolDaysFrom[$man])),
					// 		'schoolDaysSlotsTo'	=>	htmlentities(implode(',',$data->schoolDaysTo[$man])),
					// 		'schoolHolidaysSlotsFrom'	=>	htmlentities(implode(',',$data->schoolHolidaysFrom[$man])),
					// 		'schoolHolidaysSlotsTo'	=>	htmlentities(implode(',',$data->schoolHolidaysTo[$man])),
					// 		'chapters' => htmlentities(implode(',',$data->chapters[$man])),
					// 		'rectimes' => htmlentities(implode(',',$data->rectimes[$man])),
					// 		'alloted'	=>	htmlentities(implode(',',$data->alloted[$man])),
					// 		'lc' => htmlentities(implode(',',$data->lc[$man])),
					// 		'pq' => htmlentities(implode(',',$data->pq[$man])),
					// 		'tr' => htmlentities(implode(',',$data->tr[$man]))

					// 		));

					// 	}

					// 		$new_result->status = 4;
					// 		$new_result->time_from = $time_from;
					// 		$new_result->time_to = $time_to;
					// 		$new_result->subject = $subject;
					// 		$new_result->chapter = $chapter;
					// 		$new_result->start = $start;
					// 		$new_result->end = $end;
					// 		$new_result->color = $color;
					// 		$new_result->lc = $lc_new;
					// 		$new_result->pq = $pq_new;
					// 		$new_result->tr = $tr_new;


					// }

			}


			return $new_result;
		}
		public function fetchCalendar2($data,$calendarStatus)
		{

			require_once('Events.php');

			date_default_timezone_set('Asia/Kolkata');
			$new_result = new stdClass();
			$events = array();
			$offDay = array();
			$holiDay = array();
			$startDates = array();
			$endDates = array();
			$adapter = $this->adapter;

			$flag = 0;

			//$colors = array("#43a1ed","#35aa47","#e6400c","#4F5B93","#27a0c9");

			$adapter = $this->adapter;

			$sql = new Sql($adapter);
			$select = $sql->select();

			$select->from('planner');

         	$select->where(array(
         		'user_id' => $_SESSION['userId'],

			));

			$selectString = $sql->getSqlStringForSqlObject($select);
			$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
			$res = $run->toArray();


			//offDay calculation - common

			foreach($res as $google)
			{
				$abc = explode(',',$google['offDays']);
				for($i=0;$i<count($abc);$i++)
				{
					if(!in_array($abc[$i],$offDay))
					{
						array_push($offDay,$abc[$i]);
					}
				}
			}

			//start dates of all subjects
			//target dates of all subjects
			foreach($res as $google)
			{
				if(!in_array($google['startDate'],$startDates))
				{
					array_push($startDates,$google['startDate']);
				}
				if(!in_array($google['targetDate'],$endDates))
				{
					array_push($endDates,$google['targetDate']);
				}
			}

			$minStartDate = Events::getMin($startDates); //minimum od start date
			$maxEndDate = Events::getMax($endDates); //maximum of target date

			//holiday calculation - common
			$latest_array = array();
			foreach($res as $google)
			{
				$abc = explode(',',$google['schoolHolidays']);
				for($i=0;$i<count($abc);$i++)
				{
					if(!in_array($abc[$i],$latest_array))
					{
						array_push($holiDay,$abc[$i]);
					}
				}
			}

			$holiDay = Events::getHoliDays($minStartDate, $maxEndDate, $holiDay, $offDay);


			//_print_r($res);
			foreach($res as $google)
			{
				$start = $google['startDate'];
				$end = $google['targetDate'];

				$slotd1 = explode(',',$google['schoolDaysSlotsFrom']);
				$slotd2 = explode(',',$google['schoolDaysSlotsTo']);
				$sloth1 = explode(',',$google['schoolHolidaysSlotsFrom']);
				$sloth2 = explode(',',$google['schoolHolidaysSlotsTo']);

				$Days = Events::getStudyDays($start, $end, $offDay, ($google['dailyOrAlternate']=='daily')?true:false);
				$c=0;
				// print_r($Days);
				// echo "<br>";
				// print_r($holiDay);

				$timestamp = strtotime($start);

				//_print_r($Days);
				$lc = explode(',',$google['lc']);
				$pq = explode(',',$google['pq']);
				$tr = explode(',',$google['tr']);
				$chapters = explode(',',$google['chapters']);
				for ($i = 0; $i < count($lc); $i++) {
					if($lc[$i] != '')
					{

						//$t = intval($lc[$i]) * 60 * 60;

						/*while($t > 0)
						{
							if(in_array(date('d-m-Y',$timestamp),$holiDay))
							{
								for ($j=0; $j < count($sloth1) && $t > 0; $j++)
								{
									$temp = date('d-m-Y',$timestamp)." ".$sloth1[$j];
									$slot1 = date_create_from_format('d-m-Y G:i A', $temp);
									$temp = date('d-m-Y',$timestamp)." ".$sloth2[$j];
									$slot2 = date_create_from_format('d-m-Y G:i A', $temp);
									$secs = ($slot1->diff($slot2)->format('%H') * 60 * 60) + ($slot1->diff($slot2)->format('%i') * 60);
									if($timestamp >= $slot1->format('U') && $timestamp < $slot2->format('U'))
									{
										$temp = date_create_from_format('U',$timestamp);
										$temp = $temp->diff($slot2)->format('%H') * 60 * 60 + $temp->diff($slot2)->format('%i') * 60;
										if($t <= $temp)
										{

											echo " LC ".$chapters[$i]." Event ".datetime('d-m-Y H:i',$timestamp).' == '.$slot2->format('d-m-Y H:i')."<br>";

											$timestamp = $timestamp + $t;
											echo $timestamp;
											$t = 0;

											//$j = count($slotd1);
										}
									}
									else if($timestamp < $slot1->format('U'))
									{
										$timestamp = $slot1->format('U');
										//_print_r("timestamp = 2 ".date('d-m-Y H:i',$timestamp));

										if($t >= $secs)
										{
											//echo " t ".var_dump($t)." ".var_dump($secs);
											//echo $t;
											$t = $t - $secs;
											echo " LC ".$chapters[$i]." Event ".date('d-m-Y H:i',$timestamp).' == '.$slot2->format('d-m-Y H:i')."<br>";

											$timestamp = $slot2->format('U');
											//_print_r("t = ".$t);

										}
										else if($t < $secs)
										{

											echo " LC ".$chapters[$i]." Event ".date('d-m-Y H:i',$timestamp).'===='.date('d-m-Y H:i',$timestamp + $t)."<br>";
											$timestamp += $t;
											$t = 0;
											//_print_r("t = ".$t);
											//_print_r("timestamp = 3 ".date('d-m-Y H:i',$timestamp));

										}
									}
								}

							}
							else if(in_array(date('d-m-Y',$timestamp),$Days))
							{
								for ($j=0; $j < count($slotd1); $j++)
								{
									$temp = date('d-m-Y',$timestamp)." ".$slotd1[$j];
									$slot1 = date_create_from_format('d-m-Y G:i A', $temp);
									$temp = date('d-m-Y',$timestamp)." ".$slotd2[$j];
									$slot2 = date_create_from_format('d-m-Y G:i A', $temp);
									$secs = $slot1->diff($slot2)->format('%H')*60*60+$slot1->diff($slot2)->format('%i')*60;

									if($timestamp >= $slot1->format('U') && $timestamp < $slot2->format('U'))
									{
										if($t <= $slot2->diff(date_create_from_format('U',$timestamp)))
										{

											echo " LC ".$chapters[$i]." Event ".datetime('d-m-Y H:i',$timestamp).' == '.$slot2->format('d-m-Y H:i')."<br>";

											$timestamp += $t;
											$t = 0;

											// _print_r("t = ".$t);
											// _print_r("timestamp 1 = ".date('U',$timestamp));
											$j = count($slotd1);
										}
									}
									else if($timestamp < $slot1->format('U'))
									{
										$timestamp = $slot1->format('U');
										// _print_r("timestamp = 2 ".date('d-m-Y H:i',$timestamp));

										if($t >= $secs)
										{
											// echo " t ".var_dump($t)." ".var_dump($secs);
											// echo $t;
											$t = $t - $secs;
											echo " LC ".$chapters[$i]." Event ".date('d-m-Y H:i',$timestamp).' == '.$slot2->format('d-m-Y H:i')."<br>";

											$timestamp = $slot2->format('U');
											//_print_r("t = ".$t);

										}
										else if($t < $secs)
										{

											echo " LC ".$chapters[$i]." Event ".date('d-m-Y H:i',$timestamp).'===='.date('d-m-Y H:i',$timestamp + $t)."<br>";
											$timestamp += $t;
											$j = count($slotd1);
											$t = 0;
											// _print_r("t = ".$t);
											// _print_r("timestamp = 3 ".date('d-m-Y H:i',$timestamp));

										}
									}

								}//slots end of for loop
								$c++;
								$timestamp = strtotime($Days[$c]);

							}//if in days
							else if(in_array(date('d-m-Y',$timestamp),$offDay))
							{
								$c++;
								$timestamp = strtotime($Days[$c]);

							}
							//$t=0;
						}*/
					}

				}
				/*foreach ($lc as $k => $v)
				{
					if($v != '')
					{
						//echo $v;
						$t = intval($v) * 60 * 60;
						$current = $Days[$c];
						//$c++;

						while($t != 0)
						{
							if(in_array($current, $holiDay) && in_array($current, $Days))
							{
								//_print_r($current);
								foreach ($sloth1 as $key => $value)
								{
									$temp = $current." ".$value;
									$slot1 = date_create_from_format('d-m-Y G:i A', $temp);

									$temp = $current." ".$sloth2[$key];
									$slot2 = date_create_from_format('d-m-Y G:i A', $temp);
									$secs = $slot1->diff($slot2)->format('%H')*60*60 + $slot1->diff($slot2)->format('%i') * 60;

									$last = $slot2->format('U');

									if($timestamp < $slot2->format('U'))
									{
										if($t >= $secs)
										{
											$t = $t - $secs;
											$timestamp = $slot2->format('U');

											$title='LC';
											$start_date=$current;
											$start_time=$slot1;
											$end_date=$current;
											$end_time=$slot2;
											$subject_id=$google['subject_id'];
											$subject_name=$google['chapters'][$k];
											//$color=;
											array_push($events, new Events($title, $start_date, $start_time, $end_date, $end_time, $subject_id, $subject_name));

										}
										else if($t < $secs)
										{
											$t = 0;
											$timestamp += $secs;

											$title='LC';
											$start_date=$current;
											$start_time=$slot1;
											$end_date=$current;// till timestamp
											$end_time=date_create_from_format('U', $timestamp);// till timestamp
											$subject_id=$google['subject_id'];
											$subject_name=$google['chapters'][$k];
											//$color=;
											array_push($events, new Events($title, $start_date, $start_time, $end_date, $end_time, $subject_id, $subject_name));
										}
									}
								}
								if($timestamp == $last)
								{
									$c++;
								}
								$t = 0;
							}
							else if(in_array($current, $Days))
							{
								_print_r($current);
								_print_r(date('U',$timestamp));
								foreach ($slotd1 as $key => $value)
								{
									$temp = $current." ".$value;
									$slot1 = date_create_from_format('d-m-Y G:i A', $temp);

									$temp = $current." ".$slotd2[$key];
									$slot2 = date_create_from_format('d-m-Y G:i A', $temp);
									$secs = $slot1->diff($slot2)->format('%H')*60*60 + $slot1->diff($slot2)->format('%i') * 60;

									if($timestamp < $slot2->format('U'))
									{
										if($t >= $secs)
										{
											$t = $t - $secs;
											$timestamp = $slot2->format('U');

											$title='LC';
											$start_date=$current;
											$start_time=$slot1;
											$end_date=$current;
											$end_time=$slot2;
											$subject_id=$google['subject_id'];
											$subject_name=$google['chapters'][$k];
											//$color=;
											array_push($events, new Events($title, $start_date, $start_time, $end_date, $end_time, $subject_id, $subject_name));

										}
										else if($t < $secs)
										{
											$t = 0;
											$timestamp += $secs;

											$title='LC';
											$start_date=$current;
											$start_time=$slot1;
											$end_date=$current;// till timestamp
											$end_time=date_create_from_format('U', $timestamp);// till timestamp
											$subject_id=$google['subject_id'];
											$subject_name=$google['chapters'][$k];
											//$color=;
											array_push($events, new Events($title, $start_date, $start_time, $end_date, $end_time, $subject_id, $subject_name));
										}
									}

								}
								$t = 0;
							}
						}
					}
				}*/

			// _print_r($offDay);
			// _print_r($holiDay);
			//_print_r($Days);
			}


			//_print_r($events);





			return $new_result;


		}


		/**
		* Function fetchPlannedSubjects for the planning of the schedule
		*
		* @author Pushpam Matah
		* @date 01-07-2015
		* @param JSON
		* @return JSON status and data(fetch the subjects that have been planned already)
		**/

		public function fetchPlannedSubjects() {


			$result = new stdClass();

			try{

				if(isset($_SESSION['userId']))
				{

					$adapter = $this->adapter;

					$sql = new Sql($adapter);
					$select = $sql->select();

					$select->from(array('s' => 'subjects'),array())
	             			->join(array('p' => 'planner'),
	                    		's.id = p.subject_id', array('subject_id'),'left');

					$select->where(array(
						'p.user_id'	=>	$_SESSION['userId']
					));

					$select->columns(array('name'));

					$selectString = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$res = $run->toArray();
					if(count($res))
					{
						$result->status = 1;
						$result->data = $res;
					}
					else
					{
						$result->status = 0;
					}
				}

			return $result;

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}


		}

		/**
		* Function editPlannedSubjects for the planning of the schedule
		*
		* @author Pushpam Matah
		* @date 04-07-2015
		* @param JSON
		* @return JSON status and data(retrieve data of a particular subject)
		**/

		public function editPlannedSubjects($data) {


			$result = new stdClass();

			try{

				$adapter = $this->adapter;

				$sql = new Sql($adapter);


				$select1 = $sql->select();
				$select1->from(array('s' => 'subjects'),array())
             			->join(array('c' => 'classes'),
                    		's.class_id = c.id', array());

             	$select1->where(array(

             		's.id' => $data->subject_id

             		));

				$select1->columns(array('id','name'));

             	$selectString1 = $sql->getSqlStringForSqlObject($select1);
				$run1 = $adapter->query($selectString1, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res1 = $run1->toArray();

				$select2 = $sql->select();
				$select2->from(array('c' => 'classes'),array())
             			->join(array('s' => 'subjects'),
                    		's.class_id = c.id', array());

             	$select2->where(array(

             		's.id' => $data->subject_id

             		));


             	$select2->columns(array('name'));

             	$selectString2 = $sql->getSqlStringForSqlObject($select2);
				$run2 = $adapter->query($selectString2, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res2 = $run2->toArray();


				$res = PlanApi::getPlannerDetails($data);

				$res3 = PlanApi::fetchPlanChaptersTime($data);

				foreach($res3->data as $timeChap)
				{
					$chapterName[] = $timeChap['name'];
					$chapterTime[] = $timeChap['time'];
				}

				if($res)
				$result->status = 1;
				$result->data = $res->data;
				$result->subjectName = $res1[0]['name'];
				$result->className = $res2[0]['name'];
				$result->subjectId = $data->subject_id;
				$result->chapterName = $chapterName;
				$result->chapterTime = $chapterTime;
				return $result;

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}


		}

		/**
		* Function updatePlannedSubjects for the planning of the schedule
		*
		* @author Pushpam Matah
		* @date 04-07-2015
		* @param JSON
		* @return JSON status and data(updating the data of particular subject in the database)
		**/

		public function updatePlannedSubjects($data) {


			$result = new stdClass();

			try{

				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$update = $sql->update();

				$update->table('planner');

				$update->set(array(

					'startDate' => $data->startDate,
					'targetDate' => $data->targetDate,
					'offDays' => $data->offDay,
					'schoolHolidays' => $data->schoolHoliday,
					'dailyOrAlternate' => $data->dailyOrAlternate,
					'schoolDaysSlotsFrom' => implode(',',$data->schoolDaysFrom),
					'schoolDaysSlotsTo' => implode(',',$data->schoolDaysTo),
					'schoolHolidaysSlotsFrom' => implode(',',$data->schoolHolidaysFrom),
					'schoolHolidaysSlotsTo' => implode(',',$data->schoolHolidaysTo),
					'rectimes' => implode(',',$data->rectimes),
					'chapters' => implode(',',$data->chapters),
					'alloted' => implode(',',$data->alloted),
					'lc' => implode(',',$data->lc),
					'pq' => implode(',',$data->pq),
					'tr' => implode(',',$data->tr)

				));

				$update->where(array(
						'user_id'	=>	$_SESSION['userId'],
						'subject_id' => $data->subject_id

					));

				$updateString = $sql->getSqlStringForSqlObject($update);
				$run = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify


				$result->status = 1;

				return $result;

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}


		}


		public function deletePlannedSubject($data) {


			$result = new stdClass();

			try{

				$adapter = $this->adapter;

				$where = array(
					'subject_id'	=>	$data->subject_id,
					'user_id'	=>	$_SESSION['userId'],


				);
				$device = new TableGateway('planner', $adapter, null, new HydratingResultSet());
				$device->delete($where);

				$result->status = 1;
				$result->message = "Subject Deleted.";

				return $result;

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}

	public function checkTimeClashes1($data)
	{
		try
		{

			$variable1 = 0;
			$variable2 = 0;

			$schoolDaysSlotsFrom = array();
			$schoolDaysSlotsTo = array();
			$schoolHolidaysSlotsFrom = array();
			$schoolHolidaysSlotsTo = array();

			$adapter = $this->adapter;
			$result = new stdClass();
			$schoolDaysDB = 0;
			$schoolHolidaysDB = 0;
			$schoolDaysUI = 0;
			$schoolHolidaysUI = 0;

			$sql2= new Sql($adapter);
			$select2 = $sql2->select();

			$select2->from('planner');

         	$select2->where(array(
				'user_id'	=>	$_SESSION['userId']
			));

			$selectString2 = $sql2->getSqlStringForSqlObject($select2);
			$run2 = $adapter->query($selectString2, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
			$res2 = $run2->toArray();

			if(count($res2) != 0 && $data->editValue == 0)
			{

				foreach($res2 as $myResult)
				{
					$a[] = $myResult['schoolDaysSlotsFrom'];
					$b[] = $myResult['schoolDaysSlotsTo'];
					$c[] = $myResult['schoolHolidaysSlotsFrom'];
					$d[] = $myResult['schoolHolidaysSlotsTo'];
					$startDate[] = $myResult['startDate'];
					$targetDate[] = $myResult['targetDate'];
				}



				for($z1=0;$z1<count($a);$z1++)
				{
					array_push($schoolDaysSlotsFrom,explode(',',$a[$z1]));
					array_push($schoolDaysSlotsTo,explode(',',$b[$z1]));
				}

				for($z2=0;$z2<count($c);$z2++)
				{
					array_push($schoolHolidaysSlotsFrom,explode(',',$c[$z2]));
					array_push($schoolHolidaysSlotsTo,explode(',',$d[$z2]));
				}

				for($i=0;$i<count($startDate);$i++)
				{
					if((strtotime($data->startDate) < strtotime($startDate[$i]) && strtotime($data->targetDate) < strtotime($startDate[$i])) || (strtotime($data->startDate) > strtotime($targetDate[$i]) && strtotime($data->targetDate) > strtotime($targetDate[$i])))
					{

					}
					else
					{
						$variable1++;
					}
				}


				for($i=0;$i<count($data->schoolDaysFrom);$i++)
				{
					for($j=0;$j<count($schoolDaysSlotsFrom);$j++)
					{
						for($k=0;$k<count($schoolDaysSlotsFrom[$j]);$k++)
						{
							if((strtotime($data->schoolDaysFrom[$i]) >= strtotime($schoolDaysSlotsTo[$j][$k]) && strtotime($data->schoolDaysTo[$i]) >= strtotime($schoolDaysSlotsTo[$j][$k])) || (strtotime($data->schoolDaysFrom[$i]) <= strtotime($schoolDaysSlotsFrom[$j][$k]) && strtotime($data->schoolDaysTo[$i]) <= strtotime($schoolDaysSlotsFrom[$j][$k])))
							{

							}
							else
							{
								$schoolDaysDB++;
							}
						}

					}

				}

				for($i=0;$i<count($data->schoolHolidaysFrom);$i++)
				{
					for($j=0;$j<count($schoolHolidaysSlotsFrom);$j++)
					{
						for($k=0;$k<count($schoolHolidaysSlotsFrom[$j]);$k++)
						{
							if((strtotime($data->schoolHolidaysFrom[$i]) >= strtotime($schoolHolidaysSlotsTo[$j][$k]) && strtotime($data->schoolHolidaysTo[$i]) >= strtotime($schoolHolidaysSlotsTo[$j][$k])) || (strtotime($data->schoolHolidaysFrom[$i]) <= strtotime($schoolHolidaysSlotsFrom[$j][$k]) && strtotime($data->schoolHolidaysTo[$i]) <= strtotime($schoolHolidaysSlotsFrom[$j][$k])))
							{

							}
							else
							{
								$schoolHolidaysDB++;
							}
						}

					}

				}

			}


			for($i=0;$i<count($data->schoolDaysFrom);$i++)
			{
				for($j=$i+1;$j<count($data->schoolDaysFrom);$j++)
				{
					if((strtotime($data->schoolDaysFrom[$j]) >= strtotime($data->schoolDaysTo[$i]) && strtotime($data->schoolDaysTo[$j]) >= strtotime($data->schoolDaysTo[$i])) || (strtotime($data->schoolDaysFrom[$j]) <= strtotime($data->schoolDaysTo[$i]) && strtotime($data->schoolDaysTo[$j]) <= strtotime($data->schoolDaysTo[$i])))
					{

					}
					else
					{
						$schoolDaysUI++;
					}
				}

			}

			for($i=0;$i<count($data->schoolHolidaysFrom);$i++)
			{
				for($j=$i+1;$j<count($data->schoolHolidaysFrom);$j++)
				{
					if((strtotime($data->schoolHolidaysFrom[$j]) >= strtotime($data->schoolHolidaysTo[$i]) && strtotime($data->schoolHolidaysTo[$j]) >= strtotime($data->schoolHolidaysTo[$i])) || (strtotime($data->schoolHolidaysFrom[$j]) <= strtotime($data->schoolHolidaysTo[$i]) && strtotime($data->schoolHolidaysTo[$j]) <= strtotime($data->schoolHolidaysTo[$i])))
					{

					}
					else
					{
						$schoolHolidaysUI++;
					}
				}

			}

			if($schoolDaysDB != 0 && $variable1 != 0)
			{
				$result->status = 1;
				$result->data = "There are time clashes in your school days with the time already alloted";
			}
			elseif($schoolHolidaysDB != 0 && $variable1 != 0)
			{
				$result->status = 1;
				$result->data = "There are time clashes in your school holidays with the time already alloted";
			}
			else if($schoolDaysUI != 0)
			{
				$result->status = 1;
				$result->data = "There are time clashes in your school days you have chosen here";
			}
			elseif($schoolHolidaysUI != 0)
			{
				$result->status = 1;
				$result->data = "There are time clashes in your school holidays you have chosen here";
			}
			else
			{


				$result->status = 2;
			}

			return $result;

		}catch(Exception $e ){
			$result->status = 0;
			$result->message = "ERROR: Some thing Went Wrong.";
			$result->error = $e->getMessage();
			return $result;
		}

	}

	public function checkTimeClashes($data)
	{
		//_print_r($data);

		require_once 'Events.php';
		$result = new stdClass();
		try
		{
			$offDay = explode(',',$data->offDay);

			$schoolDaysSlotsFrom = array();
			$schoolDaysSlotsTo = array();
			$schoolHolidaysSlotsFrom = array();
			$schoolHolidaysSlotsTo = array();

			$adapter = $this->adapter;


			$sql= new Sql($adapter);
			$select = $sql->select();

			$select->from(array('planner'=>'planner'))
					->join(array('subject'=>'subjects'),'planner.subject_id = subject.id',array('name'=>'name'),'left');
			$where = new $select->where();
			$where->equalTo('planner.user_id', $_SESSION['userId']);
			$where->notEqualTo('planner.subject_id', $data->subject_id);

			$select->where($where);

			$selectString = $sql->getSqlStringForSqlObject($select);
			$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
			$res = $run->toArray();

			//offDay calculation - common

			foreach($res as $google)
			{
				$abc = explode(',',$google['offDays']);
				for($i=0;$i<count($abc);$i++)
				{
					if(!in_array($abc[$i],$offDay))
					{
						array_push($offDay,$abc[$i]);
					}
				}
			}

			$daily = ($data->dailyOrAlternate == "alternate") ? false : true;
			$allDay = Events::getDays($data->startDate, $data->targetDate, $offDay, $daily);
			$holiday = explode(',', $data->schoolHoliday);


			for ($i=0; $i < count($res); $i++)
			{
				// _print_r($res[$i]);
				$daily1 = ($res[$i]['dailyOrAlternate'] == "alternate") ? false : true;
				$allDay1 = Events::getDays($res[$i]['startDate'], $res[$i]['targetDate'], $offDay, $daily1);
				$holiday1 = Events::getHoliDays($res[$i]['startDate'], $res[$i]['targetDate'], $holiday,$offDay);
				//echo "all days";
				$a = array_intersect($allDay, $allDay1);
				//_print_r($a);
				//echo "all days I holidays";
				$b = array_intersect($allDay, $holiday1, $allDay1);
				$a = array_diff($a, $b);

				if(count($b))
				{
					//holiday slots check

					$X = $data->schoolHolidaysFrom;
					$Y = $data->schoolHolidaysTo;

					$s1 = explode(',',$res[$i]['schoolHolidaysSlotsFrom']);
					$s2 = explode(',',$res[$i]['schoolHolidaysSlotsTo']);
					for ($j=0; $j < count($s1); $j++) {
						$x = intval(date_create_from_format('G:i A', $s1[$j])->format('U'));
						$y = intval(date_create_from_format('G:i A', $s2[$j])->format('U'));
						for ($k=0; $k < count($X); $k++) {
							$X1 = intval(date_create_from_format('G:i A',$X[$k])->format('U'));
							$Y1 = intval(date_create_from_format('G:i A',$Y[$k])->format('U'));
							if($X1 <= $x && $Y1 <= $x || $X1 >= $y && $Y1 >= $y)
							{

							}else{
								$result->status = 0;
								$result->message = "Clash in Holiday Slots with subject ".$res[$i]['name'];
								return $result;
							}
						}
					}
				}
				if(count($a))
				{

					$X = $data->schoolDaysFrom;
					$Y = $data->schoolDaysTo;

					$s1 = explode(',',$res[$i]['schoolDaysSlotsFrom']);
					$s2 = explode(',',$res[$i]['schoolDaysSlotsTo']);
					for ($j=0; $j < count($s1); $j++) {
						$x = intval(date_create_from_format('G:i A', $s1[$j])->format('U'));
						$y = intval(date_create_from_format('G:i A', $s2[$j])->format('U'));
						for ($k=0; $k < count($X); $k++) {
							$X1 = intval(date_create_from_format('G:i A',$X[$k])->format('U'));
							$Y1 = intval(date_create_from_format('G:i A',$Y[$k])->format('U'));
							if($X1 <= $x && $Y1 <= $x || $X1 >= $y && $Y1 >= $y)
							{

							}else{
								$result->status = 0;
								$result->message = "Clash in Study day Slots with subject ".$res[$i]['name'];
								return $result;
							}
						}

					}

				}


			}


			$result->status = 1;
			$result->message = "No Clash";

			return $result;

		}catch(Exception $e ){
			$result->status = 0;
			$result->message = "ERROR: Some thing Went Wrong.";
			$result->error = $e->getMessage();
			return $result;
		}

	}


	public function findAvailableHours($data)
	{
		date_default_timezone_set('Asia/Kolkata');
		$result = new stdClass();
		try
		{
				//$newSchoolHolidays = [];

				//$res1 = PlanApi::getPlannerDetails($data);


				//$res = $res1->data[0];


				$startDate = $data->startDate;
				$targetDate = $data->targetDate;
				$offDay = explode(',',$data->offDay);
				$schoolHoliday = explode(',',$data->schoolHoliday);
				$dailyOrAlternate = $data->dailyOrAlternate;
				$schoolHolidaysSlotsFrom = $data->schoolHolidaysFrom;
				$schoolHolidaysSlotsTo = $data->schoolHolidaysTo;
				$schoolDaysSlotsFrom = $data->schoolDaysFrom;
				$schoolDaysSlotsTo = $data->schoolDaysTo;

				$finalStartDate = new DateTime($startDate);
				$finalTargetDate = new DateTime($targetDate);

				// echo '<pre>';
				// print_r($finalTargetDate);
				// print_r($finalStartDate);
				// echo '</pre>';

				$finalTargetDate->add(new DateInterval('P1D'));


				for($m=strtotime($startDate);$m<=strtotime($targetDate);$m+=86400)
				{
					if(date('l',$m) == "Sunday" && !in_array(date('d-m-Y',$m),$schoolHoliday) && !in_array(date('d-m-Y',$m),$offDay))
					{
						array_push($schoolHoliday,date('d-m-Y',$m));
					}
				}


				$allDaysCount = $finalTargetDate->diff($finalStartDate)->format("%a");


				for($mk=strtotime($startDate);$mk<=strtotime($targetDate);$mk+=86400)
				{
					$uncle[] = date('d-m-Y',$mk);
				}



				$mins = 0;
				for($i=0;$i<count($schoolHolidaysSlotsFrom);$i++)
				{
					$HolidaysSlotsTo = new DateTime($schoolHolidaysSlotsTo[$i]);
					$HolidaysSlotsFrom = new DateTime($schoolHolidaysSlotsFrom[$i]);

					$mins += $HolidaysSlotsTo->diff($HolidaysSlotsFrom)->format("%H")*60 + $HolidaysSlotsTo->diff($HolidaysSlotsFrom)->format("%i");
				}

				$mins1 = 0;
				for($i=0;$i<count($schoolDaysSlotsFrom);$i++)
				{
					$HolidaysSlotsTo = new DateTime($schoolDaysSlotsTo[$i]);
					$HolidaysSlotsFrom = new DateTime($schoolDaysSlotsFrom[$i]);

					$mins1 += $HolidaysSlotsTo->diff($HolidaysSlotsFrom)->format("%H")*60 + $HolidaysSlotsTo->diff($HolidaysSlotsFrom)->format("%i");
				}

				$newOffDays = $offDay;

				$studyDaysCount = $allDaysCount - count($newOffDays) - count($schoolHoliday);
				if($dailyOrAlternate == "alternate")
					$studyDaysCount = ceil($studyDaysCount / 2);


				/*echo '<pre>';
				print_r("Study Days Count = $studyDaysCount.<br>");
				print_r("All Days Count = $allDaysCount.<br>");
				print_r("Off Days Count = ".count($offDay)."<br>");
				print_r("schoolHolidayS Count = ".count($schoolHoliday)."<br>");*/
				$schoolDaysCount = $allDaysCount-(count($offDay)+count($schoolHoliday));
				// print_r("schoolDaysCount = ".count($schoolDaysCount)."<br>");
				$A = ($studyDaysCount)*$mins1;
				$B =  count($schoolHoliday)*$mins;
				$Th = intval(($A + $B)/60);
				$Tm = intval(($A + $B)%60);

				$result->hours = $Th;
				$result->minutes = $Tm;
				$result->status = 1;
				$result->message = "Available hours.";
			return $result;
		}catch(Exception $e ){
			$result->status = 0;
			$result->message = "ERROR: Some thing Went Wrong.";
			$result->error = $e->getMessage();
			return $result;
		}
	}
	public function fetchCalendar1($data,$calendarStatus)
	{
		//_print_r($data);
		try{
			$adapter = $this->adapter;
			if($calendarStatus == 2) {

				$insert = new TableGateway('planner', $adapter, null, new HydratingResultSet());
				$insert->insert(array(
					'user_id'	=>	$_SESSION['userId'],
					'subject_id' =>	htmlentities($data->subjectId[0]),
					'startDate'	=>	htmlentities($data->startDate[0]),
					'targetDate'	=>	htmlentities($data->targetDate[0]),
					'offDays'	=>	htmlentities($data->offDay[0]),
					'schoolHolidays'	=>	htmlentities($data->schoolHoliday[0]),
					'dailyOrAlternate'	=>	htmlentities($data->dailyOrAlternate[0]),
					'schoolDaysSlotsFrom'	=>	htmlentities(implode(',',$data->schoolDaysFrom[0])),
					'schoolDaysSlotsTo'	=>	htmlentities(implode(',',$data->schoolDaysTo[0])),
					'schoolHolidaysSlotsFrom'	=>	htmlentities(implode(',',$data->schoolHolidaysFrom[0])),
					'schoolHolidaysSlotsTo'	=>	htmlentities(implode(',',$data->schoolHolidaysTo[0])),
					'chapters' => htmlentities(implode(',',$data->chapters[0])),
					'rectimes' => htmlentities(implode(',',$data->rectimes[0])),
					'alloted'	=>	htmlentities(implode(',',$data->alloted[0])),
					'lc' => htmlentities(implode(',',$data->lc[0])),
					'pq' => htmlentities(implode(',',$data->pq[0])),
					'tr' => htmlentities(implode(',',$data->tr[0]))

				));

			}
			require_once('Events.php');

			date_default_timezone_set('Asia/Kolkata');
			$result = new stdClass();
			$events = array();
			$offDay = array();
			$holiDay = array();
			$startDates = array();
			$endDates = array();
			$adapter = $this->adapter;

			$flag = 0;

			//$colors = array("#43a1ed","#35aa47","#e6400c","#4F5B93","#27a0c9");

			$adapter = $this->adapter;

			$sql = new Sql($adapter);
			$select = $sql->select();

			$select->from('planner');

         	$select->where(array(
         		'user_id' => $_SESSION['userId'],

			));

			$selectString = $sql->getSqlStringForSqlObject($select);
			$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
			$res = $run->toArray();


			//offDay calculation - common
			//$latest_array = array();
			foreach($res as $google)
			{
				$abc = explode(',',$google['offDays']);
				for($i=0;$i<count($abc);$i++)
				{
					if(!in_array($abc[$i],$offDay))
					{
						array_push($offDay,$abc[$i]);
					}
				}
			}

			//start dates of all subjects
			//target dates of all subjects
			foreach($res as $google)
			{
				if(!in_array($google['startDate'],$startDates))
				{
					array_push($startDates,$google['startDate']);
				}
				if(!in_array($google['targetDate'],$endDates))
				{
					array_push($endDates,$google['targetDate']);
				}
			}

			$minStartDate = Events::getMin($startDates); //minimum od start date
			$maxEndDate = Events::getMax($endDates); //maximum of target date

			//holiday calculation - common
			$latest_array = array();
			foreach($res as $google)
			{
				$abc = explode(',',$google['schoolHolidays']);
				for($i=0;$i<count($abc);$i++)
				{
					if(!in_array($abc[$i],$latest_array))
					{
						array_push($holiDay,$abc[$i]);
					}
				}
			}

			$holiDay = Events::getHoliDays($minStartDate, $maxEndDate, $holiDay, $offDay);
			foreach($res as $google)
			{
				$start = $google['startDate'];
				$end = $google['targetDate'];

				$slotd1 = explode(',',$google['schoolDaysSlotsFrom']);
				$slotd2 = explode(',',$google['schoolDaysSlotsTo']);
				$sloth1 = explode(',',$google['schoolHolidaysSlotsFrom']);
				$sloth2 = explode(',',$google['schoolHolidaysSlotsTo']);

				$Days = Events::getStudyDays($start, $end, $offDay, ($google['dailyOrAlternate']=='daily')?true:false);

				$timestamp = strtotime($start);

				$lc = explode(',',$google['lc']);
				$pq = explode(',',$google['pq']);
				$tr = explode(',',$google['tr']);
				$chapters = explode(',',$google['chapters']);

				Events::$flag = true;

				Events::_getevent($slotd1,$slotd2,$sloth1,$sloth2 ,$holiDay,$Days,$offDay, $chapters, $lc,$pq,$tr, $start, $end);

			}

			Events::_plannerOff($offDay);
			return $result->events=Events::$events;



		}catch(Exception $e ){
			$result->status = 0;
			$result->message = "ERROR: Some thing Went Wrong.";
			$result->error = $e->getMessage();
			return $result;
		}
	}
}


?>