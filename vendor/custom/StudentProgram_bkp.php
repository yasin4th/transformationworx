<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Expression;

	//including connection.php file to make connection with database
	require_once 'Connection.php';
	
	class StudentProgram extends Connection {
		
		/**
		*
		* @author Ravi Arya
		* @date 28-1-2016
		* @param JSON
		* @return JSON data for available test program -> group(test paper type)
		* @todo add Payment Gateway
		*
		**/
		
		public function getTestprogramsAvailableForSave($data) {
			
			$result = new stdClass();
			
			try{
				
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('tp' => 'test_program'))
						->join(array('c'=>'classes'),'tp.class = c.id', array('class' => 'name'),'left');
				$select->columns(array('id','name', 'price', 'description', 'image'));
				$where = array();
				$where['tp.status'] = 1;
				$where['tp.deleted'] = 0;

				$select->where($where);
				$select->order('id DESC');
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				
				$result->status = 1;
				$result->data = $res;

				$sql = new Sql($adapter);
				$select = $sql->select();

				$select->from(array('tp' => 'test_papers_of_test_program'))
						->join(array('type'=>'test_paper_type'),'tp.test_paper_type_id = type.id',
						 array('test_paper_type' =>'type'),'left')
						->group(array('test_program_id','test_paper_type_id'));
				$select->columns(array('count' => new Expression('count(test_paper_id)'),'test_program_id','test_paper_type_id'));

				
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();				
				
				$result->papers = $res;
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
			
		}

		public function filterPrograms($data) {
			
			$result = new stdClass();
			
			try{
				
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();

				if($data->option == "2" ) {
					$select->from(array('tp' => 'test_program'))						
							->join(array('tps'=>'test_program_subjects'),'tps.test_program_id = tp.id', array(),'left');
					
					$select->columns(array('id','name', 'price', 'description', 'image'));
					$where=array();
					if(isset($data->keyword)){
						$select->where('tp.name LIKE \'%'.$data->keyword.'%\'');
						//$where['tp.name']=$data->keyword;
					}
					if(isset($data->class_id)){
						$where['tp.class']=$data->class_id;
					}
					if(isset($data->subject_id)){
						$where['tps.subject_id']=$data->subject_id;
					}


					
					$where['tp.status'] = 1;
					$where['tp.deleted'] = 0;
					$select->where($where);
					$select->group('tp.id');
					$select->order('created DESC');
					$selectString = $sql->getSqlStringForSqlObject($select);
					
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$res = $run->toArray();
					$result->data = $res;
					$result->status = 1;
				}
				else if($data->option == "1" ){				

					$select->from(array('tp'=>'test_program'))
							->join(array('sp'=>'student_program'),'sp.test_program_id = tp.id',array('time','count'=>new Expression('count(sp.id)')),'left');

					$select->columns(array('name', 'price', 'description', 'image', 'id'));
					$where = array();
					if(isset($data->keyword)){
						$select->where('tp.name LIKE \'%'.$data->keyword.'%\'');
					}
					if(isset($data->class_id)){
						$where['tp.class']=$data->class_id;
					}
					/*if(isset($data->subject_id)){
						$where['tps.subject_id']=$data->subject_id;
					}*/
					$where['tp.status'] = 1;
					$where['tp.deleted'] = 0;
					$select->where($where);
					$select->order('count DESC');
					$select->group('tp.id');
					$selectString = $sql->getSqlStringForSqlObject($select);
					// echo $selectString;
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$res = $run->toArray();
					$result->data = $res;
					$result->status = 1;

				}
				else if($data->option == "0" )
				{
					//no sorting

					$select->from(array('tp' => 'test_program'))						
							->join(array('tps'=>'test_program_subjects'),'tps.test_program_id = tp.id', array(),'left');
					
					$select->columns(array('id','name', 'price', 'description', 'image'));
					$where=array();
					if(isset($data->keyword))
					{
						$select->where('tp.name LIKE \'%'.$data->keyword.'%\'');						
					}
					if(isset($data->class_id))
					{
						$where['tp.class']=$data->class_id;
					}
					if(isset($data->subject_id))
					{
						$where['tps.subject_id']=$data->subject_id;
					}

					if(isset($data->category))
					{
						$category = implode(',', $data->category);
						$select->where("tp.category IN ($category)");
					}
					$where['tp.status'] = 1;
					$where['tp.deleted'] = 0;
					$select->where($where);
					$select->order('id DESC');
					$select->group('tp.id');
					
					$selectString = $sql->getSqlStringForSqlObject($select);
					
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$res = $run->toArray();
					$result->data = $res;
					$result->status = 1;
				}


				// program paper details
				$sql = new Sql($adapter);
				$select = $sql->select();

				$select->from(array('tp' => 'test_papers_of_test_program'))
						->join(array('type'=>'test_paper_type'),'tp.test_paper_type_id = type.id',
						 array('test_paper_type' =>'type'),'left')
						->group(array('test_program_id','test_paper_type_id'));
				$select->columns(array('count' => new Expression('count(test_paper_id)'),'test_program_id','test_paper_type_id'));

				
				$selectString = $sql->getSqlStringForSqlObject($select);
				
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();				
				
				
				$result->papers = $res;
				return $result;
				
			}
			catch(Exception $e )
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
			
		}

		
		/**
		* this function fetch all classes
		*
		* @author Utkarsh Pandey
		* @date 01-06-2015
		* @param JSON with all required details
		* @return JSON with all details
		* @return JSON with all classesDetails, status and message
		**/
		public function buyTestProgram($data)
		{
			$result = new stdClass();
			try{
				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('student_program');
				$select->where(array(
					'user_id'	=> $data->userId,
					'test_program_id'	=> $data->program_id
				));
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$bought = $run->toArray();
				
				if (count($bought) == 0) {
					// Query INSERT INTO `student_program`( `user_id`, `test_program_id`, `time`) VALUES ($_SESSION['userId'],time())
					$insert = new TableGateway('student_program', $adapter, null, new HydratingResultSet());
					$insert->insert(array(
						'user_id' => $data->userId,
						'test_program_id' => $data->program_id,
						'time' => time()
					));
					$id = $insert->getLastInsertValue();
					$result->status = 1;
					$result->id = $id;

					//$customer_mail = "Hi ".$data->firstname.",<br><br>Thank you for registering with MyLearningGraph.<br><br>Your login credentials -<br><br>Username: ".$data->email." <br>Password: ".$data->password." <br><br>Best Regards,<br>User Support,<br>MyLearningGraph<br>www.mylearninggraph.com<br>";
	                
	                
	                // $util = new Utility();
	                // $util->sendEmail('raviarya.4thpointer@gmail.com','Thank you for registering.',$customer_mail);
	                // $util->sendEmail('raviarya.4thpointer@gmail.com', "New Registration of student.", $customer_mail);


					$result->message = "Successfully Bought";
				}
				else {
					$result->status = 0;

					$result->message = "Program Already Bought";
				}
			
				return $result;
			}
			catch(Exception $e)
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
			}
		
		}

		public function fetchOrders($data) {

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
			
				$select = $sql->select();
				//$select->from('student_program');
				$select->from(array('sp' => 'student_program'))
						->join(array('tp'=>'test_program'),'tp.id = sp.test_program_id', array('name' => 'name','price' => 'price'),'left')
						->join(array('p'=>'users'),'p.id = sp.user_id', array('first_name' => new Expression('IFNULL(first_name,"")'),'last_name' => new Expression('IFNULL(last_name,"")')),'left');

				$select->order('id desc');
				$select->columns(array('id', 'user_id','time' => new Expression('FROM_UNIXTIME(time,"%d-%m-%y")')));
				if (isset($data->user_id)){
					$select->where(array(
						'user_id'	=>	$data->user_id
					));
				}
				if (isset($data->order_id)){
					$order = $data->order_id;
					//preg_match_all('/[{]{2}@([a-z]+)[}]{2}(.*)[{]{2}@\/\1[}]{2}/ixms', $template, $matches);
					preg_match_all('/\/?(.+)\/(.+)/', $order, $matches);
					// _print_r($matches);
					if(count($matches))
					{
						$select->where(array(
							'sp.id'	=>	end($matches)
						));						
					}
				}
				if (isset($data->to) && isset($data->from)) {
					if($data->to != '' && $data->from != ''){
						$from = strtotime($data->from);
						$to = strtotime($data->to);
						$select->where('(sp.time BETWEEN "'.$from.'" AND "'.$to.'")');
					}
				}else if(!isset($data->to) && isset($data->from)) {
					if($data->from != ''){
						$from = strtotime($data->from);
						//$to = strtotime($data->to);
						$select->where('(sp.time > "'.$from.'")');
					}
				}
				if (isset($data->price_to) && isset($data->price_from)) {
					if($data->price_to != '' && $data->price_from != ''){
						$from = $data->price_from;
						$to = $data->price_to;
						$select->where('(tp.price BETWEEN "'.$from.'" AND "'.$to.'")');
					}
				}else if(!isset($data->price_to) && isset($data->price_from)) {
					if($data->price_from != ''){
						$from = $data->price_from;
						//$to = strtotime($data->to);
						$select->where('(tp.price > "'.$from.'")');
					}
				}
				if (isset($data->name)) {
					$select->where('(first_name LIKE "%'.$data->name.'%" OR last_name LIKE "%'.$data->name.'%" )');
					
				}
				if (isset($data->program_name)) {
					$select->where('tp.name LIKE "%'.$data->program_name.'%"');
					
				}
				$statement = $sql->getSqlStringForSqlObject($select);
				
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				$result->total_questions = count($res);

				if (isset($data->limit)) {
					$select->limit(50);
					$select->offset(intval($data->limit));					
				}

				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				$result->data = $res;
				
				$result->status = 1;
				$result->message = "Operation Successful.";
				
				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
	}
?>
