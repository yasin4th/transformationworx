<?php
require_once('Question.php');

class QTI {

	/**
	* Funtion to upload and extract the zip qti assessment set
	*
	* @author Rupesh Pandey
	* @date 02/06/2015
	* @param array file details
	* @return JSON result status
	*/
	public function uploadZip($file) {
		$result = new stdclass();
		try {
			@$ext = end(explode('.', $file->file['name']));
			$dest = time();
			$filename = $dest.'.'.$ext;
			if(strcasecmp($ext, 'zip') == 0) {
				move_uploaded_file($file->file['tmp_name'], $filename);
				$zip = new ZipArchive();
				if($zip->open($filename)) {
					if(mkdir($dest)) {
						$zip->extractTo($dest);
						$zip->close();
						//calling further functions which will import the extracted question in db
						$result = $this->extractAssessment($dest,$file);
					}
					else {
						$result->status = 0;
						$result->message = 'Can not create folder please check server settings';
					}
				}
				else {
					$result->status = 0;
					$result->message = 'Failed to open file. Please try again.';
				}
			}
			else {
				$result->status = 0;
				$result->message = 'Invalid file type. Please upload a zip file';
			}
			//deleting the zip file
			@unlink($filename);

			return $result;
		}catch(Exception $e) {
			$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
			return $result;
		}
	}

	/**
	* Function to extract assessment details from zip file and import each question in DB
	*
	* @author Rupesh Pandey
	* @date 04/05/2015
	* @param string folder name where zip file is extracted
	* @return JSON result status
	*/
	public function extractAssessment($folder,$info) {
		$result = new stdClass();
		try {
			$filename = $folder.'/resources/assessment.xml';
			$importedQuestions = array();
			$total = 0;
			$failFlag = false;
			if(@$xml = simplexml_load_file($filename)) {
				$title = $xml['title'];
				$description = $xml['description'];

				foreach($xml->testPart[0]->assessmentSection[0]->assessmentItemRef as $question) {
					$result = $this->importSingleQuestion($folder, $question['href'],$info);
					if($result->status == 1) {
						$importedQuestions[] = '0';
						$total++;
					}else {
						$failFlag = true;
					}
				}
			}
			else {
				$result->status = 0;
				$result->message = 'Invalid QTI zip file. Please try again.';
			}
			$this->deleteFolder($folder);

			$result->total = $total;
			if($failFlag) {
				$result->status = 0;
				$result->message = "Some questions failed to upload. Only {$total} Questions imported.";
			}else {
				$result->status = 1;
				$result->message = "$total Questions Imported Successfully.";
			}
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
			return $result;
		}
	}

	/**
	* Function to save single question in db
	*
	* @author Rupesh Pandey
	* @date 08/06/2015
	* @param string folder name
	* @param string filename
	* @return JSON result status
	*/
	public function importSingleQuestion($folder, $file,$info) {
		$result = new stdClass();
		$filename = $folder.'/resources/'.$file;
		if(@$F = fopen($filename, 'r')) {
			$text = fread($F, filesize($filename));
			fclose($F);

			@$xml = new SimpleXMLElement($text);
			
			$cardinality = $xml->responseDeclaration['cardinality'];
			$baseType = $xml->responseDeclaration['baseType'];
			$type = $this->findQuestionType($cardinality,$baseType);
			

			$explain = null;
			foreach($xml->modalFeedback as $feedback) {
				if($feedback['showHide'] == 'hide')
					$explain = $feedback;
			}

			$body = null;
			$body = $xml->itemBody[0];
			
					//print_r($options);
			switch ($type) {
				case 1:
					if(isset($xml->responseDeclaration[0]->correctResponse[0]->value)) {
						$answer = $xml->responseDeclaration[0]->correctResponse[0]->value->__toString();

						$options = array();
						foreach($body->choiceInteraction[0]->simpleChoice as $opt) {
							//extracting and modifing images
							$tmp = $opt->asXML();
							$tmp = new SimpleXMLElement($tmp);
							$images = $tmp->xpath('//img');
							foreach($images as $key => $img) {
								$oldLocation = $folder.'/resources/'.$img['src'];
								@$imageFileName = end(explode('/', $img['src']));
								$newLocation = '../admin/uploads/question-images/'.$folder.$imageFileName;
								if(@copy($oldLocation, $newLocation)) {
									@$images[$key]['src'] = $newLocation;
								}
							}
							$obj = new stdClass();
							$obj->option = $this->innerHtml($tmp);
							$obj->answer = 0;
							if($opt['identifier']->__toString() == $answer) {
								$obj->answer = 1;
							}
							$options[] = $obj;
						}
						//print_r($options);
						unset($body->choiceInteraction[0]);
						break;
					}	
					else {
						$options = array();
						$type = 10;
						break;
					}
				
				case 2:
					$answers = $xml->responseDeclaration[0]->correctResponse[0]->value;

					$options = array();
					foreach($body->choiceInteraction[0]->simpleChoice as $opt) {
						//extracting and modifing images
						$tmp = $opt->asXML();
						$tmp = new SimpleXMLElement($tmp);
						$images = $tmp->xpath('//img');
						foreach($images as $key => $img) {
							$oldLocation = $folder.'/resources/'.$img['src'];
							@$imageFileName = end(explode('/', $img['src']));
							$newLocation = '../admin/uploads/question-images/'.$folder.$imageFileName;
							if(@copy($oldLocation, $newLocation)) {
								@$images[$key]['src'] = $newLocation;
							}
						}
						$obj = new stdClass();
						$obj->option = $this->innerHtml($tmp);
						$obj->answer = 0;
						foreach($answers as $answer) {
							if($opt['identifier']->__toString() == $answer->__toString()) {
								$obj->answer = 1;
							}
						}
							
						$options[] = $obj;
					}
					//print_r($options);
					unset($body->choiceInteraction[0]);
					break;
				
				case 4:
					$answers = $xml->responseDeclaration[0]->correctResponse[0]->value;

					$options = array();
					foreach($body->orderInteraction[0]->simpleChoice as $opt) {
						//extracting and modifing images
						$tmp = $opt->asXML();
						$tmp = new SimpleXMLElement($tmp);
						$images = $tmp->xpath('//img');
						foreach($images as $key => $img) {
							$oldLocation = $folder.'/resources/'.$img['src'];
							@$imageFileName = end(explode('/', $img['src']));
							$newLocation = '../admin/uploads/question-images/'.$folder.$imageFileName;
							if(@copy($oldLocation, $newLocation)) {
								@$images[$key]['src'] = $newLocation;
							}
						}
						$obj = new stdClass();
						$obj->option = $this->innerHtml($tmp);
						$obj->answer = 0;
						foreach($answers as $answer) {
							if($opt['identifier']->__toString() == $answer->__toString()) {
								$obj->answer = 1;
							}
						}

						$options[] = $obj;
					}
					//print_r($options);
					unset($body->orderInteraction[0]);
					break;
				
				case 5:
					$optionIdentifires = $xml->responseDeclaration[0]->correctResponse[0]->value;
					$firstColumn = array();
					$secondColumn = array();
					foreach ($optionIdentifires as $optionIdentifire) {
						$pieces = explode(' ', $optionIdentifire);
						$firstColumn[] = $pieces[0];
						$secondColumn[] = $pieces[1];
					}

					$options = array();
					foreach ($firstColumn as $identifier) {
						foreach($body->associateInteraction[0]->simpleAssociableChoice as $opt) {
							//extracting and modifing images
							$tmp = $opt->asXML();
							$tmp = new SimpleXMLElement($tmp);
							$images = $tmp->xpath('//img');
							foreach($images as $key => $img) {
								$oldLocation = $folder.'/resources/'.$img['src'];
								@$imageFileName = end(explode('/', $img['src']));
								$newLocation = '../admin/uploads/question-images/'.$folder.$imageFileName;
								if(@copy($oldLocation, $newLocation)) {
									@$images[$key]['src'] = $newLocation;
								}
							}

							
							$obj = new stdClass();
								if($opt['identifier']->__toString() == $identifier) {
									$obj->option = $this->innerHtml($tmp);
									$obj->number = 1;
									$obj->answer = 0;
									$options[] = $obj;
								}
						}

					}

					foreach ($secondColumn as $identifier) {
						foreach($body->associateInteraction[0]->simpleAssociableChoice as $opt) {
							//extracting and modifing images
							$tmp = $opt->asXML();
							$tmp = new SimpleXMLElement($tmp);
							$images = $tmp->xpath('//img');
							foreach($images as $key => $img) {
								$oldLocation = $folder.'/resources/'.$img['src'];
								@$imageFileName = end(explode('/', $img['src']));
								$newLocation = '../admin/uploads/question-images/'.$folder.$imageFileName;
								if(@copy($oldLocation, $newLocation)) {
									@$images[$key]['src'] = $newLocation;
								}
							}

							$obj = new stdClass();
								if($opt['identifier']->__toString() == $identifier) {
									$obj->option = $this->innerHtml($tmp);
									$obj->number = 2;
									$obj->answer = 1;
									$options[] = $obj;
								}
						}
							
					}

					// print_r($options);
					// die();
					unset($body->associateInteraction[0]);
					break;

					
				case 6:
					if (isset($body->gapMatchInteraction)) {
							
						$answers = $xml->responseDeclaration[0]->correctResponse[0]->value;
						$options = array();
						foreach($body->gapMatchInteraction[0]->gapText as $opt) {
							//extracting and modifing images
							$tmp = $opt->asXML();
							$tmp = new SimpleXMLElement($tmp);
							
							$obj = new stdClass();
							$obj->option = strip_tags($tmp[0]->__toString());
							$obj->answer = 0;
							foreach($answers as $answer) {
								$pices = explode(" ",$answer->__toString());
								if($opt['identifier']->__toString() == $pices[0]) {
									$obj->answer = 1;
								}
							}
								
							$options[] = $obj;
						}

						$dom = new DOMDocument();
	    				$dom->loadXML($text);

						$gaps = $dom->getElementsByTagName('gap');
						// print_r($gaps);
						// die();

						$num = 1;
						$newTag	= $dom->createElement('input');
						$attribute = $dom->createAttribute('data-id');
						$attribute->value = $num;
						$newTag->appendChild($attribute);
						foreach($gaps as $gap) {
							$gap->parentNode->replaceChild($newTag, $gap);
							$num++;
						}
						$newXml = $dom->saveXML();

						// echo $newXml;
						$xml = new SimpleXMLElement($newXml);
						$body = $xml->itemBody[0]->gapMatchInteraction->blockquote[0]->p[0];

						// $node = $tmp = $body->gapMatchInteraction->blockquote[0]->p[0]->asXML();
						// $question = $dom->getElementsByTagName('blockquote');
						// $tmp = new SimpleXMLElement($tmp);
						
						// print_r($this->innerHtml($tmp));
						// die();

						// unset($body->gapMatchInteraction[0]->gapText);
					}
					else if (isset($body->matchInteraction)) {						
						// $numberOfPaires = $body->matchInteraction[0]['maxAssociations'];
						/**
						* defining that this is Matrix Question Type
						*/
						$type = 12;
						$table = $a = $b = array();
						
						/**
						* creating a a double dimensional array(TABLE) and seting default value to zero
						*/
						$columns = count($body->matchInteraction[0]->simpleMatchSet[0]->simpleAssociableChoice) + 1;
						$rows = count($body->matchInteraction[0]->simpleMatchSet[1]->simpleAssociableChoice) + 1;
						for ($x=0; $x < $columns; $x++) { 
							for ($y=0; $y < $rows; $y++) { 
								$table[$x][$y] = 0;
							}
						}

						/**
						* fetching all rows
						*/
						$x = $y = 1;
						foreach ($body->matchInteraction[0]->simpleMatchSet[0]->simpleAssociableChoice as $option) {
							$table[$x][0] = $this->innerHtml($option);
							$a[$option['identifier']->__toString()] = $x;
							// echo "row:0 column:".$x."<br>".$table[$x][0]."<br>";
							$x++;
						}

						/**
						* fetching all columns
						*/
						foreach ($body->matchInteraction[0]->simpleMatchSet[1]->simpleAssociableChoice as $option) {
							$table[0][$y] = $this->innerHtml($option);
							$b[$option['identifier']->__toString()] = $y;
							// echo "column:0 row:".$y."<br>".$table[0][$y]."<br>";
							$y++;
						}
						

						/**
						* fetchinng mappings(associations)
						*/
						$answers = $xml->responseDeclaration[0]->correctResponse[0]->value;
						foreach ($answers as $key => $answer) {
							$identifiers = explode(' ', $answer->__toString());
							$x = $a[$identifiers[0]];						
							$y = $b[$identifiers[1]];						
							$table[$x][$y] = 1;
						}
						

						/**
						* creating options array to save table in DB
						*/
						$options = array();
						$n = 1;
						for ($x=0; $x < $columns; $x++) { 
							for ($y=0; $y < $rows; $y++) { 
								$opt = new stdclass();
								$opt->option = $table[$x][$y];
								$opt->answer = $n;
								$options[] = $opt;
							}
							$n++;
						}
						unset($options[0]); // deleting fisrt variable from array			

						// print_r($options);
						// die();

						unset($body->matchInteraction[0]);
						
					}
					break;
				
				default:
					$result->status = 0;
					$result->message = 'QTI can\'t import this type of Question.';
					break;
			}
			
			//changing the image src value and moving the images to new location
			$tmp = $body->asXML();
			$tmp = new SimpleXMLElement($tmp);
			$images = $tmp->xpath('//img');
			if($images) {
				foreach($images as $key => $img) {
					$oldLocation = $folder.'/resources/'.$img['src'];
					@$imageFileName = end(explode('/', $img['src']));
					$newLocation = '../admin/uploads/question-images/'.$folder.$imageFileName;
					//echo $key;
					if(@copy($oldLocation, $newLocation)) {
						@$images[$key]['src'] = $newLocation;
					}
				}
			}
			$question = $this->innerHtml($tmp);
			if($explain == null) {
				$explain = "";
			}
			//changing data according to normal add question function
			$obj = new stdClass();
			$obj->questionData = $question;
			$obj->DescriptionData = $explain;
			$obj->questionType = $type;
			$obj->parent_id = 0;
			$obj->class_id = $info->class_id;
			$obj->options = $options;
			$obj->subject_ids = $info->subject_ids;
			$obj->unit_ids = $info->unit_ids;
			$obj->chapter_ids = $info->chapter_ids;
			$obj->topic_ids = $info->topic_ids;
			$obj->tag_ids = $info->tag_ids;
			$obj->difficultyLevel_ids = $info->difficultyLevel_ids;
			$obj->skill_ids = $info->skill_ids;
			$obj->user = $info->user;
			$obj->userId = $info->userId;
			// print_r($obj);
			// die();
			//class for all qti import and export functions
			$q = new Question();
			$result = $q->addQuestion($obj);
			// $result->status = 1;
			// print_r($result);
			return $result;
		}else {
			$result->status = 0;
			$result->message = 'Single file not found';
		}
	}

	/**
	* Function to get the inner xml of xml object
	* @author Rupesh Pandey
	* @date 08/06/2015
	* @param SimpleXMLObject
	* @return string inner data
	*/
	public function innerHtml($object) {
		$string = '';
		foreach($object->children() as $child) {
			$string .= $child->asXML();
		}
		return $string;
	}

	/**
	* Funtion to delete a non-empty directory from linux system
	*
	* @author Rupesh Pandey
	* @date 04/06/2015
	* @param string directory name to be deleted
	* @return null
	*/
	public function deleteFolder($folder) {
	    foreach(scandir($folder) as $file) {
	        if ('.' === $file || '..' === $file)
	        	continue;
	        if (is_dir("$folder/$file"))
	        	$this->deleteFolder("$folder/$file");
	        else
	        	unlink("$folder/$file");
	    }
	    rmdir($folder);
	}

	/**
	* Function to convert html tags into not xml tags
	*
	* @author Rupesh Pandey
	* @date 08/06/2015
	* @param string xml file content
	* @return string converted string
	*/
	public function sanitizeHTML($string) {
		return $string;
	}


	/**
	* Function to convert html tags into not xml tags
	*
	* @author Rupesh Pandey
	* @date 08/06/2015
	* @param string xml file content
	* @return string converted string
	*/
	public function findQuestionType($cardinality,$baseType) {
		switch ($cardinality.'|'.$baseType) {
			case 'single|identifier':
				$type = 1;
				break;
		
			case 'multiple|identifier':
				$type = 2;
				break;
		
			case 'ordered|identifier':
				$type = 4;
				break;
		
			case 'single|pair':
				$type = 5;
				break;
		
			case 'multiple|directedPair':
				$type = 6;
				break;
		
			case 'multiple|directedPair':
				$type = 6;
				break;

			default:
				$type = 0;
				break;
		}
		return $type;

	}
}
?>