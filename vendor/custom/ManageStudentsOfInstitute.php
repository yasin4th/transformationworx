<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	//including connection.php file to make connection with database
	require_once 'Connection.php';

	class ManageStudentsOfInstitute extends Connection{


		public function __construct(){

			$this->db = new DB();
			parent::__construct();
		}

		public function fetchInstituteStudents($data) {

			$result = new stdClass();
			try
			{
				$adapter = $this->adapter;

				//FETCH INSTITUTE ID NEW CODE
				$statement ="SELECT ins_lab_type From profile where user_id='{$data->userId}'";
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$ins_lab_type =  $run->toArray();
				$result->ins_lab_type = (int)($ins_lab_type[0]['ins_lab_type']);
				//FETCH INSTITUTE ID END NEW CODE

				$sql = new Sql($adapter);

				$select  = $sql->select();
				$uid = $data->userId;

				if($_SESSION['role'] == 6)
				{
					$statement ="SELECT DISTINCT(institute_id) AS ins_id From coupons where student_id='{$data->userId}'";
					$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$rows =  $run->toArray();
					if(count($rows))
					{
						$uid = $rows[0]['ins_id'];
					}
				}

				if(isset($data->studentId))
				{
					$statement ="SELECT users.first_name,users.last_name,users.email,profile.mobile From users LEFT JOIN profile ON profile.user_id = users.id where users.id='{$data->studentId}'";
						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
						$rows =  $run->toArray();
						$result->student = $rows['0'];
						$result->status = 1;
				}
				else{
						$sql = new Sql($adapter);

						$select->from(array('c' => 'coupons'))
							->join(array('u'=>'users'),'u.id = c.student_id', array('first_name' => new Expression('IFNULL(u.first_name,"")'),'last_name' => new Expression('IFNULL(u.last_name,"")'),'deleted'=> 'deleted','email' => new Expression('IFNULL(u.email,"")') ),'left')
							->join(array('p'=>'profile'),'p.user_id = c.student_id', array('mobile' => new Expression('IFNULL(p.mobile,"")')),'left')
							->join(array('tp'=>'test_program'),'tp.id = c.test_program_id', array('name' => new Expression('tp.name')),'left')
							->join(array('s'=>'student_program'),'s.user_id = c.student_id', array('totalpaper' => new Expression('IFNULL(s.allowed_total_paper,"")'),'totallab' => new Expression('IFNULL(s.allowed_total_lab,"")')),'left');
						$select->columns(array("student_id","test_program_id",));
						$select->where->equalTo('c.institute_id', $uid)
								->notEqualTo('c.student_id', '0')
								->equalTo('u.role', '4');

						if (!empty($data->program)) {
							$select->where->equalTo('c.test_program_id', $data->program);
						}

						if (!empty($data->email)) {
							$select->where->equalTo('u.email', $data->email);
						}

						if (!empty($data->phone)) {
							$select->where->equalTo('p.mobile', $data->phone);
						}

						$select->group('c.student_id');

						$statement = $sql->getSqlStringForSqlObject($select);

						$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
						$rows = $run->toArray();

						if(count($rows)) {

							$result->students = $rows;
							$result->student = $rows[0];

							$result->status = 1;
							$result->message = "Successfully Fetched.";
						}
						else {
							$result->status = 0;
							$result->message = "No students found.";
						}
				}
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}

		}

		public function fetchInstitutePrograms($data) {

			$result = new stdClass();
			try{

				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$statement = "SELECT program AS id, test_program.name, credits FROM institute_program LEFT JOIN test_program ON test_program.id = institute_program.program WHERE institute = ".$data->userId;
				// $statement = "SELECT DISTINCT(test_program_id) AS id, test_program.name FROM coupons LEFT JOIN test_program ON test_program.id = coupons.test_program_id WHERE institute_id = ".$data->userId;

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$rows = $run->toArray();
				if(count($rows)) {

					$result->programs = $rows;

					$result->status = 1;
					$result->message = "Successfully Fetched.";
				}
				else {
					$result->status = 0;
					$result->message = "You don't have any test programs.";
				}

				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}

		}

		public function addInstituteStudents($data) {

			$result = new stdClass();
			try{

				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'email' =>  htmlentities($data->email),
					'deleted'   => 0
				));
				$select->columns(array('email'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$res = $res->toArray();
				if(count($res) <= 0) {
					$insert_student = new TableGateway('users',$adapter,null,new HydratingResultSet);
					$insert_student->insert(array(
						'first_name' => $data->fname,
						'last_name' =>  $data->lname,
						'email' =>  $data->email,
						'password'  =>  md5($data->mobile),
						'registered_on'=>time(),
						'role'  =>  $data->role
					));

					$user_id = $insert_student->getLastInsertValue();
					$insert_profile = new TableGateway('profile', $adapter, null, new HydratingResultSet());
					$insert_profile->insert(array(
						'user_id'    => $user_id,
						'email'      => $data->email,
						'first_name' => $data->fname,
						'last_name'  => $data->lname,
						'mobile'     =>  $data->mobile,
						'registered_on'=>time()
					));

					/*if(isset($data->testprograms))
					{
						$testprograms = $data->testprograms;
					}else{
						$testprograms ="";
					}*/


					$insert_coupons = new TableGateway('coupons', $adapter, null, new HydratingResultSet());
					$insert_coupons->insert(array(
						'student_id' => $user_id,
						// 'test_program_id' => $testprograms,
						'institute_id' => $data->userId,
						'created'   =>  time(),
					));

					/*
					if($data->role == 4)
					{
						$this->db->_insert('student_program', array(
							'user_id' => $user_id,
							'test_program_id' => $testprograms,
							'allowed_total_paper'   =>  $data->totalpaper,
							'allowed_total_lab' =>  $data->lab,
							'valid_till' => $data->expire_date,
						));
					}
					*/
					$result->status = 1;
					$result->message = "Operation Successful";

				}else{
					$result->status = 0;
					$result->message = "E-mail already registered.";
				}

				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}

		}

		public function fetchInstituteAssistant ($data){
			$result = new stdClass();
			try{

				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				if(isset($data->assistid))
				{
					$assistid = $data->assistid;
					$statement = "SELECT users.first_name,users.last_name, IFNULL(users.email,'') AS email, profile.mobile ,coupons.student_id FROM coupons LEFT JOIN users ON users.id = coupons.student_id LEFT JOIN profile ON profile.user_id = coupons.student_id WHERE coupons.student_id= '{$data->assistid}' AND users.role = 6 AND coupons.deleted=0 GROUP BY student_id";
				} else{

					$statement = "SELECT users.first_name,users.last_name, IFNULL(users.email,'') AS email, profile.mobile ,coupons.student_id FROM coupons LEFT JOIN users ON users.id = coupons.student_id LEFT JOIN profile ON profile.user_id = coupons.student_id WHERE coupons.institute_id= '{$data->userId}' AND users.role = 6 AND coupons.deleted=0 GROUP BY student_id";
				}


				// $statement ="SELECT DISTINCT (institute_id), concat(IFNULL(users.first_name,''),' ',IFNULL(users.last_name,'')) AS name, IFNULL(users.email,'') AS email,profile.mobile  from users LEFT join profile on profile.user_id = users.id  where users.role=6";

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$rows = $run->toArray();
				if(count($rows)) {

					$result->assistants = $rows;
					$result->assists = $rows[0];

					$result->status = 1;
					$result->message = "Successfully Fetched.";
				}
				else {
					$result->status = 0;
					$result->message = "No Assistant found.";
				}

				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}
		}

		public function studentLogin ($data){
			$result = new stdClass();
			try
			{
				@session_start();
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$_SESSION['role'] = 4;
				$_SESSION['userId'] = $data->id;
				$_SESSION['assistant'] = 1;

				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'id' =>  $data->id,
					'deleted'   => 0
				));

				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$res = $res->toArray();

				if(count($res))
				{
					$_SESSION['user'] = $res[0]['email'];
					$_SESSION['username'] = $res[0]['first_name'];
				}

				$result->status = 1;
				$result->message = "Successfully Fetched.";
				return $result;
			}
			catch(Exception $e)
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}
		}

	public function updateAssistantDetails($data){
		$result = new stdClass();
		try{

			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('users');
			$select->where->equalTo('email', htmlentities($data->email))
							->equalTo('deleted', '0')
							->notEqualTo('id', $data->id);

			$select->columns(array('email'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$res = $res->toArray();
			if(count($res) <= 0) {
				$update = $sql->update();
				$update->table('users');
				$update->set(array(
					"first_name" => htmlentities($data->fname),
					"last_name" => htmlentities($data->lname),
					"email" => htmlentities($data->email),
					"password" => md5($data->password),
				));
				$update->where(array(
					"id"    =>  $data->id
				));

				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);


				$update = $sql->update();
				$update->table('profile');
				$update->set(array(
					"email" => htmlentities($data->email),
					"first_name" => htmlentities($data->fname),
					"last_name" => htmlentities($data->lname),
					"mobile" => $data->mobile,
				));
				$update->where(array(
					"user_id"   =>$data->id
				));

				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$result->status = 1;
				$result->message = "Operation Successful";

			}else{
				$result->status = 0;
				$result->message = "E-mail already registered.";
			}

			return $result;
		}
		catch(Exception $e) {
			$result->status = 0;
			$result->message = "ERROR: Some thing Went Wrong.";
			$result->error = $e->getMessage();

			return $result;
		}
	}

	public function updateAssistantPassword($data){
		$result = new stdClass();
		try{

			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('users');
			$update->set(array(
				"password" => md5($data->password),
			));
			$update->where(array(
				"id"    => $data->userId
			));

			$statement = $sql->getSqlStringForSqlObject($update);
			$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			$result->status = 1;
			$result->message = "Operation Successful";

			return $result;
		}
		catch(Exception $e) {
			$result->status = 0;
			$result->message = "ERROR: Some thing Went Wrong.";
			$result->error = $e->getMessage();

			return $result;
		}
	}

	public function DeleteAssistantDetails($data){
		$result = new stdClass();

		try{

			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			$update = $sql->update();
			$update->table('users');
			$update->set(array(
				"deleted" => $data->deleted,
			));
			$update->where(array(
				"id"    =>$data->id
			));

			$statement = $sql->getSqlStringForSqlObject($update);
			$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);


			$update = $sql->update();
			$update->table('coupons');
			$update->set(array(
				"deleted" => $data->deleted,
			));
			$update->where(array(
				"student_id"    =>$data->id
			));

			$statement = $sql->getSqlStringForSqlObject($update);
			$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

			$result->status = 1;
			$result->message = "Operation Successful";

			return $result;
		}
		catch(Exception $e) {
			$result->status = 0;
			$result->message = "ERROR: Some thing Went Wrong.";
			$result->error = $e->getMessage();

			return $result;
		}
	}

	public function assignProgramStudent($data)
	{

		$result = new stdClass();
		try{
			$adapter = $this->adapter;

			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('profile');
			$select->where->equalTo('user_id', $data->userId);

			$select->columns(array('credits','first_name', 'last_name'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$res = $res->toArray();

			if(count($res) > 0 && $res[0]['credits'] == 0){
				$result->status = 0;
				$result->message = "Not enough credits.";
				return $result;
			}

			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('users');
			$select->where->equalTo('id', $data->studentid);

			$select->columns(array('first_name', 'last_name'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$res = $res->toArray();
			$user = '';
			if(count($res) > 0){
				$user = ' to Student '.$res[0]['first_name'] . ' '. $res[0]['last_name'];
			}

			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('student_program');
			$select->where->equalTo('user_id', $data->studentid)->equalTo('test_program_id', $data->program);

			$select->columns(array('id'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$res = $res->toArray();

			if(count($res) > 0){
				$result->status = 1;
				$result->message = "Already assigned program.";
				return $result;
			}


			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('test_program');
			$select->where->equalTo('id', $data->program);

			$select->columns(array('credits','name'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$res = $res->toArray();

			if(count($res)) {
				$credits = $res[0]['credits'];
				$program_name = $res[0]['name'];
			}

			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('profile');
			$update->set(array(
				'credits'	=>	new Expression('credits-'.$credits)
			));
			$update->where(array(
				'user_id' => $data->userId
			));

			$selectString = $sql->getSqlStringForSqlObject($update);

			$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

			// insert into notifications table

			$insert = new TableGateway('notifications', $adapter, null, new HydratingResultSet());
			$insert->insert([
				'institute_id' => $data->userId,
				'message'      => "{$credits} Credits debited. Assigned {$program_name} {$user}",
				'status'       => 1,
				'created_at'   => date('Y-m-d H:i:s')
			]);

			$id = $this->db->_iou('student_program',
				array('test_program_id'=>$data->program, 'user_id'=>$data->studentid),
				array(
					'test_program_id'=>$data->program,
					'user_id'=>$data->studentid,
					'allowed_total_paper'=>$data->assigntotalpaper,
					'allowed_total_lab'=>$data->assigninlab,
					'valid_till'=>$data->expire_date,
					'time' => time()
				));

			$coupons_id = $this->db->_iou('coupons',
				array('test_program_id'=>$data->program, 'student_id'=>$data->studentid),
				array(
					'test_program_id'=>$data->program,
					'student_id'=>$data->studentid,
					'institute_id' => $data->userId,
					'created' => time()
				));


			if($coupons_id){
				$result->status = 1;
				$result->message = "Operation Successful";
				return $result;
			}

		}
		catch(Exception $e)
		{
			$result->status = 0;
			$result->message = "ERROR: Some thing Went Wrong.";
			$result->error = $e->getMessage();
		}

	}

	public function UpdateStudentDetails($data)
	{
		$result = new stdClass();
		try{
			$id = $this->db->_iou('users',
				array('id'=>$data->id),
				array(
					'first_name'=>$data->fname,
					'last_name'=>$data->lname,
					'password'=>md5($data->password),
				));

			$id = $this->db->_iou('profile',
				array('user_id'=>$data->id),
				array(
					'first_name'=>$data->fname,
					'last_name'=>$data->lname,
					'mobile'=>$data->mobile,
				));

			if($id){
				$result->status = 1;
				$result->message = "Operation Successful";
			}

			return $result;
		}
		catch(Exception $e)
		{
			$result->status = 0;
			$result->message = "ERROR: Some thing Went Wrong.";
			$result->error = $e->getMessage();
		}

	}

	public function fetchStudentsProgram($data) {

		$result = new stdClass();
		try{

			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			$statement = "SELECT  user_id as id ,allowed_total_paper AS total_paper,allowed_total_lab AS total_lab, test_program.id As program_id, student_program.valid_till as vaild ,test_program.name as prname FROM student_program LEFT JOIN test_program ON test_program.id = student_program.test_program_id WHERE user_id = ".$data->user_Id;
			$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			$rows = $run->toArray();

			require_once 'LaunchApi.php';
			$ob = new LaunchApi();

			if(count($rows))
			{
				foreach ($rows as $key => $value) {

					$totalAttempts = $ob->validateAttempts($value['id'], $value['program_id'],0,1);
					$totalLabAttempts = $ob->validateAttempts($value['id'], $value['program_id'],1,1);
					$rows[$key]['total_attempts'] = $totalAttempts;
					$rows[$key]['total_lab_attempts'] = $totalLabAttempts;
				}
				$result->programs = $rows;
				if(isset($data->ins_lab_type)){
					$result->ins_lab_type = $data->ins_lab_type;
				}
				$result->status = 1;
				$result->message = "Successfully Fetched.";
			}
			else {
				$result->status = 0;
				$result->message = "You don't have any test programs.";
			}

			return $result;
		}
		catch(Exception $e) {
			$result->status = 0;
			$result->message = "ERROR: Some thing Went Wrong.";
			$result->error = $e->getMessage();

			return $result;
		}
	}

	public function fetchTestProgram($data){
		$result = new stdClass();
		try{

			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			$statement = "SELECT  user_id as id ,allowed_total_paper AS total_paper,allowed_total_lab AS total_lab,test_program.id As program_id, student_program.valid_till as vaild ,test_program.name as prname FROM student_program LEFT JOIN test_program ON test_program.id = student_program.test_program_id WHERE user_id = $data->user_Id AND test_program_id= $data->program_id";
			$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			$rows = $run->toArray();
			require_once 'LaunchApi.php';
			$ob = new LaunchApi();
			if(count($rows)) {

				foreach ($rows as $key => $value) {

					$totalAttempts = $ob->validateAttempts($value['id'], $value['program_id'],0,1);
					$totalLabAttempts = $ob->validateAttempts($value['id'], $value['program_id'],1,1);
					$rows[$key]['total_attempts'] = $totalAttempts;
					$rows[$key]['total_lab_attempts'] = $totalLabAttempts;
				}
				$result->programs = $rows['0'];
				$result->status = 1;
				$result->message = "Successfully Fetched.";
			}
			else {
				$result->status = 0;
				$result->message = "You don't have any test programs.";
			}

			return $result;
		}
		catch(Exception $e) {
			$result->status = 0;
			$result->message = "ERROR: Some thing Went Wrong.";
			$result->error = $e->getMessage();

			return $result;
		}
	}

	public function updateStudentProgram($data) {
		$result = new stdClass();
		try
		{
			// $adapter = $this->adapter;

			// $sql = new Sql($adapter);
			// $select = $sql->select();

			// $select->from('student_program');
			// $select->where->equalTo('test_program_id', $data->program_id)
			//               ->equalTo('user_id', $data->id);

			// $selectString = $sql->getSqlStringForSqlObject($select);
			// $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			// $programs = $run->toArray();

			// if(!count($programs))
			// {
			//     $result->status = 0;
			//     $result->message = "ERROR: Some thing Went Wrong.";
			//     $result->error = $e->getMessage();

			//     return $result;
			// }
			// else
			// {
			//     $allowed_total_paper = $programs[0]['allowed_total_paper'];
			//     $allowed_total_lab = $programs[0]['allowed_total_lab'];
			// }

			require_once 'LaunchApi.php';
			$ob = new LaunchApi();
			$totalAttempts = $ob->validateAttempts($data->id, $data->program_id,0,1);
			$totalLabAttempts = $ob->validateAttempts($data->id, $data->program_id,1,1);


			$total = $data->totalpaper + $totalAttempts;
			$lab = $data->inlab + $totalLabAttempts;


			$this->db->_iou('student_program',
				['user_id' => $data->id, 'test_program_id' => $data->program_id],
				['allowed_total_paper' => $total,'allowed_total_lab' => $lab, 'valid_till' => $data->expire_date]);
			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			$result->status = 1;
			$result->message = "Successfully update";


			return $result;
		}
		catch(Exception $e) {
			$result->status = 0;
			$result->message = "ERROR: Some thing Went Wrong.";
			$result->error = $e->getMessage();

			return $result;
		}
	}


	public function fetchNotifications($data) {

			$result = new stdClass();
			try{

				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$statement = "SELECT * FROM notifications WHERE institute_id = {$data->userId} order by id desc";

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$rows = $run->toArray();


				$sql = new Sql($adapter);

				$statement = "SELECT * FROM profile WHERE user_id = {$data->userId}";

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$profile = $run->toArray();

				if(count($rows)) {

					$result->notifications = $rows;
					$result->user = $profile;
					$result->status = 1;
					$result->message = "Successfully Fetched.";
				}
				else {
					$result->status = 0;
					$result->message = "You don't have any notifications.";
				}

				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}

		}

		public function addCredits($data) {
			$result = new stdClass();

			try{
				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('profile');
				$update->set(array(
					'credits'	=>	new Expression('credits+'.$data->credits)
				));
				$update->where(array(
					'user_id' => $data->id
				));

				$selectString = $sql->getSqlStringForSqlObject($update);

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);


				$insert = new TableGateway('notifications', $adapter, null, new HydratingResultSet());
				$insert->insert([
					'institute_id' => $data->id,
					'message'      => "{$data->credits} Credits added by admin.",
					'status'       => 1,
					'created_at'   => date('Y-m-d H:i:s')
				]);

				$result->status = 1;
				$result->message = "Credits added.";

				return $result;
			}catch(Exception $e) {

				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();

				return $result;
			}
		}

}

