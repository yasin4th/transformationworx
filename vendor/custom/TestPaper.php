<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	require_once 'Connection.php';
	require_once("TestPaperQuestions.php");


	class TestPaper extends Connection {

		public function createTestPaper($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				// INSERT INTO `test_paper`(`name`, `Instructions`, `user_id`, `created`) VALUES ("untitled","Please Fill Instructions.",$data->user_id,time())

				$insert_test_paper = new TableGateway('test_paper', $adapter, null, new HydratingResultSet());

				$insert_test_paper->insert(array(
                    'name'         => "untitled",
                    'instructions' => "Please Fill Instructions.",
                    'user_id'      => $data->userId,
                    'created'      => time()
				));
				$test_paper_id = $insert_test_paper->getLastInsertValue();
				//echo $test_paper_id;
				$insert_section = new TableGateway('section', $adapter, null, new HydratingResultSet());
				foreach($data->sections as $section) {

					// INSERT INTO `section`(`name`, `total_question`, `test_paper`) VALUES ('sec_name','Total_question','test_paper_id')
					// _print_r($section);
					if($section->qc_status !=""){
						$insert_section->insert(array(
	                        'name'           => $section->sectionName,
	                        'cut_off_marks'  => $section->cut_off_marks,
	                        'total_question' => $section->totalQuestion,
	                        'qc_status' 	 => $section->qc_status,
	                        'test_paper'     => $test_paper_id
						));
					}
					else{
						$insert_section->insert(array(
	                        'name'           => $section->sectionName,
	                        'cut_off_marks'  => $section->cut_off_marks,
	                        'total_question' => $section->totalQuestion,
	                        'test_paper'     => $test_paper_id
						));
					}
					$section_id = $insert_section->getLastInsertValue();
					// echo $section_id.'<br>';
					$insert_section_lables = new TableGateway('test_paper_labels', $adapter, null, new HydratingResultSet());
					foreach($section->questions as $question) {

						// INSERT INTO `test_paper_labels`(`section_id`, `class_id`, `subject_id`, `unit_id`, `chapter_id`, `topic_id`, `question_type`, `number_of_question`, `difficulty_level`, `skill_id`, `max_marks`, `neg_marks`) **ALL VALUES()**
						// _print_r($question);
						$insert_section_lables->insert(array(
                            'section_id'         => $section_id,
                            'class_id'           => $question->classId,
                            'subject_id'         => $question->subjectId,
                            'unit_id'            => $question->unitId,
                            'chapter_id'         => $question->chapter,
                            'topic_id'           => $question->topic,
                            'question_type'      => $question->questionType,
                            'difficulty_level'   => $question->difficulty,
                            'skill_id'           => $question->skill,
                            'max_marks'          => $question->maxMark,
                            'neg_marks'          => $question->negMark,
                            'number_of_question' => $question->numberOfQuestions
						));

						$lable_id = $insert_section_lables->getLastInsertValue();
						$insert_tags_of_lable = new TableGateway('tags_of_test_paper_label', $adapter, null, new HydratingResultSet());
						if($question->Tags != 0)
						foreach($question->Tags as $tag) {

							// INSERT INTO `tags_of_test_paper_label`(`test_paper_label_id`, `tag_id`) VALUES ()

							$insert_tags_of_lable->insert(array(
								'test_paper_label_id' => $lable_id,
								'tag_id'	=>	$tag
							));
						}
					}
				}

				//require_once 'PaperAlgo.php';

				$result->test_paper = $test_paper_id;
				$result->status = 1;
				$result->message = 'ok';

				return $result;
			}catch(Execption $e) {
				$result->status	 =	0;
				$result->message =	$e->getMessage();
				return $result;
			}
		}


		public function fetchTestPaper($data) {
			$result = new stdClass();
			try {

				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();

				$select->from("test_paper")
						->join(array('test_paper_type' => 'test_paper_type'), 'test_paper_type.id = test_paper.type', array('type'), 'left')
						//->join(array('test_paper_questions' => 'test_paper_questions'), 'test_paper_questions.test_paper_id = test_paper.id', array('questions' => new Expression('COUNT(test_paper_questions.test_paper_id)')), 'left')
						->join(array('test_paper_subjects' => 'test_paper_subjects'), 'test_paper_subjects.test_paper_id = test_paper.id', array(), 'left');
						//->join(array('questions' => 'questions'), 'questions.id = test_paper_questions.question_id', array(), 'left');
				$select->columns(array("id","name","type", "status"));
				$select->order('test_paper.id DESC');
				// $select->GROUP('test_paper.id');
				$where = new $select->where();
				$where->equalTo("test_paper.deleted", 0);
				//$where->equalTo("questions.parent_id", '0');
				$where->notEqualTo("test_paper.type", '0');
				//$where->equalTo("parent", 0);


				if (isset($data->class)) {
					$where->equalTo('test_paper.class', $data->class);
				}
				if (isset($data->type)) {
					$where->equalTo('test_paper.type', $data->type);
				}

				if (isset($data->subjects)) {
					$where->in('test_paper_subjects.subject_id', $data->subjects);
				}

				if (isset($data->test_paper)) {
					$where->like('test_paper.name', "%$data->test_paper%");
				}

				$select->where($where);
				$select->group('test_paper.id');
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$test_paper = $run->toArray();

				foreach ($test_paper as $key => $paper) {
					# code...
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('section')
							->join(array('test_paper_labels'=>'test_paper_labels'),'test_paper_labels.section_id=section.id',array('questions'=>new Expression('SUM(number_of_question)')),'left');
					$where = array();
					$where['section.test_paper']=$paper['id'];
					$select->where($where);
					$select->group('test_paper');
					$select->columns([]);

					$selectString = $sql->getSqlStringForSqlObject($select);


					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$count = $run->toArray();

					if(count($count)){
						$test_paper[$key]['questions'] = $count[0]['questions'];
					}
				}

				$result->status = 1;
				$result->message = "Operation Successful.";
				$result->data = $test_paper;
				return $result;
			}catch(Execption $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		public function fetchTestPaperDetails($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				// questions
				$select->from("test_paper");

					// ->join(array(), '', 'left');
				$select->where(array("test_paper.deleted"	=>	0,"test_paper.id"	=>	$data->test_paper_id));
				$select->columns(array("id", "name", "type", "class", "time", "total_attempts", "start_time", "end_time", "instructions", "status", "mode"));
				// $select->order('id DESC');
				$statement = $sql->getSqlStringForSqlObject($select);
				// die($statement);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$test_paper_details = $run->toArray();

				$selectSubjectsList = $sql->select();
				$selectSubjectsList->from('subjects');
				$selectSubjectsList->columns(array('id','name'));
				$where = new $selectSubjectsList->where();
				$where->equalTo('deleted',0);
				$where->equalTo('class_id',$test_paper_details[0]['class']);
				$selectSubjectsList->where($where);
				$selectString = $sql->getSqlStringForSqlObject($selectSubjectsList);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjectsList = $run->toArray();

				$selectedSubjects = $sql->select();
				$selectedSubjects->from('test_paper_subjects');
				$selectedSubjects->columns(array('subject_id'));
				$where = new $selectedSubjects->where();
				$where->equalTo('test_paper_id',$data->test_paper_id);
				$selectedSubjects->where($where);
				$selectString = $sql->getSqlStringForSqlObject($selectedSubjects);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects = $run->toArray();

				$result->data = $test_paper_details[0];

				$result->data['subjects'] = array();
				foreach ($subjects as $value) {
					array_push($result->data['subjects'], $value['subject_id']);
				}
				$result->subjectsList = $subjectsList;

				$result->status = 1;
				$result->message = "Operation Successful.";
				return $result;
			} catch(Execption $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function fetchTestPaperTypes($data) {

			$result = new stdClass();
			try {

				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();

				$select->from("test_paper_type");
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->data = $run->toArray();
				$result->status = 1;
				$result->message = 'Test Paper Types';
				return $result;
			}
            catch(Execption $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function getNextQuestion($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				// return $data;
				$select->from("test_paper_labels");
				$select->where(array(
					"id" => $data->lableId
				));
				$select->columns(array("section_id","class_id","subject_id","unit_id","chapter_id","topic_id","question_type","difficulty_level","skill_id"));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$lable =  $run->toArray();

				$select = $sql->select();
				$select->from('tags_of_test_paper_label');
				$select->where(array(
					'test_paper_label_id' => $data->lableId
				));
				$select->columns(array('tag_id'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$tags  = $run->toArray();
				$lable[0]["number_of_question"] = 1;
				$lable[0]["notIn"] = $data->notIn;
				$lable[0]["tags"] = array();

				foreach ($tags as $tag) {
					array_push($lable[0]["tags"],$tag["tag_id"]);
				}
				$test = new TestPaperQuestions();
				$temp =  $test->questionFilter($lable[0]);
				if ($temp->status == 1) {
					$result->data = $temp->data;
					$result->status = 1;
					$result->message = 'Operation Successful.';
				} else {
					$result->status = 0;
					$result->message = 'Operation Failed.'.$temp->message;
				}
				return $result;
			}catch(Execption $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function importQuestion($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				// return $data;
				$select->from("test_paper_labels");
				$select->where(array(
					"id" => $data->lableId
				));
				$select->columns(array("section_id","class_id","subject_id","unit_id","chapter_id","topic_id","question_type","difficulty_level","skill_id"));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$lable =  $run->toArray();

				$select = $sql->select();
				$select->from('tags_of_test_paper_label');
				$select->where(array(
					'test_paper_label_id' => $data->lableId
				));
				$select->columns(array('tag_id'));
				$statement = $sql->getSqlStringForSqlObject($select);


				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$tags  = $run->toArray();
				if(!empty($data->count))
					$lable[0]["number_of_question"] = $data->count;
				$lable[0]["notIn"] = $data->notIn;
				$lable[0]["tags"] = array();

				foreach ($tags as $tag) {
					array_push($lable[0]["tags"],$tag["tag_id"]);
				}
				$test = new TestPaperQuestions();
				$temp =  $test->questionFilter($lable[0]);
				if ($temp->status == 1) {
					$result->data = $temp->data;
					$result->status = 1;
					$result->message = 'Operation Successful.';
				}else {
					$result->status = 0;
					$result->message = 'Operation Failed.'.$temp->message;
				}
				return $result;
			}catch(Execption $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function fetchTestPapersOfPragram($request) {
			$result =  new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from(array('test_papers_of_test_program' => 'test_papers_of_test_program'))
						->join(array('test_paper' => 'test_paper'), 'test_papers_of_test_program.test_paper_id = test_paper.id',
                            array('id', 'name', 'type', 'class', 'time', 'total_attempts', 'start_time', 'end_time', 'Instructions'), 'left');

				$where = new $select->where();
				$where->equalTo('test_papers_of_test_program.test_program_id', $request->program_id);
				$select->columns(array());
				$select->where($where);
				$statement = $sql->getSqlStringForSqlObject($select);
				// die($statement);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->test_papers = $run->toArray();

				$result->status = 1;
				$result->message = 'Successfully Fetched';
				return $result;
			}catch(Execption $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function fetchQuestionInformation($data) {
			$result =  new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$statement = 'SELECT (CASE
								WHEN tag_type = 1 THEN (SELECT name FROM subjects WHERE id=tagged_id)
								WHEN tag_type = 2 THEN (SELECT name FROM `units` WHERE id=tagged_id)
								WHEN tag_type = 3 THEN (SELECT name FROM `chapters` WHERE id=tagged_id)
								WHEN tag_type = 4 THEN (SELECT name FROM `topics` WHERE id=tagged_id)
								WHEN tag_type = 5 THEN (SELECT tag FROM tags WHERE id=tagged_id)
								WHEN tag_type = 6 THEN (SELECT difficulty FROM difficulty WHERE id=tagged_id)
								WHEN tag_type = 7 THEN (SELECT skill FROM skills WHERE id=tagged_id)
								END) as name,tag_type,(SELECT (SELECT name FROM classes WHERE class_id=id) FROM questions where tagsofquestion.question_id = questions.id) AS class_name FROM `tagsofquestion` WHERE question_id ='.$data->question_id;


				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->tagged_data = $run->toArray();


				$result->status = 1;
				$result->message = 'ok';
				return $result;
			}catch(Execption $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		 public function saveTestPaperPackage($data) {
            $result = new stdClass();
            $not_inserted = '';
            try{
                $adapter = $this->adapter;
                foreach ($data->testpackage as $key) {
                	foreach ($data->testpackagePapers as $id) {

	                $sql = new Sql($adapter);
	                $select = $sql->select();
					$select->from('test_paper');
					$select->where->equalTo('id', $id);
					$select->columns(array('id','name','type','class'));
				    $selectString = $sql->getSqlStringForSqlObject($select);
					$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$temp = $temp->toArray();

	                $select = $sql->select();
					$select->from('test_papers_of_test_program');
					$select->where->equalTo('test_paper_id', $id);
					$select->where->equalTo('test_program_id', $key);
					$selectString2 = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($selectString2, $adapter::QUERY_MODE_EXECUTE);
					$rows = $run->toArray();

					if(count($rows)==0) {

						$insert = new TableGateway('test_papers_of_test_program', $adapter, null, new HydratingResultSet());
	                    $insert->insert(array(
	                        'test_paper_id' => $id,
	                        'test_program_id'   => $key,
	                        'parent_type'   => $data->parent_type,
	                        'test_paper_type_id'=>$temp[0]['type'],
	                        'parent_id'=>1,
	                        'created'   =>  time()
                        ));

	                }
	                else{
	                	$not_inserted .= $temp[0]['name'].",";
	                	$not_inserted = rtrim($not_inserted,',');
	                	$result->status = 1;
			            $result->message = "Unsuccessfully Updated.";
	                }

                }

                }

                 $not_inserted .= $temp[0]['name'].",";
	                	$not_inserted = rtrim($not_inserted,',');
	                	$result->status = 1;
			            $result->message = "Unsuccessfully Updated.";
			            $result->message1 = $not_inserted;
			            return $result;
            }
            catch(Exception $e){
                $result->status = 0;
                $result->message = $e->getMessage();
                return $result;
            }
        }

        /**
		* function to copy a test paper
		*
		* @param JSON
		* @return jeson with status and all data
		**/
		public function copyTestPaper($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				/*
				*	here we will fetch test paper related data
				*/
				$select = $sql->select();
				$select->from('test_paper');
				$where = new $select->where();
				$where->equalTo('deleted',0);
				$where->equalTo('status',1);
				$where->equalTo('id',$data->test_paper_id);
				$select->where($where);
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$test_paper = $run->toArray();

				/*
				*	now creating duplicate test paper 
				*/
				$insertTeatPaper = new TableGateway('test_paper', $adapter, null, new HydratingResultSet());
				// die(var_dump($test_paper[0]['type']));
				$insertTeatPaper->insert(array(
					'name' => 'untitled',
					'Instructions' => 'Please Fill Instructions.',
					'status' => 1,
					'type' => $test_paper[0]['type'],
					'user_id' => $_SESSION['userId'],
					'created' => time(),
					'deleted' => 0,
				));
				$new_test_paper_id = $insertTeatPaper->getLastInsertValue();
				/*
				*	here we will duplicate section id
				*/
				$select = $sql->select();
				$select->from('section');
				$where = new $select->where();
				$where->equalTo('test_paper',$data->test_paper_id);
				$where->equalTo('deleted',0);
				$select->where($where);
				// $select->columns(array('name','total_question','cut_off_marks','qc_status'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$sections = $run->toArray();



				if(!empty($sections)) {
					// creating query of Insert sections
					foreach($sections as $sec) {
						$insertSections = new TableGateway('section', $adapter, null, new HydratingResultSet());
						if($sec['qc_status'] != NULL){
							$insertSections->insert(array(
								'name' => $sec['name'],
								'total_question' => $sec['total_question'],
								'cut_off_marks' => $sec['cut_off_marks'],
								'qc_status' => $sec['qc_status'],
								'test_paper' => $new_test_paper_id,
								'deleted' => 0,
							));
						}
						else{
							$insertSections->insert(array(
								'name' => $sec['name'],
								'total_question' => $sec['total_question'],
								'cut_off_marks' => $sec['cut_off_marks'],
								'test_paper' => $new_test_paper_id,
								'deleted' => 0,
							));
						}

					}
				}

				/*
				*	here we will duplicate test_paper_labels
				*/
				$statement = 'SELECT id AS section_ids FROM `section` WHERE test_paper ='.$new_test_paper_id. ' AND deleted = 0 ORDER BY id ASC';


				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$new_section_ids = $run->toArray();
				$statement = 'SELECT id AS section_ids FROM `section` WHERE test_paper ='.$data->test_paper_id. ' AND deleted = 0 ORDER BY id ASC';

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$old_section_ids_2d = $run->toArray();
				$old_section_ids = array();
				foreach ($old_section_ids_2d as $kk => $old_sec_id_value) {
					array_push($old_section_ids, $old_sec_id_value['section_ids']);
				}

				$select = $sql->select();
				$select->from('test_paper_labels');
				$where = new $select->where();
				$where->in('section_id', $old_section_ids);
				$select->where($where);
				$select->order(array('id ASC'));
				// $select->columns(array('name','total_question','cut_off_marks','qc_status'));
				$statement = $sql->getSqlStringForSqlObject($select);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$test_paper_labels = $run->toArray();

				if(!empty($test_paper_labels)) {
					$i = 0;
					foreach($test_paper_labels as $tpl) {
						// creating query of Insert test_paper_labels
						$insertTestPaperLabels = new TableGateway('test_paper_labels', $adapter, null, new HydratingResultSet());
						$insertTestPaperLabels->insert(array(
							'section_id' => $new_section_ids[$i]['section_ids'],
							'class_id' => $tpl['class_id'],
							'subject_id' => $tpl['subject_id'],
							'unit_id' => $tpl['unit_id'],
							'chapter_id' => $tpl['chapter_id'],
							'topic_id' => $tpl['topic_id'],
							'question_type' => $tpl['question_type'],
							'number_of_question' => $tpl['number_of_question'],
							'difficulty_level' => $tpl['difficulty_level'],
							'skill_id' => $tpl['skill_id'],
							'max_marks' => $tpl['max_marks'],
							'neg_marks' => $tpl['neg_marks'],
						));
						$new_tpl_id = $insertTestPaperLabels->getLastInsertValue();

						/*
						*	here we will duplicate questions
						*/
						$new_section_id = $new_section_ids[$i]['section_ids'];

						//$old_section_id = $old_section_ids_2d[$i]['section_ids'];
						//$old_tpl_id = $tpl['id'];
                        //$new_test_paper_id; $data->test_paper_id;


                        $statement_tp_questions = 'SELECT * FROM `test_paper_questions` WHERE test_paper_id = '.$data->test_paper_id.' AND section_id = '.$old_section_ids_2d[$i]['section_ids']. ' AND lable_id ='.$tpl['id'];
						$run = $adapter->query($statement_tp_questions, $adapter::QUERY_MODE_EXECUTE);
						$tpq_data = $run->toArray();

						if(!empty($tpq_data)){
							foreach($tpq_data as $tpq_value){
								// creating query of Insert test_paper_questions
								$insert_test_paper_questions = "INSERT INTO `test_paper_questions`(`test_paper_id`,`section_id`,`lable_id`,`question_id`,`max_marks`,`neg_marks`,`created`) VALUES(".$new_test_paper_id.",".$new_section_id.",".$new_tpl_id.",".$tpq_value['question_id'].",".$tpq_value['max_marks'].",".$tpq_value['neg_marks'].",".time().")";
								$totplQuery = rtrim($insert_test_paper_questions,',');
								$run = $adapter->query($totplQuery, $adapter::QUERY_MODE_EXECUTE);
							}
						}


						/*$insert_test_paper_labels = "INSERT INTO `test_paper_labels`(`section_id`,`class_id`,`subject_id`,`unit_id`, `chapter_id`, `topic_id`, `question_type`, `number_of_question`, `difficulty_label`, `skill_id`, `max_marks`, `neg_marks`) VALUES ";

						$insert_test_paper_labels .= "(".$new_section_ids[0]['section_ids'].",".$tpl['class_id'].",".$tpl['subject_id'].",".$tpl['unit_id'].",".$tpl['chapter_id'].",".$tpl['topic_id'].",".$tpl['question_type'].",".$tpl['number_of_question'].",".$tpl['difficulty_label'].",".$tpl['skill_id'].",".$tpl['max_marks'].",".$tpl['neg_marks']."),";
						$tplQuery = rtrim($insert_test_paper_labels,',');
						$run = $adapter->query($tplQuery, $adapter::QUERY_MODE_EXECUTE);*/

						/*
						*	here we will duplicate tags_of_test_paper_label
						*/
						$statement_tag = 'SELECT tag_id AS tag_id FROM `tags_of_test_paper_label` WHERE test_paper_label_id = '.$tpl['id'];
						$run = $adapter->query($statement_tag, $adapter::QUERY_MODE_EXECUTE);
						$tag_id = $run->toArray();
						if(!empty($tag_id)){
							foreach($tag_id as $tag_value){
								// creating query of Insert tags_of_test_paper_label
								$insert_tags_of_test_paper_label = "INSERT INTO `tags_of_test_paper_label`(`test_paper_label_id`,`tag_id`) VALUES(".$new_tpl_id.",".$tag_value['tag_id'].")";
								$totplQuery = rtrim($insert_tags_of_test_paper_label,',');
								$run = $adapter->query($totplQuery, $adapter::QUERY_MODE_EXECUTE);
							}
						}
						$i++;
					}
				}

				$result->test_paper_id = $new_test_paper_id;
				$result->status = 1;
				$result->message = "Test Paper Coppied Successfully";
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


	}

?>