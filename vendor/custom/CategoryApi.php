<?php
	@session_start;
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	
	//including connection.php file to make connection with database
	require_once 'Connection.php';
	
	class CategoryApi extends Connection{
		
		/**
		* function fetch category details to show the category and their description to user on category.php file
		*
		* @author Utkarsh Pandey
		* @date 17-05-2015
		* @param JSON limit and offset
		* @return JSON status and data(category and description)
		**/
		public function getcategoryData($data) {
			
			$result = new stdClass();
			
			try{
				
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('category');
				$select->where(array(
					'deleted'	=>	0
				));
				// $select->limit($data->limit);
				// $select->offset($data->offset);
				$select->columns(array('id','name' => 'category', 'description',));
				
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				$result->status = 1;
				$result->data = $res;
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
			
		}
		
		
		/**
		* function add new category details row in category table
		*
		* @author Utkarsh Pandey
		* @date 17-05-2015
		* @param JSON with all required details
		* @return JSON status and message
		**/
		public function addcategory($data) {  
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('category');
				$select->where(array(
					'category'	=>	htmlentities($data->difficulty),
					'deleted'	=>	0
				));
				$select->columns(array('id', 'category', 'description',));
	
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				if(count($res) == 0) {
					$insert = new TableGateway('category', $adapter, null, new HydratingResultSet());
					$insert->insert(array(
						'category'	=>	htmlentities($data->difficulty),
						'description'	=>	htmlentities($data->description),
						'user_id'	=>	1, //$_SESSION['userId'],
						'created'	=>	date('Y-m-d H:i:s')
					));
					$id= $insert->getLastInsertValue();
					if($id) {
						$result->id = $id;
						$result->status = 1;
						$result->message = "Row Success Added";
						return $result;
					}
					else
					{
						$result->status = 0;
						$result->message = "Error Occured";
						return $result;
					}
				}
				else {
					$result->status = 0;
					$result->message = "Duplicate category entry.";
					return $result;
				}
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		
		
		/**
		* function delete a row in Category table
		*
		* @author Utkarsh Pandey
		* @date 17-05-2015
		* @param JSON id
		* @return JSON status and message
		**/
		public function DeleteCategoryRow($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('category');
				$update->set(array(
					'deleted'	=> 1,
					'created'	=>	date('Y-m-d H:i:s')
				));
				$update->where(array(
					'id' => $data->id
				));
				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->status = 1;
				$result->message = "Successfully Deleted.";
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		
		
		/**
		* function update category and description in a row of category table
		*
		* @author Utkarsh Pandey
		* @date 17-05-2015
		* @param JSON with all required details
		* @return JSON status and message
		**/
		public function updateCategoryRow($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);								
				$select = $sql->select();
				$select->from('category');
				$select->where(array(
					'category'	=>	htmlentities($data->difficulty),
					'deleted'	=>	0
				));
				$select->columns(array('id','category', 'description',));
	
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = array();
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				if(count($res) == 0) {
					$update = $sql->update();
					$update->table('category');
					$update->set(array(
						'category'	=> htmlentities($data->difficulty),
						'description'	=>	htmlentities($data->description),
						'created'	=>	date('Y-m-d H:i:s')
					));
					$update->where(array(
						'id' => $data->id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
					$result->status = 1;
					$result->message = "Successfully Update.";
					return $result;
				}
				else if(count($res) == 1 && $res[0]['id'] == $data->id) {
						$result->status = 1;
						$result->message = "Successfully Update.";
						return $result;
					}
				else {
					$result->status = 0;
					$result->message = 'Not Updated because this category already created.';
					return $result;
				}
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
	}
	
?>