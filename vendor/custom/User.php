<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;

	//including connection.php file to make connection with database

	require_once 'Connection.php';
	require_once 'Utility.php';

	class User extends Connection {

		public function adminLogin($data) {
			//creating a object for storing result
			$result = new stdClass();
			//using try catch blocks for better error handling
			try {
				//coping the connection adapter into local variable adapter
				$adapter = $this->adapter;
				//creating a sql object to create a sql statement using zend framework
				$sql = new Sql($adapter);
				$hashed = md5($data->password);
				$admin_roles = array(1,2,3);

				/*
				* Initiating all select parameters.
				* SQL Injection by making the queries parametrised
				*/
				$select = $sql->select();
				$select->from('users');

				$where = new $select->where();
				$where->equalTo('email', htmlentities($data->email));
				$where->equalTo('password', $hashed);
				$where->equalTo('banned',0);
				$where->equalTo('deleted',0);
				$where->in('role',$admin_roles);
				$select->where($where);

				$select->columns(array('id','email','role'));

				$selectString = $sql->getSqlStringForSqlObject($select);
				//now executing the generated query using the connection adapter
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				//converting the result set into an php array
				$temp = $temp->toArray();

				if(count($temp) == 1) {
					$update = $sql->update();
					$update->table('users');
					$update->set(array(
						'last_login'	=>	time()
					));
					$update->where(array(
						'id'	=>	$temp[0]['id']
					));
					$statement = $sql->getSqlStringForSqlObject($update);
					$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

					$result->status = 1;
					$result->user = $temp[0];
					return $result;
				}

				@session_start();
				@session_destroy();
				$result->status = 0;
				$result->message = 'Invalid Credentials...';
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function forgotPassword($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'email'	=>	$request->email
				));
				// $select->columns(array('id','username'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();

				if (count($rows) == 1) {
					$user = (object) $rows[0];
					$code = md5(time().uniqid());

					$user_reset_code = new TableGateway('user_reset_codes', $adapter, null, new HydratingResultSet());
					$time = time();
					$user_reset_code->insert(array(
						'user_id'	=>	$user->id,
						'reset_code'	=>	$code,
						'created'	=> date('Y-m-d H:i:s')
					));
					$user_reset_codes_id = $user_reset_code->getLastInsertValue();

					// $util = new Utility();
					// $util->sendEmail($request->email,$mailSubject,$mailBody);
					require 'Email.php';
					$mailer = new Email;
					// $mailer->forgotPassword($user->first_name.' '.$user->last_name, 'utkarsh.4thpointer@gmail.com', $code);
					$mailer->forgotPassword($user->first_name.' '.$user->last_name, $user->email, $code);
					// $util->sendEmail('utkarsh.4thpointer@gmail.com',$mailSubject,$mailBody);
					//mail($request->email, "Your order Received", 'Your order details. \n'.$customer_mail);
					//$util->sendEmail('raviarya.4thpointer@gmail.com', "New Registration of student.", $customer_mail);



					$result->status = 1;
					$result->message = "An email sent to: ".$request->email;


				}
				else{
					$result->status = 0;
					$result->message = "You entered incorrect email , please try again.";
				}

				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		public function resetPassword($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('user_reset_codes');
				$select->where(array(
					'reset_code'	=>	$request->code
				));
				// $select->columns(array('id','username'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();

				if (count($rows) == 1) {
					$user_reset_code = $rows[0];
					$update = $sql->update();
					$update->table('users');
					$update->set(array(
						'password' => md5($request->password),
						// 'reset_code' => 'xyz12555'
					));
					$update->where(array(
						'id' => $user_reset_code['user_id'],
					));
					$statement = $sql->getSqlStringForSqlObject($update);
					// die($statement);
					$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

					$statement = 'DELETE FROM user_reset_codes WHERE user_reset_codes.id ='.$user_reset_code['id'];
					// die($statement);
					$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

					$result->status = 1;
					$result->message = "Password Successfully Reset.";
				}
				else{
					$result->status = 0;
					$result->message = "Link Expired.";
				}

				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function login($data) {
			//creating a object for storing result
			$result = new stdClass();
			//using try catch blocks for better error handling
			try {
				//coping the connection adapter into local variable adapter
				$adapter = $this->adapter;
				//creating a sql object to create a sql statement using zend framework
				$sql = new Sql($adapter);
				$hashed = md5($data->password);
				$student_roles = array(4,5,6);

				/*
				* Initiating all select parameters.
				* SQL Injection by making the queries parametrised
				*/
				$select = $sql->select();
				$select->from('users');

				$where = new $select->where();
				$where->equalTo('email', htmlentities($data->email));
				$where->equalTo('password', $hashed);
				$where->equalTo('banned',0);
				$where->equalTo('deleted',0);
				$where->in('role',$student_roles);
				$select->where($where);

				$select->columns(array('id','email','role','first_name'));
				/*
				* Now converting the zend sql object into a parametrised query
				* A query SELECT id FROM test_engine.users WHERE username='INPUT USERNAME' AND password='PASSWORD'
				* will be generated in parametrised form so no sql injection is possible.
				*/
				$selectString = $sql->getSqlStringForSqlObject($select);
				//now executing the generated query using the connection adapter
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				//converting the result set into an php array
				$temp = $temp->toArray();

				/*
				* Checking if only one row is fetched
				* then returning the fetch userId
				*/
				if(count($temp) == 1) {
					$update = $sql->update();
					$update->table('users');
					$update->set(array(
						'last_login'	=>	time()
					));
					$update->where(array(
						'id'	=>	$temp[0]['id']
					));
					$statement = $sql->getSqlStringForSqlObject($update);
					$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

					$result->status = 1;
					$result->user = $temp[0];
					return $result;
				}

				@session_start();
				@session_destroy();
				$result->status = 0;
				$result->message = 'Invalid Credentials...';
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}


		public function logout($data){
			$result = new stdClass();
			try{
				@session_start();
				@session_destroy();
				// header('location: ..\index.php');
			}catch(Exception $e){
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;			}
		}




		public function register($data) {
			$result = new stdclass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'email'	=>	htmlentities($data->email),
					'deleted'	=> 0
				));
				$select->columns(array('email'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$res = $res->toArray();
				if(count($res) != 0) {
					$result->status = 0;
					$result->message = "E-mail already registered.";
					return $result;
				}
				$password = time();
				$hashed = md5($password);
				$time = time();
				$insert = new TableGateway('users', $adapter, null, new HydratingResultSet());
				$student_data = [
					'first_name'    => htmlentities($data->firstname),
					'last_name'     => htmlentities($data->lastname),
					'email'         => htmlentities($data->email),
					'password'      => $hashed,
					'last_login'    => $time,
					'banned'        => 0,
					'role'          => 4,
					'registered_on' => $time
				];
				if(isset($data->type)) {
					$student_data['mode'] = 'admin';
				}
				$insert->insert($student_data);
				$user_id = $insert->getLastInsertValue();

				$insert_new = new TableGateway('profile', $adapter, null, new HydratingResultSet());
				$insert_new->insert(array(
					'user_id'       => $user_id,
					'email'         => htmlentities($data->email),
					'first_name'    => htmlentities($data->firstname),
					'last_name'     => htmlentities($data->lastname),
					'mobile'        => htmlentities($data->mobile),
					'class'         => htmlentities($data->class),
					'registered_on' => $time
				));


				if(isset($data->batch) && count($data->batch) != 0) {
					foreach ($data->batch as $key => $batch) {
						$insert_new = new TableGateway('batch_users', $adapter, null, new HydratingResultSet());
						$insert_new->insert([
							'user' => $user_id,
							'batch' => htmlentities($batch),
							'created' => $time
						]);
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('batch_program');
						$select->where([
							'batch'		=>	htmlentities($batch),
							'deleted'	=> 0
						]);
						$select->columns(array('program'));
						$statement = $sql->getSqlStringForSqlObject($select);
						$res = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$program = $res->toArray();
						if(count($program) != 0) {
							foreach ($program as $key => $program) {
								$select = $sql->select();
								$select->from('student_program');
								$select->where(array(
									'user_id'	=> $user_id,
									'test_program_id'	=> $program['program']
								));
								$select->columns(array('id'));
								$statement = $sql->getSqlStringForSqlObject($select);
								$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
								$bought = $run->toArray();
								if(count($bought) == 0)
								{
									$insert_program = new TableGateway('student_program', $adapter, null, new HydratingResultSet());
									$insert_program->insert(array(
										'user_id' => $user_id,
										'test_program_id' => $program['program'],
										'time' => $time
									));
								}

							}
						}
					}
				}


				$id = $insert->getLastInsertValue();
				if(count($id)) {
					$select = $sql->select();
					$select->from('users');
					$select->where(array(
						'id'	=>	$id
					));
					$select->columns(array('id','email','role','first_name'));
					$statement = $sql->getSqlStringForSqlObject($select);
					$temp = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$temp = $temp->toArray();

					$result->user = $temp[0];
					$result->byAdmin = 0;
					if(isset($data->type))
					{
						$result->byAdmin = 1;
					}
					$result->status = 1;

					require_once 'Email.php';
					// Send E-mail
					$mailer = new Email;
					$mailer->registration($data->firstname.' '.$data->lastname, $data->email, $password);
				}
				return $result;
			}
			catch(Exceaption $e)
			{
				$result->status = 0;
				$result->message = "Error: Something went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}

		public function institute_register($data)
		{
		$result = new stdclass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'email'	=>	htmlentities($data->email),
					'deleted'	=> 0
				));
				// $select->columns(array('email'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$res = $res->toArray();

				if(count($res) <= 0) {
					$insert = new TableGateway('users', $adapter, null, new HydratingResultSet());
					$hashed = md5($data->password);
					$insert->insert(array(
						'first_name'	=>	htmlentities($data->name),
						'email'	=>	htmlentities($data->email),
						'password'	=>	$hashed,
						'role'	=>	5
					));

					$id = $insert->getLastInsertValue();

					if(count($id)) {
						$insert_new = new TableGateway('profile', $adapter, null, new HydratingResultSet());
						$insert_new->insert(array(
							'user_id'	=>	$id,
							'mobile'	=>	htmlentities($data->phone_no),
							'about' =>	htmlentities($data->details),
							'ins_lab_type' =>	$data->ins_lab_type,
						));
						$idd = $insert_new->getLastInsertValue();


						$select = $sql->select();
						$select->from('users');
						$select->where(array(
									'id'	=>	$id
						));
						$select->columns(array('id','email','role', 'first_name'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$temp = $temp->toArray();

						//TO ASSIGN A PROGRAN OF NEW REGISTERED INSTITUTE
						$insert_new_program = new TableGateway('institute_program', $adapter, null, new HydratingResultSet());
						$insert_new_program->insert(array(
							'institute'	=>	$id,
							'program'	=>	9,
							'created_at' =>	date('Y-m-d H:i:s'),
							'status' =>	1,
						));


						/*$db = new DB();
						$this->db->_iou('institute_program',
						['institute' => $id, 'program' => 9],
						['institute' => $id, 'program' => 9, 'created_at' => date('Y-m-d H:i:s')]);*/


						$result->user = $temp[0];
						$result->status = 1;
						$result->message = "Institute Successfully registered.";

					}
				}
				else {
					$result->status = 0;
					$result->message = "This E-mail is already registered.";
				}
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Error: Something went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		*function to sent mail
		**/
		public function sendEmail($mailto,$password)
		{
			$mailTo = $mailto;
						$mailFrom = "Transformationworx <transformationworx@dltlabs.ca>";
						$mailSubject="Transformationworx:You are registered on transformationworx.";
						$mailHeader = "From: transformationworx@dltlabs.ca \r\n";
						// $mailHeader .= "MIME-Version: 1.0\r\n";
						$mailHeader .= "Content-type: text/html; charset=utf-8\r\n";
						// $mailHeader .= "Reply-To: ".$mailFrom.".\r\n";
						// $mailHeader .= "X-Mailer: PHP/". phpversion();
						$mailBody = '<h1>You are Successfully registered on <a href=\"transformationworx.dltlabs.ca\">transformationworx.dltlabs.ca</a></h1><br><h4>Welcome on <a href=\"transformationworx.dltlabs.ca\">Transformationworx</a></h4><br><span>your registered Email is : </span>'.$mailto.'<br><span>your password is : </span>'.$password.'<br>';

						return mail($mailto, $mailSubject, $mailBody, $mailHeader);

		}

		public function createAdmin($data)
		{
			$result = new stdclass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'email'	=>	htmlentities($data->email),
					'deleted'	=> 0
				));
				// $select->columns(array('email'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$res = $res->toArray();

				if(count($res) <= 0) {
					$insert = new TableGateway('users', $adapter, null, new HydratingResultSet());
					$hashed = md5($data->password);
					$insert->insert(array(
						'first_name'	=>	htmlentities($data->name),
						'email'	=>	htmlentities($data->email),
						'password'	=>	$hashed,
						'role'	=>	$data->role
					));

					$id = $insert->getLastInsertValue();
					if(count($id)) {
						$select = $sql->select();
						$select->from('users');
						$select->where(array(
									'id'	=>	$id
						));
						$select->columns(array('id','email','role'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$temp = $temp->toArray();

						$result->user = $temp[0];
						$result->status = 1;
						$result->message = "Successfully registered.";

					}
				}
				else {
					$result->status = 0;
					$result->message = "This E-mail is already registered.";
				}
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Error: Something went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* function fetch admin for the SUPER User
		*/
		public function getTheAdmins($data)
		{
			$result = new stdclass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$statement = 'SELECT users.id, users.first_name as name, user_roles.role, IF(users.banned = 1, "In-Active", "Active") AS status, users.banned AS active FROM users LEFT JOIN user_roles ON user_roles.id = users.role WHERE deleted = 0 AND users.role IN(1,2,3) ORDER BY users.id';
				$users = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$users = $users->toArray();

				$result->users = $users;
				$result->status = 1;
				$result->message = "Successfully Fetched.";

				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Error: Something went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		/**
		* function to get Admin Details
		*/
		public function getAdminDetails($data)
		{
			$result = new stdclass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$statement = 'SELECT users.id, users.first_name as name, users.email, users.last_login, users.role FROM users WHERE deleted = 0 AND users.id = '.$data->user_id;
				$users = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$users = $users->toArray();

				$result->users = $users[0];
				$result->status = 1;
				$result->message = "Successfully Fetched.";

				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Error: Something went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		/**
		* function to update Admin Details
		*/
		public function updateAdminDetails($data)
		{
			$result = new stdclass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'email'	=>	htmlentities($data->email),
					'deleted'	=> 0
				));
				// $select->columns(array('email'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$res = $res->toArray();
				$dublicate = count($res);

				if($dublicate == 1 && $res[0]['id'] == $data->id) {
					$dublicate = 0;
				}

				if($dublicate == 0) {

					$hashed = md5($password);
					$update = $sql->update();
					$update->table('users');
					$update->set(array(
						"first_name" => htmlentities($data->name),
						"email" => htmlentities($data->email),
						"password" => $hashed,
						"role" => $data->role
					));
					$update->where(array(
						"id"	=>	$data->id
					));

					$statement = $sql->getSqlStringForSqlObject($update);
					$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$result->status = 1;
					$result->message = "Successfully Updated.";

				}
				else {
					$result->status = 0;
					$result->message = "This E-mail is already registered.";
				}

				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Error: Something went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* function to change Admin Status
		*/
		public function changeUserStatus($data)
		{
			$result = new stdclass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
				$update->table('users');
				$update->set(array(
					"banned" => $data->changed_status,
				));
				$update->where(array(
					"id"	=>	$data->user_id
				));

				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->status = 1;
				$result->message = "Successfully Updated.";

				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Error: Something went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* function to delete any Admin
		*/
		public function deleteUser($data)
		{
			$result = new stdclass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
				$update->table('users');
				if(isset($data->changed_status))
					{
						$update->set(array(
						"deleted" =>$data->changed_status
					));
					}
				else{
						$update->set(array(
							"deleted" => 1
						));
				}
				$update->where(array(
					"id"	=>	$data->user_id
				));

				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->status = 1;
				$result->message = "Successfully Updated.";

				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Error: Something went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		public function getTheInstitutes($data)
		{
			$result = new stdclass();
			try {
				$adapter = $this->adapter;

				$statement = 'SELECT users.id, users.first_name as name, users.email, profile.mobile, profile.ins_lab_type, IF(users.banned = 1, "In-Active", "Active") AS status, users.banned AS active, profile.credits FROM users LEFT JOIN profile ON profile.user_id = users.id  WHERE deleted = 0 AND users.role IN(5) ORDER BY users.id LIMIT '.$data->limit.',50';

				if (isset($data->name)) {

					$statement = 'SELECT users.id, users.first_name as name, users.email, profile.mobile, profile.ins_lab_type, IF(users.banned = 1, "In-Active", "Active") AS status, users.banned AS active, profile.credits FROM users LEFT JOIN profile ON profile.user_id = users.id  WHERE deleted = 0 AND users.role IN(5) AND (users.first_name LIKE \'%'.$data->name.'%\' OR users.last_name LIKE \'%'.$data->name.'%\') ORDER BY users.id LIMIT '.$data->limit.',50';
				}

				if (isset($data->email)) {

					$statement = 'SELECT users.id, users.first_name as name, users.email, profile.mobile, profile.ins_lab_type, IF(users.banned = 1, "In-Active", "Active") AS status, users.banned AS active, profile.credits FROM users LEFT JOIN profile ON profile.user_id = users.id  WHERE deleted = 0 AND users.role IN(5) AND users.email LIKE \'%'.$data->email.'%\' ORDER BY users.id LIMIT '.$data->limit.',50';
				}

				if (isset($data->name) && isset($data->email)) {

					$statement = 'SELECT users.id, users.first_name as name, users.email, profile.mobile, profile.ins_lab_type, IF(users.banned = 1, "In-Active", "Active") AS status, users.banned AS active, profile.credits FROM users LEFT JOIN profile ON profile.user_id = users.id  WHERE deleted = 0 AND users.role IN(5) AND (users.first_name LIKE \'%'.$data->name.'%\' OR users.last_name LIKE \'%'.$data->name.'%\') AND users.email LIKE \'%'.$data->email.'%\' ORDER BY users.id LIMIT '.$data->limit.',50';
				}

				$users = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$users = $users->toArray();

				//count

				$statement = 'SELECT count(users.id) as total_questions FROM users LEFT JOIN profile ON profile.user_id = users.id  WHERE deleted = 0 AND users.role IN(5) ORDER BY users.id';

				if (isset($data->name)) {

					$statement = 'SELECT count(users.id) as total_questions FROM users LEFT JOIN profile ON profile.user_id = users.id  WHERE deleted = 0 AND users.role IN(5) AND (users.first_name LIKE \'%'.$data->name.'%\' OR users.last_name LIKE \'%'.$data->name.'%\') ORDER BY users.id';
				}
				if (isset($data->email)) {

					$statement = 'SELECT count(users.id) as total_questions FROM users LEFT JOIN profile ON profile.user_id = users.id  WHERE deleted = 0 AND users.role IN(5) AND users.email LIKE \'%'.$data->email.'%\' ORDER BY users.id LIMIT '.$data->limit.',50';
				}
				if (isset($data->name) && isset($data->email)) {

					$statement = 'SELECT count(users.id) as total_questions FROM users LEFT JOIN profile ON profile.user_id = users.id  WHERE deleted = 0 AND users.role IN(5) AND (users.first_name LIKE \'%'.$data->name.'%\' OR users.last_name LIKE \'%'.$data->name.'%\') AND users.email LIKE \'%'.$data->email.'%\' ORDER BY users.id LIMIT '.$data->limit.',50';
				}
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$total_questions = $run->toArray();
				$result->total_questions = $total_questions[0]['total_questions'];

				$result->users = $users;
				$result->status = 1;
				$result->message = "Successfully Fetched.";

				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Error: Something went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* function to get Admin Details
		*/
		public function getInstituteDetails($data)
		{
			$result = new stdclass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$statement = 'SELECT users.id, users.first_name as name, users.email, profile.mobile, profile.about AS details, profile.ins_lab_type,profile.credits FROM users LEFT JOIN profile ON profile.user_id = users.id WHERE deleted = 0 AND users.id = '.$data->user_id;
				$users = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$users = $users->toArray();

				$result->users = $users[0];
				$result->status = 1;
				$result->message = "Successfully Fetched.";

				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Error: Something went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* function to update Institute Details
		*/
		public function updateInstituteDetails($data)
		{
			$result = new stdclass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'email'	=>	htmlentities($data->email),
					'deleted'	=> 0
				));
				// $select->columns(array('email'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$res = $res->toArray();
				$dublicate = count($res);

				if($dublicate == 1 && $res[0]['id'] == $data->id) {
					$dublicate = 0;
				}

				if($dublicate == 0) {

					$hashed = md5($data->password);
					$update = $sql->update();
					$update->table('users');
					$update->set(array(
						"first_name" => htmlentities($data->name),
						"email" => htmlentities($data->email),
						"password" => $hashed,
					));
					$update->where(array(
						"id"	=>	$data->id
					));

					$statement = $sql->getSqlStringForSqlObject($update);
					$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

					$update = $sql->update();
					$update->table('profile');
					$update->set(array(
						"mobile" => htmlentities($data->mobile),
						"about" => $data->details,
						"credits" => $data->credits,
						"ins_lab_type" => $data->ins_lab_type
					));
					$update->where(array(
						"user_id"	=>	$data->id
					));

					$statement = $sql->getSqlStringForSqlObject($update);
					$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

					$result->status = 1;
					$result->message = "Successfully Updated.";

				}
				else {
					$result->status = 0;
					$result->message = "This E-mail is already registered.";
				}

				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Error: Something went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		public function createInstitute($data) {
			$result = new stdclass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'email'	=>	htmlentities($data->email),
					'deleted'	=> 0
				));
				// $select->columns(array('email'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$res = $res->toArray();

				if(count($res) <= 0) {
					$insert = new TableGateway('users', $adapter, null, new HydratingResultSet());
					$hashed = md5($data->password);
					$insert->insert(array(
						'first_name'	=>	htmlentities($data->name),
						'email'	=>	htmlentities($data->email),
						'password'	=>	$hashed,
						'role'	=>	5
					));

					$id = $insert->getLastInsertValue();

					if(count($id)) {
						$insert_new = new TableGateway('profile', $adapter, null, new HydratingResultSet());
						$insert_new->insert(array(
							'user_id'	=>	$id,
							'mobile'	=>	htmlentities($data->phone_no),
							'about' =>	htmlentities($data->details),
							'ins_lab_type' =>	$data->ins_lab_type,
						));
						$id = $insert_new->getLastInsertValue();


						$select = $sql->select();
						$select->from('users');
						$select->where(array(
									'id'	=>	$id
						));
						$select->columns(array('id','email','role'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$temp = $temp->toArray();

						$result->user = $temp[0];
						$result->status = 1;
						$result->message = "Successfully registered.";

					}
				}
				else {
					$result->status = 0;
					$result->message = "This E-mail is already registered.";
				}
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Error: Something went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		public function fetchUserDetails($data)
		{

			$result = new stdclass();

			try
			{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('users');
				$select->where(array(
							'id'	=>	$data->userId
				));
				$select->columns(array('id','email','role'));
				$statement = $sql->getSqlStringForSqlObject($select);
				$statement = 'SELECT users.first_name, users.last_name, users.email, users.last_login, users.role, profile.mobile, profile.interests, profile.occupation, profile.about, profile.website_url, profile.address, profile.ins_lab_type, profile.image FROM users LEFT JOIN profile ON profile.user_id = users.id WHERE users.id = '.$data->userId;
				$temp = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();

				if(count($temp))
				{
					$result->user = $temp[0];
					$result->status = 1;
					$result->message = "Successfully Fetched.";
				}
				else
				{
					$result->status = 0;
					$result->message = "Your Id not found.";
				}
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = "Error: Something went wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function changePasssword($request) {

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$old_password = md5($request->old_password);
				$new_password = md5($request->new_password);

				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'id'	=>	$request->userId
				));
				// $select->columns(array('id','username'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$rows = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $rows->toArray();
				$user = $rows[0];

				if($user['password'] === $old_password)
				{
					$update = $sql->update();
					$update->table('users');
					$update->set(array(
						'password' => $new_password,
					));
					$update->where(array(
						'id' => $user['id'],
					));
					$statement = $sql->getSqlStringForSqlObject($update);

					$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

					$result->status = 1;
					$result->authKey = $new_password;
					$result->message = "Password Changed Successfully.";
				}
				else{
					$result->status = 0;
					$result->message = "Your current password is incorrect.";
				}

				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}



		public function updateUserAvtar($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$orgFileName = $data->image["name"];
				$ext = pathinfo($orgFileName, PATHINFO_EXTENSION);
				$newFileName =  md5($data->userId).'.'.$ext;
				$destination = __DIR__."/../../profilePictures/".$newFileName;
				move_uploaded_file($data->image["tmp_name"], $destination);

				$sql = new Sql($adapter);

				$update = $sql->update();
				$update->table('profile');
				$src = "profilePictures/".$newFileName;
				$update->set(array(
					'image' => $src
				));

				$update->where(array(
					'user_id'	=>	$data->userId
				));

				$updateString = $sql->getSqlStringForSqlObject($update);
				// die($updateString);
				$update = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify

				$result->status = 1;
				$result->message = "Avtar Updated.";
				$result->src = $src."?".time();
				return $result;

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function uploadStudentAssignment($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('student_assignment');
				$select->where(array(
					'test_program_id'	=>	$data->test_program_id,
					'user_id'	=> $data->userId,
					'deleted'	=> 0
				));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$res = $res->toArray();

				if(count($res) <= 0) {
					$orgFileName = $data->image["name"];
					$ext = pathinfo($orgFileName, PATHINFO_EXTENSION);
					$newFileName = preg_replace("/\.[^.]+$/", "", $orgFileName).'_'.time().'.'.$ext;
					$destination = "../assets/student_assignment/".$newFileName;

					//$destination = __DIR__."/../../profilePictures/".$newFileName;
					move_uploaded_file($data->image["tmp_name"], $destination);

					$sql = new Sql($adapter);

					$upload_data = array(
							'doc_name'	=>	$newFileName,
							'path'	=>	"student_assignment/",
							'user_id'	=>	$data->userId,
							'test_program_id'	=>	$data->test_program_id,
							'status'	=>	'0',
							'created'	=>	time(),
							'deleted'	=>	0
						);

					$up_assignment = new TableGateway('student_assignment', $adapter, null, new HydratingResultSet());
					$up_assignment->insert($upload_data);
					$result->status = 1;
					$result->message = "Assignment Uploaded Successfully.";
				}else{
					$result->status = 0;
					$result->message = "ERROR: Assignment Already Exist.";
				}
				return $result;
			

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function sendAssigmentByEmail($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('student_assignment');
				$select->where(array(
					'test_program_id'	=>	$data->test_program_id,
					'user_id'	=>	$data->userId,
					'deleted'	=>	0
				));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$res = $res->toArray();

				if(count($res) <= 0) {
					$upload_data = array(
							'doc_name'	=>	'Send By E-mail',
							'path'	=>	"",
							'user_id'	=>	$data->userId,
							'test_program_id'	=>	$data->test_program_id,
							'status'	=>	'0',
							'created'	=>	time(),
							'deleted'	=>	0
						);

					$up_assignment = new TableGateway('student_assignment', $adapter, null, new HydratingResultSet());
					$up_assignment->insert($upload_data);
					$result->status = 1;
					$result->message = "Thank you for submitting. The instructor will send a confirmation email once they have received the Capstone Project.";
				}else{
					$result->status = 0;
					$result->message = "ERROR: Assignment Already Exist.";
				}
				return $result;
			

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function b2blogin($data) {
			//creating a object for storing result
			$result = new stdClass();
			//using try catch blocks for better error handling
			try {
				//coping the connection adapter into local variable adapter
				$adapter = $this->adapter;
				//creating a sql object to create a sql statement using zend framework
				$sql = new Sql($adapter);
				$hashed = md5($data->password);
				$student_roles = array(4,5);

				/*
				* Initiating all select parameters.
				* SQL Injection by making the queries parametrised
				*/
				$select = $sql->select();
				$select->from('users');

				$where = new $select->where();
				$where->equalTo('email', htmlentities($data->email));
				$where->equalTo('password', $hashed);
				$where->equalTo('banned',0);
				$where->equalTo('deleted',0);
				$where->in('role',$student_roles);
				$select->where($where);

				$select->columns(array('id','email','role'));
				/*
				* Now converting the zend sql object into a parametrised query
				* A query SELECT id FROM test_engine.users WHERE username='INPUT USERNAME' AND password='PASSWORD'
				* will be generated in parametrised form so no sql injection is possible.
				*/
				$selectString = $sql->getSqlStringForSqlObject($select);
				//now executing the generated query using the connection adapter
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				//converting the result set into an php array
				$temp = $temp->toArray();

				/*
				* Checking if only one row is fetched
				* then returning the fetch userId
				*/
				if(count($temp) == 1) {
					$update = $sql->update();
					$update->table('users');
					$update->set(array(
						'last_login'	=>	time()
					));
					$update->where(array(
						'id'	=>	$temp[0]['id']
					));
					$statement = $sql->getSqlStringForSqlObject($update);
					$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

					$result->status = 1;
					$result->user = $temp[0];
					return $result;
				}
				/*
				* If the above condition follows the function will return
				* i.e. will not execute any line below it.
				* So any line after this condition can be considered as else
				* which will return a error message to be shown to the user.
				* If it happens destroy any previous session.
				*/
				@session_start();
				@session_destroy();
				$result->status = 0;
				$result->message = 'Invalid Credentials...';
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function submitContactForm($data)
		{
			$result = new stdClass();
			$name = $data->name;
			$email = $data->email;
			$message = $data->message;
			try
			{


				$sender = true;
				if($sender)
				{
					$result->status = 1;
					$result->message = "Message send successfully.";
					return $result;

				}
				else
				{
					$result->status = 2;
					$result->message = "Message not send,Please try lateral.";
					return $result;
				}

			}
			catch(Exception $e)
			{
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		public function fbLogin($data)
		{
			$result = new stdClass();

			try {

				$adapter = $this->adapter;

				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('users');

				$select->where->equalTo('email', htmlentities($data->email));
				$select->where->equalTo('deleted',0);
				// $select->where->equalTo('role','4');

				$select->columns(array('id','email','role','first_name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();

				if(count($temp) == 1) {
					$update = $sql->update();
					$update->table('users');
					$update->set(array(
						'last_login'	=>	time()
					));
					$update->where(array(
						'id'	=>	$temp[0]['id']
					));
					$statement = $sql->getSqlStringForSqlObject($update);
					$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

					@session_start();
					$_SESSION['userId'] = $temp[0]['id'];
					$_SESSION['role'] = 4;
					$_SESSION['user'] = $data->email;
					$_SESSION['username'] = $data->name;

					$result->status = 1;
					//$result->user = $temp[0];
					return $result;
				}
				else
				{
					$adapter = $this->adapter;
					$sql = new Sql($adapter);

					$insert = array(
						'email'	=>	$data->email,
						'first_name' => $data->first_name,
						'last_name' => $data->last_name,
						'registered_on' => time(),
						'last_login' => time(),
						'mode' => 'facebook',
						'role' => '4',
						'fb_id' => $data->fb_id
					);

					$query = new TableGateway('users', $adapter, null, new HydratingResultSet());
					$query->insert($insert);
					$id = $query->getLastInsertValue();

					$db = new DB();
					$db->_iou('profile',
						array('user_id'=>$id),
						array('user_id'=>$id,'email'=>$data->email,'first_name'=>$data->first_name,'last_name'=>$data->last_name,'registered_on'=>time(),'image'=>$data->picture));

					@session_start();
					$_SESSION['userId'] = $id;
					$_SESSION['role'] = 4;
					$_SESSION['user'] = $data->email;
					$_SESSION['username'] = $data->name;
					$result->status = 1;
					return $result;
				}

				@session_start();
				@session_destroy();
				$result->status = 0;
				$result->message = 'Invalid Credentials...';
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}


	}
?>
