<?php

	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;

	require_once 'Connection.php';

	class Batch extends connection{

		public function addBatch($data)
		{
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('batch');
				$select->where(array(
					'class_id' =>  htmlentities($data->class),
					'name'	=>	htmlentities($data->batch),
					'deleted'	=>	0
				));
				$select->columns(array('id','class_id', 'name',));
	
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				if(count($res) == 0) {
					$insert = new TableGateway('batch', $adapter, null, new HydratingResultSet());
					$insert->insert(array(
						'name'	=>	htmlentities($data->batch),
						'class_id'	=>	htmlentities($data->class),
						'created'	=>	time()
					));
					$id= $insert->getLastInsertValue();
					if($id) {
						$result->id = $id;
						$result->status = 1;
						$result->message = "Batch Successfully Added";
						return $result;
					}
					else {
						$result->status = 0;
						$result->message = "Error Occured";
						return $result;
					}
				}
				else {
					$result->status = 0;
					$result->message = "Duplicate Batch entry.";
					return $result;
				}
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function getBatchData($data) {
			
			$result = new stdClass();
			
			try{
				
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('batch')
						->join(array('class' => 'classes'),'class.id=batch.class_id',array('class_name' => 'name'),'left');
				$select->where(array(
					'batch.deleted'	=>	0
				));
				// $select->limit($data->limit);
				// $select->offset($data->offset);
				$select->order('id DESC');
				$select->columns(array('id','name' => 'name','class_id'));
				
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				if($res)
				$result->status = 1;
				$result->data = $res;
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
			
		}

		public function DeleteBatchRow($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('batch');
				$update->set(array(
					'deleted'	=> 1,
					'created'	=>	time()
				));
				$update->where(array(
					'id' => $data->id
				));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				$result->status = 1;
				$result->message = "Successfully Deleted.";
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function fetchBatches($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('batch');

				if(isset($data->class)){
					$select->where(array(
					'class_id' => htmlentities($data->class)
				));
				}

				$select->where(array(
					'deleted'	=>	0
				));
				
				$select->columns(array('id','name', 'class_id',));
	
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = array();
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				if(count($res)!=0){
					$result->batches = $res;
					$result->status = 1;
					$result->message = "Batches found.";
					return $result;
				}
				else
				{	$result->batches = [];
					$result->status = 1;
					$result->message = "Batches not found for this class.";
					return $result;
				}
				
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function updateBatchRow($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);								
				$select = $sql->select();
				$select->from('batch');
				$select->where(array(
					'name'	=>	htmlentities($data->batch),
					'class_id' => htmlentities($data->class),
					'deleted'	=>	0
				));
				$select->columns(array('id','name', 'class_id',));
	
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = array();
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				if(count($res) == 0) {

					$update = $sql->update();
					$update->table('batch');
					$update->set(array(
						'name'	=> htmlentities($data->batch),
						'class_id'	=>	htmlentities($data->class),
						'created'	=>	time()
					));
					$update->where(array(
						'id' => $data->id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
					$result->status = 1;
					$result->message = "Successfully Update.";
					return $result;
				}
				else if(count($res) == 1 && $res[0]['id'] == $data->id) {
					$update = $sql->update();
					$update->table('batch');
					$update->set(array(
						'name'	=> htmlentities($data->batch),
						'class_id'	=>	htmlentities($data->class),
						'created'	=>	time()
					));
					$update->where(array(
						'id' => $data->id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
					$result->status = 1;
					$result->message = "Successfully Update.";
					return $result;
					}
				else {
					$result->status = 0;
					$result->message = 'Not Updated. this batch already created.';
					return $result;
				}
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
	}