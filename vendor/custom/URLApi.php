<?php
	date_default_timezone_set('Asia/Kolkata');
	require_once 'URLRedirection.php';

	class URLApi
	{
		public static function validate($request)
		{
			$u = new URLRedirection();
			return $u->validate($request);
		}
	}

?>