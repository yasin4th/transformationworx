<?php
	@session_start();
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;

	//including connection.php file to make connection with database
	require_once 'Connection.php';

	class ProfileApi extends Connection{

		/**
		* Function fetchPlanClasses for the planning of the schedule
		*
		* @author Pushpam Matah
		* @date 13-07-2015
		* @param JSON
		* @return JSON status and data(classes names)
		**/

		public function displayProfile() {

			$result = new stdClass();

			try{
				if(isset($_SESSION['user']))
				{
					$adapter = $this->adapter;

					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('profile');
					$select->where(array(
						'email'	=>	$_SESSION['user']
					));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
					$res = $run->toArray();
				}

				if($res)
				$result->status = 1;
				$result->data = $res;
				return $result;

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function updateProfile($data)
		{
			$result = new stdClass();

			try{

				$adapter = $this->adapter;

				$insertp = array(
					'first_name' => $data->first_name,
					'last_name' => $data->last_name,
					'mobile' => $data->mobile,
					'occupation' => '',
					'address' => $data->address,
				);

				if(isset($data->interests)){
					$insertp['interests'] = $data->interests;
				}
				if(isset($data->website_url)){
					$insertp['website_url'] = $data->website_url;
				}
				if(isset($data->about)){
					$insertp['about'] = $data->about;
				}

				$insertu = array(
					'first_name' => $data->first_name,
					'last_name' => $data->last_name
				);

				if(isset($data->email))
				{
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('users');
					$select->where->equalTo('email',$data->email)->notEqualTo('id',$data->userId);

					$selectString = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$count = $run->toArray();

					if(count($count) == 0)
					{
						$insertp['email'] = $data->email;
						$insertu['email'] = $data->email;
						@session_start();
						$_SESSION['user'] = $data->email;
					}
					else
					{
						$result->status = 0;
						$result->message = "Email Already Exist. Please choose a different Email.";
						return $result;
					}
				}

				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('profile');

				$update->set($insertp);

				@session_start();
				$_SESSION['username'] = $data->first_name;

				$update->where(array(
					'user_id'	=>	$data->userId
				));

				$updateString = $sql->getSqlStringForSqlObject($update);
				$update = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE);

				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('users');

				$update->set($insertu);

				$update->where(array(
					'id'	=>	$data->userId
				));

				$updateString = $sql->getSqlStringForSqlObject($update);
				$update = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify

				$result->status = 1;
				$result->message = "Profile Updated";

				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}



		public function changeProfilePassword($data) {

			$result = new stdClass();

			try{

				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('users');

				$select->where(array(
					'email'	=>	$_SESSION['user']
				));

				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();

				if($data->new_password == "" || $data->confirm_password == "" || $data->current_password == "")
				{
					$result->status = 2;
					$result->data = "Some fields are required";
				}
				else if($res[0]['password'] != md5($data->current_password))
				{
					$result->status = 2;
					$result->data = "You have entered the wrong current password";
				}
				else if($data->new_password != $data->confirm_password)
				{
					$result->status = 2;
					$result->data = "The new password and confirm password fields do not match";
				}
				else
				{
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('users');

					$update->set(array(
						'password' => md5($data->confirm_password)
					));

					$update->where(array(
						'email'	=>	$_SESSION['user']
					));

					$updateString = $sql->getSqlStringForSqlObject($update);
					$run = $adapter->query($updateString, $adapter::QUERY_MODE_EXECUTE);

					$result->status = 1;
				}


				return $result;

			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}


		}


	}