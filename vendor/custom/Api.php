<?php
	/*
	* all functions in this class will be static
	* and public so that can be accessed without
	* creating objects from anywhere
	*/
	class Api {
		/*
		* User Admin Login function
		*/
		public static function adminLogin($data) {
			require_once 'User.php';
			$u = new User();
			return $u->adminLogin($data);
		}

		public static function createAdmin($data) {
			require_once 'User.php';
			$u = new User();
			return $u->createAdmin($data);
		}

		public static function getTheAdmins($data) {
			require_once 'User.php';
			$u = new User();
			return $u->getTheAdmins($data);
		}

		public static function changePasssword($data) {
			require_once 'User.php';
			$u = new User();
			return $u->changePasssword($data);
		}


		/**
		* Function to get admin details
		*/
		public static function getAdminDetails($data) {
			require_once 'User.php';
			$program = new User();
			return $program->getAdminDetails($data);
		}

		/**
		* Function to update admin details
		*/
		public static function updateAdminDetails($data) {
			require_once 'User.php';
			$program = new User();
			return $program->updateAdminDetails($data);
		}

		/**
		* Function to change Admin Status
		*/
		public static function changeUserStatus($data) {
			require_once 'User.php';
			$program = new User();
			return $program->changeUserStatus($data);
		}

		/**
		* Function to change Admin Status
		*/
		public static function deleteUser($data) {
			require_once 'User.php';
			$program = new User();
			return $program->deleteUser($data);
		}

		/*
		* Student Login function
		*/
		public static function login($data) {
			require_once 'User.php';
			$u = new User();
			return $u->login($data);
		}

		public static function fbLogin($data) {
			require_once 'User.php';
			$u = new User();
			return $u->fbLogin($data);
		}

		/**
		*	LogOut Function
		*
		**/
		public static function getTheInstitutes($data) {
			require_once 'User.php';
			$u = new User();
			return $u->getTheInstitutes($data);
		}

		public static function createInstitute($data) {
			require_once 'User.php';
			$u = new User();
			return $u->createInstitute($data);
		}

		public static function getInstituteDetails($data) {
			require_once 'User.php';
			$u = new User();
			return $u->getInstituteDetails($data);
		}

		public static function updateInstituteDetails($data) {
			require_once 'User.php';
			$u = new User();
			return $u->updateInstituteDetails($data);
		}

		public static function updateUserAvtar($data) {
			require_once 'User.php';
			$u = new User();
			return $u->updateUserAvtar($data);
		}

		public static function uploadStudentAssignment($data) {
			require_once 'User.php';
			$u = new User();
			return $u->uploadStudentAssignment($data);
		}

		public static function sendAssigmentByEmail($data) {
			require_once "User.php";
			$quest = new User();
			return $quest->sendAssigmentByEmail($data);
		}

		/**
		*	LogOut Function
		*
		**/
		public static function logout($data) {
			require_once 'User.php';
			$u = new User();
			return $u->logout($data);
		}

		/**
		*
		*
		**/
		public static function fetchUserDetails($data) {
			require_once 'User.php';
			$u = new User();
			return $u->fetchUserDetails($data);
		}

		public static function fetchStudents($data) {
			require_once 'StudentProfile.php';
			$u = new StudentProfile();
			return $u->fetchStudents($data);
		}

		public static function updateStudent($data) {
			require_once 'StudentProfile.php';
			$u = new StudentProfile();
			return $u->updateStudent($data);
		}


		/**
		* Function For Orders
		*
		**/
		public static function fetchOrders($data) {
			require_once 'StudentProgram.php';
			$u = new StudentProgram();
			return $u->fetchOrders1($data);
		}

		/**
		* Function For Student Registration
		*
		**/
		public static function register($data) {
			require_once 'User.php';
			$u = new User();
			return $u->register($data);
		}

		/**
		* Function For Institute Registration
		*
		**/
		public static function institute_register($data) {
			require_once 'User.php';
			$u = new User();
			return $u->institute_register($data);
		}

		/**
		*	forgot Password Function
		*
		**/
		public static function forgotPassword($data) {
			require_once 'User.php';
			$u = new User();
			return $u->forgotPassword($data);
		}

		/**
		*	reset Password Function
		*
		**/
		public static function resetPassword($data) {
			require_once 'User.php';
			$u = new User();
			return $u->resetPassword($data);
		}

		/**
		* this function fetch tag data
		*
		**/
		public static function getTagsData($data) {
			require_once 'TagsApi.php';
			$q = new TagsApi();
			return $q->getTagsData($data);
		}


		public static function addTag($data) {
			require_once 'TagsApi.php';
			$q = new TagsApi();
			return $q->addTag($data);
		}

		public static function DeleteTagRow($data) {
			require_once 'TagsApi.php';
			$q = new TagsApi();
			return $q->DeleteTagRow($data);
		}

		public static function updateTagRow($data){
			require_once 'TagsApi.php';
			$q = new TagsApi();
			return $q->updateTagRow($data);
		}
		/**
		* this function fetch mapping data
		*
		**/
		public static function getMappingsData($data) {
			require_once 'Mapping.php';
			$q = new Mapping();
			return $q->getMappingsData($data);
		}

		public static function DeleteMappingRow($data) {
			require_once 'Mapping.php';
			$q = new Mapping();
			return $q->DeleteMappingRow($data);
		}

		/**
		* this function fetch Skill data
		*
		**/
		public static function getSkillsData($data) {
			require_once 'SkillsApi.php';
			$q = new SkillsApi();
			return $q->getSkillsData($data);
		}


		public static function addSkill($data) {
			require_once 'SkillsApi.php';
			$q = new SkillsApi();
			return $q->addSkill($data);
		}

		public static function DeleteSkillRow($data) {
			require_once 'SkillsApi.php';
			$q = new SkillsApi();
			return $q->DeleteSkillRow($data);
		}

		public static function updateSkillRow($data){
			require_once 'SkillsApi.php';
			$q = new SkillsApi();
			return $q->updateSkillRow($data);
		}

		/**
		* this function fetch Difficulty data
		*
		**/
		public static function getDifficultyData($data) {
			require_once 'DifficultyApi.php';
			$q = new DifficultyApi();
			return $q->getDifficultyData($data);
		}


		public static function addDifficulty($data) {
			require_once 'DifficultyApi.php';
			$q = new DifficultyApi();
			return $q->addDifficulty($data);
		}

		public static function DeleteDifficultyRow($data) {
			require_once 'DifficultyApi.php';
			$q = new DifficultyApi();
			return $q->DeleteDifficultyRow($data);
		}

		public static function updateDifficultyRow($data){
			require_once 'DifficultyApi.php';
			$q = new DifficultyApi();
			return $q->updateDifficultyRow($data);
		}

		/**
		* this function fetch Category data
		*
		**/

		public static function getCategoryData($data) {
			require_once 'CategoryApi.php';
			$q = new CategoryApi();
			return $q->getCategoryData($data);
		}

		public static function addCategory($data) {
			require_once 'CategoryApi.php';
			$q = new CategoryApi();
			return $q->addCategory($data);
		}

		public static function DeleteCategoryRow($data) {
			require_once 'CategoryApi.php';
			$q = new CategoryApi();
			return $q->DeleteCategoryRow($data);
		}

		public static function updateCategoryRow($data){
			require_once 'CategoryApi.php';
			$q = new CategoryApi();
			return $q->updateCategoryRow($data);
		}

		public static function fetchTreeData($data) {
			require_once 'TreeApi.php';
			$tree = new TreeApi();
			return $tree->fetchTreeData($data);
		}

		public static function addNewClass($data) {
			require_once 'TreeApi.php';
			$tree = new TreeApi();
			return $tree->addNewClass($data);
		}

		public static function addNewSubject($data) {
			require_once 'TreeApi.php';
			$tree = new TreeApi();
			return $tree->addNewSubject($data);
		}

		public static function addNewUnit($data) {
			require_once 'TreeApi.php';
			$tree = new TreeApi();
			return $tree->addNewUnit($data);
		}

		public static function addNewChapter($data) {
			require_once 'TreeApi.php';
			$tree = new TreeApi();
			return $tree->addNewChapter($data);
		}

		public static function addNewTopic($data) {
			require_once 'TreeApi.php';
			$tree = new TreeApi();
			return $tree->addNewTopic($data);
		}

		public static function updateClass($data) {
			require_once 'TreeApi.php';
			$tree = new TreeApi();
			return $tree->updateClass($data);
		}

		public static function updateSubject($data) {
			require_once 'TreeApi.php';
			$tree = new TreeApi();
			return $tree->updateSubject($data);
		}

		public static function updateUnit($data) {
			require_once 'TreeApi.php';
			$tree = new TreeApi();
			return $tree->updateUnit($data);
		}

		public static function updateChapter($data) {
			require_once 'TreeApi.php';
			$tree = new TreeApi();
			return $tree->updateChapter($data);
		}

		public static function updateTopic($data) {
			require_once 'TreeApi.php';
			$tree = new TreeApi();
			return $tree->updateTopic($data);
		}

		public static function deleteClass($data) {
			require_once 'TreeApi.php';
			$tree = new TreeApi();
			return $tree->deleteClass($data);
		}

		public static function deleteSubject($data) {
			require_once 'TreeApi.php';
			$tree = new TreeApi();
			return $tree->deleteSubject($data);
		}

		public static function deleteUnit($data) {
			require_once 'TreeApi.php';
			$tree = new TreeApi();
			return $tree->deleteUnit($data);
		}

		public static function deleteChapter($data) {
			require_once 'TreeApi.php';
			$tree = new TreeApi();
			return $tree->deleteChapter($data);
		}

		public static function deleteTopic($data) {
			require_once 'TreeApi.php';
			$tree = new TreeApi();
			return $tree->deleteTopic($data);
		}

		/**
		* Funtion to upload QTI zip file and extract and import its questions and assessments in our DB
		*
		* @author Rupesh Pandey
		* @date 04/06/2015
		* @param array tmp file details
		* @return JSON result status and message
		*/
		public static function qtiZipUpload($data) {
			require_once 'QTI.php';
			$qti = new QTI();
			return $qti->uploadZip($data);
		}

		/**
		* Funtion to upload PDF file and import its questions and assessments in our DB
		*
		* @author Utkarsh Pandey
		* @date 14/07/2015
		* @param array tmp file details
		* @return JSON result status and message
		*/
		public static function pdfUpload($data) {
			require_once "PDF.php";
			$pdf = new PDF();
			return $pdf->pdfUpload($data);
		}

		public static function fetchClasses($data) {
			require_once 'Question.php';
			$quest = new Question();
			return $quest->fetchClasses($data);
		}

		public static function fetchMapKeywords($data) {
			require_once 'Mapping.php';
			$quest = new Mapping();
			return $quest->fetchMapKeywords($data);
		}

		public static function fetchTestpackage($data) {
			require_once 'Question.php';
			$quest = new Question();
			return $quest->fetchTestpackage($data);
		}

		public static function fetchSubjects($data) {
			require_once 'Question.php';
			$quest = new Question();
			return $quest->fetchSubjects($data);
		}

		public static function saveTestPaperPackage($data) {
			require_once 'TestPaper.php';
			$program = new TestPaper();
			return $program->saveTestPaperPackage($data);
		}

		public static function copyTestPaper($data) {
			require_once 'TestPaper.php';
			$program = new TestPaper();
			return $program->copyTestPaper($data);
		}


		public static function fetchUnits($data) {
			require_once 'Question.php';
			$quest = new Question();
			return $quest->fetchUnits($data);
		}

		public static function fetchChapters($data) {
			require_once 'Question.php';
			$quest = new Question();
			return $quest->fetchChapters($data);
		}

		public static function fetchTopics($data) {
			require_once 'Question.php';
			$quest = new Question();
			return $quest->fetchTopics($data);
		}


		public static function fetchTags($data) {
			require_once 'Question.php';
			$quest = new Question();
			return $quest->fetchTags($data);
		}

		public static function fetchDifficultyLevel($data) {
			require_once 'Question.php';
			$quest = new Question();
			return $quest->fetchDifficultyLevel($data);
		}

		public static function fetchSkills($data) {
			require_once 'Question.php';
			$quest = new Question();
			return $quest->fetchSkills($data);
		}

		public static function addQuestion($data) {
			require_once 'Question.php';
			$quest = new Question();
			return $quest->addQuestion($data);
		}

		public static function addMapping($data) {
			require_once 'Mapping.php';
			$quest = new Mapping();
			return $quest->addMapping($data);
		}

		public static function updateMapping($data) {
			require_once 'Mapping.php';
			$quest = new Mapping();
			return $quest->updateMapping($data);
		}

		public static function deleteQuestion($data) {
			require_once 'Question.php';
			$quest = new Question();
			return $quest->deleteQuestion($data);
		}

		public static function copyQuestion($data) {
			require_once 'Question.php';
			$quest = new Question();
			return $quest->copyQuestion($data);
		}

		public static function fetchQuestions($data) {
			require_once 'Question.php';
			$quest = new Question();
			//dd($quest->fetchQuestions($data));
			return $quest->fetchQuestions($data);
		}

		public static function fetchQuestionsForQBank($data) {
			require_once 'Question.php';
			$quest = new Question();
			//dd($quest->fetchQuestions($data));
			return $quest->fetchQuestionsForQBank($data);
		}

		public static function getQuestionDetails($data) {
			require_once 'Question.php';
			$quest = new Question();
			return $quest->getQuestionDetails($data);
		}

		public static function getQuestionDetailsUsingkey($data) {
			require_once 'Mapping.php';
			$quest = new Mapping();
			return $quest->getQuestionDetailsUsingkey($data);
		}

		public static function getQuestionsOfPassage($data) {
			require_once 'Question.php';
			$quest = new Question();
			return $quest->getQuestionsOfPassage($data);
		}


		// function fetches test papers attached in test program
		public static function changeQualityCheck($data) {
			require_once "Question.php";
			$quest = new Question();
			return $quest->changeQualityCheck($data);
		}


		// function fetches test papers attached in test program
		public static function changeMultipleQC($data) {
			require_once "Question.php";
			$quest = new Question();
			return $quest->changeMultipleQC($data);
		}

		public static function deleteQuestions($data) {
			require_once "Question.php";
			$quest = new Question();
			return $quest->deleteQuestions($data);
		}

		public static function fetchQuestionTypes($data) {
			require_once 'Question.php';
			$quest = new Question();
			return $quest->fetchQuestionTypes($data);
		}

		// function fetch test paper
		public static function filterQuestions($data) {
			require_once "Question.php";
			$quest = new Question();
			return $quest->filterQuestions($data);
		}

		// function fetch test paper
		// public static function filterQuestions($data) {
		// 	require_once "Questionfilterid.php";
		// 	$quest = new Questionfilterid();
		// 	return $quest->filterQuestions($data);
		// }

		public static function updateQuestion($data) {
			require_once 'Question.php';
			$quest = new Question();
			return $quest->updateQuestion($data);
		}

		// function fetch test paper
		public static function updateOption($data) {
			require_once "Question.php";
			$quest = new Question();
			return $quest->updateOption($data);
		}

		// function to update tagged-data of question
		public static function updateTaggedClass($data) {
			require_once "Question.php";
			$quest = new Question();
			return $quest->updateTaggedClass($data);
		}

		// function to update student assignment status
		public static function changeAssignmentStatus($data) {
			require_once "Assignment.php";
			$quest = new Assignment();
			return $quest->changeAssignmentStatus($data);
		}

		// function to update student assignment status
		public static function fetchCertificates($data) {
			require_once "Assignment.php";
			$quest = new Assignment();
			return $quest->fetchCertificates($data);
		}

		public static function getTranscriptDetails($data) {
			require_once "Assignment.php";
			$quest = new Assignment();
			return $quest->getTranscriptDetails($data);
		}

		public static function getCertificate($data) {
			require_once "Assignment.php";
			$quest = new Assignment();
			return $quest->getCertificate($data);
		}

		public static function fetchAssignmentsReadyForCertificate($data) {
			require_once "Assignment.php";
			$quest = new Assignment();
			return $quest->fetchAssignmentsReadyForCertificate($data);
		}

		public static function generateCertificate($data) {
			require_once "Assignment.php";
			$quest = new Assignment();
			return $quest->generateCertificate($data);
		}


		// function to update tagged-data of question
		public static function updateTaggedSubject($data) {
			require_once "Question.php";
			$quest = new Question();
			return $quest->updateTaggedSubject($data);
		}


		// function to update tagged-data of question
		public static function updateTaggedUnit($data) {
			require_once "Question.php";
			$quest = new Question();
			return $quest->updateTaggedUnit($data);
		}


		// function to update tagged-data of question
		public static function updateTaggedChapter($data) {
			require_once "Question.php";
			$quest = new Question();
			return $quest->updateTaggedChapter($data);
		}


		// function to update tagged-data of question
		public static function updateTaggedTopic($data) {
			require_once "Question.php";
			$quest = new Question();
			return $quest->updateTaggedTopic($data);
		}


		// function to update tagged-data of question
		public static function updateTaggedTag($data) {
			require_once "Question.php";
			$quest = new Question();
			return $quest->updateTaggedTag($data);
		}


		// function to update tagged-data of question
		public static function updateTaggedDifficulty($data) {
			require_once "Question.php";
			$quest = new Question();
			return $quest->updateTaggedDifficulty($data);
		}


		// function to update tagged-data of question
		public static function updateTaggedSkill($data) {
			require_once "Question.php";
			$quest = new Question();
			return $quest->updateTaggedSkill($data);
		}


		// function to add a new test paper
		public static function createTestPaper($data) {
			require_once "TestPaper.php";
			$test_paper = new TestPaper();
			return $test_paper->createTestPaper($data);
		}
		// function fetch test paper
		public static function fetchTestPaper($data) {
			require_once "TestPaper.php";
			$test_paper = new TestPaper();
			return $test_paper->fetchTestPaper($data);
		}

		// function fetch test paper
		public static function fetchTestPapersOfPragram($data) {
			require_once "TestPaper.php";
			$test_paper = new TestPaper();
			return $test_paper->fetchTestPapersOfPragram($data);
		}

		// function fetch questions of test paper
		public static function setQuestionsOfTestPaper($data) {
			require_once "TestPaperQuestions.php";
			$test_paper = new TestPaperQuestions();
			return $test_paper->setQuestionsOfTestPaper1($data);
		}

		// function update questions of test paper
		public static function updateTestPaperQuestions($data) {
			require_once "TestPaperQuestions.php";
			$test_paper = new TestPaperQuestions();
			return $test_paper->updateTestPaperQuestions($data);
		}

		// function fetch questions of test paper
		public static function fetchTestPaperDetails($data) {
			require_once "TestPaper.php";
			$test_paper = new TestPaper();
			return $test_paper->fetchTestPaperDetails($data);
		}

		// function set questions of test paper
		public static function fetchQuestionsOfTestPaper($data) {
			require_once "TestPaperQuestions.php";
			$test_paper = new TestPaperQuestions();
			return $test_paper->fetchQuestionsOfTestPaper($data);
		}


		// function to save test papers details
		public static function saveTestPaperDetails($data) {
			require_once "TestPaperQuestions.php";
			$test_paper = new TestPaperQuestions();
			return $test_paper->saveTestPaperDetails($data);
		}

		// function to add a new test paper
		public static function deleteTestPaper($data) {
			require_once "TestPaperQuestions.php";
			$test_paper = new TestPaperQuestions();
			return $test_paper->deleteTestPaper($data);
		}

		// function Save test paper Details
		public static function saveTestPaper($data) {
			require_once "TestPaperQuestions.php";
			$test_paper = new TestPaperQuestions();
			return $test_paper->saveTestPaper($data);
		}

		// function fetch test programs
		public static function getTestprograms($data) {
			require_once "TestProgram.php";
			$test = new TestProgram();
			return $test->getTestprograms($data);
		}

		// function fetch test programs
		public static function addNewTestProgram($data) {
			require_once "TestProgram.php";
			$test = new TestProgram();
			return $test->addNewTestProgram($data);
		}

		// function fetch test paper types
		public static function fetchTestPaperTypes($data) {
			require_once "TestPaper.php";
			$paper = new TestPaper();
			return $paper->fetchTestPaperTypes($data);
		}


		// function get the next question in test papers
		public static function fetchQuestionInformation($data) {
			require_once "TestPaper.php";
			$paper = new TestPaper();
			return $paper->fetchQuestionInformation($data);
		}

		// function get the next question in test papers
		public static function getNextQuestion($data) {
			require_once "TestPaper.php";
			$paper = new TestPaper();
			return $paper->getNextQuestion($data);
		}

		// function get the next question in test papers
		public static function importQuestion($data) {
			require_once "TestPaper.php";
			$paper = new TestPaper();
			return $paper->importQuestion($data);
		}

		// function fetch test paper by types
		public static function fetchTestPaperByType($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->fetchTestPaperByType($data);
		}

		// function to add a new test paper
		public static function deleteAttachedTest($data) {
			require_once "TestProgram.php";
			$test_paper = new TestProgram();
			return $test_paper->deleteAttachedTest($data);
		}

		// function fetch test paper by types
		public static function fetchNumberOfAttachedTestPapers($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->fetchNumberOfAttachedTestPapers($data);
		}

		// function fetch test paper by types
		public static function fetchChaptersOfSubject($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->fetchChaptersOfSubject($data);
		}

		// function fetches test programe details
		public static function fetchTestProgramDetails($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->fetchTestProgramDetails($data);
		}

		public static function fetchProgramDetails($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->fetchProgramDetails($data);
		}

		public static function testProgramDetails($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->testProgramDetails($data);
		}

		public static function fetchProgramPapers($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->fetchProgramPapers($data);
		}



		// function update test programe details
		public static function updateTestProgram($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->updateTestProgram($data);
		}

		// function change test program image
		public static function change_test_program_image($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->change_test_program_image($data);
		}

		// function change test program image
		public static function add_illustration_file($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->add_illustration_file($data);
		}

		// function delete test programe
		public static function deleteTestProgram($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->deleteTestProgram($data);
		}


		// function save attach test paper in programe
		public static function saveAttachedTestPaper($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->saveAttachedTestPaper($data);
		}

		// function fetches test papers attached in test program
		public static function fetchAttachedTestPaper($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->fetchAttachedTestPaper($data);
		}

		// function fetches test papers attached in test program
		public static function fetchAttachedIllustrations($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->fetchAttachedIllustrations($data);
		}

		// function to delete Illustration attached in test program
		public static function deleteIllustration($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->deleteIllustration($data);
		}

		// function fetches test papers attached in test program
		public static function getDataOfQuestions($data) {
			require_once "Question.php";
			$questions = new Question();
			return $questions->getDataOfQuestions($data);
		}

		// function create test paper from selected question
		public static function createQuestionPaper($data) {
			require_once "TestPaperQuestions.php";
			$questions = new TestPaperQuestions();
			return $questions->createQuestionPaper($data);
		}

		/**
		* @author Pushpam Matah
		* @date 20-06-2015 - 30-06-2015
		* @param JSON
		* @return JSON status and data(classes names for schedule planning)
		**/

		/*
			Function to fetch the classes from the database
		*/
		public static function fetchPlanClasses($data) {
			require_once 'PlanApi.php';
			$classes = new PlanApi();
			return $classes->fetchPlanClasses();
		}

		/*
			Function to fetch the subjects from the database
		*/
		public static function fetchPlanSubjects($data) {
			require_once 'PlanApi.php';
			$subjects = new PlanApi();
			return $subjects->fetchPlanSubjects($data);
		}

		/*
			Function to fetch the chapter and time names from the database
		*/
		public static function fetchPlanChaptersTime($data) {
			require_once 'PlanApi.php';
			$chaptersTime = new PlanApi();
			return $chaptersTime->fetchPlanChaptersTime($data);
		}

		/*
			Function to calculate the recommended average time according to the plan
		*/
		public static function findingHours($data) {
			require_once 'PlanApi.php';
			$hours = new PlanApi();
			return $hours->findingHours($data);
		}

		/*
			Function to populate calendar after submit button
		*/
		public static function populateCalendar($data) {
			require_once 'PlanApi.php';
			$hours = new PlanApi();
			return $hours->populateCalendar($data);
		}

		/*
			Function to populate calendar when user logs in
		*/
		public static function calendar($data) {
			require_once 'PlanApi.php';
			$calendar = new PlanApi();
			return $calendar->calendar($data);
		}

		/*
			Function to fetch the planned subjects from the database
		*/
		public static function fetchPlannedSubjects($data) {
			require_once 'PlanApi.php';
			$plan = new PlanApi();
			return $plan->fetchPlannedSubjects($data);
		}

		/*
			Function to edit the planned subjects
		*/
		public static function editPlannedSubjects($data) {
			require_once 'PlanApi.php';
			$plan = new PlanApi();
			return $plan->editPlannedSubjects($data);
		}

		public static function deletePlannedSubject($data) {
			require_once 'PlanApi.php';
			$plan = new PlanApi();
			return $plan->deletePlannedSubject($data);
		}

		/*
			Function to update the planned subjects in the database
		*/
		public static function updatePlannedSubjects($data) {
			require_once 'PlanApi.php';
			$plan = new PlanApi();
			return $plan->updatePlannedSubjects($data);
		}

		/*
			Function to check the time clashes while making plan
		*/
		public static function checkTimeClashes($data) {
			require_once 'PlanApi.php';
			$plan = new PlanApi();
			return $plan->checkTimeClashes($data);
		}

		/*
			Function to check the clashes while making plan for a new subject
		*/
		public static function checkSubjectClashes($data) {
			require_once 'PlanApi.php';
			$plan = new PlanApi();
			return $plan->checkSubjectClashes($data);
		}

		/*
			Function to check the clashes while making plan for a new subject
		*/
		public static function findAvailableHours($data) {
			require_once 'PlanApi.php';
			$plan = new PlanApi();
			return $plan->findAvailableHours($data);
		}


		/*
			Function to check the clashes while making plan for a new subject
		*/
		public static function fetchStudentProfileData($data) {
			require_once 'StudentProfile.php';
			$profile = new StudentProfile();
			return $profile->fetchStudentProfileData($data);
		}

		/*
			Function to check the clashes while making plan for a new subject
		*/
		public static function updateProfile($data) {
			require_once 'ProfileApi.php';
			$profile = new ProfileApi();
			return $profile->updateProfile($data);
		}

		/*
			Function to check the clashes while making plan for a new subject
		*/
		public static function changeProfilePassword($data) {
			require_once 'ProfileApi.php';
			$profile = new ProfileApi();
			return $profile->changeProfilePassword($data);
		}

		public static function changeProfilePassword1($data) {
			require_once 'StudentProfile.php';
			$profile = new StudentProfile();
			return $profile->changeProfilePassword1($data);
		}

		/*
			Function to get a test program available for sale
		*/
		public static function getTestprogramsAvailableForSave($data) {
			require_once 'StudentProgram.php';
			$program = new StudentProgram();
			return $program->getTestprogramsAvailableForSave($data);
		}

		public static function filterPrograms($data) {
			require_once 'StudentProgram.php';
			$program = new StudentProgram();
			return $program->filterPrograms($data);
		}

		/*
			Function to buy a test program
		*/
		public static function buyTestProgram($data) {
			require_once 'StudentProgram.php';
			$program = new StudentProgram();
			return $program->buyTestProgram($data);
		}

		public static function assignPrograms($data) {
			require_once 'StudentProgram.php';
			$program = new StudentProgram();
			return $program->assignPrograms($data);
		}

		public static function assignProgramStudent($data) {
			require_once 'ManageStudentsOfInstitute.php';
			$program = new ManageStudentsOfInstitute();
			return $program->assignProgramStudent($data);
		}

		public static function fetchNotifications($data) {
			require_once 'ManageStudentsOfInstitute.php';
			$program = new ManageStudentsOfInstitute();
			return $program->fetchNotifications($data);
		}

		public static function assignProgramsInstitute($data) {
			require_once 'StudentProgram.php';
			$program = new StudentProgram();
			return $program->assignProgramsInstitute($data);
		}

		/*
			Function to buy a test program {success}
		*/
		public static function buyProgram($data) {
			require_once 'Payment.php';
			$program = new Payment();
			return $program->buyProgram($data);
		}

		public static function ifBought($data) {
			require_once 'Payment.php';
			$program = new Payment();
			return $program->ifBought($data);
		}

		public static function buyFailProgram($data) {
			require_once 'Payment.php';
			$program = new Payment();
			return $program->buyFailProgram($data);
		}

		/*
			Function to buy a test program
		*/
		public static function getProgramDetails($data) {
			require_once 'ProgramApi.php';
			$program = new ProgramApi();
			return $program->getProgramDetails($data);
		}

		/*
			Function to get the program names
		*/
		public static function displayPrograms($data) {
			require_once 'ProgramApi.php';
			$program = new ProgramApi();
			return $program->displayPrograms1($data);
		}

		/*
			Function to get details of the respective programs
		*/
		public static function displayProgramDetails($data) {
			require_once 'ProgramApi.php';
			$program = new ProgramApi();
			return $program->displayProgramDetails($data);
		}

		/*
			Function to get details of the respective test papers
		*/
		public static function displayTestPapers($data) {
			require_once 'ProgramApi.php';
			$program = new ProgramApi();
			return $program->displayTestPapers($data);
		}

		/*
			Function to get details of the respective test papers
		*/
		public static function fetchSections($data) {
			require_once 'TestPaperDeliverApi.php';
			$testPaper = new TestPaperDeliverApi();
			return $testPaper->fetchSections($data);
		}

		/*
			Function to launch test papers
		*/
		public static function launchPaper($data) {
			require_once 'TestPaperDeliverApi.php';
			$testPaper = new TestPaperDeliverApi();
			return $testPaper->launchPaper($data);
		}

		// function submit test paper
		public static function submitPaper($data) {
			require_once "TestPaperDeliverApi.php";
			$questions = new TestPaperDeliverApi();
			return $questions->submitPaper($data);
		}

		// function to save question response by student
		public static function saveQuestionResponse($data) {
			require_once "TestPaperDeliverApi.php";
			$questions = new TestPaperDeliverApi();
			return $questions->saveQuestionResponse($data);
		}

		// function to show result of attempt
		public static function get_attempt_history($data) {
            chdir(__DIR__);

            require_once "StudentResults.php";
            $result = new StudentResults();
            return $result->get_attempt_history($data);
        }

        public static function fetchLeaderboard($data) {
            chdir(__DIR__);

            require_once "StudentResults.php";
			$result = new StudentResults();
			return $result->fetchLeaderboard($data);
		}

		// function to fetch question explanation
			public static function attemptedQuestionExplanation($data) {
			require_once "StudentResults.php";
			$result = new StudentResults();
			return $result->attemptedQuestionExplanation($data);
		}

		// function to fetch create obtmizer test paper
			public static function createOptmizerTest($data) {
			require_once "StudentResults.php";
			$result = new StudentResults();
			return $result->createOptmizerTest($data);
		}

		/**
		*
		*/
		public static function fetchOptmizerReport($data) {
			require_once "StudentResults.php";
			$result = new StudentResults();
			return $result->fetchOptmizerReport($data);
		}

		/*
		* Function to get the programs
		*/
		public static function getTestProgramsOfStudent($data) {
			require_once 'StudentReportCard.php';
			$program = new StudentReportCard();
			return $program->getTestProgramsOfStudent($data);
		}

		/**
		* Function to get subjects of program
		*/
		public static function getTestProgramsSubjects($data) {
			require_once 'StudentReportCard.php';
			$program = new StudentReportCard();
			return $program->getTestProgramsSubjects($data);
		}

		/**
		* Function to get subjects of program
		*/
		public static function getReportCardData($data) {
			require_once 'StudentReportCard.php';
			$program = new StudentReportCard();
			return $program->getReportCardData($data);
		}

		/**
		* Function to fetch test papers reports
		*/
		public static function fetchTestsReport($data) {
			require_once 'AdminResultAndReport.php';
			$program = new AdminResultAndReport();
			return $program->fetchTestsReport($data);
		}
		public static function fetchexamattemptreport($data) {
			require_once 'ExamAttemptReport.php';
			$program = new ExamAttemptReport();
			return $program->fetchexamattemptreport($data);
		}

		/**
		* Function to get reports of selected test
		*/
		public static function fetchSelectedTestReport($data) {
			require_once 'AdminResultAndReport.php';
			$program = new AdminResultAndReport();
			return $program->fetchSelectedTestReport($data);
		}

		/**
		* Function to get reports of selected test
		*/
		public static function fetchSelectedStudentTestReport($data) {
			require_once 'AdminResultAndReport.php';
			$program = new AdminResultAndReport();
			return $program->fetchSelectedStudentTestReport($data);
		}

		/**
		* Function to genrate and download Coupons
		*/
		public static function downloadCoupons($data) {
			require_once 'Coupons.php';
			$Coupons = new Coupons();
			return $Coupons->downloadCoupons($data);
		}

		public static function createEnrollment($data) {
			require_once 'Enrollment.php';
			$Coupons = new Enrollment();
			return $Coupons->createEnrollment($data);
		}

		public static function applyEnrollment($data) {
			require_once 'Enrollment.php';
			$Coupons = new Enrollment();
			return $Coupons->applyEnrollment($data);
		}

		/**
		* Function to genrate and download Coupons
		*/
		public static function fetchInstitutCoupons($data) {
			require_once 'Coupons.php';
			$Coupons = new Coupons();
			return $Coupons->fetchInstitutCoupons($data);
		}

		public static function fetchEnrollment($data) {
			require_once 'Enrollment.php';
			$Coupons = new Enrollment();
			return $Coupons->fetchEnrollment($data);
		}

		/**
		*
		*/
		public static function redeemCoupon($data) {
			require_once 'Coupons.php';
			$Coupons = new Coupons();
			return $Coupons->redeemCoupon($data);
		}

		/**
		*
		*/
		public static function markQuestionEvent($data) {
			require_once 'QuestionBank.php';
			$program = new QuestionBank();
			return $program->markQuestionEvent($data);
		}

		/**
		*
		*/
		public static function fetchStudentQuestions($data) {
			require_once 'QuestionBank.php';
			$program = new QuestionBank();
			return $program->fetchStudentQuestions($data);
		}

		/**
		*
		*/
		public static function fetchStudentDifficulties($data) {
			require_once 'QuestionBank.php';
			$program = new QuestionBank();
			return $program->fetchStudentDifficulties($data);
		}

		/**
		*
		*/
		public static function createStudentTestPaper($data) {
			require_once 'QuestionBank.php';
			$program = new QuestionBank();
			return $program->createStudentTestPaper($data);
		}

		/**
		*
		*/
		public static function deleteQuestionFromMyQuestionBank($data) {
			require_once 'QuestionBank.php';
			$program = new QuestionBank();
			return $program->deleteQuestionFromMyQuestionBank($data);
		}

		/**
		*
		*/
		public static function fetchRewardData($data) {
			require_once 'ProgramApi.php';
			$program = new ProgramApi();
			return $program->fetchRewardData($data);
		}

		/**
		*
		*/
		public static function fetchInstituteStudents($data) {
			require_once 'ManageStudentsOfInstitute.php';
			$program = new ManageStudentsOfInstitute();
			return $program->fetchInstituteStudents($data);
		}

		/**
		*
		*/
		public static function addInstituteStudents($data) {
			require_once 'ManageStudentsOfInstitute.php';
			$program = new ManageStudentsOfInstitute();
			return $program->addInstituteStudents($data);
		}

		public static function UpdateStudentDetails($data) {
			require_once 'ManageStudentsOfInstitute.php';
			$program = new ManageStudentsOfInstitute();
			return $program->UpdateStudentDetails($data);
		}

		/**
		*
		*/
		public static function addInstituteAssistant($data) {
			require_once 'ManageStudentsOfInstitute.php';
			$program = new ManageStudentsOfInstitute();
			return $program->addInstituteStudents($data);
		}

		/**
		*
		*/
		public static function fetchInstituteAssistant($data) {
			require_once 'ManageStudentsOfInstitute.php';
			$program = new ManageStudentsOfInstitute();
			return $program->fetchInstituteAssistant($data);
		}

		/**
		*
		*/
		public static function fetchInstitutePrograms($data) {
			require_once 'ManageStudentsOfInstitute.php';
			$program = new ManageStudentsOfInstitute();
			return $program->fetchInstitutePrograms($data);
		}

		/**
		*
		*/
		public static function fetchInstituteProfile($data) {
			require_once 'InstituteProfile.php';
			$profile = new InstituteProfile();
			return $profile->fetchInstituteProfile($data);
		}

		/**
		*
		*/
		public static function updateInstituteProfile($data) {
			require_once 'InstituteProfile.php';
			$profile = new InstituteProfile();
			return $profile->updateInstituteProfile($data);
		}

		/**
		*
		*/
		public static function updateInstitutePassword($data) {
			require_once 'InstituteProfile.php';
			$profile = new InstituteProfile();
			return $profile->updateInstitutePassword($data);
		}

		/**
		*
		*/
		public static function getTestPaperPerformanceReport($data) {
			require_once 'InstituteReportCard.php';
			$profile = new InstituteReportCard();
			return $profile->getTestPaperPerformanceReport($data);
		}


		/*
		* Dashboard Login function
		*/
		public static function fetchUserDashboard($data) {
			require_once 'UserDashboard.php';
			$dash = new UserDashboard();
			return $dash->fetchUserDashboard($data);
		}
		public static function fetchReports($data) {
			require_once 'UserDashboard.php';
			$dash = new UserDashboard();
			return $dash->fetchReports($data);
		}
		public static function fetchPrograms($data) {
			require_once 'UserDashboard.php';
			$dash = new UserDashboard();
			return $dash->fetchPrograms($data);
		}



		/*--------------------------------------------------------------------------
		 *
		 *--------------------------------------------------------------------------
		 *
		 * @author Ravi Arya
		 * @date 27-02-2016
		 * @param paper => test_paper id
		 * @return questions in the test paper attempt
		 * @todo todo
		 *
		 *--------------------------------------------------------------------------
		 */

		public static function fetchQuestionPaper($data) {
			require_once 'LaunchApi.php';
			$dash = new LaunchApi();
			return $dash->fetchQuestionPaper($data);
		}

		public static function saveQuestionPaper($data) {
			require_once 'LaunchApi.php';
			$dash = new LaunchApi();
			return $dash->saveQuestionPaper($data);
		}

		public static function saveQuestion($data) {
			require_once 'LaunchApi.php';
			$dash = new LaunchApi();
			return $dash->saveQuestion($data);
		}

		public static function testPaperDetails($data) {
			require_once 'LaunchApi.php';
			$dash = new LaunchApi();
			return $dash->testPaperDetails($data);
		}

		public static function getSubjectPapers($data) {
			require_once 'LaunchApi.php';
			$dash = new LaunchApi();
			return $dash->getSubjectPapers($data);
		}

		public static function getSubjectPapers1($data) {
			require_once 'LaunchApi.php';
			$dash = new LaunchApi();
			return $dash->getSubjectPapers1($data);
		}

		public static function checkScheduled($data) {
			require_once 'LaunchApi.php';
			$dash = new LaunchApi();
			return $dash->checkScheduled($data);
		}

		public static function createDiscount($data) {
			require_once 'Discount.php';
			$dash = new Discount();
			return $dash->createDiscount($data);
		}

		public static function fetchDiscount($data) {
			require_once 'Discount.php';
			$dash = new Discount();
			return $dash->fetchDiscount($data);
		}

		public static function updateDiscount($data) {
			require_once 'Discount.php';
			$dash = new Discount();
			return $dash->updateDiscount($data);
		}

		public static function applyDiscount($data) {
			require_once 'Discount.php';
			$dash = new Discount();
			return $dash->applyDiscount($data);
		}

		/*--------------------------------------------------------------------------
		 *
		 *--------------------------------------------------------------------------
		 *
		 * @author Ravi Arya
		 * @date 27-02-2016
		 * @param paper => test_paper id
		 * @return questions in the test paper attempt
		 * @todo todo
		 *
		 *--------------------------------------------------------------------------
		 */
		public static function fetchBBTemplate($data) {
			require_once 'BBTemplate.php';
			$dash = new BBTemplate();
			return $dash->fetchBBTemplate($data);
		}

		public static function b2blogo($data) {
			require_once 'BBTemplate.php';
			$dash = new BBTemplate();
			return $dash->b2blogo($data);
		}

		public static function b2bbanner($data) {
			require_once 'BBTemplate.php';
			$dash = new BBTemplate();
			return $dash->b2bbanner($data);
		}

		public static function saveB2BTemplate($data) {
			require_once 'BBTemplate.php';
			$dash = new BBTemplate();
			return $dash->saveB2BTemplate($data);
		}

		public static function downloadB2BTemplate($data) {
			require_once 'BBTemplate.php';
			$dash = new BBTemplate();
			return $dash->downloadB2BTemplate($data);
		}

		public static function generateStudentIds($data) {
			require_once 'BBTemplate.php';
			$dash = new BBTemplate();
			return $dash->generateStudentIds($data);
		}

		public static function b2blogin($data) {
			require_once 'BBTemplate.php';
			$dash = new BBTemplate();
			return $dash->b2blogin($data);
		}

		public static function pdfImport($data) {
			require_once "PDFImport.php";
			$pdf = new PDFImport();
			return $pdf->pdfUpload($data);
		}

		public static function excelImport($data) {
			// require_once "ExcelImport.php";
			require_once "ExcelImport.php";
			// $excel = new ExcelImport();
			$excel = new ExcelImport();
			return $excel->exclImport($data);
		}

		public static function xmlImport($data) {
			require_once "XMLImport.php";
			$excel = new XMLImport();
			return $excel->exmlImport($data);
		}

		public static function jsonImport($data) {
			require_once "JSONImport.php";
			$excel = new JSONImport();
			return $excel->jsonImport1($data);
		}

		public static function fetchTestForStudent($data) {
			require_once 'AdminResultAndReport.php';
			$dash = new AdminResultAndReport();
			return $dash->fetchTestForStudent($data);
		}

		public static function listingStudyMaterial($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->listingStudyMaterial($data);
		}


		public static function deleteStudyMaterial($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->deleteStudyMaterial($data);
		}

		public static function addBatch($data) {
			require_once "Batch.php";
			$batch = new Batch();
			return $batch->addBatch($data);
		}

		public static function getBatchData($data) {
			require_once "Batch.php";
			$batch = new Batch();
			return $batch->getBatchData($data);
		}

		public static function deleteBatchRow($data) {
			require_once "Batch.php";
			$batch = new Batch();
			return $batch->deleteBatchRow($data);
		}

		public static function updateBatchRow($data) {
			require_once "Batch.php";
			$batch = new Batch();
			return $batch->updateBatchRow($data);
		}

		public static function fetchBatches($data) {
			require_once "Batch.php";
			$batch = new Batch();
			return $batch->fetchBatches($data);
		}

		public static function fetchStudentAssignments($data) {
			require_once "Assignment.php";
			$batch = new Assignment();
			return $batch->fetchStudentAssignments($data);
		}

		public static function changePackageStatus($data) {
			require_once "TestProgram.php";
			$program = new TestProgram();
			return $program->changePackageStatus($data);
		}

		public static function deleteAssignment($data) {
			require_once "Assignment.php";
			$program = new Assignment();
			return $program->deleteAssignment($data);
		}

		public static function submitContactForm($data) {
			require_once "User.php";
			$user = new User();
			return $user->submitContactForm($data);
		}

		public static function store($data) {
			require_once "Store.php";
			$user = new Store();
			return $user->store($data);
		}

		public static function studentLogin($data) {
			require_once 'ManageStudentsOfInstitute.php';
			$program = new ManageStudentsOfInstitute();
			return $program->studentLogin($data);
		}

		public static function updateAssistantDetails($data) {
			require_once 'ManageStudentsOfInstitute.php';
			$program = new ManageStudentsOfInstitute();
			return $program->updateAssistantDetails($data);
		}

		public static function updateAssistantPassword($data) {
			require_once 'ManageStudentsOfInstitute.php';
			$program = new ManageStudentsOfInstitute();
			return $program->updateAssistantPassword($data);
		}

		public static function DeleteAssistantDetails($data) {
			require_once 'ManageStudentsOfInstitute.php';
			$program = new ManageStudentsOfInstitute();
			return $program->DeleteAssistantDetails($data);
		}

		public static function fetchStudentsProgram($data) {
			require_once 'ManageStudentsOfInstitute.php';
			$program = new ManageStudentsOfInstitute();
			return $program->fetchStudentsProgram($data);
		}

		public static function fetchTestProgram($data) {
			require_once 'ManageStudentsOfInstitute.php';
			$program = new ManageStudentsOfInstitute();
			return $program->fetchTestProgram($data);
		}

		public static function updateStudentProgram($data) {
			require_once 'ManageStudentsOfInstitute.php';
			$program = new ManageStudentsOfInstitute();
			return $program->updateStudentProgram($data);
		}

		public static function addCredits($data) {
			require_once 'ManageStudentsOfInstitute.php';
			$program = new ManageStudentsOfInstitute();
			return $program->addCredits($data);
		}
	}
?>