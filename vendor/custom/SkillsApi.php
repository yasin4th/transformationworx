<?php
	@session_start;
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	
	//including connection.php file to make connection with database
	require_once 'Connection.php';
	
	class SkillsApi extends Connection{
		
		/**
		* function fetch Skill details to show the Skills and their description to user on Skill.php file
		*
		* @author Utkarsh Pandey
		* @date 17-05-2015
		* @param JSON limit and offset
		* @return JSON status and data(Skills and description)
		**/
		public function getSkillsData($data) {
			
			$result = new stdClass();
			
			try{
				
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('skills');
				$select->where(array(
					'delete'	=>	0
				));
				//$select->limit($data->limit);
				//$select->offset($data->offset);
				$select->columns(array('id','name' => 'skill', 'description',));
				
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				$result->status = 1;
				$result->data = $res;
				return $result;
				
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
			
		}
		
		
		/**
		* function add new skill details row in skills table
		*
		* @author Utkarsh Pandey
		* @date 17-05-2015
		* @param JSON with all required details
		* @return JSON status and message
		**/
		public function addSkill($data) {  
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('skills');
				$select->where(array(
					'skill'	=>	htmlentities($data->skill),
					'delete'	=>	0
				));
				$select->columns(array('id','skill', 'description',));
	
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				if(count($res) == 0) {
					$insert = new TableGateway('skills', $adapter, null, new HydratingResultSet());
					$insert->insert(array(
						'skill'	=>	htmlentities($data->skill),
						'description'	=>	htmlentities($data->description),
						'user_id'	=>	1, //$_SESSION['userId'],
						'created'	=>	time()
					));
					$id= $insert->getLastInsertValue();
					if($id) {
							$result->id = $id;
							$result->status = 1;
							$result->message = "Row Success Added";
							return $result;
						}
						else {
							$result->status = 0;
							$result->message = "Error Occured";
							return $result;
						}
				}
				else {
					$result->status = 0;
					$result->message = "Duplicate Skill entry.";
					return $result;
				}
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		
		
		/**
		* function sets a row deleted in skills table
		*
		* @author Utkarsh Pandey
		* @date 17-05-2015
		* @param JSON id
		* @return JSON status and message
		**/
		public function DeleteSkillRow($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('skills');
				$update->set(array(
					'delete'	=> 1,
					'created'	=>	time()
				));
				$update->where(array(
					'id' => $data->id
				));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				$result->status = 1;
				$result->message = "Successfully Deleted.";
				return $result;
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
		
		
		/**
		* function update Skill and description in a row of Skills table
		*
		* @author Utkarsh Pandey
		* @date 17-05-2015
		* @param JSON with all required details
		* @return JSON status and message
		**/
		public function updateSkillRow($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);								
				$select = $sql->select();
				$select->from('skills');
				$select->where(array(
					'skill'	=>	htmlentities($data->skill),
					'delete'	=>	0
				));
				$select->columns(array('id','skill', 'description',));
	
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = array();
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE); //$run can be modify
				$res = $run->toArray();
				if(count($res) == 0) {
					$update = $sql->update();
					$update->table('skills');
					$update->set(array(
						'skill'	=> htmlentities($data->skill),
						'description'	=>	htmlentities($data->description),
						'created'	=>	time()
					));
					$update->where(array(
						'id' => $data->id
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
					$result->status = 1;
					$result->message = "Successfully Update.";
					return $result;
				}
				else if(count($res) == 1 && $res[0]['id'] == $data->id) {
						$result->status = 1;
						$result->message = "Successfully Update.";
						return $result;
				}
				else {
					$result->status = 0;
					$result->message = 'Not Updated because this Skill already created.';
					return $result;
				}
			}catch(Exception $e){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}
	}
	
?>