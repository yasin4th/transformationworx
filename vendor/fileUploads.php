<?php
date_default_timezone_set('Asia/Kolkata');

//loading zend framework
include __DIR__.'/Zend/Loader/AutoloaderFactory.php';
Zend\Loader\AutoloaderFactory::factory(array(
		'Zend\Loader\StandardAutoloader' => array(
				'autoregister_zf' => true,
				'db' => 'Zend\Db\Sql'
		)
));

$result = new stdClass();

if(isset($_POST['action'])) {
	include 'custom/Api.php';
	$data = new stdClass();
	$action = $_POST['action'];

	switch ($action) {

		case 'qtiZipUpload':
			
			@session_start();
			if(isset($_SESSION['userId'])) {
				$data->user = $_SESSION['user'];
				$data->userId = $_SESSION['userId'];
				$data->class_id = $_POST['classes'];
				$data->subject_ids = (empty($_POST['subjects']))? array():$_POST['subjects'];
				$data->unit_ids = (empty($_POST['units']))? array():$_POST['units'];
				$data->chapter_ids = (empty($_POST['chapters']))? array():$_POST['chapters'];
				$data->topic_ids = (empty($_POST['topics']))? array():$_POST['topics'];
				$data->tag_ids = (empty($_POST['tags']))? array():$_POST['tags'];
				$data->difficultyLevel_ids = (empty($_POST['difficulty-levels']))?array(): $_POST['difficulty-levels'];
				$data->skill_ids = (empty($_POST['skills']))?array():$_POST['skills'];
				if (isset($_FILES['questions_file']) && !empty($_FILES['questions_file'])) {
					@$data->file = $_FILES['questions_file'];
					@$result = Api::qtiZipUpload($data);
				}
				else {
					$result->status = 2;
					$result->message = "File not Seleted.";
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
			break;		

		case 'pdfUpload':
			@session_start();
			if(isset($_SESSION['userId'])) {
				$data->user = $_SESSION['user'];
				$data->userId = $_SESSION['userId'];
				$data->class_id = $_POST['classes'];
				$data->subject_ids = (empty($_POST['subjects']))? array():$_POST['subjects'];
				$data->unit_ids = (empty($_POST['units']))? array():$_POST['units'];
				$data->chapter_ids = (empty($_POST['chapters']))? array():$_POST['chapters'];
				$data->topic_ids = (empty($_POST['topics']))? array():$_POST['topics'];
				$data->tag_ids = (empty($_POST['tags']))? array():$_POST['tags'];
				$data->difficultyLevel_ids = (empty($_POST['difficulty-levels']))?array(): $_POST['difficulty-levels'];
				$data->skill_ids = (empty($_POST['skills']))?array():$_POST['skills'];
				if (isset($_FILES['questions_file']) && !empty($_FILES['questions_file'])) {
					@$data->file = $_FILES['questions_file'];
					$result = Api::pdfUpload($data);
				}
				else {
					$result->status = 2;
					$result->message = "File not Seleted.";
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
			break;

		case 'pdfImport':
			@session_start();
			if(isset($_SESSION['userId'])) {
				$data->user = $_SESSION['user'];
				$data->userId = $_SESSION['userId'];
				$data->class_id = $_POST['classes'];
				$data->subject_ids = (empty($_POST['subjects']))? array():$_POST['subjects'];
				$data->unit_ids = (empty($_POST['units']))? array():$_POST['units'];
				$data->chapter_ids = (empty($_POST['chapters']))? array():$_POST['chapters'];
				$data->topic_ids = (empty($_POST['topics']))? array():$_POST['topics'];
				$data->tag_ids = (empty($_POST['tags']))? array():$_POST['tags'];
				$data->difficultyLevel_ids = (empty($_POST['difficulty-levels']))?array(): $_POST['difficulty-levels'];
				if (isset($_FILES['questions_file']) && !empty($_FILES['questions_file'])) {
					@$data->file = $_FILES['questions_file'];
					
					$result = Api::pdfImport($data);
				}
				else {
					$result->status = 2;
					$result->message = "File not Seleted.";
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
			break;

		case 'excelImport':
			
			@session_start();
			if(isset($_SESSION['userId'])) {
				
				$data->user = $_SESSION['user'];
				$data->userId = $_SESSION['userId'];
				$data->class_id = $_POST['classes'];
				$data->subject_ids = (empty($_POST['subjects']))? array():$_POST['subjects'];
				$data->unit_ids = (empty($_POST['units']))? array():$_POST['units'];
				$data->chapter_ids = (empty($_POST['chapters']))? array():$_POST['chapters'];
				$data->topic_ids = (empty($_POST['topics']))? array():$_POST['topics'];
				$data->tag_ids = (empty($_POST['tags']))? array():$_POST['tags'];
				$data->difficultyLevel_ids = (empty($_POST['difficulty-levels']))?array(): $_POST['difficulty-levels'];
				$data->skills_ids = (empty($_POST['skills']))?array(): $_POST['skills'];
				
				if (isset($_FILES['questions_file']) && !empty($_FILES['questions_file'])) {
					@$data->file = $_FILES['questions_file'];
					$result = Api::excelImport($data);
				}
				else {
					$result->status = 2;
					$result->message = "File not Seleted.";
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
			break;


		case 'xmlImport':
			@session_start();
			if(isset($_SESSION['userId'])) {
				
				$data->user = $_SESSION['user'];
				$data->userId = $_SESSION['userId'];
				$data->class_id = $_POST['classes'];
				$data->subject_ids = (empty($_POST['subjects']))? array():$_POST['subjects'];
				$data->unit_ids = (empty($_POST['units']))? array():$_POST['units'];
				$data->chapter_ids = (empty($_POST['chapters']))? array():$_POST['chapters'];
				$data->topic_ids = (empty($_POST['topics']))? array():$_POST['topics'];
				$data->tag_ids = (empty($_POST['tags']))? array():$_POST['tags'];
				$data->difficultyLevel_ids = (empty($_POST['difficulty-levels']))?array(): $_POST['difficulty-levels'];
				$data->skills_ids = (empty($_POST['skills']))?array(): $_POST['skills'];
				
				if (isset($_FILES['questions_file']) && !empty($_FILES['questions_file'])) {
					@$data->file = $_FILES['questions_file'];
					$result = Api::xmlImport($data);
				}
				else {
					$result->status = 2;
					$result->message = "File not Seleted.";
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
			break;

			
		case 'jsonImport':
			
			@session_start();
			if(isset($_SESSION['userId'])) {
				
				$data->user = $_SESSION['user'];
				$data->userId = $_SESSION['userId'];
				$data->class_id = $_POST['classes'];
				$data->subject_ids = (empty($_POST['subjects']))? array():$_POST['subjects'];
				$data->unit_ids = (empty($_POST['units']))? array():$_POST['units'];
				$data->chapter_ids = (empty($_POST['chapters']))? array():$_POST['chapters'];
				$data->topic_ids = (empty($_POST['topics']))? array():$_POST['topics'];
				$data->tag_ids = (empty($_POST['tags']))? array():$_POST['tags'];
				$data->difficultyLevel_ids = (empty($_POST['difficulty-levels']))?array(): $_POST['difficulty-levels'];
				$data->skills_ids = (empty($_POST['skills']))?array(): $_POST['skills'];
				
				if (isset($_FILES['questions_file']) && !empty($_FILES['questions_file'])) {
					@$data->file = $_FILES['questions_file'];
					$result = Api::jsonImport($data);
				}
				else {
					$result->status = 2;
					$result->message = "File not Seleted.";
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
			break;

		case 'studentChangeProfilePicture':
			@session_start();
			if(isset($_SESSION['userId'])) {
				$data->user = $_SESSION['user'];
				$data->userId = $_SESSION['userId'];
				$data->action = $_POST['action'];
				
				$data->image = $_FILES['profile-picture'];
				$result = Api::updateUserAvtar($data);
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
			break;

		case 'change_test_program_image':
			@session_start();
			if(isset($_SESSION['userId'])) {
				$data->user = $_SESSION['user'];
				$data->userId = $_SESSION['userId'];
				$data->action = $_POST['action'];
				$data->id = $_POST['program_id'];
				
				$data->image = $_FILES['change-test-program-image'];
				$result = Api::change_test_program_image($data);
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
			break;

		case 'add_illustration_file':
			@session_start();
			if(isset($_SESSION['userId'])) {
				$data->user = $_SESSION['user'];
				$data->userId = $_SESSION['userId'];
				$data->action = $_POST['action'];
				$data->name = $_POST['illustration-name'];
				$data->program_id = $_POST['program_id'];
				$data->chapter_id = $_POST['chapter_id'];
				
				$data->file = $_FILES['illustration-file'];
				// echo json_encode($data);
				$result = Api::add_illustration_file($data);
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
			break;

		case 'updateInstituteAvtar':
			@session_start();
			if(isset($_SESSION['userId'])) {
				$data->user = $_SESSION['user'];
				$data->userId = $_SESSION['userId'];
				$data->action = $_POST['action'];
				$data->image = $_FILES['select-avtar'];
				if (isset($data->image['tmp_name'])) {
					$result = Api::updateUserAvtar($data);
				}else {
					$result->status = 0;
					$result->message = "File not found, please try again.";
				}
				// echo json_encode($data);
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
			break;

		case 'uploadStudentAssignment':
			@session_start();
			if(isset($_SESSION['userId'])) {
				$data->user = $_SESSION['user'];
				$data->userId = $_SESSION['userId'];
				$data->action = $_POST['action'];
				$data->test_program_id = $_POST['test_program_id'];
				$data->image = $_FILES['assignment_file'];
				if (isset($data->image['tmp_name'])) {
					$result = Api::uploadStudentAssignment($data);
				}else {
					$result->status = 0;
					$result->message = "File not found, please try again.";
				}
				// echo json_encode($data);
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
			break;

		case 'b2blogo':

			@session_start();
			if(isset($_SESSION['userId'])) {
				$data->user = $_SESSION['user'];
				$data->userId = $_SESSION['userId'];
				
				
				$data->id = $_POST['id'];
				
				if ($_FILES['logo']['name']) {
					$data->logo = $_FILES['logo'];
				}
								
				$result = Api::b2blogo($data);
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
			break;

		case 'b2bbanner':

			@session_start();
			if(isset($_SESSION['userId'])) {
				$data->user = $_SESSION['user'];
				$data->userId = $_SESSION['userId'];
				
				
				$data->id = $_POST['id'];
				
				if ($_FILES['banner']['name']) {
					$data->banner = $_FILES['banner'];
				}
								
				$result = Api::b2bbanner($data);
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
			break;

		default:
			$result->status = 0;
			$result->message = 'Invalid Input set. Please try again.';
			break;
	}
}
else
{
	$result->status = 0;
	$result->message = 'Invalid Input set. Please try again.';
}

echo json_encode($result);
