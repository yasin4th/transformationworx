<?php ob_start(); require_once '../vendor/AccessApi.php';?>
<?php
	class AccessApi {

		public function checkAccess() {
			$pages = array();
			//*****************************//
			$pages['dashboard.php'] = array(1,2,3);
			//*****************************//
			
			//*****************************//
			$pages['add-test-program.php'] = array(1);
			//*****************************//

			try {
				$filePath = $_SERVER['PHP_SELF'];
				$filePathTokens = explode('/', $filePath);
				$tokens = $filePathTokens;
				$fileName = end($filePathTokens);
				
				if (isset($pages[$fileName])) {
					$pages_access_role_ids = $pages[$fileName];
					@session_start();
					$user_role_id = $_SESSION['role'];
					if (!in_array($user_role_id, $pages_access_role_ids)) {
						header('location: index.php');
						die();
					}
				}
				else
				{
					return;
				}
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
	}

	$AccessApi = new AccessApi();
	$AccessApi->checkAccess();
?>