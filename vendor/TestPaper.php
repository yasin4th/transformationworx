<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	require_once 'Connection.php';
	require_once("TestPaperQuestions.php");


	class TestPaper extends Connection {

		/**
		* function to create test-paper
		*
		* @author Utkarsh Pandey
		* @date 25-06-2015
		* @param json with all data
		* @return json with status,message and all data
		**/
		public function createTestPaper($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				// INSERT INTO `test_paper`(`name`, `Instructions`, `user_id`, `created`) VALUES ("untitled","Please Fill Instructions.",$data->user_id,time())

				$insert_test_paper = new TableGateway('test_paper', $adapter, null, new HydratingResultSet());

				$insert_test_paper->insert(array(
					'name' => "untitled",
					'instructions'	=>	"Please Fill Instructions.",
					'user_id'	=>	$data->userId,
					'created'	=>	time()
				));
				$test_paper_id = $insert_test_paper->getLastInsertValue();

				$insert_section = new TableGateway('section', $adapter, null, new HydratingResultSet());
				foreach($data->sections as $section) {

					// INSERT INTO `section`(`name`, `total_question`, `test_paper`) VALUES ('sec_name','Total_question','test_paper_id')

					$insert_section->insert(array(
						'name'	=>	$section->sectionName,
						'cut_off_marks'	=>	$section->cut_off_marks,
						'total_question'	=>	$section->totalQuestion,
						'test_paper'	=>	$test_paper_id
					));
					$section_id = $insert_section->getLastInsertValue();

					$insert_section_lables = new TableGateway('test_paper_labels', $adapter, null, new HydratingResultSet());
					foreach($section->questions as $question) {

						// INSERT INTO `test_paper_labels`(`section_id`, `class_id`, `subject_id`, `unit_id`, `chapter_id`, `topic_id`, `question_type`, `number_of_question`, `difficulty_level`, `skill_id`, `max_marks`, `neg_marks`) **ALL VALUES()**

						$insert_section_lables->insert(array(
							'section_id'	=>	$section_id,
							'class_id' => $question->classId,
							'subject_id'	=>	$question->subjectId,
							'unit_id'	=>	$question->unitId,
							'chapter_id'	=>	$question->chapter,
							'topic_id'	=>	$question->topic,
							'question_type'	=>	$question->questionType,
							'number_of_question'	=>	$question->numberOfQuestions,
							'difficulty_level'	=>	$question->difficulty,
							'skill_id'	=>	$question->skill,
							'max_marks'	=>	$question->maxMark,
							'neg_marks'	=>	$question->negMark
						));

						$lable_id = $insert_section_lables->getLastInsertValue();
						$insert_tags_of_lable = new TableGateway('tags_of_test_paper_label', $adapter, null, new HydratingResultSet());
						if($question->Tags != 0)
						foreach($question->Tags as $tag) {

							// INSERT INTO `tags_of_test_paper_label`(`test_paper_label_id`, `tag_id`) VALUES ()

							$insert_tags_of_lable->insert(array(
								'test_paper_label_id' => $lable_id,
								'tag_id'	=>	$tag
							));
						}
					}
				}
				$result->test_paper = $test_paper_id;
				$result->status = 1;
				$result->message = 'ok';

				return $result;
			}catch(Execption $e) {
				$result->status	=	0;
				$result->message	=	$e->getMessage();
				return $result;
			}
		}

		/**
		* function to fetch test Papers
		*
		* @author Utkarsh Pandey
		* @date 27-06-2015
		* @param json with all data
		* @return json with status,message and all data
		**/
		public function fetchTestPaper($data) {
			$result = new stdClass();
			try {

				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();

				$select->from("test_paper")
						->join(array('test_paper_type' => 'test_paper_type'), 'test_paper_type.id = test_paper.type', array('type'), 'left')
						//->join(array('test_paper_questions' => 'test_paper_questions'), 'test_paper_questions.test_paper_id = test_paper.id', array('questions' => new Expression('COUNT(test_paper_questions.test_paper_id)')), 'left')
						->join(array('test_paper_subjects' => 'test_paper_subjects'), 'test_paper_subjects.test_paper_id = test_paper.id', array(), 'left');
						//->join(array('questions' => 'questions'), 'questions.id = test_paper_questions.question_id', array(), 'left');
				$select->columns(array("id","name","type", "status"));
				$select->order('test_paper.id DESC');
				// $select->GROUP('test_paper.id');
				$where = new $select->where();
				$where->equalTo("test_paper.deleted", 0);
				//$where->equalTo("questions.parent_id", '0');
				$where->notEqualTo("test_paper.type", '0');
				//$where->equalTo("parent", 0);


				if (isset($data->class)) {
					$where->equalTo('test_paper.class', $data->class);
				}
				if (isset($data->type)) {
					$where->equalTo('test_paper.type', $data->type);
				}

				if (isset($data->subjects)) {
					$where->in('test_paper_subjects.subject_id', $data->subjects);
				}

				if (isset($data->test_paper)) {
					$where->like('test_paper.name', "%$data->test_paper%");
				}

				$select->where($where);
				$select->group('test_paper.id');
				$statement = $sql->getSqlStringForSqlObject($select);



				// $statement = 'SELECT test_paper.id AS id, test_paper.name AS name, test_paper.status AS status, test_paper_type.type, COUNT(test_paper_questions.test_paper_id) AS questions FROM test_paper LEFT JOIN test_paper_type ON  test_paper_type.id=test_paper.type LEFT JOIN test_paper_questions ON test_paper_questions.test_paper_id = test_paper.id WHERE deleted = 0 and test_paper.type != 0 GROUP BY test_paper.id ORDER BY id DESC';

				// die($statement);

				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$test_paper = $run->toArray();

				foreach ($test_paper as $key => $paper) {
					# code...
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('section')
							->join(array('labels'=>'test_paper_labels'),'labels.section_id=section.id',array('questions'=>new Expression('SUM(number_of_question)')),'left');
					$where = array();
					$where['section.test_paper']=$paper['id'];
					$select->where($where);


					$selectString = $sql->getSqlStringForSqlObject($select);

					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$count = $run->toArray();

					if(count($count)){
						$test_paper[$key]['questions'] = $count[0]['questions'];
					}
				}

				$result->status = 1;
				$result->message = "Operation Successful.";
				$result->data = $test_paper;
				return $result;
			}catch(Execption $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function fetchTestPaperDetails($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				// questions
				$select->from("test_paper");

				// ->join(array(), '', 'left');
				$select->where(array(	"test_paper.deleted" => 0,
										"test_paper.id" => $data->test_paper_id));
				$select->columns(array("id", "name", "type", "class", "time", "total_attempts", "start_time", "end_time", "instructions", "status", "mode"));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$test_paper_details = $run->toArray();

				$selectSubjectsList = $sql->select();
				$selectSubjectsList->from('subjects');
				$selectSubjectsList->columns(array('id','name'));
				$where = new $selectSubjectsList->where();
				$where->equalTo('deleted',0);
				$where->equalTo('class_id',$test_paper_details[0]['class']);
				$selectSubjectsList->where($where);
				$selectString = $sql->getSqlStringForSqlObject($selectSubjectsList);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjectsList = $run->toArray();

				$selectedSubjects = $sql->select();
				$selectedSubjects->from('test_paper_subjects');
				$selectedSubjects->columns(array('subject_id'));
				$where = new $selectedSubjects->where();
				$where->equalTo('test_paper_id',$data->test_paper_id);
				$selectedSubjects->where($where);
				$selectString = $sql->getSqlStringForSqlObject($selectedSubjects);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects = $run->toArray();

				$result->data = $test_paper_details[0];

				$result->data['subjects'] = array();
				foreach ($subjects as $value) {
					array_push($result->data['subjects'], $value['subject_id']);
				}
				$result->subjectsList = $subjectsList;


				$result->status = 1;
				$result->message = "Operation Successful.";
				return $result;
			}catch(Execption $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* function to fetch test-paper types
		*
		* @author Utkarsh Pandey
		* @date 08-06-2015
		* @param json with all data
		* @return json with status,message and all data
		**/
		public function fetchTestPaperTypes($data) {
			$result = new stdClass();
			try {

				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();

				$select->from("test_paper_type");
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->data = $run->toArray();
				$result->status = 1;
				$result->message = 'Test Paper Types';
				return $result;
			}catch(Execption $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		/**
		* function to get next question in test-paper
		*
		* @author Utkarsh Pandey
		* @date 12-07-2015
		* @param json with all data
		* @return json with status,message and all data
		**/
		public function getNextQuestion($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				// return $data;
				$select->from("test_paper_labels");
				$select->where(array(
					"id" => $data->lableId
				));
				$select->columns(array("section_id","class_id","subject_id","unit_id","chapter_id","topic_id","question_type","difficulty_level","skill_id"));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$lable =  $run->toArray();

				$select = $sql->select();
				$select->from('tags_of_test_paper_label');
				$select->where(array(
					'test_paper_label_id' => $data->lableId
				));
				$select->columns(array('tag_id'));
				$statement = $sql->getSqlStringForSqlObject($select);


				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$tags  = $run->toArray();
				$lable[0]["number_of_question"] = 1;
				$lable[0]["notIn"] = $data->notIn;
				$lable[0]["tags"] = array();

				foreach ($tags as $tag) {
					array_push($lable[0]["tags"],$tag["tag_id"]);
				}
				$test = new TestPaperQuestions();
				$temp =  $test->questionFilter($lable[0]);
				if ($temp->status == 1) {
					$result->data = $temp->data;
					$result->status = 1;
					$result->message = 'Operation Successful.';
				}else {
					$result->status = 0;
					$result->message = 'Operation Failed.'.$temp->message;
				}
				return $result;
			}catch(Execption $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}



		/**
		* function to import a question in test-paper
		*
		* @author Utkarsh Pandey
		* @date 16-07-2015
		* @param json with all data
		* @return json with status,message and all data
		**/
		public function importQuestion($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				// return $data;
				$select->from("test_paper_labels");
				$select->where(array(
					"id" => $data->lableId
				));
				$select->columns(array("section_id","class_id","subject_id","unit_id","chapter_id","topic_id","question_type","difficulty_level","skill_id"));
				$statement = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$lable =  $run->toArray();

				$select = $sql->select();
				$select->from('tags_of_test_paper_label');
				$select->where(array(
					'test_paper_label_id' => $data->lableId
				));
				$select->columns(array('tag_id'));
				$statement = $sql->getSqlStringForSqlObject($select);


				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$tags  = $run->toArray();
				if(!empty($data->count))
					$lable[0]["number_of_question"] = $data->count;
				$lable[0]["notIn"] = $data->notIn;
				$lable[0]["tags"] = array();

				foreach ($tags as $tag) {
					array_push($lable[0]["tags"],$tag["tag_id"]);
				}
				$test = new TestPaperQuestions();
				$temp =  $test->questionFilter($lable[0]);
				if ($temp->status == 1) {
					$result->data = $temp->data;
					$result->status = 1;
					$result->message = 'Operation Successful.';
				}else {
					$result->status = 0;
					$result->message = 'Operation Failed.'.$temp->message;
				}
				return $result;
			}catch(Execption $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		/**
		* function to fetch test Papers
		*
		* @author Utkarsh Pandey
		* @date 07-03-2016
		* @param json with all data
		* @return json with status,message and all data
		**/
		public function fetchTestPapersOfPragram($request) {
			$result =  new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from(array('test_papers_of_test_program' => 'test_papers_of_test_program'))
						->join(array('test_paper' => 'test_paper'), 'test_papers_of_test_program.test_paper_id = test_paper.id', array('id', 'name', 'type', 'class', 'time', 'total_attempts', 'start_time', 'end_time', 'Instructions'), 'left');

				$where = new $select->where();
				$where->equalTo('test_papers_of_test_program.test_program_id', $request->program_id);
				$select->columns(array());
				$select->where($where);
				$statement = $sql->getSqlStringForSqlObject($select);
				// die($statement);
				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->test_papers = $run->toArray();


				$result->status = 1;
				$result->message = 'Successfully Fetched';
				return $result;
			}catch(Execption $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}

		public function fetchQuestionInformation($data) {
			$result =  new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$statement = 'SELECT (CASE
								WHEN tag_type = 1 THEN (SELECT name FROM subjects WHERE id=tagged_id)
								WHEN tag_type = 2 THEN (SELECT name FROM `units` WHERE id=tagged_id)
								WHEN tag_type = 3 THEN (SELECT name FROM `chapters` WHERE id=tagged_id)
								WHEN tag_type = 4 THEN (SELECT name FROM `topics` WHERE id=tagged_id)
								WHEN tag_type = 5 THEN (SELECT tag FROM tags WHERE id=tagged_id)
								WHEN tag_type = 6 THEN (SELECT difficulty FROM difficulty WHERE id=tagged_id)
								WHEN tag_type = 7 THEN (SELECT skill FROM skills WHERE id=tagged_id)
								END) as name,tag_type,(SELECT (SELECT name FROM classes WHERE class_id=id) FROM questions where tagsofquestion.question_id = questions.id) AS class_name FROM `tagsofquestion` WHERE question_id ='.$data->question_id;


				$run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$result->tagged_data = $run->toArray();


				$result->status = 1;
				$result->message = 'ok';
				return $result;
			}catch(Execption $e) {
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


	}

?>