<?php
	@session_start();
	if(isset($_SESSION['role'])) {
		if ($_SESSION['role'] == 1 || $_SESSION['role'] == 2 || $_SESSION['role'] == 3) {
			header('Location: index.php');
			die();
		}
		if ($_SESSION['role'] == 4) {
			header('Location: my-program');
			die();
		}
		if ($_SESSION['role'] == 5) {
			header('Location: manage-students.php');
			die();
		}
		if ($_SESSION['role'] == 6) {
			header('Location: assistant-profile.php');
			die();
		}
	}
	else {
		header('Location: index.php');
	}
?>