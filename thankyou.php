<?php
require_once 'config.inc.test.php';
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
		<!-- Navigation START -->
		<?php include HEADER; ?>
		<!-- Navigation END -->

		<div class="main">
			<div class="container">
				<ul class="breadcrumb">
						<li><a href="index.php">Home</a></li>

						<li class="active" ><a href="my-program">My Exam</a></li>
				</ul>
				<!-- BEGIN SIDEBAR & CONTENT -->
				<div class="row margin-bottom-40">
					<!-- BEGIN SIDEBAR -->
					<div class="sidebar col-md-2 col-sm-3">
						<?php include('html/student/sidebar.php'); ?>
					</div>
					<!-- END SIDEBAR -->

					<!-- BEGIN CONTENT -->
					<div class="col-md-10 col-sm-9 text-center">
						<!-- Start Payment Successfull -->
						<h2>Payment Successful</h2>
						<div class="well mrg-tb40">
							<h4 class="font500">Thank You. Your payment has been successfully processed. Your purchased package has been added to your “My Exam”.</h4>
							<div class="margin-top-10 margin-bottom-10 table-scrollable">
							</div>
							<p>Click on Button to Go to view your “My Exam”</p>
							<a href="my-program" class="btn green-haze btn-center mrg-top20 inline-block">My Exam</a>
						</div>
						<!-- End Payment Successfull -->
					</div>
					<!-- END CONTENT -->

				</div>
				<!-- END SIDEBAR & CONTENT -->
			</div>
		</div>

		<!-- BEGIN PRE-FOOTER -->
		<?php include('html/footer.php'); ?>
		<!-- END FOOTER -->

		<!-- /modal -->
		<!-- START PAGE LEVEL JAVASCRIPTS -->
		<?php include('html/js-files.php'); ?>
		<?php include('html/student/js-files.php'); ?>

		<!-- <script src="assets/js/custom/student_program/my-program.js" type="text/javascript"></script> -->

		<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
