<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
  <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="active">Reset Password</li>
        </ul>
        <!-- BEGIN LOGIN -->
        <div class="content">
          <!-- BEGIN LOGIN FORM -->
          <div class="col-md-2"></div>
          <div class="col-md-8">
            <form id="reset-password-form" class="login-form" method="post">
              <h3 class="form-title">Reset Your Password</h3>
              <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">New Password</label>
                <input name="new-password" id="new-password" class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="New Password" />
              </div>
              <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Confirm Password</label>
                <input name="confirm-password" id="confirm-password" class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Confirm Password" />
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn btn-success uppercase">Reset</button>
              </div>
            </form>
          </div>
          <div class="col-md-2"></div>
          <!-- END LOGIN FORM -->
        </div>
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
  <?php include('html/footer.php'); ?>
    <!-- END FOOTER -->
  <!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
    <script type="text/javascript" src="assets/js/custom/reset-password.js"></script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->

</body>
<!-- END BODY -->
</html>