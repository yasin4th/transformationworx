<?php include 'Access-API.php'; ?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>            
            <li><a href="my-program.php">My Program</a></li>
            <li><a class="program-name"><!-- Program Name --></a></li>
            <!-- <li class="active">Subject</li> -->
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          <div class="sidebar col-md-2 col-sm-3">
            <?php include('html/student/sidebar.php'); ?>
          </div>
          <!-- END SIDEBAR -->

          <!-- BEGIN CONTENT -->
          <div class="col-md-10 col-sm-9">
          	<!-- strat div for meter -->
          	<!-- <div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="col-md-4"></div><div class="col-md-4" id="meter" style="height: 180px; "></div><div class="col-md-2"></div>
				</div>
			</div> -->
			<!-- end div for meter -->
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2> 
						<span class="program-name"> <!-- Course --> </span>
					</h2>
				</div>
				<div class="col-md-6 col-sm-6"></div>
			</div>
			
			<!-- BEGIN DASHBOARD STATS -->
			<div class="row">
				<!-- Subjects -->
				<div class="col-md-9">
					<div id="subjects-div" class="row"></div>
				</div>


				<!-- Test Types -->
				<!-- <div class="col-md-3 VTP-right-side VTP-right-side-divider">
					<div class="row">
						<div id="diagnostic" class="col-md-12 col-sm-12">
							<div data-category="1" class="dashboard-stat blue-madison">
								<div class="visual">
									<span class="count"> 0</span>
								</div>
								<div class="details">
									<div class="desc">
										<a class="view-test color-white" href="#" title="View Test">Diagnostic</a>
									</div>
								</div>
							</div>
						</div>
				
						<div id="full-syllabus-test" class="col-md-12 col-sm-12">
							<div data-category="4" class="dashboard-stat red-intense">
								<div class="visual">
									<span class="count"> 0</span>
								</div>
								<div class="details">
									<div class="desc">
										<a class="view-test color-white"  title="View Test">Mock Test</a>
									</div>
								</div>
							</div>
						</div>
				
				
						<div id="scheduled-test" class="col-md-12 col-sm-12">
							<div data-category="5" class="dashboard-stat green-haze">
								<div class="visual">
									<span class="count"> 0</span>
								</div>
								<div class="details">
									<div class="desc">
										<a class="view-test color-white"  title="View Test">Scheduled Test</a>
									</div>
								</div>
							</div>
						</div>
				
						<div id="test-gentrator" class="col-md-12 col-sm-12">
							<div class="dashboard-stat red-intense">
								<div class="visual">
								</div>
								<div class="details">
									<div class="desc">
										<a class="color-white"  title="View Test">Test Generator</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> -->
			</div>
			<!-- END DASHBOARD STATS -->
			
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
    <!-- END FOOTER -->
	
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
	<?php include('html/student/js-files.php'); ?>
	<script src="admin/assets/global/plugins/highcharts/highcharts.js" type="text/javascript"></script>
	<script src="admin/assets/global/plugins/highcharts/highcharts-more.js" type="text/javascript"></script>
	<script src="admin/assets/global/plugins/highcharts/modules/solid-gauge.js" type="text/javascript"></script>

	<script src="assets/js/custom/student-course-details.js" type="text/javascript"></script>
	
	<script type="text/javascript"> 
	$(function () {
		var gaugeOptions = {

	        chart: {
	            type: 'solidgauge'
	        },

	        title: null,

	        pane: {
	            center: ['50%', '85%'],
	            size: '140%',
	            startAngle: -90,
	            endAngle: 90,
	            background: {
	                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
	                innerRadius: '60%',
	                outerRadius: '100%',
	                shape: 'arc'
	            }
	        },

	        tooltip: {
	            enabled: false
	        },

	        // the value axis
	        yAxis: {
	            stops: [
	                [0.1, '#55BF3B'], // green
	                [0.5, '#DDDF0D'], // yellow
	                [0.9, '#DF5353'] // red
	            ],
	            lineWidth: 0,
	            minorTickInterval: null,
	            tickPixelInterval: 400,
	            tickWidth: 0,
	            title: {
	                y: -70
	            },
	            labels: {
	                y: 16
	            }
	        },

	        plotOptions: {
	            solidgauge: {
	                dataLabels: {
	                    y: 5,
	                    borderWidth: 0,
	                    useHTML: true
	                }
	            }
	        }
	    };

	    // The speed gauge
	    $('#meter').highcharts(Highcharts.merge(gaugeOptions, {
	        yAxis: {
	            min: 0,
	            max: 200,
	            title: {
	                text: ''
	            }
	        },

	        credits: {
	            enabled: false
	        },

	        series: [{
	            name: 'Points',
	            data: [80],
	            dataLabels: {
	                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
	                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
	                       '<span style="font-size:12px;color:silver">Points</span></div>'
	            },
	            tooltip: {
	                valueSuffix: ' Points'
	            }
	        }]

	    }));
	})

	</script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>