<?php
    require_once 'config.inc.test.php';
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
	<!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
		<ul class="breadcrumb">
            <li><a href="<?=URI1?>">Home</a></li>
            <li class="active">Frequently Asked Questions</li>
        </ul>
        <!-- Start Disclaimer Terms BOX -->
        <div class="row">
          <div class="col-md-12 col-sm-12">
              <h2>Frequently Asked Questions</h2>
          </div>
        </div>
		    <div class="row margin-bottom-40">
          <div class="col-md-12 col-sm-12">
		        <div class="panel-group" id="accordion1">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="#accordion1_1" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle">
                      How can I edit/update my profile details?
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse collapse in" id="accordion1_1">
                  <div class="panel-body">
                    Editing/updating your profile is very easy. Just follow these simple steps:
                    <ul>
                      <li>‘Login’ into your account.</li>
                      <li>Click on ‘My Profile’ on the left side of the page.</li>
                      <li>Just add/edit your personal and educational information. It would be automatically updated.</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="#accordion1_2" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                      How can I change my registered email id?
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse collapse" id="accordion1_2">
                  <div class="panel-body">
                    You can send us a request at support@transformationworx.com.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="#accordion1_3" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                      How do I change my password?
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse collapse" id="accordion1_3">
                  <div class="panel-body">
                    You can change your password by following these simple steps:
                    <ul>
                      <li>‘Login’ into your account.</li>
                      <li>Click on ‘My Profile’ on the left side of the page.</li>
                      <li>Click on ‘Change Password’ tab.</li>
                      <li>Enter your current password and new password and click on Change Password.</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="#accordion1_4" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                      I forgot my password. What should I do?
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse collapse" id="accordion1_4">
                  <div class="panel-body">
                    You can easily recover your password by following these easy steps:
                    <ul>
                      <li>Go to the Login screen.</li>
                      <li>Click on the ‘Forgot Password’ option.</li>
                      <li>Type your Email address in the space provided and click on the ‘Send’.</li>
                      <li>You will get an Email with a link to reset your password.</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="#accordion1_5" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                      What are the available modes of payment through which I can buy your test courses?
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse collapse" id="accordion1_5">
                  <div class="panel-body">
                    The various available payment options are:
                    <ul>
                      <li>Credit Card</li>
                      <li>ATM-cum-Debit Card</li>
                      <li>Internet Banking</li>
                      <li>Paytm Wallet</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="#accordion1_6" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                      How soon will my courses be activated?
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse collapse" id="accordion1_6">
                  <div class="panel-body">
                    Your courses will be activated instantly once your payment transaction is completed successfully.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="#accordion1_7" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                      Can I get a refund for my purchased test course?
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse collapse" id="accordion1_7">
                  <div class="panel-body">
                    No. Once a test course has been purchased successfully, we will not be able to provide any refund, either in full or in part.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="#accordion1_8" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle">
                      Can I take the tests again and again?
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse collapse" id="accordion1_8">
                  <div class="panel-body">
                    Yes, Practice Tests can be attempted multiple times. However, the other tests can only be attempted once and you can view their analysis again and again.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="#accordion1_9" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                      Can I see my test analysis again?
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse collapse" id="accordion1_9">
                  <div class="panel-body">
                    Yes, you can see your test analysis under the Report Card section on the left menu after you have logged in.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="#accordion1_10" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                      Can I get analysis of my previous tests?
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse collapse" id="accordion1_10">
                  <div class="panel-body">
                    You can view the analysis of all your previous tests under the Report Card section on the left menu.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="#accordion1_11" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                      Is there any limit to the number of tests that I can take in an hour or a day?
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse collapse" id="accordion1_11">
                  <div class="panel-body">
                    No, you are free to take any number of tests as available across your multiple test courses.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="#accordion1_12" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                      Will I get an expert for any subject related query?
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse collapse" id="accordion1_12">
                  <div class="panel-body">
                    No. We do not provide expert guidance. But with all our questions you will get answers and also detailed explanations to those answers.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="#accordion1_13" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                      If I face any problem with regard to the website, what should I do?
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse collapse" id="accordion1_13">
                  <div class="panel-body">
                    If you face any problem with regard to the website, you can contact us by sending an email at support@transformationworx.com or can call us at 011-26692293/94.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="#accordion1_14" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
                      How can I give feedback related to the website?
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse collapse" id="accordion1_14">
                  <div class="panel-body">
                    Your feedback is very valuable to us. Please write to us at support@transformationworx.com. We look forward to hearing from you.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a href="#accordion1_15" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle">
                      There's an essential feature I want to recommend. What should I do?
                    </a>
                  </h4>
                </div>
                <div class="panel-collapse collapse" id="accordion1_15">
                  <div class="panel-body">
                    We are always open to suggestions! It's good that you have ideas for us. Write to us at support@transformationworx.com.
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
        <!-- End Disclaimer Terms BOX -->

      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include FOOTER; ?>
    <!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>