<<?php
    require_once 'config.inc.php';
    require_once 'config.inc.test.php';
	@session_start(); if(isset($_SESSION['role'])) { header('location: manage-students.php');}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <?php include('html/head-tag.php'); ?>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <div style="padding-top: 100px;"></div>
    <?php// include('html/navigation.php'); ?>
    <?php include HEADER; ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?=URI1?>">Home</a></li>
            <li class="active">Institute Registration</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          <!--<div class="sidebar col-md-3 col-sm-3">
            <ul class="list-group margin-bottom-25 sidebar-menu">
              <li class="list-group-item clearfix"><a href="#"><i class="fa fa-angle-right"></i> Login/Register</a></li>
              <li class="list-group-item clearfix"><a href="#"><i class="fa fa-angle-right"></i> Restore Password</a></li>
              <li class="list-group-item clearfix"><a href="#"><i class="fa fa-angle-right"></i> My account</a></li>
              <li class="list-group-item clearfix"><a href="#"><i class="fa fa-angle-right"></i> Address book</a></li>
              <li class="list-group-item clearfix"><a href="#"><i class="fa fa-angle-right"></i> Wish list</a></li>
              <li class="list-group-item clearfix"><a href="#"><i class="fa fa-angle-right"></i> Returns</a></li>
              <li class="list-group-item clearfix"><a href="#"><i class="fa fa-angle-right"></i> Newsletter</a></li>
            </ul>
          </div>-->
          <!-- END SIDEBAR -->
          	<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
	          <!-- BEGIN CONTENT -->
	          <div class="register-div">
	          <br>
	            <h1 class="text-center">Institute Registration</h1>
	            <div class="content-form-page">
	              <div class="row">
	                <div class="col-md-12 col-sm-12">
						<form id="institute-registration-form" class="form-horizontal" role="form">
							<fieldset>
							<!-- <legend>Your personal details</legend> -->
							<div class="form-group">
								<label for="institutename" class="col-lg-4 control-label">Institute Name <span class="require">*</span></label>
								<div class="col-lg-8">
								<input type="text" class="form-control" id="institutename" placeholder="Institute Name" required="required">
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="col-lg-4 control-label">Email / User Name<span class="require">*</span></label>
								<div class="col-lg-8">
								<input type="text" class="form-control" id="email" placeholder="Email" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
								</div>
							</div>
							<div class="form-group">
								<label for="mobile" class="col-lg-4 control-label">Mobile Number</label>
								<div class="col-lg-8">
								<input type="text" class="form-control" id="mobile" placeholder="Mobile Number" required="required">
								</div>
							</div>
							</fieldset>
							<fieldset>
							<!-- <legend>Your password</legend> -->
							<div class="form-group">
								<label for="password" class="col-lg-4 control-label">Password <span class="require">*</span></label>
								<div class="col-lg-8">
								<input type="password" class="form-control" id="password" placeholder="Password" required="required">
								</div>
							</div>
							<div class="form-group">
								<label for="confirm-password" class="col-lg-4 control-label">Confirm Password <span class="require">*</span></label>
								<div class="col-lg-8">
								<input type="password" class="form-control" id="confirm-password" placeholder="Confirm Password" required="required">
								</div>
							</div>
							</fieldset>
							<div class="row">
								<div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
									<button id="instituteregister" type="submit" class="btn btn-primary">Submit</button>
									<button type="button" id="reset" class="btn btn-default">Reset</button>
								</div>
							</div>

							<div class="form-group">
								<label for="confirm-password" class="col-lg-4 control-label"></label>
								<div class="col-lg-8">
								Have an account? <a href="login"><strong>click here</strong></a>.
								</div>
							</div>

							<!-- <div class="row">
								<div class="col-lg-12 text-center">
									<h6>OR</h6>
								</div>
								<div class="col-lg-12 text-center margin-top-20">
									<a href="fbaccess.php" class="btn btn-facebook btn-sociallogin" title="Facebook"><i class="fa fa-facebook"></i> Sign in with Facebook</a>
									<a href="#" class="btn btn-google btn-sociallogin" title="Google"><i class="fa fa-google"></i> Sign in with Google</a>
								</div>
							</div> -->

						</form>
	                </div>
	              </div>
	            </div>
	          </div>
	          <!-- END CONTENT -->
          	</div>
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include FOOTER; ?>
    <!-- END FOOTER -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
	<script src="assets/js/custom/instituteRegistration.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>