<?php
	//loading zend framework
	include 'vendor/Zend/Loader/AutoloaderFactory.php';
	Zend\Loader\AutoloaderFactory::factory(array(
			'Zend\Loader\StandardAutoloader' => array(
					'autoregister_zf' => true,
					'db' => 'Zend\Db\Sql'
			)
	));

	if(isset($_GET['institute']) && isset($_GET['template']))
	{
		require_once "vendor/custom/Api.php";
		$req = new stdClass();
		$req->id = $_GET['institute'];
		$req->template  = $_GET['template'];
		$a = Api::downloadB2BTemplate($req);

		if($a!=false)
		{
			header("Content-Type: text/xml");
			header('Content-Disposition: attachment; filename="index.html"');
			header("Content-Length: " . strlen($a));
			echo $a;
			exit;
		}
		else
		{
			exit;
		}
		if(isset($_GET['filename'])) {

			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$filename = 'assets/frontend/pages/test_program/illustration_file/'.$_GET['filename'];
			$filetype = finfo_file($finfo, $filename);
			// die($filetype);
			header('Content-type: '.$filetype);
			header('Content-disposition: attachment;filename='.$filename);
			readfile($filename);
			exit();
		}
		//die('Bad Boy: No Donuts For You');
	}

	if(isset($_GET['institute']) && isset($_GET['number']) && isset($_GET['program']))
	{
		require_once "vendor/custom/Api.php";
		$req = new stdClass();
		$req->institute = $_GET['institute'];
		$req->number = $_GET['number'];
		$req->program = $_GET['program'];
		$a = Api::generateStudentIds($req);

		if($a->status == 1)
		{
			header("Content-Type: application/force-download");
			header('Content-Disposition: attachment; filename="students.csv"');
			// header("Content-Length: " . strlen($a));
			echo "UserIds,Password\n";

			foreach ($a->students as $key => $value) {
				echo $value['userid'].",".$value['password']."\n";
			}
			exit;
		}
		else
		{
			exit;
		}

		//die('Bad Boy: No Donuts For You');
	}

?>