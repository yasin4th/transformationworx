<?php
		require_once 'config.inc.test.php';
		include 'Access-API.php';
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
		<!-- Navigation START -->
		<?php include('html/navigation.php'); ?>
		<!-- Navigation END -->

		<div class="main">
			<div class="container">
				<ul class="breadcrumb">
						<li><a href="/">Home</a></li>

						<li class="active" ><a href="my-program">My Program</a></li>
				</ul>
				<!-- BEGIN SIDEBAR & CONTENT -->
				<div class="row margin-bottom-40">
					<!-- BEGIN SIDEBAR -->
					<div class="sidebar col-md-2 col-sm-3">
						<?php include('html/student/sidebar.php'); ?>
					</div>
					<!-- END SIDEBAR -->

					<!-- BEGIN CONTENT -->
					<div class="col-md-10 col-sm-9 text-center">
						<!-- Start Payment Not Successfull -->
						<h2>Payment Not Successful</h2>
						<div class="well mrg-tb40">
							<h4 class="font500">There was a problem processing your payment.</h4>
							<div class="margin-top-10 margin-bottom-10 table-scrollable">
							</div>
							<p>Click on to Button to Go Back to Courses</p>
							<a href="exams" class="btn green-haze btn-center mrg-top20 inline-block">Exams</a>
						</div>
						<!-- End Payment Not Successfull -->
					</div>
					<!-- END CONTENT -->

				</div>
				<!-- END SIDEBAR & CONTENT -->
			</div>
		</div>

		<!-- BEGIN PRE-FOOTER -->
	<?php include FOOTER; ?>
		<!-- END FOOTER -->

	<!-- /modal -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
	<?php include('html/js-files.php'); ?>
	 <?php include('html/student/js-files.php'); ?>

	 <!--  <script src="assets/js/custom/student_program/my-program.js" type="text/javascript"></script> -->

		<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
