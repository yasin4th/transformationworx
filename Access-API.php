<?php
	@session_start();

	if (!(isset($_SESSION['userId'])) || !(isset($_SESSION['user'])) || (!isset($_SESSION['role']))) {

		header('Location: index');
		// header('refresh:2;url=index.php');
		// die('Your Access Is Forbidden.');
	}
	elseif ($_SESSION['role'] == 1 || $_SESSION['role'] == 2 || $_SESSION['role'] == 3)
	{
		header('Location: admin/dashboard.php');
	}
?>