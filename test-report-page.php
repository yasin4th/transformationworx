<?php include 'Access-API.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<?php include('html/head-tag.php'); ?>
	<?php include('html/student/head-tag.php'); ?>

</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">
    <!-- Navigation START -->
    <?php include('html/navigation.php'); ?>
    <!-- Navigation END -->

    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li><a href="#">student</a></li>
            <li>Report Card</li>
            <li class="active">Test Report</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          <div class="sidebar col-md-2 col-sm-3">
            <?php include('html/student/sidebar.php'); ?>
          </div>
          <!-- END SIDEBAR -->

          <!-- BEGIN CONTENT -->
          <div class="col-md-10 col-sm-9">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<h2>Test Report: T-1001</h2>
				</div>
				<div class="col-md-6 col-sm-6">
					<ul class="pagination pull-right">
						<li>
							<a href="#">
							Prev </a>
						</li>
						<li>
							<a href="#">
							Attempt #1 </a>
						</li>
						<li>
							<a href="#">
							Next </a>
						</li>
					</ul>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-3">
							<label>Name of the student</label>
						</div>
						<div class="col-md-3">
							<label>Date of test</label>
						</div>
						<div class="col-md-3"></div>
						<div class="col-md-3"></div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="margin-top-10 margin-bottom-10 table-scrollable">
						<table class="table table-striped table-bordered table-hover">
							<thead>
							<tr>
								<th>

								</th>
								<th>
									<a href="#section1">Section 1</a>
								</th>
								<th>
									<a href="#section1">Section 2</a>
								</th>
								<th>
									<a href="#section1">Section 3</a>
								</th>
								<th>
									Combined report
								</th>
							</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										Total Questions
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										75
									</td>
								</tr>
								<tr>
									<td>
										Attempted
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										75
									</td>
								</tr>
								<tr>
									<td>
										Correct
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										75
									</td>
								</tr>
								<tr>
									<td>
										Maximum Marks
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										75
									</td>
								</tr>
								<tr>
									<td>
										Net Score
									</td>
									<td>
										20
									</td>
									<td>
										20
									</td>
									<td>
										20
									</td>
									<td>
										60
									</td>
								</tr>
								<tr>
									<td>
										% Marks
									</td>
									<td>
										20
									</td>
									<td>
										20
									</td>
									<td>
										20
									</td>
									<td>
										60
									</td>
								</tr>
								<tr>
									<td>
										Strike Rate
									</td>
									<td>
										1
									</td>
									<td>
										2
									</td>
									<td>
										3
									</td>
									<td>
										5
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<div class="col-md-12">
					<h4 id="section1">Setion 1: Topic wise report</h4>
					<div class="margin-top-10 margin-bottom-10 table-scrollable">
						<table class="table table-striped table-bordered table-hover">
							<thead>
							<tr>
								<th>
									Name of Topic
								</th>
								<th>
									Total Question
								</th>
								<th>
									Attempt
								</th>
								<th>
									Correct
								</th>
								<th>
									Maximum Marks
								</th>
								<th>
									Marks Scored
								</th>
								<th>
									% Mraks
								</th>
							</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										Topic 1
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										75
									</td>
									<td>
										25
									</td>
									<td>
										75
									</td>
								</tr>
								<tr>
									<td>
										Topic 2
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										75
									</td>
									<td>
										25
									</td>
									<td>
										75
									</td>
								</tr>
								<tr>
									<td>
										Topic 3
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										75
									</td>
									<td>
										25
									</td>
									<td>
										75
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<div class="col-md-12">
					<h4>Setion 1: Skill wise report</h4>
					<div class="margin-top-10 margin-bottom-10 table-scrollable">
						<table class="table table-striped table-bordered table-hover">
							<thead>
							<tr>
								<th>
									Name of Skill
								</th>
								<th>
									Total Question
								</th>
								<th>
									Attempt
								</th>
								<th>
									Correct
								</th>
								<th>
									Maximum Marks
								</th>
								<th>
									Marks Scored
								</th>
								<th>
									% Mraks
								</th>
							</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										Knowledge
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										75
									</td>
									<td>
										25
									</td>
									<td>
										75
									</td>
								</tr>
								<tr>
									<td>
										Application
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										75
									</td>
									<td>
										25
									</td>
									<td>
										75
									</td>
								</tr>
								<tr>
									<td>
										Skill
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										25
									</td>
									<td>
										75
									</td>
									<td>
										25
									</td>
									<td>
										75
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<!-- END PAGE CONTENT-->
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
	<?php include('html/footer.php'); ?>
    <!-- END FOOTER -->

	<!-- /modal -->
	<!-- START PAGE LEVEL JAVASCRIPTS -->
    <?php include('html/js-files.php'); ?>
	<?php include('html/student/js-files.php'); ?>


	<script src="assets/global/plugins/jquery.form.js"></script>
	<script src="assets/js/custom/studentProfile.js" type="text/javascript"></script>


    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>